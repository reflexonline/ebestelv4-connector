# SaleAnalysisApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleAnalysisGetCustomerSaleAnalysis**](SaleAnalysisApi.md#saleAnalysisGetCustomerSaleAnalysis) | **POST** /Customer/{customerId}/SaleAnalysis | 


<a name="saleAnalysisGetCustomerSaleAnalysis"></a>
# **saleAnalysisGetCustomerSaleAnalysis**
> Object saleAnalysisGetCustomerSaleAnalysis(customerId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleAnalysisApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleAnalysisApi apiInstance = new SaleAnalysisApi();
Integer customerId = 56; // Integer | 
CustomerSaleAnalysisDto dto = new CustomerSaleAnalysisDto(); // CustomerSaleAnalysisDto | 
try {
    Object result = apiInstance.saleAnalysisGetCustomerSaleAnalysis(customerId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleAnalysisApi#saleAnalysisGetCustomerSaleAnalysis");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **dto** | [**CustomerSaleAnalysisDto**](CustomerSaleAnalysisDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

