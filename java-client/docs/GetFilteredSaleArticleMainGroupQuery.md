
# GetFilteredSaleArticleMainGroupQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mainGroupIdMinValue** | **Integer** |  |  [optional]
**mainGroupIdMaxValue** | **Integer** |  |  [optional]
**mainGroupDescriptionMinValue** | **String** |  |  [optional]
**mainGroupDescriptionMaxValue** | **String** |  |  [optional]
**isInternetGroupMinValue** | **Boolean** |  |  [optional]
**isInternetGroupMaxValue** | **Boolean** |  |  [optional]



