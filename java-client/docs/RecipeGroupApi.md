# RecipeGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**recipeGroupCreateRecipeGroup**](RecipeGroupApi.md#recipeGroupCreateRecipeGroup) | **POST** /RecipeGroup | 
[**recipeGroupDeleteRecipeGroup**](RecipeGroupApi.md#recipeGroupDeleteRecipeGroup) | **DELETE** /RecipeGroup/{recipeGroupId} | 
[**recipeGroupExistsRecipeGroup**](RecipeGroupApi.md#recipeGroupExistsRecipeGroup) | **GET** /RecipeGroup/exists/{recipeGroupId} | 
[**recipeGroupGetRecipeGroup**](RecipeGroupApi.md#recipeGroupGetRecipeGroup) | **GET** /RecipeGroup/{recipeGroupId} | 
[**recipeGroupGetRecipeGroups**](RecipeGroupApi.md#recipeGroupGetRecipeGroups) | **POST** /RecipeGroup/search | 
[**recipeGroupUpdateRecipeGroup**](RecipeGroupApi.md#recipeGroupUpdateRecipeGroup) | **PUT** /RecipeGroup/{recipeGroupId} | 


<a name="recipeGroupCreateRecipeGroup"></a>
# **recipeGroupCreateRecipeGroup**
> Object recipeGroupCreateRecipeGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeGroupApi apiInstance = new RecipeGroupApi();
RecipeGroupDto dto = new RecipeGroupDto(); // RecipeGroupDto | 
try {
    Object result = apiInstance.recipeGroupCreateRecipeGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeGroupApi#recipeGroupCreateRecipeGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**RecipeGroupDto**](RecipeGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="recipeGroupDeleteRecipeGroup"></a>
# **recipeGroupDeleteRecipeGroup**
> Object recipeGroupDeleteRecipeGroup(recipeGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeGroupApi apiInstance = new RecipeGroupApi();
Integer recipeGroupId = 56; // Integer | 
try {
    Object result = apiInstance.recipeGroupDeleteRecipeGroup(recipeGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeGroupApi#recipeGroupDeleteRecipeGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipeGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="recipeGroupExistsRecipeGroup"></a>
# **recipeGroupExistsRecipeGroup**
> Object recipeGroupExistsRecipeGroup(recipeGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeGroupApi apiInstance = new RecipeGroupApi();
Integer recipeGroupId = 56; // Integer | 
try {
    Object result = apiInstance.recipeGroupExistsRecipeGroup(recipeGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeGroupApi#recipeGroupExistsRecipeGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipeGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="recipeGroupGetRecipeGroup"></a>
# **recipeGroupGetRecipeGroup**
> Object recipeGroupGetRecipeGroup(recipeGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeGroupApi apiInstance = new RecipeGroupApi();
Integer recipeGroupId = 56; // Integer | 
try {
    Object result = apiInstance.recipeGroupGetRecipeGroup(recipeGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeGroupApi#recipeGroupGetRecipeGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipeGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="recipeGroupGetRecipeGroups"></a>
# **recipeGroupGetRecipeGroups**
> Object recipeGroupGetRecipeGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeGroupApi apiInstance = new RecipeGroupApi();
GetFilteredRecipeGroupQuery query = new GetFilteredRecipeGroupQuery(); // GetFilteredRecipeGroupQuery | 
try {
    Object result = apiInstance.recipeGroupGetRecipeGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeGroupApi#recipeGroupGetRecipeGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredRecipeGroupQuery**](GetFilteredRecipeGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="recipeGroupUpdateRecipeGroup"></a>
# **recipeGroupUpdateRecipeGroup**
> Object recipeGroupUpdateRecipeGroup(recipeGroupId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeGroupApi apiInstance = new RecipeGroupApi();
Integer recipeGroupId = 56; // Integer | 
RecipeGroupDto dto = new RecipeGroupDto(); // RecipeGroupDto | 
try {
    Object result = apiInstance.recipeGroupUpdateRecipeGroup(recipeGroupId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeGroupApi#recipeGroupUpdateRecipeGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipeGroupId** | **Integer**|  |
 **dto** | [**RecipeGroupDto**](RecipeGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

