
# PaymentMethodDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paymentMethodId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**currency** | **Integer** |  |  [optional]
**journal** | **String** |  |  [optional]
**ledger** | **String** |  |  [optional]
**costCentre** | **String** |  |  [optional]
**allowNegativeBalance** | **Boolean** |  |  [optional]
**roundMethod** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



