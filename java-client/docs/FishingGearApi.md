# FishingGearApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fishingGearCreateFishingGear**](FishingGearApi.md#fishingGearCreateFishingGear) | **POST** /FishingGear | 
[**fishingGearDeleteFishingGear**](FishingGearApi.md#fishingGearDeleteFishingGear) | **DELETE** /FishingGear/{fishingGearId} | 
[**fishingGearExistsFishingGear**](FishingGearApi.md#fishingGearExistsFishingGear) | **GET** /FishingGear/exists/{fishingGearId} | 
[**fishingGearGetFishingGear**](FishingGearApi.md#fishingGearGetFishingGear) | **GET** /FishingGear/{fishingGearId} | 
[**fishingGearGetFishingGears**](FishingGearApi.md#fishingGearGetFishingGears) | **POST** /FishingGear/search | 
[**fishingGearUpdateFishingGear**](FishingGearApi.md#fishingGearUpdateFishingGear) | **PUT** /FishingGear/{fishingGearId} | 


<a name="fishingGearCreateFishingGear"></a>
# **fishingGearCreateFishingGear**
> Object fishingGearCreateFishingGear(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FishingGearApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

FishingGearApi apiInstance = new FishingGearApi();
FishingGearDto dto = new FishingGearDto(); // FishingGearDto | 
try {
    Object result = apiInstance.fishingGearCreateFishingGear(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FishingGearApi#fishingGearCreateFishingGear");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**FishingGearDto**](FishingGearDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="fishingGearDeleteFishingGear"></a>
# **fishingGearDeleteFishingGear**
> Object fishingGearDeleteFishingGear(fishingGearId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FishingGearApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

FishingGearApi apiInstance = new FishingGearApi();
Integer fishingGearId = 56; // Integer | 
try {
    Object result = apiInstance.fishingGearDeleteFishingGear(fishingGearId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FishingGearApi#fishingGearDeleteFishingGear");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fishingGearId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="fishingGearExistsFishingGear"></a>
# **fishingGearExistsFishingGear**
> Object fishingGearExistsFishingGear(fishingGearId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FishingGearApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

FishingGearApi apiInstance = new FishingGearApi();
Integer fishingGearId = 56; // Integer | 
try {
    Object result = apiInstance.fishingGearExistsFishingGear(fishingGearId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FishingGearApi#fishingGearExistsFishingGear");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fishingGearId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="fishingGearGetFishingGear"></a>
# **fishingGearGetFishingGear**
> Object fishingGearGetFishingGear(fishingGearId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FishingGearApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

FishingGearApi apiInstance = new FishingGearApi();
Integer fishingGearId = 56; // Integer | 
try {
    Object result = apiInstance.fishingGearGetFishingGear(fishingGearId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FishingGearApi#fishingGearGetFishingGear");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fishingGearId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="fishingGearGetFishingGears"></a>
# **fishingGearGetFishingGears**
> Object fishingGearGetFishingGears(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FishingGearApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

FishingGearApi apiInstance = new FishingGearApi();
GetFilteredFishingGearQuery query = new GetFilteredFishingGearQuery(); // GetFilteredFishingGearQuery | 
try {
    Object result = apiInstance.fishingGearGetFishingGears(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FishingGearApi#fishingGearGetFishingGears");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredFishingGearQuery**](GetFilteredFishingGearQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="fishingGearUpdateFishingGear"></a>
# **fishingGearUpdateFishingGear**
> Object fishingGearUpdateFishingGear(fishingGearId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FishingGearApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

FishingGearApi apiInstance = new FishingGearApi();
Integer fishingGearId = 56; // Integer | 
FishingGearDto dto = new FishingGearDto(); // FishingGearDto | 
try {
    Object result = apiInstance.fishingGearUpdateFishingGear(fishingGearId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FishingGearApi#fishingGearUpdateFishingGear");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fishingGearId** | **Integer**|  |
 **dto** | [**FishingGearDto**](FishingGearDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

