
# PurchaseShortageReasonDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shortageReasonId** | **Integer** |  | 
**recordId** | **Integer** |  |  [optional]
**description** | **String** |  |  [optional]
**labelNumber** | **Integer** |  |  [optional]
**shortDescription** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



