# LotApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**lotCreateLot**](LotApi.md#lotCreateLot) | **POST** /Lot | 
[**lotDeleteLot**](LotApi.md#lotDeleteLot) | **DELETE** /Lot/{lotId} | 
[**lotExistsLot**](LotApi.md#lotExistsLot) | **GET** /Lot/exists/{lotId} | 
[**lotGetLot**](LotApi.md#lotGetLot) | **GET** /Lot/{lotId} | 
[**lotGetLots**](LotApi.md#lotGetLots) | **POST** /Lot/search | 
[**lotUpdateLot**](LotApi.md#lotUpdateLot) | **PUT** /Lot/{lotId} | 


<a name="lotCreateLot"></a>
# **lotCreateLot**
> Object lotCreateLot(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LotApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LotApi apiInstance = new LotApi();
LotDto dto = new LotDto(); // LotDto | 
try {
    Object result = apiInstance.lotCreateLot(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LotApi#lotCreateLot");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**LotDto**](LotDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="lotDeleteLot"></a>
# **lotDeleteLot**
> Object lotDeleteLot(lotId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LotApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LotApi apiInstance = new LotApi();
Integer lotId = 56; // Integer | 
try {
    Object result = apiInstance.lotDeleteLot(lotId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LotApi#lotDeleteLot");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lotId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="lotExistsLot"></a>
# **lotExistsLot**
> Object lotExistsLot(lotId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LotApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LotApi apiInstance = new LotApi();
Integer lotId = 56; // Integer | 
try {
    Object result = apiInstance.lotExistsLot(lotId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LotApi#lotExistsLot");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lotId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="lotGetLot"></a>
# **lotGetLot**
> Object lotGetLot(lotId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LotApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LotApi apiInstance = new LotApi();
Integer lotId = 56; // Integer | 
try {
    Object result = apiInstance.lotGetLot(lotId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LotApi#lotGetLot");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lotId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="lotGetLots"></a>
# **lotGetLots**
> Object lotGetLots(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LotApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LotApi apiInstance = new LotApi();
GetFilteredLotQuery query = new GetFilteredLotQuery(); // GetFilteredLotQuery | 
try {
    Object result = apiInstance.lotGetLots(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LotApi#lotGetLots");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredLotQuery**](GetFilteredLotQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="lotUpdateLot"></a>
# **lotUpdateLot**
> Object lotUpdateLot(lotId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LotApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LotApi apiInstance = new LotApi();
Integer lotId = 56; // Integer | 
LotDto dto = new LotDto(); // LotDto | 
try {
    Object result = apiInstance.lotUpdateLot(lotId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LotApi#lotUpdateLot");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lotId** | **Integer**|  |
 **dto** | [**LotDto**](LotDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

