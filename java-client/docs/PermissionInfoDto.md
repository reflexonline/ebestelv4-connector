
# PermissionInfoDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applicationIdentifiers** | **List&lt;String&gt;** |  |  [optional]
**moduleIdentifier** | **String** |  |  [optional]
**permission** | [**IdentifierDto**](IdentifierDto.md) |  |  [optional]



