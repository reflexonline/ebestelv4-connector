
# GetFilteredDeliveryRouteQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]
**departureDayMinValue** | **Integer** |  |  [optional]
**departureDayMaxValue** | **Integer** |  |  [optional]
**deliveryMomentMinValue** | [**DeliveryMomentMinValueEnum**](#DeliveryMomentMinValueEnum) |  |  [optional]
**deliveryMomentMaxValue** | [**DeliveryMomentMaxValueEnum**](#DeliveryMomentMaxValueEnum) |  |  [optional]


<a name="DeliveryMomentMinValueEnum"></a>
## Enum: DeliveryMomentMinValueEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
MORNING | &quot;Morning&quot;
AFTERNOON | &quot;Afternoon&quot;


<a name="DeliveryMomentMaxValueEnum"></a>
## Enum: DeliveryMomentMaxValueEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
MORNING | &quot;Morning&quot;
AFTERNOON | &quot;Afternoon&quot;



