
# LineSaleArticleDescriptionTranslationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**languageId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



