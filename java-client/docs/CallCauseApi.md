# CallCauseApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**callCauseCreateCallCause**](CallCauseApi.md#callCauseCreateCallCause) | **POST** /CallCause | 
[**callCauseDeleteCallCause**](CallCauseApi.md#callCauseDeleteCallCause) | **DELETE** /CallCause/{number} | 
[**callCauseExistsCallCause**](CallCauseApi.md#callCauseExistsCallCause) | **GET** /CallCause/exists/{number} | 
[**callCauseGetCallCause**](CallCauseApi.md#callCauseGetCallCause) | **GET** /CallCause/{number} | 
[**callCauseGetCallCauses**](CallCauseApi.md#callCauseGetCallCauses) | **POST** /CallCause/search | 
[**callCauseUpdateCallCause**](CallCauseApi.md#callCauseUpdateCallCause) | **PUT** /CallCause/{number} | 


<a name="callCauseCreateCallCause"></a>
# **callCauseCreateCallCause**
> Object callCauseCreateCallCause(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallCauseApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallCauseApi apiInstance = new CallCauseApi();
CallCauseDto dto = new CallCauseDto(); // CallCauseDto | 
try {
    Object result = apiInstance.callCauseCreateCallCause(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallCauseApi#callCauseCreateCallCause");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**CallCauseDto**](CallCauseDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="callCauseDeleteCallCause"></a>
# **callCauseDeleteCallCause**
> Object callCauseDeleteCallCause(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallCauseApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallCauseApi apiInstance = new CallCauseApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.callCauseDeleteCallCause(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallCauseApi#callCauseDeleteCallCause");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="callCauseExistsCallCause"></a>
# **callCauseExistsCallCause**
> Object callCauseExistsCallCause(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallCauseApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallCauseApi apiInstance = new CallCauseApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.callCauseExistsCallCause(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallCauseApi#callCauseExistsCallCause");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="callCauseGetCallCause"></a>
# **callCauseGetCallCause**
> Object callCauseGetCallCause(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallCauseApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallCauseApi apiInstance = new CallCauseApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.callCauseGetCallCause(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallCauseApi#callCauseGetCallCause");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="callCauseGetCallCauses"></a>
# **callCauseGetCallCauses**
> Object callCauseGetCallCauses(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallCauseApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallCauseApi apiInstance = new CallCauseApi();
GetFilteredCallCauseQuery query = new GetFilteredCallCauseQuery(); // GetFilteredCallCauseQuery | 
try {
    Object result = apiInstance.callCauseGetCallCauses(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallCauseApi#callCauseGetCallCauses");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredCallCauseQuery**](GetFilteredCallCauseQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="callCauseUpdateCallCause"></a>
# **callCauseUpdateCallCause**
> Object callCauseUpdateCallCause(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallCauseApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallCauseApi apiInstance = new CallCauseApi();
Integer number = 56; // Integer | 
CallCauseDto dto = new CallCauseDto(); // CallCauseDto | 
try {
    Object result = apiInstance.callCauseUpdateCallCause(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallCauseApi#callCauseUpdateCallCause");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**CallCauseDto**](CallCauseDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

