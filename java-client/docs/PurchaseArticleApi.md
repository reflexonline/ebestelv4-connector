# PurchaseArticleApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purchaseArticleCreatePurchaseArticle**](PurchaseArticleApi.md#purchaseArticleCreatePurchaseArticle) | **POST** /PurchaseArticle | 
[**purchaseArticleDeletePurchaseArticle**](PurchaseArticleApi.md#purchaseArticleDeletePurchaseArticle) | **DELETE** /PurchaseArticle/{number} | 
[**purchaseArticleExistsPurchaseArticle**](PurchaseArticleApi.md#purchaseArticleExistsPurchaseArticle) | **GET** /PurchaseArticle/exists/{number} | 
[**purchaseArticleGetPurchaseArticle**](PurchaseArticleApi.md#purchaseArticleGetPurchaseArticle) | **GET** /PurchaseArticle/{number} | 
[**purchaseArticleGetPurchaseArticlePriceComponents**](PurchaseArticleApi.md#purchaseArticleGetPurchaseArticlePriceComponents) | **GET** /PurchaseArticle/{purchaseArticleId}/Price/Components | 
[**purchaseArticleGetPurchaseArticlePriceComponents_0**](PurchaseArticleApi.md#purchaseArticleGetPurchaseArticlePriceComponents_0) | **POST** /PurchaseArticle/{purchaseArticleId}/Price/Components | 
[**purchaseArticleGetPurchaseArticlePriceComponents_1**](PurchaseArticleApi.md#purchaseArticleGetPurchaseArticlePriceComponents_1) | **GET** /PurchaseArticle/{purchaseArticleId}/Price/{supplierId}/Components | 
[**purchaseArticleGetPurchaseArticlePriceComponents_2**](PurchaseArticleApi.md#purchaseArticleGetPurchaseArticlePriceComponents_2) | **POST** /PurchaseArticle/{purchaseArticleId}/Price/{supplierId}/Components | 
[**purchaseArticleGetPurchaseArticles**](PurchaseArticleApi.md#purchaseArticleGetPurchaseArticles) | **POST** /PurchaseArticle/search | 
[**purchaseArticleUpdatePurchaseArticle**](PurchaseArticleApi.md#purchaseArticleUpdatePurchaseArticle) | **PUT** /PurchaseArticle/{number} | 


<a name="purchaseArticleCreatePurchaseArticle"></a>
# **purchaseArticleCreatePurchaseArticle**
> Object purchaseArticleCreatePurchaseArticle(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleApi apiInstance = new PurchaseArticleApi();
PurchaseArticleDto dto = new PurchaseArticleDto(); // PurchaseArticleDto | 
try {
    Object result = apiInstance.purchaseArticleCreatePurchaseArticle(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleApi#purchaseArticleCreatePurchaseArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PurchaseArticleDto**](PurchaseArticleDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseArticleDeletePurchaseArticle"></a>
# **purchaseArticleDeletePurchaseArticle**
> Object purchaseArticleDeletePurchaseArticle(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleApi apiInstance = new PurchaseArticleApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.purchaseArticleDeletePurchaseArticle(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleApi#purchaseArticleDeletePurchaseArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseArticleExistsPurchaseArticle"></a>
# **purchaseArticleExistsPurchaseArticle**
> Object purchaseArticleExistsPurchaseArticle(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleApi apiInstance = new PurchaseArticleApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.purchaseArticleExistsPurchaseArticle(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleApi#purchaseArticleExistsPurchaseArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseArticleGetPurchaseArticle"></a>
# **purchaseArticleGetPurchaseArticle**
> Object purchaseArticleGetPurchaseArticle(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleApi apiInstance = new PurchaseArticleApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.purchaseArticleGetPurchaseArticle(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleApi#purchaseArticleGetPurchaseArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseArticleGetPurchaseArticlePriceComponents"></a>
# **purchaseArticleGetPurchaseArticlePriceComponents**
> Object purchaseArticleGetPurchaseArticlePriceComponents(purchaseArticleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleApi apiInstance = new PurchaseArticleApi();
Integer purchaseArticleId = 56; // Integer | 
try {
    Object result = apiInstance.purchaseArticleGetPurchaseArticlePriceComponents(purchaseArticleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleApi#purchaseArticleGetPurchaseArticlePriceComponents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchaseArticleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseArticleGetPurchaseArticlePriceComponents_0"></a>
# **purchaseArticleGetPurchaseArticlePriceComponents_0**
> Object purchaseArticleGetPurchaseArticlePriceComponents_0(purchaseArticleId, query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleApi apiInstance = new PurchaseArticleApi();
Integer purchaseArticleId = 56; // Integer | 
GetPurchaseArticlePriceComponentsQuery query = new GetPurchaseArticlePriceComponentsQuery(); // GetPurchaseArticlePriceComponentsQuery | 
try {
    Object result = apiInstance.purchaseArticleGetPurchaseArticlePriceComponents_0(purchaseArticleId, query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleApi#purchaseArticleGetPurchaseArticlePriceComponents_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchaseArticleId** | **Integer**|  |
 **query** | [**GetPurchaseArticlePriceComponentsQuery**](GetPurchaseArticlePriceComponentsQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseArticleGetPurchaseArticlePriceComponents_1"></a>
# **purchaseArticleGetPurchaseArticlePriceComponents_1**
> Object purchaseArticleGetPurchaseArticlePriceComponents_1(purchaseArticleId, supplierId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleApi apiInstance = new PurchaseArticleApi();
Integer purchaseArticleId = 56; // Integer | 
Integer supplierId = 56; // Integer | 
try {
    Object result = apiInstance.purchaseArticleGetPurchaseArticlePriceComponents_1(purchaseArticleId, supplierId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleApi#purchaseArticleGetPurchaseArticlePriceComponents_1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchaseArticleId** | **Integer**|  |
 **supplierId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseArticleGetPurchaseArticlePriceComponents_2"></a>
# **purchaseArticleGetPurchaseArticlePriceComponents_2**
> Object purchaseArticleGetPurchaseArticlePriceComponents_2(purchaseArticleId, supplierId, query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleApi apiInstance = new PurchaseArticleApi();
Integer purchaseArticleId = 56; // Integer | 
Integer supplierId = 56; // Integer | 
GetPurchaseArticlePriceComponentsQuery query = new GetPurchaseArticlePriceComponentsQuery(); // GetPurchaseArticlePriceComponentsQuery | 
try {
    Object result = apiInstance.purchaseArticleGetPurchaseArticlePriceComponents_2(purchaseArticleId, supplierId, query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleApi#purchaseArticleGetPurchaseArticlePriceComponents_2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchaseArticleId** | **Integer**|  |
 **supplierId** | **Integer**|  |
 **query** | [**GetPurchaseArticlePriceComponentsQuery**](GetPurchaseArticlePriceComponentsQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseArticleGetPurchaseArticles"></a>
# **purchaseArticleGetPurchaseArticles**
> Object purchaseArticleGetPurchaseArticles(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleApi apiInstance = new PurchaseArticleApi();
GetFilteredPurchaseArticleQuery query = new GetFilteredPurchaseArticleQuery(); // GetFilteredPurchaseArticleQuery | 
try {
    Object result = apiInstance.purchaseArticleGetPurchaseArticles(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleApi#purchaseArticleGetPurchaseArticles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPurchaseArticleQuery**](GetFilteredPurchaseArticleQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseArticleUpdatePurchaseArticle"></a>
# **purchaseArticleUpdatePurchaseArticle**
> Object purchaseArticleUpdatePurchaseArticle(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleApi apiInstance = new PurchaseArticleApi();
Integer number = 56; // Integer | 
PurchaseArticleDto dto = new PurchaseArticleDto(); // PurchaseArticleDto | 
try {
    Object result = apiInstance.purchaseArticleUpdatePurchaseArticle(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleApi#purchaseArticleUpdatePurchaseArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**PurchaseArticleDto**](PurchaseArticleDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

