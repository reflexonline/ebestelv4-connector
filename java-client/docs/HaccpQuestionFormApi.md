# HaccpQuestionFormApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**haccpQuestionFormCreateHaccpQuestionForm**](HaccpQuestionFormApi.md#haccpQuestionFormCreateHaccpQuestionForm) | **POST** /HaccpQuestionForm | 
[**haccpQuestionFormDeleteHaccpQuestionForm**](HaccpQuestionFormApi.md#haccpQuestionFormDeleteHaccpQuestionForm) | **DELETE** /HaccpQuestionForm/{number} | 
[**haccpQuestionFormExistsHaccpQuestionForm**](HaccpQuestionFormApi.md#haccpQuestionFormExistsHaccpQuestionForm) | **GET** /HaccpQuestionForm/exists/{number} | 
[**haccpQuestionFormGetHaccpQuestionForm**](HaccpQuestionFormApi.md#haccpQuestionFormGetHaccpQuestionForm) | **GET** /HaccpQuestionForm/{number} | 
[**haccpQuestionFormGetHaccpQuestionForms**](HaccpQuestionFormApi.md#haccpQuestionFormGetHaccpQuestionForms) | **POST** /HaccpQuestionForm/search | 
[**haccpQuestionFormUpdateHaccpQuestionForm**](HaccpQuestionFormApi.md#haccpQuestionFormUpdateHaccpQuestionForm) | **PUT** /HaccpQuestionForm/{number} | 


<a name="haccpQuestionFormCreateHaccpQuestionForm"></a>
# **haccpQuestionFormCreateHaccpQuestionForm**
> Object haccpQuestionFormCreateHaccpQuestionForm(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.HaccpQuestionFormApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

HaccpQuestionFormApi apiInstance = new HaccpQuestionFormApi();
HaccpQuestionFormDto dto = new HaccpQuestionFormDto(); // HaccpQuestionFormDto | 
try {
    Object result = apiInstance.haccpQuestionFormCreateHaccpQuestionForm(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HaccpQuestionFormApi#haccpQuestionFormCreateHaccpQuestionForm");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**HaccpQuestionFormDto**](HaccpQuestionFormDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="haccpQuestionFormDeleteHaccpQuestionForm"></a>
# **haccpQuestionFormDeleteHaccpQuestionForm**
> Object haccpQuestionFormDeleteHaccpQuestionForm(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.HaccpQuestionFormApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

HaccpQuestionFormApi apiInstance = new HaccpQuestionFormApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.haccpQuestionFormDeleteHaccpQuestionForm(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HaccpQuestionFormApi#haccpQuestionFormDeleteHaccpQuestionForm");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="haccpQuestionFormExistsHaccpQuestionForm"></a>
# **haccpQuestionFormExistsHaccpQuestionForm**
> Object haccpQuestionFormExistsHaccpQuestionForm(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.HaccpQuestionFormApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

HaccpQuestionFormApi apiInstance = new HaccpQuestionFormApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.haccpQuestionFormExistsHaccpQuestionForm(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HaccpQuestionFormApi#haccpQuestionFormExistsHaccpQuestionForm");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="haccpQuestionFormGetHaccpQuestionForm"></a>
# **haccpQuestionFormGetHaccpQuestionForm**
> Object haccpQuestionFormGetHaccpQuestionForm(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.HaccpQuestionFormApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

HaccpQuestionFormApi apiInstance = new HaccpQuestionFormApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.haccpQuestionFormGetHaccpQuestionForm(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HaccpQuestionFormApi#haccpQuestionFormGetHaccpQuestionForm");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="haccpQuestionFormGetHaccpQuestionForms"></a>
# **haccpQuestionFormGetHaccpQuestionForms**
> Object haccpQuestionFormGetHaccpQuestionForms(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.HaccpQuestionFormApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

HaccpQuestionFormApi apiInstance = new HaccpQuestionFormApi();
GetFilteredHaccpQuestionFormQuery query = new GetFilteredHaccpQuestionFormQuery(); // GetFilteredHaccpQuestionFormQuery | 
try {
    Object result = apiInstance.haccpQuestionFormGetHaccpQuestionForms(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HaccpQuestionFormApi#haccpQuestionFormGetHaccpQuestionForms");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredHaccpQuestionFormQuery**](GetFilteredHaccpQuestionFormQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="haccpQuestionFormUpdateHaccpQuestionForm"></a>
# **haccpQuestionFormUpdateHaccpQuestionForm**
> Object haccpQuestionFormUpdateHaccpQuestionForm(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.HaccpQuestionFormApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

HaccpQuestionFormApi apiInstance = new HaccpQuestionFormApi();
Integer number = 56; // Integer | 
HaccpQuestionFormDto dto = new HaccpQuestionFormDto(); // HaccpQuestionFormDto | 
try {
    Object result = apiInstance.haccpQuestionFormUpdateHaccpQuestionForm(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HaccpQuestionFormApi#haccpQuestionFormUpdateHaccpQuestionForm");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**HaccpQuestionFormDto**](HaccpQuestionFormDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

