
# SupplierPostalAddressDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**addressLine1** | **String** |  |  [optional]
**addressLine2** | **String** |  |  [optional]
**postalCode** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**country** | **String** |  |  [optional]
**phoneNumber** | **String** |  |  [optional]
**faxNumber** | **String** |  |  [optional]
**contactPerson** | **String** |  |  [optional]
**isoCountryCode** | **String** |  |  [optional]
**longName** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



