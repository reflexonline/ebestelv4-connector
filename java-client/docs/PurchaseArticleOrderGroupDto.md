
# PurchaseArticleOrderGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purchaseArticleOrderGroupId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



