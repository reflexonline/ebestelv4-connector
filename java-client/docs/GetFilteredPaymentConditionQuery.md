
# GetFilteredPaymentConditionQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paymentConditionIdMinValue** | **Integer** |  |  [optional]
**paymentConditionIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]
**defaultInvoiceStatusIdMinValue** | **Integer** |  |  [optional]
**defaultInvoiceStatusIdMaxValue** | **Integer** |  |  [optional]
**vatScenarioIdMinValue** | **Integer** |  |  [optional]
**vatScenarioIdMaxValue** | **Integer** |  |  [optional]



