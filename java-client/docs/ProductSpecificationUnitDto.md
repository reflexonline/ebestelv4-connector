
# ProductSpecificationUnitDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productSpecificationUnitId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**unit** | **String** |  |  [optional]
**akom** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



