# ProductSpecificationCategoryApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productSpecificationCategoryCreateProductSpecificationCategory**](ProductSpecificationCategoryApi.md#productSpecificationCategoryCreateProductSpecificationCategory) | **POST** /ProductSpecificationCategory | 
[**productSpecificationCategoryDeleteProductSpecificationCategory**](ProductSpecificationCategoryApi.md#productSpecificationCategoryDeleteProductSpecificationCategory) | **DELETE** /ProductSpecificationCategory/{productSpecificationCategorieId} | 
[**productSpecificationCategoryExistsProductSpecificationCategory**](ProductSpecificationCategoryApi.md#productSpecificationCategoryExistsProductSpecificationCategory) | **GET** /ProductSpecificationCategory/exists/{productSpecificationCategorieId} | 
[**productSpecificationCategoryGetProductSpecificationCategories**](ProductSpecificationCategoryApi.md#productSpecificationCategoryGetProductSpecificationCategories) | **POST** /ProductSpecificationCategory/search | 
[**productSpecificationCategoryGetProductSpecificationCategory**](ProductSpecificationCategoryApi.md#productSpecificationCategoryGetProductSpecificationCategory) | **GET** /ProductSpecificationCategory/{productSpecificationCategorieId} | 
[**productSpecificationCategoryUpdateProductSpecificationCategory**](ProductSpecificationCategoryApi.md#productSpecificationCategoryUpdateProductSpecificationCategory) | **PUT** /ProductSpecificationCategory/{productSpecificationCategorieId} | 


<a name="productSpecificationCategoryCreateProductSpecificationCategory"></a>
# **productSpecificationCategoryCreateProductSpecificationCategory**
> Object productSpecificationCategoryCreateProductSpecificationCategory(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationCategoryApi apiInstance = new ProductSpecificationCategoryApi();
ProductSpecificationCategoryDto dto = new ProductSpecificationCategoryDto(); // ProductSpecificationCategoryDto | 
try {
    Object result = apiInstance.productSpecificationCategoryCreateProductSpecificationCategory(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationCategoryApi#productSpecificationCategoryCreateProductSpecificationCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ProductSpecificationCategoryDto**](ProductSpecificationCategoryDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productSpecificationCategoryDeleteProductSpecificationCategory"></a>
# **productSpecificationCategoryDeleteProductSpecificationCategory**
> Object productSpecificationCategoryDeleteProductSpecificationCategory(productSpecificationCategorieId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationCategoryApi apiInstance = new ProductSpecificationCategoryApi();
Integer productSpecificationCategorieId = 56; // Integer | 
try {
    Object result = apiInstance.productSpecificationCategoryDeleteProductSpecificationCategory(productSpecificationCategorieId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationCategoryApi#productSpecificationCategoryDeleteProductSpecificationCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationCategorieId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productSpecificationCategoryExistsProductSpecificationCategory"></a>
# **productSpecificationCategoryExistsProductSpecificationCategory**
> Object productSpecificationCategoryExistsProductSpecificationCategory(productSpecificationCategorieId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationCategoryApi apiInstance = new ProductSpecificationCategoryApi();
Integer productSpecificationCategorieId = 56; // Integer | 
try {
    Object result = apiInstance.productSpecificationCategoryExistsProductSpecificationCategory(productSpecificationCategorieId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationCategoryApi#productSpecificationCategoryExistsProductSpecificationCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationCategorieId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productSpecificationCategoryGetProductSpecificationCategories"></a>
# **productSpecificationCategoryGetProductSpecificationCategories**
> Object productSpecificationCategoryGetProductSpecificationCategories(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationCategoryApi apiInstance = new ProductSpecificationCategoryApi();
GetFilteredProductSpecificationCategoryQuery query = new GetFilteredProductSpecificationCategoryQuery(); // GetFilteredProductSpecificationCategoryQuery | 
try {
    Object result = apiInstance.productSpecificationCategoryGetProductSpecificationCategories(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationCategoryApi#productSpecificationCategoryGetProductSpecificationCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductSpecificationCategoryQuery**](GetFilteredProductSpecificationCategoryQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productSpecificationCategoryGetProductSpecificationCategory"></a>
# **productSpecificationCategoryGetProductSpecificationCategory**
> Object productSpecificationCategoryGetProductSpecificationCategory(productSpecificationCategorieId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationCategoryApi apiInstance = new ProductSpecificationCategoryApi();
Integer productSpecificationCategorieId = 56; // Integer | 
try {
    Object result = apiInstance.productSpecificationCategoryGetProductSpecificationCategory(productSpecificationCategorieId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationCategoryApi#productSpecificationCategoryGetProductSpecificationCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationCategorieId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productSpecificationCategoryUpdateProductSpecificationCategory"></a>
# **productSpecificationCategoryUpdateProductSpecificationCategory**
> Object productSpecificationCategoryUpdateProductSpecificationCategory(productSpecificationCategorieId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationCategoryApi apiInstance = new ProductSpecificationCategoryApi();
Integer productSpecificationCategorieId = 56; // Integer | 
ProductSpecificationCategoryDto dto = new ProductSpecificationCategoryDto(); // ProductSpecificationCategoryDto | 
try {
    Object result = apiInstance.productSpecificationCategoryUpdateProductSpecificationCategory(productSpecificationCategorieId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationCategoryApi#productSpecificationCategoryUpdateProductSpecificationCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationCategorieId** | **Integer**|  |
 **dto** | [**ProductSpecificationCategoryDto**](ProductSpecificationCategoryDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

