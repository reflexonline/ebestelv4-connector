
# CreateSaleOrderDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deliveryDate** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**deliveryRoute** | **Integer** |  |  [optional]



