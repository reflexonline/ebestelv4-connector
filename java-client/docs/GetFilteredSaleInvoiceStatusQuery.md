
# GetFilteredSaleInvoiceStatusQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoiceStatusIdMinValue** | **Integer** |  |  [optional]
**invoiceStatusIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]
**isPaymentFulfilledMinValue** | **Integer** |  |  [optional]
**isPaymentFulfilledMaxValue** | **Integer** |  |  [optional]



