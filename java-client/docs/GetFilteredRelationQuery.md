
# GetFilteredRelationQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**searchNameMinValue** | **String** |  |  [optional]
**searchNameMaxValue** | **String** |  |  [optional]
**postalCodeMinValue** | **String** |  |  [optional]
**postalCodeMaxValue** | **String** |  |  [optional]
**cityMinValue** | **String** |  |  [optional]
**cityMaxValue** | **String** |  |  [optional]
**attentionDateMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**attentionDateMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**vertegenwoordigerMinValue** | **Integer** |  |  [optional]
**vertegenwoordigerMaxValue** | **Integer** |  |  [optional]



