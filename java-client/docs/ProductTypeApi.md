# ProductTypeApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productTypeCreateProductType**](ProductTypeApi.md#productTypeCreateProductType) | **POST** /ProductType | 
[**productTypeDeleteProductType**](ProductTypeApi.md#productTypeDeleteProductType) | **DELETE** /ProductType/{number} | 
[**productTypeExistsProductType**](ProductTypeApi.md#productTypeExistsProductType) | **GET** /ProductType/exists/{number} | 
[**productTypeGetProductType**](ProductTypeApi.md#productTypeGetProductType) | **GET** /ProductType/{number} | 
[**productTypeGetProductTypes**](ProductTypeApi.md#productTypeGetProductTypes) | **POST** /ProductType/search | 
[**productTypeUpdateProductType**](ProductTypeApi.md#productTypeUpdateProductType) | **PUT** /ProductType/{number} | 


<a name="productTypeCreateProductType"></a>
# **productTypeCreateProductType**
> Object productTypeCreateProductType(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductTypeApi apiInstance = new ProductTypeApi();
ProductTypeDto dto = new ProductTypeDto(); // ProductTypeDto | 
try {
    Object result = apiInstance.productTypeCreateProductType(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductTypeApi#productTypeCreateProductType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ProductTypeDto**](ProductTypeDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productTypeDeleteProductType"></a>
# **productTypeDeleteProductType**
> Object productTypeDeleteProductType(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductTypeApi apiInstance = new ProductTypeApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.productTypeDeleteProductType(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductTypeApi#productTypeDeleteProductType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productTypeExistsProductType"></a>
# **productTypeExistsProductType**
> Object productTypeExistsProductType(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductTypeApi apiInstance = new ProductTypeApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.productTypeExistsProductType(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductTypeApi#productTypeExistsProductType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productTypeGetProductType"></a>
# **productTypeGetProductType**
> Object productTypeGetProductType(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductTypeApi apiInstance = new ProductTypeApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.productTypeGetProductType(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductTypeApi#productTypeGetProductType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productTypeGetProductTypes"></a>
# **productTypeGetProductTypes**
> Object productTypeGetProductTypes(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductTypeApi apiInstance = new ProductTypeApi();
GetFilteredProductTypeQuery query = new GetFilteredProductTypeQuery(); // GetFilteredProductTypeQuery | 
try {
    Object result = apiInstance.productTypeGetProductTypes(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductTypeApi#productTypeGetProductTypes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductTypeQuery**](GetFilteredProductTypeQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productTypeUpdateProductType"></a>
# **productTypeUpdateProductType**
> Object productTypeUpdateProductType(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductTypeApi apiInstance = new ProductTypeApi();
Integer number = 56; // Integer | 
ProductTypeDto dto = new ProductTypeDto(); // ProductTypeDto | 
try {
    Object result = apiInstance.productTypeUpdateProductType(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductTypeApi#productTypeUpdateProductType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**ProductTypeDto**](ProductTypeDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

