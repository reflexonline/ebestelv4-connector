
# GetFilteredProductSpecificationCategoryQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productSpecificationCategorieIdMinValue** | **Integer** |  |  [optional]
**productSpecificationCategorieIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]
**productSpecificationMainCategorieIdMinValue** | **Integer** |  |  [optional]
**productSpecificationMainCategorieIdMaxValue** | **Integer** |  |  [optional]



