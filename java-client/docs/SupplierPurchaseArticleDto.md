
# SupplierPurchaseArticleDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierId** | **Integer** |  | 
**purchaseArticleId** | **Integer** |  | 
**supplierArticleId** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**labelNumber** | **Integer** |  |  [optional]
**eanCodeTradeUnit** | **String** |  |  [optional]
**minimalRequiredBestBeforeDays** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



