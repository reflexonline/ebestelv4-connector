
# DiscountGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**name** | **String** |  |  [optional]
**discountGroupType** | [**DiscountGroupTypeEnum**](#DiscountGroupTypeEnum) |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="DiscountGroupTypeEnum"></a>
## Enum: DiscountGroupTypeEnum
Name | Value
---- | -----
ONARTICLE | &quot;OnArticle&quot;
ONORDERAMOUNT | &quot;OnOrderAmount&quot;



