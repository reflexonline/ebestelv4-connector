# SubstituteGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**substituteGroupCreateSubstituteGroup**](SubstituteGroupApi.md#substituteGroupCreateSubstituteGroup) | **POST** /SubstituteGroup | 
[**substituteGroupDeleteSubstituteGroup**](SubstituteGroupApi.md#substituteGroupDeleteSubstituteGroup) | **DELETE** /SubstituteGroup/{substituteGroupId} | 
[**substituteGroupExistsSubstituteGroup**](SubstituteGroupApi.md#substituteGroupExistsSubstituteGroup) | **GET** /SubstituteGroup/exists/{substituteGroupId} | 
[**substituteGroupGetSubstituteGroup**](SubstituteGroupApi.md#substituteGroupGetSubstituteGroup) | **GET** /SubstituteGroup/{substituteGroupId} | 
[**substituteGroupGetSubstituteGroups**](SubstituteGroupApi.md#substituteGroupGetSubstituteGroups) | **POST** /SubstituteGroup/search | 
[**substituteGroupUpdateSubstituteGroup**](SubstituteGroupApi.md#substituteGroupUpdateSubstituteGroup) | **PUT** /SubstituteGroup/{substituteGroupId} | 


<a name="substituteGroupCreateSubstituteGroup"></a>
# **substituteGroupCreateSubstituteGroup**
> Object substituteGroupCreateSubstituteGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SubstituteGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SubstituteGroupApi apiInstance = new SubstituteGroupApi();
SubstituteGroupDto dto = new SubstituteGroupDto(); // SubstituteGroupDto | 
try {
    Object result = apiInstance.substituteGroupCreateSubstituteGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SubstituteGroupApi#substituteGroupCreateSubstituteGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SubstituteGroupDto**](SubstituteGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="substituteGroupDeleteSubstituteGroup"></a>
# **substituteGroupDeleteSubstituteGroup**
> Object substituteGroupDeleteSubstituteGroup(substituteGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SubstituteGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SubstituteGroupApi apiInstance = new SubstituteGroupApi();
Integer substituteGroupId = 56; // Integer | 
try {
    Object result = apiInstance.substituteGroupDeleteSubstituteGroup(substituteGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SubstituteGroupApi#substituteGroupDeleteSubstituteGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **substituteGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="substituteGroupExistsSubstituteGroup"></a>
# **substituteGroupExistsSubstituteGroup**
> Object substituteGroupExistsSubstituteGroup(substituteGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SubstituteGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SubstituteGroupApi apiInstance = new SubstituteGroupApi();
Integer substituteGroupId = 56; // Integer | 
try {
    Object result = apiInstance.substituteGroupExistsSubstituteGroup(substituteGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SubstituteGroupApi#substituteGroupExistsSubstituteGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **substituteGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="substituteGroupGetSubstituteGroup"></a>
# **substituteGroupGetSubstituteGroup**
> Object substituteGroupGetSubstituteGroup(substituteGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SubstituteGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SubstituteGroupApi apiInstance = new SubstituteGroupApi();
Integer substituteGroupId = 56; // Integer | 
try {
    Object result = apiInstance.substituteGroupGetSubstituteGroup(substituteGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SubstituteGroupApi#substituteGroupGetSubstituteGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **substituteGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="substituteGroupGetSubstituteGroups"></a>
# **substituteGroupGetSubstituteGroups**
> Object substituteGroupGetSubstituteGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SubstituteGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SubstituteGroupApi apiInstance = new SubstituteGroupApi();
GetFilteredSubstituteGroupQuery query = new GetFilteredSubstituteGroupQuery(); // GetFilteredSubstituteGroupQuery | 
try {
    Object result = apiInstance.substituteGroupGetSubstituteGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SubstituteGroupApi#substituteGroupGetSubstituteGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSubstituteGroupQuery**](GetFilteredSubstituteGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="substituteGroupUpdateSubstituteGroup"></a>
# **substituteGroupUpdateSubstituteGroup**
> Object substituteGroupUpdateSubstituteGroup(substituteGroupId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SubstituteGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SubstituteGroupApi apiInstance = new SubstituteGroupApi();
Integer substituteGroupId = 56; // Integer | 
SubstituteGroupDto dto = new SubstituteGroupDto(); // SubstituteGroupDto | 
try {
    Object result = apiInstance.substituteGroupUpdateSubstituteGroup(substituteGroupId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SubstituteGroupApi#substituteGroupUpdateSubstituteGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **substituteGroupId** | **Integer**|  |
 **dto** | [**SubstituteGroupDto**](SubstituteGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

