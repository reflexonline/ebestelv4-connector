
# RecipeCostDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipeCostId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**recipeCostType** | [**RecipeCostTypeEnum**](#RecipeCostTypeEnum) |  |  [optional]
**bedragOld** | **String** |  |  [optional]
**ledgerNumber** | **Integer** |  |  [optional]
**costNumber** | **Integer** |  |  [optional]
**amount** | **Double** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="RecipeCostTypeEnum"></a>
## Enum: RecipeCostTypeEnum
Name | Value
---- | -----
FIXED | &quot;Fixed&quot;
VARIABLE | &quot;Variable&quot;
PERCENTAGE | &quot;Percentage&quot;



