# SaleOrderAddressApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleOrderAddressExistsSaleOrderAddress**](SaleOrderAddressApi.md#saleOrderAddressExistsSaleOrderAddress) | **GET** /SaleOrderAddress/exists/{orderNumber} | 
[**saleOrderAddressGetSaleOrderAddress**](SaleOrderAddressApi.md#saleOrderAddressGetSaleOrderAddress) | **GET** /SaleOrderAddress/{orderNumber} | 
[**saleOrderAddressGetSaleOrderAddresses**](SaleOrderAddressApi.md#saleOrderAddressGetSaleOrderAddresses) | **POST** /SaleOrderAddress/search | 


<a name="saleOrderAddressExistsSaleOrderAddress"></a>
# **saleOrderAddressExistsSaleOrderAddress**
> Object saleOrderAddressExistsSaleOrderAddress(orderNumber)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderAddressApi apiInstance = new SaleOrderAddressApi();
Integer orderNumber = 56; // Integer | 
try {
    Object result = apiInstance.saleOrderAddressExistsSaleOrderAddress(orderNumber);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderAddressApi#saleOrderAddressExistsSaleOrderAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleOrderAddressGetSaleOrderAddress"></a>
# **saleOrderAddressGetSaleOrderAddress**
> Object saleOrderAddressGetSaleOrderAddress(orderNumber)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderAddressApi apiInstance = new SaleOrderAddressApi();
Integer orderNumber = 56; // Integer | 
try {
    Object result = apiInstance.saleOrderAddressGetSaleOrderAddress(orderNumber);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderAddressApi#saleOrderAddressGetSaleOrderAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleOrderAddressGetSaleOrderAddresses"></a>
# **saleOrderAddressGetSaleOrderAddresses**
> Object saleOrderAddressGetSaleOrderAddresses(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderAddressApi apiInstance = new SaleOrderAddressApi();
GetFilteredSaleOrderAddressQuery query = new GetFilteredSaleOrderAddressQuery(); // GetFilteredSaleOrderAddressQuery | 
try {
    Object result = apiInstance.saleOrderAddressGetSaleOrderAddresses(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderAddressApi#saleOrderAddressGetSaleOrderAddresses");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSaleOrderAddressQuery**](GetFilteredSaleOrderAddressQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

