
# GetFilteredCustomerSaleArticleQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userDefinedCode1MinValue** | **String** |  |  [optional]
**userDefinedCode1MaxValue** | **String** |  |  [optional]
**customerIdMinValue** | **Integer** |  |  [optional]
**customerIdMaxValue** | **Integer** |  |  [optional]
**articleIdMinValue** | **Integer** |  |  [optional]
**articleIdMaxValue** | **Integer** |  |  [optional]
**owhToBranchMinValue** | **Integer** |  |  [optional]
**owhToBranchMaxValue** | **Integer** |  |  [optional]
**userDefinedCode2MinValue** | **String** |  |  [optional]
**userDefinedCode2MaxValue** | **String** |  |  [optional]
**packingArticleId1MinValue** | **Integer** |  |  [optional]
**packingArticleId1MaxValue** | **Integer** |  |  [optional]
**packingArticleId2MinValue** | **Integer** |  |  [optional]
**packingArticleId2MaxValue** | **Integer** |  |  [optional]



