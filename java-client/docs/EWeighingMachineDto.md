
# EWeighingMachineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**machineId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



