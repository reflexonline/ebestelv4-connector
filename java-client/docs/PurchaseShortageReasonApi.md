# PurchaseShortageReasonApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purchaseShortageReasonCreatePurchaseShortageReason**](PurchaseShortageReasonApi.md#purchaseShortageReasonCreatePurchaseShortageReason) | **POST** /PurchaseShortageReason | 
[**purchaseShortageReasonDeletePurchaseShortageReason**](PurchaseShortageReasonApi.md#purchaseShortageReasonDeletePurchaseShortageReason) | **DELETE** /PurchaseShortageReason/{shortageReasonId} | 
[**purchaseShortageReasonExistsPurchaseShortageReason**](PurchaseShortageReasonApi.md#purchaseShortageReasonExistsPurchaseShortageReason) | **GET** /PurchaseShortageReason/exists/{shortageReasonId} | 
[**purchaseShortageReasonGetPurchaseShortageReason**](PurchaseShortageReasonApi.md#purchaseShortageReasonGetPurchaseShortageReason) | **GET** /PurchaseShortageReason/{shortageReasonId} | 
[**purchaseShortageReasonGetPurchaseShortageReasons**](PurchaseShortageReasonApi.md#purchaseShortageReasonGetPurchaseShortageReasons) | **POST** /PurchaseShortageReason/search | 
[**purchaseShortageReasonUpdatePurchaseShortageReason**](PurchaseShortageReasonApi.md#purchaseShortageReasonUpdatePurchaseShortageReason) | **PUT** /PurchaseShortageReason/{shortageReasonId} | 


<a name="purchaseShortageReasonCreatePurchaseShortageReason"></a>
# **purchaseShortageReasonCreatePurchaseShortageReason**
> Object purchaseShortageReasonCreatePurchaseShortageReason(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseShortageReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseShortageReasonApi apiInstance = new PurchaseShortageReasonApi();
PurchaseShortageReasonDto dto = new PurchaseShortageReasonDto(); // PurchaseShortageReasonDto | 
try {
    Object result = apiInstance.purchaseShortageReasonCreatePurchaseShortageReason(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseShortageReasonApi#purchaseShortageReasonCreatePurchaseShortageReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PurchaseShortageReasonDto**](PurchaseShortageReasonDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseShortageReasonDeletePurchaseShortageReason"></a>
# **purchaseShortageReasonDeletePurchaseShortageReason**
> Object purchaseShortageReasonDeletePurchaseShortageReason(shortageReasonId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseShortageReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseShortageReasonApi apiInstance = new PurchaseShortageReasonApi();
Integer shortageReasonId = 56; // Integer | 
try {
    Object result = apiInstance.purchaseShortageReasonDeletePurchaseShortageReason(shortageReasonId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseShortageReasonApi#purchaseShortageReasonDeletePurchaseShortageReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shortageReasonId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseShortageReasonExistsPurchaseShortageReason"></a>
# **purchaseShortageReasonExistsPurchaseShortageReason**
> Object purchaseShortageReasonExistsPurchaseShortageReason(shortageReasonId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseShortageReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseShortageReasonApi apiInstance = new PurchaseShortageReasonApi();
Integer shortageReasonId = 56; // Integer | 
try {
    Object result = apiInstance.purchaseShortageReasonExistsPurchaseShortageReason(shortageReasonId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseShortageReasonApi#purchaseShortageReasonExistsPurchaseShortageReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shortageReasonId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseShortageReasonGetPurchaseShortageReason"></a>
# **purchaseShortageReasonGetPurchaseShortageReason**
> Object purchaseShortageReasonGetPurchaseShortageReason(shortageReasonId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseShortageReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseShortageReasonApi apiInstance = new PurchaseShortageReasonApi();
Integer shortageReasonId = 56; // Integer | 
try {
    Object result = apiInstance.purchaseShortageReasonGetPurchaseShortageReason(shortageReasonId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseShortageReasonApi#purchaseShortageReasonGetPurchaseShortageReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shortageReasonId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseShortageReasonGetPurchaseShortageReasons"></a>
# **purchaseShortageReasonGetPurchaseShortageReasons**
> Object purchaseShortageReasonGetPurchaseShortageReasons(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseShortageReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseShortageReasonApi apiInstance = new PurchaseShortageReasonApi();
GetFilteredPurchaseShortageReasonQuery query = new GetFilteredPurchaseShortageReasonQuery(); // GetFilteredPurchaseShortageReasonQuery | 
try {
    Object result = apiInstance.purchaseShortageReasonGetPurchaseShortageReasons(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseShortageReasonApi#purchaseShortageReasonGetPurchaseShortageReasons");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPurchaseShortageReasonQuery**](GetFilteredPurchaseShortageReasonQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseShortageReasonUpdatePurchaseShortageReason"></a>
# **purchaseShortageReasonUpdatePurchaseShortageReason**
> Object purchaseShortageReasonUpdatePurchaseShortageReason(shortageReasonId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseShortageReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseShortageReasonApi apiInstance = new PurchaseShortageReasonApi();
Integer shortageReasonId = 56; // Integer | 
PurchaseShortageReasonDto dto = new PurchaseShortageReasonDto(); // PurchaseShortageReasonDto | 
try {
    Object result = apiInstance.purchaseShortageReasonUpdatePurchaseShortageReason(shortageReasonId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseShortageReasonApi#purchaseShortageReasonUpdatePurchaseShortageReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shortageReasonId** | **Integer**|  |
 **dto** | [**PurchaseShortageReasonDto**](PurchaseShortageReasonDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

