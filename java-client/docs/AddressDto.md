
# AddressDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addressId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**addressLine1** | **String** |  |  [optional]
**addressLine2** | **String** |  |  [optional]
**postalCode** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**country** | **String** |  |  [optional]
**isoCountryCode** | **String** |  |  [optional]
**phoneNumber** | **String** |  |  [optional]
**faxNumber** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**text1** | **String** |  |  [optional]
**text2** | **String** |  |  [optional]
**text3** | **String** |  |  [optional]
**addressType** | [**AddressTypeEnum**](#AddressTypeEnum) |  |  [optional]
**eanOrderAddressCode** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="AddressTypeEnum"></a>
## Enum: AddressTypeEnum
Name | Value
---- | -----
ALTERNATIVEADDRESS | &quot;AlternativeAddress&quot;
ORDERTRANSPORTER | &quot;OrderTransporter&quot;
LOADADDRESS | &quot;LoadAddress&quot;



