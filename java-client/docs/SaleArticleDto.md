
# SaleArticleDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productSpecificationUrl** | **String** |  |  [optional]
**saleArticleTranslation** | [**List&lt;LineSaleArticleDescriptionTranslationDto&gt;**](LineSaleArticleDescriptionTranslationDto.md) |  |  [optional]
**tag** | [**List&lt;LineTagNameDto&gt;**](LineTagNameDto.md) |  |  [optional]
**allergens** | [**List&lt;SaleArticleAllergenDto&gt;**](SaleArticleAllergenDto.md) |  |  [optional]
**nutritionals** | [**List&lt;SaleArticleNutritionalDto&gt;**](SaleArticleNutritionalDto.md) |  |  [optional]
**number** | **Integer** |  | 
**shortName** | **String** |  |  [optional]
**longName** | **String** |  |  [optional]
**inkpOld** | **Float** |  |  [optional]
**verkpOld** | **Float** |  |  [optional]
**unitType** | [**UnitTypeEnum**](#UnitTypeEnum) |  |  [optional]
**vatCode** | [**VatCodeEnum**](#VatCodeEnum) |  |  [optional]
**tareWeight** | **Float** |  |  [optional]
**weighingFactor** | **Double** |  |  [optional]
**portionWeight** | **Double** |  |  [optional]
**processingLoss** | **Float** |  |  [optional]
**orderGroup1** | **Integer** |  |  [optional]
**bestBeforeDateFresh** | **Integer** |  |  [optional]
**keepStock** | **Integer** |  |  [optional]
**saleArticleGroup** | **Integer** |  |  [optional]
**pickLocation** | **String** |  |  [optional]
**backorderPercentage** | **Integer** |  |  [optional]
**packedPerUnit** | **Float** |  |  [optional]
**packedPerType** | [**PackedPerTypeEnum**](#PackedPerTypeEnum) |  |  [optional]
**ingredient1** | **String** |  |  [optional]
**ingredient2** | **String** |  |  [optional]
**ingredient3** | **String** |  |  [optional]
**ingredient4** | **String** |  |  [optional]
**ingredient5** | **String** |  |  [optional]
**ingredient6** | **String** |  |  [optional]
**ingredient7** | **String** |  |  [optional]
**ingredient8** | **String** |  |  [optional]
**packingType** | **String** |  |  [optional]
**cbsGoodsCode** | **String** |  |  [optional]
**storageTerms** | **String** |  |  [optional]
**userDefinableField1** | **String** |  |  [optional]
**userDefinableField2** | **String** |  |  [optional]
**userDefinableField3** | **String** |  |  [optional]
**userDefinableField4** | **String** |  |  [optional]
**userDefinableField5** | **String** |  |  [optional]
**purchaseArticle** | **Integer** |  |  [optional]
**userPurchaseFactor** | **Double** |  |  [optional]
**stockAutoPurchaseArticle** | **Integer** |  |  [optional]
**packingArticle1** | **Integer** |  |  [optional]
**isEcommerceArticle** | **String** |  |  [optional]
**orderGroup2** | **Integer** |  |  [optional]
**labelNumber** | **Integer** |  |  [optional]
**blockCustomerDiscount** | **String** |  |  [optional]
**ingredient9** | **String** |  |  [optional]
**ingredient10** | **String** |  |  [optional]
**ingredient11** | **String** |  |  [optional]
**ingredient12** | **String** |  |  [optional]
**fixedStockLocation** | **Integer** |  |  [optional]
**bestBeforeDateFrozen** | **Integer** |  |  [optional]
**adviesVerkoopPrijsOu** | **Float** |  |  [optional]
**tipUpUnit1** | **Float** |  |  [optional]
**tipUpUnit2** | **Float** |  |  [optional]
**tipUpUnit3** | **Float** |  |  [optional]
**tipUpSurcharge1** | **Float** |  |  [optional]
**tipUpSurcharge2** | **Float** |  |  [optional]
**tipUpSurcharge3** | **Float** |  |  [optional]
**substituteArticle** | **Integer** |  |  [optional]
**acknowledgement** | **String** |  |  [optional]
**eanRetailUnitCode** | **String** |  |  [optional]
**orderGroup3** | **Integer** |  |  [optional]
**storageState** | [**StorageStateEnum**](#StorageStateEnum) |  |  [optional]
**orderGroup4** | **Integer** |  |  [optional]
**orderGroup5** | **Integer** |  |  [optional]
**calculationType** | **Integer** |  |  [optional]
**priceOnPackingSlip** | **Integer** |  |  [optional]
**isBlocked** | **Boolean** |  |  [optional]
**defaultOrderAtAdministrationBranch** | **Integer** |  |  [optional]
**owhArticleIdBranch** | **Integer** |  |  [optional]
**brand** | **String** |  |  [optional]
**wrapping** | **String** |  |  [optional]
**photoPath** | **String** |  |  [optional]
**packingArticle2** | **Integer** |  |  [optional]
**blockedFrom** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**blockedUntil** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**fixedSurcharge** | **Boolean** |  |  [optional]
**surchargeFactor** | **Double** |  |  [optional]
**excludeFromPriceList** | **Boolean** |  |  [optional]
**allowedPriceDeviation** | **Double** |  |  [optional]
**occMethod** | **Integer** |  |  [optional]
**bonusParticipation** | [**BonusParticipationEnum**](#BonusParticipationEnum) |  |  [optional]
**subGroup** | **Integer** |  |  [optional]
**deliveryTimeInversco** | **Integer** |  |  [optional]
**quantityInPacking** | **Integer** |  |  [optional]
**totalPackedWeight** | **Integer** |  |  [optional]
**productionLabelBasedOnPortion** | **Boolean** |  |  [optional]
**vastFacPrijsOld** | **Float** |  |  [optional]
**consumerGroup** | **Integer** |  |  [optional]
**substituteGroup** | **Integer** |  |  [optional]
**wssAskPackingQuantity** | **String** |  |  [optional]
**gflTracingCode** | **Integer** |  |  [optional]
**guaranteedBestBeforeDays** | **Integer** |  |  [optional]
**vertebraType** | **Integer** |  |  [optional]
**manualIngredient1** | **Integer** |  |  [optional]
**manualIngredient2** | **Integer** |  |  [optional]
**manualIngredient3** | **Integer** |  |  [optional]
**manualIngredient4** | **Integer** |  |  [optional]
**manualIngredient5** | **Integer** |  |  [optional]
**manualIngredient6** | **Integer** |  |  [optional]
**manualIngredient7** | **Integer** |  |  [optional]
**manualIngredient8** | **Integer** |  |  [optional]
**manualIngredient9** | **Integer** |  |  [optional]
**manualIngredient10** | **Integer** |  |  [optional]
**manualIngredient11** | **Integer** |  |  [optional]
**manualIngredient12** | **Integer** |  |  [optional]
**gmoState** | **Integer** |  |  [optional]
**gmoManual** | **Boolean** |  |  [optional]
**displayENumber** | **Boolean** |  |  [optional]
**displayAllergens** | **Integer** |  |  [optional]
**productSpecificationNumber** | **Integer** |  |  [optional]
**productSpecificationNumberRevision** | **Integer** |  |  [optional]
**productSpecificationDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**lastProductSpecNumber** | **Integer** |  |  [optional]
**lastProductSpecNumberRevision** | **Integer** |  |  [optional]
**lastProductSpecDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**registrationWarning** | **Integer** |  |  [optional]
**isOwhArticle** | **Boolean** |  |  [optional]
**usePortionAsCbsPortion** | **Integer** |  |  [optional]
**cbsPortionWeight** | **Float** |  |  [optional]
**cbsSupplementaryUnitFactor** | **Integer** |  |  [optional]
**occNumberOfLabels** | **Integer** |  |  [optional]
**maxOrderQuantity** | **Integer** |  |  [optional]
**owhDeliveryType** | **Integer** |  |  [optional]
**eWeighingPortion** | **Double** |  |  [optional]
**usePortionAsEWeighingPort** | **Integer** |  |  [optional]
**documentPath** | **String** |  |  [optional]
**crateLabelNumber** | **Integer** |  |  [optional]
**salePriceBasedOnConsumer** | **Boolean** |  |  [optional]
**registrationMethod** | **Integer** |  |  [optional]
**minimumWeight** | **Double** |  |  [optional]
**maximumWeight** | **Double** |  |  [optional]
**createBackorderOnExcessiveStock** | **Boolean** |  |  [optional]
**createBackorderOnStockShortage** | **Boolean** |  |  [optional]
**backorderType** | [**BackorderTypeEnum**](#BackorderTypeEnum) |  |  [optional]
**boxQuantity** | **Integer** |  |  [optional]
**boxesPerLayer** | **Integer** |  |  [optional]
**layersPerPallet** | **Integer** |  |  [optional]
**useGraiCode** | **Integer** |  |  [optional]
**lengthOfGraiCode** | **Integer** |  |  [optional]
**defProdMultiBranchId** | **Integer** |  |  [optional]
**eanTradeUnitCode** | **String** |  |  [optional]
**wssCheckMaximumRegistration** | **Boolean** |  |  [optional]
**trackPackingBalance** | **Boolean** |  |  [optional]
**checkMinMaxPortionWeightOnOrderEntry** | **Boolean** |  |  [optional]
**ssccRegisterMethod** | **Integer** |  |  [optional]
**planningUnitType** | [**PlanningUnitTypeEnum**](#PlanningUnitTypeEnum) |  |  [optional]
**usePlanningUnitType** | **Boolean** |  |  [optional]
**palletLabelNumber** | **Integer** |  |  [optional]
**countryOfOrigin** | **String** |  |  [optional]
**analysisPurchasePriceType** | **Integer** |  |  [optional]
**useAnalysisPurchasePriceBatch** | **Boolean** |  |  [optional]
**priceLookUpCode** | **Integer** |  |  [optional]
**length** | **Double** |  |  [optional]
**width** | **Double** |  |  [optional]
**height** | **Double** |  |  [optional]
**volume** | **Double** |  |  [optional]
**ediExportType** | **Integer** |  |  [optional]
**ediExportCrateNumber** | **Integer** |  |  [optional]
**useFTrace** | **Boolean** |  |  [optional]
**kantelEenheid4** | **Float** |  |  [optional]
**kantelEenheid5** | **Float** |  |  [optional]
**kantelOpslag4** | **Float** |  |  [optional]
**kantelOpslag5** | **Float** |  |  [optional]
**kantelOpslagMethode** | **Integer** |  |  [optional]
**dontShowENumber** | **Integer** |  |  [optional]
**verpakkingPDMCode** | **String** |  |  [optional]
**uploadProductData** | **Integer** |  |  [optional]
**showCountryOfOrigin** | **Integer** |  |  [optional]
**weergaveExtraAlbas** | **Boolean** |  |  [optional]
**bestBeforeDateDeterminationMethod** | [**BestBeforeDateDeterminationMethodEnum**](#BestBeforeDateDeterminationMethodEnum) |  |  [optional]
**bestBeforeDateCheckUpMethod** | [**BestBeforeDateCheckUpMethodEnum**](#BestBeforeDateCheckUpMethodEnum) |  |  [optional]
**purchasePrice** | **Double** |  |  [optional]
**salePrice** | **Double** |  |  [optional]
**consumerSalePrice** | **Double** |  |  [optional]
**fixedInvoicePrice** | **Double** |  |  [optional]
**bidfoodArtCategorie** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="UnitTypeEnum"></a>
## Enum: UnitTypeEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
PIECES | &quot;Pieces&quot;
WEIGHT | &quot;Weight&quot;
WEIGHTFIXEDPORTION | &quot;WeightFixedPortion&quot;


<a name="VatCodeEnum"></a>
## Enum: VatCodeEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
LOW | &quot;Low&quot;
HIGH | &quot;High&quot;
SERVICE | &quot;Service&quot;
AGRICULTURE | &quot;Agriculture&quot;
DAIRY | &quot;Dairy&quot;
PACKING | &quot;Packing&quot;


<a name="PackedPerTypeEnum"></a>
## Enum: PackedPerTypeEnum
Name | Value
---- | -----
PACKEDPERWEIGHTUNIT | &quot;PackedPerWeightUnit&quot;
PACKEDPERPIECE | &quot;PackedPerPiece&quot;


<a name="StorageStateEnum"></a>
## Enum: StorageStateEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
FRESH | &quot;Fresh&quot;
FROZEN | &quot;Frozen&quot;
NONPERISHABLE | &quot;NonPerishable&quot;
NOTAPPLICABLE | &quot;NotApplicable&quot;


<a name="BonusParticipationEnum"></a>
## Enum: BonusParticipationEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
BONUSANDLADDER | &quot;BonusAndLadder&quot;
LADDER | &quot;Ladder&quot;
BONUS | &quot;Bonus&quot;


<a name="BackorderTypeEnum"></a>
## Enum: BackorderTypeEnum
Name | Value
---- | -----
WEIGHT | &quot;Weight&quot;
PIECES | &quot;Pieces&quot;


<a name="PlanningUnitTypeEnum"></a>
## Enum: PlanningUnitTypeEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
PIECES | &quot;Pieces&quot;
WEIGHT | &quot;Weight&quot;
WEIGHTFIXEDPORTION | &quot;WeightFixedPortion&quot;


<a name="BestBeforeDateDeterminationMethodEnum"></a>
## Enum: BestBeforeDateDeterminationMethodEnum
Name | Value
---- | -----
DEFAULT | &quot;Default&quot;
NONE | &quot;None&quot;
DATEREGISTRATION | &quot;DateRegistration&quot;
DATELOADING | &quot;DateLoading&quot;
DATEDELIVERY | &quot;DateDelivery&quot;
REGISTEREDBATCH | &quot;RegisteredBatch&quot;


<a name="BestBeforeDateCheckUpMethodEnum"></a>
## Enum: BestBeforeDateCheckUpMethodEnum
Name | Value
---- | -----
DEFAULT | &quot;Default&quot;
NONE | &quot;None&quot;
ASKUSER | &quot;AskUser&quot;
WARRANTYDATE | &quot;WarrantyDate&quot;
ASKFORDATEWARRANTYINCLUDED | &quot;AskForDateWarrantyIncluded&quot;
ASKFORDATEHOLDPERARTICLE | &quot;AskForDateHoldPerArticle&quot;



