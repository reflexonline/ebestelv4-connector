# PurchaseOrderGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purchaseOrderGroupCreatePurchaseOrderGroup**](PurchaseOrderGroupApi.md#purchaseOrderGroupCreatePurchaseOrderGroup) | **POST** /PurchaseOrderGroup | 
[**purchaseOrderGroupDeletePurchaseOrderGroup**](PurchaseOrderGroupApi.md#purchaseOrderGroupDeletePurchaseOrderGroup) | **DELETE** /PurchaseOrderGroup/{number} | 
[**purchaseOrderGroupExistsPurchaseOrderGroup**](PurchaseOrderGroupApi.md#purchaseOrderGroupExistsPurchaseOrderGroup) | **GET** /PurchaseOrderGroup/exists/{number} | 
[**purchaseOrderGroupGetPurchaseOrderGroup**](PurchaseOrderGroupApi.md#purchaseOrderGroupGetPurchaseOrderGroup) | **GET** /PurchaseOrderGroup/{number} | 
[**purchaseOrderGroupGetPurchaseOrderGroups**](PurchaseOrderGroupApi.md#purchaseOrderGroupGetPurchaseOrderGroups) | **POST** /PurchaseOrderGroup/search | 
[**purchaseOrderGroupUpdatePurchaseOrderGroup**](PurchaseOrderGroupApi.md#purchaseOrderGroupUpdatePurchaseOrderGroup) | **PUT** /PurchaseOrderGroup/{number} | 


<a name="purchaseOrderGroupCreatePurchaseOrderGroup"></a>
# **purchaseOrderGroupCreatePurchaseOrderGroup**
> Object purchaseOrderGroupCreatePurchaseOrderGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderGroupApi apiInstance = new PurchaseOrderGroupApi();
PurchaseOrderGroupDto dto = new PurchaseOrderGroupDto(); // PurchaseOrderGroupDto | 
try {
    Object result = apiInstance.purchaseOrderGroupCreatePurchaseOrderGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderGroupApi#purchaseOrderGroupCreatePurchaseOrderGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PurchaseOrderGroupDto**](PurchaseOrderGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseOrderGroupDeletePurchaseOrderGroup"></a>
# **purchaseOrderGroupDeletePurchaseOrderGroup**
> Object purchaseOrderGroupDeletePurchaseOrderGroup(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderGroupApi apiInstance = new PurchaseOrderGroupApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.purchaseOrderGroupDeletePurchaseOrderGroup(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderGroupApi#purchaseOrderGroupDeletePurchaseOrderGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseOrderGroupExistsPurchaseOrderGroup"></a>
# **purchaseOrderGroupExistsPurchaseOrderGroup**
> Object purchaseOrderGroupExistsPurchaseOrderGroup(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderGroupApi apiInstance = new PurchaseOrderGroupApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.purchaseOrderGroupExistsPurchaseOrderGroup(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderGroupApi#purchaseOrderGroupExistsPurchaseOrderGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseOrderGroupGetPurchaseOrderGroup"></a>
# **purchaseOrderGroupGetPurchaseOrderGroup**
> Object purchaseOrderGroupGetPurchaseOrderGroup(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderGroupApi apiInstance = new PurchaseOrderGroupApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.purchaseOrderGroupGetPurchaseOrderGroup(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderGroupApi#purchaseOrderGroupGetPurchaseOrderGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseOrderGroupGetPurchaseOrderGroups"></a>
# **purchaseOrderGroupGetPurchaseOrderGroups**
> Object purchaseOrderGroupGetPurchaseOrderGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderGroupApi apiInstance = new PurchaseOrderGroupApi();
GetFilteredPurchaseOrderGroupQuery query = new GetFilteredPurchaseOrderGroupQuery(); // GetFilteredPurchaseOrderGroupQuery | 
try {
    Object result = apiInstance.purchaseOrderGroupGetPurchaseOrderGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderGroupApi#purchaseOrderGroupGetPurchaseOrderGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPurchaseOrderGroupQuery**](GetFilteredPurchaseOrderGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseOrderGroupUpdatePurchaseOrderGroup"></a>
# **purchaseOrderGroupUpdatePurchaseOrderGroup**
> Object purchaseOrderGroupUpdatePurchaseOrderGroup(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderGroupApi apiInstance = new PurchaseOrderGroupApi();
Integer number = 56; // Integer | 
PurchaseOrderGroupDto dto = new PurchaseOrderGroupDto(); // PurchaseOrderGroupDto | 
try {
    Object result = apiInstance.purchaseOrderGroupUpdatePurchaseOrderGroup(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderGroupApi#purchaseOrderGroupUpdatePurchaseOrderGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**PurchaseOrderGroupDto**](PurchaseOrderGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

