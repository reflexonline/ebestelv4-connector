# BaseMaterialApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**baseMaterialCreateBaseMaterial**](BaseMaterialApi.md#baseMaterialCreateBaseMaterial) | **POST** /BaseMaterial | 
[**baseMaterialDeleteBaseMaterial**](BaseMaterialApi.md#baseMaterialDeleteBaseMaterial) | **DELETE** /BaseMaterial/{number} | 
[**baseMaterialExistsBaseMaterial**](BaseMaterialApi.md#baseMaterialExistsBaseMaterial) | **GET** /BaseMaterial/exists/{number} | 
[**baseMaterialGetBaseMaterial**](BaseMaterialApi.md#baseMaterialGetBaseMaterial) | **GET** /BaseMaterial/{number} | 
[**baseMaterialGetBaseMaterials**](BaseMaterialApi.md#baseMaterialGetBaseMaterials) | **POST** /BaseMaterial/search | 
[**baseMaterialUpdateBaseMaterial**](BaseMaterialApi.md#baseMaterialUpdateBaseMaterial) | **PUT** /BaseMaterial/{number} | 


<a name="baseMaterialCreateBaseMaterial"></a>
# **baseMaterialCreateBaseMaterial**
> Object baseMaterialCreateBaseMaterial(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BaseMaterialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

BaseMaterialApi apiInstance = new BaseMaterialApi();
BaseMaterialDto dto = new BaseMaterialDto(); // BaseMaterialDto | 
try {
    Object result = apiInstance.baseMaterialCreateBaseMaterial(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BaseMaterialApi#baseMaterialCreateBaseMaterial");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**BaseMaterialDto**](BaseMaterialDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="baseMaterialDeleteBaseMaterial"></a>
# **baseMaterialDeleteBaseMaterial**
> Object baseMaterialDeleteBaseMaterial(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BaseMaterialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

BaseMaterialApi apiInstance = new BaseMaterialApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.baseMaterialDeleteBaseMaterial(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BaseMaterialApi#baseMaterialDeleteBaseMaterial");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="baseMaterialExistsBaseMaterial"></a>
# **baseMaterialExistsBaseMaterial**
> Object baseMaterialExistsBaseMaterial(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BaseMaterialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

BaseMaterialApi apiInstance = new BaseMaterialApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.baseMaterialExistsBaseMaterial(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BaseMaterialApi#baseMaterialExistsBaseMaterial");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="baseMaterialGetBaseMaterial"></a>
# **baseMaterialGetBaseMaterial**
> Object baseMaterialGetBaseMaterial(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BaseMaterialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

BaseMaterialApi apiInstance = new BaseMaterialApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.baseMaterialGetBaseMaterial(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BaseMaterialApi#baseMaterialGetBaseMaterial");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="baseMaterialGetBaseMaterials"></a>
# **baseMaterialGetBaseMaterials**
> Object baseMaterialGetBaseMaterials(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BaseMaterialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

BaseMaterialApi apiInstance = new BaseMaterialApi();
GetFilteredBaseMaterialQuery query = new GetFilteredBaseMaterialQuery(); // GetFilteredBaseMaterialQuery | 
try {
    Object result = apiInstance.baseMaterialGetBaseMaterials(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BaseMaterialApi#baseMaterialGetBaseMaterials");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredBaseMaterialQuery**](GetFilteredBaseMaterialQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="baseMaterialUpdateBaseMaterial"></a>
# **baseMaterialUpdateBaseMaterial**
> Object baseMaterialUpdateBaseMaterial(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BaseMaterialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

BaseMaterialApi apiInstance = new BaseMaterialApi();
Integer number = 56; // Integer | 
BaseMaterialDto dto = new BaseMaterialDto(); // BaseMaterialDto | 
try {
    Object result = apiInstance.baseMaterialUpdateBaseMaterial(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BaseMaterialApi#baseMaterialUpdateBaseMaterial");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**BaseMaterialDto**](BaseMaterialDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

