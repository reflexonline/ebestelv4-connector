
# ProductSpecificationLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productSpecificationCategorieId** | **Integer** |  | 
**productSpecificationLineId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**productSpecificationUnit** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



