# RelationDeliveryAddressApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**relationDeliveryAddressExistsRelationDeliveryAddress**](RelationDeliveryAddressApi.md#relationDeliveryAddressExistsRelationDeliveryAddress) | **GET** /RelationDeliveryAddress/exists/{number} | 
[**relationDeliveryAddressGetRelationDeliveryAddress**](RelationDeliveryAddressApi.md#relationDeliveryAddressGetRelationDeliveryAddress) | **GET** /RelationDeliveryAddress/{number} | 
[**relationDeliveryAddressGetRelationDeliveryAddresses**](RelationDeliveryAddressApi.md#relationDeliveryAddressGetRelationDeliveryAddresses) | **POST** /RelationDeliveryAddress/search | 


<a name="relationDeliveryAddressExistsRelationDeliveryAddress"></a>
# **relationDeliveryAddressExistsRelationDeliveryAddress**
> Object relationDeliveryAddressExistsRelationDeliveryAddress(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RelationDeliveryAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RelationDeliveryAddressApi apiInstance = new RelationDeliveryAddressApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.relationDeliveryAddressExistsRelationDeliveryAddress(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationDeliveryAddressApi#relationDeliveryAddressExistsRelationDeliveryAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="relationDeliveryAddressGetRelationDeliveryAddress"></a>
# **relationDeliveryAddressGetRelationDeliveryAddress**
> Object relationDeliveryAddressGetRelationDeliveryAddress(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RelationDeliveryAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RelationDeliveryAddressApi apiInstance = new RelationDeliveryAddressApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.relationDeliveryAddressGetRelationDeliveryAddress(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationDeliveryAddressApi#relationDeliveryAddressGetRelationDeliveryAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="relationDeliveryAddressGetRelationDeliveryAddresses"></a>
# **relationDeliveryAddressGetRelationDeliveryAddresses**
> Object relationDeliveryAddressGetRelationDeliveryAddresses(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RelationDeliveryAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RelationDeliveryAddressApi apiInstance = new RelationDeliveryAddressApi();
GetFilteredRelationDeliveryAddressQuery query = new GetFilteredRelationDeliveryAddressQuery(); // GetFilteredRelationDeliveryAddressQuery | 
try {
    Object result = apiInstance.relationDeliveryAddressGetRelationDeliveryAddresses(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationDeliveryAddressApi#relationDeliveryAddressGetRelationDeliveryAddresses");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredRelationDeliveryAddressQuery**](GetFilteredRelationDeliveryAddressQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

