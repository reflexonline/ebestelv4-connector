# BatchSeperatorApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**batchSeperatorCreateBatchSeperator**](BatchSeperatorApi.md#batchSeperatorCreateBatchSeperator) | **POST** /BatchSeperator | 
[**batchSeperatorDeleteBatchSeperator**](BatchSeperatorApi.md#batchSeperatorDeleteBatchSeperator) | **DELETE** /BatchSeperator/{batchSeperatorId} | 
[**batchSeperatorExistsBatchSeperator**](BatchSeperatorApi.md#batchSeperatorExistsBatchSeperator) | **GET** /BatchSeperator/exists/{batchSeperatorId} | 
[**batchSeperatorGetBatchSeperator**](BatchSeperatorApi.md#batchSeperatorGetBatchSeperator) | **GET** /BatchSeperator/{batchSeperatorId} | 
[**batchSeperatorGetBatchSeperators**](BatchSeperatorApi.md#batchSeperatorGetBatchSeperators) | **POST** /BatchSeperator/search | 
[**batchSeperatorUpdateBatchSeperator**](BatchSeperatorApi.md#batchSeperatorUpdateBatchSeperator) | **PUT** /BatchSeperator/{batchSeperatorId} | 


<a name="batchSeperatorCreateBatchSeperator"></a>
# **batchSeperatorCreateBatchSeperator**
> Object batchSeperatorCreateBatchSeperator(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BatchSeperatorApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

BatchSeperatorApi apiInstance = new BatchSeperatorApi();
BatchSeperatorDto dto = new BatchSeperatorDto(); // BatchSeperatorDto | 
try {
    Object result = apiInstance.batchSeperatorCreateBatchSeperator(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BatchSeperatorApi#batchSeperatorCreateBatchSeperator");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**BatchSeperatorDto**](BatchSeperatorDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="batchSeperatorDeleteBatchSeperator"></a>
# **batchSeperatorDeleteBatchSeperator**
> Object batchSeperatorDeleteBatchSeperator(batchSeperatorId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BatchSeperatorApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

BatchSeperatorApi apiInstance = new BatchSeperatorApi();
Integer batchSeperatorId = 56; // Integer | 
try {
    Object result = apiInstance.batchSeperatorDeleteBatchSeperator(batchSeperatorId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BatchSeperatorApi#batchSeperatorDeleteBatchSeperator");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batchSeperatorId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="batchSeperatorExistsBatchSeperator"></a>
# **batchSeperatorExistsBatchSeperator**
> Object batchSeperatorExistsBatchSeperator(batchSeperatorId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BatchSeperatorApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

BatchSeperatorApi apiInstance = new BatchSeperatorApi();
Integer batchSeperatorId = 56; // Integer | 
try {
    Object result = apiInstance.batchSeperatorExistsBatchSeperator(batchSeperatorId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BatchSeperatorApi#batchSeperatorExistsBatchSeperator");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batchSeperatorId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="batchSeperatorGetBatchSeperator"></a>
# **batchSeperatorGetBatchSeperator**
> Object batchSeperatorGetBatchSeperator(batchSeperatorId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BatchSeperatorApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

BatchSeperatorApi apiInstance = new BatchSeperatorApi();
Integer batchSeperatorId = 56; // Integer | 
try {
    Object result = apiInstance.batchSeperatorGetBatchSeperator(batchSeperatorId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BatchSeperatorApi#batchSeperatorGetBatchSeperator");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batchSeperatorId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="batchSeperatorGetBatchSeperators"></a>
# **batchSeperatorGetBatchSeperators**
> Object batchSeperatorGetBatchSeperators(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BatchSeperatorApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

BatchSeperatorApi apiInstance = new BatchSeperatorApi();
GetFilteredBatchSeperatorQuery query = new GetFilteredBatchSeperatorQuery(); // GetFilteredBatchSeperatorQuery | 
try {
    Object result = apiInstance.batchSeperatorGetBatchSeperators(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BatchSeperatorApi#batchSeperatorGetBatchSeperators");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredBatchSeperatorQuery**](GetFilteredBatchSeperatorQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="batchSeperatorUpdateBatchSeperator"></a>
# **batchSeperatorUpdateBatchSeperator**
> Object batchSeperatorUpdateBatchSeperator(batchSeperatorId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.BatchSeperatorApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

BatchSeperatorApi apiInstance = new BatchSeperatorApi();
Integer batchSeperatorId = 56; // Integer | 
BatchSeperatorDto dto = new BatchSeperatorDto(); // BatchSeperatorDto | 
try {
    Object result = apiInstance.batchSeperatorUpdateBatchSeperator(batchSeperatorId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BatchSeperatorApi#batchSeperatorUpdateBatchSeperator");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batchSeperatorId** | **Integer**|  |
 **dto** | [**BatchSeperatorDto**](BatchSeperatorDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

