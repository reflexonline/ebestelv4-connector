
# GetFilteredWebshopQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webshopIdMinValue** | **Integer** |  |  [optional]
**webshopIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]
**callTypeIdMinValue** | **Integer** |  |  [optional]
**callTypeIdMaxValue** | **Integer** |  |  [optional]



