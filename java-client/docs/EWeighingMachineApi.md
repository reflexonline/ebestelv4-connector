# EWeighingMachineApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**eWeighingMachineCreateEWeighingMachine**](EWeighingMachineApi.md#eWeighingMachineCreateEWeighingMachine) | **POST** /EWeighingMachine | 
[**eWeighingMachineDeleteEWeighingMachine**](EWeighingMachineApi.md#eWeighingMachineDeleteEWeighingMachine) | **DELETE** /EWeighingMachine/{machineId} | 
[**eWeighingMachineExistsEWeighingMachine**](EWeighingMachineApi.md#eWeighingMachineExistsEWeighingMachine) | **GET** /EWeighingMachine/exists/{machineId} | 
[**eWeighingMachineGetEWeighingMachine**](EWeighingMachineApi.md#eWeighingMachineGetEWeighingMachine) | **GET** /EWeighingMachine/{machineId} | 
[**eWeighingMachineGetEWeighingMachines**](EWeighingMachineApi.md#eWeighingMachineGetEWeighingMachines) | **POST** /EWeighingMachine/search | 
[**eWeighingMachineUpdateEWeighingMachine**](EWeighingMachineApi.md#eWeighingMachineUpdateEWeighingMachine) | **PUT** /EWeighingMachine/{machineId} | 


<a name="eWeighingMachineCreateEWeighingMachine"></a>
# **eWeighingMachineCreateEWeighingMachine**
> Object eWeighingMachineCreateEWeighingMachine(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.EWeighingMachineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

EWeighingMachineApi apiInstance = new EWeighingMachineApi();
EWeighingMachineDto dto = new EWeighingMachineDto(); // EWeighingMachineDto | 
try {
    Object result = apiInstance.eWeighingMachineCreateEWeighingMachine(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EWeighingMachineApi#eWeighingMachineCreateEWeighingMachine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**EWeighingMachineDto**](EWeighingMachineDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="eWeighingMachineDeleteEWeighingMachine"></a>
# **eWeighingMachineDeleteEWeighingMachine**
> Object eWeighingMachineDeleteEWeighingMachine(machineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.EWeighingMachineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

EWeighingMachineApi apiInstance = new EWeighingMachineApi();
Integer machineId = 56; // Integer | 
try {
    Object result = apiInstance.eWeighingMachineDeleteEWeighingMachine(machineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EWeighingMachineApi#eWeighingMachineDeleteEWeighingMachine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **machineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="eWeighingMachineExistsEWeighingMachine"></a>
# **eWeighingMachineExistsEWeighingMachine**
> Object eWeighingMachineExistsEWeighingMachine(machineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.EWeighingMachineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

EWeighingMachineApi apiInstance = new EWeighingMachineApi();
Integer machineId = 56; // Integer | 
try {
    Object result = apiInstance.eWeighingMachineExistsEWeighingMachine(machineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EWeighingMachineApi#eWeighingMachineExistsEWeighingMachine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **machineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="eWeighingMachineGetEWeighingMachine"></a>
# **eWeighingMachineGetEWeighingMachine**
> Object eWeighingMachineGetEWeighingMachine(machineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.EWeighingMachineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

EWeighingMachineApi apiInstance = new EWeighingMachineApi();
Integer machineId = 56; // Integer | 
try {
    Object result = apiInstance.eWeighingMachineGetEWeighingMachine(machineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EWeighingMachineApi#eWeighingMachineGetEWeighingMachine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **machineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="eWeighingMachineGetEWeighingMachines"></a>
# **eWeighingMachineGetEWeighingMachines**
> Object eWeighingMachineGetEWeighingMachines(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.EWeighingMachineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

EWeighingMachineApi apiInstance = new EWeighingMachineApi();
GetFilteredEWeighingMachineQuery query = new GetFilteredEWeighingMachineQuery(); // GetFilteredEWeighingMachineQuery | 
try {
    Object result = apiInstance.eWeighingMachineGetEWeighingMachines(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EWeighingMachineApi#eWeighingMachineGetEWeighingMachines");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredEWeighingMachineQuery**](GetFilteredEWeighingMachineQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="eWeighingMachineUpdateEWeighingMachine"></a>
# **eWeighingMachineUpdateEWeighingMachine**
> Object eWeighingMachineUpdateEWeighingMachine(machineId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.EWeighingMachineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

EWeighingMachineApi apiInstance = new EWeighingMachineApi();
Integer machineId = 56; // Integer | 
EWeighingMachineDto dto = new EWeighingMachineDto(); // EWeighingMachineDto | 
try {
    Object result = apiInstance.eWeighingMachineUpdateEWeighingMachine(machineId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EWeighingMachineApi#eWeighingMachineUpdateEWeighingMachine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **machineId** | **Integer**|  |
 **dto** | [**EWeighingMachineDto**](EWeighingMachineDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

