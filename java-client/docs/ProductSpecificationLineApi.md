# ProductSpecificationLineApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productSpecificationLineCreateProductSpecificationLine**](ProductSpecificationLineApi.md#productSpecificationLineCreateProductSpecificationLine) | **POST** /ProductSpecificationLine | 
[**productSpecificationLineDeleteProductSpecificationLine**](ProductSpecificationLineApi.md#productSpecificationLineDeleteProductSpecificationLine) | **DELETE** /ProductSpecificationLine/{productSpecificationCategorieId}/{productSpecificationLineId} | 
[**productSpecificationLineExistsProductSpecificationLine**](ProductSpecificationLineApi.md#productSpecificationLineExistsProductSpecificationLine) | **GET** /ProductSpecificationLine/exists/{productSpecificationCategorieId}/{productSpecificationLineId} | 
[**productSpecificationLineGetProductSpecificationLine**](ProductSpecificationLineApi.md#productSpecificationLineGetProductSpecificationLine) | **GET** /ProductSpecificationLine/{productSpecificationCategorieId}/{productSpecificationLineId} | 
[**productSpecificationLineGetProductSpecificationLines**](ProductSpecificationLineApi.md#productSpecificationLineGetProductSpecificationLines) | **POST** /ProductSpecificationLine/search | 
[**productSpecificationLineUpdateProductSpecificationLine**](ProductSpecificationLineApi.md#productSpecificationLineUpdateProductSpecificationLine) | **PUT** /ProductSpecificationLine/{productSpecificationCategorieId}/{productSpecificationLineId} | 


<a name="productSpecificationLineCreateProductSpecificationLine"></a>
# **productSpecificationLineCreateProductSpecificationLine**
> Object productSpecificationLineCreateProductSpecificationLine(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationLineApi apiInstance = new ProductSpecificationLineApi();
ProductSpecificationLineDto dto = new ProductSpecificationLineDto(); // ProductSpecificationLineDto | 
try {
    Object result = apiInstance.productSpecificationLineCreateProductSpecificationLine(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationLineApi#productSpecificationLineCreateProductSpecificationLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ProductSpecificationLineDto**](ProductSpecificationLineDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productSpecificationLineDeleteProductSpecificationLine"></a>
# **productSpecificationLineDeleteProductSpecificationLine**
> Object productSpecificationLineDeleteProductSpecificationLine(productSpecificationCategorieId, productSpecificationLineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationLineApi apiInstance = new ProductSpecificationLineApi();
Integer productSpecificationCategorieId = 56; // Integer | 
Integer productSpecificationLineId = 56; // Integer | 
try {
    Object result = apiInstance.productSpecificationLineDeleteProductSpecificationLine(productSpecificationCategorieId, productSpecificationLineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationLineApi#productSpecificationLineDeleteProductSpecificationLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationCategorieId** | **Integer**|  |
 **productSpecificationLineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productSpecificationLineExistsProductSpecificationLine"></a>
# **productSpecificationLineExistsProductSpecificationLine**
> Object productSpecificationLineExistsProductSpecificationLine(productSpecificationCategorieId, productSpecificationLineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationLineApi apiInstance = new ProductSpecificationLineApi();
Integer productSpecificationCategorieId = 56; // Integer | 
Integer productSpecificationLineId = 56; // Integer | 
try {
    Object result = apiInstance.productSpecificationLineExistsProductSpecificationLine(productSpecificationCategorieId, productSpecificationLineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationLineApi#productSpecificationLineExistsProductSpecificationLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationCategorieId** | **Integer**|  |
 **productSpecificationLineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productSpecificationLineGetProductSpecificationLine"></a>
# **productSpecificationLineGetProductSpecificationLine**
> Object productSpecificationLineGetProductSpecificationLine(productSpecificationCategorieId, productSpecificationLineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationLineApi apiInstance = new ProductSpecificationLineApi();
Integer productSpecificationCategorieId = 56; // Integer | 
Integer productSpecificationLineId = 56; // Integer | 
try {
    Object result = apiInstance.productSpecificationLineGetProductSpecificationLine(productSpecificationCategorieId, productSpecificationLineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationLineApi#productSpecificationLineGetProductSpecificationLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationCategorieId** | **Integer**|  |
 **productSpecificationLineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productSpecificationLineGetProductSpecificationLines"></a>
# **productSpecificationLineGetProductSpecificationLines**
> Object productSpecificationLineGetProductSpecificationLines(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationLineApi apiInstance = new ProductSpecificationLineApi();
GetFilteredProductSpecificationLineQuery query = new GetFilteredProductSpecificationLineQuery(); // GetFilteredProductSpecificationLineQuery | 
try {
    Object result = apiInstance.productSpecificationLineGetProductSpecificationLines(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationLineApi#productSpecificationLineGetProductSpecificationLines");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductSpecificationLineQuery**](GetFilteredProductSpecificationLineQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productSpecificationLineUpdateProductSpecificationLine"></a>
# **productSpecificationLineUpdateProductSpecificationLine**
> Object productSpecificationLineUpdateProductSpecificationLine(productSpecificationCategorieId, productSpecificationLineId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationLineApi apiInstance = new ProductSpecificationLineApi();
Integer productSpecificationCategorieId = 56; // Integer | 
Integer productSpecificationLineId = 56; // Integer | 
ProductSpecificationLineDto dto = new ProductSpecificationLineDto(); // ProductSpecificationLineDto | 
try {
    Object result = apiInstance.productSpecificationLineUpdateProductSpecificationLine(productSpecificationCategorieId, productSpecificationLineId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationLineApi#productSpecificationLineUpdateProductSpecificationLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationCategorieId** | **Integer**|  |
 **productSpecificationLineId** | **Integer**|  |
 **dto** | [**ProductSpecificationLineDto**](ProductSpecificationLineDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

