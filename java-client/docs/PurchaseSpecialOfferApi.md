# PurchaseSpecialOfferApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purchaseSpecialOfferCreatePurchaseSpecialOffer**](PurchaseSpecialOfferApi.md#purchaseSpecialOfferCreatePurchaseSpecialOffer) | **POST** /PurchaseSpecialOffer | 
[**purchaseSpecialOfferDeletePurchaseSpecialOffer**](PurchaseSpecialOfferApi.md#purchaseSpecialOfferDeletePurchaseSpecialOffer) | **DELETE** /PurchaseSpecialOffer/{supplierId}/{articleId}/{dateIndex} | 
[**purchaseSpecialOfferExistsPurchaseSpecialOffer**](PurchaseSpecialOfferApi.md#purchaseSpecialOfferExistsPurchaseSpecialOffer) | **GET** /PurchaseSpecialOffer/exists/{supplierId}/{articleId}/{dateIndex} | 
[**purchaseSpecialOfferGetPurchaseSpecialOffer**](PurchaseSpecialOfferApi.md#purchaseSpecialOfferGetPurchaseSpecialOffer) | **GET** /PurchaseSpecialOffer/{supplierId}/{articleId}/{dateIndex} | 
[**purchaseSpecialOfferGetPurchaseSpecialOffers**](PurchaseSpecialOfferApi.md#purchaseSpecialOfferGetPurchaseSpecialOffers) | **POST** /PurchaseSpecialOffer/search | 
[**purchaseSpecialOfferUpdatePurchaseSpecialOffer**](PurchaseSpecialOfferApi.md#purchaseSpecialOfferUpdatePurchaseSpecialOffer) | **PUT** /PurchaseSpecialOffer/{supplierId}/{articleId}/{dateIndex} | 


<a name="purchaseSpecialOfferCreatePurchaseSpecialOffer"></a>
# **purchaseSpecialOfferCreatePurchaseSpecialOffer**
> Object purchaseSpecialOfferCreatePurchaseSpecialOffer(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseSpecialOfferApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseSpecialOfferApi apiInstance = new PurchaseSpecialOfferApi();
PurchaseSpecialOfferDto dto = new PurchaseSpecialOfferDto(); // PurchaseSpecialOfferDto | 
try {
    Object result = apiInstance.purchaseSpecialOfferCreatePurchaseSpecialOffer(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseSpecialOfferApi#purchaseSpecialOfferCreatePurchaseSpecialOffer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PurchaseSpecialOfferDto**](PurchaseSpecialOfferDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseSpecialOfferDeletePurchaseSpecialOffer"></a>
# **purchaseSpecialOfferDeletePurchaseSpecialOffer**
> Object purchaseSpecialOfferDeletePurchaseSpecialOffer(supplierId, articleId, dateIndex)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseSpecialOfferApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseSpecialOfferApi apiInstance = new PurchaseSpecialOfferApi();
Integer supplierId = 56; // Integer | 
Integer articleId = 56; // Integer | 
Integer dateIndex = 56; // Integer | 
try {
    Object result = apiInstance.purchaseSpecialOfferDeletePurchaseSpecialOffer(supplierId, articleId, dateIndex);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseSpecialOfferApi#purchaseSpecialOfferDeletePurchaseSpecialOffer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **articleId** | **Integer**|  |
 **dateIndex** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseSpecialOfferExistsPurchaseSpecialOffer"></a>
# **purchaseSpecialOfferExistsPurchaseSpecialOffer**
> Object purchaseSpecialOfferExistsPurchaseSpecialOffer(supplierId, articleId, dateIndex)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseSpecialOfferApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseSpecialOfferApi apiInstance = new PurchaseSpecialOfferApi();
Integer supplierId = 56; // Integer | 
Integer articleId = 56; // Integer | 
Integer dateIndex = 56; // Integer | 
try {
    Object result = apiInstance.purchaseSpecialOfferExistsPurchaseSpecialOffer(supplierId, articleId, dateIndex);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseSpecialOfferApi#purchaseSpecialOfferExistsPurchaseSpecialOffer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **articleId** | **Integer**|  |
 **dateIndex** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseSpecialOfferGetPurchaseSpecialOffer"></a>
# **purchaseSpecialOfferGetPurchaseSpecialOffer**
> Object purchaseSpecialOfferGetPurchaseSpecialOffer(supplierId, articleId, dateIndex)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseSpecialOfferApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseSpecialOfferApi apiInstance = new PurchaseSpecialOfferApi();
Integer supplierId = 56; // Integer | 
Integer articleId = 56; // Integer | 
Integer dateIndex = 56; // Integer | 
try {
    Object result = apiInstance.purchaseSpecialOfferGetPurchaseSpecialOffer(supplierId, articleId, dateIndex);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseSpecialOfferApi#purchaseSpecialOfferGetPurchaseSpecialOffer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **articleId** | **Integer**|  |
 **dateIndex** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseSpecialOfferGetPurchaseSpecialOffers"></a>
# **purchaseSpecialOfferGetPurchaseSpecialOffers**
> Object purchaseSpecialOfferGetPurchaseSpecialOffers(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseSpecialOfferApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseSpecialOfferApi apiInstance = new PurchaseSpecialOfferApi();
GetFilteredPurchaseSpecialOfferQuery query = new GetFilteredPurchaseSpecialOfferQuery(); // GetFilteredPurchaseSpecialOfferQuery | 
try {
    Object result = apiInstance.purchaseSpecialOfferGetPurchaseSpecialOffers(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseSpecialOfferApi#purchaseSpecialOfferGetPurchaseSpecialOffers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPurchaseSpecialOfferQuery**](GetFilteredPurchaseSpecialOfferQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseSpecialOfferUpdatePurchaseSpecialOffer"></a>
# **purchaseSpecialOfferUpdatePurchaseSpecialOffer**
> Object purchaseSpecialOfferUpdatePurchaseSpecialOffer(supplierId, articleId, dateIndex, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseSpecialOfferApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseSpecialOfferApi apiInstance = new PurchaseSpecialOfferApi();
Integer supplierId = 56; // Integer | 
Integer articleId = 56; // Integer | 
Integer dateIndex = 56; // Integer | 
PurchaseSpecialOfferDto dto = new PurchaseSpecialOfferDto(); // PurchaseSpecialOfferDto | 
try {
    Object result = apiInstance.purchaseSpecialOfferUpdatePurchaseSpecialOffer(supplierId, articleId, dateIndex, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseSpecialOfferApi#purchaseSpecialOfferUpdatePurchaseSpecialOffer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **articleId** | **Integer**|  |
 **dateIndex** | **Integer**|  |
 **dto** | [**PurchaseSpecialOfferDto**](PurchaseSpecialOfferDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

