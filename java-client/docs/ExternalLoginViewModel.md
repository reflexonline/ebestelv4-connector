
# ExternalLoginViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**state** | **String** |  |  [optional]



