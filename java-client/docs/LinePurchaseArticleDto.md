
# LinePurchaseArticleDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**shortName** | **String** |  |  [optional]
**unitType** | [**UnitTypeEnum**](#UnitTypeEnum) |  |  [optional]
**longName** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="UnitTypeEnum"></a>
## Enum: UnitTypeEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
PIECES | &quot;Pieces&quot;
WEIGHT | &quot;Weight&quot;
WEIGHTFIXEDPORTION | &quot;WeightFixedPortion&quot;



