# PurchaseOrderPatternLineApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purchaseOrderPatternLineCreatePurchaseOrderPatternLine**](PurchaseOrderPatternLineApi.md#purchaseOrderPatternLineCreatePurchaseOrderPatternLine) | **POST** /PurchaseOrderPatternLine | 
[**purchaseOrderPatternLineDeletePurchaseOrderPatternLine**](PurchaseOrderPatternLineApi.md#purchaseOrderPatternLineDeletePurchaseOrderPatternLine) | **DELETE** /PurchaseOrderPatternLine/{supplierId}/{list}/{line} | 
[**purchaseOrderPatternLineExistsPurchaseOrderPatternLine**](PurchaseOrderPatternLineApi.md#purchaseOrderPatternLineExistsPurchaseOrderPatternLine) | **GET** /PurchaseOrderPatternLine/exists/{supplierId}/{list}/{line} | 
[**purchaseOrderPatternLineGetPurchaseOrderPatternLine**](PurchaseOrderPatternLineApi.md#purchaseOrderPatternLineGetPurchaseOrderPatternLine) | **GET** /PurchaseOrderPatternLine/{supplierId}/{list}/{line} | 
[**purchaseOrderPatternLineGetPurchaseOrderPatternLines**](PurchaseOrderPatternLineApi.md#purchaseOrderPatternLineGetPurchaseOrderPatternLines) | **POST** /PurchaseOrderPatternLine/search | 
[**purchaseOrderPatternLineUpdatePurchaseOrderPatternLine**](PurchaseOrderPatternLineApi.md#purchaseOrderPatternLineUpdatePurchaseOrderPatternLine) | **PUT** /PurchaseOrderPatternLine/{supplierId}/{list}/{line} | 


<a name="purchaseOrderPatternLineCreatePurchaseOrderPatternLine"></a>
# **purchaseOrderPatternLineCreatePurchaseOrderPatternLine**
> Object purchaseOrderPatternLineCreatePurchaseOrderPatternLine(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderPatternLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderPatternLineApi apiInstance = new PurchaseOrderPatternLineApi();
PurchaseOrderPatternLineDto dto = new PurchaseOrderPatternLineDto(); // PurchaseOrderPatternLineDto | 
try {
    Object result = apiInstance.purchaseOrderPatternLineCreatePurchaseOrderPatternLine(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderPatternLineApi#purchaseOrderPatternLineCreatePurchaseOrderPatternLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PurchaseOrderPatternLineDto**](PurchaseOrderPatternLineDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseOrderPatternLineDeletePurchaseOrderPatternLine"></a>
# **purchaseOrderPatternLineDeletePurchaseOrderPatternLine**
> Object purchaseOrderPatternLineDeletePurchaseOrderPatternLine(supplierId, list, line)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderPatternLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderPatternLineApi apiInstance = new PurchaseOrderPatternLineApi();
Integer supplierId = 56; // Integer | 
Integer list = 56; // Integer | 
Integer line = 56; // Integer | 
try {
    Object result = apiInstance.purchaseOrderPatternLineDeletePurchaseOrderPatternLine(supplierId, list, line);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderPatternLineApi#purchaseOrderPatternLineDeletePurchaseOrderPatternLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **list** | **Integer**|  |
 **line** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseOrderPatternLineExistsPurchaseOrderPatternLine"></a>
# **purchaseOrderPatternLineExistsPurchaseOrderPatternLine**
> Object purchaseOrderPatternLineExistsPurchaseOrderPatternLine(supplierId, list, line)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderPatternLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderPatternLineApi apiInstance = new PurchaseOrderPatternLineApi();
Integer supplierId = 56; // Integer | 
Integer list = 56; // Integer | 
Integer line = 56; // Integer | 
try {
    Object result = apiInstance.purchaseOrderPatternLineExistsPurchaseOrderPatternLine(supplierId, list, line);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderPatternLineApi#purchaseOrderPatternLineExistsPurchaseOrderPatternLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **list** | **Integer**|  |
 **line** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseOrderPatternLineGetPurchaseOrderPatternLine"></a>
# **purchaseOrderPatternLineGetPurchaseOrderPatternLine**
> Object purchaseOrderPatternLineGetPurchaseOrderPatternLine(supplierId, list, line)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderPatternLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderPatternLineApi apiInstance = new PurchaseOrderPatternLineApi();
Integer supplierId = 56; // Integer | 
Integer list = 56; // Integer | 
Integer line = 56; // Integer | 
try {
    Object result = apiInstance.purchaseOrderPatternLineGetPurchaseOrderPatternLine(supplierId, list, line);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderPatternLineApi#purchaseOrderPatternLineGetPurchaseOrderPatternLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **list** | **Integer**|  |
 **line** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseOrderPatternLineGetPurchaseOrderPatternLines"></a>
# **purchaseOrderPatternLineGetPurchaseOrderPatternLines**
> Object purchaseOrderPatternLineGetPurchaseOrderPatternLines(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderPatternLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderPatternLineApi apiInstance = new PurchaseOrderPatternLineApi();
GetFilteredPurchaseOrderPatternLineQuery query = new GetFilteredPurchaseOrderPatternLineQuery(); // GetFilteredPurchaseOrderPatternLineQuery | 
try {
    Object result = apiInstance.purchaseOrderPatternLineGetPurchaseOrderPatternLines(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderPatternLineApi#purchaseOrderPatternLineGetPurchaseOrderPatternLines");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPurchaseOrderPatternLineQuery**](GetFilteredPurchaseOrderPatternLineQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseOrderPatternLineUpdatePurchaseOrderPatternLine"></a>
# **purchaseOrderPatternLineUpdatePurchaseOrderPatternLine**
> Object purchaseOrderPatternLineUpdatePurchaseOrderPatternLine(supplierId, list, line, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderPatternLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderPatternLineApi apiInstance = new PurchaseOrderPatternLineApi();
Integer supplierId = 56; // Integer | 
Integer list = 56; // Integer | 
Integer line = 56; // Integer | 
PurchaseOrderPatternLineDto dto = new PurchaseOrderPatternLineDto(); // PurchaseOrderPatternLineDto | 
try {
    Object result = apiInstance.purchaseOrderPatternLineUpdatePurchaseOrderPatternLine(supplierId, list, line, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderPatternLineApi#purchaseOrderPatternLineUpdatePurchaseOrderPatternLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **list** | **Integer**|  |
 **line** | **Integer**|  |
 **dto** | [**PurchaseOrderPatternLineDto**](PurchaseOrderPatternLineDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

