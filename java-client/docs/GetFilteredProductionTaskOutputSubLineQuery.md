
# GetFilteredProductionTaskOutputSubLineQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productionTaskIdMinValue** | **Integer** |  |  [optional]
**productionTaskIdMaxValue** | **Integer** |  |  [optional]
**productionTaskOutputLineIdMinValue** | **Integer** |  |  [optional]
**productionTaskOutputLineIdMaxValue** | **Integer** |  |  [optional]
**chargeIdMinValue** | **Integer** |  |  [optional]
**chargeIdMaxValue** | **Integer** |  |  [optional]
**batchIdMinValue** | **Integer** |  |  [optional]
**batchIdMaxValue** | **Integer** |  |  [optional]
**coupledSaleOrderSubLineIdMinValue** | **Integer** |  |  [optional]
**coupledSaleOrderSubLineIdMaxValue** | **Integer** |  |  [optional]
**productionTaskOutputSubLineIdMinValue** | **Integer** |  |  [optional]
**productionTaskOutputSubLineIdMaxValue** | **Integer** |  |  [optional]



