
# GetFilteredLabelPrinterQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**labelPrinterIdMinValue** | **Integer** |  |  [optional]
**labelPrinterIdMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]



