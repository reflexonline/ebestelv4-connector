
# WebshopDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webshopId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**serverUrl** | **String** |  |  [optional]
**useProxy** | **Boolean** |  |  [optional]
**proxyUrl** | **String** |  |  [optional]
**isDumpModeEnabled** | **Boolean** |  |  [optional]
**maxDumpSize** | **Integer** |  |  [optional]
**maxBackupSize** | **Integer** |  |  [optional]
**isAutoOrderActivationEnabled** | **Boolean** |  |  [optional]
**isStockCheckingEnabled** | **Boolean** |  |  [optional]
**importPrinterId** | **Integer** |  |  [optional]
**acceptedOrderPrinterId** | **Integer** |  |  [optional]
**rejectedOrderPrinterId** | **Integer** |  |  [optional]
**enablePrinterMatchReport** | **Boolean** |  |  [optional]
**matchReportPrinterId** | **Integer** |  |  [optional]
**actionOnMessage** | **Integer** |  |  [optional]
**callDestinationId** | **Integer** |  |  [optional]
**callType** | **Integer** |  |  [optional]
**isBudgetAllowed** | **Boolean** |  |  [optional]
**isAnalysisAllowed** | **Boolean** |  |  [optional]
**isPackingSlipHistoryEnabled** | **Boolean** |  |  [optional]
**isInvoiceHistoryEnabled** | **Boolean** |  |  [optional]
**isDistributionCenter** | **Boolean** |  |  [optional]
**canModifyOrders** | **Boolean** |  |  [optional]
**enableScheduler** | **Integer** |  |  [optional]
**sleepFromTime** | **String** |  |  [optional]
**sleepToTime** | **String** |  |  [optional]
**isEngineRunning** | **Boolean** |  |  [optional]
**maxNumberOfCustomers** | **Integer** |  |  [optional]
**lastActionDateTime** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**messageId** | **Integer** |  |  [optional]
**clearPhotoCache** | **Boolean** |  |  [optional]
**isNonRSWebshop** | **Boolean** |  |  [optional]
**isTestAdministration** | **Boolean** |  |  [optional]
**offertePrefix** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



