
# GetFilteredAllergenQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allergenIdMinValue** | **Integer** |  |  [optional]
**allergenIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



