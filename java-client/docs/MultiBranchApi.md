# MultiBranchApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**multiBranchCreateMultiBranch**](MultiBranchApi.md#multiBranchCreateMultiBranch) | **POST** /MultiBranch | 
[**multiBranchDeleteMultiBranch**](MultiBranchApi.md#multiBranchDeleteMultiBranch) | **DELETE** /MultiBranch/{multiBranchId} | 
[**multiBranchExistsMultiBranch**](MultiBranchApi.md#multiBranchExistsMultiBranch) | **GET** /MultiBranch/exists/{multiBranchId} | 
[**multiBranchGetMultiBranch**](MultiBranchApi.md#multiBranchGetMultiBranch) | **GET** /MultiBranch/{multiBranchId} | 
[**multiBranchGetMultiBranches**](MultiBranchApi.md#multiBranchGetMultiBranches) | **POST** /MultiBranch/search | 
[**multiBranchUpdateMultiBranch**](MultiBranchApi.md#multiBranchUpdateMultiBranch) | **PUT** /MultiBranch/{multiBranchId} | 


<a name="multiBranchCreateMultiBranch"></a>
# **multiBranchCreateMultiBranch**
> Object multiBranchCreateMultiBranch(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MultiBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MultiBranchApi apiInstance = new MultiBranchApi();
MultiBranchDto dto = new MultiBranchDto(); // MultiBranchDto | 
try {
    Object result = apiInstance.multiBranchCreateMultiBranch(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MultiBranchApi#multiBranchCreateMultiBranch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**MultiBranchDto**](MultiBranchDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="multiBranchDeleteMultiBranch"></a>
# **multiBranchDeleteMultiBranch**
> Object multiBranchDeleteMultiBranch(multiBranchId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MultiBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MultiBranchApi apiInstance = new MultiBranchApi();
Integer multiBranchId = 56; // Integer | 
try {
    Object result = apiInstance.multiBranchDeleteMultiBranch(multiBranchId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MultiBranchApi#multiBranchDeleteMultiBranch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **multiBranchId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="multiBranchExistsMultiBranch"></a>
# **multiBranchExistsMultiBranch**
> Object multiBranchExistsMultiBranch(multiBranchId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MultiBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MultiBranchApi apiInstance = new MultiBranchApi();
Integer multiBranchId = 56; // Integer | 
try {
    Object result = apiInstance.multiBranchExistsMultiBranch(multiBranchId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MultiBranchApi#multiBranchExistsMultiBranch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **multiBranchId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="multiBranchGetMultiBranch"></a>
# **multiBranchGetMultiBranch**
> Object multiBranchGetMultiBranch(multiBranchId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MultiBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MultiBranchApi apiInstance = new MultiBranchApi();
Integer multiBranchId = 56; // Integer | 
try {
    Object result = apiInstance.multiBranchGetMultiBranch(multiBranchId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MultiBranchApi#multiBranchGetMultiBranch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **multiBranchId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="multiBranchGetMultiBranches"></a>
# **multiBranchGetMultiBranches**
> Object multiBranchGetMultiBranches(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MultiBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MultiBranchApi apiInstance = new MultiBranchApi();
GetFilteredMultiBranchQuery query = new GetFilteredMultiBranchQuery(); // GetFilteredMultiBranchQuery | 
try {
    Object result = apiInstance.multiBranchGetMultiBranches(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MultiBranchApi#multiBranchGetMultiBranches");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredMultiBranchQuery**](GetFilteredMultiBranchQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="multiBranchUpdateMultiBranch"></a>
# **multiBranchUpdateMultiBranch**
> Object multiBranchUpdateMultiBranch(multiBranchId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MultiBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MultiBranchApi apiInstance = new MultiBranchApi();
Integer multiBranchId = 56; // Integer | 
MultiBranchDto dto = new MultiBranchDto(); // MultiBranchDto | 
try {
    Object result = apiInstance.multiBranchUpdateMultiBranch(multiBranchId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MultiBranchApi#multiBranchUpdateMultiBranch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **multiBranchId** | **Integer**|  |
 **dto** | [**MultiBranchDto**](MultiBranchDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

