# PurchaseArticleOrderGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purchaseArticleOrderGroupCreatePurchaseArticleOrderGroup**](PurchaseArticleOrderGroupApi.md#purchaseArticleOrderGroupCreatePurchaseArticleOrderGroup) | **POST** /PurchaseArticleOrderGroup | 
[**purchaseArticleOrderGroupDeletePurchaseArticleOrderGroup**](PurchaseArticleOrderGroupApi.md#purchaseArticleOrderGroupDeletePurchaseArticleOrderGroup) | **DELETE** /PurchaseArticleOrderGroup/{purchaseArticleOrderGroupId} | 
[**purchaseArticleOrderGroupExistsPurchaseArticleOrderGroup**](PurchaseArticleOrderGroupApi.md#purchaseArticleOrderGroupExistsPurchaseArticleOrderGroup) | **GET** /PurchaseArticleOrderGroup/exists/{purchaseArticleOrderGroupId} | 
[**purchaseArticleOrderGroupGetPurchaseArticleOrderGroup**](PurchaseArticleOrderGroupApi.md#purchaseArticleOrderGroupGetPurchaseArticleOrderGroup) | **GET** /PurchaseArticleOrderGroup/{purchaseArticleOrderGroupId} | 
[**purchaseArticleOrderGroupGetPurchaseArticleOrderGroups**](PurchaseArticleOrderGroupApi.md#purchaseArticleOrderGroupGetPurchaseArticleOrderGroups) | **POST** /PurchaseArticleOrderGroup/search | 
[**purchaseArticleOrderGroupUpdatePurchaseArticleOrderGroup**](PurchaseArticleOrderGroupApi.md#purchaseArticleOrderGroupUpdatePurchaseArticleOrderGroup) | **PUT** /PurchaseArticleOrderGroup/{purchaseArticleOrderGroupId} | 


<a name="purchaseArticleOrderGroupCreatePurchaseArticleOrderGroup"></a>
# **purchaseArticleOrderGroupCreatePurchaseArticleOrderGroup**
> Object purchaseArticleOrderGroupCreatePurchaseArticleOrderGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleOrderGroupApi apiInstance = new PurchaseArticleOrderGroupApi();
PurchaseArticleOrderGroupDto dto = new PurchaseArticleOrderGroupDto(); // PurchaseArticleOrderGroupDto | 
try {
    Object result = apiInstance.purchaseArticleOrderGroupCreatePurchaseArticleOrderGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleOrderGroupApi#purchaseArticleOrderGroupCreatePurchaseArticleOrderGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PurchaseArticleOrderGroupDto**](PurchaseArticleOrderGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseArticleOrderGroupDeletePurchaseArticleOrderGroup"></a>
# **purchaseArticleOrderGroupDeletePurchaseArticleOrderGroup**
> Object purchaseArticleOrderGroupDeletePurchaseArticleOrderGroup(purchaseArticleOrderGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleOrderGroupApi apiInstance = new PurchaseArticleOrderGroupApi();
Integer purchaseArticleOrderGroupId = 56; // Integer | 
try {
    Object result = apiInstance.purchaseArticleOrderGroupDeletePurchaseArticleOrderGroup(purchaseArticleOrderGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleOrderGroupApi#purchaseArticleOrderGroupDeletePurchaseArticleOrderGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchaseArticleOrderGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseArticleOrderGroupExistsPurchaseArticleOrderGroup"></a>
# **purchaseArticleOrderGroupExistsPurchaseArticleOrderGroup**
> Object purchaseArticleOrderGroupExistsPurchaseArticleOrderGroup(purchaseArticleOrderGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleOrderGroupApi apiInstance = new PurchaseArticleOrderGroupApi();
Integer purchaseArticleOrderGroupId = 56; // Integer | 
try {
    Object result = apiInstance.purchaseArticleOrderGroupExistsPurchaseArticleOrderGroup(purchaseArticleOrderGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleOrderGroupApi#purchaseArticleOrderGroupExistsPurchaseArticleOrderGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchaseArticleOrderGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseArticleOrderGroupGetPurchaseArticleOrderGroup"></a>
# **purchaseArticleOrderGroupGetPurchaseArticleOrderGroup**
> Object purchaseArticleOrderGroupGetPurchaseArticleOrderGroup(purchaseArticleOrderGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleOrderGroupApi apiInstance = new PurchaseArticleOrderGroupApi();
Integer purchaseArticleOrderGroupId = 56; // Integer | 
try {
    Object result = apiInstance.purchaseArticleOrderGroupGetPurchaseArticleOrderGroup(purchaseArticleOrderGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleOrderGroupApi#purchaseArticleOrderGroupGetPurchaseArticleOrderGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchaseArticleOrderGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseArticleOrderGroupGetPurchaseArticleOrderGroups"></a>
# **purchaseArticleOrderGroupGetPurchaseArticleOrderGroups**
> Object purchaseArticleOrderGroupGetPurchaseArticleOrderGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleOrderGroupApi apiInstance = new PurchaseArticleOrderGroupApi();
GetFilteredPurchaseArticleOrderGroupQuery query = new GetFilteredPurchaseArticleOrderGroupQuery(); // GetFilteredPurchaseArticleOrderGroupQuery | 
try {
    Object result = apiInstance.purchaseArticleOrderGroupGetPurchaseArticleOrderGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleOrderGroupApi#purchaseArticleOrderGroupGetPurchaseArticleOrderGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPurchaseArticleOrderGroupQuery**](GetFilteredPurchaseArticleOrderGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseArticleOrderGroupUpdatePurchaseArticleOrderGroup"></a>
# **purchaseArticleOrderGroupUpdatePurchaseArticleOrderGroup**
> Object purchaseArticleOrderGroupUpdatePurchaseArticleOrderGroup(purchaseArticleOrderGroupId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleOrderGroupApi apiInstance = new PurchaseArticleOrderGroupApi();
Integer purchaseArticleOrderGroupId = 56; // Integer | 
PurchaseArticleOrderGroupDto dto = new PurchaseArticleOrderGroupDto(); // PurchaseArticleOrderGroupDto | 
try {
    Object result = apiInstance.purchaseArticleOrderGroupUpdatePurchaseArticleOrderGroup(purchaseArticleOrderGroupId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleOrderGroupApi#purchaseArticleOrderGroupUpdatePurchaseArticleOrderGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchaseArticleOrderGroupId** | **Integer**|  |
 **dto** | [**PurchaseArticleOrderGroupDto**](PurchaseArticleOrderGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

