
# WpuSelectionRuleGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**selRuleGroupId** | **Integer** |  | 
**selRuleGroupNumber** | **Integer** |  |  [optional]
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



