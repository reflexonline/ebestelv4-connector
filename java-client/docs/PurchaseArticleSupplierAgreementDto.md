
# PurchaseArticleSupplierAgreementDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierId** | **Integer** |  | 
**purchaseArticleId** | **Integer** |  | 
**credPlu** | **String** |  |  [optional]
**extensionType** | [**ExtensionTypeEnum**](#ExtensionTypeEnum) |  |  [optional]
**bedragOld** | **Float** |  |  [optional]
**mutationDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**amount** | **Double** |  |  [optional]
**kortPerc** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="ExtensionTypeEnum"></a>
## Enum: ExtensionTypeEnum
Name | Value
---- | -----
NOPRICEAGREEMENT | &quot;NoPriceAgreement&quot;
PRICEDISCOUNT | &quot;PriceDiscount&quot;
PRICEDISCOUNTPERCENTAGE | &quot;PriceDiscountPercentage&quot;
FIXEDPRICE | &quot;FixedPrice&quot;
ARTICLEISSELECTED | &quot;ArticleIsSelected&quot;
ARTICLEISBLOCKED | &quot;ArticleIsBlocked&quot;



