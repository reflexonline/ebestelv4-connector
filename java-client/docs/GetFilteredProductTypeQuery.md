
# GetFilteredProductTypeQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**omschrijvingMinValue** | **String** |  |  [optional]
**omschrijvingMaxValue** | **String** |  |  [optional]



