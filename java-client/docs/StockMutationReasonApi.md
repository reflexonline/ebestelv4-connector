# StockMutationReasonApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stockMutationReasonCreateStockMutationReason**](StockMutationReasonApi.md#stockMutationReasonCreateStockMutationReason) | **POST** /StockMutationReason | 
[**stockMutationReasonDeleteStockMutationReason**](StockMutationReasonApi.md#stockMutationReasonDeleteStockMutationReason) | **DELETE** /StockMutationReason/{number} | 
[**stockMutationReasonExistsStockMutationReason**](StockMutationReasonApi.md#stockMutationReasonExistsStockMutationReason) | **GET** /StockMutationReason/exists/{number} | 
[**stockMutationReasonGetStockMutationReason**](StockMutationReasonApi.md#stockMutationReasonGetStockMutationReason) | **GET** /StockMutationReason/{number} | 
[**stockMutationReasonGetStockMutationReasons**](StockMutationReasonApi.md#stockMutationReasonGetStockMutationReasons) | **POST** /StockMutationReason/search | 
[**stockMutationReasonUpdateStockMutationReason**](StockMutationReasonApi.md#stockMutationReasonUpdateStockMutationReason) | **PUT** /StockMutationReason/{number} | 


<a name="stockMutationReasonCreateStockMutationReason"></a>
# **stockMutationReasonCreateStockMutationReason**
> Object stockMutationReasonCreateStockMutationReason(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockMutationReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockMutationReasonApi apiInstance = new StockMutationReasonApi();
StockMutationReasonDto dto = new StockMutationReasonDto(); // StockMutationReasonDto | 
try {
    Object result = apiInstance.stockMutationReasonCreateStockMutationReason(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockMutationReasonApi#stockMutationReasonCreateStockMutationReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**StockMutationReasonDto**](StockMutationReasonDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="stockMutationReasonDeleteStockMutationReason"></a>
# **stockMutationReasonDeleteStockMutationReason**
> Object stockMutationReasonDeleteStockMutationReason(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockMutationReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockMutationReasonApi apiInstance = new StockMutationReasonApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.stockMutationReasonDeleteStockMutationReason(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockMutationReasonApi#stockMutationReasonDeleteStockMutationReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="stockMutationReasonExistsStockMutationReason"></a>
# **stockMutationReasonExistsStockMutationReason**
> Object stockMutationReasonExistsStockMutationReason(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockMutationReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockMutationReasonApi apiInstance = new StockMutationReasonApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.stockMutationReasonExistsStockMutationReason(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockMutationReasonApi#stockMutationReasonExistsStockMutationReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="stockMutationReasonGetStockMutationReason"></a>
# **stockMutationReasonGetStockMutationReason**
> Object stockMutationReasonGetStockMutationReason(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockMutationReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockMutationReasonApi apiInstance = new StockMutationReasonApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.stockMutationReasonGetStockMutationReason(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockMutationReasonApi#stockMutationReasonGetStockMutationReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="stockMutationReasonGetStockMutationReasons"></a>
# **stockMutationReasonGetStockMutationReasons**
> Object stockMutationReasonGetStockMutationReasons(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockMutationReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockMutationReasonApi apiInstance = new StockMutationReasonApi();
GetFilteredStockMutationReasonQuery query = new GetFilteredStockMutationReasonQuery(); // GetFilteredStockMutationReasonQuery | 
try {
    Object result = apiInstance.stockMutationReasonGetStockMutationReasons(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockMutationReasonApi#stockMutationReasonGetStockMutationReasons");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredStockMutationReasonQuery**](GetFilteredStockMutationReasonQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="stockMutationReasonUpdateStockMutationReason"></a>
# **stockMutationReasonUpdateStockMutationReason**
> Object stockMutationReasonUpdateStockMutationReason(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockMutationReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockMutationReasonApi apiInstance = new StockMutationReasonApi();
Integer number = 56; // Integer | 
StockMutationReasonDto dto = new StockMutationReasonDto(); // StockMutationReasonDto | 
try {
    Object result = apiInstance.stockMutationReasonUpdateStockMutationReason(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockMutationReasonApi#stockMutationReasonUpdateStockMutationReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**StockMutationReasonDto**](StockMutationReasonDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

