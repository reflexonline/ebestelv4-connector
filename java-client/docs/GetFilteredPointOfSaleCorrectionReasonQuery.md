
# GetFilteredPointOfSaleCorrectionReasonQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pointOfSaleCorrectionReasonIdMinValue** | **Integer** |  |  [optional]
**pointOfSaleCorrectionReasonIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



