
# GetFilteredDockQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dockIdMinValue** | **Integer** |  |  [optional]
**dockIdMaxValue** | **Integer** |  |  [optional]
**multiBranchIdMinValue** | **Integer** |  |  [optional]
**multiBranchIdMaxValue** | **Integer** |  |  [optional]



