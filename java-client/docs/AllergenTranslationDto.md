
# AllergenTranslationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allergenId** | **Integer** |  | 
**languageId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



