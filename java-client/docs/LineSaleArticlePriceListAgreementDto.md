
# LineSaleArticlePriceListAgreementDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**priceListId** | **Integer** |  | 
**articleId** | **Integer** |  | 
**operations** | **List&lt;String&gt;** |  |  [optional]



