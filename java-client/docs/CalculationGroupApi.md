# CalculationGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**calculationGroupCreateCalculationGroup**](CalculationGroupApi.md#calculationGroupCreateCalculationGroup) | **POST** /CalculationGroup | 
[**calculationGroupDeleteCalculationGroup**](CalculationGroupApi.md#calculationGroupDeleteCalculationGroup) | **DELETE** /CalculationGroup/{calculationGroupId} | 
[**calculationGroupExistsCalculationGroup**](CalculationGroupApi.md#calculationGroupExistsCalculationGroup) | **GET** /CalculationGroup/exists/{calculationGroupId} | 
[**calculationGroupGetCalculationGroup**](CalculationGroupApi.md#calculationGroupGetCalculationGroup) | **GET** /CalculationGroup/{calculationGroupId} | 
[**calculationGroupGetCalculationGroups**](CalculationGroupApi.md#calculationGroupGetCalculationGroups) | **POST** /CalculationGroup/search | 
[**calculationGroupUpdateCalculationGroup**](CalculationGroupApi.md#calculationGroupUpdateCalculationGroup) | **PUT** /CalculationGroup/{calculationGroupId} | 


<a name="calculationGroupCreateCalculationGroup"></a>
# **calculationGroupCreateCalculationGroup**
> Object calculationGroupCreateCalculationGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CalculationGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CalculationGroupApi apiInstance = new CalculationGroupApi();
CalculationGroupDto dto = new CalculationGroupDto(); // CalculationGroupDto | 
try {
    Object result = apiInstance.calculationGroupCreateCalculationGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CalculationGroupApi#calculationGroupCreateCalculationGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**CalculationGroupDto**](CalculationGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="calculationGroupDeleteCalculationGroup"></a>
# **calculationGroupDeleteCalculationGroup**
> Object calculationGroupDeleteCalculationGroup(calculationGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CalculationGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CalculationGroupApi apiInstance = new CalculationGroupApi();
Integer calculationGroupId = 56; // Integer | 
try {
    Object result = apiInstance.calculationGroupDeleteCalculationGroup(calculationGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CalculationGroupApi#calculationGroupDeleteCalculationGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calculationGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="calculationGroupExistsCalculationGroup"></a>
# **calculationGroupExistsCalculationGroup**
> Object calculationGroupExistsCalculationGroup(calculationGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CalculationGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CalculationGroupApi apiInstance = new CalculationGroupApi();
Integer calculationGroupId = 56; // Integer | 
try {
    Object result = apiInstance.calculationGroupExistsCalculationGroup(calculationGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CalculationGroupApi#calculationGroupExistsCalculationGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calculationGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="calculationGroupGetCalculationGroup"></a>
# **calculationGroupGetCalculationGroup**
> Object calculationGroupGetCalculationGroup(calculationGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CalculationGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CalculationGroupApi apiInstance = new CalculationGroupApi();
Integer calculationGroupId = 56; // Integer | 
try {
    Object result = apiInstance.calculationGroupGetCalculationGroup(calculationGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CalculationGroupApi#calculationGroupGetCalculationGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calculationGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="calculationGroupGetCalculationGroups"></a>
# **calculationGroupGetCalculationGroups**
> Object calculationGroupGetCalculationGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CalculationGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CalculationGroupApi apiInstance = new CalculationGroupApi();
GetFilteredCalculationGroupQuery query = new GetFilteredCalculationGroupQuery(); // GetFilteredCalculationGroupQuery | 
try {
    Object result = apiInstance.calculationGroupGetCalculationGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CalculationGroupApi#calculationGroupGetCalculationGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredCalculationGroupQuery**](GetFilteredCalculationGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="calculationGroupUpdateCalculationGroup"></a>
# **calculationGroupUpdateCalculationGroup**
> Object calculationGroupUpdateCalculationGroup(calculationGroupId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CalculationGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CalculationGroupApi apiInstance = new CalculationGroupApi();
Integer calculationGroupId = 56; // Integer | 
CalculationGroupDto dto = new CalculationGroupDto(); // CalculationGroupDto | 
try {
    Object result = apiInstance.calculationGroupUpdateCalculationGroup(calculationGroupId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CalculationGroupApi#calculationGroupUpdateCalculationGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calculationGroupId** | **Integer**|  |
 **dto** | [**CalculationGroupDto**](CalculationGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

