# CategorialApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**categorialCreateCategorial**](CategorialApi.md#categorialCreateCategorial) | **POST** /Categorial | 
[**categorialDeleteCategorial**](CategorialApi.md#categorialDeleteCategorial) | **DELETE** /Categorial/{number} | 
[**categorialExistsCategorial**](CategorialApi.md#categorialExistsCategorial) | **GET** /Categorial/exists/{number} | 
[**categorialGetCategorial**](CategorialApi.md#categorialGetCategorial) | **GET** /Categorial/{number} | 
[**categorialGetCategorials**](CategorialApi.md#categorialGetCategorials) | **POST** /Categorial/search | 
[**categorialUpdateCategorial**](CategorialApi.md#categorialUpdateCategorial) | **PUT** /Categorial/{number} | 


<a name="categorialCreateCategorial"></a>
# **categorialCreateCategorial**
> Object categorialCreateCategorial(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CategorialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CategorialApi apiInstance = new CategorialApi();
CategorialDto dto = new CategorialDto(); // CategorialDto | 
try {
    Object result = apiInstance.categorialCreateCategorial(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CategorialApi#categorialCreateCategorial");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**CategorialDto**](CategorialDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="categorialDeleteCategorial"></a>
# **categorialDeleteCategorial**
> Object categorialDeleteCategorial(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CategorialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CategorialApi apiInstance = new CategorialApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.categorialDeleteCategorial(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CategorialApi#categorialDeleteCategorial");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="categorialExistsCategorial"></a>
# **categorialExistsCategorial**
> Object categorialExistsCategorial(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CategorialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CategorialApi apiInstance = new CategorialApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.categorialExistsCategorial(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CategorialApi#categorialExistsCategorial");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="categorialGetCategorial"></a>
# **categorialGetCategorial**
> Object categorialGetCategorial(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CategorialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CategorialApi apiInstance = new CategorialApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.categorialGetCategorial(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CategorialApi#categorialGetCategorial");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="categorialGetCategorials"></a>
# **categorialGetCategorials**
> Object categorialGetCategorials(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CategorialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CategorialApi apiInstance = new CategorialApi();
GetFilteredCategorialQuery query = new GetFilteredCategorialQuery(); // GetFilteredCategorialQuery | 
try {
    Object result = apiInstance.categorialGetCategorials(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CategorialApi#categorialGetCategorials");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredCategorialQuery**](GetFilteredCategorialQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="categorialUpdateCategorial"></a>
# **categorialUpdateCategorial**
> Object categorialUpdateCategorial(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CategorialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CategorialApi apiInstance = new CategorialApi();
Integer number = 56; // Integer | 
CategorialDto dto = new CategorialDto(); // CategorialDto | 
try {
    Object result = apiInstance.categorialUpdateCategorial(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CategorialApi#categorialUpdateCategorial");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**CategorialDto**](CategorialDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

