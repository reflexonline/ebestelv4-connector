
# ShortageReasonDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shortageReasonId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**blockade** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



