# SaleArticleCustomerAgreementApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleArticleCustomerAgreementCreateSaleArticleCustomerAgreement**](SaleArticleCustomerAgreementApi.md#saleArticleCustomerAgreementCreateSaleArticleCustomerAgreement) | **POST** /SaleArticleCustomerAgreement | 
[**saleArticleCustomerAgreementDeleteSaleArticleCustomerAgreement**](SaleArticleCustomerAgreementApi.md#saleArticleCustomerAgreementDeleteSaleArticleCustomerAgreement) | **DELETE** /SaleArticleCustomerAgreement/{customerId}/{articleId} | 
[**saleArticleCustomerAgreementExistsSaleArticleCustomerAgreement**](SaleArticleCustomerAgreementApi.md#saleArticleCustomerAgreementExistsSaleArticleCustomerAgreement) | **GET** /SaleArticleCustomerAgreement/exists/{customerId}/{articleId} | 
[**saleArticleCustomerAgreementGetSaleArticleCustomerAgreement**](SaleArticleCustomerAgreementApi.md#saleArticleCustomerAgreementGetSaleArticleCustomerAgreement) | **GET** /SaleArticleCustomerAgreement/{customerId}/{articleId} | 
[**saleArticleCustomerAgreementGetSaleArticleCustomerAgreements**](SaleArticleCustomerAgreementApi.md#saleArticleCustomerAgreementGetSaleArticleCustomerAgreements) | **POST** /SaleArticleCustomerAgreement/search | 
[**saleArticleCustomerAgreementUpdateSaleArticleCustomerAgreement**](SaleArticleCustomerAgreementApi.md#saleArticleCustomerAgreementUpdateSaleArticleCustomerAgreement) | **PUT** /SaleArticleCustomerAgreement/{customerId}/{articleId} | 


<a name="saleArticleCustomerAgreementCreateSaleArticleCustomerAgreement"></a>
# **saleArticleCustomerAgreementCreateSaleArticleCustomerAgreement**
> Object saleArticleCustomerAgreementCreateSaleArticleCustomerAgreement(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleCustomerAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleCustomerAgreementApi apiInstance = new SaleArticleCustomerAgreementApi();
SaleArticleCustomerAgreementDto dto = new SaleArticleCustomerAgreementDto(); // SaleArticleCustomerAgreementDto | 
try {
    Object result = apiInstance.saleArticleCustomerAgreementCreateSaleArticleCustomerAgreement(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleCustomerAgreementApi#saleArticleCustomerAgreementCreateSaleArticleCustomerAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SaleArticleCustomerAgreementDto**](SaleArticleCustomerAgreementDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleCustomerAgreementDeleteSaleArticleCustomerAgreement"></a>
# **saleArticleCustomerAgreementDeleteSaleArticleCustomerAgreement**
> Object saleArticleCustomerAgreementDeleteSaleArticleCustomerAgreement(customerId, articleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleCustomerAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleCustomerAgreementApi apiInstance = new SaleArticleCustomerAgreementApi();
Integer customerId = 56; // Integer | 
Integer articleId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleCustomerAgreementDeleteSaleArticleCustomerAgreement(customerId, articleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleCustomerAgreementApi#saleArticleCustomerAgreementDeleteSaleArticleCustomerAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **articleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleCustomerAgreementExistsSaleArticleCustomerAgreement"></a>
# **saleArticleCustomerAgreementExistsSaleArticleCustomerAgreement**
> Object saleArticleCustomerAgreementExistsSaleArticleCustomerAgreement(customerId, articleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleCustomerAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleCustomerAgreementApi apiInstance = new SaleArticleCustomerAgreementApi();
Integer customerId = 56; // Integer | 
Integer articleId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleCustomerAgreementExistsSaleArticleCustomerAgreement(customerId, articleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleCustomerAgreementApi#saleArticleCustomerAgreementExistsSaleArticleCustomerAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **articleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleCustomerAgreementGetSaleArticleCustomerAgreement"></a>
# **saleArticleCustomerAgreementGetSaleArticleCustomerAgreement**
> Object saleArticleCustomerAgreementGetSaleArticleCustomerAgreement(customerId, articleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleCustomerAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleCustomerAgreementApi apiInstance = new SaleArticleCustomerAgreementApi();
Integer customerId = 56; // Integer | 
Integer articleId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleCustomerAgreementGetSaleArticleCustomerAgreement(customerId, articleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleCustomerAgreementApi#saleArticleCustomerAgreementGetSaleArticleCustomerAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **articleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleCustomerAgreementGetSaleArticleCustomerAgreements"></a>
# **saleArticleCustomerAgreementGetSaleArticleCustomerAgreements**
> Object saleArticleCustomerAgreementGetSaleArticleCustomerAgreements(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleCustomerAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleCustomerAgreementApi apiInstance = new SaleArticleCustomerAgreementApi();
GetFilteredSaleArticleCustomerAgreementQuery query = new GetFilteredSaleArticleCustomerAgreementQuery(); // GetFilteredSaleArticleCustomerAgreementQuery | 
try {
    Object result = apiInstance.saleArticleCustomerAgreementGetSaleArticleCustomerAgreements(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleCustomerAgreementApi#saleArticleCustomerAgreementGetSaleArticleCustomerAgreements");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSaleArticleCustomerAgreementQuery**](GetFilteredSaleArticleCustomerAgreementQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleCustomerAgreementUpdateSaleArticleCustomerAgreement"></a>
# **saleArticleCustomerAgreementUpdateSaleArticleCustomerAgreement**
> Object saleArticleCustomerAgreementUpdateSaleArticleCustomerAgreement(customerId, articleId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleCustomerAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleCustomerAgreementApi apiInstance = new SaleArticleCustomerAgreementApi();
Integer customerId = 56; // Integer | 
Integer articleId = 56; // Integer | 
SaleArticleCustomerAgreementDto dto = new SaleArticleCustomerAgreementDto(); // SaleArticleCustomerAgreementDto | 
try {
    Object result = apiInstance.saleArticleCustomerAgreementUpdateSaleArticleCustomerAgreement(customerId, articleId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleCustomerAgreementApi#saleArticleCustomerAgreementUpdateSaleArticleCustomerAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **articleId** | **Integer**|  |
 **dto** | [**SaleArticleCustomerAgreementDto**](SaleArticleCustomerAgreementDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

