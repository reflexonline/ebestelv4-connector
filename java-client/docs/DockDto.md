
# DockDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dockId** | **Integer** |  | 
**nameShort** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**multiBranch** | **Integer** |  |  [optional]
**isBlocked** | **Boolean** |  |  [optional]
**contactName** | **String** |  |  [optional]
**phoneNumber** | **String** |  |  [optional]
**faxNumber** | **String** |  |  [optional]
**addressLine1** | **String** |  |  [optional]
**addressLine2** | **String** |  |  [optional]
**postalCode** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**isoCountryCode** | **String** |  |  [optional]
**country** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



