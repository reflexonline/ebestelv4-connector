
# GetFilteredPicAPictureGroupQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**picAPictureGroupIdMinValue** | **Integer** |  |  [optional]
**picAPictureGroupIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



