# RelationPostalAddressApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**relationPostalAddressExistsRelationPostalAddress**](RelationPostalAddressApi.md#relationPostalAddressExistsRelationPostalAddress) | **GET** /RelationPostalAddress/exists/{number} | 
[**relationPostalAddressGetRelationPostalAddress**](RelationPostalAddressApi.md#relationPostalAddressGetRelationPostalAddress) | **GET** /RelationPostalAddress/{number} | 
[**relationPostalAddressGetRelationPostalAddresses**](RelationPostalAddressApi.md#relationPostalAddressGetRelationPostalAddresses) | **POST** /RelationPostalAddress/search | 


<a name="relationPostalAddressExistsRelationPostalAddress"></a>
# **relationPostalAddressExistsRelationPostalAddress**
> Object relationPostalAddressExistsRelationPostalAddress(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RelationPostalAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RelationPostalAddressApi apiInstance = new RelationPostalAddressApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.relationPostalAddressExistsRelationPostalAddress(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationPostalAddressApi#relationPostalAddressExistsRelationPostalAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="relationPostalAddressGetRelationPostalAddress"></a>
# **relationPostalAddressGetRelationPostalAddress**
> Object relationPostalAddressGetRelationPostalAddress(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RelationPostalAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RelationPostalAddressApi apiInstance = new RelationPostalAddressApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.relationPostalAddressGetRelationPostalAddress(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationPostalAddressApi#relationPostalAddressGetRelationPostalAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="relationPostalAddressGetRelationPostalAddresses"></a>
# **relationPostalAddressGetRelationPostalAddresses**
> Object relationPostalAddressGetRelationPostalAddresses(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RelationPostalAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RelationPostalAddressApi apiInstance = new RelationPostalAddressApi();
GetFilteredRelationPostalAddressQuery query = new GetFilteredRelationPostalAddressQuery(); // GetFilteredRelationPostalAddressQuery | 
try {
    Object result = apiInstance.relationPostalAddressGetRelationPostalAddresses(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationPostalAddressApi#relationPostalAddressGetRelationPostalAddresses");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredRelationPostalAddressQuery**](GetFilteredRelationPostalAddressQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

