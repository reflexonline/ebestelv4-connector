
# GetFilteredConsumerGroupQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**consumerGroupIdMinValue** | **Integer** |  |  [optional]
**consumerGroupIdMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]



