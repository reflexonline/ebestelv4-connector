
# SaleOrderStockAvailabilityDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**saleArticle** | **Integer** |  | 
**quantity** | **Double** |  |  [optional]
**weight** | **Double** |  |  [optional]



