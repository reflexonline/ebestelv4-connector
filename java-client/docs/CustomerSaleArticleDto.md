
# CustomerSaleArticleDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **Integer** |  | 
**articleId** | **Integer** |  | 
**userDefinedCode1** | **String** |  |  [optional]
**alternativeBestBeforeDays** | **Integer** |  |  [optional]
**packedByPieces** | **Float** |  |  [optional]
**packedByWeight** | **Float** |  |  [optional]
**adviesPrijsOld** | **Float** |  |  [optional]
**labelNumber** | **Integer** |  |  [optional]
**plafondPrijsOld** | **Float** |  |  [optional]
**adviesPrijsAktieOld** | **Float** |  |  [optional]
**potionWeight** | **Float** |  |  [optional]
**name** | **String** |  |  [optional]
**minimalRequiredBestBeforeDays** | **Integer** |  |  [optional]
**maximumPocketPCMutationSize** | **Double** |  |  [optional]
**administrationBranch** | **Integer** |  |  [optional]
**owhDeliveryType** | **Integer** |  |  [optional]
**userDefinedCode2** | **String** |  |  [optional]
**unassignedField3** | **Double** |  |  [optional]
**packingLabelNumber** | **Integer** |  |  [optional]
**excludeFromTurnoverBonus** | **Integer** |  |  [optional]
**quantityPerBoxOnPallet** | **Integer** |  |  [optional]
**numberOfBoxesPerPalletLayer** | **Integer** |  |  [optional]
**numberOfPalletLayer** | **Integer** |  |  [optional]
**paymentPeriod** | **Integer** |  |  [optional]
**packingArticle1** | **Integer** |  |  [optional]
**packingArticle2** | **Integer** |  |  [optional]
**palletLabelNumber** | **Integer** |  |  [optional]
**weightCalculated** | **Integer** |  |  [optional]
**bestBeforeDateDeterminationMethod** | [**BestBeforeDateDeterminationMethodEnum**](#BestBeforeDateDeterminationMethodEnum) |  |  [optional]
**bestBeforeDateCheckUpMethod** | [**BestBeforeDateCheckUpMethodEnum**](#BestBeforeDateCheckUpMethodEnum) |  |  [optional]
**maximumPrice** | **Double** |  |  [optional]
**consumerPrice** | **Double** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="BestBeforeDateDeterminationMethodEnum"></a>
## Enum: BestBeforeDateDeterminationMethodEnum
Name | Value
---- | -----
DEFAULT | &quot;Default&quot;
NONE | &quot;None&quot;
DATEREGISTRATION | &quot;DateRegistration&quot;
DATELOADING | &quot;DateLoading&quot;
DATEDELIVERY | &quot;DateDelivery&quot;
REGISTEREDBATCH | &quot;RegisteredBatch&quot;


<a name="BestBeforeDateCheckUpMethodEnum"></a>
## Enum: BestBeforeDateCheckUpMethodEnum
Name | Value
---- | -----
DEFAULT | &quot;Default&quot;
NONE | &quot;None&quot;
ASKUSER | &quot;AskUser&quot;
WARRANTYDATE | &quot;WarrantyDate&quot;
ASKFORDATEWARRANTYINCLUDED | &quot;AskForDateWarrantyIncluded&quot;
ASKFORDATEHOLDPERARTICLE | &quot;AskForDateHoldPerArticle&quot;



