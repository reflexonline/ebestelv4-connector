# MarketSectorApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**marketSectorCreateMarketSector**](MarketSectorApi.md#marketSectorCreateMarketSector) | **POST** /MarketSector | 
[**marketSectorDeleteMarketSector**](MarketSectorApi.md#marketSectorDeleteMarketSector) | **DELETE** /MarketSector/{marketSectorId} | 
[**marketSectorExistsMarketSector**](MarketSectorApi.md#marketSectorExistsMarketSector) | **GET** /MarketSector/exists/{marketSectorId} | 
[**marketSectorGetMarketSector**](MarketSectorApi.md#marketSectorGetMarketSector) | **GET** /MarketSector/{marketSectorId} | 
[**marketSectorGetMarketSectors**](MarketSectorApi.md#marketSectorGetMarketSectors) | **POST** /MarketSector/search | 
[**marketSectorUpdateMarketSector**](MarketSectorApi.md#marketSectorUpdateMarketSector) | **PUT** /MarketSector/{marketSectorId} | 


<a name="marketSectorCreateMarketSector"></a>
# **marketSectorCreateMarketSector**
> Object marketSectorCreateMarketSector(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MarketSectorApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MarketSectorApi apiInstance = new MarketSectorApi();
MarketSectorDto dto = new MarketSectorDto(); // MarketSectorDto | 
try {
    Object result = apiInstance.marketSectorCreateMarketSector(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MarketSectorApi#marketSectorCreateMarketSector");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**MarketSectorDto**](MarketSectorDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="marketSectorDeleteMarketSector"></a>
# **marketSectorDeleteMarketSector**
> Object marketSectorDeleteMarketSector(marketSectorId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MarketSectorApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MarketSectorApi apiInstance = new MarketSectorApi();
Integer marketSectorId = 56; // Integer | 
try {
    Object result = apiInstance.marketSectorDeleteMarketSector(marketSectorId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MarketSectorApi#marketSectorDeleteMarketSector");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **marketSectorId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="marketSectorExistsMarketSector"></a>
# **marketSectorExistsMarketSector**
> Object marketSectorExistsMarketSector(marketSectorId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MarketSectorApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MarketSectorApi apiInstance = new MarketSectorApi();
Integer marketSectorId = 56; // Integer | 
try {
    Object result = apiInstance.marketSectorExistsMarketSector(marketSectorId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MarketSectorApi#marketSectorExistsMarketSector");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **marketSectorId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="marketSectorGetMarketSector"></a>
# **marketSectorGetMarketSector**
> Object marketSectorGetMarketSector(marketSectorId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MarketSectorApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MarketSectorApi apiInstance = new MarketSectorApi();
Integer marketSectorId = 56; // Integer | 
try {
    Object result = apiInstance.marketSectorGetMarketSector(marketSectorId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MarketSectorApi#marketSectorGetMarketSector");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **marketSectorId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="marketSectorGetMarketSectors"></a>
# **marketSectorGetMarketSectors**
> Object marketSectorGetMarketSectors(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MarketSectorApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MarketSectorApi apiInstance = new MarketSectorApi();
GetFilteredMarketSectorQuery query = new GetFilteredMarketSectorQuery(); // GetFilteredMarketSectorQuery | 
try {
    Object result = apiInstance.marketSectorGetMarketSectors(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MarketSectorApi#marketSectorGetMarketSectors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredMarketSectorQuery**](GetFilteredMarketSectorQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="marketSectorUpdateMarketSector"></a>
# **marketSectorUpdateMarketSector**
> Object marketSectorUpdateMarketSector(marketSectorId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MarketSectorApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MarketSectorApi apiInstance = new MarketSectorApi();
Integer marketSectorId = 56; // Integer | 
MarketSectorDto dto = new MarketSectorDto(); // MarketSectorDto | 
try {
    Object result = apiInstance.marketSectorUpdateMarketSector(marketSectorId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MarketSectorApi#marketSectorUpdateMarketSector");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **marketSectorId** | **Integer**|  |
 **dto** | [**MarketSectorDto**](MarketSectorDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

