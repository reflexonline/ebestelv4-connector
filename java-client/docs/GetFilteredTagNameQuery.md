
# GetFilteredTagNameQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tagIdMinValue** | **Integer** |  |  [optional]
**tagIdMaxValue** | **Integer** |  |  [optional]
**nameShortMinValue** | **String** |  |  [optional]
**nameShortMaxValue** | **String** |  |  [optional]



