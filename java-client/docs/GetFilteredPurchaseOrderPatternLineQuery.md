
# GetFilteredPurchaseOrderPatternLineQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purchaseArticleIdMinValue** | **Integer** |  |  [optional]
**purchaseArticleIdMaxValue** | **Integer** |  |  [optional]
**supplierIdMinValue** | **Integer** |  |  [optional]
**supplierIdMaxValue** | **Integer** |  |  [optional]
**lineMinValue** | **Integer** |  |  [optional]
**lineMaxValue** | **Integer** |  |  [optional]
**listMinValue** | **Integer** |  |  [optional]
**listMaxValue** | **Integer** |  |  [optional]



