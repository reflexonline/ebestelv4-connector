
# GetFilteredProductionPlanningFilterQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productionPlanningFilterIdMinValue** | **Integer** |  |  [optional]
**productionPlanningFilterIdMaxValue** | **Integer** |  |  [optional]
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]



