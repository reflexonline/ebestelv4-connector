
# PurchaseOrderFormulaDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**description** | **String** |  |  [optional]
**formuleDeel1** | **String** |  |  [optional]
**formuleDeel2** | **String** |  |  [optional]
**formuleDeel3** | **String** |  |  [optional]
**formuleDeel4** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



