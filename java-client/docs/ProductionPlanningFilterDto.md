
# ProductionPlanningFilterDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**productionPlanningFilterId** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**labelNumber** | **Integer** |  |  [optional]
**labelPort** | **String** |  |  [optional]
**labelQuantity** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



