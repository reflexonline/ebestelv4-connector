
# GetFilteredPurchaseArticleSupplierAgreementQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierIdMinValue** | **Integer** |  |  [optional]
**supplierIdMaxValue** | **Integer** |  |  [optional]
**purchaseArticleIdMinValue** | **Integer** |  |  [optional]
**purchaseArticleIdMaxValue** | **Integer** |  |  [optional]



