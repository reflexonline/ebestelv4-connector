# SaleArticleSubGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleArticleSubGroupCreateSaleArticleSubGroup**](SaleArticleSubGroupApi.md#saleArticleSubGroupCreateSaleArticleSubGroup) | **POST** /SaleArticleSubGroup | 
[**saleArticleSubGroupDeleteSaleArticleSubGroup**](SaleArticleSubGroupApi.md#saleArticleSubGroupDeleteSaleArticleSubGroup) | **DELETE** /SaleArticleSubGroup/{subgroupId} | 
[**saleArticleSubGroupExistsSaleArticleSubGroup**](SaleArticleSubGroupApi.md#saleArticleSubGroupExistsSaleArticleSubGroup) | **GET** /SaleArticleSubGroup/exists/{subgroupId} | 
[**saleArticleSubGroupGetSaleArticleSubGroup**](SaleArticleSubGroupApi.md#saleArticleSubGroupGetSaleArticleSubGroup) | **GET** /SaleArticleSubGroup/{subgroupId} | 
[**saleArticleSubGroupGetSaleArticleSubGroups**](SaleArticleSubGroupApi.md#saleArticleSubGroupGetSaleArticleSubGroups) | **POST** /SaleArticleSubGroup/search | 
[**saleArticleSubGroupUpdateSaleArticleSubGroup**](SaleArticleSubGroupApi.md#saleArticleSubGroupUpdateSaleArticleSubGroup) | **PUT** /SaleArticleSubGroup/{subgroupId} | 


<a name="saleArticleSubGroupCreateSaleArticleSubGroup"></a>
# **saleArticleSubGroupCreateSaleArticleSubGroup**
> Object saleArticleSubGroupCreateSaleArticleSubGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleSubGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleSubGroupApi apiInstance = new SaleArticleSubGroupApi();
SaleArticleSubGroupDto dto = new SaleArticleSubGroupDto(); // SaleArticleSubGroupDto | 
try {
    Object result = apiInstance.saleArticleSubGroupCreateSaleArticleSubGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleSubGroupApi#saleArticleSubGroupCreateSaleArticleSubGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SaleArticleSubGroupDto**](SaleArticleSubGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleSubGroupDeleteSaleArticleSubGroup"></a>
# **saleArticleSubGroupDeleteSaleArticleSubGroup**
> Object saleArticleSubGroupDeleteSaleArticleSubGroup(subgroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleSubGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleSubGroupApi apiInstance = new SaleArticleSubGroupApi();
Integer subgroupId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleSubGroupDeleteSaleArticleSubGroup(subgroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleSubGroupApi#saleArticleSubGroupDeleteSaleArticleSubGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subgroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleSubGroupExistsSaleArticleSubGroup"></a>
# **saleArticleSubGroupExistsSaleArticleSubGroup**
> Object saleArticleSubGroupExistsSaleArticleSubGroup(subgroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleSubGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleSubGroupApi apiInstance = new SaleArticleSubGroupApi();
Integer subgroupId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleSubGroupExistsSaleArticleSubGroup(subgroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleSubGroupApi#saleArticleSubGroupExistsSaleArticleSubGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subgroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleSubGroupGetSaleArticleSubGroup"></a>
# **saleArticleSubGroupGetSaleArticleSubGroup**
> Object saleArticleSubGroupGetSaleArticleSubGroup(subgroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleSubGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleSubGroupApi apiInstance = new SaleArticleSubGroupApi();
Integer subgroupId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleSubGroupGetSaleArticleSubGroup(subgroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleSubGroupApi#saleArticleSubGroupGetSaleArticleSubGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subgroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleSubGroupGetSaleArticleSubGroups"></a>
# **saleArticleSubGroupGetSaleArticleSubGroups**
> Object saleArticleSubGroupGetSaleArticleSubGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleSubGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleSubGroupApi apiInstance = new SaleArticleSubGroupApi();
GetFilteredSaleArticleSubGroupQuery query = new GetFilteredSaleArticleSubGroupQuery(); // GetFilteredSaleArticleSubGroupQuery | 
try {
    Object result = apiInstance.saleArticleSubGroupGetSaleArticleSubGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleSubGroupApi#saleArticleSubGroupGetSaleArticleSubGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSaleArticleSubGroupQuery**](GetFilteredSaleArticleSubGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleSubGroupUpdateSaleArticleSubGroup"></a>
# **saleArticleSubGroupUpdateSaleArticleSubGroup**
> Object saleArticleSubGroupUpdateSaleArticleSubGroup(subgroupId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleSubGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleSubGroupApi apiInstance = new SaleArticleSubGroupApi();
Integer subgroupId = 56; // Integer | 
SaleArticleSubGroupDto dto = new SaleArticleSubGroupDto(); // SaleArticleSubGroupDto | 
try {
    Object result = apiInstance.saleArticleSubGroupUpdateSaleArticleSubGroup(subgroupId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleSubGroupApi#saleArticleSubGroupUpdateSaleArticleSubGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subgroupId** | **Integer**|  |
 **dto** | [**SaleArticleSubGroupDto**](SaleArticleSubGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

