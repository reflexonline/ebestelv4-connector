
# GetFilteredWssDepartmentQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]
**fromProductionGroupIdMinValue** | **Integer** |  |  [optional]
**fromProductionGroupIdMaxValue** | **Integer** |  |  [optional]
**untilProductionGroupIdMinValue** | **Integer** |  |  [optional]
**untilProductionGroupIdMaxValue** | **Integer** |  |  [optional]
**replenishmentLocationIdMinValue** | **Integer** |  |  [optional]
**replenishmentLocationIdMaxValue** | **Integer** |  |  [optional]



