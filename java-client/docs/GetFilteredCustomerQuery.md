
# GetFilteredCustomerQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerIdMinValue** | **Integer** |  |  [optional]
**customerIdMaxValue** | **Integer** |  |  [optional]
**postalAddressSearchNameMinValue** | **String** |  |  [optional]
**postalAddressSearchNameMaxValue** | **String** |  |  [optional]
**postalAddressPostalCodeMinValue** | **String** |  |  [optional]
**postalAddressPostalCodeMaxValue** | **String** |  |  [optional]
**postalAddressCityMinValue** | **String** |  |  [optional]
**postalAddressCityMaxValue** | **String** |  |  [optional]
**invoiceCustomerIdMinValue** | **Integer** |  |  [optional]
**invoiceCustomerIdMaxValue** | **Integer** |  |  [optional]
**priceListIdMinValue** | **Integer** |  |  [optional]
**priceListIdMaxValue** | **Integer** |  |  [optional]
**currencyCodeIdMinValue** | **Integer** |  |  [optional]
**currencyCodeIdMaxValue** | **Integer** |  |  [optional]
**defaultRouteIdMinValue** | **Integer** |  |  [optional]
**defaultRouteIdMaxValue** | **Integer** |  |  [optional]
**deliveryAddressSearchNameMinValue** | **String** |  |  [optional]
**deliveryAddressSearchNameMaxValue** | **String** |  |  [optional]
**searchDeliveryAddressPostalCodeMinValue** | **String** |  |  [optional]
**searchDeliveryAddressPostalCodeMaxValue** | **String** |  |  [optional]
**searchDeliveryAddressCityMinValue** | **String** |  |  [optional]
**searchDeliveryAddressCityMaxValue** | **String** |  |  [optional]
**languageIdMinValue** | **Integer** |  |  [optional]
**languageIdMaxValue** | **Integer** |  |  [optional]
**bestelpatroonMinValue** | [**BestelpatroonMinValueEnum**](#BestelpatroonMinValueEnum) |  |  [optional]
**bestelpatroonMaxValue** | [**BestelpatroonMaxValueEnum**](#BestelpatroonMaxValueEnum) |  |  [optional]
**saleInvoiceGroupHeaderIdMinValue** | **Integer** |  |  [optional]
**saleInvoiceGroupHeaderIdMaxValue** | **Integer** |  |  [optional]
**eanAdresCodeMinValue** | **String** |  |  [optional]
**eanAdresCodeMaxValue** | **String** |  |  [optional]
**representativeIdMinValue** | **Integer** |  |  [optional]
**representativeIdMaxValue** | **Integer** |  |  [optional]
**organizationIdMinValue** | **Integer** |  |  [optional]
**organizationIdMaxValue** | **Integer** |  |  [optional]
**marketSectorIdMinValue** | **Integer** |  |  [optional]
**marketSectorIdMaxValue** | **Integer** |  |  [optional]
**callingUserIdMinValue** | **Integer** |  |  [optional]
**callingUserIdMaxValue** | **Integer** |  |  [optional]
**deliveryMethodIdMinValue** | **Integer** |  |  [optional]
**deliveryMethodIdMaxValue** | **Integer** |  |  [optional]
**discountGroupIdMinValue** | **Integer** |  |  [optional]
**discountGroupIdMaxValue** | **Integer** |  |  [optional]
**loadAddressIdMinValue** | **Integer** |  |  [optional]
**loadAddressIdMaxValue** | **Integer** |  |  [optional]
**freshTransporterIdMinValue** | **Integer** |  |  [optional]
**freshTransporterIdMaxValue** | **Integer** |  |  [optional]
**stockLocationFromIdMinValue** | **Integer** |  |  [optional]
**stockLocationFromIdMaxValue** | **Integer** |  |  [optional]
**bestelpatroondebMinValue** | **Integer** |  |  [optional]
**bestelpatroondebMaxValue** | **Integer** |  |  [optional]
**includeInAnpopsparenMinValue** | **Boolean** |  |  [optional]
**includeInAnpopsparenMaxValue** | **Boolean** |  |  [optional]
**subOrganizationIdMinValue** | **Integer** |  |  [optional]
**subOrganizationIdMaxValue** | **Integer** |  |  [optional]
**transporter2IdMinValue** | **Integer** |  |  [optional]
**transporter2IdMaxValue** | **Integer** |  |  [optional]
**multiBranchIdMinValue** | **Integer** |  |  [optional]
**multiBranchIdMaxValue** | **Integer** |  |  [optional]
**deviatingPackingCustomerIdMinValue** | **Integer** |  |  [optional]
**deviatingPackingCustomerIdMaxValue** | **Integer** |  |  [optional]
**defaultSaleInvoiceStatusIdMinValue** | **Integer** |  |  [optional]
**defaultSaleInvoiceStatusIdMaxValue** | **Integer** |  |  [optional]
**exportGroupIdMinValue** | **Integer** |  |  [optional]
**exportGroupIdMaxValue** | **Integer** |  |  [optional]
**stockLocationToIdMinValue** | **Integer** |  |  [optional]
**stockLocationToIdMaxValue** | **Integer** |  |  [optional]
**dockIdMinValue** | **Integer** |  |  [optional]
**dockIdMaxValue** | **Integer** |  |  [optional]


<a name="BestelpatroonMinValueEnum"></a>
## Enum: BestelpatroonMinValueEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
ON | &quot;On&quot;
AUTOMATIC | &quot;Automatic&quot;
SALEORDERPATTERNCUSTOMER | &quot;SaleOrderPatternCustomer&quot;


<a name="BestelpatroonMaxValueEnum"></a>
## Enum: BestelpatroonMaxValueEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
ON | &quot;On&quot;
AUTOMATIC | &quot;Automatic&quot;
SALEORDERPATTERNCUSTOMER | &quot;SaleOrderPatternCustomer&quot;



