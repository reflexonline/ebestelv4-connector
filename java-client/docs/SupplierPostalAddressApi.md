# SupplierPostalAddressApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**supplierPostalAddressExistsSupplierPostalAddress**](SupplierPostalAddressApi.md#supplierPostalAddressExistsSupplierPostalAddress) | **GET** /SupplierPostalAddress/exists/{supplierId} | 
[**supplierPostalAddressGetSupplierPostalAddress**](SupplierPostalAddressApi.md#supplierPostalAddressGetSupplierPostalAddress) | **GET** /SupplierPostalAddress/{supplierId} | 
[**supplierPostalAddressGetSupplierPostalAddresses**](SupplierPostalAddressApi.md#supplierPostalAddressGetSupplierPostalAddresses) | **POST** /SupplierPostalAddress/search | 


<a name="supplierPostalAddressExistsSupplierPostalAddress"></a>
# **supplierPostalAddressExistsSupplierPostalAddress**
> Object supplierPostalAddressExistsSupplierPostalAddress(supplierId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierPostalAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierPostalAddressApi apiInstance = new SupplierPostalAddressApi();
Integer supplierId = 56; // Integer | 
try {
    Object result = apiInstance.supplierPostalAddressExistsSupplierPostalAddress(supplierId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierPostalAddressApi#supplierPostalAddressExistsSupplierPostalAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="supplierPostalAddressGetSupplierPostalAddress"></a>
# **supplierPostalAddressGetSupplierPostalAddress**
> Object supplierPostalAddressGetSupplierPostalAddress(supplierId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierPostalAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierPostalAddressApi apiInstance = new SupplierPostalAddressApi();
Integer supplierId = 56; // Integer | 
try {
    Object result = apiInstance.supplierPostalAddressGetSupplierPostalAddress(supplierId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierPostalAddressApi#supplierPostalAddressGetSupplierPostalAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="supplierPostalAddressGetSupplierPostalAddresses"></a>
# **supplierPostalAddressGetSupplierPostalAddresses**
> Object supplierPostalAddressGetSupplierPostalAddresses(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierPostalAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierPostalAddressApi apiInstance = new SupplierPostalAddressApi();
GetFilteredSupplierPostalAddressQuery query = new GetFilteredSupplierPostalAddressQuery(); // GetFilteredSupplierPostalAddressQuery | 
try {
    Object result = apiInstance.supplierPostalAddressGetSupplierPostalAddresses(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierPostalAddressApi#supplierPostalAddressGetSupplierPostalAddresses");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSupplierPostalAddressQuery**](GetFilteredSupplierPostalAddressQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

