# SaleGroupPriceMutationApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleGroupPriceMutationCreateSaleGroupPriceMutation**](SaleGroupPriceMutationApi.md#saleGroupPriceMutationCreateSaleGroupPriceMutation) | **POST** /SaleGroupPriceMutation | 
[**saleGroupPriceMutationDeleteSaleGroupPriceMutation**](SaleGroupPriceMutationApi.md#saleGroupPriceMutationDeleteSaleGroupPriceMutation) | **DELETE** /SaleGroupPriceMutation/{customerId}/{salesGroupId} | 
[**saleGroupPriceMutationExistsSaleGroupPriceMutation**](SaleGroupPriceMutationApi.md#saleGroupPriceMutationExistsSaleGroupPriceMutation) | **GET** /SaleGroupPriceMutation/exists/{customerId}/{salesGroupId} | 
[**saleGroupPriceMutationGetSaleGroupPriceMutation**](SaleGroupPriceMutationApi.md#saleGroupPriceMutationGetSaleGroupPriceMutation) | **GET** /SaleGroupPriceMutation/{customerId}/{salesGroupId} | 
[**saleGroupPriceMutationGetSaleGroupPriceMutations**](SaleGroupPriceMutationApi.md#saleGroupPriceMutationGetSaleGroupPriceMutations) | **POST** /SaleGroupPriceMutation/search | 
[**saleGroupPriceMutationUpdateSaleGroupPriceMutation**](SaleGroupPriceMutationApi.md#saleGroupPriceMutationUpdateSaleGroupPriceMutation) | **PUT** /SaleGroupPriceMutation/{customerId}/{salesGroupId} | 


<a name="saleGroupPriceMutationCreateSaleGroupPriceMutation"></a>
# **saleGroupPriceMutationCreateSaleGroupPriceMutation**
> Object saleGroupPriceMutationCreateSaleGroupPriceMutation(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleGroupPriceMutationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleGroupPriceMutationApi apiInstance = new SaleGroupPriceMutationApi();
SaleGroupPriceMutationDto dto = new SaleGroupPriceMutationDto(); // SaleGroupPriceMutationDto | 
try {
    Object result = apiInstance.saleGroupPriceMutationCreateSaleGroupPriceMutation(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleGroupPriceMutationApi#saleGroupPriceMutationCreateSaleGroupPriceMutation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SaleGroupPriceMutationDto**](SaleGroupPriceMutationDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleGroupPriceMutationDeleteSaleGroupPriceMutation"></a>
# **saleGroupPriceMutationDeleteSaleGroupPriceMutation**
> Object saleGroupPriceMutationDeleteSaleGroupPriceMutation(customerId, salesGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleGroupPriceMutationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleGroupPriceMutationApi apiInstance = new SaleGroupPriceMutationApi();
Integer customerId = 56; // Integer | 
Integer salesGroupId = 56; // Integer | 
try {
    Object result = apiInstance.saleGroupPriceMutationDeleteSaleGroupPriceMutation(customerId, salesGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleGroupPriceMutationApi#saleGroupPriceMutationDeleteSaleGroupPriceMutation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **salesGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleGroupPriceMutationExistsSaleGroupPriceMutation"></a>
# **saleGroupPriceMutationExistsSaleGroupPriceMutation**
> Object saleGroupPriceMutationExistsSaleGroupPriceMutation(customerId, salesGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleGroupPriceMutationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleGroupPriceMutationApi apiInstance = new SaleGroupPriceMutationApi();
Integer customerId = 56; // Integer | 
Integer salesGroupId = 56; // Integer | 
try {
    Object result = apiInstance.saleGroupPriceMutationExistsSaleGroupPriceMutation(customerId, salesGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleGroupPriceMutationApi#saleGroupPriceMutationExistsSaleGroupPriceMutation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **salesGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleGroupPriceMutationGetSaleGroupPriceMutation"></a>
# **saleGroupPriceMutationGetSaleGroupPriceMutation**
> Object saleGroupPriceMutationGetSaleGroupPriceMutation(customerId, salesGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleGroupPriceMutationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleGroupPriceMutationApi apiInstance = new SaleGroupPriceMutationApi();
Integer customerId = 56; // Integer | 
Integer salesGroupId = 56; // Integer | 
try {
    Object result = apiInstance.saleGroupPriceMutationGetSaleGroupPriceMutation(customerId, salesGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleGroupPriceMutationApi#saleGroupPriceMutationGetSaleGroupPriceMutation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **salesGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleGroupPriceMutationGetSaleGroupPriceMutations"></a>
# **saleGroupPriceMutationGetSaleGroupPriceMutations**
> Object saleGroupPriceMutationGetSaleGroupPriceMutations(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleGroupPriceMutationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleGroupPriceMutationApi apiInstance = new SaleGroupPriceMutationApi();
GetFilteredSaleGroupPriceMutationQuery query = new GetFilteredSaleGroupPriceMutationQuery(); // GetFilteredSaleGroupPriceMutationQuery | 
try {
    Object result = apiInstance.saleGroupPriceMutationGetSaleGroupPriceMutations(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleGroupPriceMutationApi#saleGroupPriceMutationGetSaleGroupPriceMutations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSaleGroupPriceMutationQuery**](GetFilteredSaleGroupPriceMutationQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleGroupPriceMutationUpdateSaleGroupPriceMutation"></a>
# **saleGroupPriceMutationUpdateSaleGroupPriceMutation**
> Object saleGroupPriceMutationUpdateSaleGroupPriceMutation(customerId, salesGroupId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleGroupPriceMutationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleGroupPriceMutationApi apiInstance = new SaleGroupPriceMutationApi();
Integer customerId = 56; // Integer | 
Integer salesGroupId = 56; // Integer | 
SaleGroupPriceMutationDto dto = new SaleGroupPriceMutationDto(); // SaleGroupPriceMutationDto | 
try {
    Object result = apiInstance.saleGroupPriceMutationUpdateSaleGroupPriceMutation(customerId, salesGroupId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleGroupPriceMutationApi#saleGroupPriceMutationUpdateSaleGroupPriceMutation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **salesGroupId** | **Integer**|  |
 **dto** | [**SaleGroupPriceMutationDto**](SaleGroupPriceMutationDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

