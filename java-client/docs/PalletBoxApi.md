# PalletBoxApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**palletBoxCreatePalletBox**](PalletBoxApi.md#palletBoxCreatePalletBox) | **POST** /PalletBox | 
[**palletBoxDeletePalletBox**](PalletBoxApi.md#palletBoxDeletePalletBox) | **DELETE** /PalletBox/{boxId} | 
[**palletBoxExistsPalletBox**](PalletBoxApi.md#palletBoxExistsPalletBox) | **GET** /PalletBox/exists/{boxId} | 
[**palletBoxGetPalletBox**](PalletBoxApi.md#palletBoxGetPalletBox) | **GET** /PalletBox/{boxId} | 
[**palletBoxGetPalletBoxes**](PalletBoxApi.md#palletBoxGetPalletBoxes) | **POST** /PalletBox/search | 
[**palletBoxUpdatePalletBox**](PalletBoxApi.md#palletBoxUpdatePalletBox) | **PUT** /PalletBox/{boxId} | 


<a name="palletBoxCreatePalletBox"></a>
# **palletBoxCreatePalletBox**
> Object palletBoxCreatePalletBox(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PalletBoxApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PalletBoxApi apiInstance = new PalletBoxApi();
PalletBoxDto dto = new PalletBoxDto(); // PalletBoxDto | 
try {
    Object result = apiInstance.palletBoxCreatePalletBox(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PalletBoxApi#palletBoxCreatePalletBox");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PalletBoxDto**](PalletBoxDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="palletBoxDeletePalletBox"></a>
# **palletBoxDeletePalletBox**
> Object palletBoxDeletePalletBox(boxId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PalletBoxApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PalletBoxApi apiInstance = new PalletBoxApi();
Integer boxId = 56; // Integer | 
try {
    Object result = apiInstance.palletBoxDeletePalletBox(boxId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PalletBoxApi#palletBoxDeletePalletBox");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **boxId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="palletBoxExistsPalletBox"></a>
# **palletBoxExistsPalletBox**
> Object palletBoxExistsPalletBox(boxId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PalletBoxApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PalletBoxApi apiInstance = new PalletBoxApi();
Integer boxId = 56; // Integer | 
try {
    Object result = apiInstance.palletBoxExistsPalletBox(boxId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PalletBoxApi#palletBoxExistsPalletBox");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **boxId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="palletBoxGetPalletBox"></a>
# **palletBoxGetPalletBox**
> Object palletBoxGetPalletBox(boxId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PalletBoxApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PalletBoxApi apiInstance = new PalletBoxApi();
Integer boxId = 56; // Integer | 
try {
    Object result = apiInstance.palletBoxGetPalletBox(boxId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PalletBoxApi#palletBoxGetPalletBox");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **boxId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="palletBoxGetPalletBoxes"></a>
# **palletBoxGetPalletBoxes**
> Object palletBoxGetPalletBoxes(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PalletBoxApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PalletBoxApi apiInstance = new PalletBoxApi();
GetFilteredPalletBoxQuery query = new GetFilteredPalletBoxQuery(); // GetFilteredPalletBoxQuery | 
try {
    Object result = apiInstance.palletBoxGetPalletBoxes(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PalletBoxApi#palletBoxGetPalletBoxes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPalletBoxQuery**](GetFilteredPalletBoxQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="palletBoxUpdatePalletBox"></a>
# **palletBoxUpdatePalletBox**
> Object palletBoxUpdatePalletBox(boxId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PalletBoxApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PalletBoxApi apiInstance = new PalletBoxApi();
Integer boxId = 56; // Integer | 
PalletBoxDto dto = new PalletBoxDto(); // PalletBoxDto | 
try {
    Object result = apiInstance.palletBoxUpdatePalletBox(boxId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PalletBoxApi#palletBoxUpdatePalletBox");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **boxId** | **Integer**|  |
 **dto** | [**PalletBoxDto**](PalletBoxDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

