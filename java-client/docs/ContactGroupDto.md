
# ContactGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contactGroupId** | **Integer** |  | 
**userKey** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



