# PurchaseArticleSupplierAgreementApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purchaseArticleSupplierAgreementCreatePurchaseArticleSupplierAgreement**](PurchaseArticleSupplierAgreementApi.md#purchaseArticleSupplierAgreementCreatePurchaseArticleSupplierAgreement) | **POST** /PurchaseArticleSupplierAgreement | 
[**purchaseArticleSupplierAgreementDeletePurchaseArticleSupplierAgreement**](PurchaseArticleSupplierAgreementApi.md#purchaseArticleSupplierAgreementDeletePurchaseArticleSupplierAgreement) | **DELETE** /PurchaseArticleSupplierAgreement/{supplierId}/{purchaseArticleId} | 
[**purchaseArticleSupplierAgreementExistsPurchaseArticleSupplierAgreement**](PurchaseArticleSupplierAgreementApi.md#purchaseArticleSupplierAgreementExistsPurchaseArticleSupplierAgreement) | **GET** /PurchaseArticleSupplierAgreement/exists/{supplierId}/{purchaseArticleId} | 
[**purchaseArticleSupplierAgreementGetPurchaseArticleSupplierAgreement**](PurchaseArticleSupplierAgreementApi.md#purchaseArticleSupplierAgreementGetPurchaseArticleSupplierAgreement) | **GET** /PurchaseArticleSupplierAgreement/{supplierId}/{purchaseArticleId} | 
[**purchaseArticleSupplierAgreementGetPurchaseArticleSupplierAgreements**](PurchaseArticleSupplierAgreementApi.md#purchaseArticleSupplierAgreementGetPurchaseArticleSupplierAgreements) | **POST** /PurchaseArticleSupplierAgreement/search | 
[**purchaseArticleSupplierAgreementUpdatePurchaseArticleSupplierAgreement**](PurchaseArticleSupplierAgreementApi.md#purchaseArticleSupplierAgreementUpdatePurchaseArticleSupplierAgreement) | **PUT** /PurchaseArticleSupplierAgreement/{supplierId}/{purchaseArticleId} | 


<a name="purchaseArticleSupplierAgreementCreatePurchaseArticleSupplierAgreement"></a>
# **purchaseArticleSupplierAgreementCreatePurchaseArticleSupplierAgreement**
> Object purchaseArticleSupplierAgreementCreatePurchaseArticleSupplierAgreement(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleSupplierAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleSupplierAgreementApi apiInstance = new PurchaseArticleSupplierAgreementApi();
PurchaseArticleSupplierAgreementDto dto = new PurchaseArticleSupplierAgreementDto(); // PurchaseArticleSupplierAgreementDto | 
try {
    Object result = apiInstance.purchaseArticleSupplierAgreementCreatePurchaseArticleSupplierAgreement(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleSupplierAgreementApi#purchaseArticleSupplierAgreementCreatePurchaseArticleSupplierAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PurchaseArticleSupplierAgreementDto**](PurchaseArticleSupplierAgreementDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseArticleSupplierAgreementDeletePurchaseArticleSupplierAgreement"></a>
# **purchaseArticleSupplierAgreementDeletePurchaseArticleSupplierAgreement**
> Object purchaseArticleSupplierAgreementDeletePurchaseArticleSupplierAgreement(supplierId, purchaseArticleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleSupplierAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleSupplierAgreementApi apiInstance = new PurchaseArticleSupplierAgreementApi();
Integer supplierId = 56; // Integer | 
Integer purchaseArticleId = 56; // Integer | 
try {
    Object result = apiInstance.purchaseArticleSupplierAgreementDeletePurchaseArticleSupplierAgreement(supplierId, purchaseArticleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleSupplierAgreementApi#purchaseArticleSupplierAgreementDeletePurchaseArticleSupplierAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **purchaseArticleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseArticleSupplierAgreementExistsPurchaseArticleSupplierAgreement"></a>
# **purchaseArticleSupplierAgreementExistsPurchaseArticleSupplierAgreement**
> Object purchaseArticleSupplierAgreementExistsPurchaseArticleSupplierAgreement(supplierId, purchaseArticleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleSupplierAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleSupplierAgreementApi apiInstance = new PurchaseArticleSupplierAgreementApi();
Integer supplierId = 56; // Integer | 
Integer purchaseArticleId = 56; // Integer | 
try {
    Object result = apiInstance.purchaseArticleSupplierAgreementExistsPurchaseArticleSupplierAgreement(supplierId, purchaseArticleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleSupplierAgreementApi#purchaseArticleSupplierAgreementExistsPurchaseArticleSupplierAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **purchaseArticleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseArticleSupplierAgreementGetPurchaseArticleSupplierAgreement"></a>
# **purchaseArticleSupplierAgreementGetPurchaseArticleSupplierAgreement**
> Object purchaseArticleSupplierAgreementGetPurchaseArticleSupplierAgreement(supplierId, purchaseArticleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleSupplierAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleSupplierAgreementApi apiInstance = new PurchaseArticleSupplierAgreementApi();
Integer supplierId = 56; // Integer | 
Integer purchaseArticleId = 56; // Integer | 
try {
    Object result = apiInstance.purchaseArticleSupplierAgreementGetPurchaseArticleSupplierAgreement(supplierId, purchaseArticleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleSupplierAgreementApi#purchaseArticleSupplierAgreementGetPurchaseArticleSupplierAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **purchaseArticleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseArticleSupplierAgreementGetPurchaseArticleSupplierAgreements"></a>
# **purchaseArticleSupplierAgreementGetPurchaseArticleSupplierAgreements**
> Object purchaseArticleSupplierAgreementGetPurchaseArticleSupplierAgreements(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleSupplierAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleSupplierAgreementApi apiInstance = new PurchaseArticleSupplierAgreementApi();
GetFilteredPurchaseArticleSupplierAgreementQuery query = new GetFilteredPurchaseArticleSupplierAgreementQuery(); // GetFilteredPurchaseArticleSupplierAgreementQuery | 
try {
    Object result = apiInstance.purchaseArticleSupplierAgreementGetPurchaseArticleSupplierAgreements(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleSupplierAgreementApi#purchaseArticleSupplierAgreementGetPurchaseArticleSupplierAgreements");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPurchaseArticleSupplierAgreementQuery**](GetFilteredPurchaseArticleSupplierAgreementQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseArticleSupplierAgreementUpdatePurchaseArticleSupplierAgreement"></a>
# **purchaseArticleSupplierAgreementUpdatePurchaseArticleSupplierAgreement**
> Object purchaseArticleSupplierAgreementUpdatePurchaseArticleSupplierAgreement(supplierId, purchaseArticleId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleSupplierAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleSupplierAgreementApi apiInstance = new PurchaseArticleSupplierAgreementApi();
Integer supplierId = 56; // Integer | 
Integer purchaseArticleId = 56; // Integer | 
PurchaseArticleSupplierAgreementDto dto = new PurchaseArticleSupplierAgreementDto(); // PurchaseArticleSupplierAgreementDto | 
try {
    Object result = apiInstance.purchaseArticleSupplierAgreementUpdatePurchaseArticleSupplierAgreement(supplierId, purchaseArticleId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleSupplierAgreementApi#purchaseArticleSupplierAgreementUpdatePurchaseArticleSupplierAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **purchaseArticleId** | **Integer**|  |
 **dto** | [**PurchaseArticleSupplierAgreementDto**](PurchaseArticleSupplierAgreementDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

