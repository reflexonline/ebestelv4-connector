# DockApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**dockCreateDock**](DockApi.md#dockCreateDock) | **POST** /Dock | 
[**dockDeleteDock**](DockApi.md#dockDeleteDock) | **DELETE** /Dock/{dockId} | 
[**dockExistsDock**](DockApi.md#dockExistsDock) | **GET** /Dock/exists/{dockId} | 
[**dockGetDock**](DockApi.md#dockGetDock) | **GET** /Dock/{dockId} | 
[**dockGetDocks**](DockApi.md#dockGetDocks) | **POST** /Dock/search | 
[**dockUpdateDock**](DockApi.md#dockUpdateDock) | **PUT** /Dock/{dockId} | 


<a name="dockCreateDock"></a>
# **dockCreateDock**
> Object dockCreateDock(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DockApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DockApi apiInstance = new DockApi();
DockDto dto = new DockDto(); // DockDto | 
try {
    Object result = apiInstance.dockCreateDock(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DockApi#dockCreateDock");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**DockDto**](DockDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="dockDeleteDock"></a>
# **dockDeleteDock**
> Object dockDeleteDock(dockId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DockApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DockApi apiInstance = new DockApi();
Integer dockId = 56; // Integer | 
try {
    Object result = apiInstance.dockDeleteDock(dockId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DockApi#dockDeleteDock");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dockId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="dockExistsDock"></a>
# **dockExistsDock**
> Object dockExistsDock(dockId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DockApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DockApi apiInstance = new DockApi();
Integer dockId = 56; // Integer | 
try {
    Object result = apiInstance.dockExistsDock(dockId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DockApi#dockExistsDock");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dockId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="dockGetDock"></a>
# **dockGetDock**
> Object dockGetDock(dockId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DockApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DockApi apiInstance = new DockApi();
Integer dockId = 56; // Integer | 
try {
    Object result = apiInstance.dockGetDock(dockId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DockApi#dockGetDock");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dockId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="dockGetDocks"></a>
# **dockGetDocks**
> Object dockGetDocks(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DockApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DockApi apiInstance = new DockApi();
GetFilteredDockQuery query = new GetFilteredDockQuery(); // GetFilteredDockQuery | 
try {
    Object result = apiInstance.dockGetDocks(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DockApi#dockGetDocks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredDockQuery**](GetFilteredDockQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="dockUpdateDock"></a>
# **dockUpdateDock**
> Object dockUpdateDock(dockId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DockApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DockApi apiInstance = new DockApi();
Integer dockId = 56; // Integer | 
DockDto dto = new DockDto(); // DockDto | 
try {
    Object result = apiInstance.dockUpdateDock(dockId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DockApi#dockUpdateDock");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dockId** | **Integer**|  |
 **dto** | [**DockDto**](DockDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

