# SaleArticleApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleArticleCreateSaleArticle**](SaleArticleApi.md#saleArticleCreateSaleArticle) | **POST** /SaleArticle | 
[**saleArticleDeleteSaleArticle**](SaleArticleApi.md#saleArticleDeleteSaleArticle) | **DELETE** /SaleArticle/{number} | 
[**saleArticleExistsSaleArticle**](SaleArticleApi.md#saleArticleExistsSaleArticle) | **GET** /SaleArticle/exists/{number} | 
[**saleArticleGetSaleArticle**](SaleArticleApi.md#saleArticleGetSaleArticle) | **GET** /SaleArticle/{number} | 
[**saleArticleGetSaleArticlePriceComponents**](SaleArticleApi.md#saleArticleGetSaleArticlePriceComponents) | **GET** /SaleArticle/{saleArticleId}/Price/Components | 
[**saleArticleGetSaleArticlePriceComponents_0**](SaleArticleApi.md#saleArticleGetSaleArticlePriceComponents_0) | **POST** /SaleArticle/{saleArticleId}/Price/Components | 
[**saleArticleGetSaleArticlePriceComponents_1**](SaleArticleApi.md#saleArticleGetSaleArticlePriceComponents_1) | **GET** /SaleArticle/{saleArticleId}/Price/{customerId}/Components | 
[**saleArticleGetSaleArticlePriceComponents_2**](SaleArticleApi.md#saleArticleGetSaleArticlePriceComponents_2) | **POST** /SaleArticle/{saleArticleId}/Price/{customerId}/Components | 
[**saleArticleGetSaleArticleStockBatchProductionTask**](SaleArticleApi.md#saleArticleGetSaleArticleStockBatchProductionTask) | **GET** /SaleArticle/{saleArticleId}/Stock/BatchProductionTask | 
[**saleArticleGetSaleArticleStockLocation**](SaleArticleApi.md#saleArticleGetSaleArticleStockLocation) | **GET** /SaleArticle/{saleArticleId}/Stock/Location | 
[**saleArticleGetSaleArticleStockLocationBatchProductionTask**](SaleArticleApi.md#saleArticleGetSaleArticleStockLocationBatchProductionTask) | **GET** /SaleArticle/{saleArticleId}/Stock/Location/BatchProductionTask | 
[**saleArticleGetSaleArticleStockTotalized**](SaleArticleApi.md#saleArticleGetSaleArticleStockTotalized) | **GET** /SaleArticle/{saleArticleId}/Stock/Totalized | 
[**saleArticleGetSaleArticles**](SaleArticleApi.md#saleArticleGetSaleArticles) | **POST** /SaleArticle/search | 
[**saleArticleGetSalesArticleConsumerPriceComponents**](SaleArticleApi.md#saleArticleGetSalesArticleConsumerPriceComponents) | **GET** /SaleArticle/{saleArticleId}/ConsumerPrice/Components | 
[**saleArticleGetSalesArticleConsumerPriceComponents_0**](SaleArticleApi.md#saleArticleGetSalesArticleConsumerPriceComponents_0) | **POST** /SaleArticle/{saleArticleId}/ConsumerPrice/Components | 
[**saleArticleGetSalesArticleConsumerPriceComponents_1**](SaleArticleApi.md#saleArticleGetSalesArticleConsumerPriceComponents_1) | **GET** /SaleArticle/{saleArticleId}/ConsumerPrice/{customerId}/Components | 
[**saleArticleGetSalesArticleConsumerPriceComponents_2**](SaleArticleApi.md#saleArticleGetSalesArticleConsumerPriceComponents_2) | **POST** /SaleArticle/{saleArticleId}/ConsumerPrice/{customerId}/Components | 
[**saleArticleUpdateSaleArticle**](SaleArticleApi.md#saleArticleUpdateSaleArticle) | **PUT** /SaleArticle/{number} | 


<a name="saleArticleCreateSaleArticle"></a>
# **saleArticleCreateSaleArticle**
> Object saleArticleCreateSaleArticle(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
SaleArticleDto dto = new SaleArticleDto(); // SaleArticleDto | 
try {
    Object result = apiInstance.saleArticleCreateSaleArticle(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleCreateSaleArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SaleArticleDto**](SaleArticleDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleDeleteSaleArticle"></a>
# **saleArticleDeleteSaleArticle**
> Object saleArticleDeleteSaleArticle(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleDeleteSaleArticle(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleDeleteSaleArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleExistsSaleArticle"></a>
# **saleArticleExistsSaleArticle**
> Object saleArticleExistsSaleArticle(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleExistsSaleArticle(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleExistsSaleArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleGetSaleArticle"></a>
# **saleArticleGetSaleArticle**
> Object saleArticleGetSaleArticle(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleGetSaleArticle(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleGetSaleArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleGetSaleArticlePriceComponents"></a>
# **saleArticleGetSaleArticlePriceComponents**
> Object saleArticleGetSaleArticlePriceComponents(saleArticleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer saleArticleId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleGetSaleArticlePriceComponents(saleArticleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleGetSaleArticlePriceComponents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleGetSaleArticlePriceComponents_0"></a>
# **saleArticleGetSaleArticlePriceComponents_0**
> Object saleArticleGetSaleArticlePriceComponents_0(saleArticleId, query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer saleArticleId = 56; // Integer | 
GetSaleArticlePriceComponentsQuery query = new GetSaleArticlePriceComponentsQuery(); // GetSaleArticlePriceComponentsQuery | 
try {
    Object result = apiInstance.saleArticleGetSaleArticlePriceComponents_0(saleArticleId, query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleGetSaleArticlePriceComponents_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleId** | **Integer**|  |
 **query** | [**GetSaleArticlePriceComponentsQuery**](GetSaleArticlePriceComponentsQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleGetSaleArticlePriceComponents_1"></a>
# **saleArticleGetSaleArticlePriceComponents_1**
> Object saleArticleGetSaleArticlePriceComponents_1(saleArticleId, customerId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer saleArticleId = 56; // Integer | 
Integer customerId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleGetSaleArticlePriceComponents_1(saleArticleId, customerId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleGetSaleArticlePriceComponents_1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleId** | **Integer**|  |
 **customerId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleGetSaleArticlePriceComponents_2"></a>
# **saleArticleGetSaleArticlePriceComponents_2**
> Object saleArticleGetSaleArticlePriceComponents_2(saleArticleId, customerId, query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer saleArticleId = 56; // Integer | 
Integer customerId = 56; // Integer | 
GetSaleArticlePriceComponentsQuery query = new GetSaleArticlePriceComponentsQuery(); // GetSaleArticlePriceComponentsQuery | 
try {
    Object result = apiInstance.saleArticleGetSaleArticlePriceComponents_2(saleArticleId, customerId, query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleGetSaleArticlePriceComponents_2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleId** | **Integer**|  |
 **customerId** | **Integer**|  |
 **query** | [**GetSaleArticlePriceComponentsQuery**](GetSaleArticlePriceComponentsQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleGetSaleArticleStockBatchProductionTask"></a>
# **saleArticleGetSaleArticleStockBatchProductionTask**
> Object saleArticleGetSaleArticleStockBatchProductionTask(saleArticleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer saleArticleId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleGetSaleArticleStockBatchProductionTask(saleArticleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleGetSaleArticleStockBatchProductionTask");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleGetSaleArticleStockLocation"></a>
# **saleArticleGetSaleArticleStockLocation**
> Object saleArticleGetSaleArticleStockLocation(saleArticleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer saleArticleId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleGetSaleArticleStockLocation(saleArticleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleGetSaleArticleStockLocation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleGetSaleArticleStockLocationBatchProductionTask"></a>
# **saleArticleGetSaleArticleStockLocationBatchProductionTask**
> Object saleArticleGetSaleArticleStockLocationBatchProductionTask(saleArticleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer saleArticleId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleGetSaleArticleStockLocationBatchProductionTask(saleArticleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleGetSaleArticleStockLocationBatchProductionTask");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleGetSaleArticleStockTotalized"></a>
# **saleArticleGetSaleArticleStockTotalized**
> Object saleArticleGetSaleArticleStockTotalized(saleArticleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer saleArticleId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleGetSaleArticleStockTotalized(saleArticleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleGetSaleArticleStockTotalized");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleGetSaleArticles"></a>
# **saleArticleGetSaleArticles**
> Object saleArticleGetSaleArticles(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
GetFilteredSaleArticleQuery query = new GetFilteredSaleArticleQuery(); // GetFilteredSaleArticleQuery | 
try {
    Object result = apiInstance.saleArticleGetSaleArticles(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleGetSaleArticles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSaleArticleQuery**](GetFilteredSaleArticleQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleGetSalesArticleConsumerPriceComponents"></a>
# **saleArticleGetSalesArticleConsumerPriceComponents**
> Object saleArticleGetSalesArticleConsumerPriceComponents(saleArticleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer saleArticleId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleGetSalesArticleConsumerPriceComponents(saleArticleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleGetSalesArticleConsumerPriceComponents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleGetSalesArticleConsumerPriceComponents_0"></a>
# **saleArticleGetSalesArticleConsumerPriceComponents_0**
> Object saleArticleGetSalesArticleConsumerPriceComponents_0(saleArticleId, query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer saleArticleId = 56; // Integer | 
GetSaleArticleConsumerPriceComponentsQuery query = new GetSaleArticleConsumerPriceComponentsQuery(); // GetSaleArticleConsumerPriceComponentsQuery | 
try {
    Object result = apiInstance.saleArticleGetSalesArticleConsumerPriceComponents_0(saleArticleId, query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleGetSalesArticleConsumerPriceComponents_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleId** | **Integer**|  |
 **query** | [**GetSaleArticleConsumerPriceComponentsQuery**](GetSaleArticleConsumerPriceComponentsQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleGetSalesArticleConsumerPriceComponents_1"></a>
# **saleArticleGetSalesArticleConsumerPriceComponents_1**
> Object saleArticleGetSalesArticleConsumerPriceComponents_1(saleArticleId, customerId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer saleArticleId = 56; // Integer | 
Integer customerId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleGetSalesArticleConsumerPriceComponents_1(saleArticleId, customerId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleGetSalesArticleConsumerPriceComponents_1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleId** | **Integer**|  |
 **customerId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleGetSalesArticleConsumerPriceComponents_2"></a>
# **saleArticleGetSalesArticleConsumerPriceComponents_2**
> Object saleArticleGetSalesArticleConsumerPriceComponents_2(saleArticleId, customerId, query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer saleArticleId = 56; // Integer | 
Integer customerId = 56; // Integer | 
GetSaleArticleConsumerPriceComponentsQuery query = new GetSaleArticleConsumerPriceComponentsQuery(); // GetSaleArticleConsumerPriceComponentsQuery | 
try {
    Object result = apiInstance.saleArticleGetSalesArticleConsumerPriceComponents_2(saleArticleId, customerId, query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleGetSalesArticleConsumerPriceComponents_2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleId** | **Integer**|  |
 **customerId** | **Integer**|  |
 **query** | [**GetSaleArticleConsumerPriceComponentsQuery**](GetSaleArticleConsumerPriceComponentsQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleUpdateSaleArticle"></a>
# **saleArticleUpdateSaleArticle**
> Object saleArticleUpdateSaleArticle(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleApi apiInstance = new SaleArticleApi();
Integer number = 56; // Integer | 
SaleArticleDto dto = new SaleArticleDto(); // SaleArticleDto | 
try {
    Object result = apiInstance.saleArticleUpdateSaleArticle(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleApi#saleArticleUpdateSaleArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**SaleArticleDto**](SaleArticleDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

