
# CustomerSaleAnalysisDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fromDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**untilDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]



