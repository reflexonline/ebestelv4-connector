# PurchaseArticleGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purchaseArticleGroupCreatePurchaseArticleGroup**](PurchaseArticleGroupApi.md#purchaseArticleGroupCreatePurchaseArticleGroup) | **POST** /PurchaseArticleGroup | 
[**purchaseArticleGroupDeletePurchaseArticleGroup**](PurchaseArticleGroupApi.md#purchaseArticleGroupDeletePurchaseArticleGroup) | **DELETE** /PurchaseArticleGroup/{number} | 
[**purchaseArticleGroupExistsPurchaseArticleGroup**](PurchaseArticleGroupApi.md#purchaseArticleGroupExistsPurchaseArticleGroup) | **GET** /PurchaseArticleGroup/exists/{number} | 
[**purchaseArticleGroupGetPurchaseArticleGroup**](PurchaseArticleGroupApi.md#purchaseArticleGroupGetPurchaseArticleGroup) | **GET** /PurchaseArticleGroup/{number} | 
[**purchaseArticleGroupGetPurchaseArticleGroups**](PurchaseArticleGroupApi.md#purchaseArticleGroupGetPurchaseArticleGroups) | **POST** /PurchaseArticleGroup/search | 
[**purchaseArticleGroupUpdatePurchaseArticleGroup**](PurchaseArticleGroupApi.md#purchaseArticleGroupUpdatePurchaseArticleGroup) | **PUT** /PurchaseArticleGroup/{number} | 


<a name="purchaseArticleGroupCreatePurchaseArticleGroup"></a>
# **purchaseArticleGroupCreatePurchaseArticleGroup**
> Object purchaseArticleGroupCreatePurchaseArticleGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleGroupApi apiInstance = new PurchaseArticleGroupApi();
PurchaseArticleGroupDto dto = new PurchaseArticleGroupDto(); // PurchaseArticleGroupDto | 
try {
    Object result = apiInstance.purchaseArticleGroupCreatePurchaseArticleGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleGroupApi#purchaseArticleGroupCreatePurchaseArticleGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PurchaseArticleGroupDto**](PurchaseArticleGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseArticleGroupDeletePurchaseArticleGroup"></a>
# **purchaseArticleGroupDeletePurchaseArticleGroup**
> Object purchaseArticleGroupDeletePurchaseArticleGroup(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleGroupApi apiInstance = new PurchaseArticleGroupApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.purchaseArticleGroupDeletePurchaseArticleGroup(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleGroupApi#purchaseArticleGroupDeletePurchaseArticleGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseArticleGroupExistsPurchaseArticleGroup"></a>
# **purchaseArticleGroupExistsPurchaseArticleGroup**
> Object purchaseArticleGroupExistsPurchaseArticleGroup(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleGroupApi apiInstance = new PurchaseArticleGroupApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.purchaseArticleGroupExistsPurchaseArticleGroup(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleGroupApi#purchaseArticleGroupExistsPurchaseArticleGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseArticleGroupGetPurchaseArticleGroup"></a>
# **purchaseArticleGroupGetPurchaseArticleGroup**
> Object purchaseArticleGroupGetPurchaseArticleGroup(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleGroupApi apiInstance = new PurchaseArticleGroupApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.purchaseArticleGroupGetPurchaseArticleGroup(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleGroupApi#purchaseArticleGroupGetPurchaseArticleGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseArticleGroupGetPurchaseArticleGroups"></a>
# **purchaseArticleGroupGetPurchaseArticleGroups**
> Object purchaseArticleGroupGetPurchaseArticleGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleGroupApi apiInstance = new PurchaseArticleGroupApi();
GetFilteredPurchaseArticleGroupQuery query = new GetFilteredPurchaseArticleGroupQuery(); // GetFilteredPurchaseArticleGroupQuery | 
try {
    Object result = apiInstance.purchaseArticleGroupGetPurchaseArticleGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleGroupApi#purchaseArticleGroupGetPurchaseArticleGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPurchaseArticleGroupQuery**](GetFilteredPurchaseArticleGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseArticleGroupUpdatePurchaseArticleGroup"></a>
# **purchaseArticleGroupUpdatePurchaseArticleGroup**
> Object purchaseArticleGroupUpdatePurchaseArticleGroup(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseArticleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseArticleGroupApi apiInstance = new PurchaseArticleGroupApi();
Integer number = 56; // Integer | 
PurchaseArticleGroupDto dto = new PurchaseArticleGroupDto(); // PurchaseArticleGroupDto | 
try {
    Object result = apiInstance.purchaseArticleGroupUpdatePurchaseArticleGroup(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseArticleGroupApi#purchaseArticleGroupUpdatePurchaseArticleGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**PurchaseArticleGroupDto**](PurchaseArticleGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

