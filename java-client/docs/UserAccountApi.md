# UserAccountApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**userAccountGetExternalLogin**](UserAccountApi.md#userAccountGetExternalLogin) | **GET** /UserAccount/ExternalLogin | 
[**userAccountGetExternalLogins**](UserAccountApi.md#userAccountGetExternalLogins) | **GET** /UserAccount/ExternalLogins | 
[**userAccountGetManageInfo**](UserAccountApi.md#userAccountGetManageInfo) | **GET** /UserAccount/ManageInfo | 
[**userAccountGetUserInfo**](UserAccountApi.md#userAccountGetUserInfo) | **GET** /UserAccount/UserInfo | 
[**userAccountLogout**](UserAccountApi.md#userAccountLogout) | **POST** /UserAccount/Logout | 


<a name="userAccountGetExternalLogin"></a>
# **userAccountGetExternalLogin**
> Object userAccountGetExternalLogin(provider, error)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UserAccountApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

UserAccountApi apiInstance = new UserAccountApi();
String provider = "provider_example"; // String | 
String error = "error_example"; // String | 
try {
    Object result = apiInstance.userAccountGetExternalLogin(provider, error);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserAccountApi#userAccountGetExternalLogin");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **provider** | **String**|  |
 **error** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="userAccountGetExternalLogins"></a>
# **userAccountGetExternalLogins**
> List&lt;ExternalLoginViewModel&gt; userAccountGetExternalLogins(returnUrl, generateState)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UserAccountApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

UserAccountApi apiInstance = new UserAccountApi();
String returnUrl = "returnUrl_example"; // String | 
Boolean generateState = true; // Boolean | 
try {
    List<ExternalLoginViewModel> result = apiInstance.userAccountGetExternalLogins(returnUrl, generateState);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserAccountApi#userAccountGetExternalLogins");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **returnUrl** | **String**|  |
 **generateState** | **Boolean**|  | [optional]

### Return type

[**List&lt;ExternalLoginViewModel&gt;**](ExternalLoginViewModel.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="userAccountGetManageInfo"></a>
# **userAccountGetManageInfo**
> ManageInfoViewModel userAccountGetManageInfo(returnUrl, generateState)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UserAccountApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

UserAccountApi apiInstance = new UserAccountApi();
String returnUrl = "returnUrl_example"; // String | 
Boolean generateState = true; // Boolean | 
try {
    ManageInfoViewModel result = apiInstance.userAccountGetManageInfo(returnUrl, generateState);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserAccountApi#userAccountGetManageInfo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **returnUrl** | **String**|  |
 **generateState** | **Boolean**|  | [optional]

### Return type

[**ManageInfoViewModel**](ManageInfoViewModel.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="userAccountGetUserInfo"></a>
# **userAccountGetUserInfo**
> UserInfoViewModel userAccountGetUserInfo()



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UserAccountApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

UserAccountApi apiInstance = new UserAccountApi();
try {
    UserInfoViewModel result = apiInstance.userAccountGetUserInfo();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserAccountApi#userAccountGetUserInfo");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserInfoViewModel**](UserInfoViewModel.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="userAccountLogout"></a>
# **userAccountLogout**
> Object userAccountLogout()



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UserAccountApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

UserAccountApi apiInstance = new UserAccountApi();
try {
    Object result = apiInstance.userAccountLogout();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserAccountApi#userAccountLogout");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

