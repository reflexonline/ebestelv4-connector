
# GetFilteredMultiBranchQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**multiBranchIdMinValue** | **Integer** |  |  [optional]
**multiBranchIdMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]
**fromProductionGroupIdMinValue** | **Integer** |  |  [optional]
**fromProductionGroupIdMaxValue** | **Integer** |  |  [optional]
**isMainBranchMinValue** | **Integer** |  |  [optional]
**isMainBranchMaxValue** | **Integer** |  |  [optional]



