# MeatTypeApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**meatTypeCreateMeatType**](MeatTypeApi.md#meatTypeCreateMeatType) | **POST** /MeatType | 
[**meatTypeDeleteMeatType**](MeatTypeApi.md#meatTypeDeleteMeatType) | **DELETE** /MeatType/{number} | 
[**meatTypeExistsMeatType**](MeatTypeApi.md#meatTypeExistsMeatType) | **GET** /MeatType/exists/{number} | 
[**meatTypeGetMeatType**](MeatTypeApi.md#meatTypeGetMeatType) | **GET** /MeatType/{number} | 
[**meatTypeGetMeatTypes**](MeatTypeApi.md#meatTypeGetMeatTypes) | **POST** /MeatType/search | 
[**meatTypeUpdateMeatType**](MeatTypeApi.md#meatTypeUpdateMeatType) | **PUT** /MeatType/{number} | 


<a name="meatTypeCreateMeatType"></a>
# **meatTypeCreateMeatType**
> Object meatTypeCreateMeatType(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MeatTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MeatTypeApi apiInstance = new MeatTypeApi();
MeatTypeDto dto = new MeatTypeDto(); // MeatTypeDto | 
try {
    Object result = apiInstance.meatTypeCreateMeatType(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MeatTypeApi#meatTypeCreateMeatType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**MeatTypeDto**](MeatTypeDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="meatTypeDeleteMeatType"></a>
# **meatTypeDeleteMeatType**
> Object meatTypeDeleteMeatType(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MeatTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MeatTypeApi apiInstance = new MeatTypeApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.meatTypeDeleteMeatType(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MeatTypeApi#meatTypeDeleteMeatType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="meatTypeExistsMeatType"></a>
# **meatTypeExistsMeatType**
> Object meatTypeExistsMeatType(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MeatTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MeatTypeApi apiInstance = new MeatTypeApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.meatTypeExistsMeatType(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MeatTypeApi#meatTypeExistsMeatType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="meatTypeGetMeatType"></a>
# **meatTypeGetMeatType**
> Object meatTypeGetMeatType(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MeatTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MeatTypeApi apiInstance = new MeatTypeApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.meatTypeGetMeatType(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MeatTypeApi#meatTypeGetMeatType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="meatTypeGetMeatTypes"></a>
# **meatTypeGetMeatTypes**
> Object meatTypeGetMeatTypes(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MeatTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MeatTypeApi apiInstance = new MeatTypeApi();
GetFilteredMeatTypeQuery query = new GetFilteredMeatTypeQuery(); // GetFilteredMeatTypeQuery | 
try {
    Object result = apiInstance.meatTypeGetMeatTypes(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MeatTypeApi#meatTypeGetMeatTypes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredMeatTypeQuery**](GetFilteredMeatTypeQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="meatTypeUpdateMeatType"></a>
# **meatTypeUpdateMeatType**
> Object meatTypeUpdateMeatType(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MeatTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MeatTypeApi apiInstance = new MeatTypeApi();
Integer number = 56; // Integer | 
MeatTypeDto dto = new MeatTypeDto(); // MeatTypeDto | 
try {
    Object result = apiInstance.meatTypeUpdateMeatType(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MeatTypeApi#meatTypeUpdateMeatType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**MeatTypeDto**](MeatTypeDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

