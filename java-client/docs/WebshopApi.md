# WebshopApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**webshopCreateWebshop**](WebshopApi.md#webshopCreateWebshop) | **POST** /Webshop | 
[**webshopDeleteWebshop**](WebshopApi.md#webshopDeleteWebshop) | **DELETE** /Webshop/{webshopId} | 
[**webshopExistsWebshop**](WebshopApi.md#webshopExistsWebshop) | **GET** /Webshop/exists/{webshopId} | 
[**webshopGetWebshop**](WebshopApi.md#webshopGetWebshop) | **GET** /Webshop/{webshopId} | 
[**webshopGetWebshops**](WebshopApi.md#webshopGetWebshops) | **POST** /Webshop/search | 
[**webshopUpdateWebshop**](WebshopApi.md#webshopUpdateWebshop) | **PUT** /Webshop/{webshopId} | 


<a name="webshopCreateWebshop"></a>
# **webshopCreateWebshop**
> Object webshopCreateWebshop(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WebshopApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WebshopApi apiInstance = new WebshopApi();
WebshopDto dto = new WebshopDto(); // WebshopDto | 
try {
    Object result = apiInstance.webshopCreateWebshop(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebshopApi#webshopCreateWebshop");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**WebshopDto**](WebshopDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="webshopDeleteWebshop"></a>
# **webshopDeleteWebshop**
> Object webshopDeleteWebshop(webshopId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WebshopApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WebshopApi apiInstance = new WebshopApi();
Integer webshopId = 56; // Integer | 
try {
    Object result = apiInstance.webshopDeleteWebshop(webshopId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebshopApi#webshopDeleteWebshop");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webshopId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="webshopExistsWebshop"></a>
# **webshopExistsWebshop**
> Object webshopExistsWebshop(webshopId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WebshopApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WebshopApi apiInstance = new WebshopApi();
Integer webshopId = 56; // Integer | 
try {
    Object result = apiInstance.webshopExistsWebshop(webshopId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebshopApi#webshopExistsWebshop");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webshopId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="webshopGetWebshop"></a>
# **webshopGetWebshop**
> Object webshopGetWebshop(webshopId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WebshopApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WebshopApi apiInstance = new WebshopApi();
Integer webshopId = 56; // Integer | 
try {
    Object result = apiInstance.webshopGetWebshop(webshopId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebshopApi#webshopGetWebshop");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webshopId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="webshopGetWebshops"></a>
# **webshopGetWebshops**
> Object webshopGetWebshops(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WebshopApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WebshopApi apiInstance = new WebshopApi();
GetFilteredWebshopQuery query = new GetFilteredWebshopQuery(); // GetFilteredWebshopQuery | 
try {
    Object result = apiInstance.webshopGetWebshops(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebshopApi#webshopGetWebshops");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredWebshopQuery**](GetFilteredWebshopQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="webshopUpdateWebshop"></a>
# **webshopUpdateWebshop**
> Object webshopUpdateWebshop(webshopId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WebshopApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WebshopApi apiInstance = new WebshopApi();
Integer webshopId = 56; // Integer | 
WebshopDto dto = new WebshopDto(); // WebshopDto | 
try {
    Object result = apiInstance.webshopUpdateWebshop(webshopId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebshopApi#webshopUpdateWebshop");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webshopId** | **Integer**|  |
 **dto** | [**WebshopDto**](WebshopDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

