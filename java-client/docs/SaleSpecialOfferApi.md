# SaleSpecialOfferApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleSpecialOfferCreateSaleSpecialOffer**](SaleSpecialOfferApi.md#saleSpecialOfferCreateSaleSpecialOffer) | **POST** /SaleSpecialOffer | 
[**saleSpecialOfferDeleteSaleSpecialOffer**](SaleSpecialOfferApi.md#saleSpecialOfferDeleteSaleSpecialOffer) | **DELETE** /SaleSpecialOffer/{customerId}/{articleId}/{dateIndex} | 
[**saleSpecialOfferExistsSaleSpecialOffer**](SaleSpecialOfferApi.md#saleSpecialOfferExistsSaleSpecialOffer) | **GET** /SaleSpecialOffer/exists/{customerId}/{articleId}/{dateIndex} | 
[**saleSpecialOfferGetSaleSpecialOffer**](SaleSpecialOfferApi.md#saleSpecialOfferGetSaleSpecialOffer) | **GET** /SaleSpecialOffer/{customerId}/{articleId}/{dateIndex} | 
[**saleSpecialOfferGetSaleSpecialOffers**](SaleSpecialOfferApi.md#saleSpecialOfferGetSaleSpecialOffers) | **POST** /SaleSpecialOffer/search | 
[**saleSpecialOfferUpdateSaleSpecialOffer**](SaleSpecialOfferApi.md#saleSpecialOfferUpdateSaleSpecialOffer) | **PUT** /SaleSpecialOffer/{customerId}/{articleId}/{dateIndex} | 


<a name="saleSpecialOfferCreateSaleSpecialOffer"></a>
# **saleSpecialOfferCreateSaleSpecialOffer**
> Object saleSpecialOfferCreateSaleSpecialOffer(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleSpecialOfferApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleSpecialOfferApi apiInstance = new SaleSpecialOfferApi();
SaleSpecialOfferDto dto = new SaleSpecialOfferDto(); // SaleSpecialOfferDto | 
try {
    Object result = apiInstance.saleSpecialOfferCreateSaleSpecialOffer(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleSpecialOfferApi#saleSpecialOfferCreateSaleSpecialOffer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SaleSpecialOfferDto**](SaleSpecialOfferDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleSpecialOfferDeleteSaleSpecialOffer"></a>
# **saleSpecialOfferDeleteSaleSpecialOffer**
> Object saleSpecialOfferDeleteSaleSpecialOffer(customerId, articleId, dateIndex)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleSpecialOfferApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleSpecialOfferApi apiInstance = new SaleSpecialOfferApi();
Integer customerId = 56; // Integer | 
Integer articleId = 56; // Integer | 
Integer dateIndex = 56; // Integer | 
try {
    Object result = apiInstance.saleSpecialOfferDeleteSaleSpecialOffer(customerId, articleId, dateIndex);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleSpecialOfferApi#saleSpecialOfferDeleteSaleSpecialOffer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **articleId** | **Integer**|  |
 **dateIndex** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleSpecialOfferExistsSaleSpecialOffer"></a>
# **saleSpecialOfferExistsSaleSpecialOffer**
> Object saleSpecialOfferExistsSaleSpecialOffer(customerId, articleId, dateIndex)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleSpecialOfferApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleSpecialOfferApi apiInstance = new SaleSpecialOfferApi();
Integer customerId = 56; // Integer | 
Integer articleId = 56; // Integer | 
Integer dateIndex = 56; // Integer | 
try {
    Object result = apiInstance.saleSpecialOfferExistsSaleSpecialOffer(customerId, articleId, dateIndex);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleSpecialOfferApi#saleSpecialOfferExistsSaleSpecialOffer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **articleId** | **Integer**|  |
 **dateIndex** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleSpecialOfferGetSaleSpecialOffer"></a>
# **saleSpecialOfferGetSaleSpecialOffer**
> Object saleSpecialOfferGetSaleSpecialOffer(customerId, articleId, dateIndex)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleSpecialOfferApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleSpecialOfferApi apiInstance = new SaleSpecialOfferApi();
Integer customerId = 56; // Integer | 
Integer articleId = 56; // Integer | 
Integer dateIndex = 56; // Integer | 
try {
    Object result = apiInstance.saleSpecialOfferGetSaleSpecialOffer(customerId, articleId, dateIndex);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleSpecialOfferApi#saleSpecialOfferGetSaleSpecialOffer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **articleId** | **Integer**|  |
 **dateIndex** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleSpecialOfferGetSaleSpecialOffers"></a>
# **saleSpecialOfferGetSaleSpecialOffers**
> Object saleSpecialOfferGetSaleSpecialOffers(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleSpecialOfferApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleSpecialOfferApi apiInstance = new SaleSpecialOfferApi();
GetFilteredSaleSpecialOfferQuery query = new GetFilteredSaleSpecialOfferQuery(); // GetFilteredSaleSpecialOfferQuery | 
try {
    Object result = apiInstance.saleSpecialOfferGetSaleSpecialOffers(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleSpecialOfferApi#saleSpecialOfferGetSaleSpecialOffers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSaleSpecialOfferQuery**](GetFilteredSaleSpecialOfferQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleSpecialOfferUpdateSaleSpecialOffer"></a>
# **saleSpecialOfferUpdateSaleSpecialOffer**
> Object saleSpecialOfferUpdateSaleSpecialOffer(customerId, articleId, dateIndex, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleSpecialOfferApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleSpecialOfferApi apiInstance = new SaleSpecialOfferApi();
Integer customerId = 56; // Integer | 
Integer articleId = 56; // Integer | 
Integer dateIndex = 56; // Integer | 
SaleSpecialOfferDto dto = new SaleSpecialOfferDto(); // SaleSpecialOfferDto | 
try {
    Object result = apiInstance.saleSpecialOfferUpdateSaleSpecialOffer(customerId, articleId, dateIndex, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleSpecialOfferApi#saleSpecialOfferUpdateSaleSpecialOffer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **articleId** | **Integer**|  |
 **dateIndex** | **Integer**|  |
 **dto** | [**SaleSpecialOfferDto**](SaleSpecialOfferDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

