
# GetFilteredStockMutationReasonQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]
**mutationTypeMinValue** | **Integer** |  |  [optional]
**mutationTypeMaxValue** | **Integer** |  |  [optional]



