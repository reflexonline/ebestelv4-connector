
# GetFilteredPaymentMethodQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paymentMethodIdMinValue** | **Integer** |  |  [optional]
**paymentMethodIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]
**currencyIdMinValue** | **Integer** |  |  [optional]
**currencyIdMaxValue** | **Integer** |  |  [optional]



