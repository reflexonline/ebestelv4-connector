
# GetPurchaseArticlePriceComponentsQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purchaseArticleId** | **Integer** |  |  [optional]
**supplierId** | **Integer** |  |  [optional]
**date** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]



