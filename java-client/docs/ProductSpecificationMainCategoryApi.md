# ProductSpecificationMainCategoryApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productSpecificationMainCategoryCreateProductSpecificationMainCategory**](ProductSpecificationMainCategoryApi.md#productSpecificationMainCategoryCreateProductSpecificationMainCategory) | **POST** /ProductSpecificationMainCategory | 
[**productSpecificationMainCategoryDeleteProductSpecificationMainCategory**](ProductSpecificationMainCategoryApi.md#productSpecificationMainCategoryDeleteProductSpecificationMainCategory) | **DELETE** /ProductSpecificationMainCategory/{productSpecificationMainCategorieId} | 
[**productSpecificationMainCategoryExistsProductSpecificationMainCategory**](ProductSpecificationMainCategoryApi.md#productSpecificationMainCategoryExistsProductSpecificationMainCategory) | **GET** /ProductSpecificationMainCategory/exists/{productSpecificationMainCategorieId} | 
[**productSpecificationMainCategoryGetProductSpecificationMainCategories**](ProductSpecificationMainCategoryApi.md#productSpecificationMainCategoryGetProductSpecificationMainCategories) | **POST** /ProductSpecificationMainCategory/search | 
[**productSpecificationMainCategoryGetProductSpecificationMainCategory**](ProductSpecificationMainCategoryApi.md#productSpecificationMainCategoryGetProductSpecificationMainCategory) | **GET** /ProductSpecificationMainCategory/{productSpecificationMainCategorieId} | 
[**productSpecificationMainCategoryUpdateProductSpecificationMainCategory**](ProductSpecificationMainCategoryApi.md#productSpecificationMainCategoryUpdateProductSpecificationMainCategory) | **PUT** /ProductSpecificationMainCategory/{productSpecificationMainCategorieId} | 


<a name="productSpecificationMainCategoryCreateProductSpecificationMainCategory"></a>
# **productSpecificationMainCategoryCreateProductSpecificationMainCategory**
> Object productSpecificationMainCategoryCreateProductSpecificationMainCategory(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationMainCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationMainCategoryApi apiInstance = new ProductSpecificationMainCategoryApi();
ProductSpecificationMainCategoryDto dto = new ProductSpecificationMainCategoryDto(); // ProductSpecificationMainCategoryDto | 
try {
    Object result = apiInstance.productSpecificationMainCategoryCreateProductSpecificationMainCategory(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationMainCategoryApi#productSpecificationMainCategoryCreateProductSpecificationMainCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ProductSpecificationMainCategoryDto**](ProductSpecificationMainCategoryDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productSpecificationMainCategoryDeleteProductSpecificationMainCategory"></a>
# **productSpecificationMainCategoryDeleteProductSpecificationMainCategory**
> Object productSpecificationMainCategoryDeleteProductSpecificationMainCategory(productSpecificationMainCategorieId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationMainCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationMainCategoryApi apiInstance = new ProductSpecificationMainCategoryApi();
Integer productSpecificationMainCategorieId = 56; // Integer | 
try {
    Object result = apiInstance.productSpecificationMainCategoryDeleteProductSpecificationMainCategory(productSpecificationMainCategorieId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationMainCategoryApi#productSpecificationMainCategoryDeleteProductSpecificationMainCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationMainCategorieId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productSpecificationMainCategoryExistsProductSpecificationMainCategory"></a>
# **productSpecificationMainCategoryExistsProductSpecificationMainCategory**
> Object productSpecificationMainCategoryExistsProductSpecificationMainCategory(productSpecificationMainCategorieId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationMainCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationMainCategoryApi apiInstance = new ProductSpecificationMainCategoryApi();
Integer productSpecificationMainCategorieId = 56; // Integer | 
try {
    Object result = apiInstance.productSpecificationMainCategoryExistsProductSpecificationMainCategory(productSpecificationMainCategorieId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationMainCategoryApi#productSpecificationMainCategoryExistsProductSpecificationMainCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationMainCategorieId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productSpecificationMainCategoryGetProductSpecificationMainCategories"></a>
# **productSpecificationMainCategoryGetProductSpecificationMainCategories**
> Object productSpecificationMainCategoryGetProductSpecificationMainCategories(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationMainCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationMainCategoryApi apiInstance = new ProductSpecificationMainCategoryApi();
GetFilteredProductSpecificationMainCategoryQuery query = new GetFilteredProductSpecificationMainCategoryQuery(); // GetFilteredProductSpecificationMainCategoryQuery | 
try {
    Object result = apiInstance.productSpecificationMainCategoryGetProductSpecificationMainCategories(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationMainCategoryApi#productSpecificationMainCategoryGetProductSpecificationMainCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductSpecificationMainCategoryQuery**](GetFilteredProductSpecificationMainCategoryQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productSpecificationMainCategoryGetProductSpecificationMainCategory"></a>
# **productSpecificationMainCategoryGetProductSpecificationMainCategory**
> Object productSpecificationMainCategoryGetProductSpecificationMainCategory(productSpecificationMainCategorieId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationMainCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationMainCategoryApi apiInstance = new ProductSpecificationMainCategoryApi();
Integer productSpecificationMainCategorieId = 56; // Integer | 
try {
    Object result = apiInstance.productSpecificationMainCategoryGetProductSpecificationMainCategory(productSpecificationMainCategorieId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationMainCategoryApi#productSpecificationMainCategoryGetProductSpecificationMainCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationMainCategorieId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productSpecificationMainCategoryUpdateProductSpecificationMainCategory"></a>
# **productSpecificationMainCategoryUpdateProductSpecificationMainCategory**
> Object productSpecificationMainCategoryUpdateProductSpecificationMainCategory(productSpecificationMainCategorieId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationMainCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationMainCategoryApi apiInstance = new ProductSpecificationMainCategoryApi();
Integer productSpecificationMainCategorieId = 56; // Integer | 
ProductSpecificationMainCategoryDto dto = new ProductSpecificationMainCategoryDto(); // ProductSpecificationMainCategoryDto | 
try {
    Object result = apiInstance.productSpecificationMainCategoryUpdateProductSpecificationMainCategory(productSpecificationMainCategorieId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationMainCategoryApi#productSpecificationMainCategoryUpdateProductSpecificationMainCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationMainCategorieId** | **Integer**|  |
 **dto** | [**ProductSpecificationMainCategoryDto**](ProductSpecificationMainCategoryDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

