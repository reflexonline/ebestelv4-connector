# WssUserApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**wssUserCreateWssUser**](WssUserApi.md#wssUserCreateWssUser) | **POST** /WssUser | 
[**wssUserDeleteWssUser**](WssUserApi.md#wssUserDeleteWssUser) | **DELETE** /WssUser/{number} | 
[**wssUserExistsWssUser**](WssUserApi.md#wssUserExistsWssUser) | **GET** /WssUser/exists/{number} | 
[**wssUserGetWssUser**](WssUserApi.md#wssUserGetWssUser) | **GET** /WssUser/{number} | 
[**wssUserGetWssUsers**](WssUserApi.md#wssUserGetWssUsers) | **POST** /WssUser/search | 
[**wssUserUpdateWssUser**](WssUserApi.md#wssUserUpdateWssUser) | **PUT** /WssUser/{number} | 


<a name="wssUserCreateWssUser"></a>
# **wssUserCreateWssUser**
> Object wssUserCreateWssUser(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WssUserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WssUserApi apiInstance = new WssUserApi();
WssUserDto dto = new WssUserDto(); // WssUserDto | 
try {
    Object result = apiInstance.wssUserCreateWssUser(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WssUserApi#wssUserCreateWssUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**WssUserDto**](WssUserDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="wssUserDeleteWssUser"></a>
# **wssUserDeleteWssUser**
> Object wssUserDeleteWssUser(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WssUserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WssUserApi apiInstance = new WssUserApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.wssUserDeleteWssUser(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WssUserApi#wssUserDeleteWssUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="wssUserExistsWssUser"></a>
# **wssUserExistsWssUser**
> Object wssUserExistsWssUser(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WssUserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WssUserApi apiInstance = new WssUserApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.wssUserExistsWssUser(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WssUserApi#wssUserExistsWssUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="wssUserGetWssUser"></a>
# **wssUserGetWssUser**
> Object wssUserGetWssUser(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WssUserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WssUserApi apiInstance = new WssUserApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.wssUserGetWssUser(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WssUserApi#wssUserGetWssUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="wssUserGetWssUsers"></a>
# **wssUserGetWssUsers**
> Object wssUserGetWssUsers(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WssUserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WssUserApi apiInstance = new WssUserApi();
GetFilteredWssUserQuery query = new GetFilteredWssUserQuery(); // GetFilteredWssUserQuery | 
try {
    Object result = apiInstance.wssUserGetWssUsers(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WssUserApi#wssUserGetWssUsers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredWssUserQuery**](GetFilteredWssUserQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="wssUserUpdateWssUser"></a>
# **wssUserUpdateWssUser**
> Object wssUserUpdateWssUser(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WssUserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WssUserApi apiInstance = new WssUserApi();
Integer number = 56; // Integer | 
WssUserDto dto = new WssUserDto(); // WssUserDto | 
try {
    Object result = apiInstance.wssUserUpdateWssUser(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WssUserApi#wssUserUpdateWssUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**WssUserDto**](WssUserDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

