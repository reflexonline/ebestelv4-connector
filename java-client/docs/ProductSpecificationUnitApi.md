# ProductSpecificationUnitApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productSpecificationUnitCreateProductSpecificationUnit**](ProductSpecificationUnitApi.md#productSpecificationUnitCreateProductSpecificationUnit) | **POST** /ProductSpecificationUnit | 
[**productSpecificationUnitDeleteProductSpecificationUnit**](ProductSpecificationUnitApi.md#productSpecificationUnitDeleteProductSpecificationUnit) | **DELETE** /ProductSpecificationUnit/{productSpecificationUnitId} | 
[**productSpecificationUnitExistsProductSpecificationUnit**](ProductSpecificationUnitApi.md#productSpecificationUnitExistsProductSpecificationUnit) | **GET** /ProductSpecificationUnit/exists/{productSpecificationUnitId} | 
[**productSpecificationUnitGetProductSpecificationUnit**](ProductSpecificationUnitApi.md#productSpecificationUnitGetProductSpecificationUnit) | **GET** /ProductSpecificationUnit/{productSpecificationUnitId} | 
[**productSpecificationUnitGetProductSpecificationUnits**](ProductSpecificationUnitApi.md#productSpecificationUnitGetProductSpecificationUnits) | **POST** /ProductSpecificationUnit/search | 
[**productSpecificationUnitUpdateProductSpecificationUnit**](ProductSpecificationUnitApi.md#productSpecificationUnitUpdateProductSpecificationUnit) | **PUT** /ProductSpecificationUnit/{productSpecificationUnitId} | 


<a name="productSpecificationUnitCreateProductSpecificationUnit"></a>
# **productSpecificationUnitCreateProductSpecificationUnit**
> Object productSpecificationUnitCreateProductSpecificationUnit(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationUnitApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationUnitApi apiInstance = new ProductSpecificationUnitApi();
ProductSpecificationUnitDto dto = new ProductSpecificationUnitDto(); // ProductSpecificationUnitDto | 
try {
    Object result = apiInstance.productSpecificationUnitCreateProductSpecificationUnit(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationUnitApi#productSpecificationUnitCreateProductSpecificationUnit");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ProductSpecificationUnitDto**](ProductSpecificationUnitDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productSpecificationUnitDeleteProductSpecificationUnit"></a>
# **productSpecificationUnitDeleteProductSpecificationUnit**
> Object productSpecificationUnitDeleteProductSpecificationUnit(productSpecificationUnitId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationUnitApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationUnitApi apiInstance = new ProductSpecificationUnitApi();
Integer productSpecificationUnitId = 56; // Integer | 
try {
    Object result = apiInstance.productSpecificationUnitDeleteProductSpecificationUnit(productSpecificationUnitId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationUnitApi#productSpecificationUnitDeleteProductSpecificationUnit");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationUnitId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productSpecificationUnitExistsProductSpecificationUnit"></a>
# **productSpecificationUnitExistsProductSpecificationUnit**
> Object productSpecificationUnitExistsProductSpecificationUnit(productSpecificationUnitId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationUnitApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationUnitApi apiInstance = new ProductSpecificationUnitApi();
Integer productSpecificationUnitId = 56; // Integer | 
try {
    Object result = apiInstance.productSpecificationUnitExistsProductSpecificationUnit(productSpecificationUnitId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationUnitApi#productSpecificationUnitExistsProductSpecificationUnit");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationUnitId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productSpecificationUnitGetProductSpecificationUnit"></a>
# **productSpecificationUnitGetProductSpecificationUnit**
> Object productSpecificationUnitGetProductSpecificationUnit(productSpecificationUnitId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationUnitApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationUnitApi apiInstance = new ProductSpecificationUnitApi();
Integer productSpecificationUnitId = 56; // Integer | 
try {
    Object result = apiInstance.productSpecificationUnitGetProductSpecificationUnit(productSpecificationUnitId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationUnitApi#productSpecificationUnitGetProductSpecificationUnit");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationUnitId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productSpecificationUnitGetProductSpecificationUnits"></a>
# **productSpecificationUnitGetProductSpecificationUnits**
> Object productSpecificationUnitGetProductSpecificationUnits(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationUnitApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationUnitApi apiInstance = new ProductSpecificationUnitApi();
GetFilteredProductSpecificationUnitQuery query = new GetFilteredProductSpecificationUnitQuery(); // GetFilteredProductSpecificationUnitQuery | 
try {
    Object result = apiInstance.productSpecificationUnitGetProductSpecificationUnits(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationUnitApi#productSpecificationUnitGetProductSpecificationUnits");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductSpecificationUnitQuery**](GetFilteredProductSpecificationUnitQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productSpecificationUnitUpdateProductSpecificationUnit"></a>
# **productSpecificationUnitUpdateProductSpecificationUnit**
> Object productSpecificationUnitUpdateProductSpecificationUnit(productSpecificationUnitId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationUnitApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationUnitApi apiInstance = new ProductSpecificationUnitApi();
Integer productSpecificationUnitId = 56; // Integer | 
ProductSpecificationUnitDto dto = new ProductSpecificationUnitDto(); // ProductSpecificationUnitDto | 
try {
    Object result = apiInstance.productSpecificationUnitUpdateProductSpecificationUnit(productSpecificationUnitId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationUnitApi#productSpecificationUnitUpdateProductSpecificationUnit");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationUnitId** | **Integer**|  |
 **dto** | [**ProductSpecificationUnitDto**](ProductSpecificationUnitDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

