
# GetFilteredSupplierPurchaseArticleQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierArticleIdMinValue** | **String** |  |  [optional]
**supplierArticleIdMaxValue** | **String** |  |  [optional]
**supplierIdMinValue** | **Integer** |  |  [optional]
**supplierIdMaxValue** | **Integer** |  |  [optional]
**purchaseArticleIdMinValue** | **Integer** |  |  [optional]
**purchaseArticleIdMaxValue** | **Integer** |  |  [optional]



