
# BaseMaterialDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**oldOmschrijving** | **String** |  |  [optional]
**percentageVermelden** | **Integer** |  |  [optional]
**vleesSoort** | **Integer** |  |  [optional]
**vleesEiwit** | **Double** |  |  [optional]
**vleesCollageen** | **Double** |  |  [optional]
**vleesVet** | **Double** |  |  [optional]
**categorialId** | **Integer** |  |  [optional]
**eNumber** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



