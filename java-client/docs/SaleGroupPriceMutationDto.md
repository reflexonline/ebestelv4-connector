
# SaleGroupPriceMutationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **Integer** |  | 
**salesGroupId** | **Integer** |  | 
**extensionType** | [**ExtensionTypeEnum**](#ExtensionTypeEnum) |  |  [optional]
**amount** | **Double** |  |  [optional]
**discountPercentage** | **Double** |  |  [optional]
**mutationDateTime** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="ExtensionTypeEnum"></a>
## Enum: ExtensionTypeEnum
Name | Value
---- | -----
DISCOUNT | &quot;Discount&quot;
FIXED | &quot;Fixed&quot;
BLOCKED | &quot;Blocked&quot;



