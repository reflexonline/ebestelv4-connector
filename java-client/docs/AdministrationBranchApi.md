# AdministrationBranchApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**administrationBranchCreateAdministrationBranch**](AdministrationBranchApi.md#administrationBranchCreateAdministrationBranch) | **POST** /AdministrationBranch | 
[**administrationBranchDeleteAdministrationBranch**](AdministrationBranchApi.md#administrationBranchDeleteAdministrationBranch) | **DELETE** /AdministrationBranch/{number} | 
[**administrationBranchExistsAdministrationBranch**](AdministrationBranchApi.md#administrationBranchExistsAdministrationBranch) | **GET** /AdministrationBranch/exists/{number} | 
[**administrationBranchGetAdministrationBranch**](AdministrationBranchApi.md#administrationBranchGetAdministrationBranch) | **GET** /AdministrationBranch/{number} | 
[**administrationBranchGetAdministrationBranches**](AdministrationBranchApi.md#administrationBranchGetAdministrationBranches) | **POST** /AdministrationBranch/search | 
[**administrationBranchUpdateAdministrationBranch**](AdministrationBranchApi.md#administrationBranchUpdateAdministrationBranch) | **PUT** /AdministrationBranch/{number} | 


<a name="administrationBranchCreateAdministrationBranch"></a>
# **administrationBranchCreateAdministrationBranch**
> Object administrationBranchCreateAdministrationBranch(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AdministrationBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AdministrationBranchApi apiInstance = new AdministrationBranchApi();
AdministrationBranchDto dto = new AdministrationBranchDto(); // AdministrationBranchDto | 
try {
    Object result = apiInstance.administrationBranchCreateAdministrationBranch(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AdministrationBranchApi#administrationBranchCreateAdministrationBranch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**AdministrationBranchDto**](AdministrationBranchDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="administrationBranchDeleteAdministrationBranch"></a>
# **administrationBranchDeleteAdministrationBranch**
> Object administrationBranchDeleteAdministrationBranch(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AdministrationBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AdministrationBranchApi apiInstance = new AdministrationBranchApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.administrationBranchDeleteAdministrationBranch(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AdministrationBranchApi#administrationBranchDeleteAdministrationBranch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="administrationBranchExistsAdministrationBranch"></a>
# **administrationBranchExistsAdministrationBranch**
> Object administrationBranchExistsAdministrationBranch(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AdministrationBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AdministrationBranchApi apiInstance = new AdministrationBranchApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.administrationBranchExistsAdministrationBranch(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AdministrationBranchApi#administrationBranchExistsAdministrationBranch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="administrationBranchGetAdministrationBranch"></a>
# **administrationBranchGetAdministrationBranch**
> Object administrationBranchGetAdministrationBranch(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AdministrationBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AdministrationBranchApi apiInstance = new AdministrationBranchApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.administrationBranchGetAdministrationBranch(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AdministrationBranchApi#administrationBranchGetAdministrationBranch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="administrationBranchGetAdministrationBranches"></a>
# **administrationBranchGetAdministrationBranches**
> Object administrationBranchGetAdministrationBranches(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AdministrationBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AdministrationBranchApi apiInstance = new AdministrationBranchApi();
GetFilteredAdministrationBranchQuery query = new GetFilteredAdministrationBranchQuery(); // GetFilteredAdministrationBranchQuery | 
try {
    Object result = apiInstance.administrationBranchGetAdministrationBranches(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AdministrationBranchApi#administrationBranchGetAdministrationBranches");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredAdministrationBranchQuery**](GetFilteredAdministrationBranchQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="administrationBranchUpdateAdministrationBranch"></a>
# **administrationBranchUpdateAdministrationBranch**
> Object administrationBranchUpdateAdministrationBranch(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AdministrationBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AdministrationBranchApi apiInstance = new AdministrationBranchApi();
Integer number = 56; // Integer | 
AdministrationBranchDto dto = new AdministrationBranchDto(); // AdministrationBranchDto | 
try {
    Object result = apiInstance.administrationBranchUpdateAdministrationBranch(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AdministrationBranchApi#administrationBranchUpdateAdministrationBranch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**AdministrationBranchDto**](AdministrationBranchDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

