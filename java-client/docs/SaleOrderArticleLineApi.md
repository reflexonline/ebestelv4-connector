# SaleOrderArticleLineApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleOrderArticleLineExistsSaleOrderArticleLine**](SaleOrderArticleLineApi.md#saleOrderArticleLineExistsSaleOrderArticleLine) | **GET** /SaleOrderArticleLine/exists/{orderId}/{lineId} | 
[**saleOrderArticleLineGetSaleOrderArticleLine**](SaleOrderArticleLineApi.md#saleOrderArticleLineGetSaleOrderArticleLine) | **GET** /SaleOrderArticleLine/{orderId}/{lineId} | 
[**saleOrderArticleLineGetSaleOrderArticleLines**](SaleOrderArticleLineApi.md#saleOrderArticleLineGetSaleOrderArticleLines) | **POST** /SaleOrderArticleLine/search | 


<a name="saleOrderArticleLineExistsSaleOrderArticleLine"></a>
# **saleOrderArticleLineExistsSaleOrderArticleLine**
> Object saleOrderArticleLineExistsSaleOrderArticleLine(orderId, lineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderArticleLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderArticleLineApi apiInstance = new SaleOrderArticleLineApi();
Integer orderId = 56; // Integer | 
Integer lineId = 56; // Integer | 
try {
    Object result = apiInstance.saleOrderArticleLineExistsSaleOrderArticleLine(orderId, lineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderArticleLineApi#saleOrderArticleLineExistsSaleOrderArticleLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderId** | **Integer**|  |
 **lineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleOrderArticleLineGetSaleOrderArticleLine"></a>
# **saleOrderArticleLineGetSaleOrderArticleLine**
> Object saleOrderArticleLineGetSaleOrderArticleLine(orderId, lineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderArticleLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderArticleLineApi apiInstance = new SaleOrderArticleLineApi();
Integer orderId = 56; // Integer | 
Integer lineId = 56; // Integer | 
try {
    Object result = apiInstance.saleOrderArticleLineGetSaleOrderArticleLine(orderId, lineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderArticleLineApi#saleOrderArticleLineGetSaleOrderArticleLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderId** | **Integer**|  |
 **lineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleOrderArticleLineGetSaleOrderArticleLines"></a>
# **saleOrderArticleLineGetSaleOrderArticleLines**
> Object saleOrderArticleLineGetSaleOrderArticleLines(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderArticleLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderArticleLineApi apiInstance = new SaleOrderArticleLineApi();
GetFilteredSaleOrderArticleLineQuery query = new GetFilteredSaleOrderArticleLineQuery(); // GetFilteredSaleOrderArticleLineQuery | 
try {
    Object result = apiInstance.saleOrderArticleLineGetSaleOrderArticleLines(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderArticleLineApi#saleOrderArticleLineGetSaleOrderArticleLines");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSaleOrderArticleLineQuery**](GetFilteredSaleOrderArticleLineQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

