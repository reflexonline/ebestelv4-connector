# ProductionTaskOutputLineApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productionTaskOutputLineExistsProductionTaskOutputLine**](ProductionTaskOutputLineApi.md#productionTaskOutputLineExistsProductionTaskOutputLine) | **GET** /ProductionTaskOutputLine/exists/{productionTaskId}/{lineId} | 
[**productionTaskOutputLineGetProductionTaskOutputLine**](ProductionTaskOutputLineApi.md#productionTaskOutputLineGetProductionTaskOutputLine) | **GET** /ProductionTaskOutputLine/{productionTaskId}/{lineId} | 
[**productionTaskOutputLineGetProductionTaskOutputLines**](ProductionTaskOutputLineApi.md#productionTaskOutputLineGetProductionTaskOutputLines) | **POST** /ProductionTaskOutputLine/search | 


<a name="productionTaskOutputLineExistsProductionTaskOutputLine"></a>
# **productionTaskOutputLineExistsProductionTaskOutputLine**
> Object productionTaskOutputLineExistsProductionTaskOutputLine(productionTaskId, lineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskOutputLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskOutputLineApi apiInstance = new ProductionTaskOutputLineApi();
Integer productionTaskId = 56; // Integer | 
Integer lineId = 56; // Integer | 
try {
    Object result = apiInstance.productionTaskOutputLineExistsProductionTaskOutputLine(productionTaskId, lineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskOutputLineApi#productionTaskOutputLineExistsProductionTaskOutputLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionTaskId** | **Integer**|  |
 **lineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionTaskOutputLineGetProductionTaskOutputLine"></a>
# **productionTaskOutputLineGetProductionTaskOutputLine**
> Object productionTaskOutputLineGetProductionTaskOutputLine(productionTaskId, lineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskOutputLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskOutputLineApi apiInstance = new ProductionTaskOutputLineApi();
Integer productionTaskId = 56; // Integer | 
Integer lineId = 56; // Integer | 
try {
    Object result = apiInstance.productionTaskOutputLineGetProductionTaskOutputLine(productionTaskId, lineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskOutputLineApi#productionTaskOutputLineGetProductionTaskOutputLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionTaskId** | **Integer**|  |
 **lineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionTaskOutputLineGetProductionTaskOutputLines"></a>
# **productionTaskOutputLineGetProductionTaskOutputLines**
> Object productionTaskOutputLineGetProductionTaskOutputLines(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskOutputLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskOutputLineApi apiInstance = new ProductionTaskOutputLineApi();
GetFilteredProductionTaskOutputLineQuery query = new GetFilteredProductionTaskOutputLineQuery(); // GetFilteredProductionTaskOutputLineQuery | 
try {
    Object result = apiInstance.productionTaskOutputLineGetProductionTaskOutputLines(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskOutputLineApi#productionTaskOutputLineGetProductionTaskOutputLines");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductionTaskOutputLineQuery**](GetFilteredProductionTaskOutputLineQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

