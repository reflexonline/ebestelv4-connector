# AllergenTranslationApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**allergenTranslationCreateAllergenTranslation**](AllergenTranslationApi.md#allergenTranslationCreateAllergenTranslation) | **POST** /AllergenTranslation | 
[**allergenTranslationDeleteAllergenTranslation**](AllergenTranslationApi.md#allergenTranslationDeleteAllergenTranslation) | **DELETE** /AllergenTranslation/{allergenId}/{languageId} | 
[**allergenTranslationExistsAllergenTranslation**](AllergenTranslationApi.md#allergenTranslationExistsAllergenTranslation) | **GET** /AllergenTranslation/exists/{allergenId}/{languageId} | 
[**allergenTranslationGetAllergenTranslation**](AllergenTranslationApi.md#allergenTranslationGetAllergenTranslation) | **GET** /AllergenTranslation/{allergenId}/{languageId} | 
[**allergenTranslationGetAllergenTranslations**](AllergenTranslationApi.md#allergenTranslationGetAllergenTranslations) | **POST** /AllergenTranslation/search | 
[**allergenTranslationUpdateAllergenTranslation**](AllergenTranslationApi.md#allergenTranslationUpdateAllergenTranslation) | **PUT** /AllergenTranslation/{allergenId}/{languageId} | 


<a name="allergenTranslationCreateAllergenTranslation"></a>
# **allergenTranslationCreateAllergenTranslation**
> Object allergenTranslationCreateAllergenTranslation(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AllergenTranslationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AllergenTranslationApi apiInstance = new AllergenTranslationApi();
AllergenTranslationDto dto = new AllergenTranslationDto(); // AllergenTranslationDto | 
try {
    Object result = apiInstance.allergenTranslationCreateAllergenTranslation(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllergenTranslationApi#allergenTranslationCreateAllergenTranslation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**AllergenTranslationDto**](AllergenTranslationDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="allergenTranslationDeleteAllergenTranslation"></a>
# **allergenTranslationDeleteAllergenTranslation**
> Object allergenTranslationDeleteAllergenTranslation(allergenId, languageId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AllergenTranslationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AllergenTranslationApi apiInstance = new AllergenTranslationApi();
Integer allergenId = 56; // Integer | 
Integer languageId = 56; // Integer | 
try {
    Object result = apiInstance.allergenTranslationDeleteAllergenTranslation(allergenId, languageId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllergenTranslationApi#allergenTranslationDeleteAllergenTranslation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allergenId** | **Integer**|  |
 **languageId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="allergenTranslationExistsAllergenTranslation"></a>
# **allergenTranslationExistsAllergenTranslation**
> Object allergenTranslationExistsAllergenTranslation(allergenId, languageId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AllergenTranslationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AllergenTranslationApi apiInstance = new AllergenTranslationApi();
Integer allergenId = 56; // Integer | 
Integer languageId = 56; // Integer | 
try {
    Object result = apiInstance.allergenTranslationExistsAllergenTranslation(allergenId, languageId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllergenTranslationApi#allergenTranslationExistsAllergenTranslation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allergenId** | **Integer**|  |
 **languageId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="allergenTranslationGetAllergenTranslation"></a>
# **allergenTranslationGetAllergenTranslation**
> Object allergenTranslationGetAllergenTranslation(allergenId, languageId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AllergenTranslationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AllergenTranslationApi apiInstance = new AllergenTranslationApi();
Integer allergenId = 56; // Integer | 
Integer languageId = 56; // Integer | 
try {
    Object result = apiInstance.allergenTranslationGetAllergenTranslation(allergenId, languageId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllergenTranslationApi#allergenTranslationGetAllergenTranslation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allergenId** | **Integer**|  |
 **languageId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="allergenTranslationGetAllergenTranslations"></a>
# **allergenTranslationGetAllergenTranslations**
> Object allergenTranslationGetAllergenTranslations(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AllergenTranslationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AllergenTranslationApi apiInstance = new AllergenTranslationApi();
GetFilteredAllergenTranslationQuery query = new GetFilteredAllergenTranslationQuery(); // GetFilteredAllergenTranslationQuery | 
try {
    Object result = apiInstance.allergenTranslationGetAllergenTranslations(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllergenTranslationApi#allergenTranslationGetAllergenTranslations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredAllergenTranslationQuery**](GetFilteredAllergenTranslationQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="allergenTranslationUpdateAllergenTranslation"></a>
# **allergenTranslationUpdateAllergenTranslation**
> Object allergenTranslationUpdateAllergenTranslation(allergenId, languageId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AllergenTranslationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AllergenTranslationApi apiInstance = new AllergenTranslationApi();
Integer allergenId = 56; // Integer | 
Integer languageId = 56; // Integer | 
AllergenTranslationDto dto = new AllergenTranslationDto(); // AllergenTranslationDto | 
try {
    Object result = apiInstance.allergenTranslationUpdateAllergenTranslation(allergenId, languageId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllergenTranslationApi#allergenTranslationUpdateAllergenTranslation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allergenId** | **Integer**|  |
 **languageId** | **Integer**|  |
 **dto** | [**AllergenTranslationDto**](AllergenTranslationDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

