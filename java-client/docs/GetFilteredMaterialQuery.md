
# GetFilteredMaterialQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**materialIdMinValue** | **Integer** |  |  [optional]
**materialIdMaxValue** | **Integer** |  |  [optional]
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



