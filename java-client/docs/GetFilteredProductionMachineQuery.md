
# GetFilteredProductionMachineQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idMinValue** | **Integer** |  |  [optional]
**idMaxValue** | **Integer** |  |  [optional]
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



