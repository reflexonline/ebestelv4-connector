
# GetFilteredSaleArticleCustomerAgreementQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isArticleBlockOverruledMinValue** | **String** |  |  [optional]
**isArticleBlockOverruledMaxValue** | **String** |  |  [optional]
**customerIdMinValue** | **Integer** |  |  [optional]
**customerIdMaxValue** | **Integer** |  |  [optional]
**articleIdMinValue** | **Integer** |  |  [optional]
**articleIdMaxValue** | **Integer** |  |  [optional]
**unappliedArticlePriceAnpDeviationMinValue** | **Double** |  |  [optional]
**unappliedArticlePriceAnpDeviationMaxValue** | **Double** |  |  [optional]



