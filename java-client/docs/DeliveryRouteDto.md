
# DeliveryRouteDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**name** | **String** |  |  [optional]
**departureDay** | **Integer** |  |  [optional]
**deliveryMoment** | [**DeliveryMomentEnum**](#DeliveryMomentEnum) |  |  [optional]
**closingTime** | **String** |  |  [optional]
**departureTime** | **String** |  |  [optional]
**extraText** | **String** |  |  [optional]
**extraTextShowMethod** | **Integer** |  |  [optional]
**extraTHTProductionDays** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="DeliveryMomentEnum"></a>
## Enum: DeliveryMomentEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
MORNING | &quot;Morning&quot;
AFTERNOON | &quot;Afternoon&quot;



