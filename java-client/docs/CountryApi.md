# CountryApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**countryCreateCountry**](CountryApi.md#countryCreateCountry) | **POST** /Country | 
[**countryDeleteCountry**](CountryApi.md#countryDeleteCountry) | **DELETE** /Country/{isoCode} | 
[**countryExistsCountry**](CountryApi.md#countryExistsCountry) | **GET** /Country/exists/{isoCode} | 
[**countryGetCountries**](CountryApi.md#countryGetCountries) | **POST** /Country/search | 
[**countryGetCountry**](CountryApi.md#countryGetCountry) | **GET** /Country/{isoCode} | 
[**countryUpdateCountry**](CountryApi.md#countryUpdateCountry) | **PUT** /Country/{isoCode} | 


<a name="countryCreateCountry"></a>
# **countryCreateCountry**
> Object countryCreateCountry(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CountryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CountryApi apiInstance = new CountryApi();
CountryDto dto = new CountryDto(); // CountryDto | 
try {
    Object result = apiInstance.countryCreateCountry(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CountryApi#countryCreateCountry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**CountryDto**](CountryDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="countryDeleteCountry"></a>
# **countryDeleteCountry**
> Object countryDeleteCountry(isoCode)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CountryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CountryApi apiInstance = new CountryApi();
String isoCode = "isoCode_example"; // String | 
try {
    Object result = apiInstance.countryDeleteCountry(isoCode);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CountryApi#countryDeleteCountry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isoCode** | **String**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="countryExistsCountry"></a>
# **countryExistsCountry**
> Object countryExistsCountry(isoCode)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CountryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CountryApi apiInstance = new CountryApi();
String isoCode = "isoCode_example"; // String | 
try {
    Object result = apiInstance.countryExistsCountry(isoCode);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CountryApi#countryExistsCountry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isoCode** | **String**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="countryGetCountries"></a>
# **countryGetCountries**
> Object countryGetCountries(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CountryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CountryApi apiInstance = new CountryApi();
GetFilteredCountryQuery query = new GetFilteredCountryQuery(); // GetFilteredCountryQuery | 
try {
    Object result = apiInstance.countryGetCountries(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CountryApi#countryGetCountries");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredCountryQuery**](GetFilteredCountryQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="countryGetCountry"></a>
# **countryGetCountry**
> Object countryGetCountry(isoCode)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CountryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CountryApi apiInstance = new CountryApi();
String isoCode = "isoCode_example"; // String | 
try {
    Object result = apiInstance.countryGetCountry(isoCode);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CountryApi#countryGetCountry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isoCode** | **String**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="countryUpdateCountry"></a>
# **countryUpdateCountry**
> Object countryUpdateCountry(isoCode, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CountryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CountryApi apiInstance = new CountryApi();
String isoCode = "isoCode_example"; // String | 
CountryDto dto = new CountryDto(); // CountryDto | 
try {
    Object result = apiInstance.countryUpdateCountry(isoCode, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CountryApi#countryUpdateCountry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isoCode** | **String**|  |
 **dto** | [**CountryDto**](CountryDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

