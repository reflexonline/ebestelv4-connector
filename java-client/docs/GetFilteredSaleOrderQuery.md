
# GetFilteredSaleOrderQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerIdMinValue** | **Integer** |  |  [optional]
**customerIdMaxValue** | **Integer** |  |  [optional]
**orderNumberMinValue** | **Integer** |  |  [optional]
**orderNumberMaxValue** | **Integer** |  |  [optional]
**routeIdMinValue** | **Integer** |  |  [optional]
**routeIdMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]
**packingSlipIdMinValue** | **Integer** |  |  [optional]
**packingSlipIdMaxValue** | **Integer** |  |  [optional]
**headerStateMinValue** | [**HeaderStateMinValueEnum**](#HeaderStateMinValueEnum) |  |  [optional]
**headerStateMaxValue** | [**HeaderStateMaxValueEnum**](#HeaderStateMaxValueEnum) |  |  [optional]
**backupStateMinValue** | **String** |  |  [optional]
**backupStateMaxValue** | **String** |  |  [optional]
**deliveryMethodIdMinValue** | **Integer** |  |  [optional]
**deliveryMethodIdMaxValue** | **Integer** |  |  [optional]
**referenceMinValue** | **String** |  |  [optional]
**referenceMaxValue** | **String** |  |  [optional]
**deliXLOrderIdMinValue** | **String** |  |  [optional]
**deliXLOrderIdMaxValue** | **String** |  |  [optional]
**deliveryDateMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deliveryDateMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**dataWarehouseOrderIsSentMinValue** | **Boolean** |  |  [optional]
**dataWarehouseOrderIsSentMaxValue** | **Boolean** |  |  [optional]
**representativeIdMinValue** | **Integer** |  |  [optional]
**representativeIdMaxValue** | **Integer** |  |  [optional]
**loaderNumberMinValue** | **Integer** |  |  [optional]
**loaderNumberMaxValue** | **Integer** |  |  [optional]
**transporterFreshIdMinValue** | **Integer** |  |  [optional]
**transporterFreshIdMaxValue** | **Integer** |  |  [optional]
**isCalculatedForWssMinValue** | **Integer** |  |  [optional]
**isCalculatedForWssMaxValue** | **Integer** |  |  [optional]
**ediIdMinValue** | **Integer** |  |  [optional]
**ediIdMaxValue** | **Integer** |  |  [optional]
**dataWarehouseOrderSentToHeadOfficeMinValue** | **Boolean** |  |  [optional]
**dataWarehouseOrderSentToHeadOfficeMaxValue** | **Boolean** |  |  [optional]
**multiBranchIdMinValue** | **Integer** |  |  [optional]
**multiBranchIdMaxValue** | **Integer** |  |  [optional]
**paymentConditionIdMinValue** | **Integer** |  |  [optional]
**paymentConditionIdMaxValue** | **Integer** |  |  [optional]
**uniqueOrderIdMinValue** | **Integer** |  |  [optional]
**uniqueOrderIdMaxValue** | **Integer** |  |  [optional]


<a name="HeaderStateMinValueEnum"></a>
## Enum: HeaderStateMinValueEnum
Name | Value
---- | -----
UNKNOWN | &quot;Unknown&quot;
NEW | &quot;New&quot;
NEWECOMMERCE | &quot;NewEcommerce&quot;
OFFER | &quot;Offer&quot;
ACCEPTED | &quot;Accepted&quot;
BACKORDER | &quot;Backorder&quot;
WAIT | &quot;Wait&quot;
HISTORIC | &quot;Historic&quot;
MOBILEINVOICING | &quot;MobileInvoicing&quot;
PRODUCTIONORDER | &quot;ProductionOrder&quot;


<a name="HeaderStateMaxValueEnum"></a>
## Enum: HeaderStateMaxValueEnum
Name | Value
---- | -----
UNKNOWN | &quot;Unknown&quot;
NEW | &quot;New&quot;
NEWECOMMERCE | &quot;NewEcommerce&quot;
OFFER | &quot;Offer&quot;
ACCEPTED | &quot;Accepted&quot;
BACKORDER | &quot;Backorder&quot;
WAIT | &quot;Wait&quot;
HISTORIC | &quot;Historic&quot;
MOBILEINVOICING | &quot;MobileInvoicing&quot;
PRODUCTIONORDER | &quot;ProductionOrder&quot;



