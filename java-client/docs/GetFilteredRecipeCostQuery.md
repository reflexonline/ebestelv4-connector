
# GetFilteredRecipeCostQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipeCostIdMinValue** | **Integer** |  |  [optional]
**recipeCostIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



