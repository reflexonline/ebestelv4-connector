
# GetFilteredPointOfSaleQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pointOfSaleIdMinValue** | **Integer** |  |  [optional]
**pointOfSaleIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



