# SaleInvoiceStatusApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleInvoiceStatusCreateSaleInvoiceStatus**](SaleInvoiceStatusApi.md#saleInvoiceStatusCreateSaleInvoiceStatus) | **POST** /SaleInvoiceStatus | 
[**saleInvoiceStatusDeleteSaleInvoiceStatus**](SaleInvoiceStatusApi.md#saleInvoiceStatusDeleteSaleInvoiceStatus) | **DELETE** /SaleInvoiceStatus/{invoiceStatusId} | 
[**saleInvoiceStatusExistsSaleInvoiceStatus**](SaleInvoiceStatusApi.md#saleInvoiceStatusExistsSaleInvoiceStatus) | **GET** /SaleInvoiceStatus/exists/{invoiceStatusId} | 
[**saleInvoiceStatusGetSaleInvoiceStatus**](SaleInvoiceStatusApi.md#saleInvoiceStatusGetSaleInvoiceStatus) | **GET** /SaleInvoiceStatus/{invoiceStatusId} | 
[**saleInvoiceStatusGetSaleInvoiceStatuses**](SaleInvoiceStatusApi.md#saleInvoiceStatusGetSaleInvoiceStatuses) | **POST** /SaleInvoiceStatus/search | 
[**saleInvoiceStatusUpdateSaleInvoiceStatus**](SaleInvoiceStatusApi.md#saleInvoiceStatusUpdateSaleInvoiceStatus) | **PUT** /SaleInvoiceStatus/{invoiceStatusId} | 


<a name="saleInvoiceStatusCreateSaleInvoiceStatus"></a>
# **saleInvoiceStatusCreateSaleInvoiceStatus**
> Object saleInvoiceStatusCreateSaleInvoiceStatus(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleInvoiceStatusApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleInvoiceStatusApi apiInstance = new SaleInvoiceStatusApi();
SaleInvoiceStatusDto dto = new SaleInvoiceStatusDto(); // SaleInvoiceStatusDto | 
try {
    Object result = apiInstance.saleInvoiceStatusCreateSaleInvoiceStatus(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleInvoiceStatusApi#saleInvoiceStatusCreateSaleInvoiceStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SaleInvoiceStatusDto**](SaleInvoiceStatusDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleInvoiceStatusDeleteSaleInvoiceStatus"></a>
# **saleInvoiceStatusDeleteSaleInvoiceStatus**
> Object saleInvoiceStatusDeleteSaleInvoiceStatus(invoiceStatusId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleInvoiceStatusApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleInvoiceStatusApi apiInstance = new SaleInvoiceStatusApi();
Integer invoiceStatusId = 56; // Integer | 
try {
    Object result = apiInstance.saleInvoiceStatusDeleteSaleInvoiceStatus(invoiceStatusId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleInvoiceStatusApi#saleInvoiceStatusDeleteSaleInvoiceStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceStatusId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleInvoiceStatusExistsSaleInvoiceStatus"></a>
# **saleInvoiceStatusExistsSaleInvoiceStatus**
> Object saleInvoiceStatusExistsSaleInvoiceStatus(invoiceStatusId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleInvoiceStatusApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleInvoiceStatusApi apiInstance = new SaleInvoiceStatusApi();
Integer invoiceStatusId = 56; // Integer | 
try {
    Object result = apiInstance.saleInvoiceStatusExistsSaleInvoiceStatus(invoiceStatusId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleInvoiceStatusApi#saleInvoiceStatusExistsSaleInvoiceStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceStatusId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleInvoiceStatusGetSaleInvoiceStatus"></a>
# **saleInvoiceStatusGetSaleInvoiceStatus**
> Object saleInvoiceStatusGetSaleInvoiceStatus(invoiceStatusId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleInvoiceStatusApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleInvoiceStatusApi apiInstance = new SaleInvoiceStatusApi();
Integer invoiceStatusId = 56; // Integer | 
try {
    Object result = apiInstance.saleInvoiceStatusGetSaleInvoiceStatus(invoiceStatusId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleInvoiceStatusApi#saleInvoiceStatusGetSaleInvoiceStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceStatusId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleInvoiceStatusGetSaleInvoiceStatuses"></a>
# **saleInvoiceStatusGetSaleInvoiceStatuses**
> Object saleInvoiceStatusGetSaleInvoiceStatuses(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleInvoiceStatusApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleInvoiceStatusApi apiInstance = new SaleInvoiceStatusApi();
GetFilteredSaleInvoiceStatusQuery query = new GetFilteredSaleInvoiceStatusQuery(); // GetFilteredSaleInvoiceStatusQuery | 
try {
    Object result = apiInstance.saleInvoiceStatusGetSaleInvoiceStatuses(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleInvoiceStatusApi#saleInvoiceStatusGetSaleInvoiceStatuses");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSaleInvoiceStatusQuery**](GetFilteredSaleInvoiceStatusQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleInvoiceStatusUpdateSaleInvoiceStatus"></a>
# **saleInvoiceStatusUpdateSaleInvoiceStatus**
> Object saleInvoiceStatusUpdateSaleInvoiceStatus(invoiceStatusId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleInvoiceStatusApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleInvoiceStatusApi apiInstance = new SaleInvoiceStatusApi();
Integer invoiceStatusId = 56; // Integer | 
SaleInvoiceStatusDto dto = new SaleInvoiceStatusDto(); // SaleInvoiceStatusDto | 
try {
    Object result = apiInstance.saleInvoiceStatusUpdateSaleInvoiceStatus(invoiceStatusId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleInvoiceStatusApi#saleInvoiceStatusUpdateSaleInvoiceStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceStatusId** | **Integer**|  |
 **dto** | [**SaleInvoiceStatusDto**](SaleInvoiceStatusDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

