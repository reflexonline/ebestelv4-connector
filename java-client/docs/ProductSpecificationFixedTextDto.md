
# ProductSpecificationFixedTextDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productSpecificationFixedTextId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**dummy** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



