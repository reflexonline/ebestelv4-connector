# ProductionStepApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productionStepCreateProductionStep**](ProductionStepApi.md#productionStepCreateProductionStep) | **POST** /ProductionStep | 
[**productionStepDeleteProductionStep**](ProductionStepApi.md#productionStepDeleteProductionStep) | **DELETE** /ProductionStep/{productionStepId} | 
[**productionStepExistsProductionStep**](ProductionStepApi.md#productionStepExistsProductionStep) | **GET** /ProductionStep/exists/{productionStepId} | 
[**productionStepGetProductionStep**](ProductionStepApi.md#productionStepGetProductionStep) | **GET** /ProductionStep/{productionStepId} | 
[**productionStepGetProductionSteps**](ProductionStepApi.md#productionStepGetProductionSteps) | **POST** /ProductionStep/search | 
[**productionStepUpdateProductionStep**](ProductionStepApi.md#productionStepUpdateProductionStep) | **PUT** /ProductionStep/{productionStepId} | 


<a name="productionStepCreateProductionStep"></a>
# **productionStepCreateProductionStep**
> Object productionStepCreateProductionStep(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionStepApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionStepApi apiInstance = new ProductionStepApi();
ProductionStepDto dto = new ProductionStepDto(); // ProductionStepDto | 
try {
    Object result = apiInstance.productionStepCreateProductionStep(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionStepApi#productionStepCreateProductionStep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ProductionStepDto**](ProductionStepDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productionStepDeleteProductionStep"></a>
# **productionStepDeleteProductionStep**
> Object productionStepDeleteProductionStep(productionStepId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionStepApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionStepApi apiInstance = new ProductionStepApi();
Integer productionStepId = 56; // Integer | 
try {
    Object result = apiInstance.productionStepDeleteProductionStep(productionStepId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionStepApi#productionStepDeleteProductionStep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionStepId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionStepExistsProductionStep"></a>
# **productionStepExistsProductionStep**
> Object productionStepExistsProductionStep(productionStepId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionStepApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionStepApi apiInstance = new ProductionStepApi();
Integer productionStepId = 56; // Integer | 
try {
    Object result = apiInstance.productionStepExistsProductionStep(productionStepId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionStepApi#productionStepExistsProductionStep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionStepId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionStepGetProductionStep"></a>
# **productionStepGetProductionStep**
> Object productionStepGetProductionStep(productionStepId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionStepApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionStepApi apiInstance = new ProductionStepApi();
Integer productionStepId = 56; // Integer | 
try {
    Object result = apiInstance.productionStepGetProductionStep(productionStepId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionStepApi#productionStepGetProductionStep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionStepId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionStepGetProductionSteps"></a>
# **productionStepGetProductionSteps**
> Object productionStepGetProductionSteps(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionStepApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionStepApi apiInstance = new ProductionStepApi();
GetFilteredProductionStepQuery query = new GetFilteredProductionStepQuery(); // GetFilteredProductionStepQuery | 
try {
    Object result = apiInstance.productionStepGetProductionSteps(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionStepApi#productionStepGetProductionSteps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductionStepQuery**](GetFilteredProductionStepQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productionStepUpdateProductionStep"></a>
# **productionStepUpdateProductionStep**
> Object productionStepUpdateProductionStep(productionStepId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionStepApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionStepApi apiInstance = new ProductionStepApi();
Integer productionStepId = 56; // Integer | 
ProductionStepDto dto = new ProductionStepDto(); // ProductionStepDto | 
try {
    Object result = apiInstance.productionStepUpdateProductionStep(productionStepId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionStepApi#productionStepUpdateProductionStep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionStepId** | **Integer**|  |
 **dto** | [**ProductionStepDto**](ProductionStepDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

