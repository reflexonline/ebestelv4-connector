
# SpecialOfferFolderDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**specialOfferId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**saleStart** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**saleEnd** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**forAllCustomers** | **Boolean** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



