
# RepresentativeDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**representativeId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**hasAccessToRepresentativeApplication** | **Integer** |  |  [optional]
**password** | **String** |  |  [optional]
**isAllowedToViewArticles** | **Integer** |  |  [optional]
**isAllowedToViewPriceAgreements** | **Integer** |  |  [optional]
**isAllowedToViewOrderHistory** | **Integer** |  |  [optional]
**isAllowedToViewPackingSlipHistory** | **Integer** |  |  [optional]
**isAllowedToViewInvoiceHistory** | **Integer** |  |  [optional]
**isAllowedToViewSaleAnalysis** | **Integer** |  |  [optional]
**hasAccessToCalls** | **Integer** |  |  [optional]
**hasAccessToJournals** | **Integer** |  |  [optional]
**isAllowedToViewDocuments** | **Integer** |  |  [optional]
**limitAccessToOwnRelations** | **Integer** |  |  [optional]
**isAllowedToViewOffers** | **Integer** |  |  [optional]
**isAllowedToViewOutstandingEntries** | **Integer** |  |  [optional]
**isAllowedToViewPurchasePrices** | **Integer** |  |  [optional]
**callDestinationId** | **Integer** |  |  [optional]
**hasAccessToOrderEntry** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



