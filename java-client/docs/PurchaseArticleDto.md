
# PurchaseArticleDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | [**List&lt;LineTagNameDto&gt;**](LineTagNameDto.md) |  |  [optional]
**number** | **Integer** |  | 
**shortName** | **String** |  |  [optional]
**inkoopPrijsOld** | **Float** |  |  [optional]
**bestBeforeDateFrozen** | **Integer** |  |  [optional]
**unitType** | [**UnitTypeEnum**](#UnitTypeEnum) |  |  [optional]
**vatCode** | [**VatCodeEnum**](#VatCodeEnum) |  |  [optional]
**tareWeight** | **Float** |  |  [optional]
**portionWeight** | **Double** |  |  [optional]
**keepStock** | **Integer** |  |  [optional]
**purchaseArticleGroup** | **Integer** |  |  [optional]
**groepPlu** | **Integer** |  |  [optional]
**articleGroupName** | **String** |  |  [optional]
**backorderPercentage** | **Integer** |  |  [optional]
**packedPerUnit** | **Float** |  |  [optional]
**packedPerType** | [**PackedPerTypeEnum**](#PackedPerTypeEnum) |  |  [optional]
**bestBeforeDateFresh** | **Integer** |  |  [optional]
**statistischNumber** | **String** |  |  [optional]
**userDefinableField1** | **String** |  |  [optional]
**userDefinableField2** | **String** |  |  [optional]
**userDefinableField3** | **String** |  |  [optional]
**userDefinableField4** | **String** |  |  [optional]
**userDefinableField5** | **String** |  |  [optional]
**longName** | **String** |  |  [optional]
**fixedStockLocation** | **Integer** |  |  [optional]
**minimumStock** | **Double** |  |  [optional]
**supplier1** | **Integer** |  |  [optional]
**supplier2** | **Integer** |  |  [optional]
**supplier3** | **Integer** |  |  [optional]
**deliveryTimeInDays** | **Integer** |  |  [optional]
**orderSize** | **Float** |  |  [optional]
**eanRetailUnitCode** | **String** |  |  [optional]
**labelNumber** | **Integer** |  |  [optional]
**stockCheckMethod** | [**StockCheckMethodEnum**](#StockCheckMethodEnum) |  |  [optional]
**owhStockValidatioAdministrationBranch1** | **Integer** |  |  [optional]
**owhStockValidatioAdministrationBranch2** | **Integer** |  |  [optional]
**owhStockValidatioAdministrationBranch3** | **Integer** |  |  [optional]
**isBlocked** | **Boolean** |  |  [optional]
**blockedFrom** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**blockedUntil** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**brand** | **String** |  |  [optional]
**wrapping** | **String** |  |  [optional]
**isInventoryPriceEqualToPurchasePrice** | **Boolean** |  |  [optional]
**currentInventoryPrice** | **Double** |  |  [optional]
**purchaseOrderFormula** | **Integer** |  |  [optional]
**maximumStock** | **Double** |  |  [optional]
**autoVoorraadPartij** | **String** |  |  [optional]
**identificationAndRegistrationQuestions** | **String** |  |  [optional]
**scanExternPartij** | **Integer** |  |  [optional]
**rvvNumberErkenning** | **String** |  |  [optional]
**pickLocation** | **String** |  |  [optional]
**stockFactor** | **Double** |  |  [optional]
**amountOnPallet** | **Integer** |  |  [optional]
**amountOnPalletLayer** | **Integer** |  |  [optional]
**bestelArtikel** | **Boolean** |  |  [optional]
**defaultOrderAtAdministrationBranch** | **Integer** |  |  [optional]
**owhToArticleId** | **Integer** |  |  [optional]
**includeInBonusCalculation** | **Boolean** |  |  [optional]
**purchaseArticleOrderGroup** | **Integer** |  |  [optional]
**printGroundMaterialLabel** | **Boolean** |  |  [optional]
**scientificName** | **String** |  |  [optional]
**prodmethode** | **Integer** |  |  [optional]
**traceercodeArtikel** | **Integer** |  |  [optional]
**gflTraceExpirationPeriod** | **Integer** |  |  [optional]
**traceerMethode** | **Integer** |  |  [optional]
**traceOnReference** | **Boolean** |  |  [optional]
**geneticallyModifiedOrganism** | **Integer** |  |  [optional]
**displayENumber** | **Boolean** |  |  [optional]
**displayAllergens** | **Integer** |  |  [optional]
**soortOwhArtikel** | **Boolean** |  |  [optional]
**cbsPortieGelijk** | **Boolean** |  |  [optional]
**cbsPortie** | **Float** |  |  [optional]
**cbsAanVullend** | **Integer** |  |  [optional]
**productSpecificationNumber** | **Integer** |  |  [optional]
**productSpecificationNumberRevision** | **Integer** |  |  [optional]
**productSpecificationDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**previousProductSpecificationNumber** | **Integer** |  |  [optional]
**previousProductSpecificationNumberRevision** | **Integer** |  |  [optional]
**previousProductSpecificationDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**storageState** | [**StorageStateEnum**](#StorageStateEnum) |  |  [optional]
**typePartijNumber** | **Integer** |  |  [optional]
**packingArticle1** | **Integer** |  |  [optional]
**packingArticle2** | **Integer** |  |  [optional]
**defaultValueForAidMaterial** | **Boolean** |  |  [optional]
**createBackorderOnExcessiveStock** | **Boolean** |  |  [optional]
**createBackorderOnStockShortage** | **Boolean** |  |  [optional]
**backorderType** | [**BackorderTypeEnum**](#BackorderTypeEnum) |  |  [optional]
**packingTaxAmount** | **Double** |  |  [optional]
**inventoryType** | [**InventoryTypeEnum**](#InventoryTypeEnum) |  |  [optional]
**cattleCategory** | **Integer** |  |  [optional]
**isTreatPiecesAsWeightInProductionTaskActive** | **Boolean** |  |  [optional]
**isDeviatingUnitActive** | **Boolean** |  |  [optional]
**baseMaterialUnitDescription** | **String** |  |  [optional]
**deviatingUnitFactor** | **Double** |  |  [optional]
**mustExistInBatchStock** | **Integer** |  |  [optional]
**trackPackingBalance** | **Boolean** |  |  [optional]
**usePurchasePricePerBatch** | **Boolean** |  |  [optional]
**countryOfOrigin** | **String** |  |  [optional]
**finalizePreviousGflBatchIfNewGflBatchIsSet** | **Boolean** |  |  [optional]
**formulaComponentI1** | **Integer** |  |  [optional]
**formulaComponentI2** | **Integer** |  |  [optional]
**formulaComponentI3** | **Integer** |  |  [optional]
**formulaComponentD1** | **Double** |  |  [optional]
**formulaComponentD2** | **Double** |  |  [optional]
**formulaComponentD3** | **Double** |  |  [optional]
**gmoHandmatig** | **Integer** |  |  [optional]
**useFTrace** | **Boolean** |  |  [optional]
**garantieTHT** | **Integer** |  |  [optional]
**replacementArtTag** | **Integer** |  |  [optional]
**defaultRegChkMoment** | **Integer** |  |  [optional]
**showCountryOfOrigin** | **Integer** |  |  [optional]
**assignNewBatch** | [**AssignNewBatchEnum**](#AssignNewBatchEnum) |  |  [optional]
**weergaveExtraAlbas** | **Boolean** |  |  [optional]
**purchasePrice** | **Double** |  |  [optional]
**lotDefinition** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="UnitTypeEnum"></a>
## Enum: UnitTypeEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
PIECES | &quot;Pieces&quot;
WEIGHT | &quot;Weight&quot;
WEIGHTFIXEDPORTION | &quot;WeightFixedPortion&quot;


<a name="VatCodeEnum"></a>
## Enum: VatCodeEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
LOW | &quot;Low&quot;
HIGH | &quot;High&quot;
SERVICE | &quot;Service&quot;
AGRICULTURE | &quot;Agriculture&quot;
DAIRY | &quot;Dairy&quot;
PACKING | &quot;Packing&quot;


<a name="PackedPerTypeEnum"></a>
## Enum: PackedPerTypeEnum
Name | Value
---- | -----
PACKEDPERWEIGHTUNIT | &quot;PackedPerWeightUnit&quot;
PACKEDPERPIECE | &quot;PackedPerPiece&quot;


<a name="StockCheckMethodEnum"></a>
## Enum: StockCheckMethodEnum
Name | Value
---- | -----
NOVERIFICATION | &quot;NoVerification&quot;
STANDARD | &quot;Standard&quot;
WEIGHTANDPIECES | &quot;WeightAndPieces&quot;
ONLYPIECES | &quot;OnlyPieces&quot;


<a name="StorageStateEnum"></a>
## Enum: StorageStateEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
FRESH | &quot;Fresh&quot;
FROZEN | &quot;Frozen&quot;
NONPERISHABLE | &quot;NonPerishable&quot;
NOTAPPLICABLE | &quot;NotApplicable&quot;


<a name="BackorderTypeEnum"></a>
## Enum: BackorderTypeEnum
Name | Value
---- | -----
WEIGHT | &quot;Weight&quot;
PIECES | &quot;Pieces&quot;


<a name="InventoryTypeEnum"></a>
## Enum: InventoryTypeEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
FIXEDSETTLEPRICE | &quot;FixedSettlePrice&quot;
AVERAGEPURCHASEPRICE | &quot;AveragePurchasePrice&quot;
LASTPURCHASEPRICE | &quot;LastPurchasePrice&quot;


<a name="AssignNewBatchEnum"></a>
## Enum: AssignNewBatchEnum
Name | Value
---- | -----
USEFLEXSETTINGS | &quot;UseFlexSettings&quot;
PERARTICLE | &quot;PerArticle&quot;
PERREGISTRATION | &quot;PerRegistration&quot;



