# PointOfSaleCorrectionReasonApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**pointOfSaleCorrectionReasonCreatePointOfSaleCorrectionReason**](PointOfSaleCorrectionReasonApi.md#pointOfSaleCorrectionReasonCreatePointOfSaleCorrectionReason) | **POST** /PointOfSaleCorrectionReason | 
[**pointOfSaleCorrectionReasonDeletePointOfSaleCorrectionReason**](PointOfSaleCorrectionReasonApi.md#pointOfSaleCorrectionReasonDeletePointOfSaleCorrectionReason) | **DELETE** /PointOfSaleCorrectionReason/{pointOfSaleCorrectionReasonId} | 
[**pointOfSaleCorrectionReasonExistsPointOfSaleCorrectionReason**](PointOfSaleCorrectionReasonApi.md#pointOfSaleCorrectionReasonExistsPointOfSaleCorrectionReason) | **GET** /PointOfSaleCorrectionReason/exists/{pointOfSaleCorrectionReasonId} | 
[**pointOfSaleCorrectionReasonGetPointOfSaleCorrectionReason**](PointOfSaleCorrectionReasonApi.md#pointOfSaleCorrectionReasonGetPointOfSaleCorrectionReason) | **GET** /PointOfSaleCorrectionReason/{pointOfSaleCorrectionReasonId} | 
[**pointOfSaleCorrectionReasonGetPointOfSaleCorrectionReasons**](PointOfSaleCorrectionReasonApi.md#pointOfSaleCorrectionReasonGetPointOfSaleCorrectionReasons) | **POST** /PointOfSaleCorrectionReason/search | 
[**pointOfSaleCorrectionReasonUpdatePointOfSaleCorrectionReason**](PointOfSaleCorrectionReasonApi.md#pointOfSaleCorrectionReasonUpdatePointOfSaleCorrectionReason) | **PUT** /PointOfSaleCorrectionReason/{pointOfSaleCorrectionReasonId} | 


<a name="pointOfSaleCorrectionReasonCreatePointOfSaleCorrectionReason"></a>
# **pointOfSaleCorrectionReasonCreatePointOfSaleCorrectionReason**
> Object pointOfSaleCorrectionReasonCreatePointOfSaleCorrectionReason(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PointOfSaleCorrectionReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PointOfSaleCorrectionReasonApi apiInstance = new PointOfSaleCorrectionReasonApi();
PointOfSaleCorrectionReasonDto dto = new PointOfSaleCorrectionReasonDto(); // PointOfSaleCorrectionReasonDto | 
try {
    Object result = apiInstance.pointOfSaleCorrectionReasonCreatePointOfSaleCorrectionReason(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PointOfSaleCorrectionReasonApi#pointOfSaleCorrectionReasonCreatePointOfSaleCorrectionReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PointOfSaleCorrectionReasonDto**](PointOfSaleCorrectionReasonDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="pointOfSaleCorrectionReasonDeletePointOfSaleCorrectionReason"></a>
# **pointOfSaleCorrectionReasonDeletePointOfSaleCorrectionReason**
> Object pointOfSaleCorrectionReasonDeletePointOfSaleCorrectionReason(pointOfSaleCorrectionReasonId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PointOfSaleCorrectionReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PointOfSaleCorrectionReasonApi apiInstance = new PointOfSaleCorrectionReasonApi();
Integer pointOfSaleCorrectionReasonId = 56; // Integer | 
try {
    Object result = apiInstance.pointOfSaleCorrectionReasonDeletePointOfSaleCorrectionReason(pointOfSaleCorrectionReasonId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PointOfSaleCorrectionReasonApi#pointOfSaleCorrectionReasonDeletePointOfSaleCorrectionReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pointOfSaleCorrectionReasonId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="pointOfSaleCorrectionReasonExistsPointOfSaleCorrectionReason"></a>
# **pointOfSaleCorrectionReasonExistsPointOfSaleCorrectionReason**
> Object pointOfSaleCorrectionReasonExistsPointOfSaleCorrectionReason(pointOfSaleCorrectionReasonId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PointOfSaleCorrectionReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PointOfSaleCorrectionReasonApi apiInstance = new PointOfSaleCorrectionReasonApi();
Integer pointOfSaleCorrectionReasonId = 56; // Integer | 
try {
    Object result = apiInstance.pointOfSaleCorrectionReasonExistsPointOfSaleCorrectionReason(pointOfSaleCorrectionReasonId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PointOfSaleCorrectionReasonApi#pointOfSaleCorrectionReasonExistsPointOfSaleCorrectionReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pointOfSaleCorrectionReasonId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="pointOfSaleCorrectionReasonGetPointOfSaleCorrectionReason"></a>
# **pointOfSaleCorrectionReasonGetPointOfSaleCorrectionReason**
> Object pointOfSaleCorrectionReasonGetPointOfSaleCorrectionReason(pointOfSaleCorrectionReasonId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PointOfSaleCorrectionReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PointOfSaleCorrectionReasonApi apiInstance = new PointOfSaleCorrectionReasonApi();
Integer pointOfSaleCorrectionReasonId = 56; // Integer | 
try {
    Object result = apiInstance.pointOfSaleCorrectionReasonGetPointOfSaleCorrectionReason(pointOfSaleCorrectionReasonId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PointOfSaleCorrectionReasonApi#pointOfSaleCorrectionReasonGetPointOfSaleCorrectionReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pointOfSaleCorrectionReasonId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="pointOfSaleCorrectionReasonGetPointOfSaleCorrectionReasons"></a>
# **pointOfSaleCorrectionReasonGetPointOfSaleCorrectionReasons**
> Object pointOfSaleCorrectionReasonGetPointOfSaleCorrectionReasons(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PointOfSaleCorrectionReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PointOfSaleCorrectionReasonApi apiInstance = new PointOfSaleCorrectionReasonApi();
GetFilteredPointOfSaleCorrectionReasonQuery query = new GetFilteredPointOfSaleCorrectionReasonQuery(); // GetFilteredPointOfSaleCorrectionReasonQuery | 
try {
    Object result = apiInstance.pointOfSaleCorrectionReasonGetPointOfSaleCorrectionReasons(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PointOfSaleCorrectionReasonApi#pointOfSaleCorrectionReasonGetPointOfSaleCorrectionReasons");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPointOfSaleCorrectionReasonQuery**](GetFilteredPointOfSaleCorrectionReasonQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="pointOfSaleCorrectionReasonUpdatePointOfSaleCorrectionReason"></a>
# **pointOfSaleCorrectionReasonUpdatePointOfSaleCorrectionReason**
> Object pointOfSaleCorrectionReasonUpdatePointOfSaleCorrectionReason(pointOfSaleCorrectionReasonId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PointOfSaleCorrectionReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PointOfSaleCorrectionReasonApi apiInstance = new PointOfSaleCorrectionReasonApi();
Integer pointOfSaleCorrectionReasonId = 56; // Integer | 
PointOfSaleCorrectionReasonDto dto = new PointOfSaleCorrectionReasonDto(); // PointOfSaleCorrectionReasonDto | 
try {
    Object result = apiInstance.pointOfSaleCorrectionReasonUpdatePointOfSaleCorrectionReason(pointOfSaleCorrectionReasonId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PointOfSaleCorrectionReasonApi#pointOfSaleCorrectionReasonUpdatePointOfSaleCorrectionReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pointOfSaleCorrectionReasonId** | **Integer**|  |
 **dto** | [**PointOfSaleCorrectionReasonDto**](PointOfSaleCorrectionReasonDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

