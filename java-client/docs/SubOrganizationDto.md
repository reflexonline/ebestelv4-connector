
# SubOrganizationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subOrganizationId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



