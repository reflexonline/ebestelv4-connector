
# GetFilteredBaseMaterialQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**categorialIdMinValue** | **Integer** |  |  [optional]
**categorialIdMaxValue** | **Integer** |  |  [optional]
**eNumberMinValue** | **String** |  |  [optional]
**eNumberMaxValue** | **String** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



