
# AccountingPeriodDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**year** | **Integer** |  | 
**period** | **Integer** |  | 
**fromDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**untilDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



