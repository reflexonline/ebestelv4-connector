
# GetFilteredPurchaseShortageReasonQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recordIdMinValue** | **Integer** |  |  [optional]
**recordIdMaxValue** | **Integer** |  |  [optional]
**shortageReasonIdMinValue** | **Integer** |  |  [optional]
**shortageReasonIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



