
# GetFilteredProductionStepQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productionStepIdMinValue** | **Integer** |  |  [optional]
**productionStepIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]
**productionFilterGroupIdMinValue** | **Integer** |  |  [optional]
**productionFilterGroupIdMaxValue** | **Integer** |  |  [optional]
**userKeyMinValue** | **String** |  |  [optional]
**userKeyMaxValue** | **String** |  |  [optional]



