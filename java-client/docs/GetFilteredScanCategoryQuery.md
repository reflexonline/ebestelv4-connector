
# GetFilteredScanCategoryQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idMinValue** | **Integer** |  |  [optional]
**idMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]



