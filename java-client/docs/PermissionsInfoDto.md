
# PermissionsInfoDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applications** | [**List&lt;IdentifierDto&gt;**](IdentifierDto.md) |  |  [optional]
**modules** | [**List&lt;IdentifierDto&gt;**](IdentifierDto.md) |  |  [optional]
**permissions** | [**List&lt;PermissionInfoDto&gt;**](PermissionInfoDto.md) |  |  [optional]



