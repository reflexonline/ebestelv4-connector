# ShortageReasonApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**shortageReasonCreateShortageReason**](ShortageReasonApi.md#shortageReasonCreateShortageReason) | **POST** /ShortageReason | 
[**shortageReasonDeleteShortageReason**](ShortageReasonApi.md#shortageReasonDeleteShortageReason) | **DELETE** /ShortageReason/{shortageReasonId} | 
[**shortageReasonExistsShortageReason**](ShortageReasonApi.md#shortageReasonExistsShortageReason) | **GET** /ShortageReason/exists/{shortageReasonId} | 
[**shortageReasonGetShortageReason**](ShortageReasonApi.md#shortageReasonGetShortageReason) | **GET** /ShortageReason/{shortageReasonId} | 
[**shortageReasonGetShortageReasons**](ShortageReasonApi.md#shortageReasonGetShortageReasons) | **POST** /ShortageReason/search | 
[**shortageReasonUpdateShortageReason**](ShortageReasonApi.md#shortageReasonUpdateShortageReason) | **PUT** /ShortageReason/{shortageReasonId} | 


<a name="shortageReasonCreateShortageReason"></a>
# **shortageReasonCreateShortageReason**
> Object shortageReasonCreateShortageReason(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ShortageReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ShortageReasonApi apiInstance = new ShortageReasonApi();
ShortageReasonDto dto = new ShortageReasonDto(); // ShortageReasonDto | 
try {
    Object result = apiInstance.shortageReasonCreateShortageReason(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ShortageReasonApi#shortageReasonCreateShortageReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ShortageReasonDto**](ShortageReasonDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="shortageReasonDeleteShortageReason"></a>
# **shortageReasonDeleteShortageReason**
> Object shortageReasonDeleteShortageReason(shortageReasonId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ShortageReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ShortageReasonApi apiInstance = new ShortageReasonApi();
Integer shortageReasonId = 56; // Integer | 
try {
    Object result = apiInstance.shortageReasonDeleteShortageReason(shortageReasonId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ShortageReasonApi#shortageReasonDeleteShortageReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shortageReasonId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="shortageReasonExistsShortageReason"></a>
# **shortageReasonExistsShortageReason**
> Object shortageReasonExistsShortageReason(shortageReasonId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ShortageReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ShortageReasonApi apiInstance = new ShortageReasonApi();
Integer shortageReasonId = 56; // Integer | 
try {
    Object result = apiInstance.shortageReasonExistsShortageReason(shortageReasonId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ShortageReasonApi#shortageReasonExistsShortageReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shortageReasonId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="shortageReasonGetShortageReason"></a>
# **shortageReasonGetShortageReason**
> Object shortageReasonGetShortageReason(shortageReasonId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ShortageReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ShortageReasonApi apiInstance = new ShortageReasonApi();
Integer shortageReasonId = 56; // Integer | 
try {
    Object result = apiInstance.shortageReasonGetShortageReason(shortageReasonId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ShortageReasonApi#shortageReasonGetShortageReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shortageReasonId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="shortageReasonGetShortageReasons"></a>
# **shortageReasonGetShortageReasons**
> Object shortageReasonGetShortageReasons(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ShortageReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ShortageReasonApi apiInstance = new ShortageReasonApi();
GetFilteredShortageReasonQuery query = new GetFilteredShortageReasonQuery(); // GetFilteredShortageReasonQuery | 
try {
    Object result = apiInstance.shortageReasonGetShortageReasons(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ShortageReasonApi#shortageReasonGetShortageReasons");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredShortageReasonQuery**](GetFilteredShortageReasonQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="shortageReasonUpdateShortageReason"></a>
# **shortageReasonUpdateShortageReason**
> Object shortageReasonUpdateShortageReason(shortageReasonId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ShortageReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ShortageReasonApi apiInstance = new ShortageReasonApi();
Integer shortageReasonId = 56; // Integer | 
ShortageReasonDto dto = new ShortageReasonDto(); // ShortageReasonDto | 
try {
    Object result = apiInstance.shortageReasonUpdateShortageReason(shortageReasonId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ShortageReasonApi#shortageReasonUpdateShortageReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shortageReasonId** | **Integer**|  |
 **dto** | [**ShortageReasonDto**](ShortageReasonDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

