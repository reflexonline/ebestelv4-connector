
# GetFilteredCurrencyQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]
**muntMinValue** | **String** |  |  [optional]
**muntMaxValue** | **String** |  |  [optional]
**vatScenarioIdMinValue** | **Integer** |  |  [optional]
**vatScenarioIdMaxValue** | **Integer** |  |  [optional]



