
# ProductSpecificationMainCategoryDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productSpecificationMainCategorieId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



