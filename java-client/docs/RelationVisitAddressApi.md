# RelationVisitAddressApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**relationVisitAddressExistsRelationVisitAddress**](RelationVisitAddressApi.md#relationVisitAddressExistsRelationVisitAddress) | **GET** /RelationVisitAddress/exists/{number} | 
[**relationVisitAddressGetRelationVisitAddress**](RelationVisitAddressApi.md#relationVisitAddressGetRelationVisitAddress) | **GET** /RelationVisitAddress/{number} | 
[**relationVisitAddressGetRelationVisitAddresses**](RelationVisitAddressApi.md#relationVisitAddressGetRelationVisitAddresses) | **POST** /RelationVisitAddress/search | 


<a name="relationVisitAddressExistsRelationVisitAddress"></a>
# **relationVisitAddressExistsRelationVisitAddress**
> Object relationVisitAddressExistsRelationVisitAddress(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RelationVisitAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RelationVisitAddressApi apiInstance = new RelationVisitAddressApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.relationVisitAddressExistsRelationVisitAddress(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationVisitAddressApi#relationVisitAddressExistsRelationVisitAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="relationVisitAddressGetRelationVisitAddress"></a>
# **relationVisitAddressGetRelationVisitAddress**
> Object relationVisitAddressGetRelationVisitAddress(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RelationVisitAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RelationVisitAddressApi apiInstance = new RelationVisitAddressApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.relationVisitAddressGetRelationVisitAddress(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationVisitAddressApi#relationVisitAddressGetRelationVisitAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="relationVisitAddressGetRelationVisitAddresses"></a>
# **relationVisitAddressGetRelationVisitAddresses**
> Object relationVisitAddressGetRelationVisitAddresses(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RelationVisitAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RelationVisitAddressApi apiInstance = new RelationVisitAddressApi();
GetFilteredRelationVisitAddressQuery query = new GetFilteredRelationVisitAddressQuery(); // GetFilteredRelationVisitAddressQuery | 
try {
    Object result = apiInstance.relationVisitAddressGetRelationVisitAddresses(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationVisitAddressApi#relationVisitAddressGetRelationVisitAddresses");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredRelationVisitAddressQuery**](GetFilteredRelationVisitAddressQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

