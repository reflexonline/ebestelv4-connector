# SaleArticleGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleArticleGroupCreateSaleArticleGroup**](SaleArticleGroupApi.md#saleArticleGroupCreateSaleArticleGroup) | **POST** /SaleArticleGroup | 
[**saleArticleGroupDeleteSaleArticleGroup**](SaleArticleGroupApi.md#saleArticleGroupDeleteSaleArticleGroup) | **DELETE** /SaleArticleGroup/{number} | 
[**saleArticleGroupExistsSaleArticleGroup**](SaleArticleGroupApi.md#saleArticleGroupExistsSaleArticleGroup) | **GET** /SaleArticleGroup/exists/{number} | 
[**saleArticleGroupGetSaleArticleGroup**](SaleArticleGroupApi.md#saleArticleGroupGetSaleArticleGroup) | **GET** /SaleArticleGroup/{number} | 
[**saleArticleGroupGetSaleArticleGroups**](SaleArticleGroupApi.md#saleArticleGroupGetSaleArticleGroups) | **POST** /SaleArticleGroup/search | 
[**saleArticleGroupUpdateSaleArticleGroup**](SaleArticleGroupApi.md#saleArticleGroupUpdateSaleArticleGroup) | **PUT** /SaleArticleGroup/{number} | 


<a name="saleArticleGroupCreateSaleArticleGroup"></a>
# **saleArticleGroupCreateSaleArticleGroup**
> Object saleArticleGroupCreateSaleArticleGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleGroupApi apiInstance = new SaleArticleGroupApi();
SaleArticleGroupDto dto = new SaleArticleGroupDto(); // SaleArticleGroupDto | 
try {
    Object result = apiInstance.saleArticleGroupCreateSaleArticleGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleGroupApi#saleArticleGroupCreateSaleArticleGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SaleArticleGroupDto**](SaleArticleGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleGroupDeleteSaleArticleGroup"></a>
# **saleArticleGroupDeleteSaleArticleGroup**
> Object saleArticleGroupDeleteSaleArticleGroup(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleGroupApi apiInstance = new SaleArticleGroupApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleGroupDeleteSaleArticleGroup(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleGroupApi#saleArticleGroupDeleteSaleArticleGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleGroupExistsSaleArticleGroup"></a>
# **saleArticleGroupExistsSaleArticleGroup**
> Object saleArticleGroupExistsSaleArticleGroup(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleGroupApi apiInstance = new SaleArticleGroupApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleGroupExistsSaleArticleGroup(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleGroupApi#saleArticleGroupExistsSaleArticleGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleGroupGetSaleArticleGroup"></a>
# **saleArticleGroupGetSaleArticleGroup**
> Object saleArticleGroupGetSaleArticleGroup(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleGroupApi apiInstance = new SaleArticleGroupApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleGroupGetSaleArticleGroup(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleGroupApi#saleArticleGroupGetSaleArticleGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleGroupGetSaleArticleGroups"></a>
# **saleArticleGroupGetSaleArticleGroups**
> Object saleArticleGroupGetSaleArticleGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleGroupApi apiInstance = new SaleArticleGroupApi();
GetFilteredSaleArticleGroupQuery query = new GetFilteredSaleArticleGroupQuery(); // GetFilteredSaleArticleGroupQuery | 
try {
    Object result = apiInstance.saleArticleGroupGetSaleArticleGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleGroupApi#saleArticleGroupGetSaleArticleGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSaleArticleGroupQuery**](GetFilteredSaleArticleGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleGroupUpdateSaleArticleGroup"></a>
# **saleArticleGroupUpdateSaleArticleGroup**
> Object saleArticleGroupUpdateSaleArticleGroup(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleGroupApi apiInstance = new SaleArticleGroupApi();
Integer number = 56; // Integer | 
SaleArticleGroupDto dto = new SaleArticleGroupDto(); // SaleArticleGroupDto | 
try {
    Object result = apiInstance.saleArticleGroupUpdateSaleArticleGroup(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleGroupApi#saleArticleGroupUpdateSaleArticleGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**SaleArticleGroupDto**](SaleArticleGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

