
# GetFilteredProductionFlexFilterGroupQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productionFlexFilterGroupIdMinValue** | **Integer** |  |  [optional]
**productionFlexFilterGroupIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



