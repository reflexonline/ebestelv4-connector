# LabelPrinterApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**labelPrinterCreateLabelPrinter**](LabelPrinterApi.md#labelPrinterCreateLabelPrinter) | **POST** /LabelPrinter | 
[**labelPrinterDeleteLabelPrinter**](LabelPrinterApi.md#labelPrinterDeleteLabelPrinter) | **DELETE** /LabelPrinter/{labelPrinterId} | 
[**labelPrinterExistsLabelPrinter**](LabelPrinterApi.md#labelPrinterExistsLabelPrinter) | **GET** /LabelPrinter/exists/{labelPrinterId} | 
[**labelPrinterGetLabelPrinter**](LabelPrinterApi.md#labelPrinterGetLabelPrinter) | **GET** /LabelPrinter/{labelPrinterId} | 
[**labelPrinterGetLabelPrinters**](LabelPrinterApi.md#labelPrinterGetLabelPrinters) | **POST** /LabelPrinter/search | 
[**labelPrinterPrintLabel**](LabelPrinterApi.md#labelPrinterPrintLabel) | **POST** /SaleOrder/{orderId}/PrintLabel/{labelId} | 
[**labelPrinterUpdateLabelPrinter**](LabelPrinterApi.md#labelPrinterUpdateLabelPrinter) | **PUT** /LabelPrinter/{labelPrinterId} | 


<a name="labelPrinterCreateLabelPrinter"></a>
# **labelPrinterCreateLabelPrinter**
> Object labelPrinterCreateLabelPrinter(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LabelPrinterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LabelPrinterApi apiInstance = new LabelPrinterApi();
LabelPrinterDto dto = new LabelPrinterDto(); // LabelPrinterDto | 
try {
    Object result = apiInstance.labelPrinterCreateLabelPrinter(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelPrinterApi#labelPrinterCreateLabelPrinter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**LabelPrinterDto**](LabelPrinterDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="labelPrinterDeleteLabelPrinter"></a>
# **labelPrinterDeleteLabelPrinter**
> Object labelPrinterDeleteLabelPrinter(labelPrinterId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LabelPrinterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LabelPrinterApi apiInstance = new LabelPrinterApi();
Integer labelPrinterId = 56; // Integer | 
try {
    Object result = apiInstance.labelPrinterDeleteLabelPrinter(labelPrinterId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelPrinterApi#labelPrinterDeleteLabelPrinter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **labelPrinterId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="labelPrinterExistsLabelPrinter"></a>
# **labelPrinterExistsLabelPrinter**
> Object labelPrinterExistsLabelPrinter(labelPrinterId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LabelPrinterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LabelPrinterApi apiInstance = new LabelPrinterApi();
Integer labelPrinterId = 56; // Integer | 
try {
    Object result = apiInstance.labelPrinterExistsLabelPrinter(labelPrinterId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelPrinterApi#labelPrinterExistsLabelPrinter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **labelPrinterId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="labelPrinterGetLabelPrinter"></a>
# **labelPrinterGetLabelPrinter**
> Object labelPrinterGetLabelPrinter(labelPrinterId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LabelPrinterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LabelPrinterApi apiInstance = new LabelPrinterApi();
Integer labelPrinterId = 56; // Integer | 
try {
    Object result = apiInstance.labelPrinterGetLabelPrinter(labelPrinterId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelPrinterApi#labelPrinterGetLabelPrinter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **labelPrinterId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="labelPrinterGetLabelPrinters"></a>
# **labelPrinterGetLabelPrinters**
> Object labelPrinterGetLabelPrinters(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LabelPrinterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LabelPrinterApi apiInstance = new LabelPrinterApi();
GetFilteredLabelPrinterQuery query = new GetFilteredLabelPrinterQuery(); // GetFilteredLabelPrinterQuery | 
try {
    Object result = apiInstance.labelPrinterGetLabelPrinters(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelPrinterApi#labelPrinterGetLabelPrinters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredLabelPrinterQuery**](GetFilteredLabelPrinterQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="labelPrinterPrintLabel"></a>
# **labelPrinterPrintLabel**
> Object labelPrinterPrintLabel(orderId, labelId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LabelPrinterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LabelPrinterApi apiInstance = new LabelPrinterApi();
Integer orderId = 56; // Integer | 
Integer labelId = 56; // Integer | 
PrintLabelDto dto = new PrintLabelDto(); // PrintLabelDto | 
try {
    Object result = apiInstance.labelPrinterPrintLabel(orderId, labelId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelPrinterApi#labelPrinterPrintLabel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderId** | **Integer**|  |
 **labelId** | **Integer**|  |
 **dto** | [**PrintLabelDto**](PrintLabelDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="labelPrinterUpdateLabelPrinter"></a>
# **labelPrinterUpdateLabelPrinter**
> Object labelPrinterUpdateLabelPrinter(labelPrinterId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LabelPrinterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LabelPrinterApi apiInstance = new LabelPrinterApi();
Integer labelPrinterId = 56; // Integer | 
LabelPrinterDto dto = new LabelPrinterDto(); // LabelPrinterDto | 
try {
    Object result = apiInstance.labelPrinterUpdateLabelPrinter(labelPrinterId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelPrinterApi#labelPrinterUpdateLabelPrinter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **labelPrinterId** | **Integer**|  |
 **dto** | [**LabelPrinterDto**](LabelPrinterDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

