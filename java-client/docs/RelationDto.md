
# RelationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relationDeliveryAddress** | [**RelationDeliveryAddressDto**](RelationDeliveryAddressDto.md) |  |  [optional]
**relationPostalAddress** | [**RelationPostalAddressDto**](RelationPostalAddressDto.md) |  |  [optional]
**relationVisitAddress** | [**RelationVisitAddressDto**](RelationVisitAddressDto.md) |  |  [optional]
**number** | **Integer** |  | 
**searchName** | **String** |  |  [optional]
**bankAccountNumber** | **String** |  |  [optional]
**soort** | **String** |  |  [optional]
**vertegenwoordiger** | **Integer** |  |  [optional]
**kerstKaart** | **String** |  |  [optional]
**soortwaarschuwing** | **Integer** |  |  [optional]
**isBlocked** | **Boolean** |  |  [optional]
**documentCount** | **Integer** |  |  [optional]
**debiteurSF** | **String** |  |  [optional]
**crediteurSF** | **String** |  |  [optional]
**waarschuwing** | **String** |  |  [optional]
**vrijveldAlfa1** | **String** |  |  [optional]
**vrijveldAlfa2** | **String** |  |  [optional]
**vrijveldAlfa3** | **String** |  |  [optional]
**vrijveldAlfa4** | **String** |  |  [optional]
**vrijveldAlfa5** | **String** |  |  [optional]
**userDefinedFieldNumeric1** | **Float** |  |  [optional]
**userDefinedFieldNumeric2** | **Float** |  |  [optional]
**userDefinedFieldNumeric3** | **Float** |  |  [optional]
**userDefinedFieldNumeric4** | **Float** |  |  [optional]
**userDefinedFieldNumeric5** | **Float** |  |  [optional]
**freeFieldBoolean1** | **Integer** |  |  [optional]
**freeFieldBoolean2** | **Integer** |  |  [optional]
**freeFieldBoolean3** | **Integer** |  |  [optional]
**freeFieldBoolean4** | **Integer** |  |  [optional]
**freeFieldBoolean5** | **Integer** |  |  [optional]
**email** | **String** |  |  [optional]
**website** | **String** |  |  [optional]
**marktSegment** | **Integer** |  |  [optional]
**organisatiecode** | **Integer** |  |  [optional]
**giroAccountNumber** | **String** |  |  [optional]
**subOrganisatie** | **Integer** |  |  [optional]
**bankIdentifierCode** | **String** |  |  [optional]
**internationalBankAccountNumber** | **String** |  |  [optional]
**ibaN2** | **String** |  |  [optional]
**biC2** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



