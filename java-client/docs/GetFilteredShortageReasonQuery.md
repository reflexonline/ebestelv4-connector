
# GetFilteredShortageReasonQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shortageReasonIdMinValue** | **Integer** |  |  [optional]
**shortageReasonIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



