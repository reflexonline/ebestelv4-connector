
# FlexCountryDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**countryId** | **Integer** |  | 
**isoCode** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**isEuropeanUnionCountry** | **Boolean** |  |  [optional]
**countryCustoms** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



