# TransporterApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**transporterCreateTransporter**](TransporterApi.md#transporterCreateTransporter) | **POST** /Transporter | 
[**transporterDeleteTransporter**](TransporterApi.md#transporterDeleteTransporter) | **DELETE** /Transporter/{transporterId} | 
[**transporterExistsTransporter**](TransporterApi.md#transporterExistsTransporter) | **GET** /Transporter/exists/{transporterId} | 
[**transporterGetTransporter**](TransporterApi.md#transporterGetTransporter) | **GET** /Transporter/{transporterId} | 
[**transporterGetTransporters**](TransporterApi.md#transporterGetTransporters) | **POST** /Transporter/search | 
[**transporterUpdateTransporter**](TransporterApi.md#transporterUpdateTransporter) | **PUT** /Transporter/{transporterId} | 


<a name="transporterCreateTransporter"></a>
# **transporterCreateTransporter**
> Object transporterCreateTransporter(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TransporterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

TransporterApi apiInstance = new TransporterApi();
TransporterDto dto = new TransporterDto(); // TransporterDto | 
try {
    Object result = apiInstance.transporterCreateTransporter(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TransporterApi#transporterCreateTransporter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**TransporterDto**](TransporterDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="transporterDeleteTransporter"></a>
# **transporterDeleteTransporter**
> Object transporterDeleteTransporter(transporterId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TransporterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

TransporterApi apiInstance = new TransporterApi();
Integer transporterId = 56; // Integer | 
try {
    Object result = apiInstance.transporterDeleteTransporter(transporterId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TransporterApi#transporterDeleteTransporter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transporterId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="transporterExistsTransporter"></a>
# **transporterExistsTransporter**
> Object transporterExistsTransporter(transporterId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TransporterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

TransporterApi apiInstance = new TransporterApi();
Integer transporterId = 56; // Integer | 
try {
    Object result = apiInstance.transporterExistsTransporter(transporterId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TransporterApi#transporterExistsTransporter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transporterId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="transporterGetTransporter"></a>
# **transporterGetTransporter**
> Object transporterGetTransporter(transporterId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TransporterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

TransporterApi apiInstance = new TransporterApi();
Integer transporterId = 56; // Integer | 
try {
    Object result = apiInstance.transporterGetTransporter(transporterId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TransporterApi#transporterGetTransporter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transporterId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="transporterGetTransporters"></a>
# **transporterGetTransporters**
> Object transporterGetTransporters(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TransporterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

TransporterApi apiInstance = new TransporterApi();
GetFilteredTransporterQuery query = new GetFilteredTransporterQuery(); // GetFilteredTransporterQuery | 
try {
    Object result = apiInstance.transporterGetTransporters(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TransporterApi#transporterGetTransporters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredTransporterQuery**](GetFilteredTransporterQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="transporterUpdateTransporter"></a>
# **transporterUpdateTransporter**
> Object transporterUpdateTransporter(transporterId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TransporterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

TransporterApi apiInstance = new TransporterApi();
Integer transporterId = 56; // Integer | 
TransporterDto dto = new TransporterDto(); // TransporterDto | 
try {
    Object result = apiInstance.transporterUpdateTransporter(transporterId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TransporterApi#transporterUpdateTransporter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transporterId** | **Integer**|  |
 **dto** | [**TransporterDto**](TransporterDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

