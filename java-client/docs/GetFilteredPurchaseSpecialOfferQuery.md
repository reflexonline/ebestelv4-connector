
# GetFilteredPurchaseSpecialOfferQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**articleIdMinValue** | **Integer** |  |  [optional]
**articleIdMaxValue** | **Integer** |  |  [optional]
**supplierIdMinValue** | **Integer** |  |  [optional]
**supplierIdMaxValue** | **Integer** |  |  [optional]
**dateIndexMinValue** | **Integer** |  |  [optional]
**dateIndexMaxValue** | **Integer** |  |  [optional]
**fromDateMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**fromDateMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**untilDateMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**untilDateMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**isActionMinValue** | **Boolean** |  |  [optional]
**isActionMaxValue** | **Boolean** |  |  [optional]
**purchaseSpecialOfferTypeMinValue** | [**PurchaseSpecialOfferTypeMinValueEnum**](#PurchaseSpecialOfferTypeMinValueEnum) |  |  [optional]
**purchaseSpecialOfferTypeMaxValue** | [**PurchaseSpecialOfferTypeMaxValueEnum**](#PurchaseSpecialOfferTypeMaxValueEnum) |  |  [optional]
**groupTextIdMinValue** | **Integer** |  |  [optional]
**groupTextIdMaxValue** | **Integer** |  |  [optional]
**tagIdMinValue** | **Integer** |  |  [optional]
**tagIdMaxValue** | **Integer** |  |  [optional]


<a name="PurchaseSpecialOfferTypeMinValueEnum"></a>
## Enum: PurchaseSpecialOfferTypeMinValueEnum
Name | Value
---- | -----
SUPPLIER | &quot;Supplier&quot;


<a name="PurchaseSpecialOfferTypeMaxValueEnum"></a>
## Enum: PurchaseSpecialOfferTypeMaxValueEnum
Name | Value
---- | -----
SUPPLIER | &quot;Supplier&quot;



