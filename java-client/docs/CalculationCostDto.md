
# CalculationCostDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**calculationCostId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**vastVar** | **String** |  |  [optional]
**bedragOld** | **String** |  |  [optional]
**amount** | **Double** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



