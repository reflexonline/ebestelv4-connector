
# GetFilteredSaleArticleQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**shortNameMinValue** | **String** |  |  [optional]
**shortNameMaxValue** | **String** |  |  [optional]
**productionGroupIdMinValue** | **Integer** |  |  [optional]
**productionGroupIdMaxValue** | **Integer** |  |  [optional]
**articleGroupIdMinValue** | **Integer** |  |  [optional]
**articleGroupIdMaxValue** | **Integer** |  |  [optional]
**pickLocationMinValue** | **String** |  |  [optional]
**pickLocationMaxValue** | **String** |  |  [optional]
**packingArticleIdMinValue** | **Integer** |  |  [optional]
**packingArticleIdMaxValue** | **Integer** |  |  [optional]
**packingArticleNameMinValue** | **String** |  |  [optional]
**packingArticleNameMaxValue** | **String** |  |  [optional]
**stockPurchaseArticleIdMinValue** | **Integer** |  |  [optional]
**stockPurchaseArticleIdMaxValue** | **Integer** |  |  [optional]
**packingArticleId1MinValue** | **Integer** |  |  [optional]
**packingArticleId1MaxValue** | **Integer** |  |  [optional]
**productionGroupId2MinValue** | **Integer** |  |  [optional]
**productionGroupId2MaxValue** | **Integer** |  |  [optional]
**fixedStockLocationIdMinValue** | **Integer** |  |  [optional]
**fixedStockLocationIdMaxValue** | **Integer** |  |  [optional]
**substituteArticleIdMinValue** | **Integer** |  |  [optional]
**substituteArticleIdMaxValue** | **Integer** |  |  [optional]
**eanRetailUnitCodeMinValue** | **String** |  |  [optional]
**eanRetailUnitCodeMaxValue** | **String** |  |  [optional]
**productionGroupId3MinValue** | **Integer** |  |  [optional]
**productionGroupId3MaxValue** | **Integer** |  |  [optional]
**productionGroupId4MinValue** | **Integer** |  |  [optional]
**productionGroupId4MaxValue** | **Integer** |  |  [optional]
**productionGroupId5MinValue** | **Integer** |  |  [optional]
**productionGroupId5MaxValue** | **Integer** |  |  [optional]
**owhToBranchMinValue** | **Integer** |  |  [optional]
**owhToBranchMaxValue** | **Integer** |  |  [optional]
**owhArticleIdBranchMinValue** | **Integer** |  |  [optional]
**owhArticleIdBranchMaxValue** | **Integer** |  |  [optional]
**brandMinValue** | **String** |  |  [optional]
**brandMaxValue** | **String** |  |  [optional]
**packingArticleId2MinValue** | **Integer** |  |  [optional]
**packingArticleId2MaxValue** | **Integer** |  |  [optional]
**subgroupIdMinValue** | **Integer** |  |  [optional]
**subgroupIdMaxValue** | **Integer** |  |  [optional]
**consumerGroupIdMinValue** | **Integer** |  |  [optional]
**consumerGroupIdMaxValue** | **Integer** |  |  [optional]
**substituteGroupIdMinValue** | **Integer** |  |  [optional]
**substituteGroupIdMaxValue** | **Integer** |  |  [optional]
**eanTradeUnitCodeMinValue** | **String** |  |  [optional]
**eanTradeUnitCodeMaxValue** | **String** |  |  [optional]
**priceLookUpCodeMinValue** | **Integer** |  |  [optional]
**priceLookUpCodeMaxValue** | **Integer** |  |  [optional]



