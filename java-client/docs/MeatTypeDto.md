
# MeatTypeDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**omschrijving** | **String** |  |  [optional]
**collageenKantel** | **Double** |  |  [optional]
**collageenOmschrijvin** | **String** |  |  [optional]
**vetKantel** | **Double** |  |  [optional]
**vetOmschrijving** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



