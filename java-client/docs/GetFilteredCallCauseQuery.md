
# GetFilteredCallCauseQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**zoekArgMinValue** | **String** |  |  [optional]
**zoekArgMaxValue** | **String** |  |  [optional]



