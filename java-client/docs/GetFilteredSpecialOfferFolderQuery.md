
# GetFilteredSpecialOfferFolderQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**specialOfferIdMinValue** | **Integer** |  |  [optional]
**specialOfferIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]
**saleStartMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**saleStartMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**saleEndMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**saleEndMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]



