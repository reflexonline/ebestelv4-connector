
# GetFilteredAllergenTranslationQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**typeMinValue** | [**TypeMinValueEnum**](#TypeMinValueEnum) |  |  [optional]
**typeMaxValue** | [**TypeMaxValueEnum**](#TypeMaxValueEnum) |  |  [optional]
**allergenIdMinValue** | **Integer** |  |  [optional]
**allergenIdMaxValue** | **Integer** |  |  [optional]
**languageIdMinValue** | **Integer** |  |  [optional]
**languageIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]


<a name="TypeMinValueEnum"></a>
## Enum: TypeMinValueEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
TRANSLATESALEARTICLEGROUP | &quot;TranslateSaleArticleGroup&quot;
TRANSLATESALEARTICLEMAINGROUP | &quot;TranslateSaleArticleMainGroup&quot;
TRANSLATESALEARTICLESUBGROUP | &quot;TranslateSaleArticleSubGroup&quot;
TRANSLATEBASEMATERIAL | &quot;TranslateBaseMaterial&quot;
TRANSLATEALLERGEN | &quot;TranslateAllergen&quot;
TRANSLATENUTRITIONAL | &quot;TranslateNutritional&quot;
TRANSLATENUTRITIONALUNIT | &quot;TranslateNutritionalUnit&quot;
TRANSLATERECIPE | &quot;TranslateRecipe&quot;
LOTEXTRAFIELD | &quot;LotExtraField&quot;
SALEARTICLEALLERGENDESCRIPTION | &quot;SaleArticleAllergenDescription&quot;
SALEARTICLENUTRITIONALDESCRIPTION | &quot;SaleArticleNutritionalDescription&quot;
SALEARTICLEFOTOPATHSHORT | &quot;SaleArticleFotoPathShort&quot;
TRANSLATESCIENTIFICNAME | &quot;TranslateScientificName&quot;
SALEARTICLEPRODUCTSPECIFICATIONURL | &quot;SaleArticleProductSpecificationUrl&quot;
SALEARTICLEFOTOPATH | &quot;SaleArticleFotoPath&quot;
TRANSLATEMEATTYPE | &quot;TranslateMeatType&quot;
TRANSLATECOLLAGEN | &quot;TranslateCollagen&quot;
TRANSLATEFAT | &quot;TranslateFat&quot;
TRANSLATEPRODUCTSPECIFICATIONCATEGORIE | &quot;TranslateProductSpecificationCategorie&quot;
TRANSLATEPRODUCTSPECIFICATIONLINE | &quot;TranslateProductSpecificationLine&quot;
TRANSLATECATEGORAAL | &quot;TranslateCategoraal&quot;
TRANSLATEGENERICTEXT | &quot;TranslateGenericText&quot;
TRANSLATEPURCHASEARTICLESPECIFICPRODUCTSPECIFICATIONLINE | &quot;TranslatePurchaseArticleSpecificProductSpecificationLine&quot;
TRANSLATESALEARTICLESPECIFICPRODUCTSPECIFICATIONLINE | &quot;TranslateSaleArticleSpecificProductSpecificationLine&quot;
TRANSLATETYPELABELINGMACHINEINGREDIENTLINES | &quot;TranslateTypeLabelingMachineIngredientLines&quot;
TRANSLATEPRODUCTSPECIFICATIONMAINCATEGORIE | &quot;TranslateProductSpecificationMainCategorie&quot;
TRANSLATEPRODUCTSPECIFICATIONUNIT | &quot;TranslateProductSpecificationUnit&quot;
TRANSLATEPRODUCTSPECIFICATIONFIXEDTEXT | &quot;TranslateProductSpecificationFixedText&quot;
TRANSLATESALEARTICLECUSTOMERDESCRIPTION | &quot;TranslateSaleArticleCustomerDescription&quot;
TRANSLATECAPTUREREGION | &quot;TranslateCaptureRegion&quot;
TRANSLATECOUNTRYNAME | &quot;TranslateCountryName&quot;
TRANSLATEFLEXCOUNTRYNAME | &quot;TranslateFlexCountryName&quot;
TRANSLATECREDITREASON | &quot;TranslateCreditReason&quot;
TRANSLATESALEINVOICESTATUS | &quot;TranslateSaleInvoiceStatus&quot;
TRANSLATETAGNAMESHORT | &quot;TranslateTagNameShort&quot;
TRANSLATETAGNAME | &quot;TranslateTagName&quot;
TRANSLATEPURCHASEARTICLEPACKAGING | &quot;TranslatePurchaseArticlePackaging&quot;
TRANSLATESALEARTICLEPACKAGING | &quot;TranslateSaleArticlePackaging&quot;
SPECIALOFFERTRANSLATION | &quot;SpecialOfferTranslation&quot;
TRANSLATEDELIVERYMETHOD | &quot;TranslateDeliveryMethod&quot;
TRANSLATEPURCHASEARTICLEDESCRIPTION | &quot;TranslatePurchaseArticleDescription&quot;
TRANSLATESALEARTICLEDESCRIPTION | &quot;TranslateSaleArticleDescription&quot;
TRANSLATEFISHINGGEARDESCRIPTION | &quot;TranslateFishingGearDescription&quot;
ORGANIZATIONARTICLE | &quot;OrganizationArticle&quot;
ORGANIZATIONSPECIFICEANCODE | &quot;OrganizationSpecificEanCode&quot;


<a name="TypeMaxValueEnum"></a>
## Enum: TypeMaxValueEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
TRANSLATESALEARTICLEGROUP | &quot;TranslateSaleArticleGroup&quot;
TRANSLATESALEARTICLEMAINGROUP | &quot;TranslateSaleArticleMainGroup&quot;
TRANSLATESALEARTICLESUBGROUP | &quot;TranslateSaleArticleSubGroup&quot;
TRANSLATEBASEMATERIAL | &quot;TranslateBaseMaterial&quot;
TRANSLATEALLERGEN | &quot;TranslateAllergen&quot;
TRANSLATENUTRITIONAL | &quot;TranslateNutritional&quot;
TRANSLATENUTRITIONALUNIT | &quot;TranslateNutritionalUnit&quot;
TRANSLATERECIPE | &quot;TranslateRecipe&quot;
LOTEXTRAFIELD | &quot;LotExtraField&quot;
SALEARTICLEALLERGENDESCRIPTION | &quot;SaleArticleAllergenDescription&quot;
SALEARTICLENUTRITIONALDESCRIPTION | &quot;SaleArticleNutritionalDescription&quot;
SALEARTICLEFOTOPATHSHORT | &quot;SaleArticleFotoPathShort&quot;
TRANSLATESCIENTIFICNAME | &quot;TranslateScientificName&quot;
SALEARTICLEPRODUCTSPECIFICATIONURL | &quot;SaleArticleProductSpecificationUrl&quot;
SALEARTICLEFOTOPATH | &quot;SaleArticleFotoPath&quot;
TRANSLATEMEATTYPE | &quot;TranslateMeatType&quot;
TRANSLATECOLLAGEN | &quot;TranslateCollagen&quot;
TRANSLATEFAT | &quot;TranslateFat&quot;
TRANSLATEPRODUCTSPECIFICATIONCATEGORIE | &quot;TranslateProductSpecificationCategorie&quot;
TRANSLATEPRODUCTSPECIFICATIONLINE | &quot;TranslateProductSpecificationLine&quot;
TRANSLATECATEGORAAL | &quot;TranslateCategoraal&quot;
TRANSLATEGENERICTEXT | &quot;TranslateGenericText&quot;
TRANSLATEPURCHASEARTICLESPECIFICPRODUCTSPECIFICATIONLINE | &quot;TranslatePurchaseArticleSpecificProductSpecificationLine&quot;
TRANSLATESALEARTICLESPECIFICPRODUCTSPECIFICATIONLINE | &quot;TranslateSaleArticleSpecificProductSpecificationLine&quot;
TRANSLATETYPELABELINGMACHINEINGREDIENTLINES | &quot;TranslateTypeLabelingMachineIngredientLines&quot;
TRANSLATEPRODUCTSPECIFICATIONMAINCATEGORIE | &quot;TranslateProductSpecificationMainCategorie&quot;
TRANSLATEPRODUCTSPECIFICATIONUNIT | &quot;TranslateProductSpecificationUnit&quot;
TRANSLATEPRODUCTSPECIFICATIONFIXEDTEXT | &quot;TranslateProductSpecificationFixedText&quot;
TRANSLATESALEARTICLECUSTOMERDESCRIPTION | &quot;TranslateSaleArticleCustomerDescription&quot;
TRANSLATECAPTUREREGION | &quot;TranslateCaptureRegion&quot;
TRANSLATECOUNTRYNAME | &quot;TranslateCountryName&quot;
TRANSLATEFLEXCOUNTRYNAME | &quot;TranslateFlexCountryName&quot;
TRANSLATECREDITREASON | &quot;TranslateCreditReason&quot;
TRANSLATESALEINVOICESTATUS | &quot;TranslateSaleInvoiceStatus&quot;
TRANSLATETAGNAMESHORT | &quot;TranslateTagNameShort&quot;
TRANSLATETAGNAME | &quot;TranslateTagName&quot;
TRANSLATEPURCHASEARTICLEPACKAGING | &quot;TranslatePurchaseArticlePackaging&quot;
TRANSLATESALEARTICLEPACKAGING | &quot;TranslateSaleArticlePackaging&quot;
SPECIALOFFERTRANSLATION | &quot;SpecialOfferTranslation&quot;
TRANSLATEDELIVERYMETHOD | &quot;TranslateDeliveryMethod&quot;
TRANSLATEPURCHASEARTICLEDESCRIPTION | &quot;TranslatePurchaseArticleDescription&quot;
TRANSLATESALEARTICLEDESCRIPTION | &quot;TranslateSaleArticleDescription&quot;
TRANSLATEFISHINGGEARDESCRIPTION | &quot;TranslateFishingGearDescription&quot;
ORGANIZATIONARTICLE | &quot;OrganizationArticle&quot;
ORGANIZATIONSPECIFICEANCODE | &quot;OrganizationSpecificEanCode&quot;



