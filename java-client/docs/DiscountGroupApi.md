# DiscountGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**discountGroupCreateDiscountGroup**](DiscountGroupApi.md#discountGroupCreateDiscountGroup) | **POST** /DiscountGroup | 
[**discountGroupDeleteDiscountGroup**](DiscountGroupApi.md#discountGroupDeleteDiscountGroup) | **DELETE** /DiscountGroup/{number} | 
[**discountGroupExistsDiscountGroup**](DiscountGroupApi.md#discountGroupExistsDiscountGroup) | **GET** /DiscountGroup/exists/{number} | 
[**discountGroupGetDiscountGroup**](DiscountGroupApi.md#discountGroupGetDiscountGroup) | **GET** /DiscountGroup/{number} | 
[**discountGroupGetDiscountGroups**](DiscountGroupApi.md#discountGroupGetDiscountGroups) | **POST** /DiscountGroup/search | 
[**discountGroupUpdateDiscountGroup**](DiscountGroupApi.md#discountGroupUpdateDiscountGroup) | **PUT** /DiscountGroup/{number} | 


<a name="discountGroupCreateDiscountGroup"></a>
# **discountGroupCreateDiscountGroup**
> Object discountGroupCreateDiscountGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DiscountGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DiscountGroupApi apiInstance = new DiscountGroupApi();
DiscountGroupDto dto = new DiscountGroupDto(); // DiscountGroupDto | 
try {
    Object result = apiInstance.discountGroupCreateDiscountGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DiscountGroupApi#discountGroupCreateDiscountGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**DiscountGroupDto**](DiscountGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="discountGroupDeleteDiscountGroup"></a>
# **discountGroupDeleteDiscountGroup**
> Object discountGroupDeleteDiscountGroup(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DiscountGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DiscountGroupApi apiInstance = new DiscountGroupApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.discountGroupDeleteDiscountGroup(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DiscountGroupApi#discountGroupDeleteDiscountGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="discountGroupExistsDiscountGroup"></a>
# **discountGroupExistsDiscountGroup**
> Object discountGroupExistsDiscountGroup(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DiscountGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DiscountGroupApi apiInstance = new DiscountGroupApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.discountGroupExistsDiscountGroup(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DiscountGroupApi#discountGroupExistsDiscountGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="discountGroupGetDiscountGroup"></a>
# **discountGroupGetDiscountGroup**
> Object discountGroupGetDiscountGroup(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DiscountGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DiscountGroupApi apiInstance = new DiscountGroupApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.discountGroupGetDiscountGroup(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DiscountGroupApi#discountGroupGetDiscountGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="discountGroupGetDiscountGroups"></a>
# **discountGroupGetDiscountGroups**
> Object discountGroupGetDiscountGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DiscountGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DiscountGroupApi apiInstance = new DiscountGroupApi();
GetFilteredDiscountGroupQuery query = new GetFilteredDiscountGroupQuery(); // GetFilteredDiscountGroupQuery | 
try {
    Object result = apiInstance.discountGroupGetDiscountGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DiscountGroupApi#discountGroupGetDiscountGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredDiscountGroupQuery**](GetFilteredDiscountGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="discountGroupUpdateDiscountGroup"></a>
# **discountGroupUpdateDiscountGroup**
> Object discountGroupUpdateDiscountGroup(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DiscountGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DiscountGroupApi apiInstance = new DiscountGroupApi();
Integer number = 56; // Integer | 
DiscountGroupDto dto = new DiscountGroupDto(); // DiscountGroupDto | 
try {
    Object result = apiInstance.discountGroupUpdateDiscountGroup(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DiscountGroupApi#discountGroupUpdateDiscountGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**DiscountGroupDto**](DiscountGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

