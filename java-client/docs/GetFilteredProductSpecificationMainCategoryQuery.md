
# GetFilteredProductSpecificationMainCategoryQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productSpecificationMainCategorieIdMinValue** | **Integer** |  |  [optional]
**productSpecificationMainCategorieIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



