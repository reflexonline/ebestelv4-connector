# StockLocationApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stockLocationCreateStockLocation**](StockLocationApi.md#stockLocationCreateStockLocation) | **POST** /StockLocation | 
[**stockLocationDeleteStockLocation**](StockLocationApi.md#stockLocationDeleteStockLocation) | **DELETE** /StockLocation/{stockLocationId} | 
[**stockLocationExistsStockLocation**](StockLocationApi.md#stockLocationExistsStockLocation) | **GET** /StockLocation/exists/{stockLocationId} | 
[**stockLocationGetStockLocation**](StockLocationApi.md#stockLocationGetStockLocation) | **GET** /StockLocation/{stockLocationId} | 
[**stockLocationGetStockLocations**](StockLocationApi.md#stockLocationGetStockLocations) | **POST** /StockLocation/search | 
[**stockLocationUpdateStockLocation**](StockLocationApi.md#stockLocationUpdateStockLocation) | **PUT** /StockLocation/{stockLocationId} | 


<a name="stockLocationCreateStockLocation"></a>
# **stockLocationCreateStockLocation**
> Object stockLocationCreateStockLocation(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockLocationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockLocationApi apiInstance = new StockLocationApi();
StockLocationDto dto = new StockLocationDto(); // StockLocationDto | 
try {
    Object result = apiInstance.stockLocationCreateStockLocation(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockLocationApi#stockLocationCreateStockLocation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**StockLocationDto**](StockLocationDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="stockLocationDeleteStockLocation"></a>
# **stockLocationDeleteStockLocation**
> Object stockLocationDeleteStockLocation(stockLocationId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockLocationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockLocationApi apiInstance = new StockLocationApi();
Integer stockLocationId = 56; // Integer | 
try {
    Object result = apiInstance.stockLocationDeleteStockLocation(stockLocationId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockLocationApi#stockLocationDeleteStockLocation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stockLocationId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="stockLocationExistsStockLocation"></a>
# **stockLocationExistsStockLocation**
> Object stockLocationExistsStockLocation(stockLocationId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockLocationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockLocationApi apiInstance = new StockLocationApi();
Integer stockLocationId = 56; // Integer | 
try {
    Object result = apiInstance.stockLocationExistsStockLocation(stockLocationId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockLocationApi#stockLocationExistsStockLocation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stockLocationId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="stockLocationGetStockLocation"></a>
# **stockLocationGetStockLocation**
> Object stockLocationGetStockLocation(stockLocationId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockLocationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockLocationApi apiInstance = new StockLocationApi();
Integer stockLocationId = 56; // Integer | 
try {
    Object result = apiInstance.stockLocationGetStockLocation(stockLocationId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockLocationApi#stockLocationGetStockLocation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stockLocationId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="stockLocationGetStockLocations"></a>
# **stockLocationGetStockLocations**
> Object stockLocationGetStockLocations(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockLocationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockLocationApi apiInstance = new StockLocationApi();
GetFilteredStockLocationQuery query = new GetFilteredStockLocationQuery(); // GetFilteredStockLocationQuery | 
try {
    Object result = apiInstance.stockLocationGetStockLocations(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockLocationApi#stockLocationGetStockLocations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredStockLocationQuery**](GetFilteredStockLocationQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="stockLocationUpdateStockLocation"></a>
# **stockLocationUpdateStockLocation**
> Object stockLocationUpdateStockLocation(stockLocationId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockLocationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockLocationApi apiInstance = new StockLocationApi();
Integer stockLocationId = 56; // Integer | 
StockLocationDto dto = new StockLocationDto(); // StockLocationDto | 
try {
    Object result = apiInstance.stockLocationUpdateStockLocation(stockLocationId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockLocationApi#stockLocationUpdateStockLocation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stockLocationId** | **Integer**|  |
 **dto** | [**StockLocationDto**](StockLocationDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

