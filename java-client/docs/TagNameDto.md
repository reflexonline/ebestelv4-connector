
# TagNameDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer** | [**List&lt;LineCustomerDto&gt;**](LineCustomerDto.md) |  |  [optional]
**saleArticle** | [**List&lt;LineSaleArticleDto&gt;**](LineSaleArticleDto.md) |  |  [optional]
**supplier** | [**List&lt;LineSupplierDto&gt;**](LineSupplierDto.md) |  |  [optional]
**purchaseArticle** | [**List&lt;LinePurchaseArticleDto&gt;**](LinePurchaseArticleDto.md) |  |  [optional]
**tagId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



