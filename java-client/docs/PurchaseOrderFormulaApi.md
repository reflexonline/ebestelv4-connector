# PurchaseOrderFormulaApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purchaseOrderFormulaCreatePurchaseOrderFormula**](PurchaseOrderFormulaApi.md#purchaseOrderFormulaCreatePurchaseOrderFormula) | **POST** /PurchaseOrderFormula | 
[**purchaseOrderFormulaDeletePurchaseOrderFormula**](PurchaseOrderFormulaApi.md#purchaseOrderFormulaDeletePurchaseOrderFormula) | **DELETE** /PurchaseOrderFormula/{number} | 
[**purchaseOrderFormulaExistsPurchaseOrderFormula**](PurchaseOrderFormulaApi.md#purchaseOrderFormulaExistsPurchaseOrderFormula) | **GET** /PurchaseOrderFormula/exists/{number} | 
[**purchaseOrderFormulaGetPurchaseOrderFormula**](PurchaseOrderFormulaApi.md#purchaseOrderFormulaGetPurchaseOrderFormula) | **GET** /PurchaseOrderFormula/{number} | 
[**purchaseOrderFormulaGetPurchaseOrderFormulas**](PurchaseOrderFormulaApi.md#purchaseOrderFormulaGetPurchaseOrderFormulas) | **POST** /PurchaseOrderFormula/search | 
[**purchaseOrderFormulaUpdatePurchaseOrderFormula**](PurchaseOrderFormulaApi.md#purchaseOrderFormulaUpdatePurchaseOrderFormula) | **PUT** /PurchaseOrderFormula/{number} | 


<a name="purchaseOrderFormulaCreatePurchaseOrderFormula"></a>
# **purchaseOrderFormulaCreatePurchaseOrderFormula**
> Object purchaseOrderFormulaCreatePurchaseOrderFormula(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderFormulaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderFormulaApi apiInstance = new PurchaseOrderFormulaApi();
PurchaseOrderFormulaDto dto = new PurchaseOrderFormulaDto(); // PurchaseOrderFormulaDto | 
try {
    Object result = apiInstance.purchaseOrderFormulaCreatePurchaseOrderFormula(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderFormulaApi#purchaseOrderFormulaCreatePurchaseOrderFormula");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PurchaseOrderFormulaDto**](PurchaseOrderFormulaDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseOrderFormulaDeletePurchaseOrderFormula"></a>
# **purchaseOrderFormulaDeletePurchaseOrderFormula**
> Object purchaseOrderFormulaDeletePurchaseOrderFormula(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderFormulaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderFormulaApi apiInstance = new PurchaseOrderFormulaApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.purchaseOrderFormulaDeletePurchaseOrderFormula(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderFormulaApi#purchaseOrderFormulaDeletePurchaseOrderFormula");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseOrderFormulaExistsPurchaseOrderFormula"></a>
# **purchaseOrderFormulaExistsPurchaseOrderFormula**
> Object purchaseOrderFormulaExistsPurchaseOrderFormula(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderFormulaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderFormulaApi apiInstance = new PurchaseOrderFormulaApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.purchaseOrderFormulaExistsPurchaseOrderFormula(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderFormulaApi#purchaseOrderFormulaExistsPurchaseOrderFormula");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseOrderFormulaGetPurchaseOrderFormula"></a>
# **purchaseOrderFormulaGetPurchaseOrderFormula**
> Object purchaseOrderFormulaGetPurchaseOrderFormula(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderFormulaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderFormulaApi apiInstance = new PurchaseOrderFormulaApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.purchaseOrderFormulaGetPurchaseOrderFormula(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderFormulaApi#purchaseOrderFormulaGetPurchaseOrderFormula");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseOrderFormulaGetPurchaseOrderFormulas"></a>
# **purchaseOrderFormulaGetPurchaseOrderFormulas**
> Object purchaseOrderFormulaGetPurchaseOrderFormulas(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderFormulaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderFormulaApi apiInstance = new PurchaseOrderFormulaApi();
GetFilteredPurchaseOrderFormulaQuery query = new GetFilteredPurchaseOrderFormulaQuery(); // GetFilteredPurchaseOrderFormulaQuery | 
try {
    Object result = apiInstance.purchaseOrderFormulaGetPurchaseOrderFormulas(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderFormulaApi#purchaseOrderFormulaGetPurchaseOrderFormulas");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPurchaseOrderFormulaQuery**](GetFilteredPurchaseOrderFormulaQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseOrderFormulaUpdatePurchaseOrderFormula"></a>
# **purchaseOrderFormulaUpdatePurchaseOrderFormula**
> Object purchaseOrderFormulaUpdatePurchaseOrderFormula(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderFormulaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderFormulaApi apiInstance = new PurchaseOrderFormulaApi();
Integer number = 56; // Integer | 
PurchaseOrderFormulaDto dto = new PurchaseOrderFormulaDto(); // PurchaseOrderFormulaDto | 
try {
    Object result = apiInstance.purchaseOrderFormulaUpdatePurchaseOrderFormula(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderFormulaApi#purchaseOrderFormulaUpdatePurchaseOrderFormula");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**PurchaseOrderFormulaDto**](PurchaseOrderFormulaDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

