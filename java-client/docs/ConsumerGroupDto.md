
# ConsumerGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**consumerGroupId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



