# ContactGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**contactGroupCreateContactGroup**](ContactGroupApi.md#contactGroupCreateContactGroup) | **POST** /ContactGroup | 
[**contactGroupDeleteContactGroup**](ContactGroupApi.md#contactGroupDeleteContactGroup) | **DELETE** /ContactGroup/{contactGroupId} | 
[**contactGroupExistsContactGroup**](ContactGroupApi.md#contactGroupExistsContactGroup) | **GET** /ContactGroup/exists/{contactGroupId} | 
[**contactGroupGetContactGroup**](ContactGroupApi.md#contactGroupGetContactGroup) | **GET** /ContactGroup/{contactGroupId} | 
[**contactGroupGetContactGroups**](ContactGroupApi.md#contactGroupGetContactGroups) | **POST** /ContactGroup/search | 
[**contactGroupUpdateContactGroup**](ContactGroupApi.md#contactGroupUpdateContactGroup) | **PUT** /ContactGroup/{contactGroupId} | 


<a name="contactGroupCreateContactGroup"></a>
# **contactGroupCreateContactGroup**
> Object contactGroupCreateContactGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ContactGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ContactGroupApi apiInstance = new ContactGroupApi();
ContactGroupDto dto = new ContactGroupDto(); // ContactGroupDto | 
try {
    Object result = apiInstance.contactGroupCreateContactGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ContactGroupApi#contactGroupCreateContactGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ContactGroupDto**](ContactGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="contactGroupDeleteContactGroup"></a>
# **contactGroupDeleteContactGroup**
> Object contactGroupDeleteContactGroup(contactGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ContactGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ContactGroupApi apiInstance = new ContactGroupApi();
Integer contactGroupId = 56; // Integer | 
try {
    Object result = apiInstance.contactGroupDeleteContactGroup(contactGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ContactGroupApi#contactGroupDeleteContactGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contactGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="contactGroupExistsContactGroup"></a>
# **contactGroupExistsContactGroup**
> Object contactGroupExistsContactGroup(contactGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ContactGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ContactGroupApi apiInstance = new ContactGroupApi();
Integer contactGroupId = 56; // Integer | 
try {
    Object result = apiInstance.contactGroupExistsContactGroup(contactGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ContactGroupApi#contactGroupExistsContactGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contactGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="contactGroupGetContactGroup"></a>
# **contactGroupGetContactGroup**
> Object contactGroupGetContactGroup(contactGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ContactGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ContactGroupApi apiInstance = new ContactGroupApi();
Integer contactGroupId = 56; // Integer | 
try {
    Object result = apiInstance.contactGroupGetContactGroup(contactGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ContactGroupApi#contactGroupGetContactGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contactGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="contactGroupGetContactGroups"></a>
# **contactGroupGetContactGroups**
> Object contactGroupGetContactGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ContactGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ContactGroupApi apiInstance = new ContactGroupApi();
GetFilteredContactGroupQuery query = new GetFilteredContactGroupQuery(); // GetFilteredContactGroupQuery | 
try {
    Object result = apiInstance.contactGroupGetContactGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ContactGroupApi#contactGroupGetContactGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredContactGroupQuery**](GetFilteredContactGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="contactGroupUpdateContactGroup"></a>
# **contactGroupUpdateContactGroup**
> Object contactGroupUpdateContactGroup(contactGroupId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ContactGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ContactGroupApi apiInstance = new ContactGroupApi();
Integer contactGroupId = 56; // Integer | 
ContactGroupDto dto = new ContactGroupDto(); // ContactGroupDto | 
try {
    Object result = apiInstance.contactGroupUpdateContactGroup(contactGroupId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ContactGroupApi#contactGroupUpdateContactGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contactGroupId** | **Integer**|  |
 **dto** | [**ContactGroupDto**](ContactGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

