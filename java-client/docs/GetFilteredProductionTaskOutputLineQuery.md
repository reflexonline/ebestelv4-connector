
# GetFilteredProductionTaskOutputLineQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productionTaskIdMinValue** | **Integer** |  |  [optional]
**productionTaskIdMaxValue** | **Integer** |  |  [optional]
**lineIdMinValue** | **Integer** |  |  [optional]
**lineIdMaxValue** | **Integer** |  |  [optional]
**articleIdMinValue** | **Integer** |  |  [optional]
**articleIdMaxValue** | **Integer** |  |  [optional]
**recipeIdMinValue** | **Integer** |  |  [optional]
**recipeIdMaxValue** | **Integer** |  |  [optional]
**recipeDerivativeIdMinValue** | **Integer** |  |  [optional]
**recipeDerivativeIdMaxValue** | **Integer** |  |  [optional]
**filterGroupIdMinValue** | **Integer** |  |  [optional]
**filterGroupIdMaxValue** | **Integer** |  |  [optional]
**destinationOrderIdMinValue** | **Integer** |  |  [optional]
**destinationOrderIdMaxValue** | **Integer** |  |  [optional]
**destinationOrderLineIdMinValue** | **Integer** |  |  [optional]
**destinationOrderLineIdMaxValue** | **Integer** |  |  [optional]
**manualDestinationOrderIdMinValue** | **Integer** |  |  [optional]
**manualDestinationOrderIdMaxValue** | **Integer** |  |  [optional]
**manualDestinationOrderLineIdMinValue** | **Integer** |  |  [optional]
**manualDestinationOrderLineIdMaxValue** | **Integer** |  |  [optional]
**manualDestinationProductionTaskIdMinValue** | **Integer** |  |  [optional]
**manualDestinationProductionTaskIdMaxValue** | **Integer** |  |  [optional]
**manualDestinationProductionTaskLineIdMinValue** | **Integer** |  |  [optional]
**manualDestinationProductionTaskLineIdMaxValue** | **Integer** |  |  [optional]
**manualDestinationTypeMinValue** | **Integer** |  |  [optional]
**manualDestinationTypeMaxValue** | **Integer** |  |  [optional]



