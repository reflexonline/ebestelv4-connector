
# LineCustomerDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **Integer** |  | 
**postalAddressName** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



