# ScanCategoryApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**scanCategoryCreateScanCategory**](ScanCategoryApi.md#scanCategoryCreateScanCategory) | **POST** /ScanCategory | 
[**scanCategoryDeleteScanCategory**](ScanCategoryApi.md#scanCategoryDeleteScanCategory) | **DELETE** /ScanCategory/{id} | 
[**scanCategoryExistsScanCategory**](ScanCategoryApi.md#scanCategoryExistsScanCategory) | **GET** /ScanCategory/exists/{id} | 
[**scanCategoryGetScanCategories**](ScanCategoryApi.md#scanCategoryGetScanCategories) | **POST** /ScanCategory/search | 
[**scanCategoryGetScanCategory**](ScanCategoryApi.md#scanCategoryGetScanCategory) | **GET** /ScanCategory/{id} | 
[**scanCategoryUpdateScanCategory**](ScanCategoryApi.md#scanCategoryUpdateScanCategory) | **PUT** /ScanCategory/{id} | 


<a name="scanCategoryCreateScanCategory"></a>
# **scanCategoryCreateScanCategory**
> Object scanCategoryCreateScanCategory(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ScanCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ScanCategoryApi apiInstance = new ScanCategoryApi();
ScanCategoryDto dto = new ScanCategoryDto(); // ScanCategoryDto | 
try {
    Object result = apiInstance.scanCategoryCreateScanCategory(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScanCategoryApi#scanCategoryCreateScanCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ScanCategoryDto**](ScanCategoryDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="scanCategoryDeleteScanCategory"></a>
# **scanCategoryDeleteScanCategory**
> Object scanCategoryDeleteScanCategory(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ScanCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ScanCategoryApi apiInstance = new ScanCategoryApi();
Integer id = 56; // Integer | 
try {
    Object result = apiInstance.scanCategoryDeleteScanCategory(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScanCategoryApi#scanCategoryDeleteScanCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="scanCategoryExistsScanCategory"></a>
# **scanCategoryExistsScanCategory**
> Object scanCategoryExistsScanCategory(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ScanCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ScanCategoryApi apiInstance = new ScanCategoryApi();
Integer id = 56; // Integer | 
try {
    Object result = apiInstance.scanCategoryExistsScanCategory(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScanCategoryApi#scanCategoryExistsScanCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="scanCategoryGetScanCategories"></a>
# **scanCategoryGetScanCategories**
> Object scanCategoryGetScanCategories(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ScanCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ScanCategoryApi apiInstance = new ScanCategoryApi();
GetFilteredScanCategoryQuery query = new GetFilteredScanCategoryQuery(); // GetFilteredScanCategoryQuery | 
try {
    Object result = apiInstance.scanCategoryGetScanCategories(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScanCategoryApi#scanCategoryGetScanCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredScanCategoryQuery**](GetFilteredScanCategoryQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="scanCategoryGetScanCategory"></a>
# **scanCategoryGetScanCategory**
> Object scanCategoryGetScanCategory(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ScanCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ScanCategoryApi apiInstance = new ScanCategoryApi();
Integer id = 56; // Integer | 
try {
    Object result = apiInstance.scanCategoryGetScanCategory(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScanCategoryApi#scanCategoryGetScanCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="scanCategoryUpdateScanCategory"></a>
# **scanCategoryUpdateScanCategory**
> Object scanCategoryUpdateScanCategory(id, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ScanCategoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ScanCategoryApi apiInstance = new ScanCategoryApi();
Integer id = 56; // Integer | 
ScanCategoryDto dto = new ScanCategoryDto(); // ScanCategoryDto | 
try {
    Object result = apiInstance.scanCategoryUpdateScanCategory(id, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScanCategoryApi#scanCategoryUpdateScanCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **dto** | [**ScanCategoryDto**](ScanCategoryDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

