
# PurchaseSpecialOfferDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierId** | **Integer** |  | 
**articleId** | **Integer** |  | 
**dateIndex** | **Integer** |  | 
**prijsOld** | **Float** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



