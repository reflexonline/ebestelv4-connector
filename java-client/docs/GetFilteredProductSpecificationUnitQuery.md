
# GetFilteredProductSpecificationUnitQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productSpecificationUnitIdMinValue** | **Integer** |  |  [optional]
**productSpecificationUnitIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



