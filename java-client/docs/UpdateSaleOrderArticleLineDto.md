
# UpdateSaleOrderArticleLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **Integer** |  | 
**lineId** | **Integer** |  | 
**orderedWeight** | **Double** |  |  [optional]
**orderedQuantity** | **Double** |  |  [optional]
**orderedPortionWeight** | **Double** |  |  [optional]
**extraText** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



