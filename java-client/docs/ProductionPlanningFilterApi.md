# ProductionPlanningFilterApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productionPlanningFilterCreateProductionPlanningFilter**](ProductionPlanningFilterApi.md#productionPlanningFilterCreateProductionPlanningFilter) | **POST** /ProductionPlanningFilter | 
[**productionPlanningFilterDeleteProductionPlanningFilter**](ProductionPlanningFilterApi.md#productionPlanningFilterDeleteProductionPlanningFilter) | **DELETE** /ProductionPlanningFilter/{number} | 
[**productionPlanningFilterExistsProductionPlanningFilter**](ProductionPlanningFilterApi.md#productionPlanningFilterExistsProductionPlanningFilter) | **GET** /ProductionPlanningFilter/exists/{number} | 
[**productionPlanningFilterGetProductionPlanningFilter**](ProductionPlanningFilterApi.md#productionPlanningFilterGetProductionPlanningFilter) | **GET** /ProductionPlanningFilter/{number} | 
[**productionPlanningFilterGetProductionPlanningFilters**](ProductionPlanningFilterApi.md#productionPlanningFilterGetProductionPlanningFilters) | **POST** /ProductionPlanningFilter/search | 
[**productionPlanningFilterUpdateProductionPlanningFilter**](ProductionPlanningFilterApi.md#productionPlanningFilterUpdateProductionPlanningFilter) | **PUT** /ProductionPlanningFilter/{number} | 


<a name="productionPlanningFilterCreateProductionPlanningFilter"></a>
# **productionPlanningFilterCreateProductionPlanningFilter**
> Object productionPlanningFilterCreateProductionPlanningFilter(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionPlanningFilterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionPlanningFilterApi apiInstance = new ProductionPlanningFilterApi();
ProductionPlanningFilterDto dto = new ProductionPlanningFilterDto(); // ProductionPlanningFilterDto | 
try {
    Object result = apiInstance.productionPlanningFilterCreateProductionPlanningFilter(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionPlanningFilterApi#productionPlanningFilterCreateProductionPlanningFilter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ProductionPlanningFilterDto**](ProductionPlanningFilterDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productionPlanningFilterDeleteProductionPlanningFilter"></a>
# **productionPlanningFilterDeleteProductionPlanningFilter**
> Object productionPlanningFilterDeleteProductionPlanningFilter(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionPlanningFilterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionPlanningFilterApi apiInstance = new ProductionPlanningFilterApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.productionPlanningFilterDeleteProductionPlanningFilter(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionPlanningFilterApi#productionPlanningFilterDeleteProductionPlanningFilter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionPlanningFilterExistsProductionPlanningFilter"></a>
# **productionPlanningFilterExistsProductionPlanningFilter**
> Object productionPlanningFilterExistsProductionPlanningFilter(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionPlanningFilterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionPlanningFilterApi apiInstance = new ProductionPlanningFilterApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.productionPlanningFilterExistsProductionPlanningFilter(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionPlanningFilterApi#productionPlanningFilterExistsProductionPlanningFilter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionPlanningFilterGetProductionPlanningFilter"></a>
# **productionPlanningFilterGetProductionPlanningFilter**
> Object productionPlanningFilterGetProductionPlanningFilter(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionPlanningFilterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionPlanningFilterApi apiInstance = new ProductionPlanningFilterApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.productionPlanningFilterGetProductionPlanningFilter(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionPlanningFilterApi#productionPlanningFilterGetProductionPlanningFilter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionPlanningFilterGetProductionPlanningFilters"></a>
# **productionPlanningFilterGetProductionPlanningFilters**
> Object productionPlanningFilterGetProductionPlanningFilters(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionPlanningFilterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionPlanningFilterApi apiInstance = new ProductionPlanningFilterApi();
GetFilteredProductionPlanningFilterQuery query = new GetFilteredProductionPlanningFilterQuery(); // GetFilteredProductionPlanningFilterQuery | 
try {
    Object result = apiInstance.productionPlanningFilterGetProductionPlanningFilters(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionPlanningFilterApi#productionPlanningFilterGetProductionPlanningFilters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductionPlanningFilterQuery**](GetFilteredProductionPlanningFilterQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productionPlanningFilterUpdateProductionPlanningFilter"></a>
# **productionPlanningFilterUpdateProductionPlanningFilter**
> Object productionPlanningFilterUpdateProductionPlanningFilter(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionPlanningFilterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionPlanningFilterApi apiInstance = new ProductionPlanningFilterApi();
Integer number = 56; // Integer | 
ProductionPlanningFilterDto dto = new ProductionPlanningFilterDto(); // ProductionPlanningFilterDto | 
try {
    Object result = apiInstance.productionPlanningFilterUpdateProductionPlanningFilter(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionPlanningFilterApi#productionPlanningFilterUpdateProductionPlanningFilter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**ProductionPlanningFilterDto**](ProductionPlanningFilterDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

