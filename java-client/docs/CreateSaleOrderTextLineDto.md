
# CreateSaleOrderTextLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **String** |  | 
**lineNumber** | **Integer** |  |  [optional]
**printOnPackingSlib** | **Boolean** |  |  [optional]
**printOnInvoice** | **Boolean** |  |  [optional]



