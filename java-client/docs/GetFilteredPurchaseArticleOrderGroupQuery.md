
# GetFilteredPurchaseArticleOrderGroupQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purchaseArticleOrderGroupIdMinValue** | **Integer** |  |  [optional]
**purchaseArticleOrderGroupIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



