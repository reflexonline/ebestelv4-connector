
# StockLocationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stockLocationId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**allowBatchRegistration** | **Boolean** |  |  [optional]
**allowPalletLocations** | **Boolean** |  |  [optional]
**isBlocked** | **Boolean** |  |  [optional]
**multiBranch** | **Integer** |  |  [optional]
**includeInSellableStock** | **Boolean** |  |  [optional]
**isMLSLocation** | **Boolean** |  |  [optional]
**blockGFLMutation** | **Boolean** |  |  [optional]
**blockPalletByDefault** | **Boolean** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



