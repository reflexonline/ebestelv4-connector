# ProductionMachineApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productionMachineCreateProductionMachine**](ProductionMachineApi.md#productionMachineCreateProductionMachine) | **POST** /ProductionMachine | 
[**productionMachineDeleteProductionMachine**](ProductionMachineApi.md#productionMachineDeleteProductionMachine) | **DELETE** /ProductionMachine/{id} | 
[**productionMachineExistsProductionMachine**](ProductionMachineApi.md#productionMachineExistsProductionMachine) | **GET** /ProductionMachine/exists/{id} | 
[**productionMachineGetProductionMachine**](ProductionMachineApi.md#productionMachineGetProductionMachine) | **GET** /ProductionMachine/{id} | 
[**productionMachineGetProductionMachines**](ProductionMachineApi.md#productionMachineGetProductionMachines) | **POST** /ProductionMachine/search | 
[**productionMachineUpdateProductionMachine**](ProductionMachineApi.md#productionMachineUpdateProductionMachine) | **PUT** /ProductionMachine/{id} | 


<a name="productionMachineCreateProductionMachine"></a>
# **productionMachineCreateProductionMachine**
> Object productionMachineCreateProductionMachine(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionMachineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionMachineApi apiInstance = new ProductionMachineApi();
ProductionMachineDto dto = new ProductionMachineDto(); // ProductionMachineDto | 
try {
    Object result = apiInstance.productionMachineCreateProductionMachine(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionMachineApi#productionMachineCreateProductionMachine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ProductionMachineDto**](ProductionMachineDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productionMachineDeleteProductionMachine"></a>
# **productionMachineDeleteProductionMachine**
> Object productionMachineDeleteProductionMachine(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionMachineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionMachineApi apiInstance = new ProductionMachineApi();
Integer id = 56; // Integer | 
try {
    Object result = apiInstance.productionMachineDeleteProductionMachine(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionMachineApi#productionMachineDeleteProductionMachine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionMachineExistsProductionMachine"></a>
# **productionMachineExistsProductionMachine**
> Object productionMachineExistsProductionMachine(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionMachineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionMachineApi apiInstance = new ProductionMachineApi();
Integer id = 56; // Integer | 
try {
    Object result = apiInstance.productionMachineExistsProductionMachine(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionMachineApi#productionMachineExistsProductionMachine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionMachineGetProductionMachine"></a>
# **productionMachineGetProductionMachine**
> Object productionMachineGetProductionMachine(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionMachineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionMachineApi apiInstance = new ProductionMachineApi();
Integer id = 56; // Integer | 
try {
    Object result = apiInstance.productionMachineGetProductionMachine(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionMachineApi#productionMachineGetProductionMachine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionMachineGetProductionMachines"></a>
# **productionMachineGetProductionMachines**
> Object productionMachineGetProductionMachines(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionMachineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionMachineApi apiInstance = new ProductionMachineApi();
GetFilteredProductionMachineQuery query = new GetFilteredProductionMachineQuery(); // GetFilteredProductionMachineQuery | 
try {
    Object result = apiInstance.productionMachineGetProductionMachines(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionMachineApi#productionMachineGetProductionMachines");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductionMachineQuery**](GetFilteredProductionMachineQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productionMachineUpdateProductionMachine"></a>
# **productionMachineUpdateProductionMachine**
> Object productionMachineUpdateProductionMachine(id, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionMachineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionMachineApi apiInstance = new ProductionMachineApi();
Integer id = 56; // Integer | 
ProductionMachineDto dto = new ProductionMachineDto(); // ProductionMachineDto | 
try {
    Object result = apiInstance.productionMachineUpdateProductionMachine(id, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionMachineApi#productionMachineUpdateProductionMachine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **dto** | [**ProductionMachineDto**](ProductionMachineDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

