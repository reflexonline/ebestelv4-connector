
# PurchaseArticleGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**name** | **String** |  |  [optional]
**startArt** | **Integer** |  |  [optional]
**rvvNumberErkenning** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



