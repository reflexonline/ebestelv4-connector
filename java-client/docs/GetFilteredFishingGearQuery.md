
# GetFilteredFishingGearQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fishingGearIdMinValue** | **Integer** |  |  [optional]
**fishingGearIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



