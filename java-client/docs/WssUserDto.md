
# WssUserDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**description** | **String** |  |  [optional]
**autoAdvanceToNextOrderLinePremise** | **Integer** |  |  [optional]
**orderAccessPremise** | **Integer** |  |  [optional]
**mayPostponeOrder** | **String** |  |  [optional]
**unusedField** | **String** |  |  [optional]
**wssDepartment1** | **Integer** |  |  [optional]
**wssDepartment2** | **Integer** |  |  [optional]
**wssDepartment3** | **Integer** |  |  [optional]
**wssDepartment4** | **Integer** |  |  [optional]
**wssDepartment5** | **Integer** |  |  [optional]
**wssDepartment6** | **Integer** |  |  [optional]
**orderHandlingMode** | **Integer** |  |  [optional]
**previousWSSDepartment** | **Integer** |  |  [optional]
**orderLineRegistrationAndNotificationMode** | **Integer** |  |  [optional]
**alternativePrinter** | **String** |  |  [optional]
**showOrderedQuantity** | **Integer** |  |  [optional]
**processDeliXLOrders** | **Integer** |  |  [optional]
**numberOfExtraPickDays** | **Integer** |  |  [optional]
**editRegAmount** | **Integer** |  |  [optional]
**multiBranch** | **Integer** |  |  [optional]
**weighingListPrinter1** | **Integer** |  |  [optional]
**weighingListPrinter2** | **Integer** |  |  [optional]
**countStockAtReplenishment** | **Integer** |  |  [optional]
**printLabelAtOrderStart** | **Integer** |  |  [optional]
**canMentionShortage** | **Integer** |  |  [optional]
**canSkipLines** | **Integer** |  |  [optional]
**canSkipSsccPallet** | **Boolean** |  |  [optional]
**verifyWeightPurchase** | **Boolean** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



