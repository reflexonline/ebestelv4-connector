
# CallCauseDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**zoekArg** | **String** |  |  [optional]
**omschrijving** | **String** |  |  [optional]
**r1** | **String** |  |  [optional]
**r2** | **String** |  |  [optional]
**r3** | **String** |  |  [optional]
**r4** | **String** |  |  [optional]
**r5** | **String** |  |  [optional]
**r6** | **String** |  |  [optional]
**r7** | **String** |  |  [optional]
**r8** | **String** |  |  [optional]
**r9** | **String** |  |  [optional]
**r10** | **String** |  |  [optional]
**r11** | **String** |  |  [optional]
**r12** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



