
# GetFilteredOrderCharacteristicQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderCharacteristicIdMinValue** | **Integer** |  |  [optional]
**orderCharacteristicIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



