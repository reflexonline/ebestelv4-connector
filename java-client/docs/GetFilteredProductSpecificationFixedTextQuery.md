
# GetFilteredProductSpecificationFixedTextQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productSpecificationFixedTextIdMinValue** | **Integer** |  |  [optional]
**productSpecificationFixedTextIdMaxValue** | **Integer** |  |  [optional]



