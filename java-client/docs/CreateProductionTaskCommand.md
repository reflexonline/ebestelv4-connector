
# CreateProductionTaskCommand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipeId** | **Integer** |  | 
**recipeDerivativeId** | **Integer** |  |  [optional]
**recipePacingId** | **Integer** |  |  [optional]
**amount** | **Double** |  | 
**endDate** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**bestbeforeDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]



