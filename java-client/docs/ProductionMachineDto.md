
# ProductionMachineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**number** | **Integer** |  |  [optional]
**description** | **String** |  |  [optional]
**minimumChargeSize** | **Double** |  |  [optional]
**maximumChargeSize** | **Double** |  |  [optional]
**processingUnit** | **Double** |  |  [optional]
**mondayAvailableFrom** | **String** |  |  [optional]
**mondayAvailableUntil** | **String** |  |  [optional]
**stepEndInPause** | **Boolean** |  |  [optional]
**processingUnitType** | [**ProcessingUnitTypeEnum**](#ProcessingUnitTypeEnum) |  |  [optional]
**stepPlanningMethod** | [**StepPlanningMethodEnum**](#StepPlanningMethodEnum) |  |  [optional]
**processesUnmanned** | **Boolean** |  |  [optional]
**photoPath** | **String** |  |  [optional]
**crew** | **Integer** |  |  [optional]
**tuesdayAvailableFrom** | **String** |  |  [optional]
**tuesdayAvailableUntil** | **String** |  |  [optional]
**wednesdayAvailableFrom** | **String** |  |  [optional]
**wednesdayAvailableUntil** | **String** |  |  [optional]
**thursdayAvailableFrom** | **String** |  |  [optional]
**thursdayAvailableUntil** | **String** |  |  [optional]
**fridayAvailableFrom** | **String** |  |  [optional]
**fridayAvailableUntil** | **String** |  |  [optional]
**saturdayAvailableFrom** | **String** |  |  [optional]
**saturdayAvailableUntil** | **String** |  |  [optional]
**sundayAvailableFrom** | **String** |  |  [optional]
**sundayAvailableUntil** | **String** |  |  [optional]
**mondayAvailable** | **Boolean** |  |  [optional]
**tuesdayAvailable** | **Boolean** |  |  [optional]
**wednesdayAvailable** | **Boolean** |  |  [optional]
**thursdayAvailable** | **Boolean** |  |  [optional]
**fridayAvailable** | **Boolean** |  |  [optional]
**saturdayAvailable** | **Boolean** |  |  [optional]
**sundayAvailable** | **Boolean** |  |  [optional]
**processesMultipleSteps** | **Boolean** |  |  [optional]
**productionMachineType** | [**ProductionMachineTypeEnum**](#ProductionMachineTypeEnum) |  |  [optional]
**mondayExtraTime** | **String** |  |  [optional]
**tuesdayExtraTime** | **String** |  |  [optional]
**wednesdayExtraTime** | **String** |  |  [optional]
**thursdayExtraTime** | **String** |  |  [optional]
**fridayExtraTime** | **String** |  |  [optional]
**saturdayExtraTime** | **String** |  |  [optional]
**sundayExtraTime** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="ProcessingUnitTypeEnum"></a>
## Enum: ProcessingUnitTypeEnum
Name | Value
---- | -----
MINUTES | &quot;Minutes&quot;
KILOGRAMSPERMINUTE | &quot;KilogramsPerMinute&quot;
PIECESPERMINUTE | &quot;PiecesPerMinute&quot;


<a name="StepPlanningMethodEnum"></a>
## Enum: StepPlanningMethodEnum
Name | Value
---- | -----
STEPASCONTIGUOUSBLOCK | &quot;StepAsContiguousBlock&quot;
STEPCANBEINTERRUPTED | &quot;StepCanBeInterrupted&quot;


<a name="ProductionMachineTypeEnum"></a>
## Enum: ProductionMachineTypeEnum
Name | Value
---- | -----
PRODUCTIONMACHINE | &quot;ProductionMachine&quot;
WORKINGLOCATION | &quot;WorkingLocation&quot;
ALL | &quot;All&quot;



