
# CreateSaleOrderArticleLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**saleArticle** | **Integer** |  | 
**lineNumber** | **Integer** |  |  [optional]
**quantity** | **Double** |  |  [optional]
**portionWeight** | **Double** |  |  [optional]
**weight** | **Double** |  |  [optional]
**extraText** | **String** |  |  [optional]



