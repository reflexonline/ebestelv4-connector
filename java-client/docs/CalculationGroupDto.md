
# CalculationGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**calculationGroupId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



