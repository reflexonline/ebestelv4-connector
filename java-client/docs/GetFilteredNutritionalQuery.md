
# GetFilteredNutritionalQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nutritionalIdMinValue** | **Integer** |  |  [optional]
**nutritionalIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



