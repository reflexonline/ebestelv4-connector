
# SaleSpecialOfferDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **Integer** |  | 
**articleId** | **Integer** |  | 
**dateIndex** | **Integer** |  | 
**untilDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**prijsOld** | **Float** |  |  [optional]
**vlag** | [**VlagEnum**](#VlagEnum) |  |  [optional]
**text** | **String** |  |  [optional]
**isAction** | **Boolean** |  |  [optional]
**saleSpecialOfferType** | [**SaleSpecialOfferTypeEnum**](#SaleSpecialOfferTypeEnum) |  |  [optional]
**groupTextId** | **Integer** |  |  [optional]
**specialOfferId** | **Integer** |  |  [optional]
**tagName** | **Integer** |  |  [optional]
**price** | **Double** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="VlagEnum"></a>
## Enum: VlagEnum
Name | Value
---- | -----
FIXED | &quot;Fixed&quot;
AMOUNT | &quot;Amount&quot;
PERCENTAGE | &quot;Percentage&quot;


<a name="SaleSpecialOfferTypeEnum"></a>
## Enum: SaleSpecialOfferTypeEnum
Name | Value
---- | -----
CUSTOMER | &quot;Customer&quot;
SUBORGANIZATION | &quot;Suborganization&quot;
ORGANIZATION | &quot;Organization&quot;
ALLCUSTOMERS | &quot;AllCustomers&quot;



