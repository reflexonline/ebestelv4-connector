
# GetFilteredPurchaseOrderPatternHeaderQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierIdMinValue** | **Integer** |  |  [optional]
**supplierIdMaxValue** | **Integer** |  |  [optional]
**lineMinValue** | **Integer** |  |  [optional]
**lineMaxValue** | **Integer** |  |  [optional]
**listMinValue** | **Integer** |  |  [optional]
**listMaxValue** | **Integer** |  |  [optional]



