# OrganizationApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**organizationCreateOrganization**](OrganizationApi.md#organizationCreateOrganization) | **POST** /Organization | 
[**organizationDeleteOrganization**](OrganizationApi.md#organizationDeleteOrganization) | **DELETE** /Organization/{organizationId} | 
[**organizationExistsOrganization**](OrganizationApi.md#organizationExistsOrganization) | **GET** /Organization/exists/{organizationId} | 
[**organizationGetOrganization**](OrganizationApi.md#organizationGetOrganization) | **GET** /Organization/{organizationId} | 
[**organizationGetOrganizations**](OrganizationApi.md#organizationGetOrganizations) | **POST** /Organization/search | 
[**organizationUpdateOrganization**](OrganizationApi.md#organizationUpdateOrganization) | **PUT** /Organization/{organizationId} | 


<a name="organizationCreateOrganization"></a>
# **organizationCreateOrganization**
> Object organizationCreateOrganization(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.OrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationApi apiInstance = new OrganizationApi();
OrganizationDto dto = new OrganizationDto(); // OrganizationDto | 
try {
    Object result = apiInstance.organizationCreateOrganization(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationApi#organizationCreateOrganization");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**OrganizationDto**](OrganizationDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="organizationDeleteOrganization"></a>
# **organizationDeleteOrganization**
> Object organizationDeleteOrganization(organizationId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.OrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationApi apiInstance = new OrganizationApi();
Integer organizationId = 56; // Integer | 
try {
    Object result = apiInstance.organizationDeleteOrganization(organizationId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationApi#organizationDeleteOrganization");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="organizationExistsOrganization"></a>
# **organizationExistsOrganization**
> Object organizationExistsOrganization(organizationId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.OrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationApi apiInstance = new OrganizationApi();
Integer organizationId = 56; // Integer | 
try {
    Object result = apiInstance.organizationExistsOrganization(organizationId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationApi#organizationExistsOrganization");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="organizationGetOrganization"></a>
# **organizationGetOrganization**
> Object organizationGetOrganization(organizationId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.OrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationApi apiInstance = new OrganizationApi();
Integer organizationId = 56; // Integer | 
try {
    Object result = apiInstance.organizationGetOrganization(organizationId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationApi#organizationGetOrganization");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="organizationGetOrganizations"></a>
# **organizationGetOrganizations**
> Object organizationGetOrganizations(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.OrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationApi apiInstance = new OrganizationApi();
GetFilteredOrganizationQuery query = new GetFilteredOrganizationQuery(); // GetFilteredOrganizationQuery | 
try {
    Object result = apiInstance.organizationGetOrganizations(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationApi#organizationGetOrganizations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredOrganizationQuery**](GetFilteredOrganizationQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="organizationUpdateOrganization"></a>
# **organizationUpdateOrganization**
> Object organizationUpdateOrganization(organizationId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.OrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

OrganizationApi apiInstance = new OrganizationApi();
Integer organizationId = 56; // Integer | 
OrganizationDto dto = new OrganizationDto(); // OrganizationDto | 
try {
    Object result = apiInstance.organizationUpdateOrganization(organizationId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrganizationApi#organizationUpdateOrganization");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | **Integer**|  |
 **dto** | [**OrganizationDto**](OrganizationDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

