# AccountingPeriodApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountingPeriodCreateAccountingPeriod**](AccountingPeriodApi.md#accountingPeriodCreateAccountingPeriod) | **POST** /AccountingPeriod | 
[**accountingPeriodDeleteAccountingPeriod**](AccountingPeriodApi.md#accountingPeriodDeleteAccountingPeriod) | **DELETE** /AccountingPeriod/{year}/{period} | 
[**accountingPeriodExistsAccountingPeriod**](AccountingPeriodApi.md#accountingPeriodExistsAccountingPeriod) | **GET** /AccountingPeriod/exists/{year}/{period} | 
[**accountingPeriodGetAccountingPeriod**](AccountingPeriodApi.md#accountingPeriodGetAccountingPeriod) | **GET** /AccountingPeriod/{year}/{period} | 
[**accountingPeriodGetAccountingPeriods**](AccountingPeriodApi.md#accountingPeriodGetAccountingPeriods) | **POST** /AccountingPeriod/search | 
[**accountingPeriodUpdateAccountingPeriod**](AccountingPeriodApi.md#accountingPeriodUpdateAccountingPeriod) | **PUT** /AccountingPeriod/{year}/{period} | 


<a name="accountingPeriodCreateAccountingPeriod"></a>
# **accountingPeriodCreateAccountingPeriod**
> Object accountingPeriodCreateAccountingPeriod(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AccountingPeriodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AccountingPeriodApi apiInstance = new AccountingPeriodApi();
AccountingPeriodDto dto = new AccountingPeriodDto(); // AccountingPeriodDto | 
try {
    Object result = apiInstance.accountingPeriodCreateAccountingPeriod(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountingPeriodApi#accountingPeriodCreateAccountingPeriod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**AccountingPeriodDto**](AccountingPeriodDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="accountingPeriodDeleteAccountingPeriod"></a>
# **accountingPeriodDeleteAccountingPeriod**
> Object accountingPeriodDeleteAccountingPeriod(year, period)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AccountingPeriodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AccountingPeriodApi apiInstance = new AccountingPeriodApi();
Integer year = 56; // Integer | 
Integer period = 56; // Integer | 
try {
    Object result = apiInstance.accountingPeriodDeleteAccountingPeriod(year, period);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountingPeriodApi#accountingPeriodDeleteAccountingPeriod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **year** | **Integer**|  |
 **period** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="accountingPeriodExistsAccountingPeriod"></a>
# **accountingPeriodExistsAccountingPeriod**
> Object accountingPeriodExistsAccountingPeriod(year, period)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AccountingPeriodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AccountingPeriodApi apiInstance = new AccountingPeriodApi();
Integer year = 56; // Integer | 
Integer period = 56; // Integer | 
try {
    Object result = apiInstance.accountingPeriodExistsAccountingPeriod(year, period);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountingPeriodApi#accountingPeriodExistsAccountingPeriod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **year** | **Integer**|  |
 **period** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="accountingPeriodGetAccountingPeriod"></a>
# **accountingPeriodGetAccountingPeriod**
> Object accountingPeriodGetAccountingPeriod(year, period)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AccountingPeriodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AccountingPeriodApi apiInstance = new AccountingPeriodApi();
Integer year = 56; // Integer | 
Integer period = 56; // Integer | 
try {
    Object result = apiInstance.accountingPeriodGetAccountingPeriod(year, period);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountingPeriodApi#accountingPeriodGetAccountingPeriod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **year** | **Integer**|  |
 **period** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="accountingPeriodGetAccountingPeriods"></a>
# **accountingPeriodGetAccountingPeriods**
> Object accountingPeriodGetAccountingPeriods(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AccountingPeriodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AccountingPeriodApi apiInstance = new AccountingPeriodApi();
GetFilteredAccountingPeriodQuery query = new GetFilteredAccountingPeriodQuery(); // GetFilteredAccountingPeriodQuery | 
try {
    Object result = apiInstance.accountingPeriodGetAccountingPeriods(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountingPeriodApi#accountingPeriodGetAccountingPeriods");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredAccountingPeriodQuery**](GetFilteredAccountingPeriodQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="accountingPeriodUpdateAccountingPeriod"></a>
# **accountingPeriodUpdateAccountingPeriod**
> Object accountingPeriodUpdateAccountingPeriod(year, period, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AccountingPeriodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AccountingPeriodApi apiInstance = new AccountingPeriodApi();
Integer year = 56; // Integer | 
Integer period = 56; // Integer | 
AccountingPeriodDto dto = new AccountingPeriodDto(); // AccountingPeriodDto | 
try {
    Object result = apiInstance.accountingPeriodUpdateAccountingPeriod(year, period, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountingPeriodApi#accountingPeriodUpdateAccountingPeriod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **year** | **Integer**|  |
 **period** | **Integer**|  |
 **dto** | [**AccountingPeriodDto**](AccountingPeriodDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

