
# AllergenDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allergenId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**isRequired** | **Boolean** |  |  [optional]
**threshold** | **Double** |  |  [optional]
**pdmCode** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



