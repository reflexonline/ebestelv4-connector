# ProductionPlanningGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productionPlanningGroupCreateProductionPlanningGroup**](ProductionPlanningGroupApi.md#productionPlanningGroupCreateProductionPlanningGroup) | **POST** /ProductionPlanningGroup | 
[**productionPlanningGroupDeleteProductionPlanningGroup**](ProductionPlanningGroupApi.md#productionPlanningGroupDeleteProductionPlanningGroup) | **DELETE** /ProductionPlanningGroup/{planningGroupId} | 
[**productionPlanningGroupExistsProductionPlanningGroup**](ProductionPlanningGroupApi.md#productionPlanningGroupExistsProductionPlanningGroup) | **GET** /ProductionPlanningGroup/exists/{planningGroupId} | 
[**productionPlanningGroupGetProductionPlanningGroup**](ProductionPlanningGroupApi.md#productionPlanningGroupGetProductionPlanningGroup) | **GET** /ProductionPlanningGroup/{planningGroupId} | 
[**productionPlanningGroupGetProductionPlanningGroups**](ProductionPlanningGroupApi.md#productionPlanningGroupGetProductionPlanningGroups) | **POST** /ProductionPlanningGroup/search | 
[**productionPlanningGroupUpdateProductionPlanningGroup**](ProductionPlanningGroupApi.md#productionPlanningGroupUpdateProductionPlanningGroup) | **PUT** /ProductionPlanningGroup/{planningGroupId} | 


<a name="productionPlanningGroupCreateProductionPlanningGroup"></a>
# **productionPlanningGroupCreateProductionPlanningGroup**
> Object productionPlanningGroupCreateProductionPlanningGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionPlanningGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionPlanningGroupApi apiInstance = new ProductionPlanningGroupApi();
ProductionPlanningGroupDto dto = new ProductionPlanningGroupDto(); // ProductionPlanningGroupDto | 
try {
    Object result = apiInstance.productionPlanningGroupCreateProductionPlanningGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionPlanningGroupApi#productionPlanningGroupCreateProductionPlanningGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ProductionPlanningGroupDto**](ProductionPlanningGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productionPlanningGroupDeleteProductionPlanningGroup"></a>
# **productionPlanningGroupDeleteProductionPlanningGroup**
> Object productionPlanningGroupDeleteProductionPlanningGroup(planningGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionPlanningGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionPlanningGroupApi apiInstance = new ProductionPlanningGroupApi();
Integer planningGroupId = 56; // Integer | 
try {
    Object result = apiInstance.productionPlanningGroupDeleteProductionPlanningGroup(planningGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionPlanningGroupApi#productionPlanningGroupDeleteProductionPlanningGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planningGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionPlanningGroupExistsProductionPlanningGroup"></a>
# **productionPlanningGroupExistsProductionPlanningGroup**
> Object productionPlanningGroupExistsProductionPlanningGroup(planningGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionPlanningGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionPlanningGroupApi apiInstance = new ProductionPlanningGroupApi();
Integer planningGroupId = 56; // Integer | 
try {
    Object result = apiInstance.productionPlanningGroupExistsProductionPlanningGroup(planningGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionPlanningGroupApi#productionPlanningGroupExistsProductionPlanningGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planningGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionPlanningGroupGetProductionPlanningGroup"></a>
# **productionPlanningGroupGetProductionPlanningGroup**
> Object productionPlanningGroupGetProductionPlanningGroup(planningGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionPlanningGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionPlanningGroupApi apiInstance = new ProductionPlanningGroupApi();
Integer planningGroupId = 56; // Integer | 
try {
    Object result = apiInstance.productionPlanningGroupGetProductionPlanningGroup(planningGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionPlanningGroupApi#productionPlanningGroupGetProductionPlanningGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planningGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionPlanningGroupGetProductionPlanningGroups"></a>
# **productionPlanningGroupGetProductionPlanningGroups**
> Object productionPlanningGroupGetProductionPlanningGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionPlanningGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionPlanningGroupApi apiInstance = new ProductionPlanningGroupApi();
GetFilteredProductionPlanningGroupQuery query = new GetFilteredProductionPlanningGroupQuery(); // GetFilteredProductionPlanningGroupQuery | 
try {
    Object result = apiInstance.productionPlanningGroupGetProductionPlanningGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionPlanningGroupApi#productionPlanningGroupGetProductionPlanningGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductionPlanningGroupQuery**](GetFilteredProductionPlanningGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productionPlanningGroupUpdateProductionPlanningGroup"></a>
# **productionPlanningGroupUpdateProductionPlanningGroup**
> Object productionPlanningGroupUpdateProductionPlanningGroup(planningGroupId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionPlanningGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionPlanningGroupApi apiInstance = new ProductionPlanningGroupApi();
Integer planningGroupId = 56; // Integer | 
ProductionPlanningGroupDto dto = new ProductionPlanningGroupDto(); // ProductionPlanningGroupDto | 
try {
    Object result = apiInstance.productionPlanningGroupUpdateProductionPlanningGroup(planningGroupId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionPlanningGroupApi#productionPlanningGroupUpdateProductionPlanningGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planningGroupId** | **Integer**|  |
 **dto** | [**ProductionPlanningGroupDto**](ProductionPlanningGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

