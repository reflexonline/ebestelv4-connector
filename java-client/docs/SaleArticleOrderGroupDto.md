
# SaleArticleOrderGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**saleArticleOrderGroupId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**labelNumber** | **Integer** |  |  [optional]
**portName** | **String** |  |  [optional]
**orderEntryLabelNumber** | **Integer** |  |  [optional]
**orderEntryLabelPortName** | **String** |  |  [optional]
**packingListPrinterNumber** | **Integer** |  |  [optional]
**extraQuantityBasedLabelPrinterConnectionInfo** | **String** |  |  [optional]
**extraQuantityBasedLabel** | **Integer** |  |  [optional]
**labelingMachinePrinterConnectionInfo** | **String** |  |  [optional]
**labelingMachineProductionLabel** | **Integer** |  |  [optional]
**labelingMachineProductionAttentionLabel** | **Integer** |  |  [optional]
**automaticRegistrationMethod** | [**AutomaticRegistrationMethodEnum**](#AutomaticRegistrationMethodEnum) |  |  [optional]
**packingListNumber** | **Integer** |  |  [optional]
**attentionLabelNumber** | **Integer** |  |  [optional]
**attentionLabelPort** | **String** |  |  [optional]
**reapplyAutomaticRegistrationMethodAtPrintingPackingSlip** | **Boolean** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="AutomaticRegistrationMethodEnum"></a>
## Enum: AutomaticRegistrationMethodEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
PIECEARTICLESONLY | &quot;PieceArticlesOnly&quot;
WEIGHTARTICLESONLY | &quot;WeightArticlesOnly&quot;
PIECEANDWEIGHTARTICLES | &quot;PieceAndWeightArticles&quot;



