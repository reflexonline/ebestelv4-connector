
# GetFilteredFlexCountryQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**countryIdMinValue** | **Integer** |  |  [optional]
**countryIdMaxValue** | **Integer** |  |  [optional]
**isoCodeMinValue** | **String** |  |  [optional]
**isoCodeMaxValue** | **String** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]
**countryCustomsMinValue** | **Integer** |  |  [optional]
**countryCustomsMaxValue** | **Integer** |  |  [optional]



