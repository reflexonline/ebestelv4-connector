
# GetFilteredLotQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lotIdMinValue** | **Integer** |  |  [optional]
**lotIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]
**countryCodeOfButcheringMinValue** | **String** |  |  [optional]
**countryCodeOfButcheringMaxValue** | **String** |  |  [optional]
**countryCodeOfCuttingMinValue** | **String** |  |  [optional]
**countryCodeOfCuttingMaxValue** | **String** |  |  [optional]
**countryCodeOfBirthMinValue** | **String** |  |  [optional]
**countryCodeOfBirthMaxValue** | **String** |  |  [optional]
**countryCodeOfFatteningMinValue** | **String** |  |  [optional]
**countryCodeOfFatteningMaxValue** | **String** |  |  [optional]
**bestBeforeDateMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**bestBeforeDateMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**dateFirstStorageMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**dateFirstStorageMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**captureRegionIdMinValue** | **Integer** |  |  [optional]
**captureRegionIdMaxValue** | **Integer** |  |  [optional]
**captureRegionIsoMinValue** | **String** |  |  [optional]
**captureRegionIsoMaxValue** | **String** |  |  [optional]
**lotDefinitionIdMinValue** | **Integer** |  |  [optional]
**lotDefinitionIdMaxValue** | **Integer** |  |  [optional]
**supplierIdMinValue** | **Integer** |  |  [optional]
**supplierIdMaxValue** | **Integer** |  |  [optional]
**externalLotNumberMinValue** | **String** |  |  [optional]
**externalLotNumberMaxValue** | **String** |  |  [optional]
**fishingGearIdMinValue** | **Integer** |  |  [optional]
**fishingGearIdMaxValue** | **Integer** |  |  [optional]



