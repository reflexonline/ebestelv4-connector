
# GetFilteredReflexUserQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**logOnNameMinValue** | **String** |  |  [optional]
**logOnNameMaxValue** | **String** |  |  [optional]
**pointOfSaleIdMinValue** | **Integer** |  |  [optional]
**pointOfSaleIdMaxValue** | **Integer** |  |  [optional]
**transporterIdMinValue** | **Integer** |  |  [optional]
**transporterIdMaxValue** | **Integer** |  |  [optional]



