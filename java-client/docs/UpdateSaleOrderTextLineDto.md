
# UpdateSaleOrderTextLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **Integer** |  | 
**lineId** | **Integer** |  | 
**text** | **String** |  |  [optional]
**printOnPackingSlib** | **Boolean** |  |  [optional]
**printOnInvoice** | **Boolean** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



