
# GetFilteredSpecialBranchMutationUserQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fileNumberMinValue** | **Integer** |  |  [optional]
**fileNumberMaxValue** | **Integer** |  |  [optional]



