# SaleArticleOrderGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleArticleOrderGroupCreateSaleArticleOrderGroup**](SaleArticleOrderGroupApi.md#saleArticleOrderGroupCreateSaleArticleOrderGroup) | **POST** /SaleArticleOrderGroup | 
[**saleArticleOrderGroupDeleteSaleArticleOrderGroup**](SaleArticleOrderGroupApi.md#saleArticleOrderGroupDeleteSaleArticleOrderGroup) | **DELETE** /SaleArticleOrderGroup/{saleArticleOrderGroupId} | 
[**saleArticleOrderGroupExistsSaleArticleOrderGroup**](SaleArticleOrderGroupApi.md#saleArticleOrderGroupExistsSaleArticleOrderGroup) | **GET** /SaleArticleOrderGroup/exists/{saleArticleOrderGroupId} | 
[**saleArticleOrderGroupGetSaleArticleOrderGroup**](SaleArticleOrderGroupApi.md#saleArticleOrderGroupGetSaleArticleOrderGroup) | **GET** /SaleArticleOrderGroup/{saleArticleOrderGroupId} | 
[**saleArticleOrderGroupGetSaleArticleOrderGroups**](SaleArticleOrderGroupApi.md#saleArticleOrderGroupGetSaleArticleOrderGroups) | **POST** /SaleArticleOrderGroup/search | 
[**saleArticleOrderGroupUpdateSaleArticleOrderGroup**](SaleArticleOrderGroupApi.md#saleArticleOrderGroupUpdateSaleArticleOrderGroup) | **PUT** /SaleArticleOrderGroup/{saleArticleOrderGroupId} | 


<a name="saleArticleOrderGroupCreateSaleArticleOrderGroup"></a>
# **saleArticleOrderGroupCreateSaleArticleOrderGroup**
> Object saleArticleOrderGroupCreateSaleArticleOrderGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleOrderGroupApi apiInstance = new SaleArticleOrderGroupApi();
SaleArticleOrderGroupDto dto = new SaleArticleOrderGroupDto(); // SaleArticleOrderGroupDto | 
try {
    Object result = apiInstance.saleArticleOrderGroupCreateSaleArticleOrderGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleOrderGroupApi#saleArticleOrderGroupCreateSaleArticleOrderGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SaleArticleOrderGroupDto**](SaleArticleOrderGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleOrderGroupDeleteSaleArticleOrderGroup"></a>
# **saleArticleOrderGroupDeleteSaleArticleOrderGroup**
> Object saleArticleOrderGroupDeleteSaleArticleOrderGroup(saleArticleOrderGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleOrderGroupApi apiInstance = new SaleArticleOrderGroupApi();
Integer saleArticleOrderGroupId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleOrderGroupDeleteSaleArticleOrderGroup(saleArticleOrderGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleOrderGroupApi#saleArticleOrderGroupDeleteSaleArticleOrderGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleOrderGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleOrderGroupExistsSaleArticleOrderGroup"></a>
# **saleArticleOrderGroupExistsSaleArticleOrderGroup**
> Object saleArticleOrderGroupExistsSaleArticleOrderGroup(saleArticleOrderGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleOrderGroupApi apiInstance = new SaleArticleOrderGroupApi();
Integer saleArticleOrderGroupId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleOrderGroupExistsSaleArticleOrderGroup(saleArticleOrderGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleOrderGroupApi#saleArticleOrderGroupExistsSaleArticleOrderGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleOrderGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleOrderGroupGetSaleArticleOrderGroup"></a>
# **saleArticleOrderGroupGetSaleArticleOrderGroup**
> Object saleArticleOrderGroupGetSaleArticleOrderGroup(saleArticleOrderGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleOrderGroupApi apiInstance = new SaleArticleOrderGroupApi();
Integer saleArticleOrderGroupId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleOrderGroupGetSaleArticleOrderGroup(saleArticleOrderGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleOrderGroupApi#saleArticleOrderGroupGetSaleArticleOrderGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleOrderGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleOrderGroupGetSaleArticleOrderGroups"></a>
# **saleArticleOrderGroupGetSaleArticleOrderGroups**
> Object saleArticleOrderGroupGetSaleArticleOrderGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleOrderGroupApi apiInstance = new SaleArticleOrderGroupApi();
GetFilteredSaleArticleOrderGroupQuery query = new GetFilteredSaleArticleOrderGroupQuery(); // GetFilteredSaleArticleOrderGroupQuery | 
try {
    Object result = apiInstance.saleArticleOrderGroupGetSaleArticleOrderGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleOrderGroupApi#saleArticleOrderGroupGetSaleArticleOrderGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSaleArticleOrderGroupQuery**](GetFilteredSaleArticleOrderGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleOrderGroupUpdateSaleArticleOrderGroup"></a>
# **saleArticleOrderGroupUpdateSaleArticleOrderGroup**
> Object saleArticleOrderGroupUpdateSaleArticleOrderGroup(saleArticleOrderGroupId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleOrderGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleOrderGroupApi apiInstance = new SaleArticleOrderGroupApi();
Integer saleArticleOrderGroupId = 56; // Integer | 
SaleArticleOrderGroupDto dto = new SaleArticleOrderGroupDto(); // SaleArticleOrderGroupDto | 
try {
    Object result = apiInstance.saleArticleOrderGroupUpdateSaleArticleOrderGroup(saleArticleOrderGroupId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleOrderGroupApi#saleArticleOrderGroupUpdateSaleArticleOrderGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleOrderGroupId** | **Integer**|  |
 **dto** | [**SaleArticleOrderGroupDto**](SaleArticleOrderGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

