# ConsumerGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**consumerGroupCreateConsumerGroup**](ConsumerGroupApi.md#consumerGroupCreateConsumerGroup) | **POST** /ConsumerGroup | 
[**consumerGroupDeleteConsumerGroup**](ConsumerGroupApi.md#consumerGroupDeleteConsumerGroup) | **DELETE** /ConsumerGroup/{consumerGroupId} | 
[**consumerGroupExistsConsumerGroup**](ConsumerGroupApi.md#consumerGroupExistsConsumerGroup) | **GET** /ConsumerGroup/exists/{consumerGroupId} | 
[**consumerGroupGetConsumerGroup**](ConsumerGroupApi.md#consumerGroupGetConsumerGroup) | **GET** /ConsumerGroup/{consumerGroupId} | 
[**consumerGroupGetConsumerGroups**](ConsumerGroupApi.md#consumerGroupGetConsumerGroups) | **POST** /ConsumerGroup/search | 
[**consumerGroupUpdateConsumerGroup**](ConsumerGroupApi.md#consumerGroupUpdateConsumerGroup) | **PUT** /ConsumerGroup/{consumerGroupId} | 


<a name="consumerGroupCreateConsumerGroup"></a>
# **consumerGroupCreateConsumerGroup**
> Object consumerGroupCreateConsumerGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ConsumerGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ConsumerGroupApi apiInstance = new ConsumerGroupApi();
ConsumerGroupDto dto = new ConsumerGroupDto(); // ConsumerGroupDto | 
try {
    Object result = apiInstance.consumerGroupCreateConsumerGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConsumerGroupApi#consumerGroupCreateConsumerGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ConsumerGroupDto**](ConsumerGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="consumerGroupDeleteConsumerGroup"></a>
# **consumerGroupDeleteConsumerGroup**
> Object consumerGroupDeleteConsumerGroup(consumerGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ConsumerGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ConsumerGroupApi apiInstance = new ConsumerGroupApi();
Integer consumerGroupId = 56; // Integer | 
try {
    Object result = apiInstance.consumerGroupDeleteConsumerGroup(consumerGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConsumerGroupApi#consumerGroupDeleteConsumerGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **consumerGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="consumerGroupExistsConsumerGroup"></a>
# **consumerGroupExistsConsumerGroup**
> Object consumerGroupExistsConsumerGroup(consumerGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ConsumerGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ConsumerGroupApi apiInstance = new ConsumerGroupApi();
Integer consumerGroupId = 56; // Integer | 
try {
    Object result = apiInstance.consumerGroupExistsConsumerGroup(consumerGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConsumerGroupApi#consumerGroupExistsConsumerGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **consumerGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="consumerGroupGetConsumerGroup"></a>
# **consumerGroupGetConsumerGroup**
> Object consumerGroupGetConsumerGroup(consumerGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ConsumerGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ConsumerGroupApi apiInstance = new ConsumerGroupApi();
Integer consumerGroupId = 56; // Integer | 
try {
    Object result = apiInstance.consumerGroupGetConsumerGroup(consumerGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConsumerGroupApi#consumerGroupGetConsumerGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **consumerGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="consumerGroupGetConsumerGroups"></a>
# **consumerGroupGetConsumerGroups**
> Object consumerGroupGetConsumerGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ConsumerGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ConsumerGroupApi apiInstance = new ConsumerGroupApi();
GetFilteredConsumerGroupQuery query = new GetFilteredConsumerGroupQuery(); // GetFilteredConsumerGroupQuery | 
try {
    Object result = apiInstance.consumerGroupGetConsumerGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConsumerGroupApi#consumerGroupGetConsumerGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredConsumerGroupQuery**](GetFilteredConsumerGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="consumerGroupUpdateConsumerGroup"></a>
# **consumerGroupUpdateConsumerGroup**
> Object consumerGroupUpdateConsumerGroup(consumerGroupId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ConsumerGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ConsumerGroupApi apiInstance = new ConsumerGroupApi();
Integer consumerGroupId = 56; // Integer | 
ConsumerGroupDto dto = new ConsumerGroupDto(); // ConsumerGroupDto | 
try {
    Object result = apiInstance.consumerGroupUpdateConsumerGroup(consumerGroupId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ConsumerGroupApi#consumerGroupUpdateConsumerGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **consumerGroupId** | **Integer**|  |
 **dto** | [**ConsumerGroupDto**](ConsumerGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

