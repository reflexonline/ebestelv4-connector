# PicAPictureGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**picAPictureGroupCreatePicAPictureGroup**](PicAPictureGroupApi.md#picAPictureGroupCreatePicAPictureGroup) | **POST** /PicAPictureGroup | 
[**picAPictureGroupDeletePicAPictureGroup**](PicAPictureGroupApi.md#picAPictureGroupDeletePicAPictureGroup) | **DELETE** /PicAPictureGroup/{picAPictureGroupId} | 
[**picAPictureGroupExistsPicAPictureGroup**](PicAPictureGroupApi.md#picAPictureGroupExistsPicAPictureGroup) | **GET** /PicAPictureGroup/exists/{picAPictureGroupId} | 
[**picAPictureGroupGetPicAPictureGroup**](PicAPictureGroupApi.md#picAPictureGroupGetPicAPictureGroup) | **GET** /PicAPictureGroup/{picAPictureGroupId} | 
[**picAPictureGroupGetPicAPictureGroups**](PicAPictureGroupApi.md#picAPictureGroupGetPicAPictureGroups) | **POST** /PicAPictureGroup/search | 
[**picAPictureGroupUpdatePicAPictureGroup**](PicAPictureGroupApi.md#picAPictureGroupUpdatePicAPictureGroup) | **PUT** /PicAPictureGroup/{picAPictureGroupId} | 


<a name="picAPictureGroupCreatePicAPictureGroup"></a>
# **picAPictureGroupCreatePicAPictureGroup**
> Object picAPictureGroupCreatePicAPictureGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PicAPictureGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PicAPictureGroupApi apiInstance = new PicAPictureGroupApi();
PicAPictureGroupDto dto = new PicAPictureGroupDto(); // PicAPictureGroupDto | 
try {
    Object result = apiInstance.picAPictureGroupCreatePicAPictureGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PicAPictureGroupApi#picAPictureGroupCreatePicAPictureGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PicAPictureGroupDto**](PicAPictureGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="picAPictureGroupDeletePicAPictureGroup"></a>
# **picAPictureGroupDeletePicAPictureGroup**
> Object picAPictureGroupDeletePicAPictureGroup(picAPictureGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PicAPictureGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PicAPictureGroupApi apiInstance = new PicAPictureGroupApi();
Integer picAPictureGroupId = 56; // Integer | 
try {
    Object result = apiInstance.picAPictureGroupDeletePicAPictureGroup(picAPictureGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PicAPictureGroupApi#picAPictureGroupDeletePicAPictureGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picAPictureGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="picAPictureGroupExistsPicAPictureGroup"></a>
# **picAPictureGroupExistsPicAPictureGroup**
> Object picAPictureGroupExistsPicAPictureGroup(picAPictureGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PicAPictureGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PicAPictureGroupApi apiInstance = new PicAPictureGroupApi();
Integer picAPictureGroupId = 56; // Integer | 
try {
    Object result = apiInstance.picAPictureGroupExistsPicAPictureGroup(picAPictureGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PicAPictureGroupApi#picAPictureGroupExistsPicAPictureGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picAPictureGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="picAPictureGroupGetPicAPictureGroup"></a>
# **picAPictureGroupGetPicAPictureGroup**
> Object picAPictureGroupGetPicAPictureGroup(picAPictureGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PicAPictureGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PicAPictureGroupApi apiInstance = new PicAPictureGroupApi();
Integer picAPictureGroupId = 56; // Integer | 
try {
    Object result = apiInstance.picAPictureGroupGetPicAPictureGroup(picAPictureGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PicAPictureGroupApi#picAPictureGroupGetPicAPictureGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picAPictureGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="picAPictureGroupGetPicAPictureGroups"></a>
# **picAPictureGroupGetPicAPictureGroups**
> Object picAPictureGroupGetPicAPictureGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PicAPictureGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PicAPictureGroupApi apiInstance = new PicAPictureGroupApi();
GetFilteredPicAPictureGroupQuery query = new GetFilteredPicAPictureGroupQuery(); // GetFilteredPicAPictureGroupQuery | 
try {
    Object result = apiInstance.picAPictureGroupGetPicAPictureGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PicAPictureGroupApi#picAPictureGroupGetPicAPictureGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPicAPictureGroupQuery**](GetFilteredPicAPictureGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="picAPictureGroupUpdatePicAPictureGroup"></a>
# **picAPictureGroupUpdatePicAPictureGroup**
> Object picAPictureGroupUpdatePicAPictureGroup(picAPictureGroupId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PicAPictureGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PicAPictureGroupApi apiInstance = new PicAPictureGroupApi();
Integer picAPictureGroupId = 56; // Integer | 
PicAPictureGroupDto dto = new PicAPictureGroupDto(); // PicAPictureGroupDto | 
try {
    Object result = apiInstance.picAPictureGroupUpdatePicAPictureGroup(picAPictureGroupId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PicAPictureGroupApi#picAPictureGroupUpdatePicAPictureGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picAPictureGroupId** | **Integer**|  |
 **dto** | [**PicAPictureGroupDto**](PicAPictureGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

