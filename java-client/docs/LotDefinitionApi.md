# LotDefinitionApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**lotDefinitionCreateLotDefinition**](LotDefinitionApi.md#lotDefinitionCreateLotDefinition) | **POST** /LotDefinition | 
[**lotDefinitionDeleteLotDefinition**](LotDefinitionApi.md#lotDefinitionDeleteLotDefinition) | **DELETE** /LotDefinition/{lotDefinitionId} | 
[**lotDefinitionExistsLotDefinition**](LotDefinitionApi.md#lotDefinitionExistsLotDefinition) | **GET** /LotDefinition/exists/{lotDefinitionId} | 
[**lotDefinitionGetLotDefinition**](LotDefinitionApi.md#lotDefinitionGetLotDefinition) | **GET** /LotDefinition/{lotDefinitionId} | 
[**lotDefinitionGetLotDefinitions**](LotDefinitionApi.md#lotDefinitionGetLotDefinitions) | **POST** /LotDefinition/search | 
[**lotDefinitionUpdateLotDefinition**](LotDefinitionApi.md#lotDefinitionUpdateLotDefinition) | **PUT** /LotDefinition/{lotDefinitionId} | 


<a name="lotDefinitionCreateLotDefinition"></a>
# **lotDefinitionCreateLotDefinition**
> Object lotDefinitionCreateLotDefinition(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LotDefinitionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LotDefinitionApi apiInstance = new LotDefinitionApi();
LotDefinitionDto dto = new LotDefinitionDto(); // LotDefinitionDto | 
try {
    Object result = apiInstance.lotDefinitionCreateLotDefinition(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LotDefinitionApi#lotDefinitionCreateLotDefinition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**LotDefinitionDto**](LotDefinitionDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="lotDefinitionDeleteLotDefinition"></a>
# **lotDefinitionDeleteLotDefinition**
> Object lotDefinitionDeleteLotDefinition(lotDefinitionId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LotDefinitionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LotDefinitionApi apiInstance = new LotDefinitionApi();
Integer lotDefinitionId = 56; // Integer | 
try {
    Object result = apiInstance.lotDefinitionDeleteLotDefinition(lotDefinitionId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LotDefinitionApi#lotDefinitionDeleteLotDefinition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lotDefinitionId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="lotDefinitionExistsLotDefinition"></a>
# **lotDefinitionExistsLotDefinition**
> Object lotDefinitionExistsLotDefinition(lotDefinitionId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LotDefinitionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LotDefinitionApi apiInstance = new LotDefinitionApi();
Integer lotDefinitionId = 56; // Integer | 
try {
    Object result = apiInstance.lotDefinitionExistsLotDefinition(lotDefinitionId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LotDefinitionApi#lotDefinitionExistsLotDefinition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lotDefinitionId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="lotDefinitionGetLotDefinition"></a>
# **lotDefinitionGetLotDefinition**
> Object lotDefinitionGetLotDefinition(lotDefinitionId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LotDefinitionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LotDefinitionApi apiInstance = new LotDefinitionApi();
Integer lotDefinitionId = 56; // Integer | 
try {
    Object result = apiInstance.lotDefinitionGetLotDefinition(lotDefinitionId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LotDefinitionApi#lotDefinitionGetLotDefinition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lotDefinitionId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="lotDefinitionGetLotDefinitions"></a>
# **lotDefinitionGetLotDefinitions**
> Object lotDefinitionGetLotDefinitions(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LotDefinitionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LotDefinitionApi apiInstance = new LotDefinitionApi();
GetFilteredLotDefinitionQuery query = new GetFilteredLotDefinitionQuery(); // GetFilteredLotDefinitionQuery | 
try {
    Object result = apiInstance.lotDefinitionGetLotDefinitions(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LotDefinitionApi#lotDefinitionGetLotDefinitions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredLotDefinitionQuery**](GetFilteredLotDefinitionQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="lotDefinitionUpdateLotDefinition"></a>
# **lotDefinitionUpdateLotDefinition**
> Object lotDefinitionUpdateLotDefinition(lotDefinitionId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LotDefinitionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LotDefinitionApi apiInstance = new LotDefinitionApi();
Integer lotDefinitionId = 56; // Integer | 
LotDefinitionDto dto = new LotDefinitionDto(); // LotDefinitionDto | 
try {
    Object result = apiInstance.lotDefinitionUpdateLotDefinition(lotDefinitionId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LotDefinitionApi#lotDefinitionUpdateLotDefinition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lotDefinitionId** | **Integer**|  |
 **dto** | [**LotDefinitionDto**](LotDefinitionDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

