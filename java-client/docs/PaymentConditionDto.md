
# PaymentConditionDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paymentConditionId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**hasVatInvoice** | **Boolean** |  |  [optional]
**autoCollection** | **Boolean** |  |  [optional]
**paymentTerms** | **Integer** |  |  [optional]
**vatExportCode** | **Integer** |  |  [optional]
**paymentExportCode** | **String** |  |  [optional]
**directInvoice** | **Integer** |  |  [optional]
**invoiceLayoutNumber** | **Integer** |  |  [optional]
**printSaleInvoice** | **Boolean** |  |  [optional]
**paymentDiscountDays** | **Integer** |  |  [optional]
**paymentDiscountPercentage** | **Double** |  |  [optional]
**saleInvoiceStatus** | **Integer** |  |  [optional]
**printCMR** | **Boolean** |  |  [optional]
**intrastatReportMethod** | **Integer** |  |  [optional]
**paymentTermsCalculation** | **Integer** |  |  [optional]
**vatScenario** | **Integer** |  |  [optional]
**blockPaymentOnAccount** | **Boolean** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



