# SupplierApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**supplierCreateSupplier**](SupplierApi.md#supplierCreateSupplier) | **POST** /Supplier | 
[**supplierDeleteSupplier**](SupplierApi.md#supplierDeleteSupplier) | **DELETE** /Supplier/{supplierId} | 
[**supplierExistsSupplier**](SupplierApi.md#supplierExistsSupplier) | **GET** /Supplier/exists/{supplierId} | 
[**supplierGetSupplier**](SupplierApi.md#supplierGetSupplier) | **GET** /Supplier/{supplierId} | 
[**supplierGetSuppliers**](SupplierApi.md#supplierGetSuppliers) | **POST** /Supplier/search | 
[**supplierUpdateSupplier**](SupplierApi.md#supplierUpdateSupplier) | **PUT** /Supplier/{supplierId} | 


<a name="supplierCreateSupplier"></a>
# **supplierCreateSupplier**
> Object supplierCreateSupplier(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierApi apiInstance = new SupplierApi();
SupplierDto dto = new SupplierDto(); // SupplierDto | 
try {
    Object result = apiInstance.supplierCreateSupplier(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierApi#supplierCreateSupplier");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SupplierDto**](SupplierDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="supplierDeleteSupplier"></a>
# **supplierDeleteSupplier**
> Object supplierDeleteSupplier(supplierId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierApi apiInstance = new SupplierApi();
Integer supplierId = 56; // Integer | 
try {
    Object result = apiInstance.supplierDeleteSupplier(supplierId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierApi#supplierDeleteSupplier");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="supplierExistsSupplier"></a>
# **supplierExistsSupplier**
> Object supplierExistsSupplier(supplierId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierApi apiInstance = new SupplierApi();
Integer supplierId = 56; // Integer | 
try {
    Object result = apiInstance.supplierExistsSupplier(supplierId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierApi#supplierExistsSupplier");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="supplierGetSupplier"></a>
# **supplierGetSupplier**
> Object supplierGetSupplier(supplierId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierApi apiInstance = new SupplierApi();
Integer supplierId = 56; // Integer | 
try {
    Object result = apiInstance.supplierGetSupplier(supplierId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierApi#supplierGetSupplier");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="supplierGetSuppliers"></a>
# **supplierGetSuppliers**
> Object supplierGetSuppliers(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierApi apiInstance = new SupplierApi();
GetFilteredSupplierQuery query = new GetFilteredSupplierQuery(); // GetFilteredSupplierQuery | 
try {
    Object result = apiInstance.supplierGetSuppliers(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierApi#supplierGetSuppliers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSupplierQuery**](GetFilteredSupplierQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="supplierUpdateSupplier"></a>
# **supplierUpdateSupplier**
> Object supplierUpdateSupplier(supplierId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierApi apiInstance = new SupplierApi();
Integer supplierId = 56; // Integer | 
SupplierDto dto = new SupplierDto(); // SupplierDto | 
try {
    Object result = apiInstance.supplierUpdateSupplier(supplierId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierApi#supplierUpdateSupplier");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **dto** | [**SupplierDto**](SupplierDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

