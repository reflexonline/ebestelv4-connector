
# PointOfSaleDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pointOfSaleId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**reqOpeningBalance** | **Boolean** |  |  [optional]
**hasAllPaymentMethods** | **Boolean** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



