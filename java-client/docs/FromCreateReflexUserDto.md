
# FromCreateReflexUserDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**logonName** | **String** |  | 
**fullName** | **String** |  | 
**password** | **String** |  |  [optional]
**autoGeneratePassword** | **Boolean** |  | 



