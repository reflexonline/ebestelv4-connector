# LabelingMachineLabelApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**labelingMachineLabelCreateLabelingMachineLabel**](LabelingMachineLabelApi.md#labelingMachineLabelCreateLabelingMachineLabel) | **POST** /LabelingMachineLabel | 
[**labelingMachineLabelDeleteLabelingMachineLabel**](LabelingMachineLabelApi.md#labelingMachineLabelDeleteLabelingMachineLabel) | **DELETE** /LabelingMachineLabel/{labelingMachineType}/{etiketNumber} | 
[**labelingMachineLabelExistsLabelingMachineLabel**](LabelingMachineLabelApi.md#labelingMachineLabelExistsLabelingMachineLabel) | **GET** /LabelingMachineLabel/exists/{labelingMachineType}/{etiketNumber} | 
[**labelingMachineLabelGetLabelingMachineLabel**](LabelingMachineLabelApi.md#labelingMachineLabelGetLabelingMachineLabel) | **GET** /LabelingMachineLabel/{labelingMachineType}/{etiketNumber} | 
[**labelingMachineLabelGetLabelingMachineLabels**](LabelingMachineLabelApi.md#labelingMachineLabelGetLabelingMachineLabels) | **POST** /LabelingMachineLabel/search | 
[**labelingMachineLabelUpdateLabelingMachineLabel**](LabelingMachineLabelApi.md#labelingMachineLabelUpdateLabelingMachineLabel) | **PUT** /LabelingMachineLabel/{labelingMachineType}/{etiketNumber} | 


<a name="labelingMachineLabelCreateLabelingMachineLabel"></a>
# **labelingMachineLabelCreateLabelingMachineLabel**
> Object labelingMachineLabelCreateLabelingMachineLabel(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LabelingMachineLabelApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LabelingMachineLabelApi apiInstance = new LabelingMachineLabelApi();
LabelingMachineLabelDto dto = new LabelingMachineLabelDto(); // LabelingMachineLabelDto | 
try {
    Object result = apiInstance.labelingMachineLabelCreateLabelingMachineLabel(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelingMachineLabelApi#labelingMachineLabelCreateLabelingMachineLabel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**LabelingMachineLabelDto**](LabelingMachineLabelDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="labelingMachineLabelDeleteLabelingMachineLabel"></a>
# **labelingMachineLabelDeleteLabelingMachineLabel**
> Object labelingMachineLabelDeleteLabelingMachineLabel(labelingMachineType, etiketNumber)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LabelingMachineLabelApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LabelingMachineLabelApi apiInstance = new LabelingMachineLabelApi();
String labelingMachineType = "labelingMachineType_example"; // String | 
Integer etiketNumber = 56; // Integer | 
try {
    Object result = apiInstance.labelingMachineLabelDeleteLabelingMachineLabel(labelingMachineType, etiketNumber);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelingMachineLabelApi#labelingMachineLabelDeleteLabelingMachineLabel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **labelingMachineType** | **String**|  | [enum: None, EsperaCIP, Bizerba, Bopack, EsperaES, Digi, BizerbaBrain, Flex, BizerbaGLP, Delford, HerbertGemini, Manual, EsperaEST]
 **etiketNumber** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="labelingMachineLabelExistsLabelingMachineLabel"></a>
# **labelingMachineLabelExistsLabelingMachineLabel**
> Object labelingMachineLabelExistsLabelingMachineLabel(labelingMachineType, etiketNumber)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LabelingMachineLabelApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LabelingMachineLabelApi apiInstance = new LabelingMachineLabelApi();
String labelingMachineType = "labelingMachineType_example"; // String | 
Integer etiketNumber = 56; // Integer | 
try {
    Object result = apiInstance.labelingMachineLabelExistsLabelingMachineLabel(labelingMachineType, etiketNumber);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelingMachineLabelApi#labelingMachineLabelExistsLabelingMachineLabel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **labelingMachineType** | **String**|  | [enum: None, EsperaCIP, Bizerba, Bopack, EsperaES, Digi, BizerbaBrain, Flex, BizerbaGLP, Delford, HerbertGemini, Manual, EsperaEST]
 **etiketNumber** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="labelingMachineLabelGetLabelingMachineLabel"></a>
# **labelingMachineLabelGetLabelingMachineLabel**
> Object labelingMachineLabelGetLabelingMachineLabel(labelingMachineType, etiketNumber)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LabelingMachineLabelApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LabelingMachineLabelApi apiInstance = new LabelingMachineLabelApi();
String labelingMachineType = "labelingMachineType_example"; // String | 
Integer etiketNumber = 56; // Integer | 
try {
    Object result = apiInstance.labelingMachineLabelGetLabelingMachineLabel(labelingMachineType, etiketNumber);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelingMachineLabelApi#labelingMachineLabelGetLabelingMachineLabel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **labelingMachineType** | **String**|  | [enum: None, EsperaCIP, Bizerba, Bopack, EsperaES, Digi, BizerbaBrain, Flex, BizerbaGLP, Delford, HerbertGemini, Manual, EsperaEST]
 **etiketNumber** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="labelingMachineLabelGetLabelingMachineLabels"></a>
# **labelingMachineLabelGetLabelingMachineLabels**
> Object labelingMachineLabelGetLabelingMachineLabels(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LabelingMachineLabelApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LabelingMachineLabelApi apiInstance = new LabelingMachineLabelApi();
GetFilteredLabelingMachineLabelQuery query = new GetFilteredLabelingMachineLabelQuery(); // GetFilteredLabelingMachineLabelQuery | 
try {
    Object result = apiInstance.labelingMachineLabelGetLabelingMachineLabels(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelingMachineLabelApi#labelingMachineLabelGetLabelingMachineLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredLabelingMachineLabelQuery**](GetFilteredLabelingMachineLabelQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="labelingMachineLabelUpdateLabelingMachineLabel"></a>
# **labelingMachineLabelUpdateLabelingMachineLabel**
> Object labelingMachineLabelUpdateLabelingMachineLabel(labelingMachineType, etiketNumber, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.LabelingMachineLabelApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

LabelingMachineLabelApi apiInstance = new LabelingMachineLabelApi();
String labelingMachineType = "labelingMachineType_example"; // String | 
Integer etiketNumber = 56; // Integer | 
LabelingMachineLabelDto dto = new LabelingMachineLabelDto(); // LabelingMachineLabelDto | 
try {
    Object result = apiInstance.labelingMachineLabelUpdateLabelingMachineLabel(labelingMachineType, etiketNumber, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelingMachineLabelApi#labelingMachineLabelUpdateLabelingMachineLabel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **labelingMachineType** | **String**|  | [enum: None, EsperaCIP, Bizerba, Bopack, EsperaES, Digi, BizerbaBrain, Flex, BizerbaGLP, Delford, HerbertGemini, Manual, EsperaEST]
 **etiketNumber** | **Integer**|  |
 **dto** | [**LabelingMachineLabelDto**](LabelingMachineLabelDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

