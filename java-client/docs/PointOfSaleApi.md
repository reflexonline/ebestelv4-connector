# PointOfSaleApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**pointOfSaleCreatePointOfSale**](PointOfSaleApi.md#pointOfSaleCreatePointOfSale) | **POST** /PointOfSale | 
[**pointOfSaleDeletePointOfSale**](PointOfSaleApi.md#pointOfSaleDeletePointOfSale) | **DELETE** /PointOfSale/{pointOfSaleId} | 
[**pointOfSaleExistsPointOfSale**](PointOfSaleApi.md#pointOfSaleExistsPointOfSale) | **GET** /PointOfSale/exists/{pointOfSaleId} | 
[**pointOfSaleGetPointOfSale**](PointOfSaleApi.md#pointOfSaleGetPointOfSale) | **GET** /PointOfSale/{pointOfSaleId} | 
[**pointOfSaleGetPointOfSales**](PointOfSaleApi.md#pointOfSaleGetPointOfSales) | **POST** /PointOfSale/search | 
[**pointOfSaleUpdatePointOfSale**](PointOfSaleApi.md#pointOfSaleUpdatePointOfSale) | **PUT** /PointOfSale/{pointOfSaleId} | 


<a name="pointOfSaleCreatePointOfSale"></a>
# **pointOfSaleCreatePointOfSale**
> Object pointOfSaleCreatePointOfSale(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PointOfSaleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PointOfSaleApi apiInstance = new PointOfSaleApi();
PointOfSaleDto dto = new PointOfSaleDto(); // PointOfSaleDto | 
try {
    Object result = apiInstance.pointOfSaleCreatePointOfSale(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PointOfSaleApi#pointOfSaleCreatePointOfSale");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PointOfSaleDto**](PointOfSaleDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="pointOfSaleDeletePointOfSale"></a>
# **pointOfSaleDeletePointOfSale**
> Object pointOfSaleDeletePointOfSale(pointOfSaleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PointOfSaleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PointOfSaleApi apiInstance = new PointOfSaleApi();
Integer pointOfSaleId = 56; // Integer | 
try {
    Object result = apiInstance.pointOfSaleDeletePointOfSale(pointOfSaleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PointOfSaleApi#pointOfSaleDeletePointOfSale");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pointOfSaleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="pointOfSaleExistsPointOfSale"></a>
# **pointOfSaleExistsPointOfSale**
> Object pointOfSaleExistsPointOfSale(pointOfSaleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PointOfSaleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PointOfSaleApi apiInstance = new PointOfSaleApi();
Integer pointOfSaleId = 56; // Integer | 
try {
    Object result = apiInstance.pointOfSaleExistsPointOfSale(pointOfSaleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PointOfSaleApi#pointOfSaleExistsPointOfSale");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pointOfSaleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="pointOfSaleGetPointOfSale"></a>
# **pointOfSaleGetPointOfSale**
> Object pointOfSaleGetPointOfSale(pointOfSaleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PointOfSaleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PointOfSaleApi apiInstance = new PointOfSaleApi();
Integer pointOfSaleId = 56; // Integer | 
try {
    Object result = apiInstance.pointOfSaleGetPointOfSale(pointOfSaleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PointOfSaleApi#pointOfSaleGetPointOfSale");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pointOfSaleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="pointOfSaleGetPointOfSales"></a>
# **pointOfSaleGetPointOfSales**
> Object pointOfSaleGetPointOfSales(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PointOfSaleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PointOfSaleApi apiInstance = new PointOfSaleApi();
GetFilteredPointOfSaleQuery query = new GetFilteredPointOfSaleQuery(); // GetFilteredPointOfSaleQuery | 
try {
    Object result = apiInstance.pointOfSaleGetPointOfSales(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PointOfSaleApi#pointOfSaleGetPointOfSales");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPointOfSaleQuery**](GetFilteredPointOfSaleQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="pointOfSaleUpdatePointOfSale"></a>
# **pointOfSaleUpdatePointOfSale**
> Object pointOfSaleUpdatePointOfSale(pointOfSaleId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PointOfSaleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PointOfSaleApi apiInstance = new PointOfSaleApi();
Integer pointOfSaleId = 56; // Integer | 
PointOfSaleDto dto = new PointOfSaleDto(); // PointOfSaleDto | 
try {
    Object result = apiInstance.pointOfSaleUpdatePointOfSale(pointOfSaleId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PointOfSaleApi#pointOfSaleUpdatePointOfSale");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pointOfSaleId** | **Integer**|  |
 **dto** | [**PointOfSaleDto**](PointOfSaleDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

