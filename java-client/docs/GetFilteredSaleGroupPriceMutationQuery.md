
# GetFilteredSaleGroupPriceMutationQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerIdMinValue** | **Integer** |  |  [optional]
**customerIdMaxValue** | **Integer** |  |  [optional]
**salesGroupIdTypeMinValue** | **Integer** |  |  [optional]
**salesGroupIdTypeMaxValue** | **Integer** |  |  [optional]
**salesGroupIdMinValue** | **Integer** |  |  [optional]
**salesGroupIdMaxValue** | **Integer** |  |  [optional]



