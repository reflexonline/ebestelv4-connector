
# SaleArticleSubGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subgroupId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



