
# SaleReturnReasonDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**description** | **String** |  |  [optional]
**credit** | **Integer** |  |  [optional]
**updateStock** | **Boolean** |  |  [optional]
**invoiceText** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



