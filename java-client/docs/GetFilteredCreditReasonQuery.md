
# GetFilteredCreditReasonQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creditReasonIdMinValue** | **Integer** |  |  [optional]
**creditReasonIdMaxValue** | **Integer** |  |  [optional]
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]



