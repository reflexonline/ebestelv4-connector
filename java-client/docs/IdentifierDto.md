
# IdentifierDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **String** |  |  [optional]
**translationKey** | **String** |  |  [optional]



