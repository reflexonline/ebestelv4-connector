
# SupplierVisitAddressDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierId** | **Integer** |  | 
**searchName** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**searchName2** | **String** |  |  [optional]
**addressLine1** | **String** |  |  [optional]
**addressLine2** | **String** |  |  [optional]
**postalCode** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**phoneNumber** | **String** |  |  [optional]
**faxNumber** | **String** |  |  [optional]
**contactPerson** | **String** |  |  [optional]
**country** | **String** |  |  [optional]
**isoCountryCode** | **String** |  |  [optional]
**phoneNumberContactPerson** | **String** |  |  [optional]
**longName** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



