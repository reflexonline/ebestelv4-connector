
# DiscountGroupLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discountGroupId** | **Integer** |  | 
**discountEntityId** | **Integer** |  | 
**discountEntityType** | [**DiscountEntityTypeEnum**](#DiscountEntityTypeEnum) |  | 
**discountPercantage** | **Float** |  |  [optional]
**discountFrom** | **Float** |  |  [optional]
**color** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="DiscountEntityTypeEnum"></a>
## Enum: DiscountEntityTypeEnum
Name | Value
---- | -----
SALEARTICLEMAINGROUP | &quot;SaleArticleMainGroup&quot;
SALEARTICLEGROUP | &quot;SaleArticleGroup&quot;
SALEARTICLESUBGROUP | &quot;SaleArticleSubgroup&quot;
SALEARTICLE | &quot;SaleArticle&quot;
FROMORDERAMOUNT | &quot;FromOrderAmount&quot;



