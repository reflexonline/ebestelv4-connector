
# CurrencyDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**description** | **String** |  |  [optional]
**munt** | **String** |  |  [optional]
**dagBoekVerkoop** | **String** |  |  [optional]
**dagBoekInkoop** | **String** |  |  [optional]
**dagBoekMemoriaal** | **String** |  |  [optional]
**koers** | **Double** |  |  [optional]
**geinverteerdeKoers** | **Double** |  |  [optional]
**komma** | **Integer** |  |  [optional]
**vatL** | **Double** |  |  [optional]
**vatH** | **Double** |  |  [optional]
**vatD** | **Double** |  |  [optional]
**ediMuntCode** | **String** |  |  [optional]
**creditInvoiceJournal** | **String** |  |  [optional]
**vatScenario** | **Integer** |  |  [optional]
**decimalPositionPrice** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



