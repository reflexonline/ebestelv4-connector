# StockApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stockGetSalesArticleStockAvailability**](StockApi.md#stockGetSalesArticleStockAvailability) | **GET** /SaleArticle/{saleArticleId}/StockAvailability | 
[**stockGetSalesArticleStockAvailability_0**](StockApi.md#stockGetSalesArticleStockAvailability_0) | **POST** /SaleArticle/{saleArticleId}/StockAvailability | 
[**stockValidateStockAvailability**](StockApi.md#stockValidateStockAvailability) | **POST** /SaleOrder/{saleOrderId}/StockAvailability | 


<a name="stockGetSalesArticleStockAvailability"></a>
# **stockGetSalesArticleStockAvailability**
> Object stockGetSalesArticleStockAvailability(saleArticleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockApi apiInstance = new StockApi();
Integer saleArticleId = 56; // Integer | 
try {
    Object result = apiInstance.stockGetSalesArticleStockAvailability(saleArticleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockApi#stockGetSalesArticleStockAvailability");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="stockGetSalesArticleStockAvailability_0"></a>
# **stockGetSalesArticleStockAvailability_0**
> Object stockGetSalesArticleStockAvailability_0(saleArticleId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockApi apiInstance = new StockApi();
Integer saleArticleId = 56; // Integer | 
SalesArticleStockAvailabilityDto dto = new SalesArticleStockAvailabilityDto(); // SalesArticleStockAvailabilityDto | 
try {
    Object result = apiInstance.stockGetSalesArticleStockAvailability_0(saleArticleId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockApi#stockGetSalesArticleStockAvailability_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleArticleId** | **Integer**|  |
 **dto** | [**SalesArticleStockAvailabilityDto**](SalesArticleStockAvailabilityDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="stockValidateStockAvailability"></a>
# **stockValidateStockAvailability**
> Object stockValidateStockAvailability(saleOrderId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockApi apiInstance = new StockApi();
Integer saleOrderId = 56; // Integer | 
SaleOrderStockAvailabilityDto dto = new SaleOrderStockAvailabilityDto(); // SaleOrderStockAvailabilityDto | 
try {
    Object result = apiInstance.stockValidateStockAvailability(saleOrderId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockApi#stockValidateStockAvailability");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleOrderId** | **Integer**|  |
 **dto** | [**SaleOrderStockAvailabilityDto**](SaleOrderStockAvailabilityDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

