# SaleOrderTextLineApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleOrderTextLineExistsSaleOrderTextLine**](SaleOrderTextLineApi.md#saleOrderTextLineExistsSaleOrderTextLine) | **GET** /SaleOrderTextLine/exists/{orderId}/{lineId} | 
[**saleOrderTextLineGetSaleOrderTextLine**](SaleOrderTextLineApi.md#saleOrderTextLineGetSaleOrderTextLine) | **GET** /SaleOrderTextLine/{orderId}/{lineId} | 
[**saleOrderTextLineGetSaleOrderTextLines**](SaleOrderTextLineApi.md#saleOrderTextLineGetSaleOrderTextLines) | **POST** /SaleOrderTextLine/search | 


<a name="saleOrderTextLineExistsSaleOrderTextLine"></a>
# **saleOrderTextLineExistsSaleOrderTextLine**
> Object saleOrderTextLineExistsSaleOrderTextLine(orderId, lineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderTextLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderTextLineApi apiInstance = new SaleOrderTextLineApi();
Integer orderId = 56; // Integer | 
Integer lineId = 56; // Integer | 
try {
    Object result = apiInstance.saleOrderTextLineExistsSaleOrderTextLine(orderId, lineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderTextLineApi#saleOrderTextLineExistsSaleOrderTextLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderId** | **Integer**|  |
 **lineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleOrderTextLineGetSaleOrderTextLine"></a>
# **saleOrderTextLineGetSaleOrderTextLine**
> Object saleOrderTextLineGetSaleOrderTextLine(orderId, lineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderTextLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderTextLineApi apiInstance = new SaleOrderTextLineApi();
Integer orderId = 56; // Integer | 
Integer lineId = 56; // Integer | 
try {
    Object result = apiInstance.saleOrderTextLineGetSaleOrderTextLine(orderId, lineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderTextLineApi#saleOrderTextLineGetSaleOrderTextLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderId** | **Integer**|  |
 **lineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleOrderTextLineGetSaleOrderTextLines"></a>
# **saleOrderTextLineGetSaleOrderTextLines**
> Object saleOrderTextLineGetSaleOrderTextLines(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderTextLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderTextLineApi apiInstance = new SaleOrderTextLineApi();
GetFilteredSaleOrderTextLineQuery query = new GetFilteredSaleOrderTextLineQuery(); // GetFilteredSaleOrderTextLineQuery | 
try {
    Object result = apiInstance.saleOrderTextLineGetSaleOrderTextLines(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderTextLineApi#saleOrderTextLineGetSaleOrderTextLines");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSaleOrderTextLineQuery**](GetFilteredSaleOrderTextLineQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

