
# PicAPictureGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**picAPictureGroupId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**minWeight** | **Double** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



