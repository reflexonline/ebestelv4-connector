
# GetFilteredCustomerPostalAddressQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerIdMinValue** | **Integer** |  |  [optional]
**customerIdMaxValue** | **Integer** |  |  [optional]
**searchNameMinValue** | **String** |  |  [optional]
**searchNameMaxValue** | **String** |  |  [optional]
**postalCodeMinValue** | **String** |  |  [optional]
**postalCodeMaxValue** | **String** |  |  [optional]
**cityMinValue** | **String** |  |  [optional]
**cityMaxValue** | **String** |  |  [optional]
**deliveryMethodIdMinValue** | **Integer** |  |  [optional]
**deliveryMethodIdMaxValue** | **Integer** |  |  [optional]



