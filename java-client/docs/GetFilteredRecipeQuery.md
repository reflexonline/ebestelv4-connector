
# GetFilteredRecipeQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipeGroupIdMinValue** | **Integer** |  |  [optional]
**recipeGroupIdMaxValue** | **Integer** |  |  [optional]
**recipeTypeMinValue** | [**RecipeTypeMinValueEnum**](#RecipeTypeMinValueEnum) |  |  [optional]
**recipeTypeMaxValue** | [**RecipeTypeMaxValueEnum**](#RecipeTypeMaxValueEnum) |  |  [optional]
**recipeIdMinValue** | **Integer** |  |  [optional]
**recipeIdMaxValue** | **Integer** |  |  [optional]
**recipeDerivativeIdMinValue** | **Integer** |  |  [optional]
**recipeDerivativeIdMaxValue** | **Integer** |  |  [optional]
**articleIdMinValue** | **Integer** |  |  [optional]
**articleIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]
**remarkMinValue** | **String** |  |  [optional]
**remarkMaxValue** | **String** |  |  [optional]
**isBaseRecipeMinValue** | **Boolean** |  |  [optional]
**isBaseRecipeMaxValue** | **Boolean** |  |  [optional]
**productionPlanningFilterIdMinValue** | **Integer** |  |  [optional]
**productionPlanningFilterIdMaxValue** | **Integer** |  |  [optional]


<a name="RecipeTypeMinValueEnum"></a>
## Enum: RecipeTypeMinValueEnum
Name | Value
---- | -----
SALE | &quot;Sale&quot;
PURCHASE | &quot;Purchase&quot;
UNLINKED | &quot;Unlinked&quot;


<a name="RecipeTypeMaxValueEnum"></a>
## Enum: RecipeTypeMaxValueEnum
Name | Value
---- | -----
SALE | &quot;Sale&quot;
PURCHASE | &quot;Purchase&quot;
UNLINKED | &quot;Unlinked&quot;



