
# GetFilteredTransporterQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transporterIdMinValue** | **Integer** |  |  [optional]
**transporterIdMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]
**postalCodeMinValue** | **String** |  |  [optional]
**postalCodeMaxValue** | **String** |  |  [optional]
**cityMinValue** | **String** |  |  [optional]
**cityMaxValue** | **String** |  |  [optional]
**isoCountryCodeMinValue** | **String** |  |  [optional]
**isoCountryCodeMaxValue** | **String** |  |  [optional]
**transporterTypeMinValue** | **Integer** |  |  [optional]
**transporterTypeMaxValue** | **Integer** |  |  [optional]



