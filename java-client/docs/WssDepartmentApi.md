# WssDepartmentApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**wssDepartmentCreateWssDepartment**](WssDepartmentApi.md#wssDepartmentCreateWssDepartment) | **POST** /WssDepartment | 
[**wssDepartmentDeleteWssDepartment**](WssDepartmentApi.md#wssDepartmentDeleteWssDepartment) | **DELETE** /WssDepartment/{number} | 
[**wssDepartmentExistsWssDepartment**](WssDepartmentApi.md#wssDepartmentExistsWssDepartment) | **GET** /WssDepartment/exists/{number} | 
[**wssDepartmentGetWssDepartment**](WssDepartmentApi.md#wssDepartmentGetWssDepartment) | **GET** /WssDepartment/{number} | 
[**wssDepartmentGetWssDepartments**](WssDepartmentApi.md#wssDepartmentGetWssDepartments) | **POST** /WssDepartment/search | 
[**wssDepartmentUpdateWssDepartment**](WssDepartmentApi.md#wssDepartmentUpdateWssDepartment) | **PUT** /WssDepartment/{number} | 


<a name="wssDepartmentCreateWssDepartment"></a>
# **wssDepartmentCreateWssDepartment**
> Object wssDepartmentCreateWssDepartment(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WssDepartmentApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WssDepartmentApi apiInstance = new WssDepartmentApi();
WssDepartmentDto dto = new WssDepartmentDto(); // WssDepartmentDto | 
try {
    Object result = apiInstance.wssDepartmentCreateWssDepartment(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WssDepartmentApi#wssDepartmentCreateWssDepartment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**WssDepartmentDto**](WssDepartmentDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="wssDepartmentDeleteWssDepartment"></a>
# **wssDepartmentDeleteWssDepartment**
> Object wssDepartmentDeleteWssDepartment(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WssDepartmentApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WssDepartmentApi apiInstance = new WssDepartmentApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.wssDepartmentDeleteWssDepartment(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WssDepartmentApi#wssDepartmentDeleteWssDepartment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="wssDepartmentExistsWssDepartment"></a>
# **wssDepartmentExistsWssDepartment**
> Object wssDepartmentExistsWssDepartment(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WssDepartmentApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WssDepartmentApi apiInstance = new WssDepartmentApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.wssDepartmentExistsWssDepartment(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WssDepartmentApi#wssDepartmentExistsWssDepartment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="wssDepartmentGetWssDepartment"></a>
# **wssDepartmentGetWssDepartment**
> Object wssDepartmentGetWssDepartment(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WssDepartmentApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WssDepartmentApi apiInstance = new WssDepartmentApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.wssDepartmentGetWssDepartment(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WssDepartmentApi#wssDepartmentGetWssDepartment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="wssDepartmentGetWssDepartments"></a>
# **wssDepartmentGetWssDepartments**
> Object wssDepartmentGetWssDepartments(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WssDepartmentApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WssDepartmentApi apiInstance = new WssDepartmentApi();
GetFilteredWssDepartmentQuery query = new GetFilteredWssDepartmentQuery(); // GetFilteredWssDepartmentQuery | 
try {
    Object result = apiInstance.wssDepartmentGetWssDepartments(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WssDepartmentApi#wssDepartmentGetWssDepartments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredWssDepartmentQuery**](GetFilteredWssDepartmentQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="wssDepartmentUpdateWssDepartment"></a>
# **wssDepartmentUpdateWssDepartment**
> Object wssDepartmentUpdateWssDepartment(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WssDepartmentApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WssDepartmentApi apiInstance = new WssDepartmentApi();
Integer number = 56; // Integer | 
WssDepartmentDto dto = new WssDepartmentDto(); // WssDepartmentDto | 
try {
    Object result = apiInstance.wssDepartmentUpdateWssDepartment(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WssDepartmentApi#wssDepartmentUpdateWssDepartment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**WssDepartmentDto**](WssDepartmentDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

