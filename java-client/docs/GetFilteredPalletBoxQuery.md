
# GetFilteredPalletBoxQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**palletIdMinValue** | **Integer** |  |  [optional]
**palletIdMaxValue** | **Integer** |  |  [optional]
**lineNumberMinValue** | **Integer** |  |  [optional]
**lineNumberMaxValue** | **Integer** |  |  [optional]
**boxIdMinValue** | **Integer** |  |  [optional]
**boxIdMaxValue** | **Integer** |  |  [optional]
**bestBeforeFreshMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**bestBeforeFreshMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**purchaseOrderNumberMinValue** | **Integer** |  |  [optional]
**purchaseOrderNumberMaxValue** | **Integer** |  |  [optional]
**purchaseOrderLineIdMinValue** | **Integer** |  |  [optional]
**purchaseOrderLineIdMaxValue** | **Integer** |  |  [optional]
**saleOrderNumberMinValue** | **Integer** |  |  [optional]
**saleOrderNumberMaxValue** | **Integer** |  |  [optional]
**dateReceivedMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**dateReceivedMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**packingArticleIdMinValue** | **Integer** |  |  [optional]
**packingArticleIdMaxValue** | **Integer** |  |  [optional]
**supplierIdMinValue** | **Integer** |  |  [optional]
**supplierIdMaxValue** | **Integer** |  |  [optional]
**ownerIdMinValue** | **Integer** |  |  [optional]
**ownerIdMaxValue** | **Integer** |  |  [optional]
**customerIdMinValue** | **Integer** |  |  [optional]
**customerIdMaxValue** | **Integer** |  |  [optional]
**batchIdMinValue** | **Integer** |  |  [optional]
**batchIdMaxValue** | **Integer** |  |  [optional]
**saleArticleIdMinValue** | **Integer** |  |  [optional]
**saleArticleIdMaxValue** | **Integer** |  |  [optional]
**extBoxIdMinValue** | **String** |  |  [optional]
**extBoxIdMaxValue** | **String** |  |  [optional]
**graiCodeMinValue** | **String** |  |  [optional]
**graiCodeMaxValue** | **String** |  |  [optional]
**purchaseArticleIdMinValue** | **Integer** |  |  [optional]
**purchaseArticleIdMaxValue** | **Integer** |  |  [optional]



