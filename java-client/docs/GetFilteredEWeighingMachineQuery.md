
# GetFilteredEWeighingMachineQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**machineIdMinValue** | **Integer** |  |  [optional]
**machineIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



