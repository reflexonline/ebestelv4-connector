
# GetFilteredSaleArticleSubGroupQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]
**subgroupIdMinValue** | **Integer** |  |  [optional]
**subgroupIdMaxValue** | **Integer** |  |  [optional]



