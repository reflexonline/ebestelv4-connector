
# LotDefinitionDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lotDefinitionId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**hasCountryOfBirth** | **Boolean** |  |  [optional]
**hasCountryOfFattening** | **Boolean** |  |  [optional]
**hasCountryOfButchering** | **Boolean** |  |  [optional]
**hasSlaughterPlaceId** | **Boolean** |  |  [optional]
**hasCountryOfCutting** | **Boolean** |  |  [optional]
**hasCuttingPlaceId** | **Boolean** |  |  [optional]
**fedisGeslachtIn** | **Boolean** |  |  [optional]
**fedisVersnedenIn** | **Boolean** |  |  [optional]
**hasDateOfButchering** | **Boolean** |  |  [optional]
**hasCattleCategory** | **Boolean** |  |  [optional]
**hasChickenTraceIdentifier** | **Boolean** |  |  [optional]
**hasPoultryFattener** | **Boolean** |  |  [optional]
**hasDateCaptured** | **Boolean** |  |  [optional]
**hasCaptureRegion** | **Boolean** |  |  [optional]
**hasCaptureRegionSector** | **Boolean** |  |  [optional]
**hasShipNumber** | **Boolean** |  |  [optional]
**hasCaptureRegionIso** | **Boolean** |  |  [optional]
**keptIn** | **Boolean** |  |  [optional]
**keptInEUCountry** | **Boolean** |  |  [optional]
**hasFishingGearId** | **Boolean** |  |  [optional]
**hasCuttingDate** | **Boolean** |  |  [optional]
**hasProductionDate** | **Boolean** |  |  [optional]
**hasPackingDate** | **Boolean** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



