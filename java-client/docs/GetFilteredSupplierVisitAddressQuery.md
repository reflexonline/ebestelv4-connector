
# GetFilteredSupplierVisitAddressQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierIdMinValue** | **Integer** |  |  [optional]
**supplierIdMaxValue** | **Integer** |  |  [optional]
**searchNameMinValue** | **String** |  |  [optional]
**searchNameMaxValue** | **String** |  |  [optional]
**postalCodeMinValue** | **String** |  |  [optional]
**postalCodeMaxValue** | **String** |  |  [optional]
**cityMinValue** | **String** |  |  [optional]
**cityMaxValue** | **String** |  |  [optional]



