
# HaccpQuestionFormDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**description** | **String** |  |  [optional]
**version** | **String** |  |  [optional]
**remark** | **String** |  |  [optional]
**creationDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**mutationDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



