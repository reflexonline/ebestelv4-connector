
# SaleArticleNutritionalDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nutritional** | **Integer** |  | 
**unknown** | **Boolean** |  |  [optional]
**print** | **Boolean** |  |  [optional]
**manual** | **Boolean** |  |  [optional]
**nutritionalValue** | **Double** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



