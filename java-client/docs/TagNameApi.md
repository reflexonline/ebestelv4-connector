# TagNameApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tagNameCreateTagName**](TagNameApi.md#tagNameCreateTagName) | **POST** /TagName | 
[**tagNameDeleteTagName**](TagNameApi.md#tagNameDeleteTagName) | **DELETE** /TagName/{tagId} | 
[**tagNameExistsTagName**](TagNameApi.md#tagNameExistsTagName) | **GET** /TagName/exists/{tagId} | 
[**tagNameGetTagName**](TagNameApi.md#tagNameGetTagName) | **GET** /TagName/{tagId} | 
[**tagNameGetTagNames**](TagNameApi.md#tagNameGetTagNames) | **POST** /TagName/search | 
[**tagNameUpdateTagName**](TagNameApi.md#tagNameUpdateTagName) | **PUT** /TagName/{tagId} | 


<a name="tagNameCreateTagName"></a>
# **tagNameCreateTagName**
> Object tagNameCreateTagName(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TagNameApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

TagNameApi apiInstance = new TagNameApi();
TagNameDto dto = new TagNameDto(); // TagNameDto | 
try {
    Object result = apiInstance.tagNameCreateTagName(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TagNameApi#tagNameCreateTagName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**TagNameDto**](TagNameDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="tagNameDeleteTagName"></a>
# **tagNameDeleteTagName**
> Object tagNameDeleteTagName(tagId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TagNameApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

TagNameApi apiInstance = new TagNameApi();
Integer tagId = 56; // Integer | 
try {
    Object result = apiInstance.tagNameDeleteTagName(tagId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TagNameApi#tagNameDeleteTagName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="tagNameExistsTagName"></a>
# **tagNameExistsTagName**
> Object tagNameExistsTagName(tagId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TagNameApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

TagNameApi apiInstance = new TagNameApi();
Integer tagId = 56; // Integer | 
try {
    Object result = apiInstance.tagNameExistsTagName(tagId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TagNameApi#tagNameExistsTagName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="tagNameGetTagName"></a>
# **tagNameGetTagName**
> Object tagNameGetTagName(tagId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TagNameApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

TagNameApi apiInstance = new TagNameApi();
Integer tagId = 56; // Integer | 
try {
    Object result = apiInstance.tagNameGetTagName(tagId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TagNameApi#tagNameGetTagName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="tagNameGetTagNames"></a>
# **tagNameGetTagNames**
> Object tagNameGetTagNames(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TagNameApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

TagNameApi apiInstance = new TagNameApi();
GetFilteredTagNameQuery query = new GetFilteredTagNameQuery(); // GetFilteredTagNameQuery | 
try {
    Object result = apiInstance.tagNameGetTagNames(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TagNameApi#tagNameGetTagNames");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredTagNameQuery**](GetFilteredTagNameQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="tagNameUpdateTagName"></a>
# **tagNameUpdateTagName**
> Object tagNameUpdateTagName(tagId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TagNameApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

TagNameApi apiInstance = new TagNameApi();
Integer tagId = 56; // Integer | 
TagNameDto dto = new TagNameDto(); // TagNameDto | 
try {
    Object result = apiInstance.tagNameUpdateTagName(tagId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TagNameApi#tagNameUpdateTagName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagId** | **Integer**|  |
 **dto** | [**TagNameDto**](TagNameDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

