
# GetFilteredSupplierDeliveryAddressQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierIdMinValue** | **Integer** |  |  [optional]
**supplierIdMaxValue** | **Integer** |  |  [optional]



