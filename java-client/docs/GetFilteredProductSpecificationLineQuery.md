
# GetFilteredProductSpecificationLineQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productSpecificationCategorieIdMinValue** | **Integer** |  |  [optional]
**productSpecificationCategorieIdMaxValue** | **Integer** |  |  [optional]
**productSpecificationLineIdMinValue** | **Integer** |  |  [optional]
**productSpecificationLineIdMaxValue** | **Integer** |  |  [optional]
**articleIdMinValue** | **Integer** |  |  [optional]
**articleIdMaxValue** | **Integer** |  |  [optional]
**productSpecificationUnitIdMinValue** | **Integer** |  |  [optional]
**productSpecificationUnitIdMaxValue** | **Integer** |  |  [optional]



