# CustomerDeliveryAddressApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerDeliveryAddressExistsCustomerDeliveryAddress**](CustomerDeliveryAddressApi.md#customerDeliveryAddressExistsCustomerDeliveryAddress) | **GET** /CustomerDeliveryAddress/exists/{customerId} | 
[**customerDeliveryAddressGetCustomerDeliveryAddress**](CustomerDeliveryAddressApi.md#customerDeliveryAddressGetCustomerDeliveryAddress) | **GET** /CustomerDeliveryAddress/{customerId} | 
[**customerDeliveryAddressGetCustomerDeliveryAddresses**](CustomerDeliveryAddressApi.md#customerDeliveryAddressGetCustomerDeliveryAddresses) | **POST** /CustomerDeliveryAddress/search | 


<a name="customerDeliveryAddressExistsCustomerDeliveryAddress"></a>
# **customerDeliveryAddressExistsCustomerDeliveryAddress**
> Object customerDeliveryAddressExistsCustomerDeliveryAddress(customerId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerDeliveryAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CustomerDeliveryAddressApi apiInstance = new CustomerDeliveryAddressApi();
Integer customerId = 56; // Integer | 
try {
    Object result = apiInstance.customerDeliveryAddressExistsCustomerDeliveryAddress(customerId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerDeliveryAddressApi#customerDeliveryAddressExistsCustomerDeliveryAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="customerDeliveryAddressGetCustomerDeliveryAddress"></a>
# **customerDeliveryAddressGetCustomerDeliveryAddress**
> Object customerDeliveryAddressGetCustomerDeliveryAddress(customerId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerDeliveryAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CustomerDeliveryAddressApi apiInstance = new CustomerDeliveryAddressApi();
Integer customerId = 56; // Integer | 
try {
    Object result = apiInstance.customerDeliveryAddressGetCustomerDeliveryAddress(customerId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerDeliveryAddressApi#customerDeliveryAddressGetCustomerDeliveryAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="customerDeliveryAddressGetCustomerDeliveryAddresses"></a>
# **customerDeliveryAddressGetCustomerDeliveryAddresses**
> Object customerDeliveryAddressGetCustomerDeliveryAddresses(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerDeliveryAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CustomerDeliveryAddressApi apiInstance = new CustomerDeliveryAddressApi();
GetFilteredCustomerDeliveryAddressQuery query = new GetFilteredCustomerDeliveryAddressQuery(); // GetFilteredCustomerDeliveryAddressQuery | 
try {
    Object result = apiInstance.customerDeliveryAddressGetCustomerDeliveryAddresses(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerDeliveryAddressApi#customerDeliveryAddressGetCustomerDeliveryAddresses");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredCustomerDeliveryAddressQuery**](GetFilteredCustomerDeliveryAddressQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

