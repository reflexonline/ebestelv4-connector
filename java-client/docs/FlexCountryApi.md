# FlexCountryApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**flexCountryCreateFlexCountry**](FlexCountryApi.md#flexCountryCreateFlexCountry) | **POST** /FlexCountry | 
[**flexCountryDeleteFlexCountry**](FlexCountryApi.md#flexCountryDeleteFlexCountry) | **DELETE** /FlexCountry/{countryId} | 
[**flexCountryExistsFlexCountry**](FlexCountryApi.md#flexCountryExistsFlexCountry) | **GET** /FlexCountry/exists/{countryId} | 
[**flexCountryGetFlexCountries**](FlexCountryApi.md#flexCountryGetFlexCountries) | **POST** /FlexCountry/search | 
[**flexCountryGetFlexCountry**](FlexCountryApi.md#flexCountryGetFlexCountry) | **GET** /FlexCountry/{countryId} | 
[**flexCountryUpdateFlexCountry**](FlexCountryApi.md#flexCountryUpdateFlexCountry) | **PUT** /FlexCountry/{countryId} | 


<a name="flexCountryCreateFlexCountry"></a>
# **flexCountryCreateFlexCountry**
> Object flexCountryCreateFlexCountry(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FlexCountryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

FlexCountryApi apiInstance = new FlexCountryApi();
FlexCountryDto dto = new FlexCountryDto(); // FlexCountryDto | 
try {
    Object result = apiInstance.flexCountryCreateFlexCountry(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FlexCountryApi#flexCountryCreateFlexCountry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**FlexCountryDto**](FlexCountryDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="flexCountryDeleteFlexCountry"></a>
# **flexCountryDeleteFlexCountry**
> Object flexCountryDeleteFlexCountry(countryId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FlexCountryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

FlexCountryApi apiInstance = new FlexCountryApi();
Integer countryId = 56; // Integer | 
try {
    Object result = apiInstance.flexCountryDeleteFlexCountry(countryId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FlexCountryApi#flexCountryDeleteFlexCountry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **countryId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="flexCountryExistsFlexCountry"></a>
# **flexCountryExistsFlexCountry**
> Object flexCountryExistsFlexCountry(countryId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FlexCountryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

FlexCountryApi apiInstance = new FlexCountryApi();
Integer countryId = 56; // Integer | 
try {
    Object result = apiInstance.flexCountryExistsFlexCountry(countryId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FlexCountryApi#flexCountryExistsFlexCountry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **countryId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="flexCountryGetFlexCountries"></a>
# **flexCountryGetFlexCountries**
> Object flexCountryGetFlexCountries(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FlexCountryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

FlexCountryApi apiInstance = new FlexCountryApi();
GetFilteredFlexCountryQuery query = new GetFilteredFlexCountryQuery(); // GetFilteredFlexCountryQuery | 
try {
    Object result = apiInstance.flexCountryGetFlexCountries(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FlexCountryApi#flexCountryGetFlexCountries");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredFlexCountryQuery**](GetFilteredFlexCountryQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="flexCountryGetFlexCountry"></a>
# **flexCountryGetFlexCountry**
> Object flexCountryGetFlexCountry(countryId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FlexCountryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

FlexCountryApi apiInstance = new FlexCountryApi();
Integer countryId = 56; // Integer | 
try {
    Object result = apiInstance.flexCountryGetFlexCountry(countryId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FlexCountryApi#flexCountryGetFlexCountry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **countryId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="flexCountryUpdateFlexCountry"></a>
# **flexCountryUpdateFlexCountry**
> Object flexCountryUpdateFlexCountry(countryId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FlexCountryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

FlexCountryApi apiInstance = new FlexCountryApi();
Integer countryId = 56; // Integer | 
FlexCountryDto dto = new FlexCountryDto(); // FlexCountryDto | 
try {
    Object result = apiInstance.flexCountryUpdateFlexCountry(countryId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FlexCountryApi#flexCountryUpdateFlexCountry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **countryId** | **Integer**|  |
 **dto** | [**FlexCountryDto**](FlexCountryDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

