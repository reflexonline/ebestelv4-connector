# ProductionFlexFilterGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productionFlexFilterGroupCreateProductionFlexFilterGroup**](ProductionFlexFilterGroupApi.md#productionFlexFilterGroupCreateProductionFlexFilterGroup) | **POST** /ProductionFlexFilterGroup | 
[**productionFlexFilterGroupDeleteProductionFlexFilterGroup**](ProductionFlexFilterGroupApi.md#productionFlexFilterGroupDeleteProductionFlexFilterGroup) | **DELETE** /ProductionFlexFilterGroup/{productionFlexFilterGroupId} | 
[**productionFlexFilterGroupExistsProductionFlexFilterGroup**](ProductionFlexFilterGroupApi.md#productionFlexFilterGroupExistsProductionFlexFilterGroup) | **GET** /ProductionFlexFilterGroup/exists/{productionFlexFilterGroupId} | 
[**productionFlexFilterGroupGetProductionFlexFilterGroup**](ProductionFlexFilterGroupApi.md#productionFlexFilterGroupGetProductionFlexFilterGroup) | **GET** /ProductionFlexFilterGroup/{productionFlexFilterGroupId} | 
[**productionFlexFilterGroupGetProductionFlexFilterGroups**](ProductionFlexFilterGroupApi.md#productionFlexFilterGroupGetProductionFlexFilterGroups) | **POST** /ProductionFlexFilterGroup/search | 
[**productionFlexFilterGroupUpdateProductionFlexFilterGroup**](ProductionFlexFilterGroupApi.md#productionFlexFilterGroupUpdateProductionFlexFilterGroup) | **PUT** /ProductionFlexFilterGroup/{productionFlexFilterGroupId} | 


<a name="productionFlexFilterGroupCreateProductionFlexFilterGroup"></a>
# **productionFlexFilterGroupCreateProductionFlexFilterGroup**
> Object productionFlexFilterGroupCreateProductionFlexFilterGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionFlexFilterGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionFlexFilterGroupApi apiInstance = new ProductionFlexFilterGroupApi();
ProductionFlexFilterGroupDto dto = new ProductionFlexFilterGroupDto(); // ProductionFlexFilterGroupDto | 
try {
    Object result = apiInstance.productionFlexFilterGroupCreateProductionFlexFilterGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionFlexFilterGroupApi#productionFlexFilterGroupCreateProductionFlexFilterGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ProductionFlexFilterGroupDto**](ProductionFlexFilterGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productionFlexFilterGroupDeleteProductionFlexFilterGroup"></a>
# **productionFlexFilterGroupDeleteProductionFlexFilterGroup**
> Object productionFlexFilterGroupDeleteProductionFlexFilterGroup(productionFlexFilterGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionFlexFilterGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionFlexFilterGroupApi apiInstance = new ProductionFlexFilterGroupApi();
Integer productionFlexFilterGroupId = 56; // Integer | 
try {
    Object result = apiInstance.productionFlexFilterGroupDeleteProductionFlexFilterGroup(productionFlexFilterGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionFlexFilterGroupApi#productionFlexFilterGroupDeleteProductionFlexFilterGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionFlexFilterGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionFlexFilterGroupExistsProductionFlexFilterGroup"></a>
# **productionFlexFilterGroupExistsProductionFlexFilterGroup**
> Object productionFlexFilterGroupExistsProductionFlexFilterGroup(productionFlexFilterGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionFlexFilterGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionFlexFilterGroupApi apiInstance = new ProductionFlexFilterGroupApi();
Integer productionFlexFilterGroupId = 56; // Integer | 
try {
    Object result = apiInstance.productionFlexFilterGroupExistsProductionFlexFilterGroup(productionFlexFilterGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionFlexFilterGroupApi#productionFlexFilterGroupExistsProductionFlexFilterGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionFlexFilterGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionFlexFilterGroupGetProductionFlexFilterGroup"></a>
# **productionFlexFilterGroupGetProductionFlexFilterGroup**
> Object productionFlexFilterGroupGetProductionFlexFilterGroup(productionFlexFilterGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionFlexFilterGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionFlexFilterGroupApi apiInstance = new ProductionFlexFilterGroupApi();
Integer productionFlexFilterGroupId = 56; // Integer | 
try {
    Object result = apiInstance.productionFlexFilterGroupGetProductionFlexFilterGroup(productionFlexFilterGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionFlexFilterGroupApi#productionFlexFilterGroupGetProductionFlexFilterGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionFlexFilterGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionFlexFilterGroupGetProductionFlexFilterGroups"></a>
# **productionFlexFilterGroupGetProductionFlexFilterGroups**
> Object productionFlexFilterGroupGetProductionFlexFilterGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionFlexFilterGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionFlexFilterGroupApi apiInstance = new ProductionFlexFilterGroupApi();
GetFilteredProductionFlexFilterGroupQuery query = new GetFilteredProductionFlexFilterGroupQuery(); // GetFilteredProductionFlexFilterGroupQuery | 
try {
    Object result = apiInstance.productionFlexFilterGroupGetProductionFlexFilterGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionFlexFilterGroupApi#productionFlexFilterGroupGetProductionFlexFilterGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductionFlexFilterGroupQuery**](GetFilteredProductionFlexFilterGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productionFlexFilterGroupUpdateProductionFlexFilterGroup"></a>
# **productionFlexFilterGroupUpdateProductionFlexFilterGroup**
> Object productionFlexFilterGroupUpdateProductionFlexFilterGroup(productionFlexFilterGroupId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionFlexFilterGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionFlexFilterGroupApi apiInstance = new ProductionFlexFilterGroupApi();
Integer productionFlexFilterGroupId = 56; // Integer | 
ProductionFlexFilterGroupDto dto = new ProductionFlexFilterGroupDto(); // ProductionFlexFilterGroupDto | 
try {
    Object result = apiInstance.productionFlexFilterGroupUpdateProductionFlexFilterGroup(productionFlexFilterGroupId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionFlexFilterGroupApi#productionFlexFilterGroupUpdateProductionFlexFilterGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionFlexFilterGroupId** | **Integer**|  |
 **dto** | [**ProductionFlexFilterGroupDto**](ProductionFlexFilterGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

