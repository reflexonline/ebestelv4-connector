
# RelationPostalAddressDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**addressLine1** | **String** |  |  [optional]
**addressLine2** | **String** |  |  [optional]
**postalCode** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**isoCountryCode** | **String** |  |  [optional]
**marktSegment** | **Integer** |  |  [optional]
**country** | **String** |  |  [optional]
**phoneNumber** | **String** |  |  [optional]
**faxNumber** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**longName** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



