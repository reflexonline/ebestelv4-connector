# CurrencyApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**currencyCreateCurrency**](CurrencyApi.md#currencyCreateCurrency) | **POST** /Currency | 
[**currencyDeleteCurrency**](CurrencyApi.md#currencyDeleteCurrency) | **DELETE** /Currency/{number} | 
[**currencyExistsCurrency**](CurrencyApi.md#currencyExistsCurrency) | **GET** /Currency/exists/{number} | 
[**currencyGetCurrencies**](CurrencyApi.md#currencyGetCurrencies) | **POST** /Currency/search | 
[**currencyGetCurrency**](CurrencyApi.md#currencyGetCurrency) | **GET** /Currency/{number} | 
[**currencyUpdateCurrency**](CurrencyApi.md#currencyUpdateCurrency) | **PUT** /Currency/{number} | 


<a name="currencyCreateCurrency"></a>
# **currencyCreateCurrency**
> Object currencyCreateCurrency(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CurrencyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CurrencyApi apiInstance = new CurrencyApi();
CurrencyDto dto = new CurrencyDto(); // CurrencyDto | 
try {
    Object result = apiInstance.currencyCreateCurrency(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrencyApi#currencyCreateCurrency");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**CurrencyDto**](CurrencyDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="currencyDeleteCurrency"></a>
# **currencyDeleteCurrency**
> Object currencyDeleteCurrency(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CurrencyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CurrencyApi apiInstance = new CurrencyApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.currencyDeleteCurrency(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrencyApi#currencyDeleteCurrency");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="currencyExistsCurrency"></a>
# **currencyExistsCurrency**
> Object currencyExistsCurrency(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CurrencyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CurrencyApi apiInstance = new CurrencyApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.currencyExistsCurrency(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrencyApi#currencyExistsCurrency");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="currencyGetCurrencies"></a>
# **currencyGetCurrencies**
> Object currencyGetCurrencies(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CurrencyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CurrencyApi apiInstance = new CurrencyApi();
GetFilteredCurrencyQuery query = new GetFilteredCurrencyQuery(); // GetFilteredCurrencyQuery | 
try {
    Object result = apiInstance.currencyGetCurrencies(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrencyApi#currencyGetCurrencies");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredCurrencyQuery**](GetFilteredCurrencyQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="currencyGetCurrency"></a>
# **currencyGetCurrency**
> Object currencyGetCurrency(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CurrencyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CurrencyApi apiInstance = new CurrencyApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.currencyGetCurrency(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrencyApi#currencyGetCurrency");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="currencyUpdateCurrency"></a>
# **currencyUpdateCurrency**
> Object currencyUpdateCurrency(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CurrencyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CurrencyApi apiInstance = new CurrencyApi();
Integer number = 56; // Integer | 
CurrencyDto dto = new CurrencyDto(); // CurrencyDto | 
try {
    Object result = apiInstance.currencyUpdateCurrency(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrencyApi#currencyUpdateCurrency");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**CurrencyDto**](CurrencyDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

