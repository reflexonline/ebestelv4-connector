
# MultiBranchDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**multiBranchId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**customerId** | **Integer** |  |  [optional]
**supplierId** | **Integer** |  |  [optional]
**relationId** | **Integer** |  |  [optional]
**fromProductionGroupId** | **Integer** |  |  [optional]
**untilProductionGroupId** | **Integer** |  |  [optional]
**isMainBranch** | **Integer** |  |  [optional]
**purchaseFromProductionGroupId** | **Integer** |  |  [optional]
**purchaseUntilProductionGroupId** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



