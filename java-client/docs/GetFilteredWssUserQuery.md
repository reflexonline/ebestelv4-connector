
# GetFilteredWssUserQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]
**department1MinValue** | **Integer** |  |  [optional]
**department1MaxValue** | **Integer** |  |  [optional]
**department2MinValue** | **Integer** |  |  [optional]
**department2MaxValue** | **Integer** |  |  [optional]
**department3MinValue** | **Integer** |  |  [optional]
**department3MaxValue** | **Integer** |  |  [optional]
**department4MinValue** | **Integer** |  |  [optional]
**department4MaxValue** | **Integer** |  |  [optional]
**department5MinValue** | **Integer** |  |  [optional]
**department5MaxValue** | **Integer** |  |  [optional]
**department6MinValue** | **Integer** |  |  [optional]
**department6MaxValue** | **Integer** |  |  [optional]
**previousDepartmentMinValue** | **Integer** |  |  [optional]
**previousDepartmentMaxValue** | **Integer** |  |  [optional]
**multiBranchIdMinValue** | **Integer** |  |  [optional]
**multiBranchIdMaxValue** | **Integer** |  |  [optional]



