
# AdministrationBranchDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**name** | **String** |  |  [optional]
**uncPath** | **String** |  |  [optional]
**administrationBranchType** | [**AdministrationBranchTypeEnum**](#AdministrationBranchTypeEnum) |  |  [optional]
**customerExportDefinitionCode** | **String** |  |  [optional]
**saleArticleExportDefinitionCode** | **String** |  |  [optional]
**supplierExportDefinitionCode** | **String** |  |  [optional]
**purchaseArticleExportDefinitionCode** | **String** |  |  [optional]
**saleOrderHeaderExportDefinitionCode** | **String** |  |  [optional]
**saleOrderLineExportDefinitionCode** | **String** |  |  [optional]
**purchaseOrderHeaderExportDefinitionCode** | **String** |  |  [optional]
**purchaseOrderLineExportDefinitionCode** | **String** |  |  [optional]
**automaticallyExchangeSaleOrder** | **Boolean** |  |  [optional]
**automaticallyExchangePurchaseOrder** | **Boolean** |  |  [optional]
**sendPurchaseOrderPriceToAdministrationBranch** | **Boolean** |  |  [optional]
**importSaleOrderPriceFromAdministrationBranch** | **Boolean** |  |  [optional]
**sendPurchaseOrderReferenceToAdministrationBranch** | **Boolean** |  |  [optional]
**sendPackingSlipNumberToAdministrationBranch** | **Boolean** |  |  [optional]
**sendWeigingListNumberToAdministrationBranch** | **Boolean** |  |  [optional]
**importSaleOrderConsumerPriceFromAdministrationBranch** | **Boolean** |  |  [optional]
**sendSaleOrderConsumerPriceToAdministrationBranch** | **Boolean** |  |  [optional]
**sendPrognosisSaleOrderToAdministrationBranch** | **Boolean** |  |  [optional]
**specialOfferExportDefinitionCode** | **String** |  |  [optional]
**priceAgreementExportDefinitionCode** | **String** |  |  [optional]
**saleOrderExchangeMethod** | [**SaleOrderExchangeMethodEnum**](#SaleOrderExchangeMethodEnum) |  |  [optional]
**purchaseOrderExchangeMethod** | [**PurchaseOrderExchangeMethodEnum**](#PurchaseOrderExchangeMethodEnum) |  |  [optional]
**importBatchInformationForSaleOrder** | **Boolean** |  |  [optional]
**importBatchInformationForPurchaseOrder** | **Boolean** |  |  [optional]
**exportAdressDataForSaleOrder** | **Boolean** |  |  [optional]
**exportAdressDataForPurchaseOrder** | **Boolean** |  |  [optional]
**exportArticleNamesForSaleOrder** | **Boolean** |  |  [optional]
**exportArticleNamesForPurchaseOrder** | **Boolean** |  |  [optional]
**exportRouteForSaleOrder** | **Boolean** |  |  [optional]
**saleOrderTemplateExportDefinitionCode** | **String** |  |  [optional]
**dwhDefaultLanguageId** | **Integer** |  |  [optional]
**givesNoFeedback** | **Boolean** |  |  [optional]
**identificationCode** | **String** |  |  [optional]
**deletePrognoseSaleOrderMethod** | **Integer** |  |  [optional]
**deleteSaleOrderMethod** | **Integer** |  |  [optional]
**deletePurchaseOrderMethod** | **Integer** |  |  [optional]
**sendSaleOrderReferenceToAdministrationBranch** | **Boolean** |  |  [optional]
**sendSaleOrderPriceToAdministrationBranch** | **Boolean** |  |  [optional]
**importSublineRegistrationsForPurchaseOrder** | **Boolean** |  |  [optional]
**importPurchaseOrderPriceFromAdministrationBranch** | **Boolean** |  |  [optional]
**importSublineRegistrationsForSaleOrder** | **Boolean** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="AdministrationBranchTypeEnum"></a>
## Enum: AdministrationBranchTypeEnum
Name | Value
---- | -----
BRANCHNATIVE | &quot;BranchNative&quot;
BRANCHASCII | &quot;BranchAscii&quot;
BRANCHEXTERNAL | &quot;BranchExternal&quot;
HEADQUARTERS | &quot;HeadQuarters&quot;


<a name="SaleOrderExchangeMethodEnum"></a>
## Enum: SaleOrderExchangeMethodEnum
Name | Value
---- | -----
DONOTEXPORT | &quot;DoNotExport&quot;
EXPORTASSALEORDER | &quot;ExportAsSaleOrder&quot;
EXPORTASPURCHASEORDER | &quot;ExportAsPurchaseOrder&quot;


<a name="PurchaseOrderExchangeMethodEnum"></a>
## Enum: PurchaseOrderExchangeMethodEnum
Name | Value
---- | -----
DONOTEXPORT | &quot;DoNotExport&quot;
EXPORTASPURCHASEORDER | &quot;ExportAsPurchaseOrder&quot;
EXPORTASSALEORDER | &quot;ExportAsSaleOrder&quot;



