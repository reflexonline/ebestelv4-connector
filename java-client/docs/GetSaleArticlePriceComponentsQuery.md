
# GetSaleArticlePriceComponentsQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**saleArticleId** | **Integer** |  |  [optional]
**customerId** | **Integer** |  |  [optional]
**date** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]



