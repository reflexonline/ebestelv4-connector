
# LotDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lotId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**surchargePercentage** | **Float** |  |  [optional]
**countryCodeOfButchering** | **String** |  |  [optional]
**countryCodeOfCutting** | **String** |  |  [optional]
**countryCodeOfBirth** | **String** |  |  [optional]
**countryCodeOfFattening** | **String** |  |  [optional]
**chickenTraceIdentifier** | **Integer** |  |  [optional]
**poultryFattener** | **Integer** |  |  [optional]
**bestBeforeDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**dateOfButchering** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**dateFirstStorage** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**isBlocked** | **Boolean** |  |  [optional]
**dateCaptured** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**captureRegion** | **Integer** |  |  [optional]
**captureRegionSector** | **String** |  |  [optional]
**shipNumber** | **String** |  |  [optional]
**captureRegionIso** | **String** |  |  [optional]
**abattoirEegIdentifierAlphaNumeric** | **String** |  |  [optional]
**cuttingPlantEegIdentifierAlphaNumeric** | **String** |  |  [optional]
**lotDefinition** | **Integer** |  |  [optional]
**supplier** | **Integer** |  |  [optional]
**gdb** | **String** |  |  [optional]
**externalLotNumber** | **String** |  |  [optional]
**butcheredFedisIdentifier** | **Integer** |  |  [optional]
**cuttingFedisIdentifier** | **Integer** |  |  [optional]
**dateFrozen** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**cattleAgeCategory** | [**CattleAgeCategoryEnum**](#CattleAgeCategoryEnum) |  |  [optional]
**ftraceReceived** | **Integer** |  |  [optional]
**ftraceSend** | **Integer** |  |  [optional]
**keptIn** | **Integer** |  |  [optional]
**keptInEUCountry** | **String** |  |  [optional]
**fishingGear** | **Integer** |  |  [optional]
**cuttingDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**productionDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**packingDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="CattleAgeCategoryEnum"></a>
## Enum: CattleAgeCategoryEnum
Name | Value
---- | -----
UNKNOWN | &quot;Unknown&quot;
V | &quot;V&quot;
Z | &quot;Z&quot;
B | &quot;B&quot;



