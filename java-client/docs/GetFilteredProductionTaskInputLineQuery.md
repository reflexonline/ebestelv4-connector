
# GetFilteredProductionTaskInputLineQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productionTaskIdMinValue** | **Integer** |  |  [optional]
**productionTaskIdMaxValue** | **Integer** |  |  [optional]
**lineIdMinValue** | **Integer** |  |  [optional]
**lineIdMaxValue** | **Integer** |  |  [optional]
**articleIdMinValue** | **Integer** |  |  [optional]
**articleIdMaxValue** | **Integer** |  |  [optional]
**recipeIdMinValue** | **Integer** |  |  [optional]
**recipeIdMaxValue** | **Integer** |  |  [optional]
**recipeDerivativeIdMinValue** | **Integer** |  |  [optional]
**recipeDerivativeIdMaxValue** | **Integer** |  |  [optional]
**filterGroupIdMinValue** | **Integer** |  |  [optional]
**filterGroupIdMaxValue** | **Integer** |  |  [optional]
**replacesLineNumberMinValue** | **Integer** |  |  [optional]
**replacesLineNumberMaxValue** | **Integer** |  |  [optional]



