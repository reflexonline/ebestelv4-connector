# StockHistoryApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stockHistoryExistsStockHistory**](StockHistoryApi.md#stockHistoryExistsStockHistory) | **GET** /StockHistory/exists/{stockLocationId}/{purchaseArticleId}/{date}/{volgNumber} | 
[**stockHistoryGetStockHistories**](StockHistoryApi.md#stockHistoryGetStockHistories) | **POST** /StockHistory/search | 
[**stockHistoryGetStockHistory**](StockHistoryApi.md#stockHistoryGetStockHistory) | **GET** /StockHistory/{stockLocationId}/{purchaseArticleId}/{date}/{volgNumber} | 


<a name="stockHistoryExistsStockHistory"></a>
# **stockHistoryExistsStockHistory**
> Object stockHistoryExistsStockHistory(stockLocationId, purchaseArticleId, date, volgNumber)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockHistoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockHistoryApi apiInstance = new StockHistoryApi();
Integer stockLocationId = 56; // Integer | 
Integer purchaseArticleId = 56; // Integer | 
OffsetDateTime date = OffsetDateTime.now(); // OffsetDateTime | 
Integer volgNumber = 56; // Integer | 
try {
    Object result = apiInstance.stockHistoryExistsStockHistory(stockLocationId, purchaseArticleId, date, volgNumber);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockHistoryApi#stockHistoryExistsStockHistory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stockLocationId** | **Integer**|  |
 **purchaseArticleId** | **Integer**|  |
 **date** | **OffsetDateTime**|  |
 **volgNumber** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="stockHistoryGetStockHistories"></a>
# **stockHistoryGetStockHistories**
> Object stockHistoryGetStockHistories(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockHistoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockHistoryApi apiInstance = new StockHistoryApi();
GetFilteredStockHistoryQuery query = new GetFilteredStockHistoryQuery(); // GetFilteredStockHistoryQuery | 
try {
    Object result = apiInstance.stockHistoryGetStockHistories(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockHistoryApi#stockHistoryGetStockHistories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredStockHistoryQuery**](GetFilteredStockHistoryQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="stockHistoryGetStockHistory"></a>
# **stockHistoryGetStockHistory**
> Object stockHistoryGetStockHistory(stockLocationId, purchaseArticleId, date, volgNumber)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StockHistoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

StockHistoryApi apiInstance = new StockHistoryApi();
Integer stockLocationId = 56; // Integer | 
Integer purchaseArticleId = 56; // Integer | 
OffsetDateTime date = OffsetDateTime.now(); // OffsetDateTime | 
Integer volgNumber = 56; // Integer | 
try {
    Object result = apiInstance.stockHistoryGetStockHistory(stockLocationId, purchaseArticleId, date, volgNumber);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StockHistoryApi#stockHistoryGetStockHistory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stockLocationId** | **Integer**|  |
 **purchaseArticleId** | **Integer**|  |
 **date** | **OffsetDateTime**|  |
 **volgNumber** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

