
# GetFilteredRecipeGroupQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipeGroupIdMinValue** | **Integer** |  |  [optional]
**recipeGroupIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



