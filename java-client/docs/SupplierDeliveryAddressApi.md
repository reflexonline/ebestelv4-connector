# SupplierDeliveryAddressApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**supplierDeliveryAddressExistsSupplierDeliveryAddress**](SupplierDeliveryAddressApi.md#supplierDeliveryAddressExistsSupplierDeliveryAddress) | **GET** /SupplierDeliveryAddress/exists/{supplierId} | 
[**supplierDeliveryAddressGetSupplierDeliveryAddress**](SupplierDeliveryAddressApi.md#supplierDeliveryAddressGetSupplierDeliveryAddress) | **GET** /SupplierDeliveryAddress/{supplierId} | 
[**supplierDeliveryAddressGetSupplierDeliveryAddresses**](SupplierDeliveryAddressApi.md#supplierDeliveryAddressGetSupplierDeliveryAddresses) | **POST** /SupplierDeliveryAddress/search | 


<a name="supplierDeliveryAddressExistsSupplierDeliveryAddress"></a>
# **supplierDeliveryAddressExistsSupplierDeliveryAddress**
> Object supplierDeliveryAddressExistsSupplierDeliveryAddress(supplierId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierDeliveryAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierDeliveryAddressApi apiInstance = new SupplierDeliveryAddressApi();
Integer supplierId = 56; // Integer | 
try {
    Object result = apiInstance.supplierDeliveryAddressExistsSupplierDeliveryAddress(supplierId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierDeliveryAddressApi#supplierDeliveryAddressExistsSupplierDeliveryAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="supplierDeliveryAddressGetSupplierDeliveryAddress"></a>
# **supplierDeliveryAddressGetSupplierDeliveryAddress**
> Object supplierDeliveryAddressGetSupplierDeliveryAddress(supplierId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierDeliveryAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierDeliveryAddressApi apiInstance = new SupplierDeliveryAddressApi();
Integer supplierId = 56; // Integer | 
try {
    Object result = apiInstance.supplierDeliveryAddressGetSupplierDeliveryAddress(supplierId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierDeliveryAddressApi#supplierDeliveryAddressGetSupplierDeliveryAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="supplierDeliveryAddressGetSupplierDeliveryAddresses"></a>
# **supplierDeliveryAddressGetSupplierDeliveryAddresses**
> Object supplierDeliveryAddressGetSupplierDeliveryAddresses(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierDeliveryAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierDeliveryAddressApi apiInstance = new SupplierDeliveryAddressApi();
GetFilteredSupplierDeliveryAddressQuery query = new GetFilteredSupplierDeliveryAddressQuery(); // GetFilteredSupplierDeliveryAddressQuery | 
try {
    Object result = apiInstance.supplierDeliveryAddressGetSupplierDeliveryAddresses(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierDeliveryAddressApi#supplierDeliveryAddressGetSupplierDeliveryAddresses");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSupplierDeliveryAddressQuery**](GetFilteredSupplierDeliveryAddressQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

