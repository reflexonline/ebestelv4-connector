# ReflexUserApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**reflexUserChangePasswordReflexUser**](ReflexUserApi.md#reflexUserChangePasswordReflexUser) | **PUT** /ReflexUser/{number}/ChangePassword | 
[**reflexUserCreateReflexUser**](ReflexUserApi.md#reflexUserCreateReflexUser) | **POST** /ReflexUser/CreateReflexUser | 
[**reflexUserCreateReflexUser_0**](ReflexUserApi.md#reflexUserCreateReflexUser_0) | **POST** /ReflexUser | 
[**reflexUserDeleteReflexUser**](ReflexUserApi.md#reflexUserDeleteReflexUser) | **DELETE** /ReflexUser/{number} | 
[**reflexUserExistsReflexUser**](ReflexUserApi.md#reflexUserExistsReflexUser) | **GET** /ReflexUser/exists/{number} | 
[**reflexUserGetReflexUser**](ReflexUserApi.md#reflexUserGetReflexUser) | **GET** /ReflexUser/{number} | 
[**reflexUserGetReflexUsers**](ReflexUserApi.md#reflexUserGetReflexUsers) | **POST** /ReflexUser/search | 
[**reflexUserResetPasswordReflexUser**](ReflexUserApi.md#reflexUserResetPasswordReflexUser) | **POST** /ReflexUser/{number}/ResetPassword | 
[**reflexUserUpdateReflexUser**](ReflexUserApi.md#reflexUserUpdateReflexUser) | **PUT** /ReflexUser/{number} | 


<a name="reflexUserChangePasswordReflexUser"></a>
# **reflexUserChangePasswordReflexUser**
> Object reflexUserChangePasswordReflexUser(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ReflexUserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ReflexUserApi apiInstance = new ReflexUserApi();
Integer number = 56; // Integer | 
ToChangePasswordDto dto = new ToChangePasswordDto(); // ToChangePasswordDto | 
try {
    Object result = apiInstance.reflexUserChangePasswordReflexUser(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReflexUserApi#reflexUserChangePasswordReflexUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**ToChangePasswordDto**](ToChangePasswordDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="reflexUserCreateReflexUser"></a>
# **reflexUserCreateReflexUser**
> Object reflexUserCreateReflexUser(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ReflexUserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ReflexUserApi apiInstance = new ReflexUserApi();
FromCreateReflexUserDto dto = new FromCreateReflexUserDto(); // FromCreateReflexUserDto | 
try {
    Object result = apiInstance.reflexUserCreateReflexUser(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReflexUserApi#reflexUserCreateReflexUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**FromCreateReflexUserDto**](FromCreateReflexUserDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="reflexUserCreateReflexUser_0"></a>
# **reflexUserCreateReflexUser_0**
> Object reflexUserCreateReflexUser_0(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ReflexUserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ReflexUserApi apiInstance = new ReflexUserApi();
ReflexUserDto dto = new ReflexUserDto(); // ReflexUserDto | 
try {
    Object result = apiInstance.reflexUserCreateReflexUser_0(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReflexUserApi#reflexUserCreateReflexUser_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ReflexUserDto**](ReflexUserDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="reflexUserDeleteReflexUser"></a>
# **reflexUserDeleteReflexUser**
> Object reflexUserDeleteReflexUser(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ReflexUserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ReflexUserApi apiInstance = new ReflexUserApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.reflexUserDeleteReflexUser(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReflexUserApi#reflexUserDeleteReflexUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="reflexUserExistsReflexUser"></a>
# **reflexUserExistsReflexUser**
> Object reflexUserExistsReflexUser(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ReflexUserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ReflexUserApi apiInstance = new ReflexUserApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.reflexUserExistsReflexUser(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReflexUserApi#reflexUserExistsReflexUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="reflexUserGetReflexUser"></a>
# **reflexUserGetReflexUser**
> Object reflexUserGetReflexUser(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ReflexUserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ReflexUserApi apiInstance = new ReflexUserApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.reflexUserGetReflexUser(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReflexUserApi#reflexUserGetReflexUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="reflexUserGetReflexUsers"></a>
# **reflexUserGetReflexUsers**
> Object reflexUserGetReflexUsers(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ReflexUserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ReflexUserApi apiInstance = new ReflexUserApi();
GetFilteredReflexUserQuery query = new GetFilteredReflexUserQuery(); // GetFilteredReflexUserQuery | 
try {
    Object result = apiInstance.reflexUserGetReflexUsers(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReflexUserApi#reflexUserGetReflexUsers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredReflexUserQuery**](GetFilteredReflexUserQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="reflexUserResetPasswordReflexUser"></a>
# **reflexUserResetPasswordReflexUser**
> Object reflexUserResetPasswordReflexUser(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ReflexUserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ReflexUserApi apiInstance = new ReflexUserApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.reflexUserResetPasswordReflexUser(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReflexUserApi#reflexUserResetPasswordReflexUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="reflexUserUpdateReflexUser"></a>
# **reflexUserUpdateReflexUser**
> Object reflexUserUpdateReflexUser(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ReflexUserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ReflexUserApi apiInstance = new ReflexUserApi();
Integer number = 56; // Integer | 
ReflexUserDto dto = new ReflexUserDto(); // ReflexUserDto | 
try {
    Object result = apiInstance.reflexUserUpdateReflexUser(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReflexUserApi#reflexUserUpdateReflexUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**ReflexUserDto**](ReflexUserDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

