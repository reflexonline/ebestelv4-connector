
# SubstituteGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**substituteGroupId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**stockFilterMethod** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



