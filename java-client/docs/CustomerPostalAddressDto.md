
# CustomerPostalAddressDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **Integer** |  | 
**searchName** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**searchName2** | **String** |  |  [optional]
**addressLine1** | **String** |  |  [optional]
**addressLine2** | **String** |  |  [optional]
**postalCode** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**phoneNumber** | **String** |  |  [optional]
**faxNumber** | **String** |  |  [optional]
**contactPerson** | **String** |  |  [optional]
**country** | **String** |  |  [optional]
**phoneNumberContactPerson** | **String** |  |  [optional]
**isoCountryCode** | **String** |  |  [optional]
**deliveryMethodId** | **Integer** |  |  [optional]
**longName** | **String** |  |  [optional]
**bestBeforeDateDeterminationMethod** | [**BestBeforeDateDeterminationMethodEnum**](#BestBeforeDateDeterminationMethodEnum) |  |  [optional]
**bestBeforeDateCheckUpMethod** | [**BestBeforeDateCheckUpMethodEnum**](#BestBeforeDateCheckUpMethodEnum) |  |  [optional]
**checkOutstanding** | **Integer** |  |  [optional]
**cmrArtikelPartijTotaliseren** | **Boolean** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="BestBeforeDateDeterminationMethodEnum"></a>
## Enum: BestBeforeDateDeterminationMethodEnum
Name | Value
---- | -----
DEFAULT | &quot;Default&quot;
NONE | &quot;None&quot;
DATEREGISTRATION | &quot;DateRegistration&quot;
DATELOADING | &quot;DateLoading&quot;
DATEDELIVERY | &quot;DateDelivery&quot;
REGISTEREDBATCH | &quot;RegisteredBatch&quot;


<a name="BestBeforeDateCheckUpMethodEnum"></a>
## Enum: BestBeforeDateCheckUpMethodEnum
Name | Value
---- | -----
DEFAULT | &quot;Default&quot;
NONE | &quot;None&quot;
ASKUSER | &quot;AskUser&quot;
WARRANTYDATE | &quot;WarrantyDate&quot;
ASKFORDATEWARRANTYINCLUDED | &quot;AskForDateWarrantyIncluded&quot;
ASKFORDATEHOLDPERARTICLE | &quot;AskForDateHoldPerArticle&quot;



