# PaymentMethodApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentMethodCreatePaymentMethod**](PaymentMethodApi.md#paymentMethodCreatePaymentMethod) | **POST** /PaymentMethod | 
[**paymentMethodDeletePaymentMethod**](PaymentMethodApi.md#paymentMethodDeletePaymentMethod) | **DELETE** /PaymentMethod/{paymentMethodId} | 
[**paymentMethodExistsPaymentMethod**](PaymentMethodApi.md#paymentMethodExistsPaymentMethod) | **GET** /PaymentMethod/exists/{paymentMethodId} | 
[**paymentMethodGetPaymentMethod**](PaymentMethodApi.md#paymentMethodGetPaymentMethod) | **GET** /PaymentMethod/{paymentMethodId} | 
[**paymentMethodGetPaymentMethods**](PaymentMethodApi.md#paymentMethodGetPaymentMethods) | **POST** /PaymentMethod/search | 
[**paymentMethodUpdatePaymentMethod**](PaymentMethodApi.md#paymentMethodUpdatePaymentMethod) | **PUT** /PaymentMethod/{paymentMethodId} | 


<a name="paymentMethodCreatePaymentMethod"></a>
# **paymentMethodCreatePaymentMethod**
> Object paymentMethodCreatePaymentMethod(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentMethodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PaymentMethodApi apiInstance = new PaymentMethodApi();
PaymentMethodDto dto = new PaymentMethodDto(); // PaymentMethodDto | 
try {
    Object result = apiInstance.paymentMethodCreatePaymentMethod(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentMethodApi#paymentMethodCreatePaymentMethod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PaymentMethodDto**](PaymentMethodDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="paymentMethodDeletePaymentMethod"></a>
# **paymentMethodDeletePaymentMethod**
> Object paymentMethodDeletePaymentMethod(paymentMethodId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentMethodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PaymentMethodApi apiInstance = new PaymentMethodApi();
Integer paymentMethodId = 56; // Integer | 
try {
    Object result = apiInstance.paymentMethodDeletePaymentMethod(paymentMethodId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentMethodApi#paymentMethodDeletePaymentMethod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **paymentMethodId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="paymentMethodExistsPaymentMethod"></a>
# **paymentMethodExistsPaymentMethod**
> Object paymentMethodExistsPaymentMethod(paymentMethodId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentMethodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PaymentMethodApi apiInstance = new PaymentMethodApi();
Integer paymentMethodId = 56; // Integer | 
try {
    Object result = apiInstance.paymentMethodExistsPaymentMethod(paymentMethodId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentMethodApi#paymentMethodExistsPaymentMethod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **paymentMethodId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="paymentMethodGetPaymentMethod"></a>
# **paymentMethodGetPaymentMethod**
> Object paymentMethodGetPaymentMethod(paymentMethodId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentMethodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PaymentMethodApi apiInstance = new PaymentMethodApi();
Integer paymentMethodId = 56; // Integer | 
try {
    Object result = apiInstance.paymentMethodGetPaymentMethod(paymentMethodId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentMethodApi#paymentMethodGetPaymentMethod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **paymentMethodId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="paymentMethodGetPaymentMethods"></a>
# **paymentMethodGetPaymentMethods**
> Object paymentMethodGetPaymentMethods(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentMethodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PaymentMethodApi apiInstance = new PaymentMethodApi();
GetFilteredPaymentMethodQuery query = new GetFilteredPaymentMethodQuery(); // GetFilteredPaymentMethodQuery | 
try {
    Object result = apiInstance.paymentMethodGetPaymentMethods(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentMethodApi#paymentMethodGetPaymentMethods");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPaymentMethodQuery**](GetFilteredPaymentMethodQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="paymentMethodUpdatePaymentMethod"></a>
# **paymentMethodUpdatePaymentMethod**
> Object paymentMethodUpdatePaymentMethod(paymentMethodId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentMethodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PaymentMethodApi apiInstance = new PaymentMethodApi();
Integer paymentMethodId = 56; // Integer | 
PaymentMethodDto dto = new PaymentMethodDto(); // PaymentMethodDto | 
try {
    Object result = apiInstance.paymentMethodUpdatePaymentMethod(paymentMethodId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentMethodApi#paymentMethodUpdatePaymentMethod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **paymentMethodId** | **Integer**|  |
 **dto** | [**PaymentMethodDto**](PaymentMethodDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

