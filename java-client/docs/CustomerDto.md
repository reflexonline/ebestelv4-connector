
# CustomerDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerDeliveryAddress** | [**CustomerDeliveryAddressDto**](CustomerDeliveryAddressDto.md) |  |  [optional]
**customerPostalAddress** | [**CustomerPostalAddressDto**](CustomerPostalAddressDto.md) |  |  [optional]
**tag** | [**List&lt;LineTagNameDto&gt;**](LineTagNameDto.md) |  |  [optional]
**customerId** | **Integer** |  | 
**postalAddressName** | **String** |  |  [optional]
**vatNumber** | **String** |  |  [optional]
**invoiceCustomer** | **Integer** |  |  [optional]
**creditLimit** | **Double** |  |  [optional]
**priceListId** | **Integer** |  |  [optional]
**discountPercentage** | **Float** |  |  [optional]
**creditRestrictionPercentage** | **Float** |  |  [optional]
**vatInvoice** | **String** |  |  [optional]
**currencyCode** | **Integer** |  |  [optional]
**defaultRoute** | **Integer** |  |  [optional]
**hasArticleSelection** | **Boolean** |  |  [optional]
**calculatePriceOnPrintingInvoice** | [**CalculatePriceOnPrintingInvoiceEnum**](#CalculatePriceOnPrintingInvoiceEnum) |  |  [optional]
**searchKey** | **String** |  |  [optional]
**packingSlipType** | **Integer** |  |  [optional]
**korteFaktuur** | [**KorteFaktuurEnum**](#KorteFaktuurEnum) |  |  [optional]
**exporteer** | **Boolean** |  |  [optional]
**orderBevestiging** | **String** |  |  [optional]
**includeInSalesRegister** | **Boolean** |  |  [optional]
**languageId** | **Integer** |  |  [optional]
**excludeFromGeneralHaccpQuestions** | **Boolean** |  |  [optional]
**freeField1** | **String** |  |  [optional]
**freeField2** | **String** |  |  [optional]
**freeField3** | **String** |  |  [optional]
**freeField4** | **String** |  |  [optional]
**freeField5** | **String** |  |  [optional]
**proformaFaktuur** | [**ProformaFaktuurEnum**](#ProformaFaktuurEnum) |  |  [optional]
**stafVan1** | **Integer** |  |  [optional]
**stafVan2** | **Integer** |  |  [optional]
**stafVan3** | **Integer** |  |  [optional]
**stafVan4** | **Integer** |  |  [optional]
**stafVan5** | **Integer** |  |  [optional]
**stafVan6** | **Integer** |  |  [optional]
**stafTot1** | **Integer** |  |  [optional]
**stafTot2** | **Integer** |  |  [optional]
**stafTot3** | **Integer** |  |  [optional]
**stafTot4** | **Integer** |  |  [optional]
**stafTot5** | **Integer** |  |  [optional]
**stafTot6** | **Integer** |  |  [optional]
**stafKort1** | **Float** |  |  [optional]
**stafKort2** | **Float** |  |  [optional]
**stafKort3** | **Float** |  |  [optional]
**stafKort4** | **Float** |  |  [optional]
**stafKort5** | **Float** |  |  [optional]
**stafKort6** | **Float** |  |  [optional]
**bestelpatroon** | [**BestelpatroonEnum**](#BestelpatroonEnum) |  |  [optional]
**acceptGiro** | **Boolean** |  |  [optional]
**bankNumber** | **String** |  |  [optional]
**giroNumber** | **String** |  |  [optional]
**vrachtbrief** | [**VrachtbriefEnum**](#VrachtbriefEnum) |  |  [optional]
**cbsCountryHerkBest** | **Integer** |  |  [optional]
**saleInvoiceGroupHeaderId** | **Integer** |  |  [optional]
**calculatePriceOnPrintingPackingSlip** | [**CalculatePriceOnPrintingPackingSlipEnum**](#CalculatePriceOnPrintingPackingSlipEnum) |  |  [optional]
**eanAdresCode** | **String** |  |  [optional]
**soortwaarschuwing** | **Integer** |  |  [optional]
**warning** | **String** |  |  [optional]
**eanAfleverAdresCode** | **String** |  |  [optional]
**eanFaktuurAdresCode** | **String** |  |  [optional]
**askForBestBefore** | **Integer** |  |  [optional]
**prijsBepaling** | [**PrijsBepalingEnum**](#PrijsBepalingEnum) |  |  [optional]
**tariefRoutePerKilo** | **Float** |  |  [optional]
**calculateIvkVlam** | **Integer** |  |  [optional]
**staffelSoort** | [**StaffelSoortEnum**](#StaffelSoortEnum) |  |  [optional]
**kvkPlaats** | **String** |  |  [optional]
**kvkNumber** | **Integer** |  |  [optional]
**mailing** | **Integer** |  |  [optional]
**isBlocked** | **Boolean** |  |  [optional]
**representative** | **Integer** |  |  [optional]
**organization** | **Integer** |  |  [optional]
**marketSector** | **Integer** |  |  [optional]
**email** | **String** |  |  [optional]
**website** | **String** |  |  [optional]
**caller** | **Integer** |  |  [optional]
**internGrootboekNumber** | **Integer** |  |  [optional]
**numberOfInvoiceCopies** | **Integer** |  |  [optional]
**deliveryMethod** | **Integer** |  |  [optional]
**intern** | **Integer** |  |  [optional]
**minimaalOrderBedr** | **Double** |  |  [optional]
**bonusParticipation** | [**BonusParticipationEnum**](#BonusParticipationEnum) |  |  [optional]
**creditHoldType** | [**CreditHoldTypeEnum**](#CreditHoldTypeEnum) |  |  [optional]
**onCreditHoldSince** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**discountGroup** | **Integer** |  |  [optional]
**numberOfPackingSlipCopies** | **Integer** |  |  [optional]
**eegNumber** | **String** |  |  [optional]
**loadAddressId** | **Integer** |  |  [optional]
**transporter** | **Integer** |  |  [optional]
**werkelijkIsBesteld** | **Boolean** |  |  [optional]
**bestelpatroonSort** | **Integer** |  |  [optional]
**bestelpatroonOptie** | **Integer** |  |  [optional]
**multiPriceListMethod** | [**MultiPriceListMethodEnum**](#MultiPriceListMethodEnum) |  |  [optional]
**ecomKortingsGroep** | **Integer** |  |  [optional]
**inverscoRoute** | **String** |  |  [optional]
**useFixedInvoicePrice** | **Boolean** |  |  [optional]
**stockLocationFrom** | **Integer** |  |  [optional]
**labelingMachinePrijslijPieces** | **Integer** |  |  [optional]
**geenEmbalRegPakbCmr** | **Boolean** |  |  [optional]
**pakbonPerLevering** | **Boolean** |  |  [optional]
**bestelpatroondeb** | **Integer** |  |  [optional]
**includeInAnpopsparen** | **Boolean** |  |  [optional]
**isBlockedForSpecialOffer** | **Boolean** |  |  [optional]
**betaalWijze** | **String** |  |  [optional]
**eanEindbestemming** | **String** |  |  [optional]
**isEdiPackingSlipCustomer** | **Boolean** |  |  [optional]
**isEdiInvoiceCustomer** | **Boolean** |  |  [optional]
**subOrganization** | **Integer** |  |  [optional]
**proformaExtraText** | **String** |  |  [optional]
**maxPocketMut** | **Double** |  |  [optional]
**contPocketNeg** | **Integer** |  |  [optional]
**owhNaarFiliaal** | **Integer** |  |  [optional]
**owhSoortLevering** | **Integer** |  |  [optional]
**showDiscountProforma** | **Integer** |  |  [optional]
**cmrOpmaakNumber** | **Integer** |  |  [optional]
**useDiscountOnServiceTax** | **Boolean** |  |  [optional]
**bankIdentifierCode** | **String** |  |  [optional]
**internationalBankAccountNumber** | **String** |  |  [optional]
**dispPckTaxOnInvoice** | **Integer** |  |  [optional]
**ordConfPrintMoment** | **Integer** |  |  [optional]
**printTransportDoc** | **Integer** |  |  [optional]
**transporter2** | **Integer** |  |  [optional]
**multiBranch** | **Integer** |  |  [optional]
**packingInvoiceCustomer** | **Integer** |  |  [optional]
**devPackingProfInv** | [**DevPackingProfInvEnum**](#DevPackingProfInvEnum) |  |  [optional]
**internalCostCenter** | **Integer** |  |  [optional]
**sortOrdConfLinesBy** | **Integer** |  |  [optional]
**summarizePackingArticlesOnPackingSlip** | **Integer** |  |  [optional]
**bestBeforeDateWarrantyFactor** | **Double** |  |  [optional]
**printSavingActionsOnInvoice** | **Boolean** |  |  [optional]
**returnMethod** | **Integer** |  |  [optional]
**saleInvoiceStatus** | **Integer** |  |  [optional]
**owhDeliveryDayOffset** | **Integer** |  |  [optional]
**exportGroupId** | **Integer** |  |  [optional]
**stockLocationTo** | **Integer** |  |  [optional]
**useOrderCheckFile** | **Boolean** |  |  [optional]
**owhSaturdayDelivery** | **Integer** |  |  [optional]
**owhSundayDelivery** | **Integer** |  |  [optional]
**dock** | **Integer** |  |  [optional]
**internationalBankAccountNumber2** | **String** |  |  [optional]
**bankIdentifierCode2** | **String** |  |  [optional]
**useFTrace** | **Boolean** |  |  [optional]
**packingSlipMethod** | **Integer** |  |  [optional]
**owhMondayDelivery** | **Integer** |  |  [optional]
**owhTuesdayDelivery** | **Integer** |  |  [optional]
**owhWednesdayDelivery** | **Integer** |  |  [optional]
**owhThursdayDelivery** | **Integer** |  |  [optional]
**owhFridayDelivery** | **Integer** |  |  [optional]
**allowPartialDelivery** | **Boolean** |  |  [optional]
**makeInvoices** | **Boolean** |  |  [optional]
**orderAddrOnInvoice** | **Integer** |  |  [optional]
**articleLabelNumber** | **Integer** |  |  [optional]
**crateLabelNumber** | **Integer** |  |  [optional]
**palletLabelNumber** | **Integer** |  |  [optional]
**cashDiscountPerc** | **Float** |  |  [optional]
**cashDiscountDays** | **Integer** |  |  [optional]
**showBatchOnInvoice** | **Boolean** |  |  [optional]
**bestBeforeDateDeterminationMethod** | [**BestBeforeDateDeterminationMethodEnum**](#BestBeforeDateDeterminationMethodEnum) |  |  [optional]
**bestBeforeDateCheckUpMethod** | [**BestBeforeDateCheckUpMethodEnum**](#BestBeforeDateCheckUpMethodEnum) |  |  [optional]
**checkOutstanding** | **Integer** |  |  [optional]
**cmrArtikelPartijTotaliseren** | **Boolean** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="CalculatePriceOnPrintingInvoiceEnum"></a>
## Enum: CalculatePriceOnPrintingInvoiceEnum
Name | Value
---- | -----
NO | &quot;No&quot;
ONLYSPECIALOFFER | &quot;OnlySpecialOffer&quot;
YES | &quot;Yes&quot;
USETIPOPSURCHARGE | &quot;UseTipOpSurcharge&quot;


<a name="KorteFaktuurEnum"></a>
## Enum: KorteFaktuurEnum
Name | Value
---- | -----
NORMAL | &quot;Normal&quot;
SHORT | &quot;Short&quot;
PERPACKINGSLIP | &quot;PerPackingslip&quot;
ONELINEPERPACKINGSLIP | &quot;OneLinePerPackingslip&quot;
MADIWODOFIXEDPRICE | &quot;MaDiWoDoFixedPrice&quot;
ORDERBYPACKINGSLIPPRODUCTIONGROUP | &quot;OrderByPackingslipProductionGroup&quot;
ORDERBYPACKINGSLIPARTICLE | &quot;OrderByPackingslipArticle&quot;
PERPACKINGSLIPORDERBYPRODUCTIONGROUP | &quot;PerPackingslipOrderByProductionGroup&quot;
PERPACKINGSLIPORDERBYARTICLE | &quot;PerPackingslipOrderByArticle&quot;
MADIWODODAYPRICE | &quot;MaDiWoDoDayPrice&quot;


<a name="ProformaFaktuurEnum"></a>
## Enum: ProformaFaktuurEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
SHORT | &quot;SHort&quot;
PERLINE | &quot;PerLine&quot;


<a name="BestelpatroonEnum"></a>
## Enum: BestelpatroonEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
ON | &quot;On&quot;
AUTOMATIC | &quot;Automatic&quot;
SALEORDERPATTERNCUSTOMER | &quot;SaleOrderPatternCustomer&quot;


<a name="VrachtbriefEnum"></a>
## Enum: VrachtbriefEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
INTERNAL | &quot;Internal&quot;
EXTERN | &quot;Extern&quot;


<a name="CalculatePriceOnPrintingPackingSlipEnum"></a>
## Enum: CalculatePriceOnPrintingPackingSlipEnum
Name | Value
---- | -----
NO | &quot;No&quot;
YES | &quot;Yes&quot;
USETIPOPSURCHARGE | &quot;UseTipOpSurcharge&quot;


<a name="PrijsBepalingEnum"></a>
## Enum: PrijsBepalingEnum
Name | Value
---- | -----
DISCOUNTONSALEPRICE | &quot;DiscountOnSalePrice&quot;
RAISEONPURCHASEPRICE | &quot;RaiseOnPurchasePrice&quot;
RAISEONSETTLEPRICE | &quot;RaiseOnSettlePrice&quot;


<a name="StaffelSoortEnum"></a>
## Enum: StaffelSoortEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
PERARTICLEBASEDONTOTALPACKINGSLIPWEIGHT | &quot;PerArticleBasedOnTotalPackingslipWeight&quot;
ONTOTALINVOICEAMOUNT | &quot;OnTotalInvoiceAmount&quot;


<a name="BonusParticipationEnum"></a>
## Enum: BonusParticipationEnum
Name | Value
---- | -----
EXCLUDEDFROMBONUS | &quot;ExcludedFromBonus&quot;
INCLUDEDINBONUS | &quot;IncludedInBonus&quot;
INCLUDEDINBONUSWITHSPECIALOFFER | &quot;IncludedInBonusWithSpecialOffer&quot;


<a name="CreditHoldTypeEnum"></a>
## Enum: CreditHoldTypeEnum
Name | Value
---- | -----
NO | &quot;No&quot;
YES | &quot;Yes&quot;
NO_PERSISTENT | &quot;No_Persistent&quot;
YES_PERSISTENT | &quot;Yes_Persistent&quot;


<a name="MultiPriceListMethodEnum"></a>
## Enum: MultiPriceListMethodEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
FIRST | &quot;First&quot;
LOWEST | &quot;Lowest&quot;
FIRSTNONBLOCKED | &quot;FirstNonBlocked&quot;
LOWESTNONBLOCKED | &quot;LowestNonBlocked&quot;


<a name="DevPackingProfInvEnum"></a>
## Enum: DevPackingProfInvEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
SHORT | &quot;SHort&quot;
PERLINE | &quot;PerLine&quot;


<a name="BestBeforeDateDeterminationMethodEnum"></a>
## Enum: BestBeforeDateDeterminationMethodEnum
Name | Value
---- | -----
DEFAULT | &quot;Default&quot;
NONE | &quot;None&quot;
DATEREGISTRATION | &quot;DateRegistration&quot;
DATELOADING | &quot;DateLoading&quot;
DATEDELIVERY | &quot;DateDelivery&quot;
REGISTEREDBATCH | &quot;RegisteredBatch&quot;


<a name="BestBeforeDateCheckUpMethodEnum"></a>
## Enum: BestBeforeDateCheckUpMethodEnum
Name | Value
---- | -----
DEFAULT | &quot;Default&quot;
NONE | &quot;None&quot;
ASKUSER | &quot;AskUser&quot;
WARRANTYDATE | &quot;WarrantyDate&quot;
ASKFORDATEWARRANTYINCLUDED | &quot;AskForDateWarrantyIncluded&quot;
ASKFORDATEHOLDPERARTICLE | &quot;AskForDateHoldPerArticle&quot;



