
# GetFilteredAdministrationBranchQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**dwhDefaultLanguageIdMinValue** | **Integer** |  |  [optional]
**dwhDefaultLanguageIdMaxValue** | **Integer** |  |  [optional]



