# DiscountGroupLineApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**discountGroupLineCreateDiscountGroupLine**](DiscountGroupLineApi.md#discountGroupLineCreateDiscountGroupLine) | **POST** /DiscountGroupLine | 
[**discountGroupLineDeleteDiscountGroupLine**](DiscountGroupLineApi.md#discountGroupLineDeleteDiscountGroupLine) | **DELETE** /DiscountGroupLine/{discountGroupId}/{discountEntityId}/{discountEntityType} | 
[**discountGroupLineExistsDiscountGroupLine**](DiscountGroupLineApi.md#discountGroupLineExistsDiscountGroupLine) | **GET** /DiscountGroupLine/exists/{discountGroupId}/{discountEntityId}/{discountEntityType} | 
[**discountGroupLineGetDiscountGroupLine**](DiscountGroupLineApi.md#discountGroupLineGetDiscountGroupLine) | **GET** /DiscountGroupLine/{discountGroupId}/{discountEntityId}/{discountEntityType} | 
[**discountGroupLineGetDiscountGroupLines**](DiscountGroupLineApi.md#discountGroupLineGetDiscountGroupLines) | **POST** /DiscountGroupLine/search | 
[**discountGroupLineUpdateDiscountGroupLine**](DiscountGroupLineApi.md#discountGroupLineUpdateDiscountGroupLine) | **PUT** /DiscountGroupLine/{discountGroupId}/{discountEntityId}/{discountEntityType} | 


<a name="discountGroupLineCreateDiscountGroupLine"></a>
# **discountGroupLineCreateDiscountGroupLine**
> Object discountGroupLineCreateDiscountGroupLine(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DiscountGroupLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DiscountGroupLineApi apiInstance = new DiscountGroupLineApi();
DiscountGroupLineDto dto = new DiscountGroupLineDto(); // DiscountGroupLineDto | 
try {
    Object result = apiInstance.discountGroupLineCreateDiscountGroupLine(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DiscountGroupLineApi#discountGroupLineCreateDiscountGroupLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**DiscountGroupLineDto**](DiscountGroupLineDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="discountGroupLineDeleteDiscountGroupLine"></a>
# **discountGroupLineDeleteDiscountGroupLine**
> Object discountGroupLineDeleteDiscountGroupLine(discountGroupId, discountEntityId, discountEntityType)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DiscountGroupLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DiscountGroupLineApi apiInstance = new DiscountGroupLineApi();
Integer discountGroupId = 56; // Integer | 
Integer discountEntityId = 56; // Integer | 
String discountEntityType = "discountEntityType_example"; // String | 
try {
    Object result = apiInstance.discountGroupLineDeleteDiscountGroupLine(discountGroupId, discountEntityId, discountEntityType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DiscountGroupLineApi#discountGroupLineDeleteDiscountGroupLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **discountGroupId** | **Integer**|  |
 **discountEntityId** | **Integer**|  |
 **discountEntityType** | **String**|  | [enum: SaleArticleMainGroup, SaleArticleGroup, SaleArticleSubgroup, SaleArticle, FromOrderAmount]

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="discountGroupLineExistsDiscountGroupLine"></a>
# **discountGroupLineExistsDiscountGroupLine**
> Object discountGroupLineExistsDiscountGroupLine(discountGroupId, discountEntityId, discountEntityType)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DiscountGroupLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DiscountGroupLineApi apiInstance = new DiscountGroupLineApi();
Integer discountGroupId = 56; // Integer | 
Integer discountEntityId = 56; // Integer | 
String discountEntityType = "discountEntityType_example"; // String | 
try {
    Object result = apiInstance.discountGroupLineExistsDiscountGroupLine(discountGroupId, discountEntityId, discountEntityType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DiscountGroupLineApi#discountGroupLineExistsDiscountGroupLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **discountGroupId** | **Integer**|  |
 **discountEntityId** | **Integer**|  |
 **discountEntityType** | **String**|  | [enum: SaleArticleMainGroup, SaleArticleGroup, SaleArticleSubgroup, SaleArticle, FromOrderAmount]

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="discountGroupLineGetDiscountGroupLine"></a>
# **discountGroupLineGetDiscountGroupLine**
> Object discountGroupLineGetDiscountGroupLine(discountGroupId, discountEntityId, discountEntityType)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DiscountGroupLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DiscountGroupLineApi apiInstance = new DiscountGroupLineApi();
Integer discountGroupId = 56; // Integer | 
Integer discountEntityId = 56; // Integer | 
String discountEntityType = "discountEntityType_example"; // String | 
try {
    Object result = apiInstance.discountGroupLineGetDiscountGroupLine(discountGroupId, discountEntityId, discountEntityType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DiscountGroupLineApi#discountGroupLineGetDiscountGroupLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **discountGroupId** | **Integer**|  |
 **discountEntityId** | **Integer**|  |
 **discountEntityType** | **String**|  | [enum: SaleArticleMainGroup, SaleArticleGroup, SaleArticleSubgroup, SaleArticle, FromOrderAmount]

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="discountGroupLineGetDiscountGroupLines"></a>
# **discountGroupLineGetDiscountGroupLines**
> Object discountGroupLineGetDiscountGroupLines(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DiscountGroupLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DiscountGroupLineApi apiInstance = new DiscountGroupLineApi();
GetFilteredDiscountGroupLineQuery query = new GetFilteredDiscountGroupLineQuery(); // GetFilteredDiscountGroupLineQuery | 
try {
    Object result = apiInstance.discountGroupLineGetDiscountGroupLines(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DiscountGroupLineApi#discountGroupLineGetDiscountGroupLines");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredDiscountGroupLineQuery**](GetFilteredDiscountGroupLineQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="discountGroupLineUpdateDiscountGroupLine"></a>
# **discountGroupLineUpdateDiscountGroupLine**
> Object discountGroupLineUpdateDiscountGroupLine(discountGroupId, discountEntityId, discountEntityType, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DiscountGroupLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DiscountGroupLineApi apiInstance = new DiscountGroupLineApi();
Integer discountGroupId = 56; // Integer | 
Integer discountEntityId = 56; // Integer | 
String discountEntityType = "discountEntityType_example"; // String | 
DiscountGroupLineDto dto = new DiscountGroupLineDto(); // DiscountGroupLineDto | 
try {
    Object result = apiInstance.discountGroupLineUpdateDiscountGroupLine(discountGroupId, discountEntityId, discountEntityType, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DiscountGroupLineApi#discountGroupLineUpdateDiscountGroupLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **discountGroupId** | **Integer**|  |
 **discountEntityId** | **Integer**|  |
 **discountEntityType** | **String**|  | [enum: SaleArticleMainGroup, SaleArticleGroup, SaleArticleSubgroup, SaleArticle, FromOrderAmount]
 **dto** | [**DiscountGroupLineDto**](DiscountGroupLineDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

