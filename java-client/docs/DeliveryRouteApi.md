# DeliveryRouteApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deliveryRouteCreateDeliveryRoute**](DeliveryRouteApi.md#deliveryRouteCreateDeliveryRoute) | **POST** /DeliveryRoute | 
[**deliveryRouteDeleteDeliveryRoute**](DeliveryRouteApi.md#deliveryRouteDeleteDeliveryRoute) | **DELETE** /DeliveryRoute/{number} | 
[**deliveryRouteExistsDeliveryRoute**](DeliveryRouteApi.md#deliveryRouteExistsDeliveryRoute) | **GET** /DeliveryRoute/exists/{number} | 
[**deliveryRouteGetDeliveryRoute**](DeliveryRouteApi.md#deliveryRouteGetDeliveryRoute) | **GET** /DeliveryRoute/{number} | 
[**deliveryRouteGetDeliveryRoutes**](DeliveryRouteApi.md#deliveryRouteGetDeliveryRoutes) | **POST** /DeliveryRoute/search | 
[**deliveryRouteUpdateDeliveryRoute**](DeliveryRouteApi.md#deliveryRouteUpdateDeliveryRoute) | **PUT** /DeliveryRoute/{number} | 


<a name="deliveryRouteCreateDeliveryRoute"></a>
# **deliveryRouteCreateDeliveryRoute**
> Object deliveryRouteCreateDeliveryRoute(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DeliveryRouteApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DeliveryRouteApi apiInstance = new DeliveryRouteApi();
DeliveryRouteDto dto = new DeliveryRouteDto(); // DeliveryRouteDto | 
try {
    Object result = apiInstance.deliveryRouteCreateDeliveryRoute(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeliveryRouteApi#deliveryRouteCreateDeliveryRoute");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**DeliveryRouteDto**](DeliveryRouteDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="deliveryRouteDeleteDeliveryRoute"></a>
# **deliveryRouteDeleteDeliveryRoute**
> Object deliveryRouteDeleteDeliveryRoute(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DeliveryRouteApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DeliveryRouteApi apiInstance = new DeliveryRouteApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.deliveryRouteDeleteDeliveryRoute(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeliveryRouteApi#deliveryRouteDeleteDeliveryRoute");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="deliveryRouteExistsDeliveryRoute"></a>
# **deliveryRouteExistsDeliveryRoute**
> Object deliveryRouteExistsDeliveryRoute(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DeliveryRouteApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DeliveryRouteApi apiInstance = new DeliveryRouteApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.deliveryRouteExistsDeliveryRoute(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeliveryRouteApi#deliveryRouteExistsDeliveryRoute");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="deliveryRouteGetDeliveryRoute"></a>
# **deliveryRouteGetDeliveryRoute**
> Object deliveryRouteGetDeliveryRoute(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DeliveryRouteApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DeliveryRouteApi apiInstance = new DeliveryRouteApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.deliveryRouteGetDeliveryRoute(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeliveryRouteApi#deliveryRouteGetDeliveryRoute");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="deliveryRouteGetDeliveryRoutes"></a>
# **deliveryRouteGetDeliveryRoutes**
> Object deliveryRouteGetDeliveryRoutes(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DeliveryRouteApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DeliveryRouteApi apiInstance = new DeliveryRouteApi();
GetFilteredDeliveryRouteQuery query = new GetFilteredDeliveryRouteQuery(); // GetFilteredDeliveryRouteQuery | 
try {
    Object result = apiInstance.deliveryRouteGetDeliveryRoutes(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeliveryRouteApi#deliveryRouteGetDeliveryRoutes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredDeliveryRouteQuery**](GetFilteredDeliveryRouteQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="deliveryRouteUpdateDeliveryRoute"></a>
# **deliveryRouteUpdateDeliveryRoute**
> Object deliveryRouteUpdateDeliveryRoute(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DeliveryRouteApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DeliveryRouteApi apiInstance = new DeliveryRouteApi();
Integer number = 56; // Integer | 
DeliveryRouteDto dto = new DeliveryRouteDto(); // DeliveryRouteDto | 
try {
    Object result = apiInstance.deliveryRouteUpdateDeliveryRoute(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeliveryRouteApi#deliveryRouteUpdateDeliveryRoute");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**DeliveryRouteDto**](DeliveryRouteDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

