# WpuSelectionRuleGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**wpuSelectionRuleGroupCreateWpuSelectionRuleGroup**](WpuSelectionRuleGroupApi.md#wpuSelectionRuleGroupCreateWpuSelectionRuleGroup) | **POST** /WpuSelectionRuleGroup | 
[**wpuSelectionRuleGroupDeleteWpuSelectionRuleGroup**](WpuSelectionRuleGroupApi.md#wpuSelectionRuleGroupDeleteWpuSelectionRuleGroup) | **DELETE** /WpuSelectionRuleGroup/{selRuleGroupId} | 
[**wpuSelectionRuleGroupExistsWpuSelectionRuleGroup**](WpuSelectionRuleGroupApi.md#wpuSelectionRuleGroupExistsWpuSelectionRuleGroup) | **GET** /WpuSelectionRuleGroup/exists/{selRuleGroupId} | 
[**wpuSelectionRuleGroupGetWpuSelectionRuleGroup**](WpuSelectionRuleGroupApi.md#wpuSelectionRuleGroupGetWpuSelectionRuleGroup) | **GET** /WpuSelectionRuleGroup/{selRuleGroupId} | 
[**wpuSelectionRuleGroupGetWpuSelectionRuleGroups**](WpuSelectionRuleGroupApi.md#wpuSelectionRuleGroupGetWpuSelectionRuleGroups) | **POST** /WpuSelectionRuleGroup/search | 
[**wpuSelectionRuleGroupUpdateWpuSelectionRuleGroup**](WpuSelectionRuleGroupApi.md#wpuSelectionRuleGroupUpdateWpuSelectionRuleGroup) | **PUT** /WpuSelectionRuleGroup/{selRuleGroupId} | 


<a name="wpuSelectionRuleGroupCreateWpuSelectionRuleGroup"></a>
# **wpuSelectionRuleGroupCreateWpuSelectionRuleGroup**
> Object wpuSelectionRuleGroupCreateWpuSelectionRuleGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WpuSelectionRuleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WpuSelectionRuleGroupApi apiInstance = new WpuSelectionRuleGroupApi();
WpuSelectionRuleGroupDto dto = new WpuSelectionRuleGroupDto(); // WpuSelectionRuleGroupDto | 
try {
    Object result = apiInstance.wpuSelectionRuleGroupCreateWpuSelectionRuleGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WpuSelectionRuleGroupApi#wpuSelectionRuleGroupCreateWpuSelectionRuleGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**WpuSelectionRuleGroupDto**](WpuSelectionRuleGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="wpuSelectionRuleGroupDeleteWpuSelectionRuleGroup"></a>
# **wpuSelectionRuleGroupDeleteWpuSelectionRuleGroup**
> Object wpuSelectionRuleGroupDeleteWpuSelectionRuleGroup(selRuleGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WpuSelectionRuleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WpuSelectionRuleGroupApi apiInstance = new WpuSelectionRuleGroupApi();
Integer selRuleGroupId = 56; // Integer | 
try {
    Object result = apiInstance.wpuSelectionRuleGroupDeleteWpuSelectionRuleGroup(selRuleGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WpuSelectionRuleGroupApi#wpuSelectionRuleGroupDeleteWpuSelectionRuleGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **selRuleGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="wpuSelectionRuleGroupExistsWpuSelectionRuleGroup"></a>
# **wpuSelectionRuleGroupExistsWpuSelectionRuleGroup**
> Object wpuSelectionRuleGroupExistsWpuSelectionRuleGroup(selRuleGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WpuSelectionRuleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WpuSelectionRuleGroupApi apiInstance = new WpuSelectionRuleGroupApi();
Integer selRuleGroupId = 56; // Integer | 
try {
    Object result = apiInstance.wpuSelectionRuleGroupExistsWpuSelectionRuleGroup(selRuleGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WpuSelectionRuleGroupApi#wpuSelectionRuleGroupExistsWpuSelectionRuleGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **selRuleGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="wpuSelectionRuleGroupGetWpuSelectionRuleGroup"></a>
# **wpuSelectionRuleGroupGetWpuSelectionRuleGroup**
> Object wpuSelectionRuleGroupGetWpuSelectionRuleGroup(selRuleGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WpuSelectionRuleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WpuSelectionRuleGroupApi apiInstance = new WpuSelectionRuleGroupApi();
Integer selRuleGroupId = 56; // Integer | 
try {
    Object result = apiInstance.wpuSelectionRuleGroupGetWpuSelectionRuleGroup(selRuleGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WpuSelectionRuleGroupApi#wpuSelectionRuleGroupGetWpuSelectionRuleGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **selRuleGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="wpuSelectionRuleGroupGetWpuSelectionRuleGroups"></a>
# **wpuSelectionRuleGroupGetWpuSelectionRuleGroups**
> Object wpuSelectionRuleGroupGetWpuSelectionRuleGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WpuSelectionRuleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WpuSelectionRuleGroupApi apiInstance = new WpuSelectionRuleGroupApi();
GetFilteredWpuSelectionRuleGroupQuery query = new GetFilteredWpuSelectionRuleGroupQuery(); // GetFilteredWpuSelectionRuleGroupQuery | 
try {
    Object result = apiInstance.wpuSelectionRuleGroupGetWpuSelectionRuleGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WpuSelectionRuleGroupApi#wpuSelectionRuleGroupGetWpuSelectionRuleGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredWpuSelectionRuleGroupQuery**](GetFilteredWpuSelectionRuleGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="wpuSelectionRuleGroupUpdateWpuSelectionRuleGroup"></a>
# **wpuSelectionRuleGroupUpdateWpuSelectionRuleGroup**
> Object wpuSelectionRuleGroupUpdateWpuSelectionRuleGroup(selRuleGroupId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.WpuSelectionRuleGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

WpuSelectionRuleGroupApi apiInstance = new WpuSelectionRuleGroupApi();
Integer selRuleGroupId = 56; // Integer | 
WpuSelectionRuleGroupDto dto = new WpuSelectionRuleGroupDto(); // WpuSelectionRuleGroupDto | 
try {
    Object result = apiInstance.wpuSelectionRuleGroupUpdateWpuSelectionRuleGroup(selRuleGroupId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WpuSelectionRuleGroupApi#wpuSelectionRuleGroupUpdateWpuSelectionRuleGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **selRuleGroupId** | **Integer**|  |
 **dto** | [**WpuSelectionRuleGroupDto**](WpuSelectionRuleGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

