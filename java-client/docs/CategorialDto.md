
# CategorialDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**description** | **String** |  |  [optional]
**limitValue** | **Double** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



