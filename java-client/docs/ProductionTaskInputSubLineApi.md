# ProductionTaskInputSubLineApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productionTaskInputSubLineExistsProductionTaskInputSubLine**](ProductionTaskInputSubLineApi.md#productionTaskInputSubLineExistsProductionTaskInputSubLine) | **GET** /ProductionTaskInputSubLine/exists/{orderId}/{orderLineId}/{subLineId} | 
[**productionTaskInputSubLineGetProductionTaskInputSubLine**](ProductionTaskInputSubLineApi.md#productionTaskInputSubLineGetProductionTaskInputSubLine) | **GET** /ProductionTaskInputSubLine/{orderId}/{orderLineId}/{subLineId} | 
[**productionTaskInputSubLineGetProductionTaskInputSubLines**](ProductionTaskInputSubLineApi.md#productionTaskInputSubLineGetProductionTaskInputSubLines) | **POST** /ProductionTaskInputSubLine/search | 


<a name="productionTaskInputSubLineExistsProductionTaskInputSubLine"></a>
# **productionTaskInputSubLineExistsProductionTaskInputSubLine**
> Object productionTaskInputSubLineExistsProductionTaskInputSubLine(orderId, orderLineId, subLineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskInputSubLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskInputSubLineApi apiInstance = new ProductionTaskInputSubLineApi();
Integer orderId = 56; // Integer | 
Integer orderLineId = 56; // Integer | 
Integer subLineId = 56; // Integer | 
try {
    Object result = apiInstance.productionTaskInputSubLineExistsProductionTaskInputSubLine(orderId, orderLineId, subLineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskInputSubLineApi#productionTaskInputSubLineExistsProductionTaskInputSubLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderId** | **Integer**|  |
 **orderLineId** | **Integer**|  |
 **subLineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionTaskInputSubLineGetProductionTaskInputSubLine"></a>
# **productionTaskInputSubLineGetProductionTaskInputSubLine**
> Object productionTaskInputSubLineGetProductionTaskInputSubLine(orderId, orderLineId, subLineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskInputSubLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskInputSubLineApi apiInstance = new ProductionTaskInputSubLineApi();
Integer orderId = 56; // Integer | 
Integer orderLineId = 56; // Integer | 
Integer subLineId = 56; // Integer | 
try {
    Object result = apiInstance.productionTaskInputSubLineGetProductionTaskInputSubLine(orderId, orderLineId, subLineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskInputSubLineApi#productionTaskInputSubLineGetProductionTaskInputSubLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderId** | **Integer**|  |
 **orderLineId** | **Integer**|  |
 **subLineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionTaskInputSubLineGetProductionTaskInputSubLines"></a>
# **productionTaskInputSubLineGetProductionTaskInputSubLines**
> Object productionTaskInputSubLineGetProductionTaskInputSubLines(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskInputSubLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskInputSubLineApi apiInstance = new ProductionTaskInputSubLineApi();
GetFilteredProductionTaskInputSubLineQuery query = new GetFilteredProductionTaskInputSubLineQuery(); // GetFilteredProductionTaskInputSubLineQuery | 
try {
    Object result = apiInstance.productionTaskInputSubLineGetProductionTaskInputSubLines(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskInputSubLineApi#productionTaskInputSubLineGetProductionTaskInputSubLines");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductionTaskInputSubLineQuery**](GetFilteredProductionTaskInputSubLineQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

