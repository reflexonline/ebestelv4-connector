# SaleReturnReasonApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleReturnReasonCreateSaleReturnReason**](SaleReturnReasonApi.md#saleReturnReasonCreateSaleReturnReason) | **POST** /SaleReturnReason | 
[**saleReturnReasonDeleteSaleReturnReason**](SaleReturnReasonApi.md#saleReturnReasonDeleteSaleReturnReason) | **DELETE** /SaleReturnReason/{number} | 
[**saleReturnReasonExistsSaleReturnReason**](SaleReturnReasonApi.md#saleReturnReasonExistsSaleReturnReason) | **GET** /SaleReturnReason/exists/{number} | 
[**saleReturnReasonGetSaleReturnReason**](SaleReturnReasonApi.md#saleReturnReasonGetSaleReturnReason) | **GET** /SaleReturnReason/{number} | 
[**saleReturnReasonGetSaleReturnReasons**](SaleReturnReasonApi.md#saleReturnReasonGetSaleReturnReasons) | **POST** /SaleReturnReason/search | 
[**saleReturnReasonUpdateSaleReturnReason**](SaleReturnReasonApi.md#saleReturnReasonUpdateSaleReturnReason) | **PUT** /SaleReturnReason/{number} | 


<a name="saleReturnReasonCreateSaleReturnReason"></a>
# **saleReturnReasonCreateSaleReturnReason**
> Object saleReturnReasonCreateSaleReturnReason(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleReturnReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleReturnReasonApi apiInstance = new SaleReturnReasonApi();
SaleReturnReasonDto dto = new SaleReturnReasonDto(); // SaleReturnReasonDto | 
try {
    Object result = apiInstance.saleReturnReasonCreateSaleReturnReason(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleReturnReasonApi#saleReturnReasonCreateSaleReturnReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SaleReturnReasonDto**](SaleReturnReasonDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleReturnReasonDeleteSaleReturnReason"></a>
# **saleReturnReasonDeleteSaleReturnReason**
> Object saleReturnReasonDeleteSaleReturnReason(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleReturnReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleReturnReasonApi apiInstance = new SaleReturnReasonApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.saleReturnReasonDeleteSaleReturnReason(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleReturnReasonApi#saleReturnReasonDeleteSaleReturnReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleReturnReasonExistsSaleReturnReason"></a>
# **saleReturnReasonExistsSaleReturnReason**
> Object saleReturnReasonExistsSaleReturnReason(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleReturnReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleReturnReasonApi apiInstance = new SaleReturnReasonApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.saleReturnReasonExistsSaleReturnReason(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleReturnReasonApi#saleReturnReasonExistsSaleReturnReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleReturnReasonGetSaleReturnReason"></a>
# **saleReturnReasonGetSaleReturnReason**
> Object saleReturnReasonGetSaleReturnReason(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleReturnReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleReturnReasonApi apiInstance = new SaleReturnReasonApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.saleReturnReasonGetSaleReturnReason(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleReturnReasonApi#saleReturnReasonGetSaleReturnReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleReturnReasonGetSaleReturnReasons"></a>
# **saleReturnReasonGetSaleReturnReasons**
> Object saleReturnReasonGetSaleReturnReasons(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleReturnReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleReturnReasonApi apiInstance = new SaleReturnReasonApi();
GetFilteredSaleReturnReasonQuery query = new GetFilteredSaleReturnReasonQuery(); // GetFilteredSaleReturnReasonQuery | 
try {
    Object result = apiInstance.saleReturnReasonGetSaleReturnReasons(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleReturnReasonApi#saleReturnReasonGetSaleReturnReasons");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSaleReturnReasonQuery**](GetFilteredSaleReturnReasonQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleReturnReasonUpdateSaleReturnReason"></a>
# **saleReturnReasonUpdateSaleReturnReason**
> Object saleReturnReasonUpdateSaleReturnReason(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleReturnReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleReturnReasonApi apiInstance = new SaleReturnReasonApi();
Integer number = 56; // Integer | 
SaleReturnReasonDto dto = new SaleReturnReasonDto(); // SaleReturnReasonDto | 
try {
    Object result = apiInstance.saleReturnReasonUpdateSaleReturnReason(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleReturnReasonApi#saleReturnReasonUpdateSaleReturnReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**SaleReturnReasonDto**](SaleReturnReasonDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

