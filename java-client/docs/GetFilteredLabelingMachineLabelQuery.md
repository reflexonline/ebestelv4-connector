
# GetFilteredLabelingMachineLabelQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**etiketNumberMinValue** | **Integer** |  |  [optional]
**etiketNumberMaxValue** | **Integer** |  |  [optional]
**labelingMachineTypeMinValue** | [**LabelingMachineTypeMinValueEnum**](#LabelingMachineTypeMinValueEnum) |  |  [optional]
**labelingMachineTypeMaxValue** | [**LabelingMachineTypeMaxValueEnum**](#LabelingMachineTypeMaxValueEnum) |  |  [optional]


<a name="LabelingMachineTypeMinValueEnum"></a>
## Enum: LabelingMachineTypeMinValueEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
ESPERACIP | &quot;EsperaCIP&quot;
BIZERBA | &quot;Bizerba&quot;
BOPACK | &quot;Bopack&quot;
ESPERAES | &quot;EsperaES&quot;
DIGI | &quot;Digi&quot;
BIZERBABRAIN | &quot;BizerbaBrain&quot;
FLEX | &quot;Flex&quot;
BIZERBAGLP | &quot;BizerbaGLP&quot;
DELFORD | &quot;Delford&quot;
HERBERTGEMINI | &quot;HerbertGemini&quot;
MANUAL | &quot;Manual&quot;
ESPERAEST | &quot;EsperaEST&quot;


<a name="LabelingMachineTypeMaxValueEnum"></a>
## Enum: LabelingMachineTypeMaxValueEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
ESPERACIP | &quot;EsperaCIP&quot;
BIZERBA | &quot;Bizerba&quot;
BOPACK | &quot;Bopack&quot;
ESPERAES | &quot;EsperaES&quot;
DIGI | &quot;Digi&quot;
BIZERBABRAIN | &quot;BizerbaBrain&quot;
FLEX | &quot;Flex&quot;
BIZERBAGLP | &quot;BizerbaGLP&quot;
DELFORD | &quot;Delford&quot;
HERBERTGEMINI | &quot;HerbertGemini&quot;
MANUAL | &quot;Manual&quot;
ESPERAEST | &quot;EsperaEST&quot;



