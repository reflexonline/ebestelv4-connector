
# NutritionalDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nutritionalId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**unit** | **String** |  |  [optional]
**sortOrder** | **Integer** |  |  [optional]
**pdmCode** | **String** |  |  [optional]
**unitPdmCode** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



