
# GetFilteredWpuSelectionRuleGroupQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**selRuleGroupIdMinValue** | **Integer** |  |  [optional]
**selRuleGroupIdMaxValue** | **Integer** |  |  [optional]
**selRuleGroupNumberMinValue** | **Integer** |  |  [optional]
**selRuleGroupNumberMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



