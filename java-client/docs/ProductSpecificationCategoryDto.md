
# ProductSpecificationCategoryDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productSpecificationCategorieId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**productSpecificationMainCategorie** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



