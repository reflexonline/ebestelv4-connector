# CaptureRegionApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**captureRegionCreateCaptureRegion**](CaptureRegionApi.md#captureRegionCreateCaptureRegion) | **POST** /CaptureRegion | 
[**captureRegionDeleteCaptureRegion**](CaptureRegionApi.md#captureRegionDeleteCaptureRegion) | **DELETE** /CaptureRegion/{number} | 
[**captureRegionExistsCaptureRegion**](CaptureRegionApi.md#captureRegionExistsCaptureRegion) | **GET** /CaptureRegion/exists/{number} | 
[**captureRegionGetCaptureRegion**](CaptureRegionApi.md#captureRegionGetCaptureRegion) | **GET** /CaptureRegion/{number} | 
[**captureRegionGetCaptureRegions**](CaptureRegionApi.md#captureRegionGetCaptureRegions) | **POST** /CaptureRegion/search | 
[**captureRegionUpdateCaptureRegion**](CaptureRegionApi.md#captureRegionUpdateCaptureRegion) | **PUT** /CaptureRegion/{number} | 


<a name="captureRegionCreateCaptureRegion"></a>
# **captureRegionCreateCaptureRegion**
> Object captureRegionCreateCaptureRegion(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CaptureRegionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CaptureRegionApi apiInstance = new CaptureRegionApi();
CaptureRegionDto dto = new CaptureRegionDto(); // CaptureRegionDto | 
try {
    Object result = apiInstance.captureRegionCreateCaptureRegion(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CaptureRegionApi#captureRegionCreateCaptureRegion");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**CaptureRegionDto**](CaptureRegionDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="captureRegionDeleteCaptureRegion"></a>
# **captureRegionDeleteCaptureRegion**
> Object captureRegionDeleteCaptureRegion(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CaptureRegionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CaptureRegionApi apiInstance = new CaptureRegionApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.captureRegionDeleteCaptureRegion(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CaptureRegionApi#captureRegionDeleteCaptureRegion");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="captureRegionExistsCaptureRegion"></a>
# **captureRegionExistsCaptureRegion**
> Object captureRegionExistsCaptureRegion(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CaptureRegionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CaptureRegionApi apiInstance = new CaptureRegionApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.captureRegionExistsCaptureRegion(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CaptureRegionApi#captureRegionExistsCaptureRegion");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="captureRegionGetCaptureRegion"></a>
# **captureRegionGetCaptureRegion**
> Object captureRegionGetCaptureRegion(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CaptureRegionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CaptureRegionApi apiInstance = new CaptureRegionApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.captureRegionGetCaptureRegion(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CaptureRegionApi#captureRegionGetCaptureRegion");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="captureRegionGetCaptureRegions"></a>
# **captureRegionGetCaptureRegions**
> Object captureRegionGetCaptureRegions(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CaptureRegionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CaptureRegionApi apiInstance = new CaptureRegionApi();
GetFilteredCaptureRegionQuery query = new GetFilteredCaptureRegionQuery(); // GetFilteredCaptureRegionQuery | 
try {
    Object result = apiInstance.captureRegionGetCaptureRegions(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CaptureRegionApi#captureRegionGetCaptureRegions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredCaptureRegionQuery**](GetFilteredCaptureRegionQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="captureRegionUpdateCaptureRegion"></a>
# **captureRegionUpdateCaptureRegion**
> Object captureRegionUpdateCaptureRegion(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CaptureRegionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CaptureRegionApi apiInstance = new CaptureRegionApi();
Integer number = 56; // Integer | 
CaptureRegionDto dto = new CaptureRegionDto(); // CaptureRegionDto | 
try {
    Object result = apiInstance.captureRegionUpdateCaptureRegion(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CaptureRegionApi#captureRegionUpdateCaptureRegion");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**CaptureRegionDto**](CaptureRegionDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

