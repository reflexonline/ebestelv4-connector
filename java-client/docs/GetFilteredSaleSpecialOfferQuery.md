
# GetFilteredSaleSpecialOfferQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**articleIdMinValue** | **Integer** |  |  [optional]
**articleIdMaxValue** | **Integer** |  |  [optional]
**alternativeArticleGroupIdMinValue** | **Integer** |  |  [optional]
**alternativeArticleGroupIdMaxValue** | **Integer** |  |  [optional]
**customerIdMinValue** | **Integer** |  |  [optional]
**customerIdMaxValue** | **Integer** |  |  [optional]
**organizationIdMinValue** | **Integer** |  |  [optional]
**organizationIdMaxValue** | **Integer** |  |  [optional]
**subOrganizationIdMinValue** | **Integer** |  |  [optional]
**subOrganizationIdMaxValue** | **Integer** |  |  [optional]
**dateIndexMinValue** | **Integer** |  |  [optional]
**dateIndexMaxValue** | **Integer** |  |  [optional]
**fromDateMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**fromDateMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**untilDateMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**untilDateMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**isActionMinValue** | **Boolean** |  |  [optional]
**isActionMaxValue** | **Boolean** |  |  [optional]
**saleSpecialOfferTypeMinValue** | [**SaleSpecialOfferTypeMinValueEnum**](#SaleSpecialOfferTypeMinValueEnum) |  |  [optional]
**saleSpecialOfferTypeMaxValue** | [**SaleSpecialOfferTypeMaxValueEnum**](#SaleSpecialOfferTypeMaxValueEnum) |  |  [optional]
**groupTextIdMinValue** | **Integer** |  |  [optional]
**groupTextIdMaxValue** | **Integer** |  |  [optional]
**specialOfferIdMinValue** | **Integer** |  |  [optional]
**specialOfferIdMaxValue** | **Integer** |  |  [optional]
**tagIdMinValue** | **Integer** |  |  [optional]
**tagIdMaxValue** | **Integer** |  |  [optional]


<a name="SaleSpecialOfferTypeMinValueEnum"></a>
## Enum: SaleSpecialOfferTypeMinValueEnum
Name | Value
---- | -----
CUSTOMER | &quot;Customer&quot;
SUBORGANIZATION | &quot;Suborganization&quot;
ORGANIZATION | &quot;Organization&quot;
ALLCUSTOMERS | &quot;AllCustomers&quot;


<a name="SaleSpecialOfferTypeMaxValueEnum"></a>
## Enum: SaleSpecialOfferTypeMaxValueEnum
Name | Value
---- | -----
CUSTOMER | &quot;Customer&quot;
SUBORGANIZATION | &quot;Suborganization&quot;
ORGANIZATION | &quot;Organization&quot;
ALLCUSTOMERS | &quot;AllCustomers&quot;



