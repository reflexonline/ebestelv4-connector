
# GetFilteredCalculationCostQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**calculationCostIdMinValue** | **Integer** |  |  [optional]
**calculationCostIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



