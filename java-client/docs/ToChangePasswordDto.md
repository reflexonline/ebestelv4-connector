
# ToChangePasswordDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**oldPassword** | **String** |  | 
**newPassword** | **String** |  | 
**autoGeneratePassword** | **Boolean** |  | 



