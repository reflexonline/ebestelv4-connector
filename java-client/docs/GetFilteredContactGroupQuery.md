
# GetFilteredContactGroupQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contactGroupIdMinValue** | **Integer** |  |  [optional]
**contactGroupIdMaxValue** | **Integer** |  |  [optional]
**userKeyMinValue** | **String** |  |  [optional]
**userKeyMaxValue** | **String** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



