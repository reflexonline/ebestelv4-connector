# ProductionTaskInputLineApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productionTaskInputLineExistsProductionTaskInputLine**](ProductionTaskInputLineApi.md#productionTaskInputLineExistsProductionTaskInputLine) | **GET** /ProductionTaskInputLine/exists/{productionTaskId}/{lineId} | 
[**productionTaskInputLineGetProductionTaskInputLine**](ProductionTaskInputLineApi.md#productionTaskInputLineGetProductionTaskInputLine) | **GET** /ProductionTaskInputLine/{productionTaskId}/{lineId} | 
[**productionTaskInputLineGetProductionTaskInputLines**](ProductionTaskInputLineApi.md#productionTaskInputLineGetProductionTaskInputLines) | **POST** /ProductionTaskInputLine/search | 


<a name="productionTaskInputLineExistsProductionTaskInputLine"></a>
# **productionTaskInputLineExistsProductionTaskInputLine**
> Object productionTaskInputLineExistsProductionTaskInputLine(productionTaskId, lineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskInputLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskInputLineApi apiInstance = new ProductionTaskInputLineApi();
Integer productionTaskId = 56; // Integer | 
Integer lineId = 56; // Integer | 
try {
    Object result = apiInstance.productionTaskInputLineExistsProductionTaskInputLine(productionTaskId, lineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskInputLineApi#productionTaskInputLineExistsProductionTaskInputLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionTaskId** | **Integer**|  |
 **lineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionTaskInputLineGetProductionTaskInputLine"></a>
# **productionTaskInputLineGetProductionTaskInputLine**
> Object productionTaskInputLineGetProductionTaskInputLine(productionTaskId, lineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskInputLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskInputLineApi apiInstance = new ProductionTaskInputLineApi();
Integer productionTaskId = 56; // Integer | 
Integer lineId = 56; // Integer | 
try {
    Object result = apiInstance.productionTaskInputLineGetProductionTaskInputLine(productionTaskId, lineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskInputLineApi#productionTaskInputLineGetProductionTaskInputLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionTaskId** | **Integer**|  |
 **lineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionTaskInputLineGetProductionTaskInputLines"></a>
# **productionTaskInputLineGetProductionTaskInputLines**
> Object productionTaskInputLineGetProductionTaskInputLines(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskInputLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskInputLineApi apiInstance = new ProductionTaskInputLineApi();
GetFilteredProductionTaskInputLineQuery query = new GetFilteredProductionTaskInputLineQuery(); // GetFilteredProductionTaskInputLineQuery | 
try {
    Object result = apiInstance.productionTaskInputLineGetProductionTaskInputLines(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskInputLineApi#productionTaskInputLineGetProductionTaskInputLines");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductionTaskInputLineQuery**](GetFilteredProductionTaskInputLineQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

