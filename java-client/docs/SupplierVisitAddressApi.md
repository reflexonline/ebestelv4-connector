# SupplierVisitAddressApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**supplierVisitAddressExistsSupplierVisitAddress**](SupplierVisitAddressApi.md#supplierVisitAddressExistsSupplierVisitAddress) | **GET** /SupplierVisitAddress/exists/{supplierId} | 
[**supplierVisitAddressGetSupplierVisitAddress**](SupplierVisitAddressApi.md#supplierVisitAddressGetSupplierVisitAddress) | **GET** /SupplierVisitAddress/{supplierId} | 
[**supplierVisitAddressGetSupplierVisitAddresses**](SupplierVisitAddressApi.md#supplierVisitAddressGetSupplierVisitAddresses) | **POST** /SupplierVisitAddress/search | 


<a name="supplierVisitAddressExistsSupplierVisitAddress"></a>
# **supplierVisitAddressExistsSupplierVisitAddress**
> Object supplierVisitAddressExistsSupplierVisitAddress(supplierId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierVisitAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierVisitAddressApi apiInstance = new SupplierVisitAddressApi();
Integer supplierId = 56; // Integer | 
try {
    Object result = apiInstance.supplierVisitAddressExistsSupplierVisitAddress(supplierId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierVisitAddressApi#supplierVisitAddressExistsSupplierVisitAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="supplierVisitAddressGetSupplierVisitAddress"></a>
# **supplierVisitAddressGetSupplierVisitAddress**
> Object supplierVisitAddressGetSupplierVisitAddress(supplierId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierVisitAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierVisitAddressApi apiInstance = new SupplierVisitAddressApi();
Integer supplierId = 56; // Integer | 
try {
    Object result = apiInstance.supplierVisitAddressGetSupplierVisitAddress(supplierId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierVisitAddressApi#supplierVisitAddressGetSupplierVisitAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="supplierVisitAddressGetSupplierVisitAddresses"></a>
# **supplierVisitAddressGetSupplierVisitAddresses**
> Object supplierVisitAddressGetSupplierVisitAddresses(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierVisitAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierVisitAddressApi apiInstance = new SupplierVisitAddressApi();
GetFilteredSupplierVisitAddressQuery query = new GetFilteredSupplierVisitAddressQuery(); // GetFilteredSupplierVisitAddressQuery | 
try {
    Object result = apiInstance.supplierVisitAddressGetSupplierVisitAddresses(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierVisitAddressApi#supplierVisitAddressGetSupplierVisitAddresses");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSupplierVisitAddressQuery**](GetFilteredSupplierVisitAddressQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

