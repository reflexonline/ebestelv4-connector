# SpecialBranchApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**specialBranchDeleteSpecialBranchMutation**](SpecialBranchApi.md#specialBranchDeleteSpecialBranchMutation) | **DELETE** /SpecialBranch/{specialBranchId}/Mutation/{specialBranchMutationId} | 
[**specialBranchGetSpecialBranch**](SpecialBranchApi.md#specialBranchGetSpecialBranch) | **GET** /SpecialBranch/{specialBranchId} | 
[**specialBranchGetSpecialBranchMutation**](SpecialBranchApi.md#specialBranchGetSpecialBranchMutation) | **GET** /SpecialBranch/{specialBranchId}/Mutation/{specialBranchMutationId} | 
[**specialBranchGetSpecialBranchMutations**](SpecialBranchApi.md#specialBranchGetSpecialBranchMutations) | **POST** /SpecialBranch/{specialBranchId}/Mutation/Search | 


<a name="specialBranchDeleteSpecialBranchMutation"></a>
# **specialBranchDeleteSpecialBranchMutation**
> Object specialBranchDeleteSpecialBranchMutation(specialBranchId, specialBranchMutationId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SpecialBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SpecialBranchApi apiInstance = new SpecialBranchApi();
Integer specialBranchId = 56; // Integer | 
Integer specialBranchMutationId = 56; // Integer | 
try {
    Object result = apiInstance.specialBranchDeleteSpecialBranchMutation(specialBranchId, specialBranchMutationId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SpecialBranchApi#specialBranchDeleteSpecialBranchMutation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **specialBranchId** | **Integer**|  |
 **specialBranchMutationId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="specialBranchGetSpecialBranch"></a>
# **specialBranchGetSpecialBranch**
> Object specialBranchGetSpecialBranch(specialBranchId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SpecialBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SpecialBranchApi apiInstance = new SpecialBranchApi();
Integer specialBranchId = 56; // Integer | 
try {
    Object result = apiInstance.specialBranchGetSpecialBranch(specialBranchId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SpecialBranchApi#specialBranchGetSpecialBranch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **specialBranchId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="specialBranchGetSpecialBranchMutation"></a>
# **specialBranchGetSpecialBranchMutation**
> Object specialBranchGetSpecialBranchMutation(specialBranchId, specialBranchMutationId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SpecialBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SpecialBranchApi apiInstance = new SpecialBranchApi();
Integer specialBranchId = 56; // Integer | 
Integer specialBranchMutationId = 56; // Integer | 
try {
    Object result = apiInstance.specialBranchGetSpecialBranchMutation(specialBranchId, specialBranchMutationId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SpecialBranchApi#specialBranchGetSpecialBranchMutation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **specialBranchId** | **Integer**|  |
 **specialBranchMutationId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="specialBranchGetSpecialBranchMutations"></a>
# **specialBranchGetSpecialBranchMutations**
> Object specialBranchGetSpecialBranchMutations(specialBranchId, mutationQuery)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SpecialBranchApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SpecialBranchApi apiInstance = new SpecialBranchApi();
Integer specialBranchId = 56; // Integer | 
GetFilteredSpecialBranchMutationUserQuery mutationQuery = new GetFilteredSpecialBranchMutationUserQuery(); // GetFilteredSpecialBranchMutationUserQuery | 
try {
    Object result = apiInstance.specialBranchGetSpecialBranchMutations(specialBranchId, mutationQuery);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SpecialBranchApi#specialBranchGetSpecialBranchMutations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **specialBranchId** | **Integer**|  |
 **mutationQuery** | [**GetFilteredSpecialBranchMutationUserQuery**](GetFilteredSpecialBranchMutationUserQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

