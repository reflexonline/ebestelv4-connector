# PriceListApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**priceListCreatePriceList**](PriceListApi.md#priceListCreatePriceList) | **POST** /PriceList | 
[**priceListDeletePriceList**](PriceListApi.md#priceListDeletePriceList) | **DELETE** /PriceList/{salePriceListId} | 
[**priceListExistsPriceList**](PriceListApi.md#priceListExistsPriceList) | **GET** /PriceList/exists/{salePriceListId} | 
[**priceListGetPriceList**](PriceListApi.md#priceListGetPriceList) | **GET** /PriceList/{salePriceListId} | 
[**priceListGetPriceLists**](PriceListApi.md#priceListGetPriceLists) | **POST** /PriceList/search | 
[**priceListUpdatePriceList**](PriceListApi.md#priceListUpdatePriceList) | **PUT** /PriceList/{salePriceListId} | 


<a name="priceListCreatePriceList"></a>
# **priceListCreatePriceList**
> Object priceListCreatePriceList(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PriceListApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PriceListApi apiInstance = new PriceListApi();
PriceListDto dto = new PriceListDto(); // PriceListDto | 
try {
    Object result = apiInstance.priceListCreatePriceList(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PriceListApi#priceListCreatePriceList");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PriceListDto**](PriceListDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="priceListDeletePriceList"></a>
# **priceListDeletePriceList**
> Object priceListDeletePriceList(salePriceListId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PriceListApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PriceListApi apiInstance = new PriceListApi();
Integer salePriceListId = 56; // Integer | 
try {
    Object result = apiInstance.priceListDeletePriceList(salePriceListId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PriceListApi#priceListDeletePriceList");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **salePriceListId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="priceListExistsPriceList"></a>
# **priceListExistsPriceList**
> Object priceListExistsPriceList(salePriceListId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PriceListApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PriceListApi apiInstance = new PriceListApi();
Integer salePriceListId = 56; // Integer | 
try {
    Object result = apiInstance.priceListExistsPriceList(salePriceListId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PriceListApi#priceListExistsPriceList");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **salePriceListId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="priceListGetPriceList"></a>
# **priceListGetPriceList**
> Object priceListGetPriceList(salePriceListId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PriceListApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PriceListApi apiInstance = new PriceListApi();
Integer salePriceListId = 56; // Integer | 
try {
    Object result = apiInstance.priceListGetPriceList(salePriceListId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PriceListApi#priceListGetPriceList");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **salePriceListId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="priceListGetPriceLists"></a>
# **priceListGetPriceLists**
> Object priceListGetPriceLists(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PriceListApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PriceListApi apiInstance = new PriceListApi();
GetFilteredPriceListQuery query = new GetFilteredPriceListQuery(); // GetFilteredPriceListQuery | 
try {
    Object result = apiInstance.priceListGetPriceLists(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PriceListApi#priceListGetPriceLists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPriceListQuery**](GetFilteredPriceListQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="priceListUpdatePriceList"></a>
# **priceListUpdatePriceList**
> Object priceListUpdatePriceList(salePriceListId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PriceListApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PriceListApi apiInstance = new PriceListApi();
Integer salePriceListId = 56; // Integer | 
PriceListDto dto = new PriceListDto(); // PriceListDto | 
try {
    Object result = apiInstance.priceListUpdatePriceList(salePriceListId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PriceListApi#priceListUpdatePriceList");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **salePriceListId** | **Integer**|  |
 **dto** | [**PriceListDto**](PriceListDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

