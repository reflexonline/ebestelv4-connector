
# TransporterDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transporterId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**addressLine1** | **String** |  |  [optional]
**addressLine2** | **String** |  |  [optional]
**postalCode** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**country** | **String** |  |  [optional]
**isoCountryCode** | **String** |  |  [optional]
**phoneNumber** | **String** |  |  [optional]
**faxNumber** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**text1** | **String** |  |  [optional]
**text2** | **String** |  |  [optional]
**text3** | **String** |  |  [optional]
**languageId** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



