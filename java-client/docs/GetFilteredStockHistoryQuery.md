
# GetFilteredStockHistoryQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stockLocationIdMinValue** | **Integer** |  |  [optional]
**stockLocationIdMaxValue** | **Integer** |  |  [optional]
**purchaseArticleIdMinValue** | **Integer** |  |  [optional]
**purchaseArticleIdMaxValue** | **Integer** |  |  [optional]
**dateMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**dateMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**volgNumberMinValue** | **Integer** |  |  [optional]
**volgNumberMaxValue** | **Integer** |  |  [optional]
**lotIdMinValue** | **Integer** |  |  [optional]
**lotIdMaxValue** | **Integer** |  |  [optional]
**stockMutationResonIdMinValue** | **Integer** |  |  [optional]
**stockMutationResonIdMaxValue** | **Integer** |  |  [optional]
**saleArticleIdMinValue** | **Integer** |  |  [optional]
**saleArticleIdMaxValue** | **Integer** |  |  [optional]
**chargeNumberMinValue** | **Integer** |  |  [optional]
**chargeNumberMaxValue** | **Integer** |  |  [optional]
**palletIdMinValue** | **Integer** |  |  [optional]
**palletIdMaxValue** | **Integer** |  |  [optional]



