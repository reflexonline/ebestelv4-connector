
# ProductionFlexFilterGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productionFlexFilterGroupId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



