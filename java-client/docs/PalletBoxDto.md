
# PalletBoxDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**boxId** | **Integer** |  | 
**palletId** | **Integer** |  |  [optional]
**lineNumber** | **Integer** |  |  [optional]
**dateFrozen** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**bestBeforeFresh** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**bestBeforeFrozen** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**receivedTemperature** | **Double** |  |  [optional]
**tempOut** | **Double** |  |  [optional]
**receivedByUserName** | **String** |  |  [optional]
**weigherOut** | **String** |  |  [optional]
**isReserved** | **Boolean** |  |  [optional]
**purchaseOrderNumber** | **Integer** |  |  [optional]
**purchaseOrderLineId** | **Integer** |  |  [optional]
**saleOrder** | **Integer** |  |  [optional]
**saleOrderLineNumber** | **Integer** |  |  [optional]
**dateReceived** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**dateTimeOut** | **Double** |  |  [optional]
**receivedScaleId** | **Integer** |  |  [optional]
**receivedSequenceNumber** | **Integer** |  |  [optional]
**scaleNoOut** | **Integer** |  |  [optional]
**scaleIndexNoOut** | **Integer** |  |  [optional]
**packingArticle** | **Integer** |  |  [optional]
**supplier** | **Integer** |  |  [optional]
**owner** | **Integer** |  |  [optional]
**countryCode** | **String** |  |  [optional]
**packingslipNumber** | **Integer** |  |  [optional]
**weighinglistNumber** | **Integer** |  |  [optional]
**customer** | **Integer** |  |  [optional]
**grossKg** | **Double** |  |  [optional]
**tareKg** | **Double** |  |  [optional]
**netKg** | **Double** |  |  [optional]
**productionDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**lot** | **Integer** |  |  [optional]
**saleArticle** | **Integer** |  |  [optional]
**quantity** | **Double** |  |  [optional]
**extBoxId** | **String** |  |  [optional]
**graiCode** | **String** |  |  [optional]
**purchaseArticle** | **Integer** |  |  [optional]
**packingQuantity** | **Double** |  |  [optional]
**salePrice** | **Double** |  |  [optional]
**finalProductionDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**alibiInSequence** | **Integer** |  |  [optional]
**alibiInSequenceType** | [**AlibiInSequenceTypeEnum**](#AlibiInSequenceTypeEnum) |  |  [optional]
**alibiOutSequence** | **Integer** |  |  [optional]
**alibiOutSequenceType** | [**AlibiOutSequenceTypeEnum**](#AlibiOutSequenceTypeEnum) |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="AlibiInSequenceTypeEnum"></a>
## Enum: AlibiInSequenceTypeEnum
Name | Value
---- | -----
LEGACY | &quot;Legacy&quot;
WEIGHT | &quot;Weight&quot;
MANUAL | &quot;Manual&quot;
THIRDPARTYMANUAL | &quot;ThirdPartyManual&quot;
CALCULATEDWEIGHT | &quot;CalculatedWeight&quot;
WSS | &quot;Wss&quot;
THIRDPARTYLEGACY | &quot;ThirdPartyLegacy&quot;
LEGACYNOTFORTRADE | &quot;LegacyNotForTrade&quot;
THIRDPARTYLEGACYNOTFORTRADE | &quot;ThirdPartyLegacyNotForTrade&quot;


<a name="AlibiOutSequenceTypeEnum"></a>
## Enum: AlibiOutSequenceTypeEnum
Name | Value
---- | -----
LEGACY | &quot;Legacy&quot;
WEIGHT | &quot;Weight&quot;
MANUAL | &quot;Manual&quot;
THIRDPARTYMANUAL | &quot;ThirdPartyManual&quot;
CALCULATEDWEIGHT | &quot;CalculatedWeight&quot;
WSS | &quot;Wss&quot;
THIRDPARTYLEGACY | &quot;ThirdPartyLegacy&quot;
LEGACYNOTFORTRADE | &quot;LegacyNotForTrade&quot;
THIRDPARTYLEGACYNOTFORTRADE | &quot;ThirdPartyLegacyNotForTrade&quot;



