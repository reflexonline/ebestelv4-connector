
# GetFilteredSubOrganizationQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subOrganizationIdMinValue** | **Integer** |  |  [optional]
**subOrganizationIdMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]



