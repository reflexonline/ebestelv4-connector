
# SaleArticlePriceListAgreementDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**priceListId** | **Integer** |  | 
**articleId** | **Integer** |  | 
**extensionType** | [**ExtensionTypeEnum**](#ExtensionTypeEnum) |  |  [optional]
**bedragOld** | **Float** |  |  [optional]
**discountPercentage** | **String** |  |  [optional]
**displayArticleOnPriceList** | **Boolean** |  |  [optional]
**mutationDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**priceListFrozenStatus** | [**PriceListFrozenStatusEnum**](#PriceListFrozenStatusEnum) |  |  [optional]
**bevrorenPrijsOld** | **Float** |  |  [optional]
**amount** | **Double** |  |  [optional]
**frozenPrice** | **Double** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="ExtensionTypeEnum"></a>
## Enum: ExtensionTypeEnum
Name | Value
---- | -----
NOPRICEAGREEMENT | &quot;NoPriceAgreement&quot;
PRICEDISCOUNT | &quot;PriceDiscount&quot;
PRICEDISCOUNTPERCENTAGE | &quot;PriceDiscountPercentage&quot;
FIXEDPRICE | &quot;FixedPrice&quot;
ARTICLEISSELECTED | &quot;ArticleIsSelected&quot;
ARTICLEISBLOCKED | &quot;ArticleIsBlocked&quot;


<a name="PriceListFrozenStatusEnum"></a>
## Enum: PriceListFrozenStatusEnum
Name | Value
---- | -----
FROZEN | &quot;Frozen&quot;
NOTFROZEN | &quot;NotFrozen&quot;
FROZENANDBLOCKED | &quot;FrozenAndBlocked&quot;



