
# GetFilteredDiscountGroupLineQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discountGroupIdMinValue** | **Integer** |  |  [optional]
**discountGroupIdMaxValue** | **Integer** |  |  [optional]
**discountEntityTypeMinValue** | [**DiscountEntityTypeMinValueEnum**](#DiscountEntityTypeMinValueEnum) |  |  [optional]
**discountEntityTypeMaxValue** | [**DiscountEntityTypeMaxValueEnum**](#DiscountEntityTypeMaxValueEnum) |  |  [optional]
**discountEntityIdMinValue** | **Integer** |  |  [optional]
**discountEntityIdMaxValue** | **Integer** |  |  [optional]
**discountFromMinValue** | **Float** |  |  [optional]
**discountFromMaxValue** | **Float** |  |  [optional]


<a name="DiscountEntityTypeMinValueEnum"></a>
## Enum: DiscountEntityTypeMinValueEnum
Name | Value
---- | -----
SALEARTICLEMAINGROUP | &quot;SaleArticleMainGroup&quot;
SALEARTICLEGROUP | &quot;SaleArticleGroup&quot;
SALEARTICLESUBGROUP | &quot;SaleArticleSubgroup&quot;
SALEARTICLE | &quot;SaleArticle&quot;
FROMORDERAMOUNT | &quot;FromOrderAmount&quot;


<a name="DiscountEntityTypeMaxValueEnum"></a>
## Enum: DiscountEntityTypeMaxValueEnum
Name | Value
---- | -----
SALEARTICLEMAINGROUP | &quot;SaleArticleMainGroup&quot;
SALEARTICLEGROUP | &quot;SaleArticleGroup&quot;
SALEARTICLESUBGROUP | &quot;SaleArticleSubgroup&quot;
SALEARTICLE | &quot;SaleArticle&quot;
FROMORDERAMOUNT | &quot;FromOrderAmount&quot;



