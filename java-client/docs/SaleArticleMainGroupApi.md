# SaleArticleMainGroupApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleArticleMainGroupCreateSaleArticleMainGroup**](SaleArticleMainGroupApi.md#saleArticleMainGroupCreateSaleArticleMainGroup) | **POST** /SaleArticleMainGroup | 
[**saleArticleMainGroupDeleteSaleArticleMainGroup**](SaleArticleMainGroupApi.md#saleArticleMainGroupDeleteSaleArticleMainGroup) | **DELETE** /SaleArticleMainGroup/{mainGroupId} | 
[**saleArticleMainGroupExistsSaleArticleMainGroup**](SaleArticleMainGroupApi.md#saleArticleMainGroupExistsSaleArticleMainGroup) | **GET** /SaleArticleMainGroup/exists/{mainGroupId} | 
[**saleArticleMainGroupGetSaleArticleMainGroup**](SaleArticleMainGroupApi.md#saleArticleMainGroupGetSaleArticleMainGroup) | **GET** /SaleArticleMainGroup/{mainGroupId} | 
[**saleArticleMainGroupGetSaleArticleMainGroups**](SaleArticleMainGroupApi.md#saleArticleMainGroupGetSaleArticleMainGroups) | **POST** /SaleArticleMainGroup/search | 
[**saleArticleMainGroupUpdateSaleArticleMainGroup**](SaleArticleMainGroupApi.md#saleArticleMainGroupUpdateSaleArticleMainGroup) | **PUT** /SaleArticleMainGroup/{mainGroupId} | 


<a name="saleArticleMainGroupCreateSaleArticleMainGroup"></a>
# **saleArticleMainGroupCreateSaleArticleMainGroup**
> Object saleArticleMainGroupCreateSaleArticleMainGroup(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleMainGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleMainGroupApi apiInstance = new SaleArticleMainGroupApi();
SaleArticleMainGroupDto dto = new SaleArticleMainGroupDto(); // SaleArticleMainGroupDto | 
try {
    Object result = apiInstance.saleArticleMainGroupCreateSaleArticleMainGroup(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleMainGroupApi#saleArticleMainGroupCreateSaleArticleMainGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SaleArticleMainGroupDto**](SaleArticleMainGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleMainGroupDeleteSaleArticleMainGroup"></a>
# **saleArticleMainGroupDeleteSaleArticleMainGroup**
> Object saleArticleMainGroupDeleteSaleArticleMainGroup(mainGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleMainGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleMainGroupApi apiInstance = new SaleArticleMainGroupApi();
Integer mainGroupId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleMainGroupDeleteSaleArticleMainGroup(mainGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleMainGroupApi#saleArticleMainGroupDeleteSaleArticleMainGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mainGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleMainGroupExistsSaleArticleMainGroup"></a>
# **saleArticleMainGroupExistsSaleArticleMainGroup**
> Object saleArticleMainGroupExistsSaleArticleMainGroup(mainGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleMainGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleMainGroupApi apiInstance = new SaleArticleMainGroupApi();
Integer mainGroupId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleMainGroupExistsSaleArticleMainGroup(mainGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleMainGroupApi#saleArticleMainGroupExistsSaleArticleMainGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mainGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleMainGroupGetSaleArticleMainGroup"></a>
# **saleArticleMainGroupGetSaleArticleMainGroup**
> Object saleArticleMainGroupGetSaleArticleMainGroup(mainGroupId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleMainGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleMainGroupApi apiInstance = new SaleArticleMainGroupApi();
Integer mainGroupId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticleMainGroupGetSaleArticleMainGroup(mainGroupId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleMainGroupApi#saleArticleMainGroupGetSaleArticleMainGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mainGroupId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticleMainGroupGetSaleArticleMainGroups"></a>
# **saleArticleMainGroupGetSaleArticleMainGroups**
> Object saleArticleMainGroupGetSaleArticleMainGroups(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleMainGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleMainGroupApi apiInstance = new SaleArticleMainGroupApi();
GetFilteredSaleArticleMainGroupQuery query = new GetFilteredSaleArticleMainGroupQuery(); // GetFilteredSaleArticleMainGroupQuery | 
try {
    Object result = apiInstance.saleArticleMainGroupGetSaleArticleMainGroups(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleMainGroupApi#saleArticleMainGroupGetSaleArticleMainGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSaleArticleMainGroupQuery**](GetFilteredSaleArticleMainGroupQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticleMainGroupUpdateSaleArticleMainGroup"></a>
# **saleArticleMainGroupUpdateSaleArticleMainGroup**
> Object saleArticleMainGroupUpdateSaleArticleMainGroup(mainGroupId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticleMainGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticleMainGroupApi apiInstance = new SaleArticleMainGroupApi();
Integer mainGroupId = 56; // Integer | 
SaleArticleMainGroupDto dto = new SaleArticleMainGroupDto(); // SaleArticleMainGroupDto | 
try {
    Object result = apiInstance.saleArticleMainGroupUpdateSaleArticleMainGroup(mainGroupId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticleMainGroupApi#saleArticleMainGroupUpdateSaleArticleMainGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mainGroupId** | **Integer**|  |
 **dto** | [**SaleArticleMainGroupDto**](SaleArticleMainGroupDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

