# ProductionTaskOutputSubLineApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productionTaskOutputSubLineExistsProductionTaskOutputSubLine**](ProductionTaskOutputSubLineApi.md#productionTaskOutputSubLineExistsProductionTaskOutputSubLine) | **GET** /ProductionTaskOutputSubLine/exists/{productionTaskId}/{productionTaskOutputLineId}/{productionTaskOutputSubLineId} | 
[**productionTaskOutputSubLineGetProductionTaskOutputSubLine**](ProductionTaskOutputSubLineApi.md#productionTaskOutputSubLineGetProductionTaskOutputSubLine) | **GET** /ProductionTaskOutputSubLine/{productionTaskId}/{productionTaskOutputLineId}/{productionTaskOutputSubLineId} | 
[**productionTaskOutputSubLineGetProductionTaskOutputSubLines**](ProductionTaskOutputSubLineApi.md#productionTaskOutputSubLineGetProductionTaskOutputSubLines) | **POST** /ProductionTaskOutputSubLine/search | 


<a name="productionTaskOutputSubLineExistsProductionTaskOutputSubLine"></a>
# **productionTaskOutputSubLineExistsProductionTaskOutputSubLine**
> Object productionTaskOutputSubLineExistsProductionTaskOutputSubLine(productionTaskId, productionTaskOutputLineId, productionTaskOutputSubLineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskOutputSubLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskOutputSubLineApi apiInstance = new ProductionTaskOutputSubLineApi();
Integer productionTaskId = 56; // Integer | 
Integer productionTaskOutputLineId = 56; // Integer | 
Integer productionTaskOutputSubLineId = 56; // Integer | 
try {
    Object result = apiInstance.productionTaskOutputSubLineExistsProductionTaskOutputSubLine(productionTaskId, productionTaskOutputLineId, productionTaskOutputSubLineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskOutputSubLineApi#productionTaskOutputSubLineExistsProductionTaskOutputSubLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionTaskId** | **Integer**|  |
 **productionTaskOutputLineId** | **Integer**|  |
 **productionTaskOutputSubLineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionTaskOutputSubLineGetProductionTaskOutputSubLine"></a>
# **productionTaskOutputSubLineGetProductionTaskOutputSubLine**
> Object productionTaskOutputSubLineGetProductionTaskOutputSubLine(productionTaskId, productionTaskOutputLineId, productionTaskOutputSubLineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskOutputSubLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskOutputSubLineApi apiInstance = new ProductionTaskOutputSubLineApi();
Integer productionTaskId = 56; // Integer | 
Integer productionTaskOutputLineId = 56; // Integer | 
Integer productionTaskOutputSubLineId = 56; // Integer | 
try {
    Object result = apiInstance.productionTaskOutputSubLineGetProductionTaskOutputSubLine(productionTaskId, productionTaskOutputLineId, productionTaskOutputSubLineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskOutputSubLineApi#productionTaskOutputSubLineGetProductionTaskOutputSubLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionTaskId** | **Integer**|  |
 **productionTaskOutputLineId** | **Integer**|  |
 **productionTaskOutputSubLineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionTaskOutputSubLineGetProductionTaskOutputSubLines"></a>
# **productionTaskOutputSubLineGetProductionTaskOutputSubLines**
> Object productionTaskOutputSubLineGetProductionTaskOutputSubLines(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskOutputSubLineApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskOutputSubLineApi apiInstance = new ProductionTaskOutputSubLineApi();
GetFilteredProductionTaskOutputSubLineQuery query = new GetFilteredProductionTaskOutputSubLineQuery(); // GetFilteredProductionTaskOutputSubLineQuery | 
try {
    Object result = apiInstance.productionTaskOutputSubLineGetProductionTaskOutputSubLines(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskOutputSubLineApi#productionTaskOutputSubLineGetProductionTaskOutputSubLines");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductionTaskOutputSubLineQuery**](GetFilteredProductionTaskOutputSubLineQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

