
# LabelingMachineLabelDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**labelingMachineType** | [**LabelingMachineTypeEnum**](#LabelingMachineTypeEnum) |  | 
**etiketNumber** | **Integer** |  | 
**omschrijving** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="LabelingMachineTypeEnum"></a>
## Enum: LabelingMachineTypeEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
ESPERACIP | &quot;EsperaCIP&quot;
BIZERBA | &quot;Bizerba&quot;
BOPACK | &quot;Bopack&quot;
ESPERAES | &quot;EsperaES&quot;
DIGI | &quot;Digi&quot;
BIZERBABRAIN | &quot;BizerbaBrain&quot;
FLEX | &quot;Flex&quot;
BIZERBAGLP | &quot;BizerbaGLP&quot;
DELFORD | &quot;Delford&quot;
HERBERTGEMINI | &quot;HerbertGemini&quot;
MANUAL | &quot;Manual&quot;
ESPERAEST | &quot;EsperaEST&quot;



