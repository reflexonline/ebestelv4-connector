
# VatScenarioDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scenarioId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



