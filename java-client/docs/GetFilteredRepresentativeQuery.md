
# GetFilteredRepresentativeQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**representativeIdMinValue** | **Integer** |  |  [optional]
**representativeIdMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]



