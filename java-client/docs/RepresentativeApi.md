# RepresentativeApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**representativeCreateRepresentative**](RepresentativeApi.md#representativeCreateRepresentative) | **POST** /Representative | 
[**representativeDeleteRepresentative**](RepresentativeApi.md#representativeDeleteRepresentative) | **DELETE** /Representative/{representativeId} | 
[**representativeExistsRepresentative**](RepresentativeApi.md#representativeExistsRepresentative) | **GET** /Representative/exists/{representativeId} | 
[**representativeGetRepresentative**](RepresentativeApi.md#representativeGetRepresentative) | **GET** /Representative/{representativeId} | 
[**representativeGetRepresentatives**](RepresentativeApi.md#representativeGetRepresentatives) | **POST** /Representative/search | 
[**representativeUpdateRepresentative**](RepresentativeApi.md#representativeUpdateRepresentative) | **PUT** /Representative/{representativeId} | 


<a name="representativeCreateRepresentative"></a>
# **representativeCreateRepresentative**
> Object representativeCreateRepresentative(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RepresentativeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RepresentativeApi apiInstance = new RepresentativeApi();
RepresentativeDto dto = new RepresentativeDto(); // RepresentativeDto | 
try {
    Object result = apiInstance.representativeCreateRepresentative(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepresentativeApi#representativeCreateRepresentative");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**RepresentativeDto**](RepresentativeDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="representativeDeleteRepresentative"></a>
# **representativeDeleteRepresentative**
> Object representativeDeleteRepresentative(representativeId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RepresentativeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RepresentativeApi apiInstance = new RepresentativeApi();
Integer representativeId = 56; // Integer | 
try {
    Object result = apiInstance.representativeDeleteRepresentative(representativeId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepresentativeApi#representativeDeleteRepresentative");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **representativeId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="representativeExistsRepresentative"></a>
# **representativeExistsRepresentative**
> Object representativeExistsRepresentative(representativeId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RepresentativeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RepresentativeApi apiInstance = new RepresentativeApi();
Integer representativeId = 56; // Integer | 
try {
    Object result = apiInstance.representativeExistsRepresentative(representativeId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepresentativeApi#representativeExistsRepresentative");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **representativeId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="representativeGetRepresentative"></a>
# **representativeGetRepresentative**
> Object representativeGetRepresentative(representativeId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RepresentativeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RepresentativeApi apiInstance = new RepresentativeApi();
Integer representativeId = 56; // Integer | 
try {
    Object result = apiInstance.representativeGetRepresentative(representativeId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepresentativeApi#representativeGetRepresentative");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **representativeId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="representativeGetRepresentatives"></a>
# **representativeGetRepresentatives**
> Object representativeGetRepresentatives(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RepresentativeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RepresentativeApi apiInstance = new RepresentativeApi();
GetFilteredRepresentativeQuery query = new GetFilteredRepresentativeQuery(); // GetFilteredRepresentativeQuery | 
try {
    Object result = apiInstance.representativeGetRepresentatives(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepresentativeApi#representativeGetRepresentatives");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredRepresentativeQuery**](GetFilteredRepresentativeQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="representativeUpdateRepresentative"></a>
# **representativeUpdateRepresentative**
> Object representativeUpdateRepresentative(representativeId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RepresentativeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RepresentativeApi apiInstance = new RepresentativeApi();
Integer representativeId = 56; // Integer | 
RepresentativeDto dto = new RepresentativeDto(); // RepresentativeDto | 
try {
    Object result = apiInstance.representativeUpdateRepresentative(representativeId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepresentativeApi#representativeUpdateRepresentative");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **representativeId** | **Integer**|  |
 **dto** | [**RepresentativeDto**](RepresentativeDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

