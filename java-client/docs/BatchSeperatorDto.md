
# BatchSeperatorDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**batchSeperatorId** | **Integer** |  | 
**batchSeperatorCode** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



