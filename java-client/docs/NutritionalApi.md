# NutritionalApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**nutritionalCreateNutritional**](NutritionalApi.md#nutritionalCreateNutritional) | **POST** /Nutritional | 
[**nutritionalDeleteNutritional**](NutritionalApi.md#nutritionalDeleteNutritional) | **DELETE** /Nutritional/{nutritionalId} | 
[**nutritionalExistsNutritional**](NutritionalApi.md#nutritionalExistsNutritional) | **GET** /Nutritional/exists/{nutritionalId} | 
[**nutritionalGetNutritional**](NutritionalApi.md#nutritionalGetNutritional) | **GET** /Nutritional/{nutritionalId} | 
[**nutritionalGetNutritionals**](NutritionalApi.md#nutritionalGetNutritionals) | **POST** /Nutritional/search | 
[**nutritionalUpdateNutritional**](NutritionalApi.md#nutritionalUpdateNutritional) | **PUT** /Nutritional/{nutritionalId} | 


<a name="nutritionalCreateNutritional"></a>
# **nutritionalCreateNutritional**
> Object nutritionalCreateNutritional(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.NutritionalApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

NutritionalApi apiInstance = new NutritionalApi();
NutritionalDto dto = new NutritionalDto(); // NutritionalDto | 
try {
    Object result = apiInstance.nutritionalCreateNutritional(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling NutritionalApi#nutritionalCreateNutritional");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**NutritionalDto**](NutritionalDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="nutritionalDeleteNutritional"></a>
# **nutritionalDeleteNutritional**
> Object nutritionalDeleteNutritional(nutritionalId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.NutritionalApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

NutritionalApi apiInstance = new NutritionalApi();
Integer nutritionalId = 56; // Integer | 
try {
    Object result = apiInstance.nutritionalDeleteNutritional(nutritionalId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling NutritionalApi#nutritionalDeleteNutritional");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nutritionalId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="nutritionalExistsNutritional"></a>
# **nutritionalExistsNutritional**
> Object nutritionalExistsNutritional(nutritionalId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.NutritionalApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

NutritionalApi apiInstance = new NutritionalApi();
Integer nutritionalId = 56; // Integer | 
try {
    Object result = apiInstance.nutritionalExistsNutritional(nutritionalId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling NutritionalApi#nutritionalExistsNutritional");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nutritionalId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="nutritionalGetNutritional"></a>
# **nutritionalGetNutritional**
> Object nutritionalGetNutritional(nutritionalId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.NutritionalApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

NutritionalApi apiInstance = new NutritionalApi();
Integer nutritionalId = 56; // Integer | 
try {
    Object result = apiInstance.nutritionalGetNutritional(nutritionalId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling NutritionalApi#nutritionalGetNutritional");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nutritionalId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="nutritionalGetNutritionals"></a>
# **nutritionalGetNutritionals**
> Object nutritionalGetNutritionals(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.NutritionalApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

NutritionalApi apiInstance = new NutritionalApi();
GetFilteredNutritionalQuery query = new GetFilteredNutritionalQuery(); // GetFilteredNutritionalQuery | 
try {
    Object result = apiInstance.nutritionalGetNutritionals(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling NutritionalApi#nutritionalGetNutritionals");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredNutritionalQuery**](GetFilteredNutritionalQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="nutritionalUpdateNutritional"></a>
# **nutritionalUpdateNutritional**
> Object nutritionalUpdateNutritional(nutritionalId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.NutritionalApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

NutritionalApi apiInstance = new NutritionalApi();
Integer nutritionalId = 56; // Integer | 
NutritionalDto dto = new NutritionalDto(); // NutritionalDto | 
try {
    Object result = apiInstance.nutritionalUpdateNutritional(nutritionalId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling NutritionalApi#nutritionalUpdateNutritional");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nutritionalId** | **Integer**|  |
 **dto** | [**NutritionalDto**](NutritionalDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

