
# LineTagNameDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tagId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



