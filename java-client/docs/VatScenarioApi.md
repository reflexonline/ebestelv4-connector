# VatScenarioApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**vatScenarioCreateVatScenario**](VatScenarioApi.md#vatScenarioCreateVatScenario) | **POST** /VatScenario | 
[**vatScenarioDeleteVatScenario**](VatScenarioApi.md#vatScenarioDeleteVatScenario) | **DELETE** /VatScenario/{scenarioId} | 
[**vatScenarioExistsVatScenario**](VatScenarioApi.md#vatScenarioExistsVatScenario) | **GET** /VatScenario/exists/{scenarioId} | 
[**vatScenarioGetVatScenario**](VatScenarioApi.md#vatScenarioGetVatScenario) | **GET** /VatScenario/{scenarioId} | 
[**vatScenarioGetVatScenarios**](VatScenarioApi.md#vatScenarioGetVatScenarios) | **POST** /VatScenario/search | 
[**vatScenarioUpdateVatScenario**](VatScenarioApi.md#vatScenarioUpdateVatScenario) | **PUT** /VatScenario/{scenarioId} | 


<a name="vatScenarioCreateVatScenario"></a>
# **vatScenarioCreateVatScenario**
> Object vatScenarioCreateVatScenario(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VatScenarioApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

VatScenarioApi apiInstance = new VatScenarioApi();
VatScenarioDto dto = new VatScenarioDto(); // VatScenarioDto | 
try {
    Object result = apiInstance.vatScenarioCreateVatScenario(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VatScenarioApi#vatScenarioCreateVatScenario");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**VatScenarioDto**](VatScenarioDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="vatScenarioDeleteVatScenario"></a>
# **vatScenarioDeleteVatScenario**
> Object vatScenarioDeleteVatScenario(scenarioId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VatScenarioApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

VatScenarioApi apiInstance = new VatScenarioApi();
Integer scenarioId = 56; // Integer | 
try {
    Object result = apiInstance.vatScenarioDeleteVatScenario(scenarioId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VatScenarioApi#vatScenarioDeleteVatScenario");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scenarioId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="vatScenarioExistsVatScenario"></a>
# **vatScenarioExistsVatScenario**
> Object vatScenarioExistsVatScenario(scenarioId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VatScenarioApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

VatScenarioApi apiInstance = new VatScenarioApi();
Integer scenarioId = 56; // Integer | 
try {
    Object result = apiInstance.vatScenarioExistsVatScenario(scenarioId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VatScenarioApi#vatScenarioExistsVatScenario");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scenarioId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="vatScenarioGetVatScenario"></a>
# **vatScenarioGetVatScenario**
> Object vatScenarioGetVatScenario(scenarioId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VatScenarioApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

VatScenarioApi apiInstance = new VatScenarioApi();
Integer scenarioId = 56; // Integer | 
try {
    Object result = apiInstance.vatScenarioGetVatScenario(scenarioId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VatScenarioApi#vatScenarioGetVatScenario");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scenarioId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="vatScenarioGetVatScenarios"></a>
# **vatScenarioGetVatScenarios**
> Object vatScenarioGetVatScenarios(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VatScenarioApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

VatScenarioApi apiInstance = new VatScenarioApi();
GetFilteredVatScenarioQuery query = new GetFilteredVatScenarioQuery(); // GetFilteredVatScenarioQuery | 
try {
    Object result = apiInstance.vatScenarioGetVatScenarios(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VatScenarioApi#vatScenarioGetVatScenarios");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredVatScenarioQuery**](GetFilteredVatScenarioQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="vatScenarioUpdateVatScenario"></a>
# **vatScenarioUpdateVatScenario**
> Object vatScenarioUpdateVatScenario(scenarioId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VatScenarioApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

VatScenarioApi apiInstance = new VatScenarioApi();
Integer scenarioId = 56; // Integer | 
VatScenarioDto dto = new VatScenarioDto(); // VatScenarioDto | 
try {
    Object result = apiInstance.vatScenarioUpdateVatScenario(scenarioId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VatScenarioApi#vatScenarioUpdateVatScenario");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scenarioId** | **Integer**|  |
 **dto** | [**VatScenarioDto**](VatScenarioDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

