
# SaleInvoiceStatusDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoiceStatusId** | **Integer** |  | 
**isReasonMandatory** | **Boolean** |  |  [optional]
**isPaymentNoticeRequired** | **Boolean** |  |  [optional]
**isPaymentFulfilled** | **Integer** |  |  [optional]
**endDateMandatory** | **Boolean** |  |  [optional]
**callMandatory** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



