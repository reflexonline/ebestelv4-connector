
# GetFilteredProductionTaskInputSubLineQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderIdMinValue** | **Integer** |  |  [optional]
**orderIdMaxValue** | **Integer** |  |  [optional]
**orderLineIdMinValue** | **Integer** |  |  [optional]
**orderLineIdMaxValue** | **Integer** |  |  [optional]
**subLineIdMinValue** | **Integer** |  |  [optional]
**subLineIdMaxValue** | **Integer** |  |  [optional]
**chargeIdMinValue** | **Integer** |  |  [optional]
**chargeIdMaxValue** | **Integer** |  |  [optional]
**vrdpartijMinValue** | **Integer** |  |  [optional]
**vrdpartijMaxValue** | **Integer** |  |  [optional]
**plannedStepIdMinValue** | **Integer** |  |  [optional]
**plannedStepIdMaxValue** | **Integer** |  |  [optional]



