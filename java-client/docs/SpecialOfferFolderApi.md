# SpecialOfferFolderApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**specialOfferFolderCreateSpecialOfferFolder**](SpecialOfferFolderApi.md#specialOfferFolderCreateSpecialOfferFolder) | **POST** /SpecialOfferFolder | 
[**specialOfferFolderDeleteSpecialOfferFolder**](SpecialOfferFolderApi.md#specialOfferFolderDeleteSpecialOfferFolder) | **DELETE** /SpecialOfferFolder/{specialOfferId} | 
[**specialOfferFolderExistsSpecialOfferFolder**](SpecialOfferFolderApi.md#specialOfferFolderExistsSpecialOfferFolder) | **GET** /SpecialOfferFolder/exists/{specialOfferId} | 
[**specialOfferFolderGetSpecialOfferFolder**](SpecialOfferFolderApi.md#specialOfferFolderGetSpecialOfferFolder) | **GET** /SpecialOfferFolder/{specialOfferId} | 
[**specialOfferFolderGetSpecialOfferFolders**](SpecialOfferFolderApi.md#specialOfferFolderGetSpecialOfferFolders) | **POST** /SpecialOfferFolder/search | 
[**specialOfferFolderUpdateSpecialOfferFolder**](SpecialOfferFolderApi.md#specialOfferFolderUpdateSpecialOfferFolder) | **PUT** /SpecialOfferFolder/{specialOfferId} | 


<a name="specialOfferFolderCreateSpecialOfferFolder"></a>
# **specialOfferFolderCreateSpecialOfferFolder**
> Object specialOfferFolderCreateSpecialOfferFolder(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SpecialOfferFolderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SpecialOfferFolderApi apiInstance = new SpecialOfferFolderApi();
SpecialOfferFolderDto dto = new SpecialOfferFolderDto(); // SpecialOfferFolderDto | 
try {
    Object result = apiInstance.specialOfferFolderCreateSpecialOfferFolder(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SpecialOfferFolderApi#specialOfferFolderCreateSpecialOfferFolder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SpecialOfferFolderDto**](SpecialOfferFolderDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="specialOfferFolderDeleteSpecialOfferFolder"></a>
# **specialOfferFolderDeleteSpecialOfferFolder**
> Object specialOfferFolderDeleteSpecialOfferFolder(specialOfferId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SpecialOfferFolderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SpecialOfferFolderApi apiInstance = new SpecialOfferFolderApi();
Integer specialOfferId = 56; // Integer | 
try {
    Object result = apiInstance.specialOfferFolderDeleteSpecialOfferFolder(specialOfferId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SpecialOfferFolderApi#specialOfferFolderDeleteSpecialOfferFolder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **specialOfferId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="specialOfferFolderExistsSpecialOfferFolder"></a>
# **specialOfferFolderExistsSpecialOfferFolder**
> Object specialOfferFolderExistsSpecialOfferFolder(specialOfferId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SpecialOfferFolderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SpecialOfferFolderApi apiInstance = new SpecialOfferFolderApi();
Integer specialOfferId = 56; // Integer | 
try {
    Object result = apiInstance.specialOfferFolderExistsSpecialOfferFolder(specialOfferId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SpecialOfferFolderApi#specialOfferFolderExistsSpecialOfferFolder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **specialOfferId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="specialOfferFolderGetSpecialOfferFolder"></a>
# **specialOfferFolderGetSpecialOfferFolder**
> Object specialOfferFolderGetSpecialOfferFolder(specialOfferId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SpecialOfferFolderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SpecialOfferFolderApi apiInstance = new SpecialOfferFolderApi();
Integer specialOfferId = 56; // Integer | 
try {
    Object result = apiInstance.specialOfferFolderGetSpecialOfferFolder(specialOfferId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SpecialOfferFolderApi#specialOfferFolderGetSpecialOfferFolder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **specialOfferId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="specialOfferFolderGetSpecialOfferFolders"></a>
# **specialOfferFolderGetSpecialOfferFolders**
> Object specialOfferFolderGetSpecialOfferFolders(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SpecialOfferFolderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SpecialOfferFolderApi apiInstance = new SpecialOfferFolderApi();
GetFilteredSpecialOfferFolderQuery query = new GetFilteredSpecialOfferFolderQuery(); // GetFilteredSpecialOfferFolderQuery | 
try {
    Object result = apiInstance.specialOfferFolderGetSpecialOfferFolders(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SpecialOfferFolderApi#specialOfferFolderGetSpecialOfferFolders");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSpecialOfferFolderQuery**](GetFilteredSpecialOfferFolderQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="specialOfferFolderUpdateSpecialOfferFolder"></a>
# **specialOfferFolderUpdateSpecialOfferFolder**
> Object specialOfferFolderUpdateSpecialOfferFolder(specialOfferId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SpecialOfferFolderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SpecialOfferFolderApi apiInstance = new SpecialOfferFolderApi();
Integer specialOfferId = 56; // Integer | 
SpecialOfferFolderDto dto = new SpecialOfferFolderDto(); // SpecialOfferFolderDto | 
try {
    Object result = apiInstance.specialOfferFolderUpdateSpecialOfferFolder(specialOfferId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SpecialOfferFolderApi#specialOfferFolderUpdateSpecialOfferFolder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **specialOfferId** | **Integer**|  |
 **dto** | [**SpecialOfferFolderDto**](SpecialOfferFolderDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

