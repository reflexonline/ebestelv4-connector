
# GetFilteredSubstituteGroupQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**substituteGroupIdMinValue** | **Integer** |  |  [optional]
**substituteGroupIdMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]



