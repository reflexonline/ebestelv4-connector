
# ManageInfoViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**localLoginProvider** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**logins** | [**List&lt;UserLoginInfoViewModel&gt;**](UserLoginInfoViewModel.md) |  |  [optional]
**externalLoginProviders** | [**List&lt;ExternalLoginViewModel&gt;**](ExternalLoginViewModel.md) |  |  [optional]



