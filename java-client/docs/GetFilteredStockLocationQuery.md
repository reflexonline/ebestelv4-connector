
# GetFilteredStockLocationQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stockLocationIdMinValue** | **Integer** |  |  [optional]
**stockLocationIdMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]
**multiBranchIdMinValue** | **Integer** |  |  [optional]
**multiBranchIdMaxValue** | **Integer** |  |  [optional]



