
# ProductionStepDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productionStepId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**descriptionShort** | **String** |  |  [optional]
**startStopBehaviour** | [**StartStopBehaviourEnum**](#StartStopBehaviourEnum) |  |  [optional]
**productionFlexFilterGroup** | **Integer** |  |  [optional]
**photoPath** | **String** |  |  [optional]
**userKey** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="StartStopBehaviourEnum"></a>
## Enum: StartStopBehaviourEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
MANUALLYSTARTANDSTOP | &quot;ManuallyStartAndStop&quot;
AUTOSTARTMANUALLYSTOP | &quot;AutostartManuallyStop&quot;
AUTOSTARTANDSTOP | &quot;AutostartAndStop&quot;



