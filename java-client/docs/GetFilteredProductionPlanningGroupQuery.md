
# GetFilteredProductionPlanningGroupQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**planningGroupIdMinValue** | **Integer** |  |  [optional]
**planningGroupIdMaxValue** | **Integer** |  |  [optional]
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]



