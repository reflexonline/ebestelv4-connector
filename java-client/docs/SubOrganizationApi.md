# SubOrganizationApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**subOrganizationCreateSubOrganization**](SubOrganizationApi.md#subOrganizationCreateSubOrganization) | **POST** /SubOrganization | 
[**subOrganizationDeleteSubOrganization**](SubOrganizationApi.md#subOrganizationDeleteSubOrganization) | **DELETE** /SubOrganization/{subOrganizationId} | 
[**subOrganizationExistsSubOrganization**](SubOrganizationApi.md#subOrganizationExistsSubOrganization) | **GET** /SubOrganization/exists/{subOrganizationId} | 
[**subOrganizationGetSubOrganization**](SubOrganizationApi.md#subOrganizationGetSubOrganization) | **GET** /SubOrganization/{subOrganizationId} | 
[**subOrganizationGetSubOrganizations**](SubOrganizationApi.md#subOrganizationGetSubOrganizations) | **POST** /SubOrganization/search | 
[**subOrganizationUpdateSubOrganization**](SubOrganizationApi.md#subOrganizationUpdateSubOrganization) | **PUT** /SubOrganization/{subOrganizationId} | 


<a name="subOrganizationCreateSubOrganization"></a>
# **subOrganizationCreateSubOrganization**
> Object subOrganizationCreateSubOrganization(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SubOrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SubOrganizationApi apiInstance = new SubOrganizationApi();
SubOrganizationDto dto = new SubOrganizationDto(); // SubOrganizationDto | 
try {
    Object result = apiInstance.subOrganizationCreateSubOrganization(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SubOrganizationApi#subOrganizationCreateSubOrganization");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SubOrganizationDto**](SubOrganizationDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="subOrganizationDeleteSubOrganization"></a>
# **subOrganizationDeleteSubOrganization**
> Object subOrganizationDeleteSubOrganization(subOrganizationId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SubOrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SubOrganizationApi apiInstance = new SubOrganizationApi();
Integer subOrganizationId = 56; // Integer | 
try {
    Object result = apiInstance.subOrganizationDeleteSubOrganization(subOrganizationId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SubOrganizationApi#subOrganizationDeleteSubOrganization");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subOrganizationId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="subOrganizationExistsSubOrganization"></a>
# **subOrganizationExistsSubOrganization**
> Object subOrganizationExistsSubOrganization(subOrganizationId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SubOrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SubOrganizationApi apiInstance = new SubOrganizationApi();
Integer subOrganizationId = 56; // Integer | 
try {
    Object result = apiInstance.subOrganizationExistsSubOrganization(subOrganizationId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SubOrganizationApi#subOrganizationExistsSubOrganization");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subOrganizationId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="subOrganizationGetSubOrganization"></a>
# **subOrganizationGetSubOrganization**
> Object subOrganizationGetSubOrganization(subOrganizationId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SubOrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SubOrganizationApi apiInstance = new SubOrganizationApi();
Integer subOrganizationId = 56; // Integer | 
try {
    Object result = apiInstance.subOrganizationGetSubOrganization(subOrganizationId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SubOrganizationApi#subOrganizationGetSubOrganization");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subOrganizationId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="subOrganizationGetSubOrganizations"></a>
# **subOrganizationGetSubOrganizations**
> Object subOrganizationGetSubOrganizations(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SubOrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SubOrganizationApi apiInstance = new SubOrganizationApi();
GetFilteredSubOrganizationQuery query = new GetFilteredSubOrganizationQuery(); // GetFilteredSubOrganizationQuery | 
try {
    Object result = apiInstance.subOrganizationGetSubOrganizations(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SubOrganizationApi#subOrganizationGetSubOrganizations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSubOrganizationQuery**](GetFilteredSubOrganizationQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="subOrganizationUpdateSubOrganization"></a>
# **subOrganizationUpdateSubOrganization**
> Object subOrganizationUpdateSubOrganization(subOrganizationId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SubOrganizationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SubOrganizationApi apiInstance = new SubOrganizationApi();
Integer subOrganizationId = 56; // Integer | 
SubOrganizationDto dto = new SubOrganizationDto(); // SubOrganizationDto | 
try {
    Object result = apiInstance.subOrganizationUpdateSubOrganization(subOrganizationId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SubOrganizationApi#subOrganizationUpdateSubOrganization");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subOrganizationId** | **Integer**|  |
 **dto** | [**SubOrganizationDto**](SubOrganizationDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

