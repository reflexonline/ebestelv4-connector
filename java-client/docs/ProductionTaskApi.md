# ProductionTaskApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productionTaskExistsProductionTask**](ProductionTaskApi.md#productionTaskExistsProductionTask) | **GET** /ProductionTask/exists/{productionTaskId} | 
[**productionTaskGetProductionTask**](ProductionTaskApi.md#productionTaskGetProductionTask) | **GET** /ProductionTask/{productionTaskId} | 
[**productionTaskGetProductionTaskInputLines**](ProductionTaskApi.md#productionTaskGetProductionTaskInputLines) | **POST** /ProductionTask/{productionTaskId}/InputLines | 
[**productionTaskGetProductionTaskOutputLines**](ProductionTaskApi.md#productionTaskGetProductionTaskOutputLines) | **GET** /ProductionTask/{productionTaskId}/OutputLines | 
[**productionTaskGetProductionTasks**](ProductionTaskApi.md#productionTaskGetProductionTasks) | **POST** /ProductionTask/search | 
[**productionTaskMakeProductionTaskInputLineRegistration**](ProductionTaskApi.md#productionTaskMakeProductionTaskInputLineRegistration) | **POST** /ProductionTask/{productionTaskId}/InputLine/{lineId}/MakeRegistration | 
[**productionTaskMakeProductionTaskOutputLineRegistration**](ProductionTaskApi.md#productionTaskMakeProductionTaskOutputLineRegistration) | **POST** /ProductionTask/{productionTaskId}/OutputLine/{lineId}/MakeRegistration | 


<a name="productionTaskExistsProductionTask"></a>
# **productionTaskExistsProductionTask**
> Object productionTaskExistsProductionTask(productionTaskId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskApi apiInstance = new ProductionTaskApi();
Integer productionTaskId = 56; // Integer | 
try {
    Object result = apiInstance.productionTaskExistsProductionTask(productionTaskId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskApi#productionTaskExistsProductionTask");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionTaskId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionTaskGetProductionTask"></a>
# **productionTaskGetProductionTask**
> Object productionTaskGetProductionTask(productionTaskId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskApi apiInstance = new ProductionTaskApi();
Integer productionTaskId = 56; // Integer | 
try {
    Object result = apiInstance.productionTaskGetProductionTask(productionTaskId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskApi#productionTaskGetProductionTask");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionTaskId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionTaskGetProductionTaskInputLines"></a>
# **productionTaskGetProductionTaskInputLines**
> Object productionTaskGetProductionTaskInputLines(productionTaskId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskApi apiInstance = new ProductionTaskApi();
Integer productionTaskId = 56; // Integer | 
try {
    Object result = apiInstance.productionTaskGetProductionTaskInputLines(productionTaskId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskApi#productionTaskGetProductionTaskInputLines");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionTaskId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionTaskGetProductionTaskOutputLines"></a>
# **productionTaskGetProductionTaskOutputLines**
> Object productionTaskGetProductionTaskOutputLines(productionTaskId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskApi apiInstance = new ProductionTaskApi();
Integer productionTaskId = 56; // Integer | 
try {
    Object result = apiInstance.productionTaskGetProductionTaskOutputLines(productionTaskId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskApi#productionTaskGetProductionTaskOutputLines");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionTaskId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productionTaskGetProductionTasks"></a>
# **productionTaskGetProductionTasks**
> Object productionTaskGetProductionTasks(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskApi apiInstance = new ProductionTaskApi();
GetFilteredProductionTaskQuery query = new GetFilteredProductionTaskQuery(); // GetFilteredProductionTaskQuery | 
try {
    Object result = apiInstance.productionTaskGetProductionTasks(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskApi#productionTaskGetProductionTasks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductionTaskQuery**](GetFilteredProductionTaskQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productionTaskMakeProductionTaskInputLineRegistration"></a>
# **productionTaskMakeProductionTaskInputLineRegistration**
> Object productionTaskMakeProductionTaskInputLineRegistration(productionTaskId, lineId, registration)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskApi apiInstance = new ProductionTaskApi();
Integer productionTaskId = 56; // Integer | 
Integer lineId = 56; // Integer | 
RegistrationDto registration = new RegistrationDto(); // RegistrationDto | 
try {
    Object result = apiInstance.productionTaskMakeProductionTaskInputLineRegistration(productionTaskId, lineId, registration);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskApi#productionTaskMakeProductionTaskInputLineRegistration");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionTaskId** | **Integer**|  |
 **lineId** | **Integer**|  |
 **registration** | [**RegistrationDto**](RegistrationDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productionTaskMakeProductionTaskOutputLineRegistration"></a>
# **productionTaskMakeProductionTaskOutputLineRegistration**
> Object productionTaskMakeProductionTaskOutputLineRegistration(productionTaskId, lineId, registration)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductionTaskApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductionTaskApi apiInstance = new ProductionTaskApi();
Integer productionTaskId = 56; // Integer | 
Integer lineId = 56; // Integer | 
RegistrationDto registration = new RegistrationDto(); // RegistrationDto | 
try {
    Object result = apiInstance.productionTaskMakeProductionTaskOutputLineRegistration(productionTaskId, lineId, registration);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductionTaskApi#productionTaskMakeProductionTaskOutputLineRegistration");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productionTaskId** | **Integer**|  |
 **lineId** | **Integer**|  |
 **registration** | [**RegistrationDto**](RegistrationDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

