
# UserInfoViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  |  [optional]
**hasRegistered** | **Boolean** |  |  [optional]
**loginProvider** | **String** |  |  [optional]



