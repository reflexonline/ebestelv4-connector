
# GetFilteredProductionTaskQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**statusMinValue** | [**StatusMinValueEnum**](#StatusMinValueEnum) |  |  [optional]
**statusMaxValue** | [**StatusMaxValueEnum**](#StatusMaxValueEnum) |  |  [optional]
**recipeGroupIdMinValue** | **Integer** |  |  [optional]
**recipeGroupIdMaxValue** | **Integer** |  |  [optional]
**productionTaskIdMinValue** | **Integer** |  |  [optional]
**productionTaskIdMaxValue** | **Integer** |  |  [optional]
**currentProductionStepIdMinValue** | **Integer** |  |  [optional]
**currentProductionStepIdMaxValue** | **Integer** |  |  [optional]
**partijMinValue** | **Integer** |  |  [optional]
**partijMaxValue** | **Integer** |  |  [optional]
**startDateMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**startDateMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**endDateMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**endDateMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**bestBeforeDateMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**bestBeforeDateMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**articleIdMinValue** | **Integer** |  |  [optional]
**articleIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]
**producingMultiBranchIdMinValue** | **Integer** |  |  [optional]
**producingMultiBranchIdMaxValue** | **Integer** |  |  [optional]
**productionSequenceMinValue** | **Integer** |  |  [optional]
**productionSequenceMaxValue** | **Integer** |  |  [optional]
**useProductionPlanningMinValue** | **Boolean** |  |  [optional]
**useProductionPlanningMaxValue** | **Boolean** |  |  [optional]
**packingDescriptionMinValue** | **String** |  |  [optional]
**packingDescriptionMaxValue** | **String** |  |  [optional]
**closedByUserIdMinValue** | **Integer** |  |  [optional]
**closedByUserIdMaxValue** | **Integer** |  |  [optional]


<a name="StatusMinValueEnum"></a>
## Enum: StatusMinValueEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
ACTIVE | &quot;Active&quot;
HISTORIC | &quot;Historic&quot;


<a name="StatusMaxValueEnum"></a>
## Enum: StatusMaxValueEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
ACTIVE | &quot;Active&quot;
HISTORIC | &quot;Historic&quot;



