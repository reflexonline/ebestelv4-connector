# DeliveryMethodApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deliveryMethodCreateDeliveryMethod**](DeliveryMethodApi.md#deliveryMethodCreateDeliveryMethod) | **POST** /DeliveryMethod | 
[**deliveryMethodDeleteDeliveryMethod**](DeliveryMethodApi.md#deliveryMethodDeleteDeliveryMethod) | **DELETE** /DeliveryMethod/{deliveryMethodId} | 
[**deliveryMethodExistsDeliveryMethod**](DeliveryMethodApi.md#deliveryMethodExistsDeliveryMethod) | **GET** /DeliveryMethod/exists/{deliveryMethodId} | 
[**deliveryMethodGetDeliveryMethod**](DeliveryMethodApi.md#deliveryMethodGetDeliveryMethod) | **GET** /DeliveryMethod/{deliveryMethodId} | 
[**deliveryMethodGetDeliveryMethods**](DeliveryMethodApi.md#deliveryMethodGetDeliveryMethods) | **POST** /DeliveryMethod/search | 
[**deliveryMethodUpdateDeliveryMethod**](DeliveryMethodApi.md#deliveryMethodUpdateDeliveryMethod) | **PUT** /DeliveryMethod/{deliveryMethodId} | 


<a name="deliveryMethodCreateDeliveryMethod"></a>
# **deliveryMethodCreateDeliveryMethod**
> Object deliveryMethodCreateDeliveryMethod(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DeliveryMethodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DeliveryMethodApi apiInstance = new DeliveryMethodApi();
DeliveryMethodDto dto = new DeliveryMethodDto(); // DeliveryMethodDto | 
try {
    Object result = apiInstance.deliveryMethodCreateDeliveryMethod(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeliveryMethodApi#deliveryMethodCreateDeliveryMethod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**DeliveryMethodDto**](DeliveryMethodDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="deliveryMethodDeleteDeliveryMethod"></a>
# **deliveryMethodDeleteDeliveryMethod**
> Object deliveryMethodDeleteDeliveryMethod(deliveryMethodId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DeliveryMethodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DeliveryMethodApi apiInstance = new DeliveryMethodApi();
Integer deliveryMethodId = 56; // Integer | 
try {
    Object result = apiInstance.deliveryMethodDeleteDeliveryMethod(deliveryMethodId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeliveryMethodApi#deliveryMethodDeleteDeliveryMethod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deliveryMethodId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="deliveryMethodExistsDeliveryMethod"></a>
# **deliveryMethodExistsDeliveryMethod**
> Object deliveryMethodExistsDeliveryMethod(deliveryMethodId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DeliveryMethodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DeliveryMethodApi apiInstance = new DeliveryMethodApi();
Integer deliveryMethodId = 56; // Integer | 
try {
    Object result = apiInstance.deliveryMethodExistsDeliveryMethod(deliveryMethodId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeliveryMethodApi#deliveryMethodExistsDeliveryMethod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deliveryMethodId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="deliveryMethodGetDeliveryMethod"></a>
# **deliveryMethodGetDeliveryMethod**
> Object deliveryMethodGetDeliveryMethod(deliveryMethodId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DeliveryMethodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DeliveryMethodApi apiInstance = new DeliveryMethodApi();
Integer deliveryMethodId = 56; // Integer | 
try {
    Object result = apiInstance.deliveryMethodGetDeliveryMethod(deliveryMethodId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeliveryMethodApi#deliveryMethodGetDeliveryMethod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deliveryMethodId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="deliveryMethodGetDeliveryMethods"></a>
# **deliveryMethodGetDeliveryMethods**
> Object deliveryMethodGetDeliveryMethods(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DeliveryMethodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DeliveryMethodApi apiInstance = new DeliveryMethodApi();
GetFilteredDeliveryMethodQuery query = new GetFilteredDeliveryMethodQuery(); // GetFilteredDeliveryMethodQuery | 
try {
    Object result = apiInstance.deliveryMethodGetDeliveryMethods(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeliveryMethodApi#deliveryMethodGetDeliveryMethods");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredDeliveryMethodQuery**](GetFilteredDeliveryMethodQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="deliveryMethodUpdateDeliveryMethod"></a>
# **deliveryMethodUpdateDeliveryMethod**
> Object deliveryMethodUpdateDeliveryMethod(deliveryMethodId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.DeliveryMethodApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

DeliveryMethodApi apiInstance = new DeliveryMethodApi();
Integer deliveryMethodId = 56; // Integer | 
DeliveryMethodDto dto = new DeliveryMethodDto(); // DeliveryMethodDto | 
try {
    Object result = apiInstance.deliveryMethodUpdateDeliveryMethod(deliveryMethodId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeliveryMethodApi#deliveryMethodUpdateDeliveryMethod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deliveryMethodId** | **Integer**|  |
 **dto** | [**DeliveryMethodDto**](DeliveryMethodDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

