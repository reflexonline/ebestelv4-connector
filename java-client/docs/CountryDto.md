
# CountryDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isoCode** | **String** |  | 
**countryId** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**isEuropeanUnionCountry** | **Boolean** |  |  [optional]
**countryCustoms** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



