
# PurchaseOrderPatternLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierId** | **Integer** |  | 
**list** | **Integer** |  | 
**line** | **Integer** |  | 
**purchaseArticle** | **Integer** |  |  [optional]
**portion** | **Float** |  |  [optional]
**text** | **String** |  |  [optional]
**amount1** | **Float** |  |  [optional]
**weight1** | **Float** |  |  [optional]
**creationDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



