# AllergenApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**allergenCreateAllergen**](AllergenApi.md#allergenCreateAllergen) | **POST** /Allergen | 
[**allergenDeleteAllergen**](AllergenApi.md#allergenDeleteAllergen) | **DELETE** /Allergen/{allergenId} | 
[**allergenExistsAllergen**](AllergenApi.md#allergenExistsAllergen) | **GET** /Allergen/exists/{allergenId} | 
[**allergenGetAllergen**](AllergenApi.md#allergenGetAllergen) | **GET** /Allergen/{allergenId} | 
[**allergenGetAllergens**](AllergenApi.md#allergenGetAllergens) | **POST** /Allergen/search | 
[**allergenUpdateAllergen**](AllergenApi.md#allergenUpdateAllergen) | **PUT** /Allergen/{allergenId} | 


<a name="allergenCreateAllergen"></a>
# **allergenCreateAllergen**
> Object allergenCreateAllergen(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AllergenApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AllergenApi apiInstance = new AllergenApi();
AllergenDto dto = new AllergenDto(); // AllergenDto | 
try {
    Object result = apiInstance.allergenCreateAllergen(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllergenApi#allergenCreateAllergen");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**AllergenDto**](AllergenDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="allergenDeleteAllergen"></a>
# **allergenDeleteAllergen**
> Object allergenDeleteAllergen(allergenId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AllergenApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AllergenApi apiInstance = new AllergenApi();
Integer allergenId = 56; // Integer | 
try {
    Object result = apiInstance.allergenDeleteAllergen(allergenId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllergenApi#allergenDeleteAllergen");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allergenId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="allergenExistsAllergen"></a>
# **allergenExistsAllergen**
> Object allergenExistsAllergen(allergenId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AllergenApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AllergenApi apiInstance = new AllergenApi();
Integer allergenId = 56; // Integer | 
try {
    Object result = apiInstance.allergenExistsAllergen(allergenId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllergenApi#allergenExistsAllergen");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allergenId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="allergenGetAllergen"></a>
# **allergenGetAllergen**
> Object allergenGetAllergen(allergenId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AllergenApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AllergenApi apiInstance = new AllergenApi();
Integer allergenId = 56; // Integer | 
try {
    Object result = apiInstance.allergenGetAllergen(allergenId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllergenApi#allergenGetAllergen");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allergenId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="allergenGetAllergens"></a>
# **allergenGetAllergens**
> Object allergenGetAllergens(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AllergenApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AllergenApi apiInstance = new AllergenApi();
GetFilteredAllergenQuery query = new GetFilteredAllergenQuery(); // GetFilteredAllergenQuery | 
try {
    Object result = apiInstance.allergenGetAllergens(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllergenApi#allergenGetAllergens");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredAllergenQuery**](GetFilteredAllergenQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="allergenUpdateAllergen"></a>
# **allergenUpdateAllergen**
> Object allergenUpdateAllergen(allergenId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AllergenApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

AllergenApi apiInstance = new AllergenApi();
Integer allergenId = 56; // Integer | 
AllergenDto dto = new AllergenDto(); // AllergenDto | 
try {
    Object result = apiInstance.allergenUpdateAllergen(allergenId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllergenApi#allergenUpdateAllergen");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allergenId** | **Integer**|  |
 **dto** | [**AllergenDto**](AllergenDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

