
# GetFilteredMarketSectorQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketSectorIdMinValue** | **Integer** |  |  [optional]
**marketSectorIdMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]



