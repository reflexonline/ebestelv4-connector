
# SaleArticleAllergenDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allergen** | **Integer** |  | 
**manual** | **Boolean** |  |  [optional]
**print** | **Boolean** |  |  [optional]
**traces** | **Boolean** |  |  [optional]
**threshold** | **Double** |  |  [optional]
**value** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



