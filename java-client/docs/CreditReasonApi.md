# CreditReasonApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**creditReasonCreateCreditReason**](CreditReasonApi.md#creditReasonCreateCreditReason) | **POST** /CreditReason | 
[**creditReasonDeleteCreditReason**](CreditReasonApi.md#creditReasonDeleteCreditReason) | **DELETE** /CreditReason/{creditReasonId} | 
[**creditReasonExistsCreditReason**](CreditReasonApi.md#creditReasonExistsCreditReason) | **GET** /CreditReason/exists/{creditReasonId} | 
[**creditReasonGetCreditReason**](CreditReasonApi.md#creditReasonGetCreditReason) | **GET** /CreditReason/{creditReasonId} | 
[**creditReasonGetCreditReasons**](CreditReasonApi.md#creditReasonGetCreditReasons) | **POST** /CreditReason/search | 
[**creditReasonUpdateCreditReason**](CreditReasonApi.md#creditReasonUpdateCreditReason) | **PUT** /CreditReason/{creditReasonId} | 


<a name="creditReasonCreateCreditReason"></a>
# **creditReasonCreateCreditReason**
> Object creditReasonCreateCreditReason(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CreditReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CreditReasonApi apiInstance = new CreditReasonApi();
CreditReasonDto dto = new CreditReasonDto(); // CreditReasonDto | 
try {
    Object result = apiInstance.creditReasonCreateCreditReason(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CreditReasonApi#creditReasonCreateCreditReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**CreditReasonDto**](CreditReasonDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="creditReasonDeleteCreditReason"></a>
# **creditReasonDeleteCreditReason**
> Object creditReasonDeleteCreditReason(creditReasonId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CreditReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CreditReasonApi apiInstance = new CreditReasonApi();
Integer creditReasonId = 56; // Integer | 
try {
    Object result = apiInstance.creditReasonDeleteCreditReason(creditReasonId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CreditReasonApi#creditReasonDeleteCreditReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **creditReasonId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="creditReasonExistsCreditReason"></a>
# **creditReasonExistsCreditReason**
> Object creditReasonExistsCreditReason(creditReasonId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CreditReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CreditReasonApi apiInstance = new CreditReasonApi();
Integer creditReasonId = 56; // Integer | 
try {
    Object result = apiInstance.creditReasonExistsCreditReason(creditReasonId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CreditReasonApi#creditReasonExistsCreditReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **creditReasonId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="creditReasonGetCreditReason"></a>
# **creditReasonGetCreditReason**
> Object creditReasonGetCreditReason(creditReasonId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CreditReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CreditReasonApi apiInstance = new CreditReasonApi();
Integer creditReasonId = 56; // Integer | 
try {
    Object result = apiInstance.creditReasonGetCreditReason(creditReasonId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CreditReasonApi#creditReasonGetCreditReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **creditReasonId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="creditReasonGetCreditReasons"></a>
# **creditReasonGetCreditReasons**
> Object creditReasonGetCreditReasons(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CreditReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CreditReasonApi apiInstance = new CreditReasonApi();
GetFilteredCreditReasonQuery query = new GetFilteredCreditReasonQuery(); // GetFilteredCreditReasonQuery | 
try {
    Object result = apiInstance.creditReasonGetCreditReasons(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CreditReasonApi#creditReasonGetCreditReasons");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredCreditReasonQuery**](GetFilteredCreditReasonQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="creditReasonUpdateCreditReason"></a>
# **creditReasonUpdateCreditReason**
> Object creditReasonUpdateCreditReason(creditReasonId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CreditReasonApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CreditReasonApi apiInstance = new CreditReasonApi();
Integer creditReasonId = 56; // Integer | 
CreditReasonDto dto = new CreditReasonDto(); // CreditReasonDto | 
try {
    Object result = apiInstance.creditReasonUpdateCreditReason(creditReasonId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CreditReasonApi#creditReasonUpdateCreditReason");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **creditReasonId** | **Integer**|  |
 **dto** | [**CreditReasonDto**](CreditReasonDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

