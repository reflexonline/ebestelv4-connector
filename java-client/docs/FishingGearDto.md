
# FishingGearDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fishingGearId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**code** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



