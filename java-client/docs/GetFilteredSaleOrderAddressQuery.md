
# GetFilteredSaleOrderAddressQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerIdMinValue** | **Integer** |  |  [optional]
**customerIdMaxValue** | **Integer** |  |  [optional]
**orderNumberMinValue** | **Integer** |  |  [optional]
**orderNumberMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]
**deliveryMethodIdMinValue** | **Integer** |  |  [optional]
**deliveryMethodIdMaxValue** | **Integer** |  |  [optional]
**transporterFreshIdMinValue** | **Integer** |  |  [optional]
**transporterFreshIdMaxValue** | **Integer** |  |  [optional]



