# MaterialApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**materialCreateMaterial**](MaterialApi.md#materialCreateMaterial) | **POST** /Material | 
[**materialDeleteMaterial**](MaterialApi.md#materialDeleteMaterial) | **DELETE** /Material/{number} | 
[**materialExistsMaterial**](MaterialApi.md#materialExistsMaterial) | **GET** /Material/exists/{number} | 
[**materialGetMaterial**](MaterialApi.md#materialGetMaterial) | **GET** /Material/{number} | 
[**materialGetMaterials**](MaterialApi.md#materialGetMaterials) | **POST** /Material/search | 
[**materialUpdateMaterial**](MaterialApi.md#materialUpdateMaterial) | **PUT** /Material/{number} | 


<a name="materialCreateMaterial"></a>
# **materialCreateMaterial**
> Object materialCreateMaterial(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MaterialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MaterialApi apiInstance = new MaterialApi();
MaterialDto dto = new MaterialDto(); // MaterialDto | 
try {
    Object result = apiInstance.materialCreateMaterial(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MaterialApi#materialCreateMaterial");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**MaterialDto**](MaterialDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="materialDeleteMaterial"></a>
# **materialDeleteMaterial**
> Object materialDeleteMaterial(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MaterialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MaterialApi apiInstance = new MaterialApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.materialDeleteMaterial(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MaterialApi#materialDeleteMaterial");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="materialExistsMaterial"></a>
# **materialExistsMaterial**
> Object materialExistsMaterial(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MaterialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MaterialApi apiInstance = new MaterialApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.materialExistsMaterial(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MaterialApi#materialExistsMaterial");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="materialGetMaterial"></a>
# **materialGetMaterial**
> Object materialGetMaterial(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MaterialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MaterialApi apiInstance = new MaterialApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.materialGetMaterial(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MaterialApi#materialGetMaterial");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="materialGetMaterials"></a>
# **materialGetMaterials**
> Object materialGetMaterials(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MaterialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MaterialApi apiInstance = new MaterialApi();
GetFilteredMaterialQuery query = new GetFilteredMaterialQuery(); // GetFilteredMaterialQuery | 
try {
    Object result = apiInstance.materialGetMaterials(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MaterialApi#materialGetMaterials");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredMaterialQuery**](GetFilteredMaterialQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="materialUpdateMaterial"></a>
# **materialUpdateMaterial**
> Object materialUpdateMaterial(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MaterialApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

MaterialApi apiInstance = new MaterialApi();
Integer number = 56; // Integer | 
MaterialDto dto = new MaterialDto(); // MaterialDto | 
try {
    Object result = apiInstance.materialUpdateMaterial(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MaterialApi#materialUpdateMaterial");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**MaterialDto**](MaterialDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

