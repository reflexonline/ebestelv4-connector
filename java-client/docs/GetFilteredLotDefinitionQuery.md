
# GetFilteredLotDefinitionQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lotDefinitionIdMinValue** | **Integer** |  |  [optional]
**lotDefinitionIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



