
# GetFilteredSupplierQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierIdMinValue** | **Integer** |  |  [optional]
**supplierIdMaxValue** | **Integer** |  |  [optional]
**searchNameMinValue** | **String** |  |  [optional]
**searchNameMaxValue** | **String** |  |  [optional]
**visitAddressPostcodeMinValue** | **String** |  |  [optional]
**visitAddressPostcodeMaxValue** | **String** |  |  [optional]
**visitAddressCityMinValue** | **String** |  |  [optional]
**visitAddressCityMaxValue** | **String** |  |  [optional]
**currencyCodeIdMinValue** | **Integer** |  |  [optional]
**currencyCodeIdMaxValue** | **Integer** |  |  [optional]
**taalCodeMinValue** | **Integer** |  |  [optional]
**taalCodeMaxValue** | **Integer** |  |  [optional]
**stockLocationToIdMinValue** | **Integer** |  |  [optional]
**stockLocationToIdMaxValue** | **Integer** |  |  [optional]
**defaultMultiBranchIdMinValue** | **Integer** |  |  [optional]
**defaultMultiBranchIdMaxValue** | **Integer** |  |  [optional]
**stockLocationFromIdMinValue** | **Integer** |  |  [optional]
**stockLocationFromIdMaxValue** | **Integer** |  |  [optional]
**dockIdMinValue** | **Integer** |  |  [optional]
**dockIdMaxValue** | **Integer** |  |  [optional]



