
# OrganizationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**organizationId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**deviatingPrinterNumberPickingList** | **Integer** |  |  [optional]
**pickingSlipLabelNumber** | **Integer** |  |  [optional]
**pickingSlipLabelPort** | **String** |  |  [optional]
**pickingSlipLabelQuantity** | **Integer** |  |  [optional]
**ediInvoiceEnvelopeSource** | **Integer** |  |  [optional]
**ediBuyerSource** | **Integer** |  |  [optional]
**ediInvoiceSource** | **Integer** |  |  [optional]
**ediDeliveryPointSource** | **Integer** |  |  [optional]
**allowEdiCrossDock** | **Integer** |  |  [optional]
**exportEdiUseArticlePackedPer** | **Boolean** |  |  [optional]
**ediPackingSlipEnvelopeSource** | **Integer** |  |  [optional]
**ediUltimateConsigneeSource** | **Integer** |  |  [optional]
**ediSsccExportType** | [**EdiSsccExportTypeEnum**](#EdiSsccExportTypeEnum) |  |  [optional]
**ssccLabelNumber** | **Integer** |  |  [optional]
**exportEdiCorrectionInvoice** | **Boolean** |  |  [optional]
**mergeExport** | **Integer** |  |  [optional]
**canExportEdiArticlesWithoutPrice** | **Boolean** |  |  [optional]
**exportEdiArticleDescription** | **Boolean** |  |  [optional]
**ediOrderReference** | **String** |  |  [optional]
**printProductionLabel** | **Integer** |  |  [optional]
**ediPackingSlipCopyEnvelopeSource** | **Integer** |  |  [optional]
**exportEdiMessageFormat** | [**ExportEdiMessageFormatEnum**](#ExportEdiMessageFormatEnum) |  |  [optional]
**exportEdiInvoiceMessageFormat** | **Integer** |  |  [optional]
**exportEdiPackingTaxFormat** | **Integer** |  |  [optional]
**exportEdiRegisteredAmountMethod** | **Integer** |  |  [optional]
**ssccMaximumNumberOfDifferentArticlesOnPallet** | **Integer** |  |  [optional]
**isDeliveryAppointmentRequired** | **Boolean** |  |  [optional]
**alternateSsccLabelNumberForPalletPreCreation** | **Integer** |  |  [optional]
**printSsccLabelAfterPalletRegistration** | **Boolean** |  |  [optional]
**ssccPrefix** | **String** |  |  [optional]
**ediEanSource** | **Integer** |  |  [optional]
**ediExportArticleId** | **Boolean** |  |  [optional]
**ediPackingSlipExportDirectory** | **String** |  |  [optional]
**ediInvoiceExportDirectory** | **String** |  |  [optional]
**ediRouteIsBasedOnDeliveryTime** | **Boolean** |  |  [optional]
**useSsccForOcc** | **Boolean** |  |  [optional]
**dockId** | **Integer** |  |  [optional]
**exportBatchData** | **Boolean** |  |  [optional]
**joinEdiOrders** | **Boolean** |  |  [optional]
**primaryEanOrderAdressForCrossDockOrder** | **Integer** |  |  [optional]
**secondaryEanOrderAdressForCrossDockOrder** | **Integer** |  |  [optional]
**primaryEanOrderAdressForNormalOrder** | **Integer** |  |  [optional]
**secondaryEanOrderAdressForNormalOrder** | **Integer** |  |  [optional]
**ediTransportMessageExportTemplatePath** | **String** |  |  [optional]
**ediTransportMessageExportPath** | **String** |  |  [optional]
**identificationCode** | **String** |  |  [optional]
**ediOrdNoIsRef** | **Boolean** |  |  [optional]
**exportPrices** | **Boolean** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="EdiSsccExportTypeEnum"></a>
## Enum: EdiSsccExportTypeEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
DONOTEXPORT | &quot;DoNotExport&quot;
HETEROGENEOUSPALLETS | &quot;HeterogeneousPallets&quot;
HOMOGENEOUSPALLETS | &quot;HomogeneousPallets&quot;
HETEROGENEOUSPALLETSWITHGRAICODE | &quot;HeterogeneousPalletsWithGraiCode&quot;
HOMOGENEOUSPALLETSWITHGRAICODE | &quot;HomogeneousPalletsWithGraiCode&quot;


<a name="ExportEdiMessageFormatEnum"></a>
## Enum: ExportEdiMessageFormatEnum
Name | Value
---- | -----
EDI93A | &quot;Edi93A&quot;
EDI01B | &quot;Edi01B&quot;
EDI96A_DELHAIZE | &quot;Edi96A_Delhaize&quot;
EDI96A | &quot;Edi96A&quot;
EDI96A_COLRUYT | &quot;Edi96A_Colruyt&quot;
EDI01B_COLRUYT | &quot;Edi01B_Colruyt&quot;
EDI01B_ALBERTHEIJN | &quot;Edi01B_AlbertHeijn&quot;
NONE | &quot;None&quot;



