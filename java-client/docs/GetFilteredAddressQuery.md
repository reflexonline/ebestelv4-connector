
# GetFilteredAddressQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addressIdMinValue** | **Integer** |  |  [optional]
**addressIdMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]
**postalCodeMinValue** | **String** |  |  [optional]
**postalCodeMaxValue** | **String** |  |  [optional]
**cityMinValue** | **String** |  |  [optional]
**cityMaxValue** | **String** |  |  [optional]
**isoCountryCodeMinValue** | **String** |  |  [optional]
**isoCountryCodeMaxValue** | **String** |  |  [optional]
**addressTypeMinValue** | [**AddressTypeMinValueEnum**](#AddressTypeMinValueEnum) |  |  [optional]
**addressTypeMaxValue** | [**AddressTypeMaxValueEnum**](#AddressTypeMaxValueEnum) |  |  [optional]


<a name="AddressTypeMinValueEnum"></a>
## Enum: AddressTypeMinValueEnum
Name | Value
---- | -----
ALTERNATIVEADDRESS | &quot;AlternativeAddress&quot;
ORDERTRANSPORTER | &quot;OrderTransporter&quot;
LOADADDRESS | &quot;LoadAddress&quot;


<a name="AddressTypeMaxValueEnum"></a>
## Enum: AddressTypeMaxValueEnum
Name | Value
---- | -----
ALTERNATIVEADDRESS | &quot;AlternativeAddress&quot;
ORDERTRANSPORTER | &quot;OrderTransporter&quot;
LOADADDRESS | &quot;LoadAddress&quot;



