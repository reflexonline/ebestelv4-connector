# SaleArticlePriceListAgreementApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleArticlePriceListAgreementCreateSaleArticlePriceListAgreement**](SaleArticlePriceListAgreementApi.md#saleArticlePriceListAgreementCreateSaleArticlePriceListAgreement) | **POST** /SaleArticlePriceListAgreement | 
[**saleArticlePriceListAgreementDeleteSaleArticlePriceListAgreement**](SaleArticlePriceListAgreementApi.md#saleArticlePriceListAgreementDeleteSaleArticlePriceListAgreement) | **DELETE** /SaleArticlePriceListAgreement/{priceListId}/{articleId} | 
[**saleArticlePriceListAgreementExistsSaleArticlePriceListAgreement**](SaleArticlePriceListAgreementApi.md#saleArticlePriceListAgreementExistsSaleArticlePriceListAgreement) | **GET** /SaleArticlePriceListAgreement/exists/{priceListId}/{articleId} | 
[**saleArticlePriceListAgreementGetSaleArticlePriceListAgreement**](SaleArticlePriceListAgreementApi.md#saleArticlePriceListAgreementGetSaleArticlePriceListAgreement) | **GET** /SaleArticlePriceListAgreement/{priceListId}/{articleId} | 
[**saleArticlePriceListAgreementGetSaleArticlePriceListAgreements**](SaleArticlePriceListAgreementApi.md#saleArticlePriceListAgreementGetSaleArticlePriceListAgreements) | **POST** /SaleArticlePriceListAgreement/search | 
[**saleArticlePriceListAgreementUpdateSaleArticlePriceListAgreement**](SaleArticlePriceListAgreementApi.md#saleArticlePriceListAgreementUpdateSaleArticlePriceListAgreement) | **PUT** /SaleArticlePriceListAgreement/{priceListId}/{articleId} | 


<a name="saleArticlePriceListAgreementCreateSaleArticlePriceListAgreement"></a>
# **saleArticlePriceListAgreementCreateSaleArticlePriceListAgreement**
> Object saleArticlePriceListAgreementCreateSaleArticlePriceListAgreement(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticlePriceListAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticlePriceListAgreementApi apiInstance = new SaleArticlePriceListAgreementApi();
SaleArticlePriceListAgreementDto dto = new SaleArticlePriceListAgreementDto(); // SaleArticlePriceListAgreementDto | 
try {
    Object result = apiInstance.saleArticlePriceListAgreementCreateSaleArticlePriceListAgreement(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticlePriceListAgreementApi#saleArticlePriceListAgreementCreateSaleArticlePriceListAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SaleArticlePriceListAgreementDto**](SaleArticlePriceListAgreementDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticlePriceListAgreementDeleteSaleArticlePriceListAgreement"></a>
# **saleArticlePriceListAgreementDeleteSaleArticlePriceListAgreement**
> Object saleArticlePriceListAgreementDeleteSaleArticlePriceListAgreement(priceListId, articleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticlePriceListAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticlePriceListAgreementApi apiInstance = new SaleArticlePriceListAgreementApi();
Integer priceListId = 56; // Integer | 
Integer articleId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticlePriceListAgreementDeleteSaleArticlePriceListAgreement(priceListId, articleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticlePriceListAgreementApi#saleArticlePriceListAgreementDeleteSaleArticlePriceListAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **priceListId** | **Integer**|  |
 **articleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticlePriceListAgreementExistsSaleArticlePriceListAgreement"></a>
# **saleArticlePriceListAgreementExistsSaleArticlePriceListAgreement**
> Object saleArticlePriceListAgreementExistsSaleArticlePriceListAgreement(priceListId, articleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticlePriceListAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticlePriceListAgreementApi apiInstance = new SaleArticlePriceListAgreementApi();
Integer priceListId = 56; // Integer | 
Integer articleId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticlePriceListAgreementExistsSaleArticlePriceListAgreement(priceListId, articleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticlePriceListAgreementApi#saleArticlePriceListAgreementExistsSaleArticlePriceListAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **priceListId** | **Integer**|  |
 **articleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticlePriceListAgreementGetSaleArticlePriceListAgreement"></a>
# **saleArticlePriceListAgreementGetSaleArticlePriceListAgreement**
> Object saleArticlePriceListAgreementGetSaleArticlePriceListAgreement(priceListId, articleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticlePriceListAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticlePriceListAgreementApi apiInstance = new SaleArticlePriceListAgreementApi();
Integer priceListId = 56; // Integer | 
Integer articleId = 56; // Integer | 
try {
    Object result = apiInstance.saleArticlePriceListAgreementGetSaleArticlePriceListAgreement(priceListId, articleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticlePriceListAgreementApi#saleArticlePriceListAgreementGetSaleArticlePriceListAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **priceListId** | **Integer**|  |
 **articleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleArticlePriceListAgreementGetSaleArticlePriceListAgreements"></a>
# **saleArticlePriceListAgreementGetSaleArticlePriceListAgreements**
> Object saleArticlePriceListAgreementGetSaleArticlePriceListAgreements(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticlePriceListAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticlePriceListAgreementApi apiInstance = new SaleArticlePriceListAgreementApi();
GetFilteredSaleArticlePriceListAgreementQuery query = new GetFilteredSaleArticlePriceListAgreementQuery(); // GetFilteredSaleArticlePriceListAgreementQuery | 
try {
    Object result = apiInstance.saleArticlePriceListAgreementGetSaleArticlePriceListAgreements(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticlePriceListAgreementApi#saleArticlePriceListAgreementGetSaleArticlePriceListAgreements");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSaleArticlePriceListAgreementQuery**](GetFilteredSaleArticlePriceListAgreementQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleArticlePriceListAgreementUpdateSaleArticlePriceListAgreement"></a>
# **saleArticlePriceListAgreementUpdateSaleArticlePriceListAgreement**
> Object saleArticlePriceListAgreementUpdateSaleArticlePriceListAgreement(priceListId, articleId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleArticlePriceListAgreementApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleArticlePriceListAgreementApi apiInstance = new SaleArticlePriceListAgreementApi();
Integer priceListId = 56; // Integer | 
Integer articleId = 56; // Integer | 
SaleArticlePriceListAgreementDto dto = new SaleArticlePriceListAgreementDto(); // SaleArticlePriceListAgreementDto | 
try {
    Object result = apiInstance.saleArticlePriceListAgreementUpdateSaleArticlePriceListAgreement(priceListId, articleId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleArticlePriceListAgreementApi#saleArticlePriceListAgreementUpdateSaleArticlePriceListAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **priceListId** | **Integer**|  |
 **articleId** | **Integer**|  |
 **dto** | [**SaleArticlePriceListAgreementDto**](SaleArticlePriceListAgreementDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

