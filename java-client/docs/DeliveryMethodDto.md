
# DeliveryMethodDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deliveryMethodId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



