# RecipeApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**recipeCreateProductionTask**](RecipeApi.md#recipeCreateProductionTask) | **POST** /Recipe/{recipeId}/{recipeDerivativeId}/{recipepackingId}/ProductionTask/Create | 
[**recipeCreateProductionTask_0**](RecipeApi.md#recipeCreateProductionTask_0) | **POST** /Recipe/{recipeId}/{recipeDerivativeId}/ProductionTask/Create | 
[**recipeCreateProductionTask_1**](RecipeApi.md#recipeCreateProductionTask_1) | **POST** /Recipe/{recipeId}/ProductionTask/Create | 
[**recipeCreateRecipe**](RecipeApi.md#recipeCreateRecipe) | **POST** /Recipe | 
[**recipeDeleteRecipe**](RecipeApi.md#recipeDeleteRecipe) | **DELETE** /Recipe/{recipeId}/{recipeDerivativeId} | 
[**recipeExistsRecipe**](RecipeApi.md#recipeExistsRecipe) | **GET** /Recipe/exists/{recipeId}/{recipeDerivativeId} | 
[**recipeGetRecipe**](RecipeApi.md#recipeGetRecipe) | **GET** /Recipe/{recipeId}/{recipeDerivativeId} | 
[**recipeGetRecipes**](RecipeApi.md#recipeGetRecipes) | **POST** /Recipe/search | 
[**recipeUpdateRecipe**](RecipeApi.md#recipeUpdateRecipe) | **PUT** /Recipe/{recipeId}/{recipeDerivativeId} | 


<a name="recipeCreateProductionTask"></a>
# **recipeCreateProductionTask**
> Object recipeCreateProductionTask(recipeId, recipeDerivativeId, recipepackingId, command)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeApi apiInstance = new RecipeApi();
Integer recipeId = 56; // Integer | 
Integer recipeDerivativeId = 56; // Integer | 
Integer recipepackingId = 56; // Integer | 
CreateProductionTaskCommand command = new CreateProductionTaskCommand(); // CreateProductionTaskCommand | 
try {
    Object result = apiInstance.recipeCreateProductionTask(recipeId, recipeDerivativeId, recipepackingId, command);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeApi#recipeCreateProductionTask");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipeId** | **Integer**|  |
 **recipeDerivativeId** | **Integer**|  |
 **recipepackingId** | **Integer**|  |
 **command** | [**CreateProductionTaskCommand**](CreateProductionTaskCommand.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="recipeCreateProductionTask_0"></a>
# **recipeCreateProductionTask_0**
> Object recipeCreateProductionTask_0(recipeId, recipeDerivativeId, command)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeApi apiInstance = new RecipeApi();
Integer recipeId = 56; // Integer | 
Integer recipeDerivativeId = 56; // Integer | 
CreateProductionTaskCommand command = new CreateProductionTaskCommand(); // CreateProductionTaskCommand | 
try {
    Object result = apiInstance.recipeCreateProductionTask_0(recipeId, recipeDerivativeId, command);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeApi#recipeCreateProductionTask_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipeId** | **Integer**|  |
 **recipeDerivativeId** | **Integer**|  |
 **command** | [**CreateProductionTaskCommand**](CreateProductionTaskCommand.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="recipeCreateProductionTask_1"></a>
# **recipeCreateProductionTask_1**
> Object recipeCreateProductionTask_1(recipeId, command)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeApi apiInstance = new RecipeApi();
Integer recipeId = 56; // Integer | 
CreateProductionTaskCommand command = new CreateProductionTaskCommand(); // CreateProductionTaskCommand | 
try {
    Object result = apiInstance.recipeCreateProductionTask_1(recipeId, command);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeApi#recipeCreateProductionTask_1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipeId** | **Integer**|  |
 **command** | [**CreateProductionTaskCommand**](CreateProductionTaskCommand.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="recipeCreateRecipe"></a>
# **recipeCreateRecipe**
> Object recipeCreateRecipe(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeApi apiInstance = new RecipeApi();
RecipeDto dto = new RecipeDto(); // RecipeDto | 
try {
    Object result = apiInstance.recipeCreateRecipe(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeApi#recipeCreateRecipe");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**RecipeDto**](RecipeDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="recipeDeleteRecipe"></a>
# **recipeDeleteRecipe**
> Object recipeDeleteRecipe(recipeId, recipeDerivativeId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeApi apiInstance = new RecipeApi();
Integer recipeId = 56; // Integer | 
Integer recipeDerivativeId = 56; // Integer | 
try {
    Object result = apiInstance.recipeDeleteRecipe(recipeId, recipeDerivativeId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeApi#recipeDeleteRecipe");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipeId** | **Integer**|  |
 **recipeDerivativeId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="recipeExistsRecipe"></a>
# **recipeExistsRecipe**
> Object recipeExistsRecipe(recipeId, recipeDerivativeId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeApi apiInstance = new RecipeApi();
Integer recipeId = 56; // Integer | 
Integer recipeDerivativeId = 56; // Integer | 
try {
    Object result = apiInstance.recipeExistsRecipe(recipeId, recipeDerivativeId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeApi#recipeExistsRecipe");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipeId** | **Integer**|  |
 **recipeDerivativeId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="recipeGetRecipe"></a>
# **recipeGetRecipe**
> Object recipeGetRecipe(recipeId, recipeDerivativeId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeApi apiInstance = new RecipeApi();
Integer recipeId = 56; // Integer | 
Integer recipeDerivativeId = 56; // Integer | 
try {
    Object result = apiInstance.recipeGetRecipe(recipeId, recipeDerivativeId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeApi#recipeGetRecipe");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipeId** | **Integer**|  |
 **recipeDerivativeId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="recipeGetRecipes"></a>
# **recipeGetRecipes**
> Object recipeGetRecipes(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeApi apiInstance = new RecipeApi();
GetFilteredRecipeQuery query = new GetFilteredRecipeQuery(); // GetFilteredRecipeQuery | 
try {
    Object result = apiInstance.recipeGetRecipes(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeApi#recipeGetRecipes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredRecipeQuery**](GetFilteredRecipeQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="recipeUpdateRecipe"></a>
# **recipeUpdateRecipe**
> Object recipeUpdateRecipe(recipeId, recipeDerivativeId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeApi apiInstance = new RecipeApi();
Integer recipeId = 56; // Integer | 
Integer recipeDerivativeId = 56; // Integer | 
RecipeDto dto = new RecipeDto(); // RecipeDto | 
try {
    Object result = apiInstance.recipeUpdateRecipe(recipeId, recipeDerivativeId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeApi#recipeUpdateRecipe");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipeId** | **Integer**|  |
 **recipeDerivativeId** | **Integer**|  |
 **dto** | [**RecipeDto**](RecipeDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

