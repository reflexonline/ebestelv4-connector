
# GetFilteredSaleArticleGroupQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberMinValue** | **Integer** |  |  [optional]
**numberMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]
**articleMainGroupIdMinValue** | **Integer** |  |  [optional]
**articleMainGroupIdMaxValue** | **Integer** |  |  [optional]



