
# SaleArticleGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**name** | **String** |  |  [optional]
**startArt** | **Integer** |  |  [optional]
**rvvNumberErkenning** | **String** |  |  [optional]
**omschrPrijslijst** | **String** |  |  [optional]
**ivkJournalLedgerNumberLow** | **String** |  |  [optional]
**ndaltpJournalLedgerNumberLow** | **String** |  |  [optional]
**teleBestel** | **Integer** |  |  [optional]
**mainGroup** | **Integer** |  |  [optional]
**nziCode** | **Integer** |  |  [optional]
**ivkJournalLedgerNumberHigh** | **String** |  |  [optional]
**ndaltpJournalLedgerNumberHigh** | **String** |  |  [optional]
**paymentPeriod** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



