
# StockMutationReasonDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**name** | **String** |  |  [optional]
**mutationType** | **Integer** |  |  [optional]
**ledger** | **Integer** |  |  [optional]
**costCentre** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



