
# SaleArticleMainGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mainGroupId** | **Integer** |  | 
**operations** | **List&lt;String&gt;** |  |  [optional]



