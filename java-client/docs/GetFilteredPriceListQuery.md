
# GetFilteredPriceListQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**salePriceListIdMinValue** | **Integer** |  |  [optional]
**salePriceListIdMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]



