
# ReflexUserDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**permissions** | [**List&lt;PermissionDescriptionDto&gt;**](PermissionDescriptionDto.md) |  |  [optional]
**permissionsInfo** | [**PermissionsInfoDto**](PermissionsInfoDto.md) |  |  [optional]
**number** | **Integer** |  | 
**logOnName** | **String** |  |  [optional]
**fullName** | **String** |  |  [optional]
**selfEditable** | **Integer** |  |  [optional]
**isBlocked** | **Boolean** |  |  [optional]
**showCleanupWarning** | **Boolean** |  |  [optional]
**securityEqualToUserId** | **Integer** |  |  [optional]
**birthdayAlert** | **Boolean** |  |  [optional]
**productionGroupFilterMethod** | **Integer** |  |  [optional]
**productionGroupFilterStartId** | **Integer** |  |  [optional]
**productionGroupFilterEndId** | **Integer** |  |  [optional]
**purchaseProductionGroupFilterMethod** | **Integer** |  |  [optional]
**purchaseProductionGroupFilterStartId** | **Integer** |  |  [optional]
**purchaseProductionGroupFilterEndId** | **Integer** |  |  [optional]
**pointOfSale** | **Integer** |  |  [optional]
**transporter** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



