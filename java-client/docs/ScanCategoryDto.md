
# ScanCategoryDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**name** | **String** |  |  [optional]
**relationAsCustomerMandatory** | **Boolean** |  |  [optional]
**relationAsSupplierMandatory** | **Boolean** |  |  [optional]
**tabName** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



