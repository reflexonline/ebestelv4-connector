# RecipeCostApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**recipeCostCreateRecipeCost**](RecipeCostApi.md#recipeCostCreateRecipeCost) | **POST** /RecipeCost | 
[**recipeCostDeleteRecipeCost**](RecipeCostApi.md#recipeCostDeleteRecipeCost) | **DELETE** /RecipeCost/{recipeCostId} | 
[**recipeCostExistsRecipeCost**](RecipeCostApi.md#recipeCostExistsRecipeCost) | **GET** /RecipeCost/exists/{recipeCostId} | 
[**recipeCostGetRecipeCost**](RecipeCostApi.md#recipeCostGetRecipeCost) | **GET** /RecipeCost/{recipeCostId} | 
[**recipeCostGetRecipeCosts**](RecipeCostApi.md#recipeCostGetRecipeCosts) | **POST** /RecipeCost/search | 
[**recipeCostUpdateRecipeCost**](RecipeCostApi.md#recipeCostUpdateRecipeCost) | **PUT** /RecipeCost/{recipeCostId} | 


<a name="recipeCostCreateRecipeCost"></a>
# **recipeCostCreateRecipeCost**
> Object recipeCostCreateRecipeCost(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeCostApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeCostApi apiInstance = new RecipeCostApi();
RecipeCostDto dto = new RecipeCostDto(); // RecipeCostDto | 
try {
    Object result = apiInstance.recipeCostCreateRecipeCost(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeCostApi#recipeCostCreateRecipeCost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**RecipeCostDto**](RecipeCostDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="recipeCostDeleteRecipeCost"></a>
# **recipeCostDeleteRecipeCost**
> Object recipeCostDeleteRecipeCost(recipeCostId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeCostApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeCostApi apiInstance = new RecipeCostApi();
Integer recipeCostId = 56; // Integer | 
try {
    Object result = apiInstance.recipeCostDeleteRecipeCost(recipeCostId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeCostApi#recipeCostDeleteRecipeCost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipeCostId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="recipeCostExistsRecipeCost"></a>
# **recipeCostExistsRecipeCost**
> Object recipeCostExistsRecipeCost(recipeCostId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeCostApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeCostApi apiInstance = new RecipeCostApi();
Integer recipeCostId = 56; // Integer | 
try {
    Object result = apiInstance.recipeCostExistsRecipeCost(recipeCostId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeCostApi#recipeCostExistsRecipeCost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipeCostId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="recipeCostGetRecipeCost"></a>
# **recipeCostGetRecipeCost**
> Object recipeCostGetRecipeCost(recipeCostId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeCostApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeCostApi apiInstance = new RecipeCostApi();
Integer recipeCostId = 56; // Integer | 
try {
    Object result = apiInstance.recipeCostGetRecipeCost(recipeCostId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeCostApi#recipeCostGetRecipeCost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipeCostId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="recipeCostGetRecipeCosts"></a>
# **recipeCostGetRecipeCosts**
> Object recipeCostGetRecipeCosts(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeCostApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeCostApi apiInstance = new RecipeCostApi();
GetFilteredRecipeCostQuery query = new GetFilteredRecipeCostQuery(); // GetFilteredRecipeCostQuery | 
try {
    Object result = apiInstance.recipeCostGetRecipeCosts(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeCostApi#recipeCostGetRecipeCosts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredRecipeCostQuery**](GetFilteredRecipeCostQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="recipeCostUpdateRecipeCost"></a>
# **recipeCostUpdateRecipeCost**
> Object recipeCostUpdateRecipeCost(recipeCostId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RecipeCostApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RecipeCostApi apiInstance = new RecipeCostApi();
Integer recipeCostId = 56; // Integer | 
RecipeCostDto dto = new RecipeCostDto(); // RecipeCostDto | 
try {
    Object result = apiInstance.recipeCostUpdateRecipeCost(recipeCostId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RecipeCostApi#recipeCostUpdateRecipeCost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipeCostId** | **Integer**|  |
 **dto** | [**RecipeCostDto**](RecipeCostDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

