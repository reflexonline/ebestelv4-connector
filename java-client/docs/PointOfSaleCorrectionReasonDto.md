
# PointOfSaleCorrectionReasonDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pointOfSaleCorrectionReasonId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



