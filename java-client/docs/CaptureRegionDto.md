
# CaptureRegionDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**omschrijving** | **String** |  |  [optional]
**faoNumber** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



