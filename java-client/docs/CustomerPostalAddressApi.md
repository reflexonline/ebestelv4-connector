# CustomerPostalAddressApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerPostalAddressExistsCustomerPostalAddress**](CustomerPostalAddressApi.md#customerPostalAddressExistsCustomerPostalAddress) | **GET** /CustomerPostalAddress/exists/{customerId} | 
[**customerPostalAddressGetCustomerPostalAddress**](CustomerPostalAddressApi.md#customerPostalAddressGetCustomerPostalAddress) | **GET** /CustomerPostalAddress/{customerId} | 
[**customerPostalAddressGetCustomerPostalAddresses**](CustomerPostalAddressApi.md#customerPostalAddressGetCustomerPostalAddresses) | **POST** /CustomerPostalAddress/search | 


<a name="customerPostalAddressExistsCustomerPostalAddress"></a>
# **customerPostalAddressExistsCustomerPostalAddress**
> Object customerPostalAddressExistsCustomerPostalAddress(customerId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerPostalAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CustomerPostalAddressApi apiInstance = new CustomerPostalAddressApi();
Integer customerId = 56; // Integer | 
try {
    Object result = apiInstance.customerPostalAddressExistsCustomerPostalAddress(customerId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerPostalAddressApi#customerPostalAddressExistsCustomerPostalAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="customerPostalAddressGetCustomerPostalAddress"></a>
# **customerPostalAddressGetCustomerPostalAddress**
> Object customerPostalAddressGetCustomerPostalAddress(customerId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerPostalAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CustomerPostalAddressApi apiInstance = new CustomerPostalAddressApi();
Integer customerId = 56; // Integer | 
try {
    Object result = apiInstance.customerPostalAddressGetCustomerPostalAddress(customerId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerPostalAddressApi#customerPostalAddressGetCustomerPostalAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="customerPostalAddressGetCustomerPostalAddresses"></a>
# **customerPostalAddressGetCustomerPostalAddresses**
> Object customerPostalAddressGetCustomerPostalAddresses(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerPostalAddressApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CustomerPostalAddressApi apiInstance = new CustomerPostalAddressApi();
GetFilteredCustomerPostalAddressQuery query = new GetFilteredCustomerPostalAddressQuery(); // GetFilteredCustomerPostalAddressQuery | 
try {
    Object result = apiInstance.customerPostalAddressGetCustomerPostalAddresses(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerPostalAddressApi#customerPostalAddressGetCustomerPostalAddresses");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredCustomerPostalAddressQuery**](GetFilteredCustomerPostalAddressQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

