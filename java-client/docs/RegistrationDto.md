
# RegistrationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**palletId** | **Integer** |  |  [optional]
**palletArticleId** | **Integer** |  |  [optional]
**palletTare** | **Double** |  |  [optional]
**palletBoxId** | **Integer** |  |  [optional]
**sourceProductionTaskId** | **Integer** |  |  [optional]
**dateTime** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**quantity** | **Double** |  |  [optional]
**grossWeight** | **Double** |  |  [optional]
**netWeight** | **Double** |  |  [optional]
**tareWeight** | **Double** |  |  [optional]
**decimalPosition** | **Integer** |  |  [optional]
**sourceStockLocationId** | **Integer** |  |  [optional]
**destinationStockLocationId** | **Integer** |  |  [optional]
**sourceBatchId** | **Integer** |  |  [optional]
**destinationBatchId** | **Integer** |  |  [optional]
**externalBoxId** | **String** |  |  [optional]
**box1ArticleId** | **Integer** |  |  [optional]
**box1Tare** | **Double** |  |  [optional]
**box1Quantity** | **Double** |  |  [optional]
**box2ArticleId** | **Integer** |  |  [optional]
**box2Tare** | **Double** |  |  [optional]
**box2Quantity** | **Double** |  |  [optional]
**sourceBatchReference** | **String** |  |  [optional]
**purchaseArticleId** | **Integer** |  |  [optional]
**saleArticleId** | **Integer** |  |  [optional]
**temperature** | **Double** |  |  [optional]
**bestBeforeDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]



