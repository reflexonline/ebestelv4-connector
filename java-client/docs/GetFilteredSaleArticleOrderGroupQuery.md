
# GetFilteredSaleArticleOrderGroupQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**saleArticleOrderGroupIdMinValue** | **Integer** |  |  [optional]
**saleArticleOrderGroupIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



