
# WssDepartmentDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**name** | **String** |  |  [optional]
**fromProductionGroup** | **Integer** |  |  [optional]
**untilProductionGroup** | **Integer** |  |  [optional]
**lableNumber** | **Integer** |  |  [optional]
**poort** | **String** |  |  [optional]
**aantalLabelsEO** | **Integer** |  |  [optional]
**minColliMonday** | **Integer** |  |  [optional]
**vrijgaveTimeMonday** | **String** |  |  [optional]
**aantalBaserenOpEO** | **Integer** |  |  [optional]
**embArtikel** | **Integer** |  |  [optional]
**aantalBaserenOp** | **Integer** |  |  [optional]
**aantalLabels** | **Integer** |  |  [optional]
**pakbonPrinter1** | **Integer** |  |  [optional]
**pakbonPrinter2** | **Integer** |  |  [optional]
**minColliTuesday** | **Integer** |  |  [optional]
**vrijgaveTimeTuesday** | **String** |  |  [optional]
**minColliWednesday** | **Integer** |  |  [optional]
**vrijgaveTimeWednesday** | **String** |  |  [optional]
**minColliThursday** | **Integer** |  |  [optional]
**vrijgaveTimeThursday** | **String** |  |  [optional]
**minColliFriday** | **Integer** |  |  [optional]
**vrijgaveTimeFriday** | **String** |  |  [optional]
**minColliSaturday** | **Integer** |  |  [optional]
**vrijgaveTimeSaturday** | **String** |  |  [optional]
**minColliSunday** | **Integer** |  |  [optional]
**vrijgaveTimeSunday** | **String** |  |  [optional]
**defaultArticleQuality** | **Integer** |  |  [optional]
**replenishmentLocation** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



