# CallResolutionApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**callResolutionCreateCallResolution**](CallResolutionApi.md#callResolutionCreateCallResolution) | **POST** /CallResolution | 
[**callResolutionDeleteCallResolution**](CallResolutionApi.md#callResolutionDeleteCallResolution) | **DELETE** /CallResolution/{number} | 
[**callResolutionExistsCallResolution**](CallResolutionApi.md#callResolutionExistsCallResolution) | **GET** /CallResolution/exists/{number} | 
[**callResolutionGetCallResolution**](CallResolutionApi.md#callResolutionGetCallResolution) | **GET** /CallResolution/{number} | 
[**callResolutionGetCallResolutions**](CallResolutionApi.md#callResolutionGetCallResolutions) | **POST** /CallResolution/search | 
[**callResolutionUpdateCallResolution**](CallResolutionApi.md#callResolutionUpdateCallResolution) | **PUT** /CallResolution/{number} | 


<a name="callResolutionCreateCallResolution"></a>
# **callResolutionCreateCallResolution**
> Object callResolutionCreateCallResolution(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallResolutionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallResolutionApi apiInstance = new CallResolutionApi();
CallResolutionDto dto = new CallResolutionDto(); // CallResolutionDto | 
try {
    Object result = apiInstance.callResolutionCreateCallResolution(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallResolutionApi#callResolutionCreateCallResolution");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**CallResolutionDto**](CallResolutionDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="callResolutionDeleteCallResolution"></a>
# **callResolutionDeleteCallResolution**
> Object callResolutionDeleteCallResolution(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallResolutionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallResolutionApi apiInstance = new CallResolutionApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.callResolutionDeleteCallResolution(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallResolutionApi#callResolutionDeleteCallResolution");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="callResolutionExistsCallResolution"></a>
# **callResolutionExistsCallResolution**
> Object callResolutionExistsCallResolution(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallResolutionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallResolutionApi apiInstance = new CallResolutionApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.callResolutionExistsCallResolution(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallResolutionApi#callResolutionExistsCallResolution");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="callResolutionGetCallResolution"></a>
# **callResolutionGetCallResolution**
> Object callResolutionGetCallResolution(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallResolutionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallResolutionApi apiInstance = new CallResolutionApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.callResolutionGetCallResolution(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallResolutionApi#callResolutionGetCallResolution");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="callResolutionGetCallResolutions"></a>
# **callResolutionGetCallResolutions**
> Object callResolutionGetCallResolutions(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallResolutionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallResolutionApi apiInstance = new CallResolutionApi();
GetFilteredCallResolutionQuery query = new GetFilteredCallResolutionQuery(); // GetFilteredCallResolutionQuery | 
try {
    Object result = apiInstance.callResolutionGetCallResolutions(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallResolutionApi#callResolutionGetCallResolutions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredCallResolutionQuery**](GetFilteredCallResolutionQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="callResolutionUpdateCallResolution"></a>
# **callResolutionUpdateCallResolution**
> Object callResolutionUpdateCallResolution(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallResolutionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallResolutionApi apiInstance = new CallResolutionApi();
Integer number = 56; // Integer | 
CallResolutionDto dto = new CallResolutionDto(); // CallResolutionDto | 
try {
    Object result = apiInstance.callResolutionUpdateCallResolution(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallResolutionApi#callResolutionUpdateCallResolution");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**CallResolutionDto**](CallResolutionDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

