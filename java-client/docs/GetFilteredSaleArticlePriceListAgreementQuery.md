
# GetFilteredSaleArticlePriceListAgreementQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**priceListIdMinValue** | **Integer** |  |  [optional]
**priceListIdMaxValue** | **Integer** |  |  [optional]
**articleIdMinValue** | **Integer** |  |  [optional]
**articleIdMaxValue** | **Integer** |  |  [optional]



