
# CallTypeDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**zoekArg** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**formulier1** | **Integer** |  |  [optional]
**formulier2** | **Integer** |  |  [optional]
**afhandelform** | **Integer** |  |  [optional]
**afhandelform2** | **Integer** |  |  [optional]
**r7** | **String** |  |  [optional]
**r8** | **String** |  |  [optional]
**r9** | **String** |  |  [optional]
**r10** | **String** |  |  [optional]
**r11** | **String** |  |  [optional]
**r12** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



