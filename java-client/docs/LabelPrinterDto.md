
# LabelPrinterDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**labelPrinterId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**port** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



