# OrderCharacteristicApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orderCharacteristicCreateOrderCharacteristic**](OrderCharacteristicApi.md#orderCharacteristicCreateOrderCharacteristic) | **POST** /OrderCharacteristic | 
[**orderCharacteristicDeleteOrderCharacteristic**](OrderCharacteristicApi.md#orderCharacteristicDeleteOrderCharacteristic) | **DELETE** /OrderCharacteristic/{orderCharacteristicId} | 
[**orderCharacteristicExistsOrderCharacteristic**](OrderCharacteristicApi.md#orderCharacteristicExistsOrderCharacteristic) | **GET** /OrderCharacteristic/exists/{orderCharacteristicId} | 
[**orderCharacteristicGetOrderCharacteristic**](OrderCharacteristicApi.md#orderCharacteristicGetOrderCharacteristic) | **GET** /OrderCharacteristic/{orderCharacteristicId} | 
[**orderCharacteristicGetOrderCharacteristics**](OrderCharacteristicApi.md#orderCharacteristicGetOrderCharacteristics) | **POST** /OrderCharacteristic/search | 
[**orderCharacteristicUpdateOrderCharacteristic**](OrderCharacteristicApi.md#orderCharacteristicUpdateOrderCharacteristic) | **PUT** /OrderCharacteristic/{orderCharacteristicId} | 


<a name="orderCharacteristicCreateOrderCharacteristic"></a>
# **orderCharacteristicCreateOrderCharacteristic**
> Object orderCharacteristicCreateOrderCharacteristic(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.OrderCharacteristicApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

OrderCharacteristicApi apiInstance = new OrderCharacteristicApi();
OrderCharacteristicDto dto = new OrderCharacteristicDto(); // OrderCharacteristicDto | 
try {
    Object result = apiInstance.orderCharacteristicCreateOrderCharacteristic(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrderCharacteristicApi#orderCharacteristicCreateOrderCharacteristic");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**OrderCharacteristicDto**](OrderCharacteristicDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="orderCharacteristicDeleteOrderCharacteristic"></a>
# **orderCharacteristicDeleteOrderCharacteristic**
> Object orderCharacteristicDeleteOrderCharacteristic(orderCharacteristicId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.OrderCharacteristicApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

OrderCharacteristicApi apiInstance = new OrderCharacteristicApi();
Integer orderCharacteristicId = 56; // Integer | 
try {
    Object result = apiInstance.orderCharacteristicDeleteOrderCharacteristic(orderCharacteristicId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrderCharacteristicApi#orderCharacteristicDeleteOrderCharacteristic");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderCharacteristicId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="orderCharacteristicExistsOrderCharacteristic"></a>
# **orderCharacteristicExistsOrderCharacteristic**
> Object orderCharacteristicExistsOrderCharacteristic(orderCharacteristicId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.OrderCharacteristicApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

OrderCharacteristicApi apiInstance = new OrderCharacteristicApi();
Integer orderCharacteristicId = 56; // Integer | 
try {
    Object result = apiInstance.orderCharacteristicExistsOrderCharacteristic(orderCharacteristicId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrderCharacteristicApi#orderCharacteristicExistsOrderCharacteristic");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderCharacteristicId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="orderCharacteristicGetOrderCharacteristic"></a>
# **orderCharacteristicGetOrderCharacteristic**
> Object orderCharacteristicGetOrderCharacteristic(orderCharacteristicId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.OrderCharacteristicApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

OrderCharacteristicApi apiInstance = new OrderCharacteristicApi();
Integer orderCharacteristicId = 56; // Integer | 
try {
    Object result = apiInstance.orderCharacteristicGetOrderCharacteristic(orderCharacteristicId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrderCharacteristicApi#orderCharacteristicGetOrderCharacteristic");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderCharacteristicId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="orderCharacteristicGetOrderCharacteristics"></a>
# **orderCharacteristicGetOrderCharacteristics**
> Object orderCharacteristicGetOrderCharacteristics(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.OrderCharacteristicApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

OrderCharacteristicApi apiInstance = new OrderCharacteristicApi();
GetFilteredOrderCharacteristicQuery query = new GetFilteredOrderCharacteristicQuery(); // GetFilteredOrderCharacteristicQuery | 
try {
    Object result = apiInstance.orderCharacteristicGetOrderCharacteristics(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrderCharacteristicApi#orderCharacteristicGetOrderCharacteristics");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredOrderCharacteristicQuery**](GetFilteredOrderCharacteristicQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="orderCharacteristicUpdateOrderCharacteristic"></a>
# **orderCharacteristicUpdateOrderCharacteristic**
> Object orderCharacteristicUpdateOrderCharacteristic(orderCharacteristicId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.OrderCharacteristicApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

OrderCharacteristicApi apiInstance = new OrderCharacteristicApi();
Integer orderCharacteristicId = 56; // Integer | 
OrderCharacteristicDto dto = new OrderCharacteristicDto(); // OrderCharacteristicDto | 
try {
    Object result = apiInstance.orderCharacteristicUpdateOrderCharacteristic(orderCharacteristicId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrderCharacteristicApi#orderCharacteristicUpdateOrderCharacteristic");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderCharacteristicId** | **Integer**|  |
 **dto** | [**OrderCharacteristicDto**](OrderCharacteristicDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

