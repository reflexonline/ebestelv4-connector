
# GetFilteredBatchSeperatorQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**batchSeperatorIdMinValue** | **Integer** |  |  [optional]
**batchSeperatorIdMaxValue** | **Integer** |  |  [optional]
**batchSeperatorCodeMinValue** | **String** |  |  [optional]
**batchSeperatorCodeMaxValue** | **String** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



