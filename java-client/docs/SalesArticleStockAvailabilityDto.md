
# SalesArticleStockAvailabilityDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestDateTime** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**quantity** | **Double** |  |  [optional]
**weight** | **Double** |  |  [optional]



