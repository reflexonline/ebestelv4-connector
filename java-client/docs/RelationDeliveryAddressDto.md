
# RelationDeliveryAddressDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**marktSegment** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**addressLine1** | **String** |  |  [optional]
**addressLine2** | **String** |  |  [optional]
**postalCode** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**phoneNumber** | **String** |  |  [optional]
**faxNumber** | **String** |  |  [optional]
**isoCountryCode** | **String** |  |  [optional]
**country** | **String** |  |  [optional]
**longName** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



