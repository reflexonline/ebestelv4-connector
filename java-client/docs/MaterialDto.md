
# MaterialDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**description** | **String** |  |  [optional]
**taxLevel** | **Integer** |  |  [optional]
**amount** | **Double** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



