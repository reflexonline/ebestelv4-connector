
# ProductTypeDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**omschrijving** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



