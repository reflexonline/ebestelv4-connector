
# RecipeGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipeGroupId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**internalCustomerIdBaseMaterial** | **Integer** |  |  [optional]
**internalCustomerIdSemiFinishedProduct** | **Integer** |  |  [optional]
**ledgerNumberProductionLoss** | **Integer** |  |  [optional]
**costNumberProductionLoss** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



