
# OrderCharacteristicDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderCharacteristicId** | **Integer** |  | 
**description** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



