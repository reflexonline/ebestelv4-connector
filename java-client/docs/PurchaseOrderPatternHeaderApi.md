# PurchaseOrderPatternHeaderApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purchaseOrderPatternHeaderCreatePurchaseOrderPatternHeader**](PurchaseOrderPatternHeaderApi.md#purchaseOrderPatternHeaderCreatePurchaseOrderPatternHeader) | **POST** /PurchaseOrderPatternHeader | 
[**purchaseOrderPatternHeaderDeletePurchaseOrderPatternHeader**](PurchaseOrderPatternHeaderApi.md#purchaseOrderPatternHeaderDeletePurchaseOrderPatternHeader) | **DELETE** /PurchaseOrderPatternHeader/{supplierId}/{list} | 
[**purchaseOrderPatternHeaderExistsPurchaseOrderPatternHeader**](PurchaseOrderPatternHeaderApi.md#purchaseOrderPatternHeaderExistsPurchaseOrderPatternHeader) | **GET** /PurchaseOrderPatternHeader/exists/{supplierId}/{list} | 
[**purchaseOrderPatternHeaderGetPurchaseOrderPatternHeader**](PurchaseOrderPatternHeaderApi.md#purchaseOrderPatternHeaderGetPurchaseOrderPatternHeader) | **GET** /PurchaseOrderPatternHeader/{supplierId}/{list} | 
[**purchaseOrderPatternHeaderGetPurchaseOrderPatternHeaders**](PurchaseOrderPatternHeaderApi.md#purchaseOrderPatternHeaderGetPurchaseOrderPatternHeaders) | **POST** /PurchaseOrderPatternHeader/search | 
[**purchaseOrderPatternHeaderUpdatePurchaseOrderPatternHeader**](PurchaseOrderPatternHeaderApi.md#purchaseOrderPatternHeaderUpdatePurchaseOrderPatternHeader) | **PUT** /PurchaseOrderPatternHeader/{supplierId}/{list} | 


<a name="purchaseOrderPatternHeaderCreatePurchaseOrderPatternHeader"></a>
# **purchaseOrderPatternHeaderCreatePurchaseOrderPatternHeader**
> Object purchaseOrderPatternHeaderCreatePurchaseOrderPatternHeader(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderPatternHeaderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderPatternHeaderApi apiInstance = new PurchaseOrderPatternHeaderApi();
PurchaseOrderPatternHeaderDto dto = new PurchaseOrderPatternHeaderDto(); // PurchaseOrderPatternHeaderDto | 
try {
    Object result = apiInstance.purchaseOrderPatternHeaderCreatePurchaseOrderPatternHeader(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderPatternHeaderApi#purchaseOrderPatternHeaderCreatePurchaseOrderPatternHeader");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PurchaseOrderPatternHeaderDto**](PurchaseOrderPatternHeaderDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseOrderPatternHeaderDeletePurchaseOrderPatternHeader"></a>
# **purchaseOrderPatternHeaderDeletePurchaseOrderPatternHeader**
> Object purchaseOrderPatternHeaderDeletePurchaseOrderPatternHeader(supplierId, list)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderPatternHeaderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderPatternHeaderApi apiInstance = new PurchaseOrderPatternHeaderApi();
Integer supplierId = 56; // Integer | 
Integer list = 56; // Integer | 
try {
    Object result = apiInstance.purchaseOrderPatternHeaderDeletePurchaseOrderPatternHeader(supplierId, list);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderPatternHeaderApi#purchaseOrderPatternHeaderDeletePurchaseOrderPatternHeader");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **list** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseOrderPatternHeaderExistsPurchaseOrderPatternHeader"></a>
# **purchaseOrderPatternHeaderExistsPurchaseOrderPatternHeader**
> Object purchaseOrderPatternHeaderExistsPurchaseOrderPatternHeader(supplierId, list)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderPatternHeaderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderPatternHeaderApi apiInstance = new PurchaseOrderPatternHeaderApi();
Integer supplierId = 56; // Integer | 
Integer list = 56; // Integer | 
try {
    Object result = apiInstance.purchaseOrderPatternHeaderExistsPurchaseOrderPatternHeader(supplierId, list);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderPatternHeaderApi#purchaseOrderPatternHeaderExistsPurchaseOrderPatternHeader");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **list** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseOrderPatternHeaderGetPurchaseOrderPatternHeader"></a>
# **purchaseOrderPatternHeaderGetPurchaseOrderPatternHeader**
> Object purchaseOrderPatternHeaderGetPurchaseOrderPatternHeader(supplierId, list)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderPatternHeaderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderPatternHeaderApi apiInstance = new PurchaseOrderPatternHeaderApi();
Integer supplierId = 56; // Integer | 
Integer list = 56; // Integer | 
try {
    Object result = apiInstance.purchaseOrderPatternHeaderGetPurchaseOrderPatternHeader(supplierId, list);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderPatternHeaderApi#purchaseOrderPatternHeaderGetPurchaseOrderPatternHeader");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **list** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="purchaseOrderPatternHeaderGetPurchaseOrderPatternHeaders"></a>
# **purchaseOrderPatternHeaderGetPurchaseOrderPatternHeaders**
> Object purchaseOrderPatternHeaderGetPurchaseOrderPatternHeaders(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderPatternHeaderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderPatternHeaderApi apiInstance = new PurchaseOrderPatternHeaderApi();
GetFilteredPurchaseOrderPatternHeaderQuery query = new GetFilteredPurchaseOrderPatternHeaderQuery(); // GetFilteredPurchaseOrderPatternHeaderQuery | 
try {
    Object result = apiInstance.purchaseOrderPatternHeaderGetPurchaseOrderPatternHeaders(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderPatternHeaderApi#purchaseOrderPatternHeaderGetPurchaseOrderPatternHeaders");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPurchaseOrderPatternHeaderQuery**](GetFilteredPurchaseOrderPatternHeaderQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="purchaseOrderPatternHeaderUpdatePurchaseOrderPatternHeader"></a>
# **purchaseOrderPatternHeaderUpdatePurchaseOrderPatternHeader**
> Object purchaseOrderPatternHeaderUpdatePurchaseOrderPatternHeader(supplierId, list, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PurchaseOrderPatternHeaderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PurchaseOrderPatternHeaderApi apiInstance = new PurchaseOrderPatternHeaderApi();
Integer supplierId = 56; // Integer | 
Integer list = 56; // Integer | 
PurchaseOrderPatternHeaderDto dto = new PurchaseOrderPatternHeaderDto(); // PurchaseOrderPatternHeaderDto | 
try {
    Object result = apiInstance.purchaseOrderPatternHeaderUpdatePurchaseOrderPatternHeader(supplierId, list, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PurchaseOrderPatternHeaderApi#purchaseOrderPatternHeaderUpdatePurchaseOrderPatternHeader");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **list** | **Integer**|  |
 **dto** | [**PurchaseOrderPatternHeaderDto**](PurchaseOrderPatternHeaderDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

