# SupplierPurchaseArticleApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**supplierPurchaseArticleCreateSupplierPurchaseArticle**](SupplierPurchaseArticleApi.md#supplierPurchaseArticleCreateSupplierPurchaseArticle) | **POST** /SupplierPurchaseArticle | 
[**supplierPurchaseArticleDeleteSupplierPurchaseArticle**](SupplierPurchaseArticleApi.md#supplierPurchaseArticleDeleteSupplierPurchaseArticle) | **DELETE** /SupplierPurchaseArticle/{supplierId}/{purchaseArticleId} | 
[**supplierPurchaseArticleExistsSupplierPurchaseArticle**](SupplierPurchaseArticleApi.md#supplierPurchaseArticleExistsSupplierPurchaseArticle) | **GET** /SupplierPurchaseArticle/exists/{supplierId}/{purchaseArticleId} | 
[**supplierPurchaseArticleGetSupplierPurchaseArticle**](SupplierPurchaseArticleApi.md#supplierPurchaseArticleGetSupplierPurchaseArticle) | **GET** /SupplierPurchaseArticle/{supplierId}/{purchaseArticleId} | 
[**supplierPurchaseArticleGetSupplierPurchaseArticles**](SupplierPurchaseArticleApi.md#supplierPurchaseArticleGetSupplierPurchaseArticles) | **POST** /SupplierPurchaseArticle/search | 
[**supplierPurchaseArticleUpdateSupplierPurchaseArticle**](SupplierPurchaseArticleApi.md#supplierPurchaseArticleUpdateSupplierPurchaseArticle) | **PUT** /SupplierPurchaseArticle/{supplierId}/{purchaseArticleId} | 


<a name="supplierPurchaseArticleCreateSupplierPurchaseArticle"></a>
# **supplierPurchaseArticleCreateSupplierPurchaseArticle**
> Object supplierPurchaseArticleCreateSupplierPurchaseArticle(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierPurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierPurchaseArticleApi apiInstance = new SupplierPurchaseArticleApi();
SupplierPurchaseArticleDto dto = new SupplierPurchaseArticleDto(); // SupplierPurchaseArticleDto | 
try {
    Object result = apiInstance.supplierPurchaseArticleCreateSupplierPurchaseArticle(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierPurchaseArticleApi#supplierPurchaseArticleCreateSupplierPurchaseArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**SupplierPurchaseArticleDto**](SupplierPurchaseArticleDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="supplierPurchaseArticleDeleteSupplierPurchaseArticle"></a>
# **supplierPurchaseArticleDeleteSupplierPurchaseArticle**
> Object supplierPurchaseArticleDeleteSupplierPurchaseArticle(supplierId, purchaseArticleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierPurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierPurchaseArticleApi apiInstance = new SupplierPurchaseArticleApi();
Integer supplierId = 56; // Integer | 
Integer purchaseArticleId = 56; // Integer | 
try {
    Object result = apiInstance.supplierPurchaseArticleDeleteSupplierPurchaseArticle(supplierId, purchaseArticleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierPurchaseArticleApi#supplierPurchaseArticleDeleteSupplierPurchaseArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **purchaseArticleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="supplierPurchaseArticleExistsSupplierPurchaseArticle"></a>
# **supplierPurchaseArticleExistsSupplierPurchaseArticle**
> Object supplierPurchaseArticleExistsSupplierPurchaseArticle(supplierId, purchaseArticleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierPurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierPurchaseArticleApi apiInstance = new SupplierPurchaseArticleApi();
Integer supplierId = 56; // Integer | 
Integer purchaseArticleId = 56; // Integer | 
try {
    Object result = apiInstance.supplierPurchaseArticleExistsSupplierPurchaseArticle(supplierId, purchaseArticleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierPurchaseArticleApi#supplierPurchaseArticleExistsSupplierPurchaseArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **purchaseArticleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="supplierPurchaseArticleGetSupplierPurchaseArticle"></a>
# **supplierPurchaseArticleGetSupplierPurchaseArticle**
> Object supplierPurchaseArticleGetSupplierPurchaseArticle(supplierId, purchaseArticleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierPurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierPurchaseArticleApi apiInstance = new SupplierPurchaseArticleApi();
Integer supplierId = 56; // Integer | 
Integer purchaseArticleId = 56; // Integer | 
try {
    Object result = apiInstance.supplierPurchaseArticleGetSupplierPurchaseArticle(supplierId, purchaseArticleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierPurchaseArticleApi#supplierPurchaseArticleGetSupplierPurchaseArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **purchaseArticleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="supplierPurchaseArticleGetSupplierPurchaseArticles"></a>
# **supplierPurchaseArticleGetSupplierPurchaseArticles**
> Object supplierPurchaseArticleGetSupplierPurchaseArticles(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierPurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierPurchaseArticleApi apiInstance = new SupplierPurchaseArticleApi();
GetFilteredSupplierPurchaseArticleQuery query = new GetFilteredSupplierPurchaseArticleQuery(); // GetFilteredSupplierPurchaseArticleQuery | 
try {
    Object result = apiInstance.supplierPurchaseArticleGetSupplierPurchaseArticles(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierPurchaseArticleApi#supplierPurchaseArticleGetSupplierPurchaseArticles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSupplierPurchaseArticleQuery**](GetFilteredSupplierPurchaseArticleQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="supplierPurchaseArticleUpdateSupplierPurchaseArticle"></a>
# **supplierPurchaseArticleUpdateSupplierPurchaseArticle**
> Object supplierPurchaseArticleUpdateSupplierPurchaseArticle(supplierId, purchaseArticleId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupplierPurchaseArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SupplierPurchaseArticleApi apiInstance = new SupplierPurchaseArticleApi();
Integer supplierId = 56; // Integer | 
Integer purchaseArticleId = 56; // Integer | 
SupplierPurchaseArticleDto dto = new SupplierPurchaseArticleDto(); // SupplierPurchaseArticleDto | 
try {
    Object result = apiInstance.supplierPurchaseArticleUpdateSupplierPurchaseArticle(supplierId, purchaseArticleId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupplierPurchaseArticleApi#supplierPurchaseArticleUpdateSupplierPurchaseArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplierId** | **Integer**|  |
 **purchaseArticleId** | **Integer**|  |
 **dto** | [**SupplierPurchaseArticleDto**](SupplierPurchaseArticleDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

