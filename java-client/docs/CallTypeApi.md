# CallTypeApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**callTypeCreateCallType**](CallTypeApi.md#callTypeCreateCallType) | **POST** /CallType | 
[**callTypeDeleteCallType**](CallTypeApi.md#callTypeDeleteCallType) | **DELETE** /CallType/{number} | 
[**callTypeExistsCallType**](CallTypeApi.md#callTypeExistsCallType) | **GET** /CallType/exists/{number} | 
[**callTypeGetCallType**](CallTypeApi.md#callTypeGetCallType) | **GET** /CallType/{number} | 
[**callTypeGetCallTypes**](CallTypeApi.md#callTypeGetCallTypes) | **POST** /CallType/search | 
[**callTypeUpdateCallType**](CallTypeApi.md#callTypeUpdateCallType) | **PUT** /CallType/{number} | 


<a name="callTypeCreateCallType"></a>
# **callTypeCreateCallType**
> Object callTypeCreateCallType(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallTypeApi apiInstance = new CallTypeApi();
CallTypeDto dto = new CallTypeDto(); // CallTypeDto | 
try {
    Object result = apiInstance.callTypeCreateCallType(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallTypeApi#callTypeCreateCallType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**CallTypeDto**](CallTypeDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="callTypeDeleteCallType"></a>
# **callTypeDeleteCallType**
> Object callTypeDeleteCallType(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallTypeApi apiInstance = new CallTypeApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.callTypeDeleteCallType(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallTypeApi#callTypeDeleteCallType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="callTypeExistsCallType"></a>
# **callTypeExistsCallType**
> Object callTypeExistsCallType(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallTypeApi apiInstance = new CallTypeApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.callTypeExistsCallType(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallTypeApi#callTypeExistsCallType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="callTypeGetCallType"></a>
# **callTypeGetCallType**
> Object callTypeGetCallType(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallTypeApi apiInstance = new CallTypeApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.callTypeGetCallType(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallTypeApi#callTypeGetCallType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="callTypeGetCallTypes"></a>
# **callTypeGetCallTypes**
> Object callTypeGetCallTypes(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallTypeApi apiInstance = new CallTypeApi();
GetFilteredCallTypeQuery query = new GetFilteredCallTypeQuery(); // GetFilteredCallTypeQuery | 
try {
    Object result = apiInstance.callTypeGetCallTypes(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallTypeApi#callTypeGetCallTypes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredCallTypeQuery**](GetFilteredCallTypeQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="callTypeUpdateCallType"></a>
# **callTypeUpdateCallType**
> Object callTypeUpdateCallType(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CallTypeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CallTypeApi apiInstance = new CallTypeApi();
Integer number = 56; // Integer | 
CallTypeDto dto = new CallTypeDto(); // CallTypeDto | 
try {
    Object result = apiInstance.callTypeUpdateCallType(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CallTypeApi#callTypeUpdateCallType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**CallTypeDto**](CallTypeDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

