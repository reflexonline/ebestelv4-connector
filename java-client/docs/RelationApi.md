# RelationApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**relationCreateRelation**](RelationApi.md#relationCreateRelation) | **POST** /Relation | 
[**relationDeleteRelation**](RelationApi.md#relationDeleteRelation) | **DELETE** /Relation/{number} | 
[**relationExistsRelation**](RelationApi.md#relationExistsRelation) | **GET** /Relation/exists/{number} | 
[**relationGetRelation**](RelationApi.md#relationGetRelation) | **GET** /Relation/{number} | 
[**relationGetRelations**](RelationApi.md#relationGetRelations) | **POST** /Relation/search | 
[**relationUpdateRelation**](RelationApi.md#relationUpdateRelation) | **PUT** /Relation/{number} | 


<a name="relationCreateRelation"></a>
# **relationCreateRelation**
> Object relationCreateRelation(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RelationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RelationApi apiInstance = new RelationApi();
RelationDto dto = new RelationDto(); // RelationDto | 
try {
    Object result = apiInstance.relationCreateRelation(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationApi#relationCreateRelation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**RelationDto**](RelationDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="relationDeleteRelation"></a>
# **relationDeleteRelation**
> Object relationDeleteRelation(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RelationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RelationApi apiInstance = new RelationApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.relationDeleteRelation(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationApi#relationDeleteRelation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="relationExistsRelation"></a>
# **relationExistsRelation**
> Object relationExistsRelation(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RelationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RelationApi apiInstance = new RelationApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.relationExistsRelation(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationApi#relationExistsRelation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="relationGetRelation"></a>
# **relationGetRelation**
> Object relationGetRelation(number)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RelationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RelationApi apiInstance = new RelationApi();
Integer number = 56; // Integer | 
try {
    Object result = apiInstance.relationGetRelation(number);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationApi#relationGetRelation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="relationGetRelations"></a>
# **relationGetRelations**
> Object relationGetRelations(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RelationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RelationApi apiInstance = new RelationApi();
GetFilteredRelationQuery query = new GetFilteredRelationQuery(); // GetFilteredRelationQuery | 
try {
    Object result = apiInstance.relationGetRelations(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationApi#relationGetRelations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredRelationQuery**](GetFilteredRelationQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="relationUpdateRelation"></a>
# **relationUpdateRelation**
> Object relationUpdateRelation(number, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RelationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

RelationApi apiInstance = new RelationApi();
Integer number = 56; // Integer | 
RelationDto dto = new RelationDto(); // RelationDto | 
try {
    Object result = apiInstance.relationUpdateRelation(number, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RelationApi#relationUpdateRelation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Integer**|  |
 **dto** | [**RelationDto**](RelationDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

