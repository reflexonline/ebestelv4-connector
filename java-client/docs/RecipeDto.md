
# RecipeDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipeId** | **Integer** |  | 
**recipeDerivativeId** | **Integer** |  | 
**recipeGroup** | **Integer** |  |  [optional]
**recipeType** | [**RecipeTypeEnum**](#RecipeTypeEnum) |  |  [optional]
**articleId** | **Integer** |  |  [optional]
**description** | **String** |  |  [optional]
**version** | **String** |  |  [optional]
**remark** | **String** |  |  [optional]
**dateCreated** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**dateModified** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**productionLossPercentage** | **Float** |  |  [optional]
**surchargePercentage** | **Float** |  |  [optional]
**commissionAmountPerKG** | **Float** |  |  [optional]
**maximumChargeSize** | **Double** |  |  [optional]
**productionTaskOutputLineCreationMethod** | [**ProductionTaskOutputLineCreationMethodEnum**](#ProductionTaskOutputLineCreationMethodEnum) |  |  [optional]
**maximumFinalizingNormDeviation** | **Integer** |  |  [optional]
**surchargeAmountPerKG** | **Float** |  |  [optional]
**profitAndRiskPercentage** | **Float** |  |  [optional]
**productionDaysCalculationMethod** | [**ProductionDaysCalculationMethodEnum**](#ProductionDaysCalculationMethodEnum) |  |  [optional]
**tracering** | **Integer** |  |  [optional]
**isBlocked** | **Boolean** |  |  [optional]
**availableFromDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**baseMaterialStockReservationOnOrderEntry** | [**BaseMaterialStockReservationOnOrderEntryEnum**](#BaseMaterialStockReservationOnOrderEntryEnum) |  |  [optional]
**costPriceCalculationMethod** | [**CostPriceCalculationMethodEnum**](#CostPriceCalculationMethodEnum) |  |  [optional]
**costPriceCalculationAmount** | **Double** |  |  [optional]
**isBaseRecipe** | **Boolean** |  |  [optional]
**inheritBaseMaterials** | **Boolean** |  |  [optional]
**createProductionTaskWhenUsedAsSemiFinishedProduct** | **Boolean** |  |  [optional]
**useProductionPlanning** | **Boolean** |  |  [optional]
**finalizePreviousGflBatchIfNewGflBatchIsSet** | **Boolean** |  |  [optional]
**batchInputByTraceMethod** | [**BatchInputByTraceMethodEnum**](#BatchInputByTraceMethodEnum) |  |  [optional]
**productionLossUnitType** | [**ProductionLossUnitTypeEnum**](#ProductionLossUnitTypeEnum) |  |  [optional]
**productionLossKilo** | **Double** |  |  [optional]
**productionPlanningFilter** | **Integer** |  |  [optional]
**showRecipeForProd** | **Integer** |  |  [optional]
**minimalChargeSize** | **Double** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]


<a name="RecipeTypeEnum"></a>
## Enum: RecipeTypeEnum
Name | Value
---- | -----
SALE | &quot;Sale&quot;
PURCHASE | &quot;Purchase&quot;
UNLINKED | &quot;Unlinked&quot;


<a name="ProductionTaskOutputLineCreationMethodEnum"></a>
## Enum: ProductionTaskOutputLineCreationMethodEnum
Name | Value
---- | -----
CREATEMAINRECIPEONLY | &quot;CreateMainRecipeOnly&quot;
CREATEMAINRECIPEINCLUDINGPACKINGS | &quot;CreateMainRecipeIncludingPackings&quot;
CREATEFROMPRODUCTIONREFERENCES | &quot;CreateFromProductionReferences&quot;
DONOTREGISTER | &quot;DoNotRegister&quot;


<a name="ProductionDaysCalculationMethodEnum"></a>
## Enum: ProductionDaysCalculationMethodEnum
Name | Value
---- | -----
INCLUDESEMIFINISHEDPRODUCTPRODUCTIONDAYS | &quot;IncludeSemiFinishedProductProductionDays&quot;
USEONLYTHISRECIPEPRODUCTIONDAYS | &quot;UseOnlyThisRecipeProductionDays&quot;


<a name="BaseMaterialStockReservationOnOrderEntryEnum"></a>
## Enum: BaseMaterialStockReservationOnOrderEntryEnum
Name | Value
---- | -----
ENABLED | &quot;Enabled&quot;
DISABLED | &quot;Disabled&quot;


<a name="CostPriceCalculationMethodEnum"></a>
## Enum: CostPriceCalculationMethodEnum
Name | Value
---- | -----
BASEDONFIXEDAMOUNT | &quot;BasedOnFixedAmount&quot;
BASEDONMAXIMUMCHARGESIZE | &quot;BasedOnMaximumChargeSize&quot;
BASEDONDEFAULTRECIPEAMOUNT | &quot;BasedOnDefaultRecipeAmount&quot;
BASEDONAMOUNTREQUIREDBYPARENTRECIPE | &quot;BasedOnAmountRequiredByParentRecipe&quot;


<a name="BatchInputByTraceMethodEnum"></a>
## Enum: BatchInputByTraceMethodEnum
Name | Value
---- | -----
NONE | &quot;None&quot;
GENERATEBATCHATCREATION | &quot;GenerateBatchAtCreation&quot;
GENERATEBATCHATCREATIONONEBATCHPERPRODUCTIONTASK | &quot;GenerateBatchAtCreationOneBatchPerProductionTask&quot;
GENERATEBATCHATREGISTRATION | &quot;GenerateBatchAtRegistration&quot;
ASKBATCHATREGISTRATION | &quot;AskBatchAtRegistration&quot;
ASKBATCHATREGISTRATIONONCE | &quot;AskBatchAtRegistrationOnce&quot;
GENERATEBATCHATCREATIONINCLUDEBYPRODUCT | &quot;GenerateBatchAtCreationIncludeByProduct&quot;
GENERATEBATCHATCREATIONONEBATCHPERPRODUCTIONTASKINCLUDEBYPRODUCT | &quot;GenerateBatchAtCreationOneBatchPerProductionTaskIncludeByProduct&quot;
GENERATEBATCHATREGISTRATIONINCLUDEBYPRODUCT | &quot;GenerateBatchAtRegistrationIncludeByProduct&quot;


<a name="ProductionLossUnitTypeEnum"></a>
## Enum: ProductionLossUnitTypeEnum
Name | Value
---- | -----
PERCENTAGE | &quot;Percentage&quot;
KILO | &quot;Kilo&quot;



