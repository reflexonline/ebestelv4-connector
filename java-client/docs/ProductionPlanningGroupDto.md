
# ProductionPlanningGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**planningGroupId** | **Integer** |  | 
**number** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**planningDaysOffset** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



