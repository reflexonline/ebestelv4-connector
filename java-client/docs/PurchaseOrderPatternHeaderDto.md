
# PurchaseOrderPatternHeaderDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierId** | **Integer** |  | 
**list** | **Integer** |  | 
**text** | **String** |  |  [optional]
**creationDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



