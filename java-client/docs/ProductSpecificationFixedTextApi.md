# ProductSpecificationFixedTextApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productSpecificationFixedTextCreateProductSpecificationFixedText**](ProductSpecificationFixedTextApi.md#productSpecificationFixedTextCreateProductSpecificationFixedText) | **POST** /ProductSpecificationFixedText | 
[**productSpecificationFixedTextDeleteProductSpecificationFixedText**](ProductSpecificationFixedTextApi.md#productSpecificationFixedTextDeleteProductSpecificationFixedText) | **DELETE** /ProductSpecificationFixedText/{productSpecificationFixedTextId} | 
[**productSpecificationFixedTextExistsProductSpecificationFixedText**](ProductSpecificationFixedTextApi.md#productSpecificationFixedTextExistsProductSpecificationFixedText) | **GET** /ProductSpecificationFixedText/exists/{productSpecificationFixedTextId} | 
[**productSpecificationFixedTextGetProductSpecificationFixedText**](ProductSpecificationFixedTextApi.md#productSpecificationFixedTextGetProductSpecificationFixedText) | **GET** /ProductSpecificationFixedText/{productSpecificationFixedTextId} | 
[**productSpecificationFixedTextGetProductSpecificationFixedTexts**](ProductSpecificationFixedTextApi.md#productSpecificationFixedTextGetProductSpecificationFixedTexts) | **POST** /ProductSpecificationFixedText/search | 
[**productSpecificationFixedTextUpdateProductSpecificationFixedText**](ProductSpecificationFixedTextApi.md#productSpecificationFixedTextUpdateProductSpecificationFixedText) | **PUT** /ProductSpecificationFixedText/{productSpecificationFixedTextId} | 


<a name="productSpecificationFixedTextCreateProductSpecificationFixedText"></a>
# **productSpecificationFixedTextCreateProductSpecificationFixedText**
> Object productSpecificationFixedTextCreateProductSpecificationFixedText(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationFixedTextApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationFixedTextApi apiInstance = new ProductSpecificationFixedTextApi();
ProductSpecificationFixedTextDto dto = new ProductSpecificationFixedTextDto(); // ProductSpecificationFixedTextDto | 
try {
    Object result = apiInstance.productSpecificationFixedTextCreateProductSpecificationFixedText(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationFixedTextApi#productSpecificationFixedTextCreateProductSpecificationFixedText");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**ProductSpecificationFixedTextDto**](ProductSpecificationFixedTextDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productSpecificationFixedTextDeleteProductSpecificationFixedText"></a>
# **productSpecificationFixedTextDeleteProductSpecificationFixedText**
> Object productSpecificationFixedTextDeleteProductSpecificationFixedText(productSpecificationFixedTextId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationFixedTextApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationFixedTextApi apiInstance = new ProductSpecificationFixedTextApi();
Integer productSpecificationFixedTextId = 56; // Integer | 
try {
    Object result = apiInstance.productSpecificationFixedTextDeleteProductSpecificationFixedText(productSpecificationFixedTextId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationFixedTextApi#productSpecificationFixedTextDeleteProductSpecificationFixedText");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationFixedTextId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productSpecificationFixedTextExistsProductSpecificationFixedText"></a>
# **productSpecificationFixedTextExistsProductSpecificationFixedText**
> Object productSpecificationFixedTextExistsProductSpecificationFixedText(productSpecificationFixedTextId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationFixedTextApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationFixedTextApi apiInstance = new ProductSpecificationFixedTextApi();
Integer productSpecificationFixedTextId = 56; // Integer | 
try {
    Object result = apiInstance.productSpecificationFixedTextExistsProductSpecificationFixedText(productSpecificationFixedTextId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationFixedTextApi#productSpecificationFixedTextExistsProductSpecificationFixedText");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationFixedTextId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productSpecificationFixedTextGetProductSpecificationFixedText"></a>
# **productSpecificationFixedTextGetProductSpecificationFixedText**
> Object productSpecificationFixedTextGetProductSpecificationFixedText(productSpecificationFixedTextId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationFixedTextApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationFixedTextApi apiInstance = new ProductSpecificationFixedTextApi();
Integer productSpecificationFixedTextId = 56; // Integer | 
try {
    Object result = apiInstance.productSpecificationFixedTextGetProductSpecificationFixedText(productSpecificationFixedTextId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationFixedTextApi#productSpecificationFixedTextGetProductSpecificationFixedText");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationFixedTextId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="productSpecificationFixedTextGetProductSpecificationFixedTexts"></a>
# **productSpecificationFixedTextGetProductSpecificationFixedTexts**
> Object productSpecificationFixedTextGetProductSpecificationFixedTexts(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationFixedTextApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationFixedTextApi apiInstance = new ProductSpecificationFixedTextApi();
GetFilteredProductSpecificationFixedTextQuery query = new GetFilteredProductSpecificationFixedTextQuery(); // GetFilteredProductSpecificationFixedTextQuery | 
try {
    Object result = apiInstance.productSpecificationFixedTextGetProductSpecificationFixedTexts(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationFixedTextApi#productSpecificationFixedTextGetProductSpecificationFixedTexts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredProductSpecificationFixedTextQuery**](GetFilteredProductSpecificationFixedTextQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="productSpecificationFixedTextUpdateProductSpecificationFixedText"></a>
# **productSpecificationFixedTextUpdateProductSpecificationFixedText**
> Object productSpecificationFixedTextUpdateProductSpecificationFixedText(productSpecificationFixedTextId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProductSpecificationFixedTextApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

ProductSpecificationFixedTextApi apiInstance = new ProductSpecificationFixedTextApi();
Integer productSpecificationFixedTextId = 56; // Integer | 
ProductSpecificationFixedTextDto dto = new ProductSpecificationFixedTextDto(); // ProductSpecificationFixedTextDto | 
try {
    Object result = apiInstance.productSpecificationFixedTextUpdateProductSpecificationFixedText(productSpecificationFixedTextId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductSpecificationFixedTextApi#productSpecificationFixedTextUpdateProductSpecificationFixedText");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productSpecificationFixedTextId** | **Integer**|  |
 **dto** | [**ProductSpecificationFixedTextDto**](ProductSpecificationFixedTextDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

