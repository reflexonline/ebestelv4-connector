
# LineSupplierDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplierId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



