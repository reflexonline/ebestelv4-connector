
# RelationVisitAddressDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Integer** |  | 
**name** | **String** |  |  [optional]
**country** | **String** |  |  [optional]
**phoneNumber** | **String** |  |  [optional]
**faxNumber** | **String** |  |  [optional]
**addressLine1** | **String** |  |  [optional]
**postalCode** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**isoCountryCode** | **String** |  |  [optional]
**addressLine2** | **String** |  |  [optional]
**marktSegment** | **Integer** |  |  [optional]
**longName** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



