
# GetFilteredVatScenarioQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scenarioIdMinValue** | **Integer** |  |  [optional]
**scenarioIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



