
# GetFilteredDeliveryMethodQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deliveryMethodIdMinValue** | **Integer** |  |  [optional]
**deliveryMethodIdMaxValue** | **Integer** |  |  [optional]
**descriptionMinValue** | **String** |  |  [optional]
**descriptionMaxValue** | **String** |  |  [optional]



