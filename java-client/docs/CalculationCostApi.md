# CalculationCostApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**calculationCostCreateCalculationCost**](CalculationCostApi.md#calculationCostCreateCalculationCost) | **POST** /CalculationCost | 
[**calculationCostDeleteCalculationCost**](CalculationCostApi.md#calculationCostDeleteCalculationCost) | **DELETE** /CalculationCost/{calculationCostId} | 
[**calculationCostExistsCalculationCost**](CalculationCostApi.md#calculationCostExistsCalculationCost) | **GET** /CalculationCost/exists/{calculationCostId} | 
[**calculationCostGetCalculationCost**](CalculationCostApi.md#calculationCostGetCalculationCost) | **GET** /CalculationCost/{calculationCostId} | 
[**calculationCostGetCalculationCosts**](CalculationCostApi.md#calculationCostGetCalculationCosts) | **POST** /CalculationCost/search | 
[**calculationCostUpdateCalculationCost**](CalculationCostApi.md#calculationCostUpdateCalculationCost) | **PUT** /CalculationCost/{calculationCostId} | 


<a name="calculationCostCreateCalculationCost"></a>
# **calculationCostCreateCalculationCost**
> Object calculationCostCreateCalculationCost(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CalculationCostApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CalculationCostApi apiInstance = new CalculationCostApi();
CalculationCostDto dto = new CalculationCostDto(); // CalculationCostDto | 
try {
    Object result = apiInstance.calculationCostCreateCalculationCost(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CalculationCostApi#calculationCostCreateCalculationCost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**CalculationCostDto**](CalculationCostDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="calculationCostDeleteCalculationCost"></a>
# **calculationCostDeleteCalculationCost**
> Object calculationCostDeleteCalculationCost(calculationCostId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CalculationCostApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CalculationCostApi apiInstance = new CalculationCostApi();
Integer calculationCostId = 56; // Integer | 
try {
    Object result = apiInstance.calculationCostDeleteCalculationCost(calculationCostId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CalculationCostApi#calculationCostDeleteCalculationCost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calculationCostId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="calculationCostExistsCalculationCost"></a>
# **calculationCostExistsCalculationCost**
> Object calculationCostExistsCalculationCost(calculationCostId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CalculationCostApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CalculationCostApi apiInstance = new CalculationCostApi();
Integer calculationCostId = 56; // Integer | 
try {
    Object result = apiInstance.calculationCostExistsCalculationCost(calculationCostId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CalculationCostApi#calculationCostExistsCalculationCost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calculationCostId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="calculationCostGetCalculationCost"></a>
# **calculationCostGetCalculationCost**
> Object calculationCostGetCalculationCost(calculationCostId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CalculationCostApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CalculationCostApi apiInstance = new CalculationCostApi();
Integer calculationCostId = 56; // Integer | 
try {
    Object result = apiInstance.calculationCostGetCalculationCost(calculationCostId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CalculationCostApi#calculationCostGetCalculationCost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calculationCostId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="calculationCostGetCalculationCosts"></a>
# **calculationCostGetCalculationCosts**
> Object calculationCostGetCalculationCosts(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CalculationCostApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CalculationCostApi apiInstance = new CalculationCostApi();
GetFilteredCalculationCostQuery query = new GetFilteredCalculationCostQuery(); // GetFilteredCalculationCostQuery | 
try {
    Object result = apiInstance.calculationCostGetCalculationCosts(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CalculationCostApi#calculationCostGetCalculationCosts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredCalculationCostQuery**](GetFilteredCalculationCostQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="calculationCostUpdateCalculationCost"></a>
# **calculationCostUpdateCalculationCost**
> Object calculationCostUpdateCalculationCost(calculationCostId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CalculationCostApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CalculationCostApi apiInstance = new CalculationCostApi();
Integer calculationCostId = 56; // Integer | 
CalculationCostDto dto = new CalculationCostDto(); // CalculationCostDto | 
try {
    Object result = apiInstance.calculationCostUpdateCalculationCost(calculationCostId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CalculationCostApi#calculationCostUpdateCalculationCost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calculationCostId** | **Integer**|  |
 **dto** | [**CalculationCostDto**](CalculationCostDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

