# SaleOrderApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saleOrderCreateSaleOrder**](SaleOrderApi.md#saleOrderCreateSaleOrder) | **POST** /Customer/{customerId}/SaleOrder | 
[**saleOrderCreateSaleOrderArticleLine**](SaleOrderApi.md#saleOrderCreateSaleOrderArticleLine) | **POST** /SaleOrder/{saleOrderId}/ArticleLine | 
[**saleOrderCreateSaleOrderTextLine**](SaleOrderApi.md#saleOrderCreateSaleOrderTextLine) | **POST** /SaleOrder/{saleOrderId}/TextLine | 
[**saleOrderDeleteSaleOrderArticleLine**](SaleOrderApi.md#saleOrderDeleteSaleOrderArticleLine) | **DELETE** /SaleOrder/{SaleOrderId}/ArticleLine/{Lineid} | 
[**saleOrderDeleteSaleOrderTextLine**](SaleOrderApi.md#saleOrderDeleteSaleOrderTextLine) | **DELETE** /SaleOrder/{SaleOrderId}/TextLine/{lineId} | 
[**saleOrderExistsSaleOrder**](SaleOrderApi.md#saleOrderExistsSaleOrder) | **GET** /SaleOrder/exists/{orderNumber} | 
[**saleOrderGetSaleOrder**](SaleOrderApi.md#saleOrderGetSaleOrder) | **GET** /SaleOrder/{orderNumber} | 
[**saleOrderGetSaleOrders**](SaleOrderApi.md#saleOrderGetSaleOrders) | **POST** /SaleOrder/search | 
[**saleOrderUpdateSaleOrderArticleLine**](SaleOrderApi.md#saleOrderUpdateSaleOrderArticleLine) | **PUT** /SaleOrder/{SaleOrderId}/ArticleLine/{Lineid} | 
[**saleOrderUpdateSaleOrderTextLine**](SaleOrderApi.md#saleOrderUpdateSaleOrderTextLine) | **PUT** /SaleOrder/{SaleOrderId}/TextLine/{lineId} | 


<a name="saleOrderCreateSaleOrder"></a>
# **saleOrderCreateSaleOrder**
> Object saleOrderCreateSaleOrder(customerId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderApi apiInstance = new SaleOrderApi();
Integer customerId = 56; // Integer | 
CreateSaleOrderDto dto = new CreateSaleOrderDto(); // CreateSaleOrderDto | 
try {
    Object result = apiInstance.saleOrderCreateSaleOrder(customerId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderApi#saleOrderCreateSaleOrder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **dto** | [**CreateSaleOrderDto**](CreateSaleOrderDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleOrderCreateSaleOrderArticleLine"></a>
# **saleOrderCreateSaleOrderArticleLine**
> Object saleOrderCreateSaleOrderArticleLine(saleOrderId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderApi apiInstance = new SaleOrderApi();
Integer saleOrderId = 56; // Integer | 
CreateSaleOrderArticleLineDto dto = new CreateSaleOrderArticleLineDto(); // CreateSaleOrderArticleLineDto | 
try {
    Object result = apiInstance.saleOrderCreateSaleOrderArticleLine(saleOrderId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderApi#saleOrderCreateSaleOrderArticleLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleOrderId** | **Integer**|  |
 **dto** | [**CreateSaleOrderArticleLineDto**](CreateSaleOrderArticleLineDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleOrderCreateSaleOrderTextLine"></a>
# **saleOrderCreateSaleOrderTextLine**
> Object saleOrderCreateSaleOrderTextLine(saleOrderId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderApi apiInstance = new SaleOrderApi();
Integer saleOrderId = 56; // Integer | 
CreateSaleOrderTextLineDto dto = new CreateSaleOrderTextLineDto(); // CreateSaleOrderTextLineDto | 
try {
    Object result = apiInstance.saleOrderCreateSaleOrderTextLine(saleOrderId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderApi#saleOrderCreateSaleOrderTextLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleOrderId** | **Integer**|  |
 **dto** | [**CreateSaleOrderTextLineDto**](CreateSaleOrderTextLineDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleOrderDeleteSaleOrderArticleLine"></a>
# **saleOrderDeleteSaleOrderArticleLine**
> Object saleOrderDeleteSaleOrderArticleLine(saleOrderId, lineid)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderApi apiInstance = new SaleOrderApi();
Integer saleOrderId = 56; // Integer | 
Integer lineid = 56; // Integer | 
try {
    Object result = apiInstance.saleOrderDeleteSaleOrderArticleLine(saleOrderId, lineid);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderApi#saleOrderDeleteSaleOrderArticleLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleOrderId** | **Integer**|  |
 **lineid** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleOrderDeleteSaleOrderTextLine"></a>
# **saleOrderDeleteSaleOrderTextLine**
> Object saleOrderDeleteSaleOrderTextLine(saleOrderId, lineId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderApi apiInstance = new SaleOrderApi();
Integer saleOrderId = 56; // Integer | 
Integer lineId = 56; // Integer | 
try {
    Object result = apiInstance.saleOrderDeleteSaleOrderTextLine(saleOrderId, lineId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderApi#saleOrderDeleteSaleOrderTextLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleOrderId** | **Integer**|  |
 **lineId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleOrderExistsSaleOrder"></a>
# **saleOrderExistsSaleOrder**
> Object saleOrderExistsSaleOrder(orderNumber)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderApi apiInstance = new SaleOrderApi();
Integer orderNumber = 56; // Integer | 
try {
    Object result = apiInstance.saleOrderExistsSaleOrder(orderNumber);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderApi#saleOrderExistsSaleOrder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleOrderGetSaleOrder"></a>
# **saleOrderGetSaleOrder**
> Object saleOrderGetSaleOrder(orderNumber)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderApi apiInstance = new SaleOrderApi();
Integer orderNumber = 56; // Integer | 
try {
    Object result = apiInstance.saleOrderGetSaleOrder(orderNumber);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderApi#saleOrderGetSaleOrder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderNumber** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="saleOrderGetSaleOrders"></a>
# **saleOrderGetSaleOrders**
> Object saleOrderGetSaleOrders(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderApi apiInstance = new SaleOrderApi();
GetFilteredSaleOrderQuery query = new GetFilteredSaleOrderQuery(); // GetFilteredSaleOrderQuery | 
try {
    Object result = apiInstance.saleOrderGetSaleOrders(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderApi#saleOrderGetSaleOrders");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredSaleOrderQuery**](GetFilteredSaleOrderQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleOrderUpdateSaleOrderArticleLine"></a>
# **saleOrderUpdateSaleOrderArticleLine**
> Object saleOrderUpdateSaleOrderArticleLine(saleOrderId, lineid, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderApi apiInstance = new SaleOrderApi();
Integer saleOrderId = 56; // Integer | 
Integer lineid = 56; // Integer | 
UpdateSaleOrderArticleLineDto dto = new UpdateSaleOrderArticleLineDto(); // UpdateSaleOrderArticleLineDto | 
try {
    Object result = apiInstance.saleOrderUpdateSaleOrderArticleLine(saleOrderId, lineid, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderApi#saleOrderUpdateSaleOrderArticleLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleOrderId** | **Integer**|  |
 **lineid** | **Integer**|  |
 **dto** | [**UpdateSaleOrderArticleLineDto**](UpdateSaleOrderArticleLineDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="saleOrderUpdateSaleOrderTextLine"></a>
# **saleOrderUpdateSaleOrderTextLine**
> Object saleOrderUpdateSaleOrderTextLine(saleOrderId, lineId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SaleOrderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

SaleOrderApi apiInstance = new SaleOrderApi();
Integer saleOrderId = 56; // Integer | 
Integer lineId = 56; // Integer | 
UpdateSaleOrderTextLineDto dto = new UpdateSaleOrderTextLineDto(); // UpdateSaleOrderTextLineDto | 
try {
    Object result = apiInstance.saleOrderUpdateSaleOrderTextLine(saleOrderId, lineId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SaleOrderApi#saleOrderUpdateSaleOrderTextLine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saleOrderId** | **Integer**|  |
 **lineId** | **Integer**|  |
 **dto** | [**UpdateSaleOrderTextLineDto**](UpdateSaleOrderTextLineDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

