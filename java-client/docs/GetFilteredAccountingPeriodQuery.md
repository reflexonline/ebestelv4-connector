
# GetFilteredAccountingPeriodQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**yearMinValue** | **Integer** |  |  [optional]
**yearMaxValue** | **Integer** |  |  [optional]
**periodMinValue** | **Integer** |  |  [optional]
**periodMaxValue** | **Integer** |  |  [optional]
**fromDateMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**fromDateMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**untilDateMinValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**untilDateMaxValue** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]



