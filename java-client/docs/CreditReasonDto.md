
# CreditReasonDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creditReasonId** | **Integer** |  | 
**number** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



