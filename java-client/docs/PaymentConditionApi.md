# PaymentConditionApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentConditionCreatePaymentCondition**](PaymentConditionApi.md#paymentConditionCreatePaymentCondition) | **POST** /PaymentCondition | 
[**paymentConditionDeletePaymentCondition**](PaymentConditionApi.md#paymentConditionDeletePaymentCondition) | **DELETE** /PaymentCondition/{paymentConditionId} | 
[**paymentConditionExistsPaymentCondition**](PaymentConditionApi.md#paymentConditionExistsPaymentCondition) | **GET** /PaymentCondition/exists/{paymentConditionId} | 
[**paymentConditionGetPaymentCondition**](PaymentConditionApi.md#paymentConditionGetPaymentCondition) | **GET** /PaymentCondition/{paymentConditionId} | 
[**paymentConditionGetPaymentConditions**](PaymentConditionApi.md#paymentConditionGetPaymentConditions) | **POST** /PaymentCondition/search | 
[**paymentConditionUpdatePaymentCondition**](PaymentConditionApi.md#paymentConditionUpdatePaymentCondition) | **PUT** /PaymentCondition/{paymentConditionId} | 


<a name="paymentConditionCreatePaymentCondition"></a>
# **paymentConditionCreatePaymentCondition**
> Object paymentConditionCreatePaymentCondition(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentConditionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PaymentConditionApi apiInstance = new PaymentConditionApi();
PaymentConditionDto dto = new PaymentConditionDto(); // PaymentConditionDto | 
try {
    Object result = apiInstance.paymentConditionCreatePaymentCondition(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentConditionApi#paymentConditionCreatePaymentCondition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**PaymentConditionDto**](PaymentConditionDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="paymentConditionDeletePaymentCondition"></a>
# **paymentConditionDeletePaymentCondition**
> Object paymentConditionDeletePaymentCondition(paymentConditionId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentConditionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PaymentConditionApi apiInstance = new PaymentConditionApi();
Integer paymentConditionId = 56; // Integer | 
try {
    Object result = apiInstance.paymentConditionDeletePaymentCondition(paymentConditionId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentConditionApi#paymentConditionDeletePaymentCondition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **paymentConditionId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="paymentConditionExistsPaymentCondition"></a>
# **paymentConditionExistsPaymentCondition**
> Object paymentConditionExistsPaymentCondition(paymentConditionId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentConditionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PaymentConditionApi apiInstance = new PaymentConditionApi();
Integer paymentConditionId = 56; // Integer | 
try {
    Object result = apiInstance.paymentConditionExistsPaymentCondition(paymentConditionId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentConditionApi#paymentConditionExistsPaymentCondition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **paymentConditionId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="paymentConditionGetPaymentCondition"></a>
# **paymentConditionGetPaymentCondition**
> Object paymentConditionGetPaymentCondition(paymentConditionId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentConditionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PaymentConditionApi apiInstance = new PaymentConditionApi();
Integer paymentConditionId = 56; // Integer | 
try {
    Object result = apiInstance.paymentConditionGetPaymentCondition(paymentConditionId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentConditionApi#paymentConditionGetPaymentCondition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **paymentConditionId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="paymentConditionGetPaymentConditions"></a>
# **paymentConditionGetPaymentConditions**
> Object paymentConditionGetPaymentConditions(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentConditionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PaymentConditionApi apiInstance = new PaymentConditionApi();
GetFilteredPaymentConditionQuery query = new GetFilteredPaymentConditionQuery(); // GetFilteredPaymentConditionQuery | 
try {
    Object result = apiInstance.paymentConditionGetPaymentConditions(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentConditionApi#paymentConditionGetPaymentConditions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredPaymentConditionQuery**](GetFilteredPaymentConditionQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="paymentConditionUpdatePaymentCondition"></a>
# **paymentConditionUpdatePaymentCondition**
> Object paymentConditionUpdatePaymentCondition(paymentConditionId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PaymentConditionApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

PaymentConditionApi apiInstance = new PaymentConditionApi();
Integer paymentConditionId = 56; // Integer | 
PaymentConditionDto dto = new PaymentConditionDto(); // PaymentConditionDto | 
try {
    Object result = apiInstance.paymentConditionUpdatePaymentCondition(paymentConditionId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentConditionApi#paymentConditionUpdatePaymentCondition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **paymentConditionId** | **Integer**|  |
 **dto** | [**PaymentConditionDto**](PaymentConditionDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

