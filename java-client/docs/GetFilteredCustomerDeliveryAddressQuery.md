
# GetFilteredCustomerDeliveryAddressQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerIdMinValue** | **Integer** |  |  [optional]
**customerIdMaxValue** | **Integer** |  |  [optional]
**searchNameMinValue** | **String** |  |  [optional]
**searchNameMaxValue** | **String** |  |  [optional]
**searchDeliveryPostalCodeMinValue** | **String** |  |  [optional]
**searchDeliveryPostalCodeMaxValue** | **String** |  |  [optional]
**searchDeliveryCityMinValue** | **String** |  |  [optional]
**searchDeliveryCityMaxValue** | **String** |  |  [optional]
**deliveryMethodIdMinValue** | **Integer** |  |  [optional]
**deliveryMethodIdMaxValue** | **Integer** |  |  [optional]



