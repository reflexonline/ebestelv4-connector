# CustomerSaleArticleApi

All URIs are relative to *http://localhost:7000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerSaleArticleCreateCustomerSaleArticle**](CustomerSaleArticleApi.md#customerSaleArticleCreateCustomerSaleArticle) | **POST** /CustomerSaleArticle | 
[**customerSaleArticleDeleteCustomerSaleArticle**](CustomerSaleArticleApi.md#customerSaleArticleDeleteCustomerSaleArticle) | **DELETE** /CustomerSaleArticle/{customerId}/{articleId} | 
[**customerSaleArticleExistsCustomerSaleArticle**](CustomerSaleArticleApi.md#customerSaleArticleExistsCustomerSaleArticle) | **GET** /CustomerSaleArticle/exists/{customerId}/{articleId} | 
[**customerSaleArticleGetCustomerSaleArticle**](CustomerSaleArticleApi.md#customerSaleArticleGetCustomerSaleArticle) | **GET** /CustomerSaleArticle/{customerId}/{articleId} | 
[**customerSaleArticleGetCustomerSaleArticles**](CustomerSaleArticleApi.md#customerSaleArticleGetCustomerSaleArticles) | **POST** /CustomerSaleArticle/search | 
[**customerSaleArticleUpdateCustomerSaleArticle**](CustomerSaleArticleApi.md#customerSaleArticleUpdateCustomerSaleArticle) | **PUT** /CustomerSaleArticle/{customerId}/{articleId} | 


<a name="customerSaleArticleCreateCustomerSaleArticle"></a>
# **customerSaleArticleCreateCustomerSaleArticle**
> Object customerSaleArticleCreateCustomerSaleArticle(dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerSaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CustomerSaleArticleApi apiInstance = new CustomerSaleArticleApi();
CustomerSaleArticleDto dto = new CustomerSaleArticleDto(); // CustomerSaleArticleDto | 
try {
    Object result = apiInstance.customerSaleArticleCreateCustomerSaleArticle(dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerSaleArticleApi#customerSaleArticleCreateCustomerSaleArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dto** | [**CustomerSaleArticleDto**](CustomerSaleArticleDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="customerSaleArticleDeleteCustomerSaleArticle"></a>
# **customerSaleArticleDeleteCustomerSaleArticle**
> Object customerSaleArticleDeleteCustomerSaleArticle(customerId, articleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerSaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CustomerSaleArticleApi apiInstance = new CustomerSaleArticleApi();
Integer customerId = 56; // Integer | 
Integer articleId = 56; // Integer | 
try {
    Object result = apiInstance.customerSaleArticleDeleteCustomerSaleArticle(customerId, articleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerSaleArticleApi#customerSaleArticleDeleteCustomerSaleArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **articleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="customerSaleArticleExistsCustomerSaleArticle"></a>
# **customerSaleArticleExistsCustomerSaleArticle**
> Object customerSaleArticleExistsCustomerSaleArticle(customerId, articleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerSaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CustomerSaleArticleApi apiInstance = new CustomerSaleArticleApi();
Integer customerId = 56; // Integer | 
Integer articleId = 56; // Integer | 
try {
    Object result = apiInstance.customerSaleArticleExistsCustomerSaleArticle(customerId, articleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerSaleArticleApi#customerSaleArticleExistsCustomerSaleArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **articleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="customerSaleArticleGetCustomerSaleArticle"></a>
# **customerSaleArticleGetCustomerSaleArticle**
> Object customerSaleArticleGetCustomerSaleArticle(customerId, articleId)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerSaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CustomerSaleArticleApi apiInstance = new CustomerSaleArticleApi();
Integer customerId = 56; // Integer | 
Integer articleId = 56; // Integer | 
try {
    Object result = apiInstance.customerSaleArticleGetCustomerSaleArticle(customerId, articleId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerSaleArticleApi#customerSaleArticleGetCustomerSaleArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **articleId** | **Integer**|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

<a name="customerSaleArticleGetCustomerSaleArticles"></a>
# **customerSaleArticleGetCustomerSaleArticles**
> Object customerSaleArticleGetCustomerSaleArticles(query)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerSaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CustomerSaleArticleApi apiInstance = new CustomerSaleArticleApi();
GetFilteredCustomerSaleArticleQuery query = new GetFilteredCustomerSaleArticleQuery(); // GetFilteredCustomerSaleArticleQuery | 
try {
    Object result = apiInstance.customerSaleArticleGetCustomerSaleArticles(query);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerSaleArticleApi#customerSaleArticleGetCustomerSaleArticles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | [**GetFilteredCustomerSaleArticleQuery**](GetFilteredCustomerSaleArticleQuery.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

<a name="customerSaleArticleUpdateCustomerSaleArticle"></a>
# **customerSaleArticleUpdateCustomerSaleArticle**
> Object customerSaleArticleUpdateCustomerSaleArticle(customerId, articleId, dto)



### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomerSaleArticleApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: oauth2
OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
oauth2.setAccessToken("YOUR ACCESS TOKEN");

CustomerSaleArticleApi apiInstance = new CustomerSaleArticleApi();
Integer customerId = 56; // Integer | 
Integer articleId = 56; // Integer | 
CustomerSaleArticleDto dto = new CustomerSaleArticleDto(); // CustomerSaleArticleDto | 
try {
    Object result = apiInstance.customerSaleArticleUpdateCustomerSaleArticle(customerId, articleId, dto);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerSaleArticleApi#customerSaleArticleUpdateCustomerSaleArticle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **Integer**|  |
 **articleId** | **Integer**|  |
 **dto** | [**CustomerSaleArticleDto**](CustomerSaleArticleDto.md)|  |

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

