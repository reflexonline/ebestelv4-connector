
# PriceListDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**argement** | [**List&lt;LineSaleArticlePriceListAgreementDto&gt;**](LineSaleArticlePriceListAgreementDto.md) |  |  [optional]
**salePriceListId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**hasArticleSelection** | **Boolean** |  |  [optional]
**baseOfPrice** | **Integer** |  |  [optional]
**operations** | **List&lt;String&gt;** |  |  [optional]



