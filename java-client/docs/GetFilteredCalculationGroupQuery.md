
# GetFilteredCalculationGroupQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**calculationGroupIdMinValue** | **Integer** |  |  [optional]
**calculationGroupIdMaxValue** | **Integer** |  |  [optional]



