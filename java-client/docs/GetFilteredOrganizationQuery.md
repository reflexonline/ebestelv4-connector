
# GetFilteredOrganizationQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**organizationIdMinValue** | **Integer** |  |  [optional]
**organizationIdMaxValue** | **Integer** |  |  [optional]
**nameMinValue** | **String** |  |  [optional]
**nameMaxValue** | **String** |  |  [optional]



