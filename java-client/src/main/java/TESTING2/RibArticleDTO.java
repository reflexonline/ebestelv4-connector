package TESTING2;

public class RibArticleDTO {

	Number product_number;
    String name;
    Number purchase_price;
    Number sale_price;
    String ean_number_cu;
    String unit;
    String vat;
    String brand;
    String packaging;
    Number is_stock_article;
    Number is_packaging_article;
    Number is_weight_article;
    Number quality;
    Number portion_weight_lower_bound;
    Number portion_weight_upper_bound;
    Number portion_weight;
    String product_specification_url;
    Number block_customer_discount;	
	
	
	public RibArticleDTO() {
	}

	public Number getProductNumber() {
		return product_number;
	}

	public void setProductNumber(Number productNumber) {
		this.product_number = productNumber;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Number getPurchasePrice() {
		return purchase_price;
	}

	public void setPurchasePrice(Number purchasePrice) {
		this.purchase_price = purchasePrice;
	}
	
	public Number getSalePrice() {
		return sale_price;
	}

	public void setSalePrice(Number salePrice) {
		this.sale_price = salePrice;
	}
	
	public String getEanNumberCu() {
		return ean_number_cu;
	}

	public void setEanNumberCu(String eanNumberCu) {
		this.ean_number_cu = eanNumberCu;
	}
	
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public String getVat() {
		return vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}
	
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public String getPackaging() {
		return packaging;
	}

	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}
	
	public Number getIsStockArticle() {
		return is_stock_article;
	}

	public void setIsStockArticle(Number isStockArticle) {
		this.is_stock_article = isStockArticle;
	}
	
	public Number getIsPackagingArticle() {
		return is_packaging_article;
	}

	public void setIsPackagingArticle(Number isPackagingArticle) {
		this.is_packaging_article = isPackagingArticle;
	}
	
	public Number getIsWeightArticle() {
		return is_weight_article;
	}

	public void setIsWeightArticle(Number isWeightArticle) {
		this.is_weight_article = isWeightArticle;
	}
	
	public Number getQuality() {
		return quality;
	}

	public void setQuality(Number quality) {
		this.quality = quality;
	}
	
	public Number getPortionWeightLowerBound() {
		return portion_weight_lower_bound;
	}

	public void setPortionWeightLowerBound(Number portionWeightLowerBound) {
		this.portion_weight_lower_bound = portionWeightLowerBound;
	}
	
	public Number getPortionWeightUpperBound() {
		return portion_weight_upper_bound;
	}

	public void setPortionWeightUpperBound(Number portionWeightUpperBound) {
		this.portion_weight_upper_bound = portionWeightUpperBound;
	}
	
	public Number getPortionWeight() {
		return portion_weight;
	}

	public void setPortionWeight(Number portionWeight) {
		this.portion_weight = portionWeight;
	}
	
	public String getProductSpecificationUrl() {
		return product_specification_url;
	}

	public void setProductSpecificationUrl(String productSpecificationUrl) {
		this.product_specification_url = productSpecificationUrl;
	}
	
	public Number getBlockCustomerDiscount() {
		return block_customer_discount;
	}

	public void setBlockCustomerDiscount(Number blockCustomerDiscount) {
		this.block_customer_discount = blockCustomerDiscount;
	}
	
}
