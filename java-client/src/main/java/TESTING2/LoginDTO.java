package TESTING2;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class LoginDTO {

	String userName;
	String password;
	String grant_type;

	public LoginDTO(String clientUsername, String clientPassword, String clientGrantType) {
		this.userName = clientUsername;
		this.password = clientPassword;
		this.grant_type = clientGrantType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGrantType() {
		return grant_type;
	}

	public void setGrantType(String grantType) {
		this.grant_type = grantType;
	}

    public String getFormEncodedContent()
    {
        try {
            String contentStr = "grant_type=" + "password";
            contentStr += "&username=" + URLEncoder.encode( userName, StandardCharsets.UTF_8.toString());
            contentStr += "&password=" + URLEncoder.encode( password, StandardCharsets.UTF_8.toString());
            return contentStr;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
	
}
