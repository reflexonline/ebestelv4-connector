package TESTING2;

import com.google.gson.Gson;
import json2pojo.*;
import java.util.ArrayList;


class RibGetArticle {
	
	private String environment;
	
	public RibGetArticle(String env) {
		environment = env;
	}
	
	public void getArticle(int number) throws NoSuchFieldException {
		getApiResponse apiResp = new getApiResponse();
		Object response = null;
		if (environment == "r3k") {
			response = apiResp.getSaleArticleResponse(number);
		}
		Gson gson = new Gson();
        String json = gson.toJson(response);

		//target
		RibArticleDTO ribArticle = new RibArticleDTO();
		if (environment == "r3k") {	
			//source
			SaleArticle r3kArticle = gson.fromJson(json, SaleArticle.class);
			mapArticleR3k2Rib(r3kArticle, ribArticle);
		}
	}
	
	private void mapArticleR3k2Rib(SaleArticle r3kArticle, RibArticleDTO ribArticle) {

	    ArrayList<String> vatArr = new ArrayList<String>();
	    vatArr.add("");
	    vatArr.add("L");
	    vatArr.add("H");
	    vatArr.add("");
	    vatArr.add("");
	    vatArr.add("E");
	    vatArr.add("");
		
	    ArrayList<String> unitArr = new ArrayList<String>();
	    unitArr.add("S");
	    unitArr.add("G");
	    unitArr.add("g");
	    
		ribArticle.setProductNumber(r3kArticle.getNumber());
		ribArticle.setName(r3kArticle.getLongName());
		ribArticle.setPurchasePrice(r3kArticle.getPurchasePrice());
		ribArticle.setSalePrice(r3kArticle.getSalePrice());
		ribArticle.setEanNumberCu(r3kArticle.getEanRetailUnitCode());
		ribArticle.setUnit(unitArr.get(r3kArticle.getUnitType().intValue()));
		ribArticle.setVat(vatArr.get(r3kArticle.getVatCode().intValue()));
		ribArticle.setBrand(r3kArticle.getBrand());
        ribArticle.setPackaging(r3kArticle.getPackingType()); 
        ribArticle.setIsStockArticle(r3kArticle.getKeepStock());   // this must be checked !!!
        ribArticle.setIsPackagingArticle(ribArticle.getVat() == "E" ? 1 : 0);
        ribArticle.setIsWeightArticle(ribArticle.getUnit() != "S" ? 1 : 0);
        ribArticle.setQuality(r3kArticle.getStorageState());
        ribArticle.setPortionWeightLowerBound(r3kArticle.getMinimumWeight());
        ribArticle.setPortionWeightUpperBound(r3kArticle.getMaximumWeight());
        ribArticle.setPortionWeight(r3kArticle.getPortionWeight());
        ribArticle.setProductSpecificationUrl(r3kArticle.getProductSpecificationUrl());
        ribArticle.setBlockCustomerDiscount(r3kArticle.getBlockCustomerDiscount().charAt(0) == 'N' ? 0 : 1);   // this must be checked !!!
		
		System.out.println("ribArticle.getProductNumber(): " + ribArticle.getProductNumber());
		System.out.println("ribArticle.getName(): " + ribArticle.getName());
		System.out.println("ribArticle.getPurchasePrice(): " + ribArticle.getPurchasePrice());
		System.out.println("ribArticle.getSalePrice(): " + ribArticle.getSalePrice());
		System.out.println("ribArticle.getEanNumberCu(): " + ribArticle.getEanNumberCu());
		System.out.println("ribArticle.getUnit(): " + ribArticle.getUnit());
		System.out.println("ribArticle.getVat(): " + ribArticle.getVat());
		System.out.println("ribArticle.getBrand(): " + ribArticle.getBrand());
		System.out.println("ribArticle.getPackaging(): " + ribArticle.getPackaging());
		System.out.println("ribArticle.getIsStockArticle(): " + ribArticle.getIsStockArticle());
		System.out.println("ribArticle.getIsPackagingArticle(): " + ribArticle.getIsPackagingArticle());
		System.out.println("ribArticle.getIsWeightArticle(): " + ribArticle.getIsWeightArticle());
		System.out.println("ribArticle.getQuality(): " + ribArticle.getQuality());
		System.out.println("ribArticle.getPortionWeightLowerBound(): " + ribArticle.getPortionWeightLowerBound());
		System.out.println("ribArticle.getPortionWeightUpperBound(): " + ribArticle.getPortionWeightUpperBound());
		System.out.println("ribArticle.getPortionWeight(): " + ribArticle.getPortionWeight());
		System.out.println("ribArticle.getProductSpecificationUrl(): " + ribArticle.getProductSpecificationUrl());
		System.out.println("ribArticle.getBlockCustomerDiscount(): " + ribArticle.getBlockCustomerDiscount());
		
	}
	
}
