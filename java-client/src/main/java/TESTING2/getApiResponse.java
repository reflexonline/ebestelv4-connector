package TESTING2;

import java.util.Properties;

import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.Configuration;
import io.swagger.client.auth.*;
import io.swagger.client.api.SaleArticleApi;


public class getApiResponse {

	private void getBlueAccess() {
		
	}
	
	private void getR3kAccess() {
		//TODO: use of refresh_token
		Properties appProps = new Properties();
		appProps.put("reflex.apiurl", "http://localhost:7000");
		appProps.put("reflex.clientidentifier", "reflex");
		appProps.put("reflex.user", "ADMIN");
		appProps.put("reflex.password", "REFLEX");
		appProps.put("reflex.granttype", "password");
		RestConsumerService resConServ = new RestConsumerService(appProps);

		ApiClient apiCl = Configuration.getDefaultApiClient();

		// Configure OAuth2 access token for authorization: oauth2
		OAuth oauth2 = (OAuth) apiCl.getAuthentication("oauth2");
		oauth2.setAccessToken(resConServ.getToken());
	}
	
	public Object getSaleArticleResponse(Integer number) throws NoSuchFieldException {
		getR3kAccess();

		SaleArticleApi apiInstance = new SaleArticleApi();
		try {
			Object result = apiInstance.saleArticleGetSaleArticle(number);
			return result;
		} catch (ApiException e) {
			System.err.println("Exception when calling SaleArticleApi#saleArticleGetSaleArticle");
			e.printStackTrace();
		}
		
		return null;
	}
	
}