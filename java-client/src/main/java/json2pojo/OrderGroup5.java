
package json2pojo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "saleArticleOrderGroupId",
    "exists",
    "description"
})
public class OrderGroup5 {

    @JsonProperty("saleArticleOrderGroupId")
    private Double saleArticleOrderGroupId;
    @JsonProperty("exists")
    private Boolean exists;
    @JsonProperty("description")
    private String description;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("saleArticleOrderGroupId")
    public Double getSaleArticleOrderGroupId() {
        return saleArticleOrderGroupId;
    }

    @JsonProperty("saleArticleOrderGroupId")
    public void setSaleArticleOrderGroupId(Double saleArticleOrderGroupId) {
        this.saleArticleOrderGroupId = saleArticleOrderGroupId;
    }

    @JsonProperty("exists")
    public Boolean getExists() {
        return exists;
    }

    @JsonProperty("exists")
    public void setExists(Boolean exists) {
        this.exists = exists;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
