
package json2pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "allergen",
    "manual",
    "print",
    "traces",
    "threshold",
    "value",
    "operations"
})
public class Allergen {

    @JsonProperty("allergen")
    private Allergen_ allergen;
    @JsonProperty("manual")
    private Boolean manual;
    @JsonProperty("print")
    private Boolean print;
    @JsonProperty("traces")
    private Boolean traces;
    @JsonProperty("threshold")
    private Double threshold;
    @JsonProperty("value")
    private Double value;
    @JsonProperty("operations")
    private List<Object> operations = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("allergen")
    public Allergen_ getAllergen() {
        return allergen;
    }

    @JsonProperty("allergen")
    public void setAllergen(Allergen_ allergen) {
        this.allergen = allergen;
    }

    @JsonProperty("manual")
    public Boolean getManual() {
        return manual;
    }

    @JsonProperty("manual")
    public void setManual(Boolean manual) {
        this.manual = manual;
    }

    @JsonProperty("print")
    public Boolean getPrint() {
        return print;
    }

    @JsonProperty("print")
    public void setPrint(Boolean print) {
        this.print = print;
    }

    @JsonProperty("traces")
    public Boolean getTraces() {
        return traces;
    }

    @JsonProperty("traces")
    public void setTraces(Boolean traces) {
        this.traces = traces;
    }

    @JsonProperty("threshold")
    public Double getThreshold() {
        return threshold;
    }

    @JsonProperty("threshold")
    public void setThreshold(Double threshold) {
        this.threshold = threshold;
    }

    @JsonProperty("value")
    public Double getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(Double value) {
        this.value = value;
    }

    @JsonProperty("operations")
    public List<Object> getOperations() {
        return operations;
    }

    @JsonProperty("operations")
    public void setOperations(List<Object> operations) {
        this.operations = operations;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
