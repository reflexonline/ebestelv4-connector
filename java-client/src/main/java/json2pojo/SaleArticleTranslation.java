
package json2pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "languageId",
    "description",
    "operations"
})
public class SaleArticleTranslation {

    @JsonProperty("languageId")
    private Double languageId;
    @JsonProperty("description")
    private String description;
    @JsonProperty("operations")
    private List<String> operations = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("languageId")
    public Double getLanguageId() {
        return languageId;
    }

    @JsonProperty("languageId")
    public void setLanguageId(Double languageId) {
        this.languageId = languageId;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("operations")
    public List<String> getOperations() {
        return operations;
    }

    @JsonProperty("operations")
    public void setOperations(List<String> operations) {
        this.operations = operations;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
