
package json2pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "productSpecificationUrl",
    "saleArticleTranslation",
    "tag",
    "allergens",
    "nutritionals",
    "number",
    "shortName",
    "longName",
    "inkpOld",
    "verkpOld",
    "unitType",
    "vatCode",
    "tareWeight",
    "weighingFactor",
    "portionWeight",
    "processingLoss",
    "orderGroup1",
    "bestBeforeDateFresh",
    "keepStock",
    "saleArticleGroup",
    "pickLocation",
    "backorderPercentage",
    "packedPerUnit",
    "packedPerType",
    "ingredient1",
    "ingredient2",
    "ingredient3",
    "ingredient4",
    "ingredient5",
    "ingredient6",
    "ingredient7",
    "ingredient8",
    "packingType",
    "cbsGoodsCode",
    "storageTerms",
    "userDefinableField1",
    "userDefinableField2",
    "userDefinableField3",
    "userDefinableField4",
    "userDefinableField5",
    "purchaseArticle",
    "userPurchaseFactor",
    "stockAutoPurchaseArticle",
    "packingArticle1",
    "isEcommerceArticle",
    "orderGroup2",
    "labelNumber",
    "blockCustomerDiscount",
    "ingredient9",
    "ingredient10",
    "ingredient11",
    "ingredient12",
    "fixedStockLocation",
    "bestBeforeDateFrozen",
    "adviesVerkoopPrijsOu",
    "tipUpUnit1",
    "tipUpUnit2",
    "tipUpUnit3",
    "tipUpSurcharge1",
    "tipUpSurcharge2",
    "tipUpSurcharge3",
    "substituteArticle",
    "acknowledgement",
    "eanRetailUnitCode",
    "orderGroup3",
    "storageState",
    "orderGroup4",
    "orderGroup5",
    "calculationType",
    "priceOnPackingSlip",
    "isBlocked",
    "defaultOrderAtAdministrationBranch",
    "owhArticleIdBranch",
    "brand",
    "wrapping",
    "photoPath",
    "packingArticle2",
    "blockedFrom",
    "blockedUntil",
    "fixedSurcharge",
    "surchargeFactor",
    "excludeFromPriceList",
    "allowedPriceDeviation",
    "occMethod",
    "bonusParticipation",
    "subGroup",
    "deliveryTimeInversco",
    "quantityInPacking",
    "totalPackedWeight",
    "productionLabelBasedOnPortion",
    "vastFacPrijsOld",
    "consumerGroup",
    "substituteGroup",
    "wssAskPackingQuantity",
    "gflTracingCode",
    "guaranteedBestBeforeDays",
    "vertebraType",
    "manualIngredient1",
    "manualIngredient2",
    "manualIngredient3",
    "manualIngredient4",
    "manualIngredient5",
    "manualIngredient6",
    "manualIngredient7",
    "manualIngredient8",
    "manualIngredient9",
    "manualIngredient10",
    "manualIngredient11",
    "manualIngredient12",
    "gmoState",
    "gmoManual",
    "displayENumber",
    "displayAllergens",
    "productSpecificationNumber",
    "productSpecificationNumberRevision",
    "productSpecificationDate",
    "lastProductSpecNumber",
    "lastProductSpecNumberRevision",
    "lastProductSpecDate",
    "registrationWarning",
    "isOwhArticle",
    "usePortionAsCbsPortion",
    "cbsPortionWeight",
    "cbsSupplementaryUnitFactor",
    "occNumberOfLabels",
    "maxOrderQuantity",
    "owhDeliveryType",
    "eWeighingPortion",
    "usePortionAsEWeighingPort",
    "documentPath",
    "crateLabelNumber",
    "salePriceBasedOnConsumer",
    "registrationMethod",
    "minimumWeight",
    "maximumWeight",
    "createBackorderOnExcessiveStock",
    "createBackorderOnStockShortage",
    "backorderType",
    "boxQuantity",
    "boxesPerLayer",
    "layersPerPallet",
    "useGraiCode",
    "lengthOfGraiCode",
    "defProdMultiBranchId",
    "eanTradeUnitCode",
    "wssCheckMaximumRegistration",
    "trackPackingBalance",
    "checkMinMaxPortionWeightOnOrderEntry",
    "ssccRegisterMethod",
    "planningUnitType",
    "usePlanningUnitType",
    "palletLabelNumber",
    "countryOfOrigin",
    "analysisPurchasePriceType",
    "useAnalysisPurchasePriceBatch",
    "priceLookUpCode",
    "length",
    "width",
    "height",
    "volume",
    "ediExportType",
    "ediExportCrateNumber",
    "useFTrace",
    "kantelEenheid_4",
    "kantelEenheid_5",
    "kantelOpslag_4",
    "kantelOpslag_5",
    "kantelOpslagMethode",
    "dontShowENumber",
    "verpakkingPDMCode",
    "uploadProductData",
    "showCountryOfOrigin",
    "weergaveExtraAlbas",
    "bestBeforeDateDeterminationMethod",
    "bestBeforeDateCheckUpMethod",
    "purchasePrice",
    "salePrice",
    "consumerSalePrice",
    "fixedInvoicePrice",
    "bidfoodArtCategorie",
    "operations"
})
public class SaleArticle {

    @JsonProperty("productSpecificationUrl")
    private String productSpecificationUrl;
    @JsonProperty("saleArticleTranslation")
    private List<SaleArticleTranslation> saleArticleTranslation = null;
    @JsonProperty("tag")
    private List<Object> tag = null;
    @JsonProperty("allergens")
    private List<Allergen> allergens = null;
    @JsonProperty("nutritionals")
    private List<Object> nutritionals = null;
    @JsonProperty("number")
    private Double number;
    @JsonProperty("shortName")
    private String shortName;
    @JsonProperty("longName")
    private String longName;
    @JsonProperty("inkpOld")
    private Double inkpOld;
    @JsonProperty("verkpOld")
    private Double verkpOld;
    @JsonProperty("unitType")
    private Double unitType;
    @JsonProperty("vatCode")
    private Double vatCode;
    @JsonProperty("tareWeight")
    private Double tareWeight;
    @JsonProperty("weighingFactor")
    private Double weighingFactor;
    @JsonProperty("portionWeight")
    private Double portionWeight;
    @JsonProperty("processingLoss")
    private Double processingLoss;
    @JsonProperty("orderGroup1")
    private OrderGroup1 orderGroup1;
    @JsonProperty("bestBeforeDateFresh")
    private Double bestBeforeDateFresh;
    @JsonProperty("keepStock")
    private Double keepStock;
    @JsonProperty("saleArticleGroup")
    private SaleArticleGroup saleArticleGroup;
    @JsonProperty("pickLocation")
    private String pickLocation;
    @JsonProperty("backorderPercentage")
    private Double backorderPercentage;
    @JsonProperty("packedPerUnit")
    private Double packedPerUnit;
    @JsonProperty("packedPerType")
    private Double packedPerType;
    @JsonProperty("ingredient1")
    private String ingredient1;
    @JsonProperty("ingredient2")
    private String ingredient2;
    @JsonProperty("ingredient3")
    private String ingredient3;
    @JsonProperty("ingredient4")
    private String ingredient4;
    @JsonProperty("ingredient5")
    private String ingredient5;
    @JsonProperty("ingredient6")
    private String ingredient6;
    @JsonProperty("ingredient7")
    private String ingredient7;
    @JsonProperty("ingredient8")
    private String ingredient8;
    @JsonProperty("packingType")
    private String packingType;
    @JsonProperty("cbsGoodsCode")
    private String cbsGoodsCode;
    @JsonProperty("storageTerms")
    private String storageTerms;
    @JsonProperty("userDefinableField1")
    private String userDefinableField1;
    @JsonProperty("userDefinableField2")
    private String userDefinableField2;
    @JsonProperty("userDefinableField3")
    private String userDefinableField3;
    @JsonProperty("userDefinableField4")
    private String userDefinableField4;
    @JsonProperty("userDefinableField5")
    private String userDefinableField5;
    @JsonProperty("purchaseArticle")
    private PurchaseArticle purchaseArticle;
    @JsonProperty("userPurchaseFactor")
    private Double userPurchaseFactor;
    @JsonProperty("stockAutoPurchaseArticle")
    private Double stockAutoPurchaseArticle;
    @JsonProperty("packingArticle1")
    private PackingArticle1 packingArticle1;
    @JsonProperty("isEcommerceArticle")
    private String isEcommerceArticle;
    @JsonProperty("orderGroup2")
    private OrderGroup2 orderGroup2;
    @JsonProperty("labelNumber")
    private Double labelNumber;
    @JsonProperty("blockCustomerDiscount")
    private String blockCustomerDiscount;
    @JsonProperty("ingredient9")
    private String ingredient9;
    @JsonProperty("ingredient10")
    private String ingredient10;
    @JsonProperty("ingredient11")
    private String ingredient11;
    @JsonProperty("ingredient12")
    private String ingredient12;
    @JsonProperty("fixedStockLocation")
    private FixedStockLocation fixedStockLocation;
    @JsonProperty("bestBeforeDateFrozen")
    private Double bestBeforeDateFrozen;
    @JsonProperty("adviesVerkoopPrijsOu")
    private Double adviesVerkoopPrijsOu;
    @JsonProperty("tipUpUnit1")
    private Double tipUpUnit1;
    @JsonProperty("tipUpUnit2")
    private Double tipUpUnit2;
    @JsonProperty("tipUpUnit3")
    private Double tipUpUnit3;
    @JsonProperty("tipUpSurcharge1")
    private Double tipUpSurcharge1;
    @JsonProperty("tipUpSurcharge2")
    private Double tipUpSurcharge2;
    @JsonProperty("tipUpSurcharge3")
    private Double tipUpSurcharge3;
    @JsonProperty("substituteArticle")
    private SubstituteArticle substituteArticle;
    @JsonProperty("acknowledgement")
    private String acknowledgement;
    @JsonProperty("eanRetailUnitCode")
    private String eanRetailUnitCode;
    @JsonProperty("orderGroup3")
    private OrderGroup3 orderGroup3;
    @JsonProperty("storageState")
    private Double storageState;
    @JsonProperty("orderGroup4")
    private OrderGroup4 orderGroup4;
    @JsonProperty("orderGroup5")
    private OrderGroup5 orderGroup5;
    @JsonProperty("calculationType")
    private Double calculationType;
    @JsonProperty("priceOnPackingSlip")
    private Double priceOnPackingSlip;
    @JsonProperty("isBlocked")
    private Boolean isBlocked;
    @JsonProperty("defaultOrderAtAdministrationBranch")
    private DefaultOrderAtAdministrationBranch defaultOrderAtAdministrationBranch;
    @JsonProperty("owhArticleIdBranch")
    private Double owhArticleIdBranch;
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("wrapping")
    private String wrapping;
    @JsonProperty("photoPath")
    private String photoPath;
    @JsonProperty("packingArticle2")
    private PackingArticle2 packingArticle2;
    @JsonProperty("blockedFrom")
    private String blockedFrom;
    @JsonProperty("blockedUntil")
    private String blockedUntil;
    @JsonProperty("fixedSurcharge")
    private Boolean fixedSurcharge;
    @JsonProperty("surchargeFactor")
    private Double surchargeFactor;
    @JsonProperty("excludeFromPriceList")
    private Boolean excludeFromPriceList;
    @JsonProperty("allowedPriceDeviation")
    private Double allowedPriceDeviation;
    @JsonProperty("occMethod")
    private Double occMethod;
    @JsonProperty("bonusParticipation")
    private Double bonusParticipation;
    @JsonProperty("subGroup")
    private SubGroup subGroup;
    @JsonProperty("deliveryTimeInversco")
    private Double deliveryTimeInversco;
    @JsonProperty("quantityInPacking")
    private Double quantityInPacking;
    @JsonProperty("totalPackedWeight")
    private Double totalPackedWeight;
    @JsonProperty("productionLabelBasedOnPortion")
    private Boolean productionLabelBasedOnPortion;
    @JsonProperty("vastFacPrijsOld")
    private Double vastFacPrijsOld;
    @JsonProperty("consumerGroup")
    private ConsumerGroup consumerGroup;
    @JsonProperty("substituteGroup")
    private SubstituteGroup substituteGroup;
    @JsonProperty("wssAskPackingQuantity")
    private String wssAskPackingQuantity;
    @JsonProperty("gflTracingCode")
    private Double gflTracingCode;
    @JsonProperty("guaranteedBestBeforeDays")
    private Double guaranteedBestBeforeDays;
    @JsonProperty("vertebraType")
    private Double vertebraType;
    @JsonProperty("manualIngredient1")
    private Double manualIngredient1;
    @JsonProperty("manualIngredient2")
    private Double manualIngredient2;
    @JsonProperty("manualIngredient3")
    private Double manualIngredient3;
    @JsonProperty("manualIngredient4")
    private Double manualIngredient4;
    @JsonProperty("manualIngredient5")
    private Double manualIngredient5;
    @JsonProperty("manualIngredient6")
    private Double manualIngredient6;
    @JsonProperty("manualIngredient7")
    private Double manualIngredient7;
    @JsonProperty("manualIngredient8")
    private Double manualIngredient8;
    @JsonProperty("manualIngredient9")
    private Double manualIngredient9;
    @JsonProperty("manualIngredient10")
    private Double manualIngredient10;
    @JsonProperty("manualIngredient11")
    private Double manualIngredient11;
    @JsonProperty("manualIngredient12")
    private Double manualIngredient12;
    @JsonProperty("gmoState")
    private Double gmoState;
    @JsonProperty("gmoManual")
    private Boolean gmoManual;
    @JsonProperty("displayENumber")
    private Boolean displayENumber;
    @JsonProperty("displayAllergens")
    private Double displayAllergens;
    @JsonProperty("productSpecificationNumber")
    private Double productSpecificationNumber;
    @JsonProperty("productSpecificationNumberRevision")
    private Double productSpecificationNumberRevision;
    @JsonProperty("productSpecificationDate")
    private String productSpecificationDate;
    @JsonProperty("lastProductSpecNumber")
    private Double lastProductSpecNumber;
    @JsonProperty("lastProductSpecNumberRevision")
    private Double lastProductSpecNumberRevision;
    @JsonProperty("lastProductSpecDate")
    private String lastProductSpecDate;
    @JsonProperty("registrationWarning")
    private Double registrationWarning;
    @JsonProperty("isOwhArticle")
    private Boolean isOwhArticle;
    @JsonProperty("usePortionAsCbsPortion")
    private Double usePortionAsCbsPortion;
    @JsonProperty("cbsPortionWeight")
    private Double cbsPortionWeight;
    @JsonProperty("cbsSupplementaryUnitFactor")
    private Double cbsSupplementaryUnitFactor;
    @JsonProperty("occNumberOfLabels")
    private Double occNumberOfLabels;
    @JsonProperty("maxOrderQuantity")
    private Double maxOrderQuantity;
    @JsonProperty("owhDeliveryType")
    private Double owhDeliveryType;
    @JsonProperty("eWeighingPortion")
    private Double eWeighingPortion;
    @JsonProperty("usePortionAsEWeighingPort")
    private Double usePortionAsEWeighingPort;
    @JsonProperty("documentPath")
    private String documentPath;
    @JsonProperty("crateLabelNumber")
    private Double crateLabelNumber;
    @JsonProperty("salePriceBasedOnConsumer")
    private Boolean salePriceBasedOnConsumer;
    @JsonProperty("registrationMethod")
    private Double registrationMethod;
    @JsonProperty("minimumWeight")
    private Double minimumWeight;
    @JsonProperty("maximumWeight")
    private Double maximumWeight;
    @JsonProperty("createBackorderOnExcessiveStock")
    private Boolean createBackorderOnExcessiveStock;
    @JsonProperty("createBackorderOnStockShortage")
    private Boolean createBackorderOnStockShortage;
    @JsonProperty("backorderType")
    private Double backorderType;
    @JsonProperty("boxQuantity")
    private Double boxQuantity;
    @JsonProperty("boxesPerLayer")
    private Double boxesPerLayer;
    @JsonProperty("layersPerPallet")
    private Double layersPerPallet;
    @JsonProperty("useGraiCode")
    private Double useGraiCode;
    @JsonProperty("lengthOfGraiCode")
    private Double lengthOfGraiCode;
    @JsonProperty("defProdMultiBranchId")
    private Double defProdMultiBranchId;
    @JsonProperty("eanTradeUnitCode")
    private String eanTradeUnitCode;
    @JsonProperty("wssCheckMaximumRegistration")
    private Boolean wssCheckMaximumRegistration;
    @JsonProperty("trackPackingBalance")
    private Boolean trackPackingBalance;
    @JsonProperty("checkMinMaxPortionWeightOnOrderEntry")
    private Boolean checkMinMaxPortionWeightOnOrderEntry;
    @JsonProperty("ssccRegisterMethod")
    private Double ssccRegisterMethod;
    @JsonProperty("planningUnitType")
    private Double planningUnitType;
    @JsonProperty("usePlanningUnitType")
    private Boolean usePlanningUnitType;
    @JsonProperty("palletLabelNumber")
    private Double palletLabelNumber;
    @JsonProperty("countryOfOrigin")
    private String countryOfOrigin;
    @JsonProperty("analysisPurchasePriceType")
    private Double analysisPurchasePriceType;
    @JsonProperty("useAnalysisPurchasePriceBatch")
    private Boolean useAnalysisPurchasePriceBatch;
    @JsonProperty("priceLookUpCode")
    private Double priceLookUpCode;
    @JsonProperty("length")
    private Double length;
    @JsonProperty("width")
    private Double width;
    @JsonProperty("height")
    private Double height;
    @JsonProperty("volume")
    private Double volume;
    @JsonProperty("ediExportType")
    private Double ediExportType;
    @JsonProperty("ediExportCrateNumber")
    private Double ediExportCrateNumber;
    @JsonProperty("useFTrace")
    private Boolean useFTrace;
    @JsonProperty("kantelEenheid_4")
    private Double kantelEenheid4;
    @JsonProperty("kantelEenheid_5")
    private Double kantelEenheid5;
    @JsonProperty("kantelOpslag_4")
    private Double kantelOpslag4;
    @JsonProperty("kantelOpslag_5")
    private Double kantelOpslag5;
    @JsonProperty("kantelOpslagMethode")
    private Double kantelOpslagMethode;
    @JsonProperty("dontShowENumber")
    private Double dontShowENumber;
    @JsonProperty("verpakkingPDMCode")
    private String verpakkingPDMCode;
    @JsonProperty("uploadProductData")
    private Double uploadProductData;
    @JsonProperty("showCountryOfOrigin")
    private Double showCountryOfOrigin;
    @JsonProperty("weergaveExtraAlbas")
    private Boolean weergaveExtraAlbas;
    @JsonProperty("bestBeforeDateDeterminationMethod")
    private Double bestBeforeDateDeterminationMethod;
    @JsonProperty("bestBeforeDateCheckUpMethod")
    private Double bestBeforeDateCheckUpMethod;
    @JsonProperty("purchasePrice")
    private Double purchasePrice;
    @JsonProperty("salePrice")
    private Double salePrice;
    @JsonProperty("consumerSalePrice")
    private Double consumerSalePrice;
    @JsonProperty("fixedInvoicePrice")
    private Double fixedInvoicePrice;
    @JsonProperty("bidfoodArtCategorie")
    private Double bidfoodArtCategorie;
    @JsonProperty("operations")
    private List<String> operations = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("productSpecificationUrl")
    public String getProductSpecificationUrl() {
        return productSpecificationUrl;
    }

    @JsonProperty("productSpecificationUrl")
    public void setProductSpecificationUrl(String productSpecificationUrl) {
        this.productSpecificationUrl = productSpecificationUrl;
    }

    @JsonProperty("saleArticleTranslation")
    public List<SaleArticleTranslation> getSaleArticleTranslation() {
        return saleArticleTranslation;
    }

    @JsonProperty("saleArticleTranslation")
    public void setSaleArticleTranslation(List<SaleArticleTranslation> saleArticleTranslation) {
        this.saleArticleTranslation = saleArticleTranslation;
    }

    @JsonProperty("tag")
    public List<Object> getTag() {
        return tag;
    }

    @JsonProperty("tag")
    public void setTag(List<Object> tag) {
        this.tag = tag;
    }

    @JsonProperty("allergens")
    public List<Allergen> getAllergens() {
        return allergens;
    }

    @JsonProperty("allergens")
    public void setAllergens(List<Allergen> allergens) {
        this.allergens = allergens;
    }

    @JsonProperty("nutritionals")
    public List<Object> getNutritionals() {
        return nutritionals;
    }

    @JsonProperty("nutritionals")
    public void setNutritionals(List<Object> nutritionals) {
        this.nutritionals = nutritionals;
    }

    @JsonProperty("number")
    public Double getNumber() {
        return number;
    }

    @JsonProperty("number")
    public void setNumber(Double number) {
        this.number = number;
    }

    @JsonProperty("shortName")
    public String getShortName() {
        return shortName;
    }

    @JsonProperty("shortName")
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @JsonProperty("longName")
    public String getLongName() {
        return longName;
    }

    @JsonProperty("longName")
    public void setLongName(String longName) {
        this.longName = longName;
    }

    @JsonProperty("inkpOld")
    public Double getInkpOld() {
        return inkpOld;
    }

    @JsonProperty("inkpOld")
    public void setInkpOld(Double inkpOld) {
        this.inkpOld = inkpOld;
    }

    @JsonProperty("verkpOld")
    public Double getVerkpOld() {
        return verkpOld;
    }

    @JsonProperty("verkpOld")
    public void setVerkpOld(Double verkpOld) {
        this.verkpOld = verkpOld;
    }

    @JsonProperty("unitType")
    public Double getUnitType() {
        return unitType;
    }

    @JsonProperty("unitType")
    public void setUnitType(Double unitType) {
        this.unitType = unitType;
    }

    @JsonProperty("vatCode")
    public Double getVatCode() {
        return vatCode;
    }

    @JsonProperty("vatCode")
    public void setVatCode(Double vatCode) {
        this.vatCode = vatCode;
    }

    @JsonProperty("tareWeight")
    public Double getTareWeight() {
        return tareWeight;
    }

    @JsonProperty("tareWeight")
    public void setTareWeight(Double tareWeight) {
        this.tareWeight = tareWeight;
    }

    @JsonProperty("weighingFactor")
    public Double getWeighingFactor() {
        return weighingFactor;
    }

    @JsonProperty("weighingFactor")
    public void setWeighingFactor(Double weighingFactor) {
        this.weighingFactor = weighingFactor;
    }

    @JsonProperty("portionWeight")
    public Double getPortionWeight() {
        return portionWeight;
    }

    @JsonProperty("portionWeight")
    public void setPortionWeight(Double portionWeight) {
        this.portionWeight = portionWeight;
    }

    @JsonProperty("processingLoss")
    public Double getProcessingLoss() {
        return processingLoss;
    }

    @JsonProperty("processingLoss")
    public void setProcessingLoss(Double processingLoss) {
        this.processingLoss = processingLoss;
    }

    @JsonProperty("orderGroup1")
    public OrderGroup1 getOrderGroup1() {
        return orderGroup1;
    }

    @JsonProperty("orderGroup1")
    public void setOrderGroup1(OrderGroup1 orderGroup1) {
        this.orderGroup1 = orderGroup1;
    }

    @JsonProperty("bestBeforeDateFresh")
    public Double getBestBeforeDateFresh() {
        return bestBeforeDateFresh;
    }

    @JsonProperty("bestBeforeDateFresh")
    public void setBestBeforeDateFresh(Double bestBeforeDateFresh) {
        this.bestBeforeDateFresh = bestBeforeDateFresh;
    }

    @JsonProperty("keepStock")
    public Double getKeepStock() {
        return keepStock;
    }

    @JsonProperty("keepStock")
    public void setKeepStock(Double keepStock) {
        this.keepStock = keepStock;
    }

    @JsonProperty("saleArticleGroup")
    public SaleArticleGroup getSaleArticleGroup() {
        return saleArticleGroup;
    }

    @JsonProperty("saleArticleGroup")
    public void setSaleArticleGroup(SaleArticleGroup saleArticleGroup) {
        this.saleArticleGroup = saleArticleGroup;
    }

    @JsonProperty("pickLocation")
    public String getPickLocation() {
        return pickLocation;
    }

    @JsonProperty("pickLocation")
    public void setPickLocation(String pickLocation) {
        this.pickLocation = pickLocation;
    }

    @JsonProperty("backorderPercentage")
    public Double getBackorderPercentage() {
        return backorderPercentage;
    }

    @JsonProperty("backorderPercentage")
    public void setBackorderPercentage(Double backorderPercentage) {
        this.backorderPercentage = backorderPercentage;
    }

    @JsonProperty("packedPerUnit")
    public Double getPackedPerUnit() {
        return packedPerUnit;
    }

    @JsonProperty("packedPerUnit")
    public void setPackedPerUnit(Double packedPerUnit) {
        this.packedPerUnit = packedPerUnit;
    }

    @JsonProperty("packedPerType")
    public Double getPackedPerType() {
        return packedPerType;
    }

    @JsonProperty("packedPerType")
    public void setPackedPerType(Double packedPerType) {
        this.packedPerType = packedPerType;
    }

    @JsonProperty("ingredient1")
    public String getIngredient1() {
        return ingredient1;
    }

    @JsonProperty("ingredient1")
    public void setIngredient1(String ingredient1) {
        this.ingredient1 = ingredient1;
    }

    @JsonProperty("ingredient2")
    public String getIngredient2() {
        return ingredient2;
    }

    @JsonProperty("ingredient2")
    public void setIngredient2(String ingredient2) {
        this.ingredient2 = ingredient2;
    }

    @JsonProperty("ingredient3")
    public String getIngredient3() {
        return ingredient3;
    }

    @JsonProperty("ingredient3")
    public void setIngredient3(String ingredient3) {
        this.ingredient3 = ingredient3;
    }

    @JsonProperty("ingredient4")
    public String getIngredient4() {
        return ingredient4;
    }

    @JsonProperty("ingredient4")
    public void setIngredient4(String ingredient4) {
        this.ingredient4 = ingredient4;
    }

    @JsonProperty("ingredient5")
    public String getIngredient5() {
        return ingredient5;
    }

    @JsonProperty("ingredient5")
    public void setIngredient5(String ingredient5) {
        this.ingredient5 = ingredient5;
    }

    @JsonProperty("ingredient6")
    public String getIngredient6() {
        return ingredient6;
    }

    @JsonProperty("ingredient6")
    public void setIngredient6(String ingredient6) {
        this.ingredient6 = ingredient6;
    }

    @JsonProperty("ingredient7")
    public String getIngredient7() {
        return ingredient7;
    }

    @JsonProperty("ingredient7")
    public void setIngredient7(String ingredient7) {
        this.ingredient7 = ingredient7;
    }

    @JsonProperty("ingredient8")
    public String getIngredient8() {
        return ingredient8;
    }

    @JsonProperty("ingredient8")
    public void setIngredient8(String ingredient8) {
        this.ingredient8 = ingredient8;
    }

    @JsonProperty("packingType")
    public String getPackingType() {
        return packingType;
    }

    @JsonProperty("packingType")
    public void setPackingType(String packingType) {
        this.packingType = packingType;
    }

    @JsonProperty("cbsGoodsCode")
    public String getCbsGoodsCode() {
        return cbsGoodsCode;
    }

    @JsonProperty("cbsGoodsCode")
    public void setCbsGoodsCode(String cbsGoodsCode) {
        this.cbsGoodsCode = cbsGoodsCode;
    }

    @JsonProperty("storageTerms")
    public String getStorageTerms() {
        return storageTerms;
    }

    @JsonProperty("storageTerms")
    public void setStorageTerms(String storageTerms) {
        this.storageTerms = storageTerms;
    }

    @JsonProperty("userDefinableField1")
    public String getUserDefinableField1() {
        return userDefinableField1;
    }

    @JsonProperty("userDefinableField1")
    public void setUserDefinableField1(String userDefinableField1) {
        this.userDefinableField1 = userDefinableField1;
    }

    @JsonProperty("userDefinableField2")
    public String getUserDefinableField2() {
        return userDefinableField2;
    }

    @JsonProperty("userDefinableField2")
    public void setUserDefinableField2(String userDefinableField2) {
        this.userDefinableField2 = userDefinableField2;
    }

    @JsonProperty("userDefinableField3")
    public String getUserDefinableField3() {
        return userDefinableField3;
    }

    @JsonProperty("userDefinableField3")
    public void setUserDefinableField3(String userDefinableField3) {
        this.userDefinableField3 = userDefinableField3;
    }

    @JsonProperty("userDefinableField4")
    public String getUserDefinableField4() {
        return userDefinableField4;
    }

    @JsonProperty("userDefinableField4")
    public void setUserDefinableField4(String userDefinableField4) {
        this.userDefinableField4 = userDefinableField4;
    }

    @JsonProperty("userDefinableField5")
    public String getUserDefinableField5() {
        return userDefinableField5;
    }

    @JsonProperty("userDefinableField5")
    public void setUserDefinableField5(String userDefinableField5) {
        this.userDefinableField5 = userDefinableField5;
    }

    @JsonProperty("purchaseArticle")
    public PurchaseArticle getPurchaseArticle() {
        return purchaseArticle;
    }

    @JsonProperty("purchaseArticle")
    public void setPurchaseArticle(PurchaseArticle purchaseArticle) {
        this.purchaseArticle = purchaseArticle;
    }

    @JsonProperty("userPurchaseFactor")
    public Double getUserPurchaseFactor() {
        return userPurchaseFactor;
    }

    @JsonProperty("userPurchaseFactor")
    public void setUserPurchaseFactor(Double userPurchaseFactor) {
        this.userPurchaseFactor = userPurchaseFactor;
    }

    @JsonProperty("stockAutoPurchaseArticle")
    public Double getStockAutoPurchaseArticle() {
        return stockAutoPurchaseArticle;
    }

    @JsonProperty("stockAutoPurchaseArticle")
    public void setStockAutoPurchaseArticle(Double stockAutoPurchaseArticle) {
        this.stockAutoPurchaseArticle = stockAutoPurchaseArticle;
    }

    @JsonProperty("packingArticle1")
    public PackingArticle1 getPackingArticle1() {
        return packingArticle1;
    }

    @JsonProperty("packingArticle1")
    public void setPackingArticle1(PackingArticle1 packingArticle1) {
        this.packingArticle1 = packingArticle1;
    }

    @JsonProperty("isEcommerceArticle")
    public String getIsEcommerceArticle() {
        return isEcommerceArticle;
    }

    @JsonProperty("isEcommerceArticle")
    public void setIsEcommerceArticle(String isEcommerceArticle) {
        this.isEcommerceArticle = isEcommerceArticle;
    }

    @JsonProperty("orderGroup2")
    public OrderGroup2 getOrderGroup2() {
        return orderGroup2;
    }

    @JsonProperty("orderGroup2")
    public void setOrderGroup2(OrderGroup2 orderGroup2) {
        this.orderGroup2 = orderGroup2;
    }

    @JsonProperty("labelNumber")
    public Double getLabelNumber() {
        return labelNumber;
    }

    @JsonProperty("labelNumber")
    public void setLabelNumber(Double labelNumber) {
        this.labelNumber = labelNumber;
    }

    @JsonProperty("blockCustomerDiscount")
    public String getBlockCustomerDiscount() {
        return blockCustomerDiscount;
    }

    @JsonProperty("blockCustomerDiscount")
    public void setBlockCustomerDiscount(String blockCustomerDiscount) {
        this.blockCustomerDiscount = blockCustomerDiscount;
    }

    @JsonProperty("ingredient9")
    public String getIngredient9() {
        return ingredient9;
    }

    @JsonProperty("ingredient9")
    public void setIngredient9(String ingredient9) {
        this.ingredient9 = ingredient9;
    }

    @JsonProperty("ingredient10")
    public String getIngredient10() {
        return ingredient10;
    }

    @JsonProperty("ingredient10")
    public void setIngredient10(String ingredient10) {
        this.ingredient10 = ingredient10;
    }

    @JsonProperty("ingredient11")
    public String getIngredient11() {
        return ingredient11;
    }

    @JsonProperty("ingredient11")
    public void setIngredient11(String ingredient11) {
        this.ingredient11 = ingredient11;
    }

    @JsonProperty("ingredient12")
    public String getIngredient12() {
        return ingredient12;
    }

    @JsonProperty("ingredient12")
    public void setIngredient12(String ingredient12) {
        this.ingredient12 = ingredient12;
    }

    @JsonProperty("fixedStockLocation")
    public FixedStockLocation getFixedStockLocation() {
        return fixedStockLocation;
    }

    @JsonProperty("fixedStockLocation")
    public void setFixedStockLocation(FixedStockLocation fixedStockLocation) {
        this.fixedStockLocation = fixedStockLocation;
    }

    @JsonProperty("bestBeforeDateFrozen")
    public Double getBestBeforeDateFrozen() {
        return bestBeforeDateFrozen;
    }

    @JsonProperty("bestBeforeDateFrozen")
    public void setBestBeforeDateFrozen(Double bestBeforeDateFrozen) {
        this.bestBeforeDateFrozen = bestBeforeDateFrozen;
    }

    @JsonProperty("adviesVerkoopPrijsOu")
    public Double getAdviesVerkoopPrijsOu() {
        return adviesVerkoopPrijsOu;
    }

    @JsonProperty("adviesVerkoopPrijsOu")
    public void setAdviesVerkoopPrijsOu(Double adviesVerkoopPrijsOu) {
        this.adviesVerkoopPrijsOu = adviesVerkoopPrijsOu;
    }

    @JsonProperty("tipUpUnit1")
    public Double getTipUpUnit1() {
        return tipUpUnit1;
    }

    @JsonProperty("tipUpUnit1")
    public void setTipUpUnit1(Double tipUpUnit1) {
        this.tipUpUnit1 = tipUpUnit1;
    }

    @JsonProperty("tipUpUnit2")
    public Double getTipUpUnit2() {
        return tipUpUnit2;
    }

    @JsonProperty("tipUpUnit2")
    public void setTipUpUnit2(Double tipUpUnit2) {
        this.tipUpUnit2 = tipUpUnit2;
    }

    @JsonProperty("tipUpUnit3")
    public Double getTipUpUnit3() {
        return tipUpUnit3;
    }

    @JsonProperty("tipUpUnit3")
    public void setTipUpUnit3(Double tipUpUnit3) {
        this.tipUpUnit3 = tipUpUnit3;
    }

    @JsonProperty("tipUpSurcharge1")
    public Double getTipUpSurcharge1() {
        return tipUpSurcharge1;
    }

    @JsonProperty("tipUpSurcharge1")
    public void setTipUpSurcharge1(Double tipUpSurcharge1) {
        this.tipUpSurcharge1 = tipUpSurcharge1;
    }

    @JsonProperty("tipUpSurcharge2")
    public Double getTipUpSurcharge2() {
        return tipUpSurcharge2;
    }

    @JsonProperty("tipUpSurcharge2")
    public void setTipUpSurcharge2(Double tipUpSurcharge2) {
        this.tipUpSurcharge2 = tipUpSurcharge2;
    }

    @JsonProperty("tipUpSurcharge3")
    public Double getTipUpSurcharge3() {
        return tipUpSurcharge3;
    }

    @JsonProperty("tipUpSurcharge3")
    public void setTipUpSurcharge3(Double tipUpSurcharge3) {
        this.tipUpSurcharge3 = tipUpSurcharge3;
    }

    @JsonProperty("substituteArticle")
    public SubstituteArticle getSubstituteArticle() {
        return substituteArticle;
    }

    @JsonProperty("substituteArticle")
    public void setSubstituteArticle(SubstituteArticle substituteArticle) {
        this.substituteArticle = substituteArticle;
    }

    @JsonProperty("acknowledgement")
    public String getAcknowledgement() {
        return acknowledgement;
    }

    @JsonProperty("acknowledgement")
    public void setAcknowledgement(String acknowledgement) {
        this.acknowledgement = acknowledgement;
    }

    @JsonProperty("eanRetailUnitCode")
    public String getEanRetailUnitCode() {
        return eanRetailUnitCode;
    }

    @JsonProperty("eanRetailUnitCode")
    public void setEanRetailUnitCode(String eanRetailUnitCode) {
        this.eanRetailUnitCode = eanRetailUnitCode;
    }

    @JsonProperty("orderGroup3")
    public OrderGroup3 getOrderGroup3() {
        return orderGroup3;
    }

    @JsonProperty("orderGroup3")
    public void setOrderGroup3(OrderGroup3 orderGroup3) {
        this.orderGroup3 = orderGroup3;
    }

    @JsonProperty("storageState")
    public Double getStorageState() {
        return storageState;
    }

    @JsonProperty("storageState")
    public void setStorageState(Double storageState) {
        this.storageState = storageState;
    }

    @JsonProperty("orderGroup4")
    public OrderGroup4 getOrderGroup4() {
        return orderGroup4;
    }

    @JsonProperty("orderGroup4")
    public void setOrderGroup4(OrderGroup4 orderGroup4) {
        this.orderGroup4 = orderGroup4;
    }

    @JsonProperty("orderGroup5")
    public OrderGroup5 getOrderGroup5() {
        return orderGroup5;
    }

    @JsonProperty("orderGroup5")
    public void setOrderGroup5(OrderGroup5 orderGroup5) {
        this.orderGroup5 = orderGroup5;
    }

    @JsonProperty("calculationType")
    public Double getCalculationType() {
        return calculationType;
    }

    @JsonProperty("calculationType")
    public void setCalculationType(Double calculationType) {
        this.calculationType = calculationType;
    }

    @JsonProperty("priceOnPackingSlip")
    public Double getPriceOnPackingSlip() {
        return priceOnPackingSlip;
    }

    @JsonProperty("priceOnPackingSlip")
    public void setPriceOnPackingSlip(Double priceOnPackingSlip) {
        this.priceOnPackingSlip = priceOnPackingSlip;
    }

    @JsonProperty("isBlocked")
    public Boolean getIsBlocked() {
        return isBlocked;
    }

    @JsonProperty("isBlocked")
    public void setIsBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    @JsonProperty("defaultOrderAtAdministrationBranch")
    public DefaultOrderAtAdministrationBranch getDefaultOrderAtAdministrationBranch() {
        return defaultOrderAtAdministrationBranch;
    }

    @JsonProperty("defaultOrderAtAdministrationBranch")
    public void setDefaultOrderAtAdministrationBranch(DefaultOrderAtAdministrationBranch defaultOrderAtAdministrationBranch) {
        this.defaultOrderAtAdministrationBranch = defaultOrderAtAdministrationBranch;
    }

    @JsonProperty("owhArticleIdBranch")
    public Double getOwhArticleIdBranch() {
        return owhArticleIdBranch;
    }

    @JsonProperty("owhArticleIdBranch")
    public void setOwhArticleIdBranch(Double owhArticleIdBranch) {
        this.owhArticleIdBranch = owhArticleIdBranch;
    }

    @JsonProperty("brand")
    public String getBrand() {
        return brand;
    }

    @JsonProperty("brand")
    public void setBrand(String brand) {
        this.brand = brand;
    }

    @JsonProperty("wrapping")
    public String getWrapping() {
        return wrapping;
    }

    @JsonProperty("wrapping")
    public void setWrapping(String wrapping) {
        this.wrapping = wrapping;
    }

    @JsonProperty("photoPath")
    public String getPhotoPath() {
        return photoPath;
    }

    @JsonProperty("photoPath")
    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    @JsonProperty("packingArticle2")
    public PackingArticle2 getPackingArticle2() {
        return packingArticle2;
    }

    @JsonProperty("packingArticle2")
    public void setPackingArticle2(PackingArticle2 packingArticle2) {
        this.packingArticle2 = packingArticle2;
    }

    @JsonProperty("blockedFrom")
    public String getBlockedFrom() {
        return blockedFrom;
    }

    @JsonProperty("blockedFrom")
    public void setBlockedFrom(String blockedFrom) {
        this.blockedFrom = blockedFrom;
    }

    @JsonProperty("blockedUntil")
    public String getBlockedUntil() {
        return blockedUntil;
    }

    @JsonProperty("blockedUntil")
    public void setBlockedUntil(String blockedUntil) {
        this.blockedUntil = blockedUntil;
    }

    @JsonProperty("fixedSurcharge")
    public Boolean getFixedSurcharge() {
        return fixedSurcharge;
    }

    @JsonProperty("fixedSurcharge")
    public void setFixedSurcharge(Boolean fixedSurcharge) {
        this.fixedSurcharge = fixedSurcharge;
    }

    @JsonProperty("surchargeFactor")
    public Double getSurchargeFactor() {
        return surchargeFactor;
    }

    @JsonProperty("surchargeFactor")
    public void setSurchargeFactor(Double surchargeFactor) {
        this.surchargeFactor = surchargeFactor;
    }

    @JsonProperty("excludeFromPriceList")
    public Boolean getExcludeFromPriceList() {
        return excludeFromPriceList;
    }

    @JsonProperty("excludeFromPriceList")
    public void setExcludeFromPriceList(Boolean excludeFromPriceList) {
        this.excludeFromPriceList = excludeFromPriceList;
    }

    @JsonProperty("allowedPriceDeviation")
    public Double getAllowedPriceDeviation() {
        return allowedPriceDeviation;
    }

    @JsonProperty("allowedPriceDeviation")
    public void setAllowedPriceDeviation(Double allowedPriceDeviation) {
        this.allowedPriceDeviation = allowedPriceDeviation;
    }

    @JsonProperty("occMethod")
    public Double getOccMethod() {
        return occMethod;
    }

    @JsonProperty("occMethod")
    public void setOccMethod(Double occMethod) {
        this.occMethod = occMethod;
    }

    @JsonProperty("bonusParticipation")
    public Double getBonusParticipation() {
        return bonusParticipation;
    }

    @JsonProperty("bonusParticipation")
    public void setBonusParticipation(Double bonusParticipation) {
        this.bonusParticipation = bonusParticipation;
    }

    @JsonProperty("subGroup")
    public SubGroup getSubGroup() {
        return subGroup;
    }

    @JsonProperty("subGroup")
    public void setSubGroup(SubGroup subGroup) {
        this.subGroup = subGroup;
    }

    @JsonProperty("deliveryTimeInversco")
    public Double getDeliveryTimeInversco() {
        return deliveryTimeInversco;
    }

    @JsonProperty("deliveryTimeInversco")
    public void setDeliveryTimeInversco(Double deliveryTimeInversco) {
        this.deliveryTimeInversco = deliveryTimeInversco;
    }

    @JsonProperty("quantityInPacking")
    public Double getQuantityInPacking() {
        return quantityInPacking;
    }

    @JsonProperty("quantityInPacking")
    public void setQuantityInPacking(Double quantityInPacking) {
        this.quantityInPacking = quantityInPacking;
    }

    @JsonProperty("totalPackedWeight")
    public Double getTotalPackedWeight() {
        return totalPackedWeight;
    }

    @JsonProperty("totalPackedWeight")
    public void setTotalPackedWeight(Double totalPackedWeight) {
        this.totalPackedWeight = totalPackedWeight;
    }

    @JsonProperty("productionLabelBasedOnPortion")
    public Boolean getProductionLabelBasedOnPortion() {
        return productionLabelBasedOnPortion;
    }

    @JsonProperty("productionLabelBasedOnPortion")
    public void setProductionLabelBasedOnPortion(Boolean productionLabelBasedOnPortion) {
        this.productionLabelBasedOnPortion = productionLabelBasedOnPortion;
    }

    @JsonProperty("vastFacPrijsOld")
    public Double getVastFacPrijsOld() {
        return vastFacPrijsOld;
    }

    @JsonProperty("vastFacPrijsOld")
    public void setVastFacPrijsOld(Double vastFacPrijsOld) {
        this.vastFacPrijsOld = vastFacPrijsOld;
    }

    @JsonProperty("consumerGroup")
    public ConsumerGroup getConsumerGroup() {
        return consumerGroup;
    }

    @JsonProperty("consumerGroup")
    public void setConsumerGroup(ConsumerGroup consumerGroup) {
        this.consumerGroup = consumerGroup;
    }

    @JsonProperty("substituteGroup")
    public SubstituteGroup getSubstituteGroup() {
        return substituteGroup;
    }

    @JsonProperty("substituteGroup")
    public void setSubstituteGroup(SubstituteGroup substituteGroup) {
        this.substituteGroup = substituteGroup;
    }

    @JsonProperty("wssAskPackingQuantity")
    public String getWssAskPackingQuantity() {
        return wssAskPackingQuantity;
    }

    @JsonProperty("wssAskPackingQuantity")
    public void setWssAskPackingQuantity(String wssAskPackingQuantity) {
        this.wssAskPackingQuantity = wssAskPackingQuantity;
    }

    @JsonProperty("gflTracingCode")
    public Double getGflTracingCode() {
        return gflTracingCode;
    }

    @JsonProperty("gflTracingCode")
    public void setGflTracingCode(Double gflTracingCode) {
        this.gflTracingCode = gflTracingCode;
    }

    @JsonProperty("guaranteedBestBeforeDays")
    public Double getGuaranteedBestBeforeDays() {
        return guaranteedBestBeforeDays;
    }

    @JsonProperty("guaranteedBestBeforeDays")
    public void setGuaranteedBestBeforeDays(Double guaranteedBestBeforeDays) {
        this.guaranteedBestBeforeDays = guaranteedBestBeforeDays;
    }

    @JsonProperty("vertebraType")
    public Double getVertebraType() {
        return vertebraType;
    }

    @JsonProperty("vertebraType")
    public void setVertebraType(Double vertebraType) {
        this.vertebraType = vertebraType;
    }

    @JsonProperty("manualIngredient1")
    public Double getManualIngredient1() {
        return manualIngredient1;
    }

    @JsonProperty("manualIngredient1")
    public void setManualIngredient1(Double manualIngredient1) {
        this.manualIngredient1 = manualIngredient1;
    }

    @JsonProperty("manualIngredient2")
    public Double getManualIngredient2() {
        return manualIngredient2;
    }

    @JsonProperty("manualIngredient2")
    public void setManualIngredient2(Double manualIngredient2) {
        this.manualIngredient2 = manualIngredient2;
    }

    @JsonProperty("manualIngredient3")
    public Double getManualIngredient3() {
        return manualIngredient3;
    }

    @JsonProperty("manualIngredient3")
    public void setManualIngredient3(Double manualIngredient3) {
        this.manualIngredient3 = manualIngredient3;
    }

    @JsonProperty("manualIngredient4")
    public Double getManualIngredient4() {
        return manualIngredient4;
    }

    @JsonProperty("manualIngredient4")
    public void setManualIngredient4(Double manualIngredient4) {
        this.manualIngredient4 = manualIngredient4;
    }

    @JsonProperty("manualIngredient5")
    public Double getManualIngredient5() {
        return manualIngredient5;
    }

    @JsonProperty("manualIngredient5")
    public void setManualIngredient5(Double manualIngredient5) {
        this.manualIngredient5 = manualIngredient5;
    }

    @JsonProperty("manualIngredient6")
    public Double getManualIngredient6() {
        return manualIngredient6;
    }

    @JsonProperty("manualIngredient6")
    public void setManualIngredient6(Double manualIngredient6) {
        this.manualIngredient6 = manualIngredient6;
    }

    @JsonProperty("manualIngredient7")
    public Double getManualIngredient7() {
        return manualIngredient7;
    }

    @JsonProperty("manualIngredient7")
    public void setManualIngredient7(Double manualIngredient7) {
        this.manualIngredient7 = manualIngredient7;
    }

    @JsonProperty("manualIngredient8")
    public Double getManualIngredient8() {
        return manualIngredient8;
    }

    @JsonProperty("manualIngredient8")
    public void setManualIngredient8(Double manualIngredient8) {
        this.manualIngredient8 = manualIngredient8;
    }

    @JsonProperty("manualIngredient9")
    public Double getManualIngredient9() {
        return manualIngredient9;
    }

    @JsonProperty("manualIngredient9")
    public void setManualIngredient9(Double manualIngredient9) {
        this.manualIngredient9 = manualIngredient9;
    }

    @JsonProperty("manualIngredient10")
    public Double getManualIngredient10() {
        return manualIngredient10;
    }

    @JsonProperty("manualIngredient10")
    public void setManualIngredient10(Double manualIngredient10) {
        this.manualIngredient10 = manualIngredient10;
    }

    @JsonProperty("manualIngredient11")
    public Double getManualIngredient11() {
        return manualIngredient11;
    }

    @JsonProperty("manualIngredient11")
    public void setManualIngredient11(Double manualIngredient11) {
        this.manualIngredient11 = manualIngredient11;
    }

    @JsonProperty("manualIngredient12")
    public Double getManualIngredient12() {
        return manualIngredient12;
    }

    @JsonProperty("manualIngredient12")
    public void setManualIngredient12(Double manualIngredient12) {
        this.manualIngredient12 = manualIngredient12;
    }

    @JsonProperty("gmoState")
    public Double getGmoState() {
        return gmoState;
    }

    @JsonProperty("gmoState")
    public void setGmoState(Double gmoState) {
        this.gmoState = gmoState;
    }

    @JsonProperty("gmoManual")
    public Boolean getGmoManual() {
        return gmoManual;
    }

    @JsonProperty("gmoManual")
    public void setGmoManual(Boolean gmoManual) {
        this.gmoManual = gmoManual;
    }

    @JsonProperty("displayENumber")
    public Boolean getDisplayENumber() {
        return displayENumber;
    }

    @JsonProperty("displayENumber")
    public void setDisplayENumber(Boolean displayENumber) {
        this.displayENumber = displayENumber;
    }

    @JsonProperty("displayAllergens")
    public Double getDisplayAllergens() {
        return displayAllergens;
    }

    @JsonProperty("displayAllergens")
    public void setDisplayAllergens(Double displayAllergens) {
        this.displayAllergens = displayAllergens;
    }

    @JsonProperty("productSpecificationNumber")
    public Double getProductSpecificationNumber() {
        return productSpecificationNumber;
    }

    @JsonProperty("productSpecificationNumber")
    public void setProductSpecificationNumber(Double productSpecificationNumber) {
        this.productSpecificationNumber = productSpecificationNumber;
    }

    @JsonProperty("productSpecificationNumberRevision")
    public Double getProductSpecificationNumberRevision() {
        return productSpecificationNumberRevision;
    }

    @JsonProperty("productSpecificationNumberRevision")
    public void setProductSpecificationNumberRevision(Double productSpecificationNumberRevision) {
        this.productSpecificationNumberRevision = productSpecificationNumberRevision;
    }

    @JsonProperty("productSpecificationDate")
    public String getProductSpecificationDate() {
        return productSpecificationDate;
    }

    @JsonProperty("productSpecificationDate")
    public void setProductSpecificationDate(String productSpecificationDate) {
        this.productSpecificationDate = productSpecificationDate;
    }

    @JsonProperty("lastProductSpecNumber")
    public Double getLastProductSpecNumber() {
        return lastProductSpecNumber;
    }

    @JsonProperty("lastProductSpecNumber")
    public void setLastProductSpecNumber(Double lastProductSpecNumber) {
        this.lastProductSpecNumber = lastProductSpecNumber;
    }

    @JsonProperty("lastProductSpecNumberRevision")
    public Double getLastProductSpecNumberRevision() {
        return lastProductSpecNumberRevision;
    }

    @JsonProperty("lastProductSpecNumberRevision")
    public void setLastProductSpecNumberRevision(Double lastProductSpecNumberRevision) {
        this.lastProductSpecNumberRevision = lastProductSpecNumberRevision;
    }

    @JsonProperty("lastProductSpecDate")
    public String getLastProductSpecDate() {
        return lastProductSpecDate;
    }

    @JsonProperty("lastProductSpecDate")
    public void setLastProductSpecDate(String lastProductSpecDate) {
        this.lastProductSpecDate = lastProductSpecDate;
    }

    @JsonProperty("registrationWarning")
    public Double getRegistrationWarning() {
        return registrationWarning;
    }

    @JsonProperty("registrationWarning")
    public void setRegistrationWarning(Double registrationWarning) {
        this.registrationWarning = registrationWarning;
    }

    @JsonProperty("isOwhArticle")
    public Boolean getIsOwhArticle() {
        return isOwhArticle;
    }

    @JsonProperty("isOwhArticle")
    public void setIsOwhArticle(Boolean isOwhArticle) {
        this.isOwhArticle = isOwhArticle;
    }

    @JsonProperty("usePortionAsCbsPortion")
    public Double getUsePortionAsCbsPortion() {
        return usePortionAsCbsPortion;
    }

    @JsonProperty("usePortionAsCbsPortion")
    public void setUsePortionAsCbsPortion(Double usePortionAsCbsPortion) {
        this.usePortionAsCbsPortion = usePortionAsCbsPortion;
    }

    @JsonProperty("cbsPortionWeight")
    public Double getCbsPortionWeight() {
        return cbsPortionWeight;
    }

    @JsonProperty("cbsPortionWeight")
    public void setCbsPortionWeight(Double cbsPortionWeight) {
        this.cbsPortionWeight = cbsPortionWeight;
    }

    @JsonProperty("cbsSupplementaryUnitFactor")
    public Double getCbsSupplementaryUnitFactor() {
        return cbsSupplementaryUnitFactor;
    }

    @JsonProperty("cbsSupplementaryUnitFactor")
    public void setCbsSupplementaryUnitFactor(Double cbsSupplementaryUnitFactor) {
        this.cbsSupplementaryUnitFactor = cbsSupplementaryUnitFactor;
    }

    @JsonProperty("occNumberOfLabels")
    public Double getOccNumberOfLabels() {
        return occNumberOfLabels;
    }

    @JsonProperty("occNumberOfLabels")
    public void setOccNumberOfLabels(Double occNumberOfLabels) {
        this.occNumberOfLabels = occNumberOfLabels;
    }

    @JsonProperty("maxOrderQuantity")
    public Double getMaxOrderQuantity() {
        return maxOrderQuantity;
    }

    @JsonProperty("maxOrderQuantity")
    public void setMaxOrderQuantity(Double maxOrderQuantity) {
        this.maxOrderQuantity = maxOrderQuantity;
    }

    @JsonProperty("owhDeliveryType")
    public Double getOwhDeliveryType() {
        return owhDeliveryType;
    }

    @JsonProperty("owhDeliveryType")
    public void setOwhDeliveryType(Double owhDeliveryType) {
        this.owhDeliveryType = owhDeliveryType;
    }

    @JsonProperty("eWeighingPortion")
    public Double getEWeighingPortion() {
        return eWeighingPortion;
    }

    @JsonProperty("eWeighingPortion")
    public void setEWeighingPortion(Double eWeighingPortion) {
        this.eWeighingPortion = eWeighingPortion;
    }

    @JsonProperty("usePortionAsEWeighingPort")
    public Double getUsePortionAsEWeighingPort() {
        return usePortionAsEWeighingPort;
    }

    @JsonProperty("usePortionAsEWeighingPort")
    public void setUsePortionAsEWeighingPort(Double usePortionAsEWeighingPort) {
        this.usePortionAsEWeighingPort = usePortionAsEWeighingPort;
    }

    @JsonProperty("documentPath")
    public String getDocumentPath() {
        return documentPath;
    }

    @JsonProperty("documentPath")
    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    @JsonProperty("crateLabelNumber")
    public Double getCrateLabelNumber() {
        return crateLabelNumber;
    }

    @JsonProperty("crateLabelNumber")
    public void setCrateLabelNumber(Double crateLabelNumber) {
        this.crateLabelNumber = crateLabelNumber;
    }

    @JsonProperty("salePriceBasedOnConsumer")
    public Boolean getSalePriceBasedOnConsumer() {
        return salePriceBasedOnConsumer;
    }

    @JsonProperty("salePriceBasedOnConsumer")
    public void setSalePriceBasedOnConsumer(Boolean salePriceBasedOnConsumer) {
        this.salePriceBasedOnConsumer = salePriceBasedOnConsumer;
    }

    @JsonProperty("registrationMethod")
    public Double getRegistrationMethod() {
        return registrationMethod;
    }

    @JsonProperty("registrationMethod")
    public void setRegistrationMethod(Double registrationMethod) {
        this.registrationMethod = registrationMethod;
    }

    @JsonProperty("minimumWeight")
    public Double getMinimumWeight() {
        return minimumWeight;
    }

    @JsonProperty("minimumWeight")
    public void setMinimumWeight(Double minimumWeight) {
        this.minimumWeight = minimumWeight;
    }

    @JsonProperty("maximumWeight")
    public Double getMaximumWeight() {
        return maximumWeight;
    }

    @JsonProperty("maximumWeight")
    public void setMaximumWeight(Double maximumWeight) {
        this.maximumWeight = maximumWeight;
    }

    @JsonProperty("createBackorderOnExcessiveStock")
    public Boolean getCreateBackorderOnExcessiveStock() {
        return createBackorderOnExcessiveStock;
    }

    @JsonProperty("createBackorderOnExcessiveStock")
    public void setCreateBackorderOnExcessiveStock(Boolean createBackorderOnExcessiveStock) {
        this.createBackorderOnExcessiveStock = createBackorderOnExcessiveStock;
    }

    @JsonProperty("createBackorderOnStockShortage")
    public Boolean getCreateBackorderOnStockShortage() {
        return createBackorderOnStockShortage;
    }

    @JsonProperty("createBackorderOnStockShortage")
    public void setCreateBackorderOnStockShortage(Boolean createBackorderOnStockShortage) {
        this.createBackorderOnStockShortage = createBackorderOnStockShortage;
    }

    @JsonProperty("backorderType")
    public Double getBackorderType() {
        return backorderType;
    }

    @JsonProperty("backorderType")
    public void setBackorderType(Double backorderType) {
        this.backorderType = backorderType;
    }

    @JsonProperty("boxQuantity")
    public Double getBoxQuantity() {
        return boxQuantity;
    }

    @JsonProperty("boxQuantity")
    public void setBoxQuantity(Double boxQuantity) {
        this.boxQuantity = boxQuantity;
    }

    @JsonProperty("boxesPerLayer")
    public Double getBoxesPerLayer() {
        return boxesPerLayer;
    }

    @JsonProperty("boxesPerLayer")
    public void setBoxesPerLayer(Double boxesPerLayer) {
        this.boxesPerLayer = boxesPerLayer;
    }

    @JsonProperty("layersPerPallet")
    public Double getLayersPerPallet() {
        return layersPerPallet;
    }

    @JsonProperty("layersPerPallet")
    public void setLayersPerPallet(Double layersPerPallet) {
        this.layersPerPallet = layersPerPallet;
    }

    @JsonProperty("useGraiCode")
    public Double getUseGraiCode() {
        return useGraiCode;
    }

    @JsonProperty("useGraiCode")
    public void setUseGraiCode(Double useGraiCode) {
        this.useGraiCode = useGraiCode;
    }

    @JsonProperty("lengthOfGraiCode")
    public Double getLengthOfGraiCode() {
        return lengthOfGraiCode;
    }

    @JsonProperty("lengthOfGraiCode")
    public void setLengthOfGraiCode(Double lengthOfGraiCode) {
        this.lengthOfGraiCode = lengthOfGraiCode;
    }

    @JsonProperty("defProdMultiBranchId")
    public Double getDefProdMultiBranchId() {
        return defProdMultiBranchId;
    }

    @JsonProperty("defProdMultiBranchId")
    public void setDefProdMultiBranchId(Double defProdMultiBranchId) {
        this.defProdMultiBranchId = defProdMultiBranchId;
    }

    @JsonProperty("eanTradeUnitCode")
    public String getEanTradeUnitCode() {
        return eanTradeUnitCode;
    }

    @JsonProperty("eanTradeUnitCode")
    public void setEanTradeUnitCode(String eanTradeUnitCode) {
        this.eanTradeUnitCode = eanTradeUnitCode;
    }

    @JsonProperty("wssCheckMaximumRegistration")
    public Boolean getWssCheckMaximumRegistration() {
        return wssCheckMaximumRegistration;
    }

    @JsonProperty("wssCheckMaximumRegistration")
    public void setWssCheckMaximumRegistration(Boolean wssCheckMaximumRegistration) {
        this.wssCheckMaximumRegistration = wssCheckMaximumRegistration;
    }

    @JsonProperty("trackPackingBalance")
    public Boolean getTrackPackingBalance() {
        return trackPackingBalance;
    }

    @JsonProperty("trackPackingBalance")
    public void setTrackPackingBalance(Boolean trackPackingBalance) {
        this.trackPackingBalance = trackPackingBalance;
    }

    @JsonProperty("checkMinMaxPortionWeightOnOrderEntry")
    public Boolean getCheckMinMaxPortionWeightOnOrderEntry() {
        return checkMinMaxPortionWeightOnOrderEntry;
    }

    @JsonProperty("checkMinMaxPortionWeightOnOrderEntry")
    public void setCheckMinMaxPortionWeightOnOrderEntry(Boolean checkMinMaxPortionWeightOnOrderEntry) {
        this.checkMinMaxPortionWeightOnOrderEntry = checkMinMaxPortionWeightOnOrderEntry;
    }

    @JsonProperty("ssccRegisterMethod")
    public Double getSsccRegisterMethod() {
        return ssccRegisterMethod;
    }

    @JsonProperty("ssccRegisterMethod")
    public void setSsccRegisterMethod(Double ssccRegisterMethod) {
        this.ssccRegisterMethod = ssccRegisterMethod;
    }

    @JsonProperty("planningUnitType")
    public Double getPlanningUnitType() {
        return planningUnitType;
    }

    @JsonProperty("planningUnitType")
    public void setPlanningUnitType(Double planningUnitType) {
        this.planningUnitType = planningUnitType;
    }

    @JsonProperty("usePlanningUnitType")
    public Boolean getUsePlanningUnitType() {
        return usePlanningUnitType;
    }

    @JsonProperty("usePlanningUnitType")
    public void setUsePlanningUnitType(Boolean usePlanningUnitType) {
        this.usePlanningUnitType = usePlanningUnitType;
    }

    @JsonProperty("palletLabelNumber")
    public Double getPalletLabelNumber() {
        return palletLabelNumber;
    }

    @JsonProperty("palletLabelNumber")
    public void setPalletLabelNumber(Double palletLabelNumber) {
        this.palletLabelNumber = palletLabelNumber;
    }

    @JsonProperty("countryOfOrigin")
    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    @JsonProperty("countryOfOrigin")
    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    @JsonProperty("analysisPurchasePriceType")
    public Double getAnalysisPurchasePriceType() {
        return analysisPurchasePriceType;
    }

    @JsonProperty("analysisPurchasePriceType")
    public void setAnalysisPurchasePriceType(Double analysisPurchasePriceType) {
        this.analysisPurchasePriceType = analysisPurchasePriceType;
    }

    @JsonProperty("useAnalysisPurchasePriceBatch")
    public Boolean getUseAnalysisPurchasePriceBatch() {
        return useAnalysisPurchasePriceBatch;
    }

    @JsonProperty("useAnalysisPurchasePriceBatch")
    public void setUseAnalysisPurchasePriceBatch(Boolean useAnalysisPurchasePriceBatch) {
        this.useAnalysisPurchasePriceBatch = useAnalysisPurchasePriceBatch;
    }

    @JsonProperty("priceLookUpCode")
    public Double getPriceLookUpCode() {
        return priceLookUpCode;
    }

    @JsonProperty("priceLookUpCode")
    public void setPriceLookUpCode(Double priceLookUpCode) {
        this.priceLookUpCode = priceLookUpCode;
    }

    @JsonProperty("length")
    public Double getLength() {
        return length;
    }

    @JsonProperty("length")
    public void setLength(Double length) {
        this.length = length;
    }

    @JsonProperty("width")
    public Double getWidth() {
        return width;
    }

    @JsonProperty("width")
    public void setWidth(Double width) {
        this.width = width;
    }

    @JsonProperty("height")
    public Double getHeight() {
        return height;
    }

    @JsonProperty("height")
    public void setHeight(Double height) {
        this.height = height;
    }

    @JsonProperty("volume")
    public Double getVolume() {
        return volume;
    }

    @JsonProperty("volume")
    public void setVolume(Double volume) {
        this.volume = volume;
    }

    @JsonProperty("ediExportType")
    public Double getEdiExportType() {
        return ediExportType;
    }

    @JsonProperty("ediExportType")
    public void setEdiExportType(Double ediExportType) {
        this.ediExportType = ediExportType;
    }

    @JsonProperty("ediExportCrateNumber")
    public Double getEdiExportCrateNumber() {
        return ediExportCrateNumber;
    }

    @JsonProperty("ediExportCrateNumber")
    public void setEdiExportCrateNumber(Double ediExportCrateNumber) {
        this.ediExportCrateNumber = ediExportCrateNumber;
    }

    @JsonProperty("useFTrace")
    public Boolean getUseFTrace() {
        return useFTrace;
    }

    @JsonProperty("useFTrace")
    public void setUseFTrace(Boolean useFTrace) {
        this.useFTrace = useFTrace;
    }

    @JsonProperty("kantelEenheid_4")
    public Double getKantelEenheid4() {
        return kantelEenheid4;
    }

    @JsonProperty("kantelEenheid_4")
    public void setKantelEenheid4(Double kantelEenheid4) {
        this.kantelEenheid4 = kantelEenheid4;
    }

    @JsonProperty("kantelEenheid_5")
    public Double getKantelEenheid5() {
        return kantelEenheid5;
    }

    @JsonProperty("kantelEenheid_5")
    public void setKantelEenheid5(Double kantelEenheid5) {
        this.kantelEenheid5 = kantelEenheid5;
    }

    @JsonProperty("kantelOpslag_4")
    public Double getKantelOpslag4() {
        return kantelOpslag4;
    }

    @JsonProperty("kantelOpslag_4")
    public void setKantelOpslag4(Double kantelOpslag4) {
        this.kantelOpslag4 = kantelOpslag4;
    }

    @JsonProperty("kantelOpslag_5")
    public Double getKantelOpslag5() {
        return kantelOpslag5;
    }

    @JsonProperty("kantelOpslag_5")
    public void setKantelOpslag5(Double kantelOpslag5) {
        this.kantelOpslag5 = kantelOpslag5;
    }

    @JsonProperty("kantelOpslagMethode")
    public Double getKantelOpslagMethode() {
        return kantelOpslagMethode;
    }

    @JsonProperty("kantelOpslagMethode")
    public void setKantelOpslagMethode(Double kantelOpslagMethode) {
        this.kantelOpslagMethode = kantelOpslagMethode;
    }

    @JsonProperty("dontShowENumber")
    public Double getDontShowENumber() {
        return dontShowENumber;
    }

    @JsonProperty("dontShowENumber")
    public void setDontShowENumber(Double dontShowENumber) {
        this.dontShowENumber = dontShowENumber;
    }

    @JsonProperty("verpakkingPDMCode")
    public String getVerpakkingPDMCode() {
        return verpakkingPDMCode;
    }

    @JsonProperty("verpakkingPDMCode")
    public void setVerpakkingPDMCode(String verpakkingPDMCode) {
        this.verpakkingPDMCode = verpakkingPDMCode;
    }

    @JsonProperty("uploadProductData")
    public Double getUploadProductData() {
        return uploadProductData;
    }

    @JsonProperty("uploadProductData")
    public void setUploadProductData(Double uploadProductData) {
        this.uploadProductData = uploadProductData;
    }

    @JsonProperty("showCountryOfOrigin")
    public Double getShowCountryOfOrigin() {
        return showCountryOfOrigin;
    }

    @JsonProperty("showCountryOfOrigin")
    public void setShowCountryOfOrigin(Double showCountryOfOrigin) {
        this.showCountryOfOrigin = showCountryOfOrigin;
    }

    @JsonProperty("weergaveExtraAlbas")
    public Boolean getWeergaveExtraAlbas() {
        return weergaveExtraAlbas;
    }

    @JsonProperty("weergaveExtraAlbas")
    public void setWeergaveExtraAlbas(Boolean weergaveExtraAlbas) {
        this.weergaveExtraAlbas = weergaveExtraAlbas;
    }

    @JsonProperty("bestBeforeDateDeterminationMethod")
    public Double getBestBeforeDateDeterminationMethod() {
        return bestBeforeDateDeterminationMethod;
    }

    @JsonProperty("bestBeforeDateDeterminationMethod")
    public void setBestBeforeDateDeterminationMethod(Double bestBeforeDateDeterminationMethod) {
        this.bestBeforeDateDeterminationMethod = bestBeforeDateDeterminationMethod;
    }

    @JsonProperty("bestBeforeDateCheckUpMethod")
    public Double getBestBeforeDateCheckUpMethod() {
        return bestBeforeDateCheckUpMethod;
    }

    @JsonProperty("bestBeforeDateCheckUpMethod")
    public void setBestBeforeDateCheckUpMethod(Double bestBeforeDateCheckUpMethod) {
        this.bestBeforeDateCheckUpMethod = bestBeforeDateCheckUpMethod;
    }

    @JsonProperty("purchasePrice")
    public Double getPurchasePrice() {
        return purchasePrice;
    }

    @JsonProperty("purchasePrice")
    public void setPurchasePrice(Double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    @JsonProperty("salePrice")
    public Double getSalePrice() {
        return salePrice;
    }

    @JsonProperty("salePrice")
    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    @JsonProperty("consumerSalePrice")
    public Double getConsumerSalePrice() {
        return consumerSalePrice;
    }

    @JsonProperty("consumerSalePrice")
    public void setConsumerSalePrice(Double consumerSalePrice) {
        this.consumerSalePrice = consumerSalePrice;
    }

    @JsonProperty("fixedInvoicePrice")
    public Double getFixedInvoicePrice() {
        return fixedInvoicePrice;
    }

    @JsonProperty("fixedInvoicePrice")
    public void setFixedInvoicePrice(Double fixedInvoicePrice) {
        this.fixedInvoicePrice = fixedInvoicePrice;
    }

    @JsonProperty("bidfoodArtCategorie")
    public Double getBidfoodArtCategorie() {
        return bidfoodArtCategorie;
    }

    @JsonProperty("bidfoodArtCategorie")
    public void setBidfoodArtCategorie(Double bidfoodArtCategorie) {
        this.bidfoodArtCategorie = bidfoodArtCategorie;
    }

    @JsonProperty("operations")
    public List<String> getOperations() {
        return operations;
    }

    @JsonProperty("operations")
    public void setOperations(List<String> operations) {
        this.operations = operations;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
