
package json2pojo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "number",
    "exists",
    "name",
    "uncPath",
    "administrationBranchType"
})
public class DefaultOrderAtAdministrationBranch {

    @JsonProperty("number")
    private Double number;
    @JsonProperty("exists")
    private Boolean exists;
    @JsonProperty("name")
    private String name;
    @JsonProperty("uncPath")
    private String uncPath;
    @JsonProperty("administrationBranchType")
    private Double administrationBranchType;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("number")
    public Double getNumber() {
        return number;
    }

    @JsonProperty("number")
    public void setNumber(Double number) {
        this.number = number;
    }

    @JsonProperty("exists")
    public Boolean getExists() {
        return exists;
    }

    @JsonProperty("exists")
    public void setExists(Boolean exists) {
        this.exists = exists;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("uncPath")
    public String getUncPath() {
        return uncPath;
    }

    @JsonProperty("uncPath")
    public void setUncPath(String uncPath) {
        this.uncPath = uncPath;
    }

    @JsonProperty("administrationBranchType")
    public Double getAdministrationBranchType() {
        return administrationBranchType;
    }

    @JsonProperty("administrationBranchType")
    public void setAdministrationBranchType(Double administrationBranchType) {
        this.administrationBranchType = administrationBranchType;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
