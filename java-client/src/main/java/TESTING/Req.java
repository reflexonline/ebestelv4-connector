package TESTING;

import java.util.HashMap;

public class Req {

	private Types.RequestType reqType;
	private Enum apiEndpointEnum;
	private ReqDTO requestDTO;
	private HashMap<String, String> urlParams;

	public Enum getApiEndpointEnum() {
		return apiEndpointEnum;
	}
	public Types.RequestType getReqType() {
		return reqType;
	}
	public void setReqType(Types.RequestType reqType) {
		this.reqType = reqType;
	}
	public void setApiEndpointEnum(Enum apiEndpointEnum) {
		this.apiEndpointEnum = apiEndpointEnum;
	}
	public ReqDTO getRequestDTO() {
		return requestDTO;
	}
	public void setRequestDTO(ReqDTO requestDTO) {
		this.requestDTO = requestDTO;
	}

	public HashMap<String, String> getUrlParams() {
		return urlParams;
	}

	public void setUrlParams(HashMap<String, String> urlParams) {
		this.urlParams = urlParams;
	}

}
