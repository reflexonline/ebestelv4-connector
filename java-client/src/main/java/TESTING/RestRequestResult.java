package TESTING;

//import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class RestRequestResult {
	private Object resultObject;
	private String resultObjectClassName;
	private Integer errorCode;
	private Integer httpResponseCode;
	private String errorDescription;
	private Exception exception;

	

	@JsonIgnore
	public Object getResultObject() {
		return resultObject;
	}

	public void setResultObject(Object resultObject) {
		this.resultObject = resultObject;
	}
	
	
	@JsonIgnore
	public String getResultObjectClassName() {
		return resultObjectClassName;
	}

	public void setResultObjectClassName(String resultObjectClassName) {
		this.resultObjectClassName = resultObjectClassName;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public Integer getHttpResponseCode() {
		return httpResponseCode;
	}

	public void setHttpResponseCode(Integer httpResponseCode) {
		this.httpResponseCode = httpResponseCode;
	}


	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	@JsonIgnore
	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

}
