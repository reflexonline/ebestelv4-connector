package TESTING;

import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Properties;

import javax.xml.transform.Result;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.MimeType;

import com.google.gson.Gson;

import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.Configuration;
import io.swagger.client.auth.*;
import io.swagger.client.model.SaleArticleDto;
import io.swagger.client.api.SaleArticleApi;
//import io.swagger.v3.core.jackson.*;

import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.stream.JsonReader;

import com.google.gson.JsonObject;


public class StartB {

	public void doSomething() {
		System.out.println("doSomething");
		
		ApiClient apiCl = Configuration.getDefaultApiClient();
		
        String at = "AQAAANCMnd8BFdERjHoAwE_Cl-sBAAAAVt3R5gwuKUGYKrVFQpWDNwAAAAACAAAAAAAQZgAAAAEAACAAAAA23Ki04KwBwFrAaiW2AXknZlRar4kPma8kVi-VKMPM1AAAAAAOgAAAAAIAACAAAAAfE7wNtMvy4RoACSE8SMZoyTIuMVqt9_LtJmXbB347tgABAAAP7CbqqtgoDovuU_guqZhh6dhJ1PA3_t6KhGSeViz-NkK1duPhMSlyQlNOfz_0aJ9cXZI_ZC2MGqthUmYNKhD2q2KF_7vmbCbtKo18dV62ztHICCK_yI8mY5e1sdorEHA5tDtWpOgrRLldsNveEfzF-pNS9ldXM9XrByJem0UllLHCdcHe184HEinsJC43Ew4vwRYIH4RoeXP7ijzRND_rum6NEENVziGHDtbMasoOGX4EzEVatUAwWoYrAQKRyVyNRFQq-hSFSbXJq2slpIK04cApm1Ig0SbzynIxyfJKbbftTkkR2wFYYa1naNZNgMN52nlQvXho2zoIbfeTmIyvQAAAAPiArJ1Ez1Yl6XiY0ew2tXSiZ812ZhZ0ymK6Vraou_zRJIdK-QtXBz3_s0Ppzf0wWgPb7KLKTZrpzN84UawNE-Q";
		
		// Configure OAuth2 access token for authorization: oauth2
		OAuth oauth2 = (OAuth) apiCl.getAuthentication("oauth2");
		oauth2.setAccessToken(at);
	
		SaleArticleApi apiInstance = new SaleArticleApi();
		Integer number = 1; // Integer | 
		try {
		    Object result = apiInstance.saleArticleGetSaleArticle(number);
		    System.out.println(result);
		} catch (ApiException e) {
		    System.err.println("Exception when calling SaleArticleApi#saleArticleGetSaleArticle");
		    e.printStackTrace();
		}
		
	}	

	public Object doSomethingElse(Integer number) throws NoSuchFieldException {
		System.out.println("doSomethingElse");

		Properties appProps = new Properties();
		appProps.put("reflex.apiurl", "http://localhost:7000");
		appProps.put("reflex.clientidentifier", "reflex");
		appProps.put("reflex.user", "ADMIN");
		appProps.put("reflex.password", "REFLEX");
		appProps.put("reflex.granttype", "password");
		RestConsumerService resConServ = new RestConsumerService(appProps);

		ApiClient apiCl = Configuration.getDefaultApiClient();

		// Configure OAuth2 access token for authorization: oauth2
		OAuth oauth2 = (OAuth) apiCl.getAuthentication("oauth2");
		oauth2.setAccessToken(resConServ.getToken());

		SaleArticleApi apiInstance = new SaleArticleApi();
		//SaleArticleDto result = new SaleArticleDto(); // SaleArticleDto | 
		try {
			//JsonObject result = new JsonObject();
			Object result = apiInstance.saleArticleGetSaleArticle(number);
			//result = (SaleArticleDto) apiInstance.saleArticleGetSaleArticle(number);
			System.out.println(result);
			//System.out.println(result.getClass().getField("saleArticleTranslation"));
//			System.out.println(result.getClass().getFields());
//			
//			System.out.println(((LinkedTreeMap) result).containsKey("saleArticleTranslation"));
//			System.out.println(((LinkedTreeMap) result).values());
//			System.out.println(((LinkedTreeMap) result).get("saleArticleTranslation"));
			
//			System.out.println(((LinkedTreeMap) result).get("saleArticleTranslation").get);
//
//			JSONObject employeeObject = (JSONObject) employee.get("employee")
			
			//System.out.println(((LinkedTreeMap) result).getType().isArray());
			//((SaleArticleDto) result).
			
			//System.out.println("A0: " + result.getClass().getFields()[0]);
//			System.out.println("A1: " + result.getClass().getFields()[1]);
//			System.out.println("A2: " + result.getClass().getFields()[2]);
//			System.out.println("A3: " + result.getClass().getFields()[3]);
//			System.out.println("A4: " + result.getClass().getFields()[4]);
//			System.out.println("A5: " + result.getClass().getFields()[5]);
//			System.out.println("A6: " + result.getClass().getFields()[6]);
//			System.out.println("A7: " + result.getClass().getFields()[7]);
//			System.out.println("A8: " + result.getClass().getFields()[8]);
//			System.out.println("A9: " + result.getClass().getFields()[9]);
//			
//			System.out.println("B: " + result.getClass().getField("saleArticleTranslation").toString());
//			try {
//				System.out.println("B: " + result.getClass().getField("purchaseArticle").toString());
//			}
//			catch (NoSuchFieldException e) {}
			return result;
			
		} catch (ApiException e) {
			System.err.println("Exception when calling SaleArticleApi#saleArticleGetSaleArticle");
			e.printStackTrace();
		}
		
		return null;
	}
	
}