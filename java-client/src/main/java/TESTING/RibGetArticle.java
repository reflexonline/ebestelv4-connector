package TESTING;

import java.io.File;
import java.io.StringReader;

import javax.tools.JavaFileObject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonSubTypes;
//import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.*;

import io.swagger.client.model.SaleArticleDto;

import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.persistence.jaxb.MarshallerProperties;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.eclipse.persistence.jaxb.xmlmodel.ObjectFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


class RibGetArticle {

	public static void main(String[] args) throws NoSuchFieldException {
		StartB b = new StartB();
		
		Object result = b.doSomethingElse(2);
		System.out.println("RibGetArticle result: " + result);

		
		Gson gson = new Gson();

		// Serialize.
        String json = gson.toJson(result);
        String json2 = json;
        
        json2 = jsonReplace(json2,"allergens","allergen","allergenId",12);
        json2 = jsonReplace2(json2,"orderGroup1","saleArticleOrderGroupId",1);
        json2 = jsonReplace2(json2,"saleArticleGroup","number",1);
        
        
//        System.out.println("RibGetArticle json: " + json);
//        JSONObject jsonObject = new JSONObject(json);
//		//SaleArticleDto sadto = gson.fromJson(json, SaleArticleDto.class);
//
//        JSONArray jsonArray = new JSONArray();
//        jsonArray = jsonObject.getJSONArray("allergens");
//		System.out.println("RibGetArticle allergens: " + jsonObject.getJSONArray("allergens"));
//        
//		for (int count = 0; count <= 11; count++) {
//	        JSONObject jsonObject2 = jsonArray.getJSONObject(count);
//			System.out.println("RibGetArticle jsonObject2: " + jsonObject2);
//			
//			JSONObject jsonObject3 = jsonObject2.getJSONObject("allergen");
//			System.out.println("RibGetArticle jsonObject3: " + jsonObject3);
//			//System.out.println("RibGetArticle jsonObject3.toString().replace(\"1\", \"1.0\"): " + jsonObject3.toString().replace("1", "1.0"));
//			
//			json2 = json2.replace(jsonObject3.toString().replace(Integer.toString(count + 1), Integer.toString(count + 1) + ".0"), Integer.toString(count + 1));
//		}
		
		System.out.println("RibGetArticle json2: " + json2);
		 
		SaleArticleDto sadto = gson.fromJson(json2, SaleArticleDto.class);
		
		System.out.println("sadto: " + sadto);		

       // System.out.println("RibGetArticle shortName: " + jsonObject.getString("shortName"));
        
//        // Deserialize.
//        Map<String, String> map2 = gson.fromJson(json, new TypeToken<Map<String, String>>() {}.getType());
//        System.out.println(map2); // {key1=value1, key2=value2, key3=value3}
        
//		Type listType = new TypeToken<ArrayList<SaleArticleDto>>() { }.getType();
//        List<SaleArticleDto> sadto = new Gson().fromJson(json, listType);		

        
		
//		JSONObject jobj = new JSONObject();
//		jobj = convertKeyValueToJSON(LinkedTreeMap<String, Object> result);
		
		
//		Type listType = new TypeToken<ArrayList<SaleArticleDto>>() { }.getType();
//		String jsonArray = null;
//		List<SaleArticleDto> sadto = new Gson().fromJson(jsonArray, listType);		
		
//		Gson gson = new Gson();
//		SaleArticleDto sadto = gson.fromJson(result.toString(), SaleArticleDto.class);
		
		
//		Gson gson = new Gson();
//		JSONObject jsonObject = gson.toJsonTree(result).getAsJsonObject();
//		gson.fromJson(jsonObject, JSONObject.class);
		
//        Gson gson = new Gson();
//		SaleArticleDto sadto = gson.fromJson(result.toString(),SaleArticleDto.class);		
		
//        Gson gson = new Gson();
//        JavaFileObject jsonObj = new JavaFileObject(result.get());
//            .toString());
//        servDto = gson.fromJson(jsonObj.toString(),
//            ServiceDto.class);		
		
		
		
//		Gson gson = new Gson();
//		JsonObject jsonObj = new JsonObject(result.toString());
//		Type type = new TypeToken<SaleArticleDto>() {}.getType();
//		SaleArticleDto sadto = gson.fromJson(result.toString(),(java.lang.reflect.Type) type);		

		//jsonObj.toString()
		
//		Gson gson = new Gson();
//		JSONObject jsonObj = new JSONObject(object.get(DO_SERVICES).toString());
//		Type type = new TypeToken<MyDto>() {}.getType();
//		servDto = gson.fromJson(jsonObj.toString(),type);		
		
		//jaxbObjectToJSON(result);		
		
//		String strResult = result.toString();

//        Gson gson = new Gson();
//        SaleArticleDto sadto = gson.fromJson(result.toString(), SaleArticleDto.class);
        
		//SaleArticleDto sadto = new SaleArticleDto();
		//sadto = (SaleArticleDto) result;

//		SaleArticleDto sadto = new SaleArticleDto();
//		sadto = Unmarshal(strResult);
		
//		System.out.println("sadto: " + sadto);		
		
//		System.out.println("sadto.getNumber(): " + sadto.getNumber());
		
		//result mappen naar RibArticleDTO
		
		RibArticleDTO art = new RibArticleDTO();
		art.setName("jopie");
		System.out.println("art.getName(): " + art.getName());
	}
	
//eventueel eigen json adapter schrijven voor enum 1 = None, 2 = Low, 3 = High ...  	

//	public String psInFoodMarshalXML(Products psInFoodXml)
//	{
//		JAXBContext jaxbContext;
//		try {
//			jaxbContext = JAXBContext.newInstance(Products.class);
//			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
//
//			StringWriter stringWriter = new StringWriter();
//			jaxbMarshaller.marshal(psInFoodXml, stringWriter);
//			String productsXml = stringWriter.toString();
//
//			return productsXml;
//
//		} catch (JAXBException e) {
//			// TODO Auto-generated catch block
//			LOG.error("Cannot marshal body", e);
//		}
//		return null;
//
//	}	
	


//    private static void jaxbObjectToJSON(Object obj) 
//    {
//        try
//        {
//            JAXBContext jaxbContext = JAXBContext.newInstance(Object.class);
//            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
// 
//            // To format JSON
//            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); 
//             
//            //Set JSON type
//            jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
//            jaxbMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
// 
//            
//            //JAXBContextProperties
//            
//             //Store JSON to File
//            File file = new File("object.json");
//            jaxbMarshaller.marshal(obj, file);
//        } 
//        catch (JAXBException e) 
//        {
//            e.printStackTrace();
//        }
//    }
	
	

//    public static JSONObject convertKeyValueToJSON(LinkedTreeMap<String, Object> ltm) {
//    	JSONObject jo=new JSONObject();
//        Object[] objs = ltm.entrySet().toArray();
//        for (int l=0;l<objs.length;l++)
//        {
//            Map.Entry o= (Map.Entry) objs[l];
//            try {
//                if (o.getValue() instanceof LinkedTreeMap)
//                    jo.put(o.getKey().toString(),convertKeyValueToJSON((LinkedTreeMap<String, Object>) o.getValue()));
//                else
//                    jo.put(o.getKey().toString(),o.getValue());
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//        return jo;
//    }    
    
  public static String jsonReplace(String jsonIn, String searchStr1, String searchStr2, String searchStr3, int total) {

    System.out.println("RibGetArticle jsonIn: " + jsonIn);
    JSONObject jsonObject = new JSONObject(jsonIn);

    JSONArray jsonArray = new JSONArray();
    jsonArray = jsonObject.getJSONArray(searchStr1);
	System.out.println("RibGetArticle " + searchStr1  + " : " + jsonObject.getJSONArray(searchStr1));
    
	for (int count = 1; count <= total; count++) {
        JSONObject jsonObject2 = jsonArray.getJSONObject(count - 1);
		System.out.println("RibGetArticle jsonObject2: " + jsonObject2);
		
		JSONObject jsonObject3 = jsonObject2.getJSONObject(searchStr2);
		System.out.println("RibGetArticle jsonObject3: " + jsonObject3);
		Integer nrInteger = jsonObject3.getInt(searchStr3);
		System.out.println("RibGetArticle nrInt: " + nrInteger.toString());
		Float nrFloat = jsonObject3.getFloat(searchStr3);
		System.out.println("RibGetArticle nrFloat: " + nrFloat.toString());
		
		jsonIn = jsonIn.replace(jsonObject3.toString().replace(nrInteger.toString(), nrFloat.toString()), nrFloat.toString());
	}
	return jsonIn;

  }
	
  public static String jsonReplace2(String jsonIn, String searchStr1, String searchStr2, int total) {

	    System.out.println("RibGetArticle jsonIn: " + jsonIn);
	    JSONObject jsonObject = new JSONObject(jsonIn);

		for (int count = 1; count <= total; count++) {
	        JSONObject jsonObject2 = jsonObject.getJSONObject(searchStr1);
			System.out.println("RibGetArticle jsonObject2: " + jsonObject2);

			Integer nrInteger = jsonObject2.getInt(searchStr2);
			System.out.println("RibGetArticle nrInt: " + nrInteger.toString());
			Float nrFloat = jsonObject2.getFloat(searchStr2);
			System.out.println("RibGetArticle nrFloat: " + nrFloat.toString());
			
			System.out.println("RibGetArticle jsonObject2.toString().replace(nrInteger.toString(), nrFloat.toString()): " + jsonObject2.toString().replace(nrInteger.toString(), nrFloat.toString()));

			
			jsonObject.put(searchStr1, nrFloat);
			
			
			//jsonIn = jsonIn.replace(jsonObject2.toString().replace(nrInteger.toString(), nrFloat.toString()), nrFloat.toString());
		}
		
		jsonIn = jsonObject.toString();
		
		return jsonIn;

	  }
	
}