package TESTING;

//logging is voorlopig uitgeschakeld

//package com.reflex.plugins.online.r3k.api;


import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

//import com.reflex.plugins.online.r3k.types.R3kTypes;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

//import com.reflex.plugins.online.r3k.api._1_0_0.dto.R3kLoginDTO;
//import com.reflex.plugins.online.r3k.api._1_0_0.dto.R3kTokenDTO;
//import com.reflex.plugins.online.r3k.req.R3kReq;
//import com.reflex.plugins.online.rblue.api.dto.RblueTokenDTO;

public class RestConsumerService {

//    private static final Logger LOG = LoggerFactory.getLogger(RestConsumerService.class);

    //private static String R3kEndPointLogin = "/api/security/login";
	private static String EndPointLogin = "/token";
    //private static String R3kEndPointCreateProductionTask = "CreateProductionTask";
    private String token = null;
    private Properties appProps = null;

    private static HttpComponentsClientHttpRequestFactory requestFactory;


    public RestConsumerService(Properties appProps) {
		super();
		this.appProps = appProps;
	}

//	@Override
    public String getToken() {
        if (token != null)
        {
            return token;
        }
        LoginDTO loginDTO = new LoginDTO(appProps.getProperty("reflex.user"), appProps.getProperty("reflex.password"), appProps.getProperty("reflex.granttype"));
        //RbLoginDto rbLoginDto = new RbLoginDto(rbRestClientProps.getRbClientJiraUserName(), rbRestClientProps.getRbClientJiraUserPassword());
        RestRequestResult restRequestResult = new RestRequestResult();
        ResponseEntity<TokenDTO> exchange = sendRestRequest(RestConsumerService.EndPointLogin, loginDTO.getFormEncodedContent(), HttpMethod.POST, TokenDTO.class, restRequestResult);
        
//        if (exchange != null) {
//            LOG.debug("Received Token on LOGON: " + exchange.getBody());
//            this.token = exchange.getBody().getToken();
//        }
        if (restRequestResult.getResultObject() != null) {
//          LOG.debug("Received Token on LOGON: " + exchange.getBody());
        	this.token = ((TokenDTO) restRequestResult.getResultObject()).access_token;
        }
        else
        {
            this.token = null;
        }
        return this.token;
    }

    private HttpHeaders getRestConsumerHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
        headers.add("x-rng-client-identifier", appProps.getProperty("reflex.clientidentifier"));
        headers.add("Content-Type", "application/json; charset=UTF-8");
        return headers;
    }

																									 

////    @Override
//    public String handleReq(Req Req, RestRequestResult restRequestResult) {
//    	
//    	R3kEndpointMapping mapping = R3kEndpointMapping.getR3kEndpointMappingByRequestTypeAndEndpointEnum(Req.getReqType(), Req.getApiEndpointEnum());
//    	if (mapping ==null)
//        {
////            LOG.error("No mapping found for " + Req.getReqType() + "," + Req.getApiEndpointEnum());
//            return null;
//        }
//        final String[] endpointPath = {mapping.getEndpointPath()};
//        ResponseEntity<String> exchange = null;
//        HttpMethod httpMethodToUse = HttpMethod.GET;
//        HashMap<String, String> urlParamsMap = Req.getUrlParams();
//        if (urlParamsMap != null)
//        {
//            Set<String> urlParams = urlParamsMap.keySet();
//            urlParams.forEach( urlParam -> endpointPath[0] = endpointPath[0].replace("{" + urlParam + "}", urlParamsMap.get(urlParam)));
//        }
//    	if (Req.getReqType().equals(Types.RequestType.GET))
//        {
//            httpMethodToUse = HttpMethod.GET;
//        }
//        else if (Req.getReqType().equals(Types.RequestType.POST)) {
////            LOG.debug("POST request to " + endpointPath[0]);
//            httpMethodToUse = HttpMethod.POST;
//        }
//        exchange  = sendRestRequest(endpointPath[0].toString(), Req.getRequestDTO(), httpMethodToUse, mapping.getResponseDTOClass(), restRequestResult);
//
//        if (exchange != null) {
//            return exchange.getBody();
//        }
//        return null;    
//	
//
//    }

    private <T> ResponseEntity<T> sendRestRequest(String endPointUrl, Object entityObject, HttpMethod httpMethod, Class responseClass, RestRequestResult restRequestResult) {
        HttpEntity<Object> entity = null;
        HttpComponentsClientHttpRequestFactory requestFactory = null;
        RestTemplate restTemplate = null;
        HttpHeaders headers = null;
        restRequestResult.setErrorCode(0); //Suppose we are succesful
        try {
            restTemplate = new RestTemplate();
            requestFactory = getHttpComponentsClientHttpRequestFactory();
            restTemplate.setRequestFactory(requestFactory);

            headers = getRestConsumerHeader();
            if (responseClass != TokenDTO.class) {
                //headers.add("Authorization", "Bearer " + getToken());
            }
            if (entityObject != null) {
//                LOG.debug("Received entity object: " + entityObject.toString());
                entity = new HttpEntity<Object>(entityObject, headers);
            }
            else
            {
                entity = new HttpEntity<Object>(headers);
            }
//            LOG.debug("Call to: " + appProps.getProperty("reflex.apiurl") + endPointUrl);
            ResponseEntity<T> result = restTemplate.exchange(appProps.getProperty("reflex.apiurl") + endPointUrl, httpMethod, entity, responseClass);
            if (responseClass != null) {
                restRequestResult.setResultObjectClassName(responseClass.getName());
            }
            restRequestResult.setResultObject(result.getBody());
            restRequestResult.setHttpResponseCode(result.getStatusCode().value());
						  
        }
        catch (HttpClientErrorException e) {
            //Retry if connection error and we did not request the token, get token
            if ((e.getStatusCode() == HttpStatus.UNAUTHORIZED) && responseClass != TokenDTO.class) {
                token = null; //Invalidate token
                try {
                    headers = getRestConsumerHeader();
                    headers.add("Authorization", "Bearer " + getToken());
                    entity = new HttpEntity<Object>(entityObject, headers);
                    ResponseEntity<T> result = restTemplate.exchange(appProps.getProperty("reflex.apiurl") + endPointUrl, httpMethod, entity, responseClass);
                    restRequestResult.setResultObjectClassName(responseClass.getName());
                    restRequestResult.setResultObject(result.getBody());
                    restRequestResult.setHttpResponseCode(result.getStatusCode().value());
                    return result;
                } catch (Exception e2) {
//                    LOG.error("Exception after retry: ", e2);
                    restRequestResult.setHttpResponseCode(e.getStatusCode().value());
                    restRequestResult.setErrorCode(1);
                    restRequestResult.setErrorDescription("");
                    restRequestResult.setException(e2);
                 
                    if (appProps != null) {
                    	restRequestResult.setErrorDescription(restRequestResult.getErrorDescription() + e.getResponseBodyAsString() + "\nDetails of call: \n\t" +
                        		appProps.getProperty("reflex.apiurl") + endPointUrl + "\n\t" +
                                httpMethod + "\n");
                        
                    	HttpHeaders finalHeaders = headers;
                        StringBuffer headerVals = new StringBuffer();
                        headers.keySet().forEach(key ->
                        {
                            headerVals.append("\tKey: " + key.toString() + "=") ;
                            for (String value : finalHeaders.get(key.toString())) {
                                headerVals.append(" " + value + ",");
                            }
                        });
                        restRequestResult.setErrorDescription(restRequestResult.getErrorDescription() + "\nHeaders=" + headerVals.toString());
//                        LOG.error(restRequestResult.getErrorDescription());
                    }
                }
            }
            else
            {
                restRequestResult.setHttpResponseCode(e.getStatusCode().value());
                restRequestResult.setErrorCode(1);
                restRequestResult.setErrorDescription("");
                restRequestResult.setException(e);
//                LOG.error("HttpClientError: " + e.getStatusCode() + ", " + e.getResponseBodyAsString());
                if (appProps != null) {
                	restRequestResult.setErrorDescription(restRequestResult.getErrorDescription() + e.getResponseBodyAsString() + "\nDetails of call: \n\t" +
                    		appProps.getProperty("reflex.apiurl") + endPointUrl + "\n\t" +
                            httpMethod + "\n");
                    
                    HttpHeaders finalHeaders = headers;
                    StringBuffer headerVals = new StringBuffer();
                    headers.keySet().forEach(key ->
                    {
                        headerVals.append("\tKey: " + key.toString() + "=") ;
                        for (String value : finalHeaders.get(key.toString())) {
                            headerVals.append(" " + value + ",");
                        }
                    });
                    restRequestResult.setErrorDescription(restRequestResult.getErrorDescription() + "\nHeaders=" + headerVals.toString() +  "\n" + e.getResponseBodyAsString());
//                    LOG.error(restRequestResult.getErrorDescription());
                }

            }
        }
        catch (HttpServerErrorException e4) {
            restRequestResult.setHttpResponseCode(e4.getStatusCode().value());
            restRequestResult.setErrorCode(4);
            restRequestResult.setErrorDescription("");
            restRequestResult.setException(e4);
            if (appProps != null) {
                restRequestResult.setErrorDescription(restRequestResult.getErrorDescription() + e4.getResponseBodyAsString() + "\nDetails of call: \n\t" +
                        appProps.getProperty("reflex.apiurl") + endPointUrl + "\n\t" +
                        httpMethod + "\n");

                HttpHeaders finalHeaders = headers;
                StringBuffer headerVals = new StringBuffer();
                headers.keySet().forEach(key ->
                {
                    headerVals.append("\tKey: " + key.toString() + "=");
                    for (String value : finalHeaders.get(key.toString())) {
                        headerVals.append(" " + value + ",");
                    }
                });
                restRequestResult.setErrorDescription(restRequestResult.getErrorDescription() + "\nHeaders=" + headerVals.toString() + "\n" + e4.getResponseBodyAsString());
//                LOG.error(restRequestResult.getErrorDescription());
            }
        }
        catch (Exception e2)
        {
            if ( entity==null || appProps == null)
            {
//                LOG.error("Exception appProps or entity is null, endpoint=" + endPointUrl, e2);
            }
            else {
//                LOG.error("Failed sending entity " + entity == null ? "NULL" : entity.toString() + " to " + appProps.getProperty("reflex.apiurl") + endPointUrl, e2);
            }
            restRequestResult.setErrorCode(2);
            restRequestResult.setErrorDescription("Exception " + e2.getMessage());
            restRequestResult.setException(e2);
        }
        finally {
//            if (requestFactory != null)
//            {
//                try {
//                    requestFactory.destroy();
//                } catch (Exception e) {
//                }
//            }
        }
        return null;
    }

    private static  HttpComponentsClientHttpRequestFactory getHttpComponentsClientHttpRequestFactory() {

        if (requestFactory != null)
        {
            return requestFactory;
        }
        requestFactory = new HttpComponentsClientHttpRequestFactory();
        return requestFactory;
    }
}
