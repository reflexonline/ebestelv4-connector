/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * SaleArticleOrderGroupDto
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class SaleArticleOrderGroupDto {
  @SerializedName("saleArticleOrderGroupId")
  private Integer saleArticleOrderGroupId = null;

  @SerializedName("description")
  private String description = null;

  @SerializedName("labelNumber")
  private Integer labelNumber = null;

  @SerializedName("portName")
  private String portName = null;

  @SerializedName("orderEntryLabelNumber")
  private Integer orderEntryLabelNumber = null;

  @SerializedName("orderEntryLabelPortName")
  private String orderEntryLabelPortName = null;

  @SerializedName("packingListPrinterNumber")
  private Integer packingListPrinterNumber = null;

  @SerializedName("extraQuantityBasedLabelPrinterConnectionInfo")
  private String extraQuantityBasedLabelPrinterConnectionInfo = null;

  @SerializedName("extraQuantityBasedLabel")
  private Integer extraQuantityBasedLabel = null;

  @SerializedName("labelingMachinePrinterConnectionInfo")
  private String labelingMachinePrinterConnectionInfo = null;

  @SerializedName("labelingMachineProductionLabel")
  private Integer labelingMachineProductionLabel = null;

  @SerializedName("labelingMachineProductionAttentionLabel")
  private Integer labelingMachineProductionAttentionLabel = null;

  /**
   * Gets or Sets automaticRegistrationMethod
   */
  @JsonAdapter(AutomaticRegistrationMethodEnum.Adapter.class)
  public enum AutomaticRegistrationMethodEnum {
    NONE("None"),
    
    PIECEARTICLESONLY("PieceArticlesOnly"),
    
    WEIGHTARTICLESONLY("WeightArticlesOnly"),
    
    PIECEANDWEIGHTARTICLES("PieceAndWeightArticles");

    private String value;

    AutomaticRegistrationMethodEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static AutomaticRegistrationMethodEnum fromValue(String text) {
      for (AutomaticRegistrationMethodEnum b : AutomaticRegistrationMethodEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    public static class Adapter extends TypeAdapter<AutomaticRegistrationMethodEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final AutomaticRegistrationMethodEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public AutomaticRegistrationMethodEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return AutomaticRegistrationMethodEnum.fromValue(String.valueOf(value));
      }
    }
  }

  @SerializedName("automaticRegistrationMethod")
  private AutomaticRegistrationMethodEnum automaticRegistrationMethod = null;

  @SerializedName("packingListNumber")
  private Integer packingListNumber = null;

  @SerializedName("attentionLabelNumber")
  private Integer attentionLabelNumber = null;

  @SerializedName("attentionLabelPort")
  private String attentionLabelPort = null;

  @SerializedName("reapplyAutomaticRegistrationMethodAtPrintingPackingSlip")
  private Boolean reapplyAutomaticRegistrationMethodAtPrintingPackingSlip = null;

  @SerializedName("operations")
  private List<String> operations = null;

  public SaleArticleOrderGroupDto saleArticleOrderGroupId(Integer saleArticleOrderGroupId) {
    this.saleArticleOrderGroupId = saleArticleOrderGroupId;
    return this;
  }

   /**
   * Get saleArticleOrderGroupId
   * @return saleArticleOrderGroupId
  **/
  @ApiModelProperty(required = true, value = "")
  public Integer getSaleArticleOrderGroupId() {
    return saleArticleOrderGroupId;
  }

  public void setSaleArticleOrderGroupId(Integer saleArticleOrderGroupId) {
    this.saleArticleOrderGroupId = saleArticleOrderGroupId;
  }

  public SaleArticleOrderGroupDto description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public SaleArticleOrderGroupDto labelNumber(Integer labelNumber) {
    this.labelNumber = labelNumber;
    return this;
  }

   /**
   * Get labelNumber
   * @return labelNumber
  **/
  @ApiModelProperty(value = "")
  public Integer getLabelNumber() {
    return labelNumber;
  }

  public void setLabelNumber(Integer labelNumber) {
    this.labelNumber = labelNumber;
  }

  public SaleArticleOrderGroupDto portName(String portName) {
    this.portName = portName;
    return this;
  }

   /**
   * Get portName
   * @return portName
  **/
  @ApiModelProperty(value = "")
  public String getPortName() {
    return portName;
  }

  public void setPortName(String portName) {
    this.portName = portName;
  }

  public SaleArticleOrderGroupDto orderEntryLabelNumber(Integer orderEntryLabelNumber) {
    this.orderEntryLabelNumber = orderEntryLabelNumber;
    return this;
  }

   /**
   * Get orderEntryLabelNumber
   * @return orderEntryLabelNumber
  **/
  @ApiModelProperty(value = "")
  public Integer getOrderEntryLabelNumber() {
    return orderEntryLabelNumber;
  }

  public void setOrderEntryLabelNumber(Integer orderEntryLabelNumber) {
    this.orderEntryLabelNumber = orderEntryLabelNumber;
  }

  public SaleArticleOrderGroupDto orderEntryLabelPortName(String orderEntryLabelPortName) {
    this.orderEntryLabelPortName = orderEntryLabelPortName;
    return this;
  }

   /**
   * Get orderEntryLabelPortName
   * @return orderEntryLabelPortName
  **/
  @ApiModelProperty(value = "")
  public String getOrderEntryLabelPortName() {
    return orderEntryLabelPortName;
  }

  public void setOrderEntryLabelPortName(String orderEntryLabelPortName) {
    this.orderEntryLabelPortName = orderEntryLabelPortName;
  }

  public SaleArticleOrderGroupDto packingListPrinterNumber(Integer packingListPrinterNumber) {
    this.packingListPrinterNumber = packingListPrinterNumber;
    return this;
  }

   /**
   * Get packingListPrinterNumber
   * @return packingListPrinterNumber
  **/
  @ApiModelProperty(value = "")
  public Integer getPackingListPrinterNumber() {
    return packingListPrinterNumber;
  }

  public void setPackingListPrinterNumber(Integer packingListPrinterNumber) {
    this.packingListPrinterNumber = packingListPrinterNumber;
  }

  public SaleArticleOrderGroupDto extraQuantityBasedLabelPrinterConnectionInfo(String extraQuantityBasedLabelPrinterConnectionInfo) {
    this.extraQuantityBasedLabelPrinterConnectionInfo = extraQuantityBasedLabelPrinterConnectionInfo;
    return this;
  }

   /**
   * Get extraQuantityBasedLabelPrinterConnectionInfo
   * @return extraQuantityBasedLabelPrinterConnectionInfo
  **/
  @ApiModelProperty(value = "")
  public String getExtraQuantityBasedLabelPrinterConnectionInfo() {
    return extraQuantityBasedLabelPrinterConnectionInfo;
  }

  public void setExtraQuantityBasedLabelPrinterConnectionInfo(String extraQuantityBasedLabelPrinterConnectionInfo) {
    this.extraQuantityBasedLabelPrinterConnectionInfo = extraQuantityBasedLabelPrinterConnectionInfo;
  }

  public SaleArticleOrderGroupDto extraQuantityBasedLabel(Integer extraQuantityBasedLabel) {
    this.extraQuantityBasedLabel = extraQuantityBasedLabel;
    return this;
  }

   /**
   * Get extraQuantityBasedLabel
   * @return extraQuantityBasedLabel
  **/
  @ApiModelProperty(value = "")
  public Integer getExtraQuantityBasedLabel() {
    return extraQuantityBasedLabel;
  }

  public void setExtraQuantityBasedLabel(Integer extraQuantityBasedLabel) {
    this.extraQuantityBasedLabel = extraQuantityBasedLabel;
  }

  public SaleArticleOrderGroupDto labelingMachinePrinterConnectionInfo(String labelingMachinePrinterConnectionInfo) {
    this.labelingMachinePrinterConnectionInfo = labelingMachinePrinterConnectionInfo;
    return this;
  }

   /**
   * Get labelingMachinePrinterConnectionInfo
   * @return labelingMachinePrinterConnectionInfo
  **/
  @ApiModelProperty(value = "")
  public String getLabelingMachinePrinterConnectionInfo() {
    return labelingMachinePrinterConnectionInfo;
  }

  public void setLabelingMachinePrinterConnectionInfo(String labelingMachinePrinterConnectionInfo) {
    this.labelingMachinePrinterConnectionInfo = labelingMachinePrinterConnectionInfo;
  }

  public SaleArticleOrderGroupDto labelingMachineProductionLabel(Integer labelingMachineProductionLabel) {
    this.labelingMachineProductionLabel = labelingMachineProductionLabel;
    return this;
  }

   /**
   * Get labelingMachineProductionLabel
   * @return labelingMachineProductionLabel
  **/
  @ApiModelProperty(value = "")
  public Integer getLabelingMachineProductionLabel() {
    return labelingMachineProductionLabel;
  }

  public void setLabelingMachineProductionLabel(Integer labelingMachineProductionLabel) {
    this.labelingMachineProductionLabel = labelingMachineProductionLabel;
  }

  public SaleArticleOrderGroupDto labelingMachineProductionAttentionLabel(Integer labelingMachineProductionAttentionLabel) {
    this.labelingMachineProductionAttentionLabel = labelingMachineProductionAttentionLabel;
    return this;
  }

   /**
   * Get labelingMachineProductionAttentionLabel
   * @return labelingMachineProductionAttentionLabel
  **/
  @ApiModelProperty(value = "")
  public Integer getLabelingMachineProductionAttentionLabel() {
    return labelingMachineProductionAttentionLabel;
  }

  public void setLabelingMachineProductionAttentionLabel(Integer labelingMachineProductionAttentionLabel) {
    this.labelingMachineProductionAttentionLabel = labelingMachineProductionAttentionLabel;
  }

  public SaleArticleOrderGroupDto automaticRegistrationMethod(AutomaticRegistrationMethodEnum automaticRegistrationMethod) {
    this.automaticRegistrationMethod = automaticRegistrationMethod;
    return this;
  }

   /**
   * Get automaticRegistrationMethod
   * @return automaticRegistrationMethod
  **/
  @ApiModelProperty(value = "")
  public AutomaticRegistrationMethodEnum getAutomaticRegistrationMethod() {
    return automaticRegistrationMethod;
  }

  public void setAutomaticRegistrationMethod(AutomaticRegistrationMethodEnum automaticRegistrationMethod) {
    this.automaticRegistrationMethod = automaticRegistrationMethod;
  }

  public SaleArticleOrderGroupDto packingListNumber(Integer packingListNumber) {
    this.packingListNumber = packingListNumber;
    return this;
  }

   /**
   * Get packingListNumber
   * @return packingListNumber
  **/
  @ApiModelProperty(value = "")
  public Integer getPackingListNumber() {
    return packingListNumber;
  }

  public void setPackingListNumber(Integer packingListNumber) {
    this.packingListNumber = packingListNumber;
  }

  public SaleArticleOrderGroupDto attentionLabelNumber(Integer attentionLabelNumber) {
    this.attentionLabelNumber = attentionLabelNumber;
    return this;
  }

   /**
   * Get attentionLabelNumber
   * @return attentionLabelNumber
  **/
  @ApiModelProperty(value = "")
  public Integer getAttentionLabelNumber() {
    return attentionLabelNumber;
  }

  public void setAttentionLabelNumber(Integer attentionLabelNumber) {
    this.attentionLabelNumber = attentionLabelNumber;
  }

  public SaleArticleOrderGroupDto attentionLabelPort(String attentionLabelPort) {
    this.attentionLabelPort = attentionLabelPort;
    return this;
  }

   /**
   * Get attentionLabelPort
   * @return attentionLabelPort
  **/
  @ApiModelProperty(value = "")
  public String getAttentionLabelPort() {
    return attentionLabelPort;
  }

  public void setAttentionLabelPort(String attentionLabelPort) {
    this.attentionLabelPort = attentionLabelPort;
  }

  public SaleArticleOrderGroupDto reapplyAutomaticRegistrationMethodAtPrintingPackingSlip(Boolean reapplyAutomaticRegistrationMethodAtPrintingPackingSlip) {
    this.reapplyAutomaticRegistrationMethodAtPrintingPackingSlip = reapplyAutomaticRegistrationMethodAtPrintingPackingSlip;
    return this;
  }

   /**
   * Get reapplyAutomaticRegistrationMethodAtPrintingPackingSlip
   * @return reapplyAutomaticRegistrationMethodAtPrintingPackingSlip
  **/
  @ApiModelProperty(value = "")
  public Boolean isReapplyAutomaticRegistrationMethodAtPrintingPackingSlip() {
    return reapplyAutomaticRegistrationMethodAtPrintingPackingSlip;
  }

  public void setReapplyAutomaticRegistrationMethodAtPrintingPackingSlip(Boolean reapplyAutomaticRegistrationMethodAtPrintingPackingSlip) {
    this.reapplyAutomaticRegistrationMethodAtPrintingPackingSlip = reapplyAutomaticRegistrationMethodAtPrintingPackingSlip;
  }

   /**
   * Get operations
   * @return operations
  **/
  @ApiModelProperty(value = "")
  public List<String> getOperations() {
    return operations;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SaleArticleOrderGroupDto saleArticleOrderGroupDto = (SaleArticleOrderGroupDto) o;
    return Objects.equals(this.saleArticleOrderGroupId, saleArticleOrderGroupDto.saleArticleOrderGroupId) &&
        Objects.equals(this.description, saleArticleOrderGroupDto.description) &&
        Objects.equals(this.labelNumber, saleArticleOrderGroupDto.labelNumber) &&
        Objects.equals(this.portName, saleArticleOrderGroupDto.portName) &&
        Objects.equals(this.orderEntryLabelNumber, saleArticleOrderGroupDto.orderEntryLabelNumber) &&
        Objects.equals(this.orderEntryLabelPortName, saleArticleOrderGroupDto.orderEntryLabelPortName) &&
        Objects.equals(this.packingListPrinterNumber, saleArticleOrderGroupDto.packingListPrinterNumber) &&
        Objects.equals(this.extraQuantityBasedLabelPrinterConnectionInfo, saleArticleOrderGroupDto.extraQuantityBasedLabelPrinterConnectionInfo) &&
        Objects.equals(this.extraQuantityBasedLabel, saleArticleOrderGroupDto.extraQuantityBasedLabel) &&
        Objects.equals(this.labelingMachinePrinterConnectionInfo, saleArticleOrderGroupDto.labelingMachinePrinterConnectionInfo) &&
        Objects.equals(this.labelingMachineProductionLabel, saleArticleOrderGroupDto.labelingMachineProductionLabel) &&
        Objects.equals(this.labelingMachineProductionAttentionLabel, saleArticleOrderGroupDto.labelingMachineProductionAttentionLabel) &&
        Objects.equals(this.automaticRegistrationMethod, saleArticleOrderGroupDto.automaticRegistrationMethod) &&
        Objects.equals(this.packingListNumber, saleArticleOrderGroupDto.packingListNumber) &&
        Objects.equals(this.attentionLabelNumber, saleArticleOrderGroupDto.attentionLabelNumber) &&
        Objects.equals(this.attentionLabelPort, saleArticleOrderGroupDto.attentionLabelPort) &&
        Objects.equals(this.reapplyAutomaticRegistrationMethodAtPrintingPackingSlip, saleArticleOrderGroupDto.reapplyAutomaticRegistrationMethodAtPrintingPackingSlip) &&
        Objects.equals(this.operations, saleArticleOrderGroupDto.operations);
  }

  @Override
  public int hashCode() {
    return Objects.hash(saleArticleOrderGroupId, description, labelNumber, portName, orderEntryLabelNumber, orderEntryLabelPortName, packingListPrinterNumber, extraQuantityBasedLabelPrinterConnectionInfo, extraQuantityBasedLabel, labelingMachinePrinterConnectionInfo, labelingMachineProductionLabel, labelingMachineProductionAttentionLabel, automaticRegistrationMethod, packingListNumber, attentionLabelNumber, attentionLabelPort, reapplyAutomaticRegistrationMethodAtPrintingPackingSlip, operations);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SaleArticleOrderGroupDto {\n");
    
    sb.append("    saleArticleOrderGroupId: ").append(toIndentedString(saleArticleOrderGroupId)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    labelNumber: ").append(toIndentedString(labelNumber)).append("\n");
    sb.append("    portName: ").append(toIndentedString(portName)).append("\n");
    sb.append("    orderEntryLabelNumber: ").append(toIndentedString(orderEntryLabelNumber)).append("\n");
    sb.append("    orderEntryLabelPortName: ").append(toIndentedString(orderEntryLabelPortName)).append("\n");
    sb.append("    packingListPrinterNumber: ").append(toIndentedString(packingListPrinterNumber)).append("\n");
    sb.append("    extraQuantityBasedLabelPrinterConnectionInfo: ").append(toIndentedString(extraQuantityBasedLabelPrinterConnectionInfo)).append("\n");
    sb.append("    extraQuantityBasedLabel: ").append(toIndentedString(extraQuantityBasedLabel)).append("\n");
    sb.append("    labelingMachinePrinterConnectionInfo: ").append(toIndentedString(labelingMachinePrinterConnectionInfo)).append("\n");
    sb.append("    labelingMachineProductionLabel: ").append(toIndentedString(labelingMachineProductionLabel)).append("\n");
    sb.append("    labelingMachineProductionAttentionLabel: ").append(toIndentedString(labelingMachineProductionAttentionLabel)).append("\n");
    sb.append("    automaticRegistrationMethod: ").append(toIndentedString(automaticRegistrationMethod)).append("\n");
    sb.append("    packingListNumber: ").append(toIndentedString(packingListNumber)).append("\n");
    sb.append("    attentionLabelNumber: ").append(toIndentedString(attentionLabelNumber)).append("\n");
    sb.append("    attentionLabelPort: ").append(toIndentedString(attentionLabelPort)).append("\n");
    sb.append("    reapplyAutomaticRegistrationMethodAtPrintingPackingSlip: ").append(toIndentedString(reapplyAutomaticRegistrationMethodAtPrintingPackingSlip)).append("\n");
    sb.append("    operations: ").append(toIndentedString(operations)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

