/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * RelationDeliveryAddressDto
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class RelationDeliveryAddressDto {
  @SerializedName("number")
  private Integer number = null;

  @SerializedName("marktSegment")
  private Integer marktSegment = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("addressLine1")
  private String addressLine1 = null;

  @SerializedName("addressLine2")
  private String addressLine2 = null;

  @SerializedName("postalCode")
  private String postalCode = null;

  @SerializedName("city")
  private String city = null;

  @SerializedName("phoneNumber")
  private String phoneNumber = null;

  @SerializedName("faxNumber")
  private String faxNumber = null;

  @SerializedName("isoCountryCode")
  private String isoCountryCode = null;

  @SerializedName("country")
  private String country = null;

  @SerializedName("longName")
  private String longName = null;

  @SerializedName("operations")
  private List<String> operations = null;

  public RelationDeliveryAddressDto number(Integer number) {
    this.number = number;
    return this;
  }

   /**
   * Get number
   * @return number
  **/
  @ApiModelProperty(required = true, value = "")
  public Integer getNumber() {
    return number;
  }

  public void setNumber(Integer number) {
    this.number = number;
  }

  public RelationDeliveryAddressDto marktSegment(Integer marktSegment) {
    this.marktSegment = marktSegment;
    return this;
  }

   /**
   * Get marktSegment
   * @return marktSegment
  **/
  @ApiModelProperty(value = "")
  public Integer getMarktSegment() {
    return marktSegment;
  }

  public void setMarktSegment(Integer marktSegment) {
    this.marktSegment = marktSegment;
  }

  public RelationDeliveryAddressDto name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public RelationDeliveryAddressDto addressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
    return this;
  }

   /**
   * Get addressLine1
   * @return addressLine1
  **/
  @ApiModelProperty(value = "")
  public String getAddressLine1() {
    return addressLine1;
  }

  public void setAddressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
  }

  public RelationDeliveryAddressDto addressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
    return this;
  }

   /**
   * Get addressLine2
   * @return addressLine2
  **/
  @ApiModelProperty(value = "")
  public String getAddressLine2() {
    return addressLine2;
  }

  public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }

  public RelationDeliveryAddressDto postalCode(String postalCode) {
    this.postalCode = postalCode;
    return this;
  }

   /**
   * Get postalCode
   * @return postalCode
  **/
  @ApiModelProperty(value = "")
  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public RelationDeliveryAddressDto city(String city) {
    this.city = city;
    return this;
  }

   /**
   * Get city
   * @return city
  **/
  @ApiModelProperty(value = "")
  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public RelationDeliveryAddressDto phoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }

   /**
   * Get phoneNumber
   * @return phoneNumber
  **/
  @ApiModelProperty(value = "")
  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public RelationDeliveryAddressDto faxNumber(String faxNumber) {
    this.faxNumber = faxNumber;
    return this;
  }

   /**
   * Get faxNumber
   * @return faxNumber
  **/
  @ApiModelProperty(value = "")
  public String getFaxNumber() {
    return faxNumber;
  }

  public void setFaxNumber(String faxNumber) {
    this.faxNumber = faxNumber;
  }

  public RelationDeliveryAddressDto isoCountryCode(String isoCountryCode) {
    this.isoCountryCode = isoCountryCode;
    return this;
  }

   /**
   * Get isoCountryCode
   * @return isoCountryCode
  **/
  @ApiModelProperty(value = "")
  public String getIsoCountryCode() {
    return isoCountryCode;
  }

  public void setIsoCountryCode(String isoCountryCode) {
    this.isoCountryCode = isoCountryCode;
  }

  public RelationDeliveryAddressDto country(String country) {
    this.country = country;
    return this;
  }

   /**
   * Get country
   * @return country
  **/
  @ApiModelProperty(value = "")
  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public RelationDeliveryAddressDto longName(String longName) {
    this.longName = longName;
    return this;
  }

   /**
   * Get longName
   * @return longName
  **/
  @ApiModelProperty(value = "")
  public String getLongName() {
    return longName;
  }

  public void setLongName(String longName) {
    this.longName = longName;
  }

   /**
   * Get operations
   * @return operations
  **/
  @ApiModelProperty(value = "")
  public List<String> getOperations() {
    return operations;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RelationDeliveryAddressDto relationDeliveryAddressDto = (RelationDeliveryAddressDto) o;
    return Objects.equals(this.number, relationDeliveryAddressDto.number) &&
        Objects.equals(this.marktSegment, relationDeliveryAddressDto.marktSegment) &&
        Objects.equals(this.name, relationDeliveryAddressDto.name) &&
        Objects.equals(this.addressLine1, relationDeliveryAddressDto.addressLine1) &&
        Objects.equals(this.addressLine2, relationDeliveryAddressDto.addressLine2) &&
        Objects.equals(this.postalCode, relationDeliveryAddressDto.postalCode) &&
        Objects.equals(this.city, relationDeliveryAddressDto.city) &&
        Objects.equals(this.phoneNumber, relationDeliveryAddressDto.phoneNumber) &&
        Objects.equals(this.faxNumber, relationDeliveryAddressDto.faxNumber) &&
        Objects.equals(this.isoCountryCode, relationDeliveryAddressDto.isoCountryCode) &&
        Objects.equals(this.country, relationDeliveryAddressDto.country) &&
        Objects.equals(this.longName, relationDeliveryAddressDto.longName) &&
        Objects.equals(this.operations, relationDeliveryAddressDto.operations);
  }

  @Override
  public int hashCode() {
    return Objects.hash(number, marktSegment, name, addressLine1, addressLine2, postalCode, city, phoneNumber, faxNumber, isoCountryCode, country, longName, operations);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RelationDeliveryAddressDto {\n");
    
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    marktSegment: ").append(toIndentedString(marktSegment)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    addressLine1: ").append(toIndentedString(addressLine1)).append("\n");
    sb.append("    addressLine2: ").append(toIndentedString(addressLine2)).append("\n");
    sb.append("    postalCode: ").append(toIndentedString(postalCode)).append("\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("    phoneNumber: ").append(toIndentedString(phoneNumber)).append("\n");
    sb.append("    faxNumber: ").append(toIndentedString(faxNumber)).append("\n");
    sb.append("    isoCountryCode: ").append(toIndentedString(isoCountryCode)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("    longName: ").append(toIndentedString(longName)).append("\n");
    sb.append("    operations: ").append(toIndentedString(operations)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

