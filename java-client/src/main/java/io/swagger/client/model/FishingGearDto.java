/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * FishingGearDto
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class FishingGearDto {
  @SerializedName("fishingGearId")
  private Integer fishingGearId = null;

  @SerializedName("description")
  private String description = null;

  @SerializedName("code")
  private String code = null;

  @SerializedName("operations")
  private List<String> operations = null;

  public FishingGearDto fishingGearId(Integer fishingGearId) {
    this.fishingGearId = fishingGearId;
    return this;
  }

   /**
   * Get fishingGearId
   * @return fishingGearId
  **/
  @ApiModelProperty(required = true, value = "")
  public Integer getFishingGearId() {
    return fishingGearId;
  }

  public void setFishingGearId(Integer fishingGearId) {
    this.fishingGearId = fishingGearId;
  }

  public FishingGearDto description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public FishingGearDto code(String code) {
    this.code = code;
    return this;
  }

   /**
   * Get code
   * @return code
  **/
  @ApiModelProperty(value = "")
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

   /**
   * Get operations
   * @return operations
  **/
  @ApiModelProperty(value = "")
  public List<String> getOperations() {
    return operations;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FishingGearDto fishingGearDto = (FishingGearDto) o;
    return Objects.equals(this.fishingGearId, fishingGearDto.fishingGearId) &&
        Objects.equals(this.description, fishingGearDto.description) &&
        Objects.equals(this.code, fishingGearDto.code) &&
        Objects.equals(this.operations, fishingGearDto.operations);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fishingGearId, description, code, operations);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FishingGearDto {\n");
    
    sb.append("    fishingGearId: ").append(toIndentedString(fishingGearId)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    operations: ").append(toIndentedString(operations)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

