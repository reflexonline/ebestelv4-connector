/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * GetFilteredLabelingMachineLabelQuery
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class GetFilteredLabelingMachineLabelQuery {
  @SerializedName("etiketNumberMinValue")
  private Integer etiketNumberMinValue = null;

  @SerializedName("etiketNumberMaxValue")
  private Integer etiketNumberMaxValue = null;

  /**
   * Gets or Sets labelingMachineTypeMinValue
   */
  @JsonAdapter(LabelingMachineTypeMinValueEnum.Adapter.class)
  public enum LabelingMachineTypeMinValueEnum {
    NONE("None"),
    
    ESPERACIP("EsperaCIP"),
    
    BIZERBA("Bizerba"),
    
    BOPACK("Bopack"),
    
    ESPERAES("EsperaES"),
    
    DIGI("Digi"),
    
    BIZERBABRAIN("BizerbaBrain"),
    
    FLEX("Flex"),
    
    BIZERBAGLP("BizerbaGLP"),
    
    DELFORD("Delford"),
    
    HERBERTGEMINI("HerbertGemini"),
    
    MANUAL("Manual"),
    
    ESPERAEST("EsperaEST");

    private String value;

    LabelingMachineTypeMinValueEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static LabelingMachineTypeMinValueEnum fromValue(String text) {
      for (LabelingMachineTypeMinValueEnum b : LabelingMachineTypeMinValueEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    public static class Adapter extends TypeAdapter<LabelingMachineTypeMinValueEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final LabelingMachineTypeMinValueEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public LabelingMachineTypeMinValueEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return LabelingMachineTypeMinValueEnum.fromValue(String.valueOf(value));
      }
    }
  }

  @SerializedName("labelingMachineTypeMinValue")
  private LabelingMachineTypeMinValueEnum labelingMachineTypeMinValue = null;

  /**
   * Gets or Sets labelingMachineTypeMaxValue
   */
  @JsonAdapter(LabelingMachineTypeMaxValueEnum.Adapter.class)
  public enum LabelingMachineTypeMaxValueEnum {
    NONE("None"),
    
    ESPERACIP("EsperaCIP"),
    
    BIZERBA("Bizerba"),
    
    BOPACK("Bopack"),
    
    ESPERAES("EsperaES"),
    
    DIGI("Digi"),
    
    BIZERBABRAIN("BizerbaBrain"),
    
    FLEX("Flex"),
    
    BIZERBAGLP("BizerbaGLP"),
    
    DELFORD("Delford"),
    
    HERBERTGEMINI("HerbertGemini"),
    
    MANUAL("Manual"),
    
    ESPERAEST("EsperaEST");

    private String value;

    LabelingMachineTypeMaxValueEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static LabelingMachineTypeMaxValueEnum fromValue(String text) {
      for (LabelingMachineTypeMaxValueEnum b : LabelingMachineTypeMaxValueEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    public static class Adapter extends TypeAdapter<LabelingMachineTypeMaxValueEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final LabelingMachineTypeMaxValueEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public LabelingMachineTypeMaxValueEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return LabelingMachineTypeMaxValueEnum.fromValue(String.valueOf(value));
      }
    }
  }

  @SerializedName("labelingMachineTypeMaxValue")
  private LabelingMachineTypeMaxValueEnum labelingMachineTypeMaxValue = null;

  public GetFilteredLabelingMachineLabelQuery etiketNumberMinValue(Integer etiketNumberMinValue) {
    this.etiketNumberMinValue = etiketNumberMinValue;
    return this;
  }

   /**
   * Get etiketNumberMinValue
   * @return etiketNumberMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getEtiketNumberMinValue() {
    return etiketNumberMinValue;
  }

  public void setEtiketNumberMinValue(Integer etiketNumberMinValue) {
    this.etiketNumberMinValue = etiketNumberMinValue;
  }

  public GetFilteredLabelingMachineLabelQuery etiketNumberMaxValue(Integer etiketNumberMaxValue) {
    this.etiketNumberMaxValue = etiketNumberMaxValue;
    return this;
  }

   /**
   * Get etiketNumberMaxValue
   * @return etiketNumberMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getEtiketNumberMaxValue() {
    return etiketNumberMaxValue;
  }

  public void setEtiketNumberMaxValue(Integer etiketNumberMaxValue) {
    this.etiketNumberMaxValue = etiketNumberMaxValue;
  }

  public GetFilteredLabelingMachineLabelQuery labelingMachineTypeMinValue(LabelingMachineTypeMinValueEnum labelingMachineTypeMinValue) {
    this.labelingMachineTypeMinValue = labelingMachineTypeMinValue;
    return this;
  }

   /**
   * Get labelingMachineTypeMinValue
   * @return labelingMachineTypeMinValue
  **/
  @ApiModelProperty(value = "")
  public LabelingMachineTypeMinValueEnum getLabelingMachineTypeMinValue() {
    return labelingMachineTypeMinValue;
  }

  public void setLabelingMachineTypeMinValue(LabelingMachineTypeMinValueEnum labelingMachineTypeMinValue) {
    this.labelingMachineTypeMinValue = labelingMachineTypeMinValue;
  }

  public GetFilteredLabelingMachineLabelQuery labelingMachineTypeMaxValue(LabelingMachineTypeMaxValueEnum labelingMachineTypeMaxValue) {
    this.labelingMachineTypeMaxValue = labelingMachineTypeMaxValue;
    return this;
  }

   /**
   * Get labelingMachineTypeMaxValue
   * @return labelingMachineTypeMaxValue
  **/
  @ApiModelProperty(value = "")
  public LabelingMachineTypeMaxValueEnum getLabelingMachineTypeMaxValue() {
    return labelingMachineTypeMaxValue;
  }

  public void setLabelingMachineTypeMaxValue(LabelingMachineTypeMaxValueEnum labelingMachineTypeMaxValue) {
    this.labelingMachineTypeMaxValue = labelingMachineTypeMaxValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetFilteredLabelingMachineLabelQuery getFilteredLabelingMachineLabelQuery = (GetFilteredLabelingMachineLabelQuery) o;
    return Objects.equals(this.etiketNumberMinValue, getFilteredLabelingMachineLabelQuery.etiketNumberMinValue) &&
        Objects.equals(this.etiketNumberMaxValue, getFilteredLabelingMachineLabelQuery.etiketNumberMaxValue) &&
        Objects.equals(this.labelingMachineTypeMinValue, getFilteredLabelingMachineLabelQuery.labelingMachineTypeMinValue) &&
        Objects.equals(this.labelingMachineTypeMaxValue, getFilteredLabelingMachineLabelQuery.labelingMachineTypeMaxValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(etiketNumberMinValue, etiketNumberMaxValue, labelingMachineTypeMinValue, labelingMachineTypeMaxValue);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetFilteredLabelingMachineLabelQuery {\n");
    
    sb.append("    etiketNumberMinValue: ").append(toIndentedString(etiketNumberMinValue)).append("\n");
    sb.append("    etiketNumberMaxValue: ").append(toIndentedString(etiketNumberMaxValue)).append("\n");
    sb.append("    labelingMachineTypeMinValue: ").append(toIndentedString(labelingMachineTypeMinValue)).append("\n");
    sb.append("    labelingMachineTypeMaxValue: ").append(toIndentedString(labelingMachineTypeMaxValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

