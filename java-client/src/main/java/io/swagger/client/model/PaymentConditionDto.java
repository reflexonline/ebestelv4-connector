/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * PaymentConditionDto
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class PaymentConditionDto {
  @SerializedName("paymentConditionId")
  private Integer paymentConditionId = null;

  @SerializedName("description")
  private String description = null;

  @SerializedName("hasVatInvoice")
  private Boolean hasVatInvoice = null;

  @SerializedName("autoCollection")
  private Boolean autoCollection = null;

  @SerializedName("paymentTerms")
  private Integer paymentTerms = null;

  @SerializedName("vatExportCode")
  private Integer vatExportCode = null;

  @SerializedName("paymentExportCode")
  private String paymentExportCode = null;

  @SerializedName("directInvoice")
  private Integer directInvoice = null;

  @SerializedName("invoiceLayoutNumber")
  private Integer invoiceLayoutNumber = null;

  @SerializedName("printSaleInvoice")
  private Boolean printSaleInvoice = null;

  @SerializedName("paymentDiscountDays")
  private Integer paymentDiscountDays = null;

  @SerializedName("paymentDiscountPercentage")
  private Double paymentDiscountPercentage = null;

  @SerializedName("saleInvoiceStatus")
  private Integer saleInvoiceStatus = null;

  @SerializedName("printCMR")
  private Boolean printCMR = null;

  @SerializedName("intrastatReportMethod")
  private Integer intrastatReportMethod = null;

  @SerializedName("paymentTermsCalculation")
  private Integer paymentTermsCalculation = null;

  @SerializedName("vatScenario")
  private Integer vatScenario = null;

  @SerializedName("blockPaymentOnAccount")
  private Boolean blockPaymentOnAccount = null;

  @SerializedName("operations")
  private List<String> operations = null;

  public PaymentConditionDto paymentConditionId(Integer paymentConditionId) {
    this.paymentConditionId = paymentConditionId;
    return this;
  }

   /**
   * Get paymentConditionId
   * @return paymentConditionId
  **/
  @ApiModelProperty(required = true, value = "")
  public Integer getPaymentConditionId() {
    return paymentConditionId;
  }

  public void setPaymentConditionId(Integer paymentConditionId) {
    this.paymentConditionId = paymentConditionId;
  }

  public PaymentConditionDto description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public PaymentConditionDto hasVatInvoice(Boolean hasVatInvoice) {
    this.hasVatInvoice = hasVatInvoice;
    return this;
  }

   /**
   * Get hasVatInvoice
   * @return hasVatInvoice
  **/
  @ApiModelProperty(value = "")
  public Boolean isHasVatInvoice() {
    return hasVatInvoice;
  }

  public void setHasVatInvoice(Boolean hasVatInvoice) {
    this.hasVatInvoice = hasVatInvoice;
  }

  public PaymentConditionDto autoCollection(Boolean autoCollection) {
    this.autoCollection = autoCollection;
    return this;
  }

   /**
   * Get autoCollection
   * @return autoCollection
  **/
  @ApiModelProperty(value = "")
  public Boolean isAutoCollection() {
    return autoCollection;
  }

  public void setAutoCollection(Boolean autoCollection) {
    this.autoCollection = autoCollection;
  }

  public PaymentConditionDto paymentTerms(Integer paymentTerms) {
    this.paymentTerms = paymentTerms;
    return this;
  }

   /**
   * Get paymentTerms
   * @return paymentTerms
  **/
  @ApiModelProperty(value = "")
  public Integer getPaymentTerms() {
    return paymentTerms;
  }

  public void setPaymentTerms(Integer paymentTerms) {
    this.paymentTerms = paymentTerms;
  }

  public PaymentConditionDto vatExportCode(Integer vatExportCode) {
    this.vatExportCode = vatExportCode;
    return this;
  }

   /**
   * Get vatExportCode
   * @return vatExportCode
  **/
  @ApiModelProperty(value = "")
  public Integer getVatExportCode() {
    return vatExportCode;
  }

  public void setVatExportCode(Integer vatExportCode) {
    this.vatExportCode = vatExportCode;
  }

  public PaymentConditionDto paymentExportCode(String paymentExportCode) {
    this.paymentExportCode = paymentExportCode;
    return this;
  }

   /**
   * Get paymentExportCode
   * @return paymentExportCode
  **/
  @ApiModelProperty(value = "")
  public String getPaymentExportCode() {
    return paymentExportCode;
  }

  public void setPaymentExportCode(String paymentExportCode) {
    this.paymentExportCode = paymentExportCode;
  }

  public PaymentConditionDto directInvoice(Integer directInvoice) {
    this.directInvoice = directInvoice;
    return this;
  }

   /**
   * Get directInvoice
   * @return directInvoice
  **/
  @ApiModelProperty(value = "")
  public Integer getDirectInvoice() {
    return directInvoice;
  }

  public void setDirectInvoice(Integer directInvoice) {
    this.directInvoice = directInvoice;
  }

  public PaymentConditionDto invoiceLayoutNumber(Integer invoiceLayoutNumber) {
    this.invoiceLayoutNumber = invoiceLayoutNumber;
    return this;
  }

   /**
   * Get invoiceLayoutNumber
   * @return invoiceLayoutNumber
  **/
  @ApiModelProperty(value = "")
  public Integer getInvoiceLayoutNumber() {
    return invoiceLayoutNumber;
  }

  public void setInvoiceLayoutNumber(Integer invoiceLayoutNumber) {
    this.invoiceLayoutNumber = invoiceLayoutNumber;
  }

  public PaymentConditionDto printSaleInvoice(Boolean printSaleInvoice) {
    this.printSaleInvoice = printSaleInvoice;
    return this;
  }

   /**
   * Get printSaleInvoice
   * @return printSaleInvoice
  **/
  @ApiModelProperty(value = "")
  public Boolean isPrintSaleInvoice() {
    return printSaleInvoice;
  }

  public void setPrintSaleInvoice(Boolean printSaleInvoice) {
    this.printSaleInvoice = printSaleInvoice;
  }

  public PaymentConditionDto paymentDiscountDays(Integer paymentDiscountDays) {
    this.paymentDiscountDays = paymentDiscountDays;
    return this;
  }

   /**
   * Get paymentDiscountDays
   * @return paymentDiscountDays
  **/
  @ApiModelProperty(value = "")
  public Integer getPaymentDiscountDays() {
    return paymentDiscountDays;
  }

  public void setPaymentDiscountDays(Integer paymentDiscountDays) {
    this.paymentDiscountDays = paymentDiscountDays;
  }

  public PaymentConditionDto paymentDiscountPercentage(Double paymentDiscountPercentage) {
    this.paymentDiscountPercentage = paymentDiscountPercentage;
    return this;
  }

   /**
   * Get paymentDiscountPercentage
   * @return paymentDiscountPercentage
  **/
  @ApiModelProperty(value = "")
  public Double getPaymentDiscountPercentage() {
    return paymentDiscountPercentage;
  }

  public void setPaymentDiscountPercentage(Double paymentDiscountPercentage) {
    this.paymentDiscountPercentage = paymentDiscountPercentage;
  }

  public PaymentConditionDto saleInvoiceStatus(Integer saleInvoiceStatus) {
    this.saleInvoiceStatus = saleInvoiceStatus;
    return this;
  }

   /**
   * Get saleInvoiceStatus
   * @return saleInvoiceStatus
  **/
  @ApiModelProperty(value = "")
  public Integer getSaleInvoiceStatus() {
    return saleInvoiceStatus;
  }

  public void setSaleInvoiceStatus(Integer saleInvoiceStatus) {
    this.saleInvoiceStatus = saleInvoiceStatus;
  }

  public PaymentConditionDto printCMR(Boolean printCMR) {
    this.printCMR = printCMR;
    return this;
  }

   /**
   * Get printCMR
   * @return printCMR
  **/
  @ApiModelProperty(value = "")
  public Boolean isPrintCMR() {
    return printCMR;
  }

  public void setPrintCMR(Boolean printCMR) {
    this.printCMR = printCMR;
  }

  public PaymentConditionDto intrastatReportMethod(Integer intrastatReportMethod) {
    this.intrastatReportMethod = intrastatReportMethod;
    return this;
  }

   /**
   * Get intrastatReportMethod
   * @return intrastatReportMethod
  **/
  @ApiModelProperty(value = "")
  public Integer getIntrastatReportMethod() {
    return intrastatReportMethod;
  }

  public void setIntrastatReportMethod(Integer intrastatReportMethod) {
    this.intrastatReportMethod = intrastatReportMethod;
  }

  public PaymentConditionDto paymentTermsCalculation(Integer paymentTermsCalculation) {
    this.paymentTermsCalculation = paymentTermsCalculation;
    return this;
  }

   /**
   * Get paymentTermsCalculation
   * @return paymentTermsCalculation
  **/
  @ApiModelProperty(value = "")
  public Integer getPaymentTermsCalculation() {
    return paymentTermsCalculation;
  }

  public void setPaymentTermsCalculation(Integer paymentTermsCalculation) {
    this.paymentTermsCalculation = paymentTermsCalculation;
  }

  public PaymentConditionDto vatScenario(Integer vatScenario) {
    this.vatScenario = vatScenario;
    return this;
  }

   /**
   * Get vatScenario
   * @return vatScenario
  **/
  @ApiModelProperty(value = "")
  public Integer getVatScenario() {
    return vatScenario;
  }

  public void setVatScenario(Integer vatScenario) {
    this.vatScenario = vatScenario;
  }

  public PaymentConditionDto blockPaymentOnAccount(Boolean blockPaymentOnAccount) {
    this.blockPaymentOnAccount = blockPaymentOnAccount;
    return this;
  }

   /**
   * Get blockPaymentOnAccount
   * @return blockPaymentOnAccount
  **/
  @ApiModelProperty(value = "")
  public Boolean isBlockPaymentOnAccount() {
    return blockPaymentOnAccount;
  }

  public void setBlockPaymentOnAccount(Boolean blockPaymentOnAccount) {
    this.blockPaymentOnAccount = blockPaymentOnAccount;
  }

   /**
   * Get operations
   * @return operations
  **/
  @ApiModelProperty(value = "")
  public List<String> getOperations() {
    return operations;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentConditionDto paymentConditionDto = (PaymentConditionDto) o;
    return Objects.equals(this.paymentConditionId, paymentConditionDto.paymentConditionId) &&
        Objects.equals(this.description, paymentConditionDto.description) &&
        Objects.equals(this.hasVatInvoice, paymentConditionDto.hasVatInvoice) &&
        Objects.equals(this.autoCollection, paymentConditionDto.autoCollection) &&
        Objects.equals(this.paymentTerms, paymentConditionDto.paymentTerms) &&
        Objects.equals(this.vatExportCode, paymentConditionDto.vatExportCode) &&
        Objects.equals(this.paymentExportCode, paymentConditionDto.paymentExportCode) &&
        Objects.equals(this.directInvoice, paymentConditionDto.directInvoice) &&
        Objects.equals(this.invoiceLayoutNumber, paymentConditionDto.invoiceLayoutNumber) &&
        Objects.equals(this.printSaleInvoice, paymentConditionDto.printSaleInvoice) &&
        Objects.equals(this.paymentDiscountDays, paymentConditionDto.paymentDiscountDays) &&
        Objects.equals(this.paymentDiscountPercentage, paymentConditionDto.paymentDiscountPercentage) &&
        Objects.equals(this.saleInvoiceStatus, paymentConditionDto.saleInvoiceStatus) &&
        Objects.equals(this.printCMR, paymentConditionDto.printCMR) &&
        Objects.equals(this.intrastatReportMethod, paymentConditionDto.intrastatReportMethod) &&
        Objects.equals(this.paymentTermsCalculation, paymentConditionDto.paymentTermsCalculation) &&
        Objects.equals(this.vatScenario, paymentConditionDto.vatScenario) &&
        Objects.equals(this.blockPaymentOnAccount, paymentConditionDto.blockPaymentOnAccount) &&
        Objects.equals(this.operations, paymentConditionDto.operations);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentConditionId, description, hasVatInvoice, autoCollection, paymentTerms, vatExportCode, paymentExportCode, directInvoice, invoiceLayoutNumber, printSaleInvoice, paymentDiscountDays, paymentDiscountPercentage, saleInvoiceStatus, printCMR, intrastatReportMethod, paymentTermsCalculation, vatScenario, blockPaymentOnAccount, operations);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentConditionDto {\n");
    
    sb.append("    paymentConditionId: ").append(toIndentedString(paymentConditionId)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    hasVatInvoice: ").append(toIndentedString(hasVatInvoice)).append("\n");
    sb.append("    autoCollection: ").append(toIndentedString(autoCollection)).append("\n");
    sb.append("    paymentTerms: ").append(toIndentedString(paymentTerms)).append("\n");
    sb.append("    vatExportCode: ").append(toIndentedString(vatExportCode)).append("\n");
    sb.append("    paymentExportCode: ").append(toIndentedString(paymentExportCode)).append("\n");
    sb.append("    directInvoice: ").append(toIndentedString(directInvoice)).append("\n");
    sb.append("    invoiceLayoutNumber: ").append(toIndentedString(invoiceLayoutNumber)).append("\n");
    sb.append("    printSaleInvoice: ").append(toIndentedString(printSaleInvoice)).append("\n");
    sb.append("    paymentDiscountDays: ").append(toIndentedString(paymentDiscountDays)).append("\n");
    sb.append("    paymentDiscountPercentage: ").append(toIndentedString(paymentDiscountPercentage)).append("\n");
    sb.append("    saleInvoiceStatus: ").append(toIndentedString(saleInvoiceStatus)).append("\n");
    sb.append("    printCMR: ").append(toIndentedString(printCMR)).append("\n");
    sb.append("    intrastatReportMethod: ").append(toIndentedString(intrastatReportMethod)).append("\n");
    sb.append("    paymentTermsCalculation: ").append(toIndentedString(paymentTermsCalculation)).append("\n");
    sb.append("    vatScenario: ").append(toIndentedString(vatScenario)).append("\n");
    sb.append("    blockPaymentOnAccount: ").append(toIndentedString(blockPaymentOnAccount)).append("\n");
    sb.append("    operations: ").append(toIndentedString(operations)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

