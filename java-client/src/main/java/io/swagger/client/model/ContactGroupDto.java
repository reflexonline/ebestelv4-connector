/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * ContactGroupDto
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class ContactGroupDto {
  @SerializedName("contactGroupId")
  private Integer contactGroupId = null;

  @SerializedName("userKey")
  private String userKey = null;

  @SerializedName("description")
  private String description = null;

  @SerializedName("operations")
  private List<String> operations = null;

  public ContactGroupDto contactGroupId(Integer contactGroupId) {
    this.contactGroupId = contactGroupId;
    return this;
  }

   /**
   * Get contactGroupId
   * @return contactGroupId
  **/
  @ApiModelProperty(required = true, value = "")
  public Integer getContactGroupId() {
    return contactGroupId;
  }

  public void setContactGroupId(Integer contactGroupId) {
    this.contactGroupId = contactGroupId;
  }

  public ContactGroupDto userKey(String userKey) {
    this.userKey = userKey;
    return this;
  }

   /**
   * Get userKey
   * @return userKey
  **/
  @ApiModelProperty(value = "")
  public String getUserKey() {
    return userKey;
  }

  public void setUserKey(String userKey) {
    this.userKey = userKey;
  }

  public ContactGroupDto description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

   /**
   * Get operations
   * @return operations
  **/
  @ApiModelProperty(value = "")
  public List<String> getOperations() {
    return operations;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContactGroupDto contactGroupDto = (ContactGroupDto) o;
    return Objects.equals(this.contactGroupId, contactGroupDto.contactGroupId) &&
        Objects.equals(this.userKey, contactGroupDto.userKey) &&
        Objects.equals(this.description, contactGroupDto.description) &&
        Objects.equals(this.operations, contactGroupDto.operations);
  }

  @Override
  public int hashCode() {
    return Objects.hash(contactGroupId, userKey, description, operations);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ContactGroupDto {\n");
    
    sb.append("    contactGroupId: ").append(toIndentedString(contactGroupId)).append("\n");
    sb.append("    userKey: ").append(toIndentedString(userKey)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    operations: ").append(toIndentedString(operations)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

