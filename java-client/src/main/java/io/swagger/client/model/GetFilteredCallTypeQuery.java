/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * GetFilteredCallTypeQuery
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class GetFilteredCallTypeQuery {
  @SerializedName("numberMinValue")
  private Integer numberMinValue = null;

  @SerializedName("numberMaxValue")
  private Integer numberMaxValue = null;

  @SerializedName("zoekArgMinValue")
  private String zoekArgMinValue = null;

  @SerializedName("zoekArgMaxValue")
  private String zoekArgMaxValue = null;

  public GetFilteredCallTypeQuery numberMinValue(Integer numberMinValue) {
    this.numberMinValue = numberMinValue;
    return this;
  }

   /**
   * Get numberMinValue
   * @return numberMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getNumberMinValue() {
    return numberMinValue;
  }

  public void setNumberMinValue(Integer numberMinValue) {
    this.numberMinValue = numberMinValue;
  }

  public GetFilteredCallTypeQuery numberMaxValue(Integer numberMaxValue) {
    this.numberMaxValue = numberMaxValue;
    return this;
  }

   /**
   * Get numberMaxValue
   * @return numberMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getNumberMaxValue() {
    return numberMaxValue;
  }

  public void setNumberMaxValue(Integer numberMaxValue) {
    this.numberMaxValue = numberMaxValue;
  }

  public GetFilteredCallTypeQuery zoekArgMinValue(String zoekArgMinValue) {
    this.zoekArgMinValue = zoekArgMinValue;
    return this;
  }

   /**
   * Get zoekArgMinValue
   * @return zoekArgMinValue
  **/
  @ApiModelProperty(value = "")
  public String getZoekArgMinValue() {
    return zoekArgMinValue;
  }

  public void setZoekArgMinValue(String zoekArgMinValue) {
    this.zoekArgMinValue = zoekArgMinValue;
  }

  public GetFilteredCallTypeQuery zoekArgMaxValue(String zoekArgMaxValue) {
    this.zoekArgMaxValue = zoekArgMaxValue;
    return this;
  }

   /**
   * Get zoekArgMaxValue
   * @return zoekArgMaxValue
  **/
  @ApiModelProperty(value = "")
  public String getZoekArgMaxValue() {
    return zoekArgMaxValue;
  }

  public void setZoekArgMaxValue(String zoekArgMaxValue) {
    this.zoekArgMaxValue = zoekArgMaxValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetFilteredCallTypeQuery getFilteredCallTypeQuery = (GetFilteredCallTypeQuery) o;
    return Objects.equals(this.numberMinValue, getFilteredCallTypeQuery.numberMinValue) &&
        Objects.equals(this.numberMaxValue, getFilteredCallTypeQuery.numberMaxValue) &&
        Objects.equals(this.zoekArgMinValue, getFilteredCallTypeQuery.zoekArgMinValue) &&
        Objects.equals(this.zoekArgMaxValue, getFilteredCallTypeQuery.zoekArgMaxValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(numberMinValue, numberMaxValue, zoekArgMinValue, zoekArgMaxValue);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetFilteredCallTypeQuery {\n");
    
    sb.append("    numberMinValue: ").append(toIndentedString(numberMinValue)).append("\n");
    sb.append("    numberMaxValue: ").append(toIndentedString(numberMaxValue)).append("\n");
    sb.append("    zoekArgMinValue: ").append(toIndentedString(zoekArgMinValue)).append("\n");
    sb.append("    zoekArgMaxValue: ").append(toIndentedString(zoekArgMaxValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

