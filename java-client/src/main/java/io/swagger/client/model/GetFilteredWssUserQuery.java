/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * GetFilteredWssUserQuery
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class GetFilteredWssUserQuery {
  @SerializedName("numberMinValue")
  private Integer numberMinValue = null;

  @SerializedName("numberMaxValue")
  private Integer numberMaxValue = null;

  @SerializedName("descriptionMinValue")
  private String descriptionMinValue = null;

  @SerializedName("descriptionMaxValue")
  private String descriptionMaxValue = null;

  @SerializedName("department1MinValue")
  private Integer department1MinValue = null;

  @SerializedName("department1MaxValue")
  private Integer department1MaxValue = null;

  @SerializedName("department2MinValue")
  private Integer department2MinValue = null;

  @SerializedName("department2MaxValue")
  private Integer department2MaxValue = null;

  @SerializedName("department3MinValue")
  private Integer department3MinValue = null;

  @SerializedName("department3MaxValue")
  private Integer department3MaxValue = null;

  @SerializedName("department4MinValue")
  private Integer department4MinValue = null;

  @SerializedName("department4MaxValue")
  private Integer department4MaxValue = null;

  @SerializedName("department5MinValue")
  private Integer department5MinValue = null;

  @SerializedName("department5MaxValue")
  private Integer department5MaxValue = null;

  @SerializedName("department6MinValue")
  private Integer department6MinValue = null;

  @SerializedName("department6MaxValue")
  private Integer department6MaxValue = null;

  @SerializedName("previousDepartmentMinValue")
  private Integer previousDepartmentMinValue = null;

  @SerializedName("previousDepartmentMaxValue")
  private Integer previousDepartmentMaxValue = null;

  @SerializedName("multiBranchIdMinValue")
  private Integer multiBranchIdMinValue = null;

  @SerializedName("multiBranchIdMaxValue")
  private Integer multiBranchIdMaxValue = null;

  public GetFilteredWssUserQuery numberMinValue(Integer numberMinValue) {
    this.numberMinValue = numberMinValue;
    return this;
  }

   /**
   * Get numberMinValue
   * @return numberMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getNumberMinValue() {
    return numberMinValue;
  }

  public void setNumberMinValue(Integer numberMinValue) {
    this.numberMinValue = numberMinValue;
  }

  public GetFilteredWssUserQuery numberMaxValue(Integer numberMaxValue) {
    this.numberMaxValue = numberMaxValue;
    return this;
  }

   /**
   * Get numberMaxValue
   * @return numberMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getNumberMaxValue() {
    return numberMaxValue;
  }

  public void setNumberMaxValue(Integer numberMaxValue) {
    this.numberMaxValue = numberMaxValue;
  }

  public GetFilteredWssUserQuery descriptionMinValue(String descriptionMinValue) {
    this.descriptionMinValue = descriptionMinValue;
    return this;
  }

   /**
   * Get descriptionMinValue
   * @return descriptionMinValue
  **/
  @ApiModelProperty(value = "")
  public String getDescriptionMinValue() {
    return descriptionMinValue;
  }

  public void setDescriptionMinValue(String descriptionMinValue) {
    this.descriptionMinValue = descriptionMinValue;
  }

  public GetFilteredWssUserQuery descriptionMaxValue(String descriptionMaxValue) {
    this.descriptionMaxValue = descriptionMaxValue;
    return this;
  }

   /**
   * Get descriptionMaxValue
   * @return descriptionMaxValue
  **/
  @ApiModelProperty(value = "")
  public String getDescriptionMaxValue() {
    return descriptionMaxValue;
  }

  public void setDescriptionMaxValue(String descriptionMaxValue) {
    this.descriptionMaxValue = descriptionMaxValue;
  }

  public GetFilteredWssUserQuery department1MinValue(Integer department1MinValue) {
    this.department1MinValue = department1MinValue;
    return this;
  }

   /**
   * Get department1MinValue
   * @return department1MinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getDepartment1MinValue() {
    return department1MinValue;
  }

  public void setDepartment1MinValue(Integer department1MinValue) {
    this.department1MinValue = department1MinValue;
  }

  public GetFilteredWssUserQuery department1MaxValue(Integer department1MaxValue) {
    this.department1MaxValue = department1MaxValue;
    return this;
  }

   /**
   * Get department1MaxValue
   * @return department1MaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getDepartment1MaxValue() {
    return department1MaxValue;
  }

  public void setDepartment1MaxValue(Integer department1MaxValue) {
    this.department1MaxValue = department1MaxValue;
  }

  public GetFilteredWssUserQuery department2MinValue(Integer department2MinValue) {
    this.department2MinValue = department2MinValue;
    return this;
  }

   /**
   * Get department2MinValue
   * @return department2MinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getDepartment2MinValue() {
    return department2MinValue;
  }

  public void setDepartment2MinValue(Integer department2MinValue) {
    this.department2MinValue = department2MinValue;
  }

  public GetFilteredWssUserQuery department2MaxValue(Integer department2MaxValue) {
    this.department2MaxValue = department2MaxValue;
    return this;
  }

   /**
   * Get department2MaxValue
   * @return department2MaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getDepartment2MaxValue() {
    return department2MaxValue;
  }

  public void setDepartment2MaxValue(Integer department2MaxValue) {
    this.department2MaxValue = department2MaxValue;
  }

  public GetFilteredWssUserQuery department3MinValue(Integer department3MinValue) {
    this.department3MinValue = department3MinValue;
    return this;
  }

   /**
   * Get department3MinValue
   * @return department3MinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getDepartment3MinValue() {
    return department3MinValue;
  }

  public void setDepartment3MinValue(Integer department3MinValue) {
    this.department3MinValue = department3MinValue;
  }

  public GetFilteredWssUserQuery department3MaxValue(Integer department3MaxValue) {
    this.department3MaxValue = department3MaxValue;
    return this;
  }

   /**
   * Get department3MaxValue
   * @return department3MaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getDepartment3MaxValue() {
    return department3MaxValue;
  }

  public void setDepartment3MaxValue(Integer department3MaxValue) {
    this.department3MaxValue = department3MaxValue;
  }

  public GetFilteredWssUserQuery department4MinValue(Integer department4MinValue) {
    this.department4MinValue = department4MinValue;
    return this;
  }

   /**
   * Get department4MinValue
   * @return department4MinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getDepartment4MinValue() {
    return department4MinValue;
  }

  public void setDepartment4MinValue(Integer department4MinValue) {
    this.department4MinValue = department4MinValue;
  }

  public GetFilteredWssUserQuery department4MaxValue(Integer department4MaxValue) {
    this.department4MaxValue = department4MaxValue;
    return this;
  }

   /**
   * Get department4MaxValue
   * @return department4MaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getDepartment4MaxValue() {
    return department4MaxValue;
  }

  public void setDepartment4MaxValue(Integer department4MaxValue) {
    this.department4MaxValue = department4MaxValue;
  }

  public GetFilteredWssUserQuery department5MinValue(Integer department5MinValue) {
    this.department5MinValue = department5MinValue;
    return this;
  }

   /**
   * Get department5MinValue
   * @return department5MinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getDepartment5MinValue() {
    return department5MinValue;
  }

  public void setDepartment5MinValue(Integer department5MinValue) {
    this.department5MinValue = department5MinValue;
  }

  public GetFilteredWssUserQuery department5MaxValue(Integer department5MaxValue) {
    this.department5MaxValue = department5MaxValue;
    return this;
  }

   /**
   * Get department5MaxValue
   * @return department5MaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getDepartment5MaxValue() {
    return department5MaxValue;
  }

  public void setDepartment5MaxValue(Integer department5MaxValue) {
    this.department5MaxValue = department5MaxValue;
  }

  public GetFilteredWssUserQuery department6MinValue(Integer department6MinValue) {
    this.department6MinValue = department6MinValue;
    return this;
  }

   /**
   * Get department6MinValue
   * @return department6MinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getDepartment6MinValue() {
    return department6MinValue;
  }

  public void setDepartment6MinValue(Integer department6MinValue) {
    this.department6MinValue = department6MinValue;
  }

  public GetFilteredWssUserQuery department6MaxValue(Integer department6MaxValue) {
    this.department6MaxValue = department6MaxValue;
    return this;
  }

   /**
   * Get department6MaxValue
   * @return department6MaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getDepartment6MaxValue() {
    return department6MaxValue;
  }

  public void setDepartment6MaxValue(Integer department6MaxValue) {
    this.department6MaxValue = department6MaxValue;
  }

  public GetFilteredWssUserQuery previousDepartmentMinValue(Integer previousDepartmentMinValue) {
    this.previousDepartmentMinValue = previousDepartmentMinValue;
    return this;
  }

   /**
   * Get previousDepartmentMinValue
   * @return previousDepartmentMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getPreviousDepartmentMinValue() {
    return previousDepartmentMinValue;
  }

  public void setPreviousDepartmentMinValue(Integer previousDepartmentMinValue) {
    this.previousDepartmentMinValue = previousDepartmentMinValue;
  }

  public GetFilteredWssUserQuery previousDepartmentMaxValue(Integer previousDepartmentMaxValue) {
    this.previousDepartmentMaxValue = previousDepartmentMaxValue;
    return this;
  }

   /**
   * Get previousDepartmentMaxValue
   * @return previousDepartmentMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getPreviousDepartmentMaxValue() {
    return previousDepartmentMaxValue;
  }

  public void setPreviousDepartmentMaxValue(Integer previousDepartmentMaxValue) {
    this.previousDepartmentMaxValue = previousDepartmentMaxValue;
  }

  public GetFilteredWssUserQuery multiBranchIdMinValue(Integer multiBranchIdMinValue) {
    this.multiBranchIdMinValue = multiBranchIdMinValue;
    return this;
  }

   /**
   * Get multiBranchIdMinValue
   * @return multiBranchIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getMultiBranchIdMinValue() {
    return multiBranchIdMinValue;
  }

  public void setMultiBranchIdMinValue(Integer multiBranchIdMinValue) {
    this.multiBranchIdMinValue = multiBranchIdMinValue;
  }

  public GetFilteredWssUserQuery multiBranchIdMaxValue(Integer multiBranchIdMaxValue) {
    this.multiBranchIdMaxValue = multiBranchIdMaxValue;
    return this;
  }

   /**
   * Get multiBranchIdMaxValue
   * @return multiBranchIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getMultiBranchIdMaxValue() {
    return multiBranchIdMaxValue;
  }

  public void setMultiBranchIdMaxValue(Integer multiBranchIdMaxValue) {
    this.multiBranchIdMaxValue = multiBranchIdMaxValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetFilteredWssUserQuery getFilteredWssUserQuery = (GetFilteredWssUserQuery) o;
    return Objects.equals(this.numberMinValue, getFilteredWssUserQuery.numberMinValue) &&
        Objects.equals(this.numberMaxValue, getFilteredWssUserQuery.numberMaxValue) &&
        Objects.equals(this.descriptionMinValue, getFilteredWssUserQuery.descriptionMinValue) &&
        Objects.equals(this.descriptionMaxValue, getFilteredWssUserQuery.descriptionMaxValue) &&
        Objects.equals(this.department1MinValue, getFilteredWssUserQuery.department1MinValue) &&
        Objects.equals(this.department1MaxValue, getFilteredWssUserQuery.department1MaxValue) &&
        Objects.equals(this.department2MinValue, getFilteredWssUserQuery.department2MinValue) &&
        Objects.equals(this.department2MaxValue, getFilteredWssUserQuery.department2MaxValue) &&
        Objects.equals(this.department3MinValue, getFilteredWssUserQuery.department3MinValue) &&
        Objects.equals(this.department3MaxValue, getFilteredWssUserQuery.department3MaxValue) &&
        Objects.equals(this.department4MinValue, getFilteredWssUserQuery.department4MinValue) &&
        Objects.equals(this.department4MaxValue, getFilteredWssUserQuery.department4MaxValue) &&
        Objects.equals(this.department5MinValue, getFilteredWssUserQuery.department5MinValue) &&
        Objects.equals(this.department5MaxValue, getFilteredWssUserQuery.department5MaxValue) &&
        Objects.equals(this.department6MinValue, getFilteredWssUserQuery.department6MinValue) &&
        Objects.equals(this.department6MaxValue, getFilteredWssUserQuery.department6MaxValue) &&
        Objects.equals(this.previousDepartmentMinValue, getFilteredWssUserQuery.previousDepartmentMinValue) &&
        Objects.equals(this.previousDepartmentMaxValue, getFilteredWssUserQuery.previousDepartmentMaxValue) &&
        Objects.equals(this.multiBranchIdMinValue, getFilteredWssUserQuery.multiBranchIdMinValue) &&
        Objects.equals(this.multiBranchIdMaxValue, getFilteredWssUserQuery.multiBranchIdMaxValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(numberMinValue, numberMaxValue, descriptionMinValue, descriptionMaxValue, department1MinValue, department1MaxValue, department2MinValue, department2MaxValue, department3MinValue, department3MaxValue, department4MinValue, department4MaxValue, department5MinValue, department5MaxValue, department6MinValue, department6MaxValue, previousDepartmentMinValue, previousDepartmentMaxValue, multiBranchIdMinValue, multiBranchIdMaxValue);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetFilteredWssUserQuery {\n");
    
    sb.append("    numberMinValue: ").append(toIndentedString(numberMinValue)).append("\n");
    sb.append("    numberMaxValue: ").append(toIndentedString(numberMaxValue)).append("\n");
    sb.append("    descriptionMinValue: ").append(toIndentedString(descriptionMinValue)).append("\n");
    sb.append("    descriptionMaxValue: ").append(toIndentedString(descriptionMaxValue)).append("\n");
    sb.append("    department1MinValue: ").append(toIndentedString(department1MinValue)).append("\n");
    sb.append("    department1MaxValue: ").append(toIndentedString(department1MaxValue)).append("\n");
    sb.append("    department2MinValue: ").append(toIndentedString(department2MinValue)).append("\n");
    sb.append("    department2MaxValue: ").append(toIndentedString(department2MaxValue)).append("\n");
    sb.append("    department3MinValue: ").append(toIndentedString(department3MinValue)).append("\n");
    sb.append("    department3MaxValue: ").append(toIndentedString(department3MaxValue)).append("\n");
    sb.append("    department4MinValue: ").append(toIndentedString(department4MinValue)).append("\n");
    sb.append("    department4MaxValue: ").append(toIndentedString(department4MaxValue)).append("\n");
    sb.append("    department5MinValue: ").append(toIndentedString(department5MinValue)).append("\n");
    sb.append("    department5MaxValue: ").append(toIndentedString(department5MaxValue)).append("\n");
    sb.append("    department6MinValue: ").append(toIndentedString(department6MinValue)).append("\n");
    sb.append("    department6MaxValue: ").append(toIndentedString(department6MaxValue)).append("\n");
    sb.append("    previousDepartmentMinValue: ").append(toIndentedString(previousDepartmentMinValue)).append("\n");
    sb.append("    previousDepartmentMaxValue: ").append(toIndentedString(previousDepartmentMaxValue)).append("\n");
    sb.append("    multiBranchIdMinValue: ").append(toIndentedString(multiBranchIdMinValue)).append("\n");
    sb.append("    multiBranchIdMaxValue: ").append(toIndentedString(multiBranchIdMaxValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

