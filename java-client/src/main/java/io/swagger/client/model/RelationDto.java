/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.RelationDeliveryAddressDto;
import io.swagger.client.model.RelationPostalAddressDto;
import io.swagger.client.model.RelationVisitAddressDto;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * RelationDto
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class RelationDto {
  @SerializedName("relationDeliveryAddress")
  private RelationDeliveryAddressDto relationDeliveryAddress = null;

  @SerializedName("relationPostalAddress")
  private RelationPostalAddressDto relationPostalAddress = null;

  @SerializedName("relationVisitAddress")
  private RelationVisitAddressDto relationVisitAddress = null;

  @SerializedName("number")
  private Integer number = null;

  @SerializedName("searchName")
  private String searchName = null;

  @SerializedName("bankAccountNumber")
  private String bankAccountNumber = null;

  @SerializedName("soort")
  private String soort = null;

  @SerializedName("vertegenwoordiger")
  private Integer vertegenwoordiger = null;

  @SerializedName("kerstKaart")
  private String kerstKaart = null;

  @SerializedName("soortwaarschuwing")
  private Integer soortwaarschuwing = null;

  @SerializedName("isBlocked")
  private Boolean isBlocked = null;

  @SerializedName("documentCount")
  private Integer documentCount = null;

  @SerializedName("debiteurSF")
  private String debiteurSF = null;

  @SerializedName("crediteurSF")
  private String crediteurSF = null;

  @SerializedName("waarschuwing")
  private String waarschuwing = null;

  @SerializedName("vrijveldAlfa1")
  private String vrijveldAlfa1 = null;

  @SerializedName("vrijveldAlfa2")
  private String vrijveldAlfa2 = null;

  @SerializedName("vrijveldAlfa3")
  private String vrijveldAlfa3 = null;

  @SerializedName("vrijveldAlfa4")
  private String vrijveldAlfa4 = null;

  @SerializedName("vrijveldAlfa5")
  private String vrijveldAlfa5 = null;

  @SerializedName("userDefinedFieldNumeric1")
  private Float userDefinedFieldNumeric1 = null;

  @SerializedName("userDefinedFieldNumeric2")
  private Float userDefinedFieldNumeric2 = null;

  @SerializedName("userDefinedFieldNumeric3")
  private Float userDefinedFieldNumeric3 = null;

  @SerializedName("userDefinedFieldNumeric4")
  private Float userDefinedFieldNumeric4 = null;

  @SerializedName("userDefinedFieldNumeric5")
  private Float userDefinedFieldNumeric5 = null;

  @SerializedName("freeFieldBoolean1")
  private Integer freeFieldBoolean1 = null;

  @SerializedName("freeFieldBoolean2")
  private Integer freeFieldBoolean2 = null;

  @SerializedName("freeFieldBoolean3")
  private Integer freeFieldBoolean3 = null;

  @SerializedName("freeFieldBoolean4")
  private Integer freeFieldBoolean4 = null;

  @SerializedName("freeFieldBoolean5")
  private Integer freeFieldBoolean5 = null;

  @SerializedName("email")
  private String email = null;

  @SerializedName("website")
  private String website = null;

  @SerializedName("marktSegment")
  private Integer marktSegment = null;

  @SerializedName("organisatiecode")
  private Integer organisatiecode = null;

  @SerializedName("giroAccountNumber")
  private String giroAccountNumber = null;

  @SerializedName("subOrganisatie")
  private Integer subOrganisatie = null;

  @SerializedName("bankIdentifierCode")
  private String bankIdentifierCode = null;

  @SerializedName("internationalBankAccountNumber")
  private String internationalBankAccountNumber = null;

  @SerializedName("ibaN2")
  private String ibaN2 = null;

  @SerializedName("biC2")
  private String biC2 = null;

  @SerializedName("operations")
  private List<String> operations = null;

  public RelationDto relationDeliveryAddress(RelationDeliveryAddressDto relationDeliveryAddress) {
    this.relationDeliveryAddress = relationDeliveryAddress;
    return this;
  }

   /**
   * Get relationDeliveryAddress
   * @return relationDeliveryAddress
  **/
  @ApiModelProperty(value = "")
  public RelationDeliveryAddressDto getRelationDeliveryAddress() {
    return relationDeliveryAddress;
  }

  public void setRelationDeliveryAddress(RelationDeliveryAddressDto relationDeliveryAddress) {
    this.relationDeliveryAddress = relationDeliveryAddress;
  }

  public RelationDto relationPostalAddress(RelationPostalAddressDto relationPostalAddress) {
    this.relationPostalAddress = relationPostalAddress;
    return this;
  }

   /**
   * Get relationPostalAddress
   * @return relationPostalAddress
  **/
  @ApiModelProperty(value = "")
  public RelationPostalAddressDto getRelationPostalAddress() {
    return relationPostalAddress;
  }

  public void setRelationPostalAddress(RelationPostalAddressDto relationPostalAddress) {
    this.relationPostalAddress = relationPostalAddress;
  }

  public RelationDto relationVisitAddress(RelationVisitAddressDto relationVisitAddress) {
    this.relationVisitAddress = relationVisitAddress;
    return this;
  }

   /**
   * Get relationVisitAddress
   * @return relationVisitAddress
  **/
  @ApiModelProperty(value = "")
  public RelationVisitAddressDto getRelationVisitAddress() {
    return relationVisitAddress;
  }

  public void setRelationVisitAddress(RelationVisitAddressDto relationVisitAddress) {
    this.relationVisitAddress = relationVisitAddress;
  }

  public RelationDto number(Integer number) {
    this.number = number;
    return this;
  }

   /**
   * Get number
   * @return number
  **/
  @ApiModelProperty(required = true, value = "")
  public Integer getNumber() {
    return number;
  }

  public void setNumber(Integer number) {
    this.number = number;
  }

  public RelationDto searchName(String searchName) {
    this.searchName = searchName;
    return this;
  }

   /**
   * Get searchName
   * @return searchName
  **/
  @ApiModelProperty(value = "")
  public String getSearchName() {
    return searchName;
  }

  public void setSearchName(String searchName) {
    this.searchName = searchName;
  }

  public RelationDto bankAccountNumber(String bankAccountNumber) {
    this.bankAccountNumber = bankAccountNumber;
    return this;
  }

   /**
   * Get bankAccountNumber
   * @return bankAccountNumber
  **/
  @ApiModelProperty(value = "")
  public String getBankAccountNumber() {
    return bankAccountNumber;
  }

  public void setBankAccountNumber(String bankAccountNumber) {
    this.bankAccountNumber = bankAccountNumber;
  }

  public RelationDto soort(String soort) {
    this.soort = soort;
    return this;
  }

   /**
   * Get soort
   * @return soort
  **/
  @ApiModelProperty(value = "")
  public String getSoort() {
    return soort;
  }

  public void setSoort(String soort) {
    this.soort = soort;
  }

  public RelationDto vertegenwoordiger(Integer vertegenwoordiger) {
    this.vertegenwoordiger = vertegenwoordiger;
    return this;
  }

   /**
   * Get vertegenwoordiger
   * @return vertegenwoordiger
  **/
  @ApiModelProperty(value = "")
  public Integer getVertegenwoordiger() {
    return vertegenwoordiger;
  }

  public void setVertegenwoordiger(Integer vertegenwoordiger) {
    this.vertegenwoordiger = vertegenwoordiger;
  }

  public RelationDto kerstKaart(String kerstKaart) {
    this.kerstKaart = kerstKaart;
    return this;
  }

   /**
   * Get kerstKaart
   * @return kerstKaart
  **/
  @ApiModelProperty(value = "")
  public String getKerstKaart() {
    return kerstKaart;
  }

  public void setKerstKaart(String kerstKaart) {
    this.kerstKaart = kerstKaart;
  }

  public RelationDto soortwaarschuwing(Integer soortwaarschuwing) {
    this.soortwaarschuwing = soortwaarschuwing;
    return this;
  }

   /**
   * Get soortwaarschuwing
   * @return soortwaarschuwing
  **/
  @ApiModelProperty(value = "")
  public Integer getSoortwaarschuwing() {
    return soortwaarschuwing;
  }

  public void setSoortwaarschuwing(Integer soortwaarschuwing) {
    this.soortwaarschuwing = soortwaarschuwing;
  }

  public RelationDto isBlocked(Boolean isBlocked) {
    this.isBlocked = isBlocked;
    return this;
  }

   /**
   * Get isBlocked
   * @return isBlocked
  **/
  @ApiModelProperty(value = "")
  public Boolean isIsBlocked() {
    return isBlocked;
  }

  public void setIsBlocked(Boolean isBlocked) {
    this.isBlocked = isBlocked;
  }

  public RelationDto documentCount(Integer documentCount) {
    this.documentCount = documentCount;
    return this;
  }

   /**
   * Get documentCount
   * @return documentCount
  **/
  @ApiModelProperty(value = "")
  public Integer getDocumentCount() {
    return documentCount;
  }

  public void setDocumentCount(Integer documentCount) {
    this.documentCount = documentCount;
  }

  public RelationDto debiteurSF(String debiteurSF) {
    this.debiteurSF = debiteurSF;
    return this;
  }

   /**
   * Get debiteurSF
   * @return debiteurSF
  **/
  @ApiModelProperty(value = "")
  public String getDebiteurSF() {
    return debiteurSF;
  }

  public void setDebiteurSF(String debiteurSF) {
    this.debiteurSF = debiteurSF;
  }

  public RelationDto crediteurSF(String crediteurSF) {
    this.crediteurSF = crediteurSF;
    return this;
  }

   /**
   * Get crediteurSF
   * @return crediteurSF
  **/
  @ApiModelProperty(value = "")
  public String getCrediteurSF() {
    return crediteurSF;
  }

  public void setCrediteurSF(String crediteurSF) {
    this.crediteurSF = crediteurSF;
  }

  public RelationDto waarschuwing(String waarschuwing) {
    this.waarschuwing = waarschuwing;
    return this;
  }

   /**
   * Get waarschuwing
   * @return waarschuwing
  **/
  @ApiModelProperty(value = "")
  public String getWaarschuwing() {
    return waarschuwing;
  }

  public void setWaarschuwing(String waarschuwing) {
    this.waarschuwing = waarschuwing;
  }

  public RelationDto vrijveldAlfa1(String vrijveldAlfa1) {
    this.vrijveldAlfa1 = vrijveldAlfa1;
    return this;
  }

   /**
   * Get vrijveldAlfa1
   * @return vrijveldAlfa1
  **/
  @ApiModelProperty(value = "")
  public String getVrijveldAlfa1() {
    return vrijveldAlfa1;
  }

  public void setVrijveldAlfa1(String vrijveldAlfa1) {
    this.vrijveldAlfa1 = vrijveldAlfa1;
  }

  public RelationDto vrijveldAlfa2(String vrijveldAlfa2) {
    this.vrijveldAlfa2 = vrijveldAlfa2;
    return this;
  }

   /**
   * Get vrijveldAlfa2
   * @return vrijveldAlfa2
  **/
  @ApiModelProperty(value = "")
  public String getVrijveldAlfa2() {
    return vrijveldAlfa2;
  }

  public void setVrijveldAlfa2(String vrijveldAlfa2) {
    this.vrijveldAlfa2 = vrijveldAlfa2;
  }

  public RelationDto vrijveldAlfa3(String vrijveldAlfa3) {
    this.vrijveldAlfa3 = vrijveldAlfa3;
    return this;
  }

   /**
   * Get vrijveldAlfa3
   * @return vrijveldAlfa3
  **/
  @ApiModelProperty(value = "")
  public String getVrijveldAlfa3() {
    return vrijveldAlfa3;
  }

  public void setVrijveldAlfa3(String vrijveldAlfa3) {
    this.vrijveldAlfa3 = vrijveldAlfa3;
  }

  public RelationDto vrijveldAlfa4(String vrijveldAlfa4) {
    this.vrijveldAlfa4 = vrijveldAlfa4;
    return this;
  }

   /**
   * Get vrijveldAlfa4
   * @return vrijveldAlfa4
  **/
  @ApiModelProperty(value = "")
  public String getVrijveldAlfa4() {
    return vrijveldAlfa4;
  }

  public void setVrijveldAlfa4(String vrijveldAlfa4) {
    this.vrijveldAlfa4 = vrijveldAlfa4;
  }

  public RelationDto vrijveldAlfa5(String vrijveldAlfa5) {
    this.vrijveldAlfa5 = vrijveldAlfa5;
    return this;
  }

   /**
   * Get vrijveldAlfa5
   * @return vrijveldAlfa5
  **/
  @ApiModelProperty(value = "")
  public String getVrijveldAlfa5() {
    return vrijveldAlfa5;
  }

  public void setVrijveldAlfa5(String vrijveldAlfa5) {
    this.vrijveldAlfa5 = vrijveldAlfa5;
  }

  public RelationDto userDefinedFieldNumeric1(Float userDefinedFieldNumeric1) {
    this.userDefinedFieldNumeric1 = userDefinedFieldNumeric1;
    return this;
  }

   /**
   * Get userDefinedFieldNumeric1
   * @return userDefinedFieldNumeric1
  **/
  @ApiModelProperty(value = "")
  public Float getUserDefinedFieldNumeric1() {
    return userDefinedFieldNumeric1;
  }

  public void setUserDefinedFieldNumeric1(Float userDefinedFieldNumeric1) {
    this.userDefinedFieldNumeric1 = userDefinedFieldNumeric1;
  }

  public RelationDto userDefinedFieldNumeric2(Float userDefinedFieldNumeric2) {
    this.userDefinedFieldNumeric2 = userDefinedFieldNumeric2;
    return this;
  }

   /**
   * Get userDefinedFieldNumeric2
   * @return userDefinedFieldNumeric2
  **/
  @ApiModelProperty(value = "")
  public Float getUserDefinedFieldNumeric2() {
    return userDefinedFieldNumeric2;
  }

  public void setUserDefinedFieldNumeric2(Float userDefinedFieldNumeric2) {
    this.userDefinedFieldNumeric2 = userDefinedFieldNumeric2;
  }

  public RelationDto userDefinedFieldNumeric3(Float userDefinedFieldNumeric3) {
    this.userDefinedFieldNumeric3 = userDefinedFieldNumeric3;
    return this;
  }

   /**
   * Get userDefinedFieldNumeric3
   * @return userDefinedFieldNumeric3
  **/
  @ApiModelProperty(value = "")
  public Float getUserDefinedFieldNumeric3() {
    return userDefinedFieldNumeric3;
  }

  public void setUserDefinedFieldNumeric3(Float userDefinedFieldNumeric3) {
    this.userDefinedFieldNumeric3 = userDefinedFieldNumeric3;
  }

  public RelationDto userDefinedFieldNumeric4(Float userDefinedFieldNumeric4) {
    this.userDefinedFieldNumeric4 = userDefinedFieldNumeric4;
    return this;
  }

   /**
   * Get userDefinedFieldNumeric4
   * @return userDefinedFieldNumeric4
  **/
  @ApiModelProperty(value = "")
  public Float getUserDefinedFieldNumeric4() {
    return userDefinedFieldNumeric4;
  }

  public void setUserDefinedFieldNumeric4(Float userDefinedFieldNumeric4) {
    this.userDefinedFieldNumeric4 = userDefinedFieldNumeric4;
  }

  public RelationDto userDefinedFieldNumeric5(Float userDefinedFieldNumeric5) {
    this.userDefinedFieldNumeric5 = userDefinedFieldNumeric5;
    return this;
  }

   /**
   * Get userDefinedFieldNumeric5
   * @return userDefinedFieldNumeric5
  **/
  @ApiModelProperty(value = "")
  public Float getUserDefinedFieldNumeric5() {
    return userDefinedFieldNumeric5;
  }

  public void setUserDefinedFieldNumeric5(Float userDefinedFieldNumeric5) {
    this.userDefinedFieldNumeric5 = userDefinedFieldNumeric5;
  }

  public RelationDto freeFieldBoolean1(Integer freeFieldBoolean1) {
    this.freeFieldBoolean1 = freeFieldBoolean1;
    return this;
  }

   /**
   * Get freeFieldBoolean1
   * @return freeFieldBoolean1
  **/
  @ApiModelProperty(value = "")
  public Integer getFreeFieldBoolean1() {
    return freeFieldBoolean1;
  }

  public void setFreeFieldBoolean1(Integer freeFieldBoolean1) {
    this.freeFieldBoolean1 = freeFieldBoolean1;
  }

  public RelationDto freeFieldBoolean2(Integer freeFieldBoolean2) {
    this.freeFieldBoolean2 = freeFieldBoolean2;
    return this;
  }

   /**
   * Get freeFieldBoolean2
   * @return freeFieldBoolean2
  **/
  @ApiModelProperty(value = "")
  public Integer getFreeFieldBoolean2() {
    return freeFieldBoolean2;
  }

  public void setFreeFieldBoolean2(Integer freeFieldBoolean2) {
    this.freeFieldBoolean2 = freeFieldBoolean2;
  }

  public RelationDto freeFieldBoolean3(Integer freeFieldBoolean3) {
    this.freeFieldBoolean3 = freeFieldBoolean3;
    return this;
  }

   /**
   * Get freeFieldBoolean3
   * @return freeFieldBoolean3
  **/
  @ApiModelProperty(value = "")
  public Integer getFreeFieldBoolean3() {
    return freeFieldBoolean3;
  }

  public void setFreeFieldBoolean3(Integer freeFieldBoolean3) {
    this.freeFieldBoolean3 = freeFieldBoolean3;
  }

  public RelationDto freeFieldBoolean4(Integer freeFieldBoolean4) {
    this.freeFieldBoolean4 = freeFieldBoolean4;
    return this;
  }

   /**
   * Get freeFieldBoolean4
   * @return freeFieldBoolean4
  **/
  @ApiModelProperty(value = "")
  public Integer getFreeFieldBoolean4() {
    return freeFieldBoolean4;
  }

  public void setFreeFieldBoolean4(Integer freeFieldBoolean4) {
    this.freeFieldBoolean4 = freeFieldBoolean4;
  }

  public RelationDto freeFieldBoolean5(Integer freeFieldBoolean5) {
    this.freeFieldBoolean5 = freeFieldBoolean5;
    return this;
  }

   /**
   * Get freeFieldBoolean5
   * @return freeFieldBoolean5
  **/
  @ApiModelProperty(value = "")
  public Integer getFreeFieldBoolean5() {
    return freeFieldBoolean5;
  }

  public void setFreeFieldBoolean5(Integer freeFieldBoolean5) {
    this.freeFieldBoolean5 = freeFieldBoolean5;
  }

  public RelationDto email(String email) {
    this.email = email;
    return this;
  }

   /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(value = "")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public RelationDto website(String website) {
    this.website = website;
    return this;
  }

   /**
   * Get website
   * @return website
  **/
  @ApiModelProperty(value = "")
  public String getWebsite() {
    return website;
  }

  public void setWebsite(String website) {
    this.website = website;
  }

  public RelationDto marktSegment(Integer marktSegment) {
    this.marktSegment = marktSegment;
    return this;
  }

   /**
   * Get marktSegment
   * @return marktSegment
  **/
  @ApiModelProperty(value = "")
  public Integer getMarktSegment() {
    return marktSegment;
  }

  public void setMarktSegment(Integer marktSegment) {
    this.marktSegment = marktSegment;
  }

  public RelationDto organisatiecode(Integer organisatiecode) {
    this.organisatiecode = organisatiecode;
    return this;
  }

   /**
   * Get organisatiecode
   * @return organisatiecode
  **/
  @ApiModelProperty(value = "")
  public Integer getOrganisatiecode() {
    return organisatiecode;
  }

  public void setOrganisatiecode(Integer organisatiecode) {
    this.organisatiecode = organisatiecode;
  }

  public RelationDto giroAccountNumber(String giroAccountNumber) {
    this.giroAccountNumber = giroAccountNumber;
    return this;
  }

   /**
   * Get giroAccountNumber
   * @return giroAccountNumber
  **/
  @ApiModelProperty(value = "")
  public String getGiroAccountNumber() {
    return giroAccountNumber;
  }

  public void setGiroAccountNumber(String giroAccountNumber) {
    this.giroAccountNumber = giroAccountNumber;
  }

  public RelationDto subOrganisatie(Integer subOrganisatie) {
    this.subOrganisatie = subOrganisatie;
    return this;
  }

   /**
   * Get subOrganisatie
   * @return subOrganisatie
  **/
  @ApiModelProperty(value = "")
  public Integer getSubOrganisatie() {
    return subOrganisatie;
  }

  public void setSubOrganisatie(Integer subOrganisatie) {
    this.subOrganisatie = subOrganisatie;
  }

  public RelationDto bankIdentifierCode(String bankIdentifierCode) {
    this.bankIdentifierCode = bankIdentifierCode;
    return this;
  }

   /**
   * Get bankIdentifierCode
   * @return bankIdentifierCode
  **/
  @ApiModelProperty(value = "")
  public String getBankIdentifierCode() {
    return bankIdentifierCode;
  }

  public void setBankIdentifierCode(String bankIdentifierCode) {
    this.bankIdentifierCode = bankIdentifierCode;
  }

  public RelationDto internationalBankAccountNumber(String internationalBankAccountNumber) {
    this.internationalBankAccountNumber = internationalBankAccountNumber;
    return this;
  }

   /**
   * Get internationalBankAccountNumber
   * @return internationalBankAccountNumber
  **/
  @ApiModelProperty(value = "")
  public String getInternationalBankAccountNumber() {
    return internationalBankAccountNumber;
  }

  public void setInternationalBankAccountNumber(String internationalBankAccountNumber) {
    this.internationalBankAccountNumber = internationalBankAccountNumber;
  }

  public RelationDto ibaN2(String ibaN2) {
    this.ibaN2 = ibaN2;
    return this;
  }

   /**
   * Get ibaN2
   * @return ibaN2
  **/
  @ApiModelProperty(value = "")
  public String getIbaN2() {
    return ibaN2;
  }

  public void setIbaN2(String ibaN2) {
    this.ibaN2 = ibaN2;
  }

  public RelationDto biC2(String biC2) {
    this.biC2 = biC2;
    return this;
  }

   /**
   * Get biC2
   * @return biC2
  **/
  @ApiModelProperty(value = "")
  public String getBiC2() {
    return biC2;
  }

  public void setBiC2(String biC2) {
    this.biC2 = biC2;
  }

   /**
   * Get operations
   * @return operations
  **/
  @ApiModelProperty(value = "")
  public List<String> getOperations() {
    return operations;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RelationDto relationDto = (RelationDto) o;
    return Objects.equals(this.relationDeliveryAddress, relationDto.relationDeliveryAddress) &&
        Objects.equals(this.relationPostalAddress, relationDto.relationPostalAddress) &&
        Objects.equals(this.relationVisitAddress, relationDto.relationVisitAddress) &&
        Objects.equals(this.number, relationDto.number) &&
        Objects.equals(this.searchName, relationDto.searchName) &&
        Objects.equals(this.bankAccountNumber, relationDto.bankAccountNumber) &&
        Objects.equals(this.soort, relationDto.soort) &&
        Objects.equals(this.vertegenwoordiger, relationDto.vertegenwoordiger) &&
        Objects.equals(this.kerstKaart, relationDto.kerstKaart) &&
        Objects.equals(this.soortwaarschuwing, relationDto.soortwaarschuwing) &&
        Objects.equals(this.isBlocked, relationDto.isBlocked) &&
        Objects.equals(this.documentCount, relationDto.documentCount) &&
        Objects.equals(this.debiteurSF, relationDto.debiteurSF) &&
        Objects.equals(this.crediteurSF, relationDto.crediteurSF) &&
        Objects.equals(this.waarschuwing, relationDto.waarschuwing) &&
        Objects.equals(this.vrijveldAlfa1, relationDto.vrijveldAlfa1) &&
        Objects.equals(this.vrijveldAlfa2, relationDto.vrijveldAlfa2) &&
        Objects.equals(this.vrijveldAlfa3, relationDto.vrijveldAlfa3) &&
        Objects.equals(this.vrijveldAlfa4, relationDto.vrijveldAlfa4) &&
        Objects.equals(this.vrijveldAlfa5, relationDto.vrijveldAlfa5) &&
        Objects.equals(this.userDefinedFieldNumeric1, relationDto.userDefinedFieldNumeric1) &&
        Objects.equals(this.userDefinedFieldNumeric2, relationDto.userDefinedFieldNumeric2) &&
        Objects.equals(this.userDefinedFieldNumeric3, relationDto.userDefinedFieldNumeric3) &&
        Objects.equals(this.userDefinedFieldNumeric4, relationDto.userDefinedFieldNumeric4) &&
        Objects.equals(this.userDefinedFieldNumeric5, relationDto.userDefinedFieldNumeric5) &&
        Objects.equals(this.freeFieldBoolean1, relationDto.freeFieldBoolean1) &&
        Objects.equals(this.freeFieldBoolean2, relationDto.freeFieldBoolean2) &&
        Objects.equals(this.freeFieldBoolean3, relationDto.freeFieldBoolean3) &&
        Objects.equals(this.freeFieldBoolean4, relationDto.freeFieldBoolean4) &&
        Objects.equals(this.freeFieldBoolean5, relationDto.freeFieldBoolean5) &&
        Objects.equals(this.email, relationDto.email) &&
        Objects.equals(this.website, relationDto.website) &&
        Objects.equals(this.marktSegment, relationDto.marktSegment) &&
        Objects.equals(this.organisatiecode, relationDto.organisatiecode) &&
        Objects.equals(this.giroAccountNumber, relationDto.giroAccountNumber) &&
        Objects.equals(this.subOrganisatie, relationDto.subOrganisatie) &&
        Objects.equals(this.bankIdentifierCode, relationDto.bankIdentifierCode) &&
        Objects.equals(this.internationalBankAccountNumber, relationDto.internationalBankAccountNumber) &&
        Objects.equals(this.ibaN2, relationDto.ibaN2) &&
        Objects.equals(this.biC2, relationDto.biC2) &&
        Objects.equals(this.operations, relationDto.operations);
  }

  @Override
  public int hashCode() {
    return Objects.hash(relationDeliveryAddress, relationPostalAddress, relationVisitAddress, number, searchName, bankAccountNumber, soort, vertegenwoordiger, kerstKaart, soortwaarschuwing, isBlocked, documentCount, debiteurSF, crediteurSF, waarschuwing, vrijveldAlfa1, vrijveldAlfa2, vrijveldAlfa3, vrijveldAlfa4, vrijveldAlfa5, userDefinedFieldNumeric1, userDefinedFieldNumeric2, userDefinedFieldNumeric3, userDefinedFieldNumeric4, userDefinedFieldNumeric5, freeFieldBoolean1, freeFieldBoolean2, freeFieldBoolean3, freeFieldBoolean4, freeFieldBoolean5, email, website, marktSegment, organisatiecode, giroAccountNumber, subOrganisatie, bankIdentifierCode, internationalBankAccountNumber, ibaN2, biC2, operations);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RelationDto {\n");
    
    sb.append("    relationDeliveryAddress: ").append(toIndentedString(relationDeliveryAddress)).append("\n");
    sb.append("    relationPostalAddress: ").append(toIndentedString(relationPostalAddress)).append("\n");
    sb.append("    relationVisitAddress: ").append(toIndentedString(relationVisitAddress)).append("\n");
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    searchName: ").append(toIndentedString(searchName)).append("\n");
    sb.append("    bankAccountNumber: ").append(toIndentedString(bankAccountNumber)).append("\n");
    sb.append("    soort: ").append(toIndentedString(soort)).append("\n");
    sb.append("    vertegenwoordiger: ").append(toIndentedString(vertegenwoordiger)).append("\n");
    sb.append("    kerstKaart: ").append(toIndentedString(kerstKaart)).append("\n");
    sb.append("    soortwaarschuwing: ").append(toIndentedString(soortwaarschuwing)).append("\n");
    sb.append("    isBlocked: ").append(toIndentedString(isBlocked)).append("\n");
    sb.append("    documentCount: ").append(toIndentedString(documentCount)).append("\n");
    sb.append("    debiteurSF: ").append(toIndentedString(debiteurSF)).append("\n");
    sb.append("    crediteurSF: ").append(toIndentedString(crediteurSF)).append("\n");
    sb.append("    waarschuwing: ").append(toIndentedString(waarschuwing)).append("\n");
    sb.append("    vrijveldAlfa1: ").append(toIndentedString(vrijveldAlfa1)).append("\n");
    sb.append("    vrijveldAlfa2: ").append(toIndentedString(vrijveldAlfa2)).append("\n");
    sb.append("    vrijveldAlfa3: ").append(toIndentedString(vrijveldAlfa3)).append("\n");
    sb.append("    vrijveldAlfa4: ").append(toIndentedString(vrijveldAlfa4)).append("\n");
    sb.append("    vrijveldAlfa5: ").append(toIndentedString(vrijveldAlfa5)).append("\n");
    sb.append("    userDefinedFieldNumeric1: ").append(toIndentedString(userDefinedFieldNumeric1)).append("\n");
    sb.append("    userDefinedFieldNumeric2: ").append(toIndentedString(userDefinedFieldNumeric2)).append("\n");
    sb.append("    userDefinedFieldNumeric3: ").append(toIndentedString(userDefinedFieldNumeric3)).append("\n");
    sb.append("    userDefinedFieldNumeric4: ").append(toIndentedString(userDefinedFieldNumeric4)).append("\n");
    sb.append("    userDefinedFieldNumeric5: ").append(toIndentedString(userDefinedFieldNumeric5)).append("\n");
    sb.append("    freeFieldBoolean1: ").append(toIndentedString(freeFieldBoolean1)).append("\n");
    sb.append("    freeFieldBoolean2: ").append(toIndentedString(freeFieldBoolean2)).append("\n");
    sb.append("    freeFieldBoolean3: ").append(toIndentedString(freeFieldBoolean3)).append("\n");
    sb.append("    freeFieldBoolean4: ").append(toIndentedString(freeFieldBoolean4)).append("\n");
    sb.append("    freeFieldBoolean5: ").append(toIndentedString(freeFieldBoolean5)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    website: ").append(toIndentedString(website)).append("\n");
    sb.append("    marktSegment: ").append(toIndentedString(marktSegment)).append("\n");
    sb.append("    organisatiecode: ").append(toIndentedString(organisatiecode)).append("\n");
    sb.append("    giroAccountNumber: ").append(toIndentedString(giroAccountNumber)).append("\n");
    sb.append("    subOrganisatie: ").append(toIndentedString(subOrganisatie)).append("\n");
    sb.append("    bankIdentifierCode: ").append(toIndentedString(bankIdentifierCode)).append("\n");
    sb.append("    internationalBankAccountNumber: ").append(toIndentedString(internationalBankAccountNumber)).append("\n");
    sb.append("    ibaN2: ").append(toIndentedString(ibaN2)).append("\n");
    sb.append("    biC2: ").append(toIndentedString(biC2)).append("\n");
    sb.append("    operations: ").append(toIndentedString(operations)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

