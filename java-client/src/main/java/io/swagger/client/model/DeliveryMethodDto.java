/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * DeliveryMethodDto
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class DeliveryMethodDto {
  @SerializedName("deliveryMethodId")
  private Integer deliveryMethodId = null;

  @SerializedName("description")
  private String description = null;

  @SerializedName("operations")
  private List<String> operations = null;

  public DeliveryMethodDto deliveryMethodId(Integer deliveryMethodId) {
    this.deliveryMethodId = deliveryMethodId;
    return this;
  }

   /**
   * Get deliveryMethodId
   * @return deliveryMethodId
  **/
  @ApiModelProperty(required = true, value = "")
  public Integer getDeliveryMethodId() {
    return deliveryMethodId;
  }

  public void setDeliveryMethodId(Integer deliveryMethodId) {
    this.deliveryMethodId = deliveryMethodId;
  }

  public DeliveryMethodDto description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

   /**
   * Get operations
   * @return operations
  **/
  @ApiModelProperty(value = "")
  public List<String> getOperations() {
    return operations;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeliveryMethodDto deliveryMethodDto = (DeliveryMethodDto) o;
    return Objects.equals(this.deliveryMethodId, deliveryMethodDto.deliveryMethodId) &&
        Objects.equals(this.description, deliveryMethodDto.description) &&
        Objects.equals(this.operations, deliveryMethodDto.operations);
  }

  @Override
  public int hashCode() {
    return Objects.hash(deliveryMethodId, description, operations);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeliveryMethodDto {\n");
    
    sb.append("    deliveryMethodId: ").append(toIndentedString(deliveryMethodId)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    operations: ").append(toIndentedString(operations)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

