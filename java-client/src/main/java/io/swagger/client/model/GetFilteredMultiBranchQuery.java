/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * GetFilteredMultiBranchQuery
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class GetFilteredMultiBranchQuery {
  @SerializedName("multiBranchIdMinValue")
  private Integer multiBranchIdMinValue = null;

  @SerializedName("multiBranchIdMaxValue")
  private Integer multiBranchIdMaxValue = null;

  @SerializedName("nameMinValue")
  private String nameMinValue = null;

  @SerializedName("nameMaxValue")
  private String nameMaxValue = null;

  @SerializedName("fromProductionGroupIdMinValue")
  private Integer fromProductionGroupIdMinValue = null;

  @SerializedName("fromProductionGroupIdMaxValue")
  private Integer fromProductionGroupIdMaxValue = null;

  @SerializedName("isMainBranchMinValue")
  private Integer isMainBranchMinValue = null;

  @SerializedName("isMainBranchMaxValue")
  private Integer isMainBranchMaxValue = null;

  public GetFilteredMultiBranchQuery multiBranchIdMinValue(Integer multiBranchIdMinValue) {
    this.multiBranchIdMinValue = multiBranchIdMinValue;
    return this;
  }

   /**
   * Get multiBranchIdMinValue
   * @return multiBranchIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getMultiBranchIdMinValue() {
    return multiBranchIdMinValue;
  }

  public void setMultiBranchIdMinValue(Integer multiBranchIdMinValue) {
    this.multiBranchIdMinValue = multiBranchIdMinValue;
  }

  public GetFilteredMultiBranchQuery multiBranchIdMaxValue(Integer multiBranchIdMaxValue) {
    this.multiBranchIdMaxValue = multiBranchIdMaxValue;
    return this;
  }

   /**
   * Get multiBranchIdMaxValue
   * @return multiBranchIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getMultiBranchIdMaxValue() {
    return multiBranchIdMaxValue;
  }

  public void setMultiBranchIdMaxValue(Integer multiBranchIdMaxValue) {
    this.multiBranchIdMaxValue = multiBranchIdMaxValue;
  }

  public GetFilteredMultiBranchQuery nameMinValue(String nameMinValue) {
    this.nameMinValue = nameMinValue;
    return this;
  }

   /**
   * Get nameMinValue
   * @return nameMinValue
  **/
  @ApiModelProperty(value = "")
  public String getNameMinValue() {
    return nameMinValue;
  }

  public void setNameMinValue(String nameMinValue) {
    this.nameMinValue = nameMinValue;
  }

  public GetFilteredMultiBranchQuery nameMaxValue(String nameMaxValue) {
    this.nameMaxValue = nameMaxValue;
    return this;
  }

   /**
   * Get nameMaxValue
   * @return nameMaxValue
  **/
  @ApiModelProperty(value = "")
  public String getNameMaxValue() {
    return nameMaxValue;
  }

  public void setNameMaxValue(String nameMaxValue) {
    this.nameMaxValue = nameMaxValue;
  }

  public GetFilteredMultiBranchQuery fromProductionGroupIdMinValue(Integer fromProductionGroupIdMinValue) {
    this.fromProductionGroupIdMinValue = fromProductionGroupIdMinValue;
    return this;
  }

   /**
   * Get fromProductionGroupIdMinValue
   * @return fromProductionGroupIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getFromProductionGroupIdMinValue() {
    return fromProductionGroupIdMinValue;
  }

  public void setFromProductionGroupIdMinValue(Integer fromProductionGroupIdMinValue) {
    this.fromProductionGroupIdMinValue = fromProductionGroupIdMinValue;
  }

  public GetFilteredMultiBranchQuery fromProductionGroupIdMaxValue(Integer fromProductionGroupIdMaxValue) {
    this.fromProductionGroupIdMaxValue = fromProductionGroupIdMaxValue;
    return this;
  }

   /**
   * Get fromProductionGroupIdMaxValue
   * @return fromProductionGroupIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getFromProductionGroupIdMaxValue() {
    return fromProductionGroupIdMaxValue;
  }

  public void setFromProductionGroupIdMaxValue(Integer fromProductionGroupIdMaxValue) {
    this.fromProductionGroupIdMaxValue = fromProductionGroupIdMaxValue;
  }

  public GetFilteredMultiBranchQuery isMainBranchMinValue(Integer isMainBranchMinValue) {
    this.isMainBranchMinValue = isMainBranchMinValue;
    return this;
  }

   /**
   * Get isMainBranchMinValue
   * @return isMainBranchMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getIsMainBranchMinValue() {
    return isMainBranchMinValue;
  }

  public void setIsMainBranchMinValue(Integer isMainBranchMinValue) {
    this.isMainBranchMinValue = isMainBranchMinValue;
  }

  public GetFilteredMultiBranchQuery isMainBranchMaxValue(Integer isMainBranchMaxValue) {
    this.isMainBranchMaxValue = isMainBranchMaxValue;
    return this;
  }

   /**
   * Get isMainBranchMaxValue
   * @return isMainBranchMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getIsMainBranchMaxValue() {
    return isMainBranchMaxValue;
  }

  public void setIsMainBranchMaxValue(Integer isMainBranchMaxValue) {
    this.isMainBranchMaxValue = isMainBranchMaxValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetFilteredMultiBranchQuery getFilteredMultiBranchQuery = (GetFilteredMultiBranchQuery) o;
    return Objects.equals(this.multiBranchIdMinValue, getFilteredMultiBranchQuery.multiBranchIdMinValue) &&
        Objects.equals(this.multiBranchIdMaxValue, getFilteredMultiBranchQuery.multiBranchIdMaxValue) &&
        Objects.equals(this.nameMinValue, getFilteredMultiBranchQuery.nameMinValue) &&
        Objects.equals(this.nameMaxValue, getFilteredMultiBranchQuery.nameMaxValue) &&
        Objects.equals(this.fromProductionGroupIdMinValue, getFilteredMultiBranchQuery.fromProductionGroupIdMinValue) &&
        Objects.equals(this.fromProductionGroupIdMaxValue, getFilteredMultiBranchQuery.fromProductionGroupIdMaxValue) &&
        Objects.equals(this.isMainBranchMinValue, getFilteredMultiBranchQuery.isMainBranchMinValue) &&
        Objects.equals(this.isMainBranchMaxValue, getFilteredMultiBranchQuery.isMainBranchMaxValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(multiBranchIdMinValue, multiBranchIdMaxValue, nameMinValue, nameMaxValue, fromProductionGroupIdMinValue, fromProductionGroupIdMaxValue, isMainBranchMinValue, isMainBranchMaxValue);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetFilteredMultiBranchQuery {\n");
    
    sb.append("    multiBranchIdMinValue: ").append(toIndentedString(multiBranchIdMinValue)).append("\n");
    sb.append("    multiBranchIdMaxValue: ").append(toIndentedString(multiBranchIdMaxValue)).append("\n");
    sb.append("    nameMinValue: ").append(toIndentedString(nameMinValue)).append("\n");
    sb.append("    nameMaxValue: ").append(toIndentedString(nameMaxValue)).append("\n");
    sb.append("    fromProductionGroupIdMinValue: ").append(toIndentedString(fromProductionGroupIdMinValue)).append("\n");
    sb.append("    fromProductionGroupIdMaxValue: ").append(toIndentedString(fromProductionGroupIdMaxValue)).append("\n");
    sb.append("    isMainBranchMinValue: ").append(toIndentedString(isMainBranchMinValue)).append("\n");
    sb.append("    isMainBranchMaxValue: ").append(toIndentedString(isMainBranchMaxValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

