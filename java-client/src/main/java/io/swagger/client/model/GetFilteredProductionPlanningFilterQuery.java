/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * GetFilteredProductionPlanningFilterQuery
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class GetFilteredProductionPlanningFilterQuery {
  @SerializedName("productionPlanningFilterIdMinValue")
  private Integer productionPlanningFilterIdMinValue = null;

  @SerializedName("productionPlanningFilterIdMaxValue")
  private Integer productionPlanningFilterIdMaxValue = null;

  @SerializedName("numberMinValue")
  private Integer numberMinValue = null;

  @SerializedName("numberMaxValue")
  private Integer numberMaxValue = null;

  @SerializedName("nameMinValue")
  private String nameMinValue = null;

  @SerializedName("nameMaxValue")
  private String nameMaxValue = null;

  public GetFilteredProductionPlanningFilterQuery productionPlanningFilterIdMinValue(Integer productionPlanningFilterIdMinValue) {
    this.productionPlanningFilterIdMinValue = productionPlanningFilterIdMinValue;
    return this;
  }

   /**
   * Get productionPlanningFilterIdMinValue
   * @return productionPlanningFilterIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getProductionPlanningFilterIdMinValue() {
    return productionPlanningFilterIdMinValue;
  }

  public void setProductionPlanningFilterIdMinValue(Integer productionPlanningFilterIdMinValue) {
    this.productionPlanningFilterIdMinValue = productionPlanningFilterIdMinValue;
  }

  public GetFilteredProductionPlanningFilterQuery productionPlanningFilterIdMaxValue(Integer productionPlanningFilterIdMaxValue) {
    this.productionPlanningFilterIdMaxValue = productionPlanningFilterIdMaxValue;
    return this;
  }

   /**
   * Get productionPlanningFilterIdMaxValue
   * @return productionPlanningFilterIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getProductionPlanningFilterIdMaxValue() {
    return productionPlanningFilterIdMaxValue;
  }

  public void setProductionPlanningFilterIdMaxValue(Integer productionPlanningFilterIdMaxValue) {
    this.productionPlanningFilterIdMaxValue = productionPlanningFilterIdMaxValue;
  }

  public GetFilteredProductionPlanningFilterQuery numberMinValue(Integer numberMinValue) {
    this.numberMinValue = numberMinValue;
    return this;
  }

   /**
   * Get numberMinValue
   * @return numberMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getNumberMinValue() {
    return numberMinValue;
  }

  public void setNumberMinValue(Integer numberMinValue) {
    this.numberMinValue = numberMinValue;
  }

  public GetFilteredProductionPlanningFilterQuery numberMaxValue(Integer numberMaxValue) {
    this.numberMaxValue = numberMaxValue;
    return this;
  }

   /**
   * Get numberMaxValue
   * @return numberMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getNumberMaxValue() {
    return numberMaxValue;
  }

  public void setNumberMaxValue(Integer numberMaxValue) {
    this.numberMaxValue = numberMaxValue;
  }

  public GetFilteredProductionPlanningFilterQuery nameMinValue(String nameMinValue) {
    this.nameMinValue = nameMinValue;
    return this;
  }

   /**
   * Get nameMinValue
   * @return nameMinValue
  **/
  @ApiModelProperty(value = "")
  public String getNameMinValue() {
    return nameMinValue;
  }

  public void setNameMinValue(String nameMinValue) {
    this.nameMinValue = nameMinValue;
  }

  public GetFilteredProductionPlanningFilterQuery nameMaxValue(String nameMaxValue) {
    this.nameMaxValue = nameMaxValue;
    return this;
  }

   /**
   * Get nameMaxValue
   * @return nameMaxValue
  **/
  @ApiModelProperty(value = "")
  public String getNameMaxValue() {
    return nameMaxValue;
  }

  public void setNameMaxValue(String nameMaxValue) {
    this.nameMaxValue = nameMaxValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetFilteredProductionPlanningFilterQuery getFilteredProductionPlanningFilterQuery = (GetFilteredProductionPlanningFilterQuery) o;
    return Objects.equals(this.productionPlanningFilterIdMinValue, getFilteredProductionPlanningFilterQuery.productionPlanningFilterIdMinValue) &&
        Objects.equals(this.productionPlanningFilterIdMaxValue, getFilteredProductionPlanningFilterQuery.productionPlanningFilterIdMaxValue) &&
        Objects.equals(this.numberMinValue, getFilteredProductionPlanningFilterQuery.numberMinValue) &&
        Objects.equals(this.numberMaxValue, getFilteredProductionPlanningFilterQuery.numberMaxValue) &&
        Objects.equals(this.nameMinValue, getFilteredProductionPlanningFilterQuery.nameMinValue) &&
        Objects.equals(this.nameMaxValue, getFilteredProductionPlanningFilterQuery.nameMaxValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(productionPlanningFilterIdMinValue, productionPlanningFilterIdMaxValue, numberMinValue, numberMaxValue, nameMinValue, nameMaxValue);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetFilteredProductionPlanningFilterQuery {\n");
    
    sb.append("    productionPlanningFilterIdMinValue: ").append(toIndentedString(productionPlanningFilterIdMinValue)).append("\n");
    sb.append("    productionPlanningFilterIdMaxValue: ").append(toIndentedString(productionPlanningFilterIdMaxValue)).append("\n");
    sb.append("    numberMinValue: ").append(toIndentedString(numberMinValue)).append("\n");
    sb.append("    numberMaxValue: ").append(toIndentedString(numberMaxValue)).append("\n");
    sb.append("    nameMinValue: ").append(toIndentedString(nameMinValue)).append("\n");
    sb.append("    nameMaxValue: ").append(toIndentedString(nameMaxValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

