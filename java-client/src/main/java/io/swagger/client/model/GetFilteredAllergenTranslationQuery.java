/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * GetFilteredAllergenTranslationQuery
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class GetFilteredAllergenTranslationQuery {
  /**
   * Gets or Sets typeMinValue
   */
  @JsonAdapter(TypeMinValueEnum.Adapter.class)
  public enum TypeMinValueEnum {
    NONE("None"),
    
    TRANSLATESALEARTICLEGROUP("TranslateSaleArticleGroup"),
    
    TRANSLATESALEARTICLEMAINGROUP("TranslateSaleArticleMainGroup"),
    
    TRANSLATESALEARTICLESUBGROUP("TranslateSaleArticleSubGroup"),
    
    TRANSLATEBASEMATERIAL("TranslateBaseMaterial"),
    
    TRANSLATEALLERGEN("TranslateAllergen"),
    
    TRANSLATENUTRITIONAL("TranslateNutritional"),
    
    TRANSLATENUTRITIONALUNIT("TranslateNutritionalUnit"),
    
    TRANSLATERECIPE("TranslateRecipe"),
    
    LOTEXTRAFIELD("LotExtraField"),
    
    SALEARTICLEALLERGENDESCRIPTION("SaleArticleAllergenDescription"),
    
    SALEARTICLENUTRITIONALDESCRIPTION("SaleArticleNutritionalDescription"),
    
    SALEARTICLEFOTOPATHSHORT("SaleArticleFotoPathShort"),
    
    TRANSLATESCIENTIFICNAME("TranslateScientificName"),
    
    SALEARTICLEPRODUCTSPECIFICATIONURL("SaleArticleProductSpecificationUrl"),
    
    SALEARTICLEFOTOPATH("SaleArticleFotoPath"),
    
    TRANSLATEMEATTYPE("TranslateMeatType"),
    
    TRANSLATECOLLAGEN("TranslateCollagen"),
    
    TRANSLATEFAT("TranslateFat"),
    
    TRANSLATEPRODUCTSPECIFICATIONCATEGORIE("TranslateProductSpecificationCategorie"),
    
    TRANSLATEPRODUCTSPECIFICATIONLINE("TranslateProductSpecificationLine"),
    
    TRANSLATECATEGORAAL("TranslateCategoraal"),
    
    TRANSLATEGENERICTEXT("TranslateGenericText"),
    
    TRANSLATEPURCHASEARTICLESPECIFICPRODUCTSPECIFICATIONLINE("TranslatePurchaseArticleSpecificProductSpecificationLine"),
    
    TRANSLATESALEARTICLESPECIFICPRODUCTSPECIFICATIONLINE("TranslateSaleArticleSpecificProductSpecificationLine"),
    
    TRANSLATETYPELABELINGMACHINEINGREDIENTLINES("TranslateTypeLabelingMachineIngredientLines"),
    
    TRANSLATEPRODUCTSPECIFICATIONMAINCATEGORIE("TranslateProductSpecificationMainCategorie"),
    
    TRANSLATEPRODUCTSPECIFICATIONUNIT("TranslateProductSpecificationUnit"),
    
    TRANSLATEPRODUCTSPECIFICATIONFIXEDTEXT("TranslateProductSpecificationFixedText"),
    
    TRANSLATESALEARTICLECUSTOMERDESCRIPTION("TranslateSaleArticleCustomerDescription"),
    
    TRANSLATECAPTUREREGION("TranslateCaptureRegion"),
    
    TRANSLATECOUNTRYNAME("TranslateCountryName"),
    
    TRANSLATEFLEXCOUNTRYNAME("TranslateFlexCountryName"),
    
    TRANSLATECREDITREASON("TranslateCreditReason"),
    
    TRANSLATESALEINVOICESTATUS("TranslateSaleInvoiceStatus"),
    
    TRANSLATETAGNAMESHORT("TranslateTagNameShort"),
    
    TRANSLATETAGNAME("TranslateTagName"),
    
    TRANSLATEPURCHASEARTICLEPACKAGING("TranslatePurchaseArticlePackaging"),
    
    TRANSLATESALEARTICLEPACKAGING("TranslateSaleArticlePackaging"),
    
    SPECIALOFFERTRANSLATION("SpecialOfferTranslation"),
    
    TRANSLATEDELIVERYMETHOD("TranslateDeliveryMethod"),
    
    TRANSLATEPURCHASEARTICLEDESCRIPTION("TranslatePurchaseArticleDescription"),
    
    TRANSLATESALEARTICLEDESCRIPTION("TranslateSaleArticleDescription"),
    
    TRANSLATEFISHINGGEARDESCRIPTION("TranslateFishingGearDescription"),
    
    ORGANIZATIONARTICLE("OrganizationArticle"),
    
    ORGANIZATIONSPECIFICEANCODE("OrganizationSpecificEanCode");

    private String value;

    TypeMinValueEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static TypeMinValueEnum fromValue(String text) {
      for (TypeMinValueEnum b : TypeMinValueEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    public static class Adapter extends TypeAdapter<TypeMinValueEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final TypeMinValueEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public TypeMinValueEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return TypeMinValueEnum.fromValue(String.valueOf(value));
      }
    }
  }

  @SerializedName("typeMinValue")
  private TypeMinValueEnum typeMinValue = null;

  /**
   * Gets or Sets typeMaxValue
   */
  @JsonAdapter(TypeMaxValueEnum.Adapter.class)
  public enum TypeMaxValueEnum {
    NONE("None"),
    
    TRANSLATESALEARTICLEGROUP("TranslateSaleArticleGroup"),
    
    TRANSLATESALEARTICLEMAINGROUP("TranslateSaleArticleMainGroup"),
    
    TRANSLATESALEARTICLESUBGROUP("TranslateSaleArticleSubGroup"),
    
    TRANSLATEBASEMATERIAL("TranslateBaseMaterial"),
    
    TRANSLATEALLERGEN("TranslateAllergen"),
    
    TRANSLATENUTRITIONAL("TranslateNutritional"),
    
    TRANSLATENUTRITIONALUNIT("TranslateNutritionalUnit"),
    
    TRANSLATERECIPE("TranslateRecipe"),
    
    LOTEXTRAFIELD("LotExtraField"),
    
    SALEARTICLEALLERGENDESCRIPTION("SaleArticleAllergenDescription"),
    
    SALEARTICLENUTRITIONALDESCRIPTION("SaleArticleNutritionalDescription"),
    
    SALEARTICLEFOTOPATHSHORT("SaleArticleFotoPathShort"),
    
    TRANSLATESCIENTIFICNAME("TranslateScientificName"),
    
    SALEARTICLEPRODUCTSPECIFICATIONURL("SaleArticleProductSpecificationUrl"),
    
    SALEARTICLEFOTOPATH("SaleArticleFotoPath"),
    
    TRANSLATEMEATTYPE("TranslateMeatType"),
    
    TRANSLATECOLLAGEN("TranslateCollagen"),
    
    TRANSLATEFAT("TranslateFat"),
    
    TRANSLATEPRODUCTSPECIFICATIONCATEGORIE("TranslateProductSpecificationCategorie"),
    
    TRANSLATEPRODUCTSPECIFICATIONLINE("TranslateProductSpecificationLine"),
    
    TRANSLATECATEGORAAL("TranslateCategoraal"),
    
    TRANSLATEGENERICTEXT("TranslateGenericText"),
    
    TRANSLATEPURCHASEARTICLESPECIFICPRODUCTSPECIFICATIONLINE("TranslatePurchaseArticleSpecificProductSpecificationLine"),
    
    TRANSLATESALEARTICLESPECIFICPRODUCTSPECIFICATIONLINE("TranslateSaleArticleSpecificProductSpecificationLine"),
    
    TRANSLATETYPELABELINGMACHINEINGREDIENTLINES("TranslateTypeLabelingMachineIngredientLines"),
    
    TRANSLATEPRODUCTSPECIFICATIONMAINCATEGORIE("TranslateProductSpecificationMainCategorie"),
    
    TRANSLATEPRODUCTSPECIFICATIONUNIT("TranslateProductSpecificationUnit"),
    
    TRANSLATEPRODUCTSPECIFICATIONFIXEDTEXT("TranslateProductSpecificationFixedText"),
    
    TRANSLATESALEARTICLECUSTOMERDESCRIPTION("TranslateSaleArticleCustomerDescription"),
    
    TRANSLATECAPTUREREGION("TranslateCaptureRegion"),
    
    TRANSLATECOUNTRYNAME("TranslateCountryName"),
    
    TRANSLATEFLEXCOUNTRYNAME("TranslateFlexCountryName"),
    
    TRANSLATECREDITREASON("TranslateCreditReason"),
    
    TRANSLATESALEINVOICESTATUS("TranslateSaleInvoiceStatus"),
    
    TRANSLATETAGNAMESHORT("TranslateTagNameShort"),
    
    TRANSLATETAGNAME("TranslateTagName"),
    
    TRANSLATEPURCHASEARTICLEPACKAGING("TranslatePurchaseArticlePackaging"),
    
    TRANSLATESALEARTICLEPACKAGING("TranslateSaleArticlePackaging"),
    
    SPECIALOFFERTRANSLATION("SpecialOfferTranslation"),
    
    TRANSLATEDELIVERYMETHOD("TranslateDeliveryMethod"),
    
    TRANSLATEPURCHASEARTICLEDESCRIPTION("TranslatePurchaseArticleDescription"),
    
    TRANSLATESALEARTICLEDESCRIPTION("TranslateSaleArticleDescription"),
    
    TRANSLATEFISHINGGEARDESCRIPTION("TranslateFishingGearDescription"),
    
    ORGANIZATIONARTICLE("OrganizationArticle"),
    
    ORGANIZATIONSPECIFICEANCODE("OrganizationSpecificEanCode");

    private String value;

    TypeMaxValueEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static TypeMaxValueEnum fromValue(String text) {
      for (TypeMaxValueEnum b : TypeMaxValueEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    public static class Adapter extends TypeAdapter<TypeMaxValueEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final TypeMaxValueEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public TypeMaxValueEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return TypeMaxValueEnum.fromValue(String.valueOf(value));
      }
    }
  }

  @SerializedName("typeMaxValue")
  private TypeMaxValueEnum typeMaxValue = null;

  @SerializedName("allergenIdMinValue")
  private Integer allergenIdMinValue = null;

  @SerializedName("allergenIdMaxValue")
  private Integer allergenIdMaxValue = null;

  @SerializedName("languageIdMinValue")
  private Integer languageIdMinValue = null;

  @SerializedName("languageIdMaxValue")
  private Integer languageIdMaxValue = null;

  @SerializedName("descriptionMinValue")
  private String descriptionMinValue = null;

  @SerializedName("descriptionMaxValue")
  private String descriptionMaxValue = null;

  public GetFilteredAllergenTranslationQuery typeMinValue(TypeMinValueEnum typeMinValue) {
    this.typeMinValue = typeMinValue;
    return this;
  }

   /**
   * Get typeMinValue
   * @return typeMinValue
  **/
  @ApiModelProperty(value = "")
  public TypeMinValueEnum getTypeMinValue() {
    return typeMinValue;
  }

  public void setTypeMinValue(TypeMinValueEnum typeMinValue) {
    this.typeMinValue = typeMinValue;
  }

  public GetFilteredAllergenTranslationQuery typeMaxValue(TypeMaxValueEnum typeMaxValue) {
    this.typeMaxValue = typeMaxValue;
    return this;
  }

   /**
   * Get typeMaxValue
   * @return typeMaxValue
  **/
  @ApiModelProperty(value = "")
  public TypeMaxValueEnum getTypeMaxValue() {
    return typeMaxValue;
  }

  public void setTypeMaxValue(TypeMaxValueEnum typeMaxValue) {
    this.typeMaxValue = typeMaxValue;
  }

  public GetFilteredAllergenTranslationQuery allergenIdMinValue(Integer allergenIdMinValue) {
    this.allergenIdMinValue = allergenIdMinValue;
    return this;
  }

   /**
   * Get allergenIdMinValue
   * @return allergenIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getAllergenIdMinValue() {
    return allergenIdMinValue;
  }

  public void setAllergenIdMinValue(Integer allergenIdMinValue) {
    this.allergenIdMinValue = allergenIdMinValue;
  }

  public GetFilteredAllergenTranslationQuery allergenIdMaxValue(Integer allergenIdMaxValue) {
    this.allergenIdMaxValue = allergenIdMaxValue;
    return this;
  }

   /**
   * Get allergenIdMaxValue
   * @return allergenIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getAllergenIdMaxValue() {
    return allergenIdMaxValue;
  }

  public void setAllergenIdMaxValue(Integer allergenIdMaxValue) {
    this.allergenIdMaxValue = allergenIdMaxValue;
  }

  public GetFilteredAllergenTranslationQuery languageIdMinValue(Integer languageIdMinValue) {
    this.languageIdMinValue = languageIdMinValue;
    return this;
  }

   /**
   * Get languageIdMinValue
   * @return languageIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getLanguageIdMinValue() {
    return languageIdMinValue;
  }

  public void setLanguageIdMinValue(Integer languageIdMinValue) {
    this.languageIdMinValue = languageIdMinValue;
  }

  public GetFilteredAllergenTranslationQuery languageIdMaxValue(Integer languageIdMaxValue) {
    this.languageIdMaxValue = languageIdMaxValue;
    return this;
  }

   /**
   * Get languageIdMaxValue
   * @return languageIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getLanguageIdMaxValue() {
    return languageIdMaxValue;
  }

  public void setLanguageIdMaxValue(Integer languageIdMaxValue) {
    this.languageIdMaxValue = languageIdMaxValue;
  }

  public GetFilteredAllergenTranslationQuery descriptionMinValue(String descriptionMinValue) {
    this.descriptionMinValue = descriptionMinValue;
    return this;
  }

   /**
   * Get descriptionMinValue
   * @return descriptionMinValue
  **/
  @ApiModelProperty(value = "")
  public String getDescriptionMinValue() {
    return descriptionMinValue;
  }

  public void setDescriptionMinValue(String descriptionMinValue) {
    this.descriptionMinValue = descriptionMinValue;
  }

  public GetFilteredAllergenTranslationQuery descriptionMaxValue(String descriptionMaxValue) {
    this.descriptionMaxValue = descriptionMaxValue;
    return this;
  }

   /**
   * Get descriptionMaxValue
   * @return descriptionMaxValue
  **/
  @ApiModelProperty(value = "")
  public String getDescriptionMaxValue() {
    return descriptionMaxValue;
  }

  public void setDescriptionMaxValue(String descriptionMaxValue) {
    this.descriptionMaxValue = descriptionMaxValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetFilteredAllergenTranslationQuery getFilteredAllergenTranslationQuery = (GetFilteredAllergenTranslationQuery) o;
    return Objects.equals(this.typeMinValue, getFilteredAllergenTranslationQuery.typeMinValue) &&
        Objects.equals(this.typeMaxValue, getFilteredAllergenTranslationQuery.typeMaxValue) &&
        Objects.equals(this.allergenIdMinValue, getFilteredAllergenTranslationQuery.allergenIdMinValue) &&
        Objects.equals(this.allergenIdMaxValue, getFilteredAllergenTranslationQuery.allergenIdMaxValue) &&
        Objects.equals(this.languageIdMinValue, getFilteredAllergenTranslationQuery.languageIdMinValue) &&
        Objects.equals(this.languageIdMaxValue, getFilteredAllergenTranslationQuery.languageIdMaxValue) &&
        Objects.equals(this.descriptionMinValue, getFilteredAllergenTranslationQuery.descriptionMinValue) &&
        Objects.equals(this.descriptionMaxValue, getFilteredAllergenTranslationQuery.descriptionMaxValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(typeMinValue, typeMaxValue, allergenIdMinValue, allergenIdMaxValue, languageIdMinValue, languageIdMaxValue, descriptionMinValue, descriptionMaxValue);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetFilteredAllergenTranslationQuery {\n");
    
    sb.append("    typeMinValue: ").append(toIndentedString(typeMinValue)).append("\n");
    sb.append("    typeMaxValue: ").append(toIndentedString(typeMaxValue)).append("\n");
    sb.append("    allergenIdMinValue: ").append(toIndentedString(allergenIdMinValue)).append("\n");
    sb.append("    allergenIdMaxValue: ").append(toIndentedString(allergenIdMaxValue)).append("\n");
    sb.append("    languageIdMinValue: ").append(toIndentedString(languageIdMinValue)).append("\n");
    sb.append("    languageIdMaxValue: ").append(toIndentedString(languageIdMaxValue)).append("\n");
    sb.append("    descriptionMinValue: ").append(toIndentedString(descriptionMinValue)).append("\n");
    sb.append("    descriptionMaxValue: ").append(toIndentedString(descriptionMaxValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

