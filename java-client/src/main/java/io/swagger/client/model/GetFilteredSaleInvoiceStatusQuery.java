/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * GetFilteredSaleInvoiceStatusQuery
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class GetFilteredSaleInvoiceStatusQuery {
  @SerializedName("invoiceStatusIdMinValue")
  private Integer invoiceStatusIdMinValue = null;

  @SerializedName("invoiceStatusIdMaxValue")
  private Integer invoiceStatusIdMaxValue = null;

  @SerializedName("descriptionMinValue")
  private String descriptionMinValue = null;

  @SerializedName("descriptionMaxValue")
  private String descriptionMaxValue = null;

  @SerializedName("isPaymentFulfilledMinValue")
  private Integer isPaymentFulfilledMinValue = null;

  @SerializedName("isPaymentFulfilledMaxValue")
  private Integer isPaymentFulfilledMaxValue = null;

  public GetFilteredSaleInvoiceStatusQuery invoiceStatusIdMinValue(Integer invoiceStatusIdMinValue) {
    this.invoiceStatusIdMinValue = invoiceStatusIdMinValue;
    return this;
  }

   /**
   * Get invoiceStatusIdMinValue
   * @return invoiceStatusIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getInvoiceStatusIdMinValue() {
    return invoiceStatusIdMinValue;
  }

  public void setInvoiceStatusIdMinValue(Integer invoiceStatusIdMinValue) {
    this.invoiceStatusIdMinValue = invoiceStatusIdMinValue;
  }

  public GetFilteredSaleInvoiceStatusQuery invoiceStatusIdMaxValue(Integer invoiceStatusIdMaxValue) {
    this.invoiceStatusIdMaxValue = invoiceStatusIdMaxValue;
    return this;
  }

   /**
   * Get invoiceStatusIdMaxValue
   * @return invoiceStatusIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getInvoiceStatusIdMaxValue() {
    return invoiceStatusIdMaxValue;
  }

  public void setInvoiceStatusIdMaxValue(Integer invoiceStatusIdMaxValue) {
    this.invoiceStatusIdMaxValue = invoiceStatusIdMaxValue;
  }

  public GetFilteredSaleInvoiceStatusQuery descriptionMinValue(String descriptionMinValue) {
    this.descriptionMinValue = descriptionMinValue;
    return this;
  }

   /**
   * Get descriptionMinValue
   * @return descriptionMinValue
  **/
  @ApiModelProperty(value = "")
  public String getDescriptionMinValue() {
    return descriptionMinValue;
  }

  public void setDescriptionMinValue(String descriptionMinValue) {
    this.descriptionMinValue = descriptionMinValue;
  }

  public GetFilteredSaleInvoiceStatusQuery descriptionMaxValue(String descriptionMaxValue) {
    this.descriptionMaxValue = descriptionMaxValue;
    return this;
  }

   /**
   * Get descriptionMaxValue
   * @return descriptionMaxValue
  **/
  @ApiModelProperty(value = "")
  public String getDescriptionMaxValue() {
    return descriptionMaxValue;
  }

  public void setDescriptionMaxValue(String descriptionMaxValue) {
    this.descriptionMaxValue = descriptionMaxValue;
  }

  public GetFilteredSaleInvoiceStatusQuery isPaymentFulfilledMinValue(Integer isPaymentFulfilledMinValue) {
    this.isPaymentFulfilledMinValue = isPaymentFulfilledMinValue;
    return this;
  }

   /**
   * Get isPaymentFulfilledMinValue
   * @return isPaymentFulfilledMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getIsPaymentFulfilledMinValue() {
    return isPaymentFulfilledMinValue;
  }

  public void setIsPaymentFulfilledMinValue(Integer isPaymentFulfilledMinValue) {
    this.isPaymentFulfilledMinValue = isPaymentFulfilledMinValue;
  }

  public GetFilteredSaleInvoiceStatusQuery isPaymentFulfilledMaxValue(Integer isPaymentFulfilledMaxValue) {
    this.isPaymentFulfilledMaxValue = isPaymentFulfilledMaxValue;
    return this;
  }

   /**
   * Get isPaymentFulfilledMaxValue
   * @return isPaymentFulfilledMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getIsPaymentFulfilledMaxValue() {
    return isPaymentFulfilledMaxValue;
  }

  public void setIsPaymentFulfilledMaxValue(Integer isPaymentFulfilledMaxValue) {
    this.isPaymentFulfilledMaxValue = isPaymentFulfilledMaxValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetFilteredSaleInvoiceStatusQuery getFilteredSaleInvoiceStatusQuery = (GetFilteredSaleInvoiceStatusQuery) o;
    return Objects.equals(this.invoiceStatusIdMinValue, getFilteredSaleInvoiceStatusQuery.invoiceStatusIdMinValue) &&
        Objects.equals(this.invoiceStatusIdMaxValue, getFilteredSaleInvoiceStatusQuery.invoiceStatusIdMaxValue) &&
        Objects.equals(this.descriptionMinValue, getFilteredSaleInvoiceStatusQuery.descriptionMinValue) &&
        Objects.equals(this.descriptionMaxValue, getFilteredSaleInvoiceStatusQuery.descriptionMaxValue) &&
        Objects.equals(this.isPaymentFulfilledMinValue, getFilteredSaleInvoiceStatusQuery.isPaymentFulfilledMinValue) &&
        Objects.equals(this.isPaymentFulfilledMaxValue, getFilteredSaleInvoiceStatusQuery.isPaymentFulfilledMaxValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(invoiceStatusIdMinValue, invoiceStatusIdMaxValue, descriptionMinValue, descriptionMaxValue, isPaymentFulfilledMinValue, isPaymentFulfilledMaxValue);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetFilteredSaleInvoiceStatusQuery {\n");
    
    sb.append("    invoiceStatusIdMinValue: ").append(toIndentedString(invoiceStatusIdMinValue)).append("\n");
    sb.append("    invoiceStatusIdMaxValue: ").append(toIndentedString(invoiceStatusIdMaxValue)).append("\n");
    sb.append("    descriptionMinValue: ").append(toIndentedString(descriptionMinValue)).append("\n");
    sb.append("    descriptionMaxValue: ").append(toIndentedString(descriptionMaxValue)).append("\n");
    sb.append("    isPaymentFulfilledMinValue: ").append(toIndentedString(isPaymentFulfilledMinValue)).append("\n");
    sb.append("    isPaymentFulfilledMaxValue: ").append(toIndentedString(isPaymentFulfilledMaxValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

