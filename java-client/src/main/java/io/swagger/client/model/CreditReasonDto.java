/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * CreditReasonDto
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class CreditReasonDto {
  @SerializedName("creditReasonId")
  private Integer creditReasonId = null;

  @SerializedName("number")
  private Integer number = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("operations")
  private List<String> operations = null;

  public CreditReasonDto creditReasonId(Integer creditReasonId) {
    this.creditReasonId = creditReasonId;
    return this;
  }

   /**
   * Get creditReasonId
   * @return creditReasonId
  **/
  @ApiModelProperty(required = true, value = "")
  public Integer getCreditReasonId() {
    return creditReasonId;
  }

  public void setCreditReasonId(Integer creditReasonId) {
    this.creditReasonId = creditReasonId;
  }

  public CreditReasonDto number(Integer number) {
    this.number = number;
    return this;
  }

   /**
   * Get number
   * @return number
  **/
  @ApiModelProperty(value = "")
  public Integer getNumber() {
    return number;
  }

  public void setNumber(Integer number) {
    this.number = number;
  }

  public CreditReasonDto name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

   /**
   * Get operations
   * @return operations
  **/
  @ApiModelProperty(value = "")
  public List<String> getOperations() {
    return operations;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreditReasonDto creditReasonDto = (CreditReasonDto) o;
    return Objects.equals(this.creditReasonId, creditReasonDto.creditReasonId) &&
        Objects.equals(this.number, creditReasonDto.number) &&
        Objects.equals(this.name, creditReasonDto.name) &&
        Objects.equals(this.operations, creditReasonDto.operations);
  }

  @Override
  public int hashCode() {
    return Objects.hash(creditReasonId, number, name, operations);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreditReasonDto {\n");
    
    sb.append("    creditReasonId: ").append(toIndentedString(creditReasonId)).append("\n");
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    operations: ").append(toIndentedString(operations)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

