/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * GetFilteredProductionTaskOutputSubLineQuery
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class GetFilteredProductionTaskOutputSubLineQuery {
  @SerializedName("productionTaskIdMinValue")
  private Integer productionTaskIdMinValue = null;

  @SerializedName("productionTaskIdMaxValue")
  private Integer productionTaskIdMaxValue = null;

  @SerializedName("productionTaskOutputLineIdMinValue")
  private Integer productionTaskOutputLineIdMinValue = null;

  @SerializedName("productionTaskOutputLineIdMaxValue")
  private Integer productionTaskOutputLineIdMaxValue = null;

  @SerializedName("chargeIdMinValue")
  private Integer chargeIdMinValue = null;

  @SerializedName("chargeIdMaxValue")
  private Integer chargeIdMaxValue = null;

  @SerializedName("batchIdMinValue")
  private Integer batchIdMinValue = null;

  @SerializedName("batchIdMaxValue")
  private Integer batchIdMaxValue = null;

  @SerializedName("coupledSaleOrderSubLineIdMinValue")
  private Integer coupledSaleOrderSubLineIdMinValue = null;

  @SerializedName("coupledSaleOrderSubLineIdMaxValue")
  private Integer coupledSaleOrderSubLineIdMaxValue = null;

  @SerializedName("productionTaskOutputSubLineIdMinValue")
  private Integer productionTaskOutputSubLineIdMinValue = null;

  @SerializedName("productionTaskOutputSubLineIdMaxValue")
  private Integer productionTaskOutputSubLineIdMaxValue = null;

  public GetFilteredProductionTaskOutputSubLineQuery productionTaskIdMinValue(Integer productionTaskIdMinValue) {
    this.productionTaskIdMinValue = productionTaskIdMinValue;
    return this;
  }

   /**
   * Get productionTaskIdMinValue
   * @return productionTaskIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getProductionTaskIdMinValue() {
    return productionTaskIdMinValue;
  }

  public void setProductionTaskIdMinValue(Integer productionTaskIdMinValue) {
    this.productionTaskIdMinValue = productionTaskIdMinValue;
  }

  public GetFilteredProductionTaskOutputSubLineQuery productionTaskIdMaxValue(Integer productionTaskIdMaxValue) {
    this.productionTaskIdMaxValue = productionTaskIdMaxValue;
    return this;
  }

   /**
   * Get productionTaskIdMaxValue
   * @return productionTaskIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getProductionTaskIdMaxValue() {
    return productionTaskIdMaxValue;
  }

  public void setProductionTaskIdMaxValue(Integer productionTaskIdMaxValue) {
    this.productionTaskIdMaxValue = productionTaskIdMaxValue;
  }

  public GetFilteredProductionTaskOutputSubLineQuery productionTaskOutputLineIdMinValue(Integer productionTaskOutputLineIdMinValue) {
    this.productionTaskOutputLineIdMinValue = productionTaskOutputLineIdMinValue;
    return this;
  }

   /**
   * Get productionTaskOutputLineIdMinValue
   * @return productionTaskOutputLineIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getProductionTaskOutputLineIdMinValue() {
    return productionTaskOutputLineIdMinValue;
  }

  public void setProductionTaskOutputLineIdMinValue(Integer productionTaskOutputLineIdMinValue) {
    this.productionTaskOutputLineIdMinValue = productionTaskOutputLineIdMinValue;
  }

  public GetFilteredProductionTaskOutputSubLineQuery productionTaskOutputLineIdMaxValue(Integer productionTaskOutputLineIdMaxValue) {
    this.productionTaskOutputLineIdMaxValue = productionTaskOutputLineIdMaxValue;
    return this;
  }

   /**
   * Get productionTaskOutputLineIdMaxValue
   * @return productionTaskOutputLineIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getProductionTaskOutputLineIdMaxValue() {
    return productionTaskOutputLineIdMaxValue;
  }

  public void setProductionTaskOutputLineIdMaxValue(Integer productionTaskOutputLineIdMaxValue) {
    this.productionTaskOutputLineIdMaxValue = productionTaskOutputLineIdMaxValue;
  }

  public GetFilteredProductionTaskOutputSubLineQuery chargeIdMinValue(Integer chargeIdMinValue) {
    this.chargeIdMinValue = chargeIdMinValue;
    return this;
  }

   /**
   * Get chargeIdMinValue
   * @return chargeIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getChargeIdMinValue() {
    return chargeIdMinValue;
  }

  public void setChargeIdMinValue(Integer chargeIdMinValue) {
    this.chargeIdMinValue = chargeIdMinValue;
  }

  public GetFilteredProductionTaskOutputSubLineQuery chargeIdMaxValue(Integer chargeIdMaxValue) {
    this.chargeIdMaxValue = chargeIdMaxValue;
    return this;
  }

   /**
   * Get chargeIdMaxValue
   * @return chargeIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getChargeIdMaxValue() {
    return chargeIdMaxValue;
  }

  public void setChargeIdMaxValue(Integer chargeIdMaxValue) {
    this.chargeIdMaxValue = chargeIdMaxValue;
  }

  public GetFilteredProductionTaskOutputSubLineQuery batchIdMinValue(Integer batchIdMinValue) {
    this.batchIdMinValue = batchIdMinValue;
    return this;
  }

   /**
   * Get batchIdMinValue
   * @return batchIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getBatchIdMinValue() {
    return batchIdMinValue;
  }

  public void setBatchIdMinValue(Integer batchIdMinValue) {
    this.batchIdMinValue = batchIdMinValue;
  }

  public GetFilteredProductionTaskOutputSubLineQuery batchIdMaxValue(Integer batchIdMaxValue) {
    this.batchIdMaxValue = batchIdMaxValue;
    return this;
  }

   /**
   * Get batchIdMaxValue
   * @return batchIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getBatchIdMaxValue() {
    return batchIdMaxValue;
  }

  public void setBatchIdMaxValue(Integer batchIdMaxValue) {
    this.batchIdMaxValue = batchIdMaxValue;
  }

  public GetFilteredProductionTaskOutputSubLineQuery coupledSaleOrderSubLineIdMinValue(Integer coupledSaleOrderSubLineIdMinValue) {
    this.coupledSaleOrderSubLineIdMinValue = coupledSaleOrderSubLineIdMinValue;
    return this;
  }

   /**
   * Get coupledSaleOrderSubLineIdMinValue
   * @return coupledSaleOrderSubLineIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getCoupledSaleOrderSubLineIdMinValue() {
    return coupledSaleOrderSubLineIdMinValue;
  }

  public void setCoupledSaleOrderSubLineIdMinValue(Integer coupledSaleOrderSubLineIdMinValue) {
    this.coupledSaleOrderSubLineIdMinValue = coupledSaleOrderSubLineIdMinValue;
  }

  public GetFilteredProductionTaskOutputSubLineQuery coupledSaleOrderSubLineIdMaxValue(Integer coupledSaleOrderSubLineIdMaxValue) {
    this.coupledSaleOrderSubLineIdMaxValue = coupledSaleOrderSubLineIdMaxValue;
    return this;
  }

   /**
   * Get coupledSaleOrderSubLineIdMaxValue
   * @return coupledSaleOrderSubLineIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getCoupledSaleOrderSubLineIdMaxValue() {
    return coupledSaleOrderSubLineIdMaxValue;
  }

  public void setCoupledSaleOrderSubLineIdMaxValue(Integer coupledSaleOrderSubLineIdMaxValue) {
    this.coupledSaleOrderSubLineIdMaxValue = coupledSaleOrderSubLineIdMaxValue;
  }

  public GetFilteredProductionTaskOutputSubLineQuery productionTaskOutputSubLineIdMinValue(Integer productionTaskOutputSubLineIdMinValue) {
    this.productionTaskOutputSubLineIdMinValue = productionTaskOutputSubLineIdMinValue;
    return this;
  }

   /**
   * Get productionTaskOutputSubLineIdMinValue
   * @return productionTaskOutputSubLineIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getProductionTaskOutputSubLineIdMinValue() {
    return productionTaskOutputSubLineIdMinValue;
  }

  public void setProductionTaskOutputSubLineIdMinValue(Integer productionTaskOutputSubLineIdMinValue) {
    this.productionTaskOutputSubLineIdMinValue = productionTaskOutputSubLineIdMinValue;
  }

  public GetFilteredProductionTaskOutputSubLineQuery productionTaskOutputSubLineIdMaxValue(Integer productionTaskOutputSubLineIdMaxValue) {
    this.productionTaskOutputSubLineIdMaxValue = productionTaskOutputSubLineIdMaxValue;
    return this;
  }

   /**
   * Get productionTaskOutputSubLineIdMaxValue
   * @return productionTaskOutputSubLineIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getProductionTaskOutputSubLineIdMaxValue() {
    return productionTaskOutputSubLineIdMaxValue;
  }

  public void setProductionTaskOutputSubLineIdMaxValue(Integer productionTaskOutputSubLineIdMaxValue) {
    this.productionTaskOutputSubLineIdMaxValue = productionTaskOutputSubLineIdMaxValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetFilteredProductionTaskOutputSubLineQuery getFilteredProductionTaskOutputSubLineQuery = (GetFilteredProductionTaskOutputSubLineQuery) o;
    return Objects.equals(this.productionTaskIdMinValue, getFilteredProductionTaskOutputSubLineQuery.productionTaskIdMinValue) &&
        Objects.equals(this.productionTaskIdMaxValue, getFilteredProductionTaskOutputSubLineQuery.productionTaskIdMaxValue) &&
        Objects.equals(this.productionTaskOutputLineIdMinValue, getFilteredProductionTaskOutputSubLineQuery.productionTaskOutputLineIdMinValue) &&
        Objects.equals(this.productionTaskOutputLineIdMaxValue, getFilteredProductionTaskOutputSubLineQuery.productionTaskOutputLineIdMaxValue) &&
        Objects.equals(this.chargeIdMinValue, getFilteredProductionTaskOutputSubLineQuery.chargeIdMinValue) &&
        Objects.equals(this.chargeIdMaxValue, getFilteredProductionTaskOutputSubLineQuery.chargeIdMaxValue) &&
        Objects.equals(this.batchIdMinValue, getFilteredProductionTaskOutputSubLineQuery.batchIdMinValue) &&
        Objects.equals(this.batchIdMaxValue, getFilteredProductionTaskOutputSubLineQuery.batchIdMaxValue) &&
        Objects.equals(this.coupledSaleOrderSubLineIdMinValue, getFilteredProductionTaskOutputSubLineQuery.coupledSaleOrderSubLineIdMinValue) &&
        Objects.equals(this.coupledSaleOrderSubLineIdMaxValue, getFilteredProductionTaskOutputSubLineQuery.coupledSaleOrderSubLineIdMaxValue) &&
        Objects.equals(this.productionTaskOutputSubLineIdMinValue, getFilteredProductionTaskOutputSubLineQuery.productionTaskOutputSubLineIdMinValue) &&
        Objects.equals(this.productionTaskOutputSubLineIdMaxValue, getFilteredProductionTaskOutputSubLineQuery.productionTaskOutputSubLineIdMaxValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(productionTaskIdMinValue, productionTaskIdMaxValue, productionTaskOutputLineIdMinValue, productionTaskOutputLineIdMaxValue, chargeIdMinValue, chargeIdMaxValue, batchIdMinValue, batchIdMaxValue, coupledSaleOrderSubLineIdMinValue, coupledSaleOrderSubLineIdMaxValue, productionTaskOutputSubLineIdMinValue, productionTaskOutputSubLineIdMaxValue);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetFilteredProductionTaskOutputSubLineQuery {\n");
    
    sb.append("    productionTaskIdMinValue: ").append(toIndentedString(productionTaskIdMinValue)).append("\n");
    sb.append("    productionTaskIdMaxValue: ").append(toIndentedString(productionTaskIdMaxValue)).append("\n");
    sb.append("    productionTaskOutputLineIdMinValue: ").append(toIndentedString(productionTaskOutputLineIdMinValue)).append("\n");
    sb.append("    productionTaskOutputLineIdMaxValue: ").append(toIndentedString(productionTaskOutputLineIdMaxValue)).append("\n");
    sb.append("    chargeIdMinValue: ").append(toIndentedString(chargeIdMinValue)).append("\n");
    sb.append("    chargeIdMaxValue: ").append(toIndentedString(chargeIdMaxValue)).append("\n");
    sb.append("    batchIdMinValue: ").append(toIndentedString(batchIdMinValue)).append("\n");
    sb.append("    batchIdMaxValue: ").append(toIndentedString(batchIdMaxValue)).append("\n");
    sb.append("    coupledSaleOrderSubLineIdMinValue: ").append(toIndentedString(coupledSaleOrderSubLineIdMinValue)).append("\n");
    sb.append("    coupledSaleOrderSubLineIdMaxValue: ").append(toIndentedString(coupledSaleOrderSubLineIdMaxValue)).append("\n");
    sb.append("    productionTaskOutputSubLineIdMinValue: ").append(toIndentedString(productionTaskOutputSubLineIdMinValue)).append("\n");
    sb.append("    productionTaskOutputSubLineIdMaxValue: ").append(toIndentedString(productionTaskOutputSubLineIdMaxValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

