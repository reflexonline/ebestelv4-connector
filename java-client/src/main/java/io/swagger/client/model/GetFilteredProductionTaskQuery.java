/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.threeten.bp.OffsetDateTime;

/**
 * GetFilteredProductionTaskQuery
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class GetFilteredProductionTaskQuery {
  /**
   * Gets or Sets statusMinValue
   */
  @JsonAdapter(StatusMinValueEnum.Adapter.class)
  public enum StatusMinValueEnum {
    NONE("None"),
    
    ACTIVE("Active"),
    
    HISTORIC("Historic");

    private String value;

    StatusMinValueEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static StatusMinValueEnum fromValue(String text) {
      for (StatusMinValueEnum b : StatusMinValueEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    public static class Adapter extends TypeAdapter<StatusMinValueEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final StatusMinValueEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public StatusMinValueEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return StatusMinValueEnum.fromValue(String.valueOf(value));
      }
    }
  }

  @SerializedName("statusMinValue")
  private StatusMinValueEnum statusMinValue = null;

  /**
   * Gets or Sets statusMaxValue
   */
  @JsonAdapter(StatusMaxValueEnum.Adapter.class)
  public enum StatusMaxValueEnum {
    NONE("None"),
    
    ACTIVE("Active"),
    
    HISTORIC("Historic");

    private String value;

    StatusMaxValueEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static StatusMaxValueEnum fromValue(String text) {
      for (StatusMaxValueEnum b : StatusMaxValueEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    public static class Adapter extends TypeAdapter<StatusMaxValueEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final StatusMaxValueEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public StatusMaxValueEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return StatusMaxValueEnum.fromValue(String.valueOf(value));
      }
    }
  }

  @SerializedName("statusMaxValue")
  private StatusMaxValueEnum statusMaxValue = null;

  @SerializedName("recipeGroupIdMinValue")
  private Integer recipeGroupIdMinValue = null;

  @SerializedName("recipeGroupIdMaxValue")
  private Integer recipeGroupIdMaxValue = null;

  @SerializedName("productionTaskIdMinValue")
  private Integer productionTaskIdMinValue = null;

  @SerializedName("productionTaskIdMaxValue")
  private Integer productionTaskIdMaxValue = null;

  @SerializedName("currentProductionStepIdMinValue")
  private Integer currentProductionStepIdMinValue = null;

  @SerializedName("currentProductionStepIdMaxValue")
  private Integer currentProductionStepIdMaxValue = null;

  @SerializedName("partijMinValue")
  private Integer partijMinValue = null;

  @SerializedName("partijMaxValue")
  private Integer partijMaxValue = null;

  @SerializedName("startDateMinValue")
  private OffsetDateTime startDateMinValue = null;

  @SerializedName("startDateMaxValue")
  private OffsetDateTime startDateMaxValue = null;

  @SerializedName("endDateMinValue")
  private OffsetDateTime endDateMinValue = null;

  @SerializedName("endDateMaxValue")
  private OffsetDateTime endDateMaxValue = null;

  @SerializedName("bestBeforeDateMinValue")
  private OffsetDateTime bestBeforeDateMinValue = null;

  @SerializedName("bestBeforeDateMaxValue")
  private OffsetDateTime bestBeforeDateMaxValue = null;

  @SerializedName("articleIdMinValue")
  private Integer articleIdMinValue = null;

  @SerializedName("articleIdMaxValue")
  private Integer articleIdMaxValue = null;

  @SerializedName("descriptionMinValue")
  private String descriptionMinValue = null;

  @SerializedName("descriptionMaxValue")
  private String descriptionMaxValue = null;

  @SerializedName("producingMultiBranchIdMinValue")
  private Integer producingMultiBranchIdMinValue = null;

  @SerializedName("producingMultiBranchIdMaxValue")
  private Integer producingMultiBranchIdMaxValue = null;

  @SerializedName("productionSequenceMinValue")
  private Integer productionSequenceMinValue = null;

  @SerializedName("productionSequenceMaxValue")
  private Integer productionSequenceMaxValue = null;

  @SerializedName("useProductionPlanningMinValue")
  private Boolean useProductionPlanningMinValue = null;

  @SerializedName("useProductionPlanningMaxValue")
  private Boolean useProductionPlanningMaxValue = null;

  @SerializedName("packingDescriptionMinValue")
  private String packingDescriptionMinValue = null;

  @SerializedName("packingDescriptionMaxValue")
  private String packingDescriptionMaxValue = null;

  @SerializedName("closedByUserIdMinValue")
  private Integer closedByUserIdMinValue = null;

  @SerializedName("closedByUserIdMaxValue")
  private Integer closedByUserIdMaxValue = null;

  public GetFilteredProductionTaskQuery statusMinValue(StatusMinValueEnum statusMinValue) {
    this.statusMinValue = statusMinValue;
    return this;
  }

   /**
   * Get statusMinValue
   * @return statusMinValue
  **/
  @ApiModelProperty(value = "")
  public StatusMinValueEnum getStatusMinValue() {
    return statusMinValue;
  }

  public void setStatusMinValue(StatusMinValueEnum statusMinValue) {
    this.statusMinValue = statusMinValue;
  }

  public GetFilteredProductionTaskQuery statusMaxValue(StatusMaxValueEnum statusMaxValue) {
    this.statusMaxValue = statusMaxValue;
    return this;
  }

   /**
   * Get statusMaxValue
   * @return statusMaxValue
  **/
  @ApiModelProperty(value = "")
  public StatusMaxValueEnum getStatusMaxValue() {
    return statusMaxValue;
  }

  public void setStatusMaxValue(StatusMaxValueEnum statusMaxValue) {
    this.statusMaxValue = statusMaxValue;
  }

  public GetFilteredProductionTaskQuery recipeGroupIdMinValue(Integer recipeGroupIdMinValue) {
    this.recipeGroupIdMinValue = recipeGroupIdMinValue;
    return this;
  }

   /**
   * Get recipeGroupIdMinValue
   * @return recipeGroupIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getRecipeGroupIdMinValue() {
    return recipeGroupIdMinValue;
  }

  public void setRecipeGroupIdMinValue(Integer recipeGroupIdMinValue) {
    this.recipeGroupIdMinValue = recipeGroupIdMinValue;
  }

  public GetFilteredProductionTaskQuery recipeGroupIdMaxValue(Integer recipeGroupIdMaxValue) {
    this.recipeGroupIdMaxValue = recipeGroupIdMaxValue;
    return this;
  }

   /**
   * Get recipeGroupIdMaxValue
   * @return recipeGroupIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getRecipeGroupIdMaxValue() {
    return recipeGroupIdMaxValue;
  }

  public void setRecipeGroupIdMaxValue(Integer recipeGroupIdMaxValue) {
    this.recipeGroupIdMaxValue = recipeGroupIdMaxValue;
  }

  public GetFilteredProductionTaskQuery productionTaskIdMinValue(Integer productionTaskIdMinValue) {
    this.productionTaskIdMinValue = productionTaskIdMinValue;
    return this;
  }

   /**
   * Get productionTaskIdMinValue
   * @return productionTaskIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getProductionTaskIdMinValue() {
    return productionTaskIdMinValue;
  }

  public void setProductionTaskIdMinValue(Integer productionTaskIdMinValue) {
    this.productionTaskIdMinValue = productionTaskIdMinValue;
  }

  public GetFilteredProductionTaskQuery productionTaskIdMaxValue(Integer productionTaskIdMaxValue) {
    this.productionTaskIdMaxValue = productionTaskIdMaxValue;
    return this;
  }

   /**
   * Get productionTaskIdMaxValue
   * @return productionTaskIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getProductionTaskIdMaxValue() {
    return productionTaskIdMaxValue;
  }

  public void setProductionTaskIdMaxValue(Integer productionTaskIdMaxValue) {
    this.productionTaskIdMaxValue = productionTaskIdMaxValue;
  }

  public GetFilteredProductionTaskQuery currentProductionStepIdMinValue(Integer currentProductionStepIdMinValue) {
    this.currentProductionStepIdMinValue = currentProductionStepIdMinValue;
    return this;
  }

   /**
   * Get currentProductionStepIdMinValue
   * @return currentProductionStepIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getCurrentProductionStepIdMinValue() {
    return currentProductionStepIdMinValue;
  }

  public void setCurrentProductionStepIdMinValue(Integer currentProductionStepIdMinValue) {
    this.currentProductionStepIdMinValue = currentProductionStepIdMinValue;
  }

  public GetFilteredProductionTaskQuery currentProductionStepIdMaxValue(Integer currentProductionStepIdMaxValue) {
    this.currentProductionStepIdMaxValue = currentProductionStepIdMaxValue;
    return this;
  }

   /**
   * Get currentProductionStepIdMaxValue
   * @return currentProductionStepIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getCurrentProductionStepIdMaxValue() {
    return currentProductionStepIdMaxValue;
  }

  public void setCurrentProductionStepIdMaxValue(Integer currentProductionStepIdMaxValue) {
    this.currentProductionStepIdMaxValue = currentProductionStepIdMaxValue;
  }

  public GetFilteredProductionTaskQuery partijMinValue(Integer partijMinValue) {
    this.partijMinValue = partijMinValue;
    return this;
  }

   /**
   * Get partijMinValue
   * @return partijMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getPartijMinValue() {
    return partijMinValue;
  }

  public void setPartijMinValue(Integer partijMinValue) {
    this.partijMinValue = partijMinValue;
  }

  public GetFilteredProductionTaskQuery partijMaxValue(Integer partijMaxValue) {
    this.partijMaxValue = partijMaxValue;
    return this;
  }

   /**
   * Get partijMaxValue
   * @return partijMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getPartijMaxValue() {
    return partijMaxValue;
  }

  public void setPartijMaxValue(Integer partijMaxValue) {
    this.partijMaxValue = partijMaxValue;
  }

  public GetFilteredProductionTaskQuery startDateMinValue(OffsetDateTime startDateMinValue) {
    this.startDateMinValue = startDateMinValue;
    return this;
  }

   /**
   * Get startDateMinValue
   * @return startDateMinValue
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getStartDateMinValue() {
    return startDateMinValue;
  }

  public void setStartDateMinValue(OffsetDateTime startDateMinValue) {
    this.startDateMinValue = startDateMinValue;
  }

  public GetFilteredProductionTaskQuery startDateMaxValue(OffsetDateTime startDateMaxValue) {
    this.startDateMaxValue = startDateMaxValue;
    return this;
  }

   /**
   * Get startDateMaxValue
   * @return startDateMaxValue
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getStartDateMaxValue() {
    return startDateMaxValue;
  }

  public void setStartDateMaxValue(OffsetDateTime startDateMaxValue) {
    this.startDateMaxValue = startDateMaxValue;
  }

  public GetFilteredProductionTaskQuery endDateMinValue(OffsetDateTime endDateMinValue) {
    this.endDateMinValue = endDateMinValue;
    return this;
  }

   /**
   * Get endDateMinValue
   * @return endDateMinValue
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getEndDateMinValue() {
    return endDateMinValue;
  }

  public void setEndDateMinValue(OffsetDateTime endDateMinValue) {
    this.endDateMinValue = endDateMinValue;
  }

  public GetFilteredProductionTaskQuery endDateMaxValue(OffsetDateTime endDateMaxValue) {
    this.endDateMaxValue = endDateMaxValue;
    return this;
  }

   /**
   * Get endDateMaxValue
   * @return endDateMaxValue
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getEndDateMaxValue() {
    return endDateMaxValue;
  }

  public void setEndDateMaxValue(OffsetDateTime endDateMaxValue) {
    this.endDateMaxValue = endDateMaxValue;
  }

  public GetFilteredProductionTaskQuery bestBeforeDateMinValue(OffsetDateTime bestBeforeDateMinValue) {
    this.bestBeforeDateMinValue = bestBeforeDateMinValue;
    return this;
  }

   /**
   * Get bestBeforeDateMinValue
   * @return bestBeforeDateMinValue
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getBestBeforeDateMinValue() {
    return bestBeforeDateMinValue;
  }

  public void setBestBeforeDateMinValue(OffsetDateTime bestBeforeDateMinValue) {
    this.bestBeforeDateMinValue = bestBeforeDateMinValue;
  }

  public GetFilteredProductionTaskQuery bestBeforeDateMaxValue(OffsetDateTime bestBeforeDateMaxValue) {
    this.bestBeforeDateMaxValue = bestBeforeDateMaxValue;
    return this;
  }

   /**
   * Get bestBeforeDateMaxValue
   * @return bestBeforeDateMaxValue
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getBestBeforeDateMaxValue() {
    return bestBeforeDateMaxValue;
  }

  public void setBestBeforeDateMaxValue(OffsetDateTime bestBeforeDateMaxValue) {
    this.bestBeforeDateMaxValue = bestBeforeDateMaxValue;
  }

  public GetFilteredProductionTaskQuery articleIdMinValue(Integer articleIdMinValue) {
    this.articleIdMinValue = articleIdMinValue;
    return this;
  }

   /**
   * Get articleIdMinValue
   * @return articleIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getArticleIdMinValue() {
    return articleIdMinValue;
  }

  public void setArticleIdMinValue(Integer articleIdMinValue) {
    this.articleIdMinValue = articleIdMinValue;
  }

  public GetFilteredProductionTaskQuery articleIdMaxValue(Integer articleIdMaxValue) {
    this.articleIdMaxValue = articleIdMaxValue;
    return this;
  }

   /**
   * Get articleIdMaxValue
   * @return articleIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getArticleIdMaxValue() {
    return articleIdMaxValue;
  }

  public void setArticleIdMaxValue(Integer articleIdMaxValue) {
    this.articleIdMaxValue = articleIdMaxValue;
  }

  public GetFilteredProductionTaskQuery descriptionMinValue(String descriptionMinValue) {
    this.descriptionMinValue = descriptionMinValue;
    return this;
  }

   /**
   * Get descriptionMinValue
   * @return descriptionMinValue
  **/
  @ApiModelProperty(value = "")
  public String getDescriptionMinValue() {
    return descriptionMinValue;
  }

  public void setDescriptionMinValue(String descriptionMinValue) {
    this.descriptionMinValue = descriptionMinValue;
  }

  public GetFilteredProductionTaskQuery descriptionMaxValue(String descriptionMaxValue) {
    this.descriptionMaxValue = descriptionMaxValue;
    return this;
  }

   /**
   * Get descriptionMaxValue
   * @return descriptionMaxValue
  **/
  @ApiModelProperty(value = "")
  public String getDescriptionMaxValue() {
    return descriptionMaxValue;
  }

  public void setDescriptionMaxValue(String descriptionMaxValue) {
    this.descriptionMaxValue = descriptionMaxValue;
  }

  public GetFilteredProductionTaskQuery producingMultiBranchIdMinValue(Integer producingMultiBranchIdMinValue) {
    this.producingMultiBranchIdMinValue = producingMultiBranchIdMinValue;
    return this;
  }

   /**
   * Get producingMultiBranchIdMinValue
   * @return producingMultiBranchIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getProducingMultiBranchIdMinValue() {
    return producingMultiBranchIdMinValue;
  }

  public void setProducingMultiBranchIdMinValue(Integer producingMultiBranchIdMinValue) {
    this.producingMultiBranchIdMinValue = producingMultiBranchIdMinValue;
  }

  public GetFilteredProductionTaskQuery producingMultiBranchIdMaxValue(Integer producingMultiBranchIdMaxValue) {
    this.producingMultiBranchIdMaxValue = producingMultiBranchIdMaxValue;
    return this;
  }

   /**
   * Get producingMultiBranchIdMaxValue
   * @return producingMultiBranchIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getProducingMultiBranchIdMaxValue() {
    return producingMultiBranchIdMaxValue;
  }

  public void setProducingMultiBranchIdMaxValue(Integer producingMultiBranchIdMaxValue) {
    this.producingMultiBranchIdMaxValue = producingMultiBranchIdMaxValue;
  }

  public GetFilteredProductionTaskQuery productionSequenceMinValue(Integer productionSequenceMinValue) {
    this.productionSequenceMinValue = productionSequenceMinValue;
    return this;
  }

   /**
   * Get productionSequenceMinValue
   * @return productionSequenceMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getProductionSequenceMinValue() {
    return productionSequenceMinValue;
  }

  public void setProductionSequenceMinValue(Integer productionSequenceMinValue) {
    this.productionSequenceMinValue = productionSequenceMinValue;
  }

  public GetFilteredProductionTaskQuery productionSequenceMaxValue(Integer productionSequenceMaxValue) {
    this.productionSequenceMaxValue = productionSequenceMaxValue;
    return this;
  }

   /**
   * Get productionSequenceMaxValue
   * @return productionSequenceMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getProductionSequenceMaxValue() {
    return productionSequenceMaxValue;
  }

  public void setProductionSequenceMaxValue(Integer productionSequenceMaxValue) {
    this.productionSequenceMaxValue = productionSequenceMaxValue;
  }

  public GetFilteredProductionTaskQuery useProductionPlanningMinValue(Boolean useProductionPlanningMinValue) {
    this.useProductionPlanningMinValue = useProductionPlanningMinValue;
    return this;
  }

   /**
   * Get useProductionPlanningMinValue
   * @return useProductionPlanningMinValue
  **/
  @ApiModelProperty(value = "")
  public Boolean isUseProductionPlanningMinValue() {
    return useProductionPlanningMinValue;
  }

  public void setUseProductionPlanningMinValue(Boolean useProductionPlanningMinValue) {
    this.useProductionPlanningMinValue = useProductionPlanningMinValue;
  }

  public GetFilteredProductionTaskQuery useProductionPlanningMaxValue(Boolean useProductionPlanningMaxValue) {
    this.useProductionPlanningMaxValue = useProductionPlanningMaxValue;
    return this;
  }

   /**
   * Get useProductionPlanningMaxValue
   * @return useProductionPlanningMaxValue
  **/
  @ApiModelProperty(value = "")
  public Boolean isUseProductionPlanningMaxValue() {
    return useProductionPlanningMaxValue;
  }

  public void setUseProductionPlanningMaxValue(Boolean useProductionPlanningMaxValue) {
    this.useProductionPlanningMaxValue = useProductionPlanningMaxValue;
  }

  public GetFilteredProductionTaskQuery packingDescriptionMinValue(String packingDescriptionMinValue) {
    this.packingDescriptionMinValue = packingDescriptionMinValue;
    return this;
  }

   /**
   * Get packingDescriptionMinValue
   * @return packingDescriptionMinValue
  **/
  @ApiModelProperty(value = "")
  public String getPackingDescriptionMinValue() {
    return packingDescriptionMinValue;
  }

  public void setPackingDescriptionMinValue(String packingDescriptionMinValue) {
    this.packingDescriptionMinValue = packingDescriptionMinValue;
  }

  public GetFilteredProductionTaskQuery packingDescriptionMaxValue(String packingDescriptionMaxValue) {
    this.packingDescriptionMaxValue = packingDescriptionMaxValue;
    return this;
  }

   /**
   * Get packingDescriptionMaxValue
   * @return packingDescriptionMaxValue
  **/
  @ApiModelProperty(value = "")
  public String getPackingDescriptionMaxValue() {
    return packingDescriptionMaxValue;
  }

  public void setPackingDescriptionMaxValue(String packingDescriptionMaxValue) {
    this.packingDescriptionMaxValue = packingDescriptionMaxValue;
  }

  public GetFilteredProductionTaskQuery closedByUserIdMinValue(Integer closedByUserIdMinValue) {
    this.closedByUserIdMinValue = closedByUserIdMinValue;
    return this;
  }

   /**
   * Get closedByUserIdMinValue
   * @return closedByUserIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getClosedByUserIdMinValue() {
    return closedByUserIdMinValue;
  }

  public void setClosedByUserIdMinValue(Integer closedByUserIdMinValue) {
    this.closedByUserIdMinValue = closedByUserIdMinValue;
  }

  public GetFilteredProductionTaskQuery closedByUserIdMaxValue(Integer closedByUserIdMaxValue) {
    this.closedByUserIdMaxValue = closedByUserIdMaxValue;
    return this;
  }

   /**
   * Get closedByUserIdMaxValue
   * @return closedByUserIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getClosedByUserIdMaxValue() {
    return closedByUserIdMaxValue;
  }

  public void setClosedByUserIdMaxValue(Integer closedByUserIdMaxValue) {
    this.closedByUserIdMaxValue = closedByUserIdMaxValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetFilteredProductionTaskQuery getFilteredProductionTaskQuery = (GetFilteredProductionTaskQuery) o;
    return Objects.equals(this.statusMinValue, getFilteredProductionTaskQuery.statusMinValue) &&
        Objects.equals(this.statusMaxValue, getFilteredProductionTaskQuery.statusMaxValue) &&
        Objects.equals(this.recipeGroupIdMinValue, getFilteredProductionTaskQuery.recipeGroupIdMinValue) &&
        Objects.equals(this.recipeGroupIdMaxValue, getFilteredProductionTaskQuery.recipeGroupIdMaxValue) &&
        Objects.equals(this.productionTaskIdMinValue, getFilteredProductionTaskQuery.productionTaskIdMinValue) &&
        Objects.equals(this.productionTaskIdMaxValue, getFilteredProductionTaskQuery.productionTaskIdMaxValue) &&
        Objects.equals(this.currentProductionStepIdMinValue, getFilteredProductionTaskQuery.currentProductionStepIdMinValue) &&
        Objects.equals(this.currentProductionStepIdMaxValue, getFilteredProductionTaskQuery.currentProductionStepIdMaxValue) &&
        Objects.equals(this.partijMinValue, getFilteredProductionTaskQuery.partijMinValue) &&
        Objects.equals(this.partijMaxValue, getFilteredProductionTaskQuery.partijMaxValue) &&
        Objects.equals(this.startDateMinValue, getFilteredProductionTaskQuery.startDateMinValue) &&
        Objects.equals(this.startDateMaxValue, getFilteredProductionTaskQuery.startDateMaxValue) &&
        Objects.equals(this.endDateMinValue, getFilteredProductionTaskQuery.endDateMinValue) &&
        Objects.equals(this.endDateMaxValue, getFilteredProductionTaskQuery.endDateMaxValue) &&
        Objects.equals(this.bestBeforeDateMinValue, getFilteredProductionTaskQuery.bestBeforeDateMinValue) &&
        Objects.equals(this.bestBeforeDateMaxValue, getFilteredProductionTaskQuery.bestBeforeDateMaxValue) &&
        Objects.equals(this.articleIdMinValue, getFilteredProductionTaskQuery.articleIdMinValue) &&
        Objects.equals(this.articleIdMaxValue, getFilteredProductionTaskQuery.articleIdMaxValue) &&
        Objects.equals(this.descriptionMinValue, getFilteredProductionTaskQuery.descriptionMinValue) &&
        Objects.equals(this.descriptionMaxValue, getFilteredProductionTaskQuery.descriptionMaxValue) &&
        Objects.equals(this.producingMultiBranchIdMinValue, getFilteredProductionTaskQuery.producingMultiBranchIdMinValue) &&
        Objects.equals(this.producingMultiBranchIdMaxValue, getFilteredProductionTaskQuery.producingMultiBranchIdMaxValue) &&
        Objects.equals(this.productionSequenceMinValue, getFilteredProductionTaskQuery.productionSequenceMinValue) &&
        Objects.equals(this.productionSequenceMaxValue, getFilteredProductionTaskQuery.productionSequenceMaxValue) &&
        Objects.equals(this.useProductionPlanningMinValue, getFilteredProductionTaskQuery.useProductionPlanningMinValue) &&
        Objects.equals(this.useProductionPlanningMaxValue, getFilteredProductionTaskQuery.useProductionPlanningMaxValue) &&
        Objects.equals(this.packingDescriptionMinValue, getFilteredProductionTaskQuery.packingDescriptionMinValue) &&
        Objects.equals(this.packingDescriptionMaxValue, getFilteredProductionTaskQuery.packingDescriptionMaxValue) &&
        Objects.equals(this.closedByUserIdMinValue, getFilteredProductionTaskQuery.closedByUserIdMinValue) &&
        Objects.equals(this.closedByUserIdMaxValue, getFilteredProductionTaskQuery.closedByUserIdMaxValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(statusMinValue, statusMaxValue, recipeGroupIdMinValue, recipeGroupIdMaxValue, productionTaskIdMinValue, productionTaskIdMaxValue, currentProductionStepIdMinValue, currentProductionStepIdMaxValue, partijMinValue, partijMaxValue, startDateMinValue, startDateMaxValue, endDateMinValue, endDateMaxValue, bestBeforeDateMinValue, bestBeforeDateMaxValue, articleIdMinValue, articleIdMaxValue, descriptionMinValue, descriptionMaxValue, producingMultiBranchIdMinValue, producingMultiBranchIdMaxValue, productionSequenceMinValue, productionSequenceMaxValue, useProductionPlanningMinValue, useProductionPlanningMaxValue, packingDescriptionMinValue, packingDescriptionMaxValue, closedByUserIdMinValue, closedByUserIdMaxValue);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetFilteredProductionTaskQuery {\n");
    
    sb.append("    statusMinValue: ").append(toIndentedString(statusMinValue)).append("\n");
    sb.append("    statusMaxValue: ").append(toIndentedString(statusMaxValue)).append("\n");
    sb.append("    recipeGroupIdMinValue: ").append(toIndentedString(recipeGroupIdMinValue)).append("\n");
    sb.append("    recipeGroupIdMaxValue: ").append(toIndentedString(recipeGroupIdMaxValue)).append("\n");
    sb.append("    productionTaskIdMinValue: ").append(toIndentedString(productionTaskIdMinValue)).append("\n");
    sb.append("    productionTaskIdMaxValue: ").append(toIndentedString(productionTaskIdMaxValue)).append("\n");
    sb.append("    currentProductionStepIdMinValue: ").append(toIndentedString(currentProductionStepIdMinValue)).append("\n");
    sb.append("    currentProductionStepIdMaxValue: ").append(toIndentedString(currentProductionStepIdMaxValue)).append("\n");
    sb.append("    partijMinValue: ").append(toIndentedString(partijMinValue)).append("\n");
    sb.append("    partijMaxValue: ").append(toIndentedString(partijMaxValue)).append("\n");
    sb.append("    startDateMinValue: ").append(toIndentedString(startDateMinValue)).append("\n");
    sb.append("    startDateMaxValue: ").append(toIndentedString(startDateMaxValue)).append("\n");
    sb.append("    endDateMinValue: ").append(toIndentedString(endDateMinValue)).append("\n");
    sb.append("    endDateMaxValue: ").append(toIndentedString(endDateMaxValue)).append("\n");
    sb.append("    bestBeforeDateMinValue: ").append(toIndentedString(bestBeforeDateMinValue)).append("\n");
    sb.append("    bestBeforeDateMaxValue: ").append(toIndentedString(bestBeforeDateMaxValue)).append("\n");
    sb.append("    articleIdMinValue: ").append(toIndentedString(articleIdMinValue)).append("\n");
    sb.append("    articleIdMaxValue: ").append(toIndentedString(articleIdMaxValue)).append("\n");
    sb.append("    descriptionMinValue: ").append(toIndentedString(descriptionMinValue)).append("\n");
    sb.append("    descriptionMaxValue: ").append(toIndentedString(descriptionMaxValue)).append("\n");
    sb.append("    producingMultiBranchIdMinValue: ").append(toIndentedString(producingMultiBranchIdMinValue)).append("\n");
    sb.append("    producingMultiBranchIdMaxValue: ").append(toIndentedString(producingMultiBranchIdMaxValue)).append("\n");
    sb.append("    productionSequenceMinValue: ").append(toIndentedString(productionSequenceMinValue)).append("\n");
    sb.append("    productionSequenceMaxValue: ").append(toIndentedString(productionSequenceMaxValue)).append("\n");
    sb.append("    useProductionPlanningMinValue: ").append(toIndentedString(useProductionPlanningMinValue)).append("\n");
    sb.append("    useProductionPlanningMaxValue: ").append(toIndentedString(useProductionPlanningMaxValue)).append("\n");
    sb.append("    packingDescriptionMinValue: ").append(toIndentedString(packingDescriptionMinValue)).append("\n");
    sb.append("    packingDescriptionMaxValue: ").append(toIndentedString(packingDescriptionMaxValue)).append("\n");
    sb.append("    closedByUserIdMinValue: ").append(toIndentedString(closedByUserIdMinValue)).append("\n");
    sb.append("    closedByUserIdMaxValue: ").append(toIndentedString(closedByUserIdMaxValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

