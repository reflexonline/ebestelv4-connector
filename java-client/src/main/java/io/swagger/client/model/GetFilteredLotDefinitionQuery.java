/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * GetFilteredLotDefinitionQuery
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class GetFilteredLotDefinitionQuery {
  @SerializedName("lotDefinitionIdMinValue")
  private Integer lotDefinitionIdMinValue = null;

  @SerializedName("lotDefinitionIdMaxValue")
  private Integer lotDefinitionIdMaxValue = null;

  @SerializedName("descriptionMinValue")
  private String descriptionMinValue = null;

  @SerializedName("descriptionMaxValue")
  private String descriptionMaxValue = null;

  public GetFilteredLotDefinitionQuery lotDefinitionIdMinValue(Integer lotDefinitionIdMinValue) {
    this.lotDefinitionIdMinValue = lotDefinitionIdMinValue;
    return this;
  }

   /**
   * Get lotDefinitionIdMinValue
   * @return lotDefinitionIdMinValue
  **/
  @ApiModelProperty(value = "")
  public Integer getLotDefinitionIdMinValue() {
    return lotDefinitionIdMinValue;
  }

  public void setLotDefinitionIdMinValue(Integer lotDefinitionIdMinValue) {
    this.lotDefinitionIdMinValue = lotDefinitionIdMinValue;
  }

  public GetFilteredLotDefinitionQuery lotDefinitionIdMaxValue(Integer lotDefinitionIdMaxValue) {
    this.lotDefinitionIdMaxValue = lotDefinitionIdMaxValue;
    return this;
  }

   /**
   * Get lotDefinitionIdMaxValue
   * @return lotDefinitionIdMaxValue
  **/
  @ApiModelProperty(value = "")
  public Integer getLotDefinitionIdMaxValue() {
    return lotDefinitionIdMaxValue;
  }

  public void setLotDefinitionIdMaxValue(Integer lotDefinitionIdMaxValue) {
    this.lotDefinitionIdMaxValue = lotDefinitionIdMaxValue;
  }

  public GetFilteredLotDefinitionQuery descriptionMinValue(String descriptionMinValue) {
    this.descriptionMinValue = descriptionMinValue;
    return this;
  }

   /**
   * Get descriptionMinValue
   * @return descriptionMinValue
  **/
  @ApiModelProperty(value = "")
  public String getDescriptionMinValue() {
    return descriptionMinValue;
  }

  public void setDescriptionMinValue(String descriptionMinValue) {
    this.descriptionMinValue = descriptionMinValue;
  }

  public GetFilteredLotDefinitionQuery descriptionMaxValue(String descriptionMaxValue) {
    this.descriptionMaxValue = descriptionMaxValue;
    return this;
  }

   /**
   * Get descriptionMaxValue
   * @return descriptionMaxValue
  **/
  @ApiModelProperty(value = "")
  public String getDescriptionMaxValue() {
    return descriptionMaxValue;
  }

  public void setDescriptionMaxValue(String descriptionMaxValue) {
    this.descriptionMaxValue = descriptionMaxValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetFilteredLotDefinitionQuery getFilteredLotDefinitionQuery = (GetFilteredLotDefinitionQuery) o;
    return Objects.equals(this.lotDefinitionIdMinValue, getFilteredLotDefinitionQuery.lotDefinitionIdMinValue) &&
        Objects.equals(this.lotDefinitionIdMaxValue, getFilteredLotDefinitionQuery.lotDefinitionIdMaxValue) &&
        Objects.equals(this.descriptionMinValue, getFilteredLotDefinitionQuery.descriptionMinValue) &&
        Objects.equals(this.descriptionMaxValue, getFilteredLotDefinitionQuery.descriptionMaxValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(lotDefinitionIdMinValue, lotDefinitionIdMaxValue, descriptionMinValue, descriptionMaxValue);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetFilteredLotDefinitionQuery {\n");
    
    sb.append("    lotDefinitionIdMinValue: ").append(toIndentedString(lotDefinitionIdMinValue)).append("\n");
    sb.append("    lotDefinitionIdMaxValue: ").append(toIndentedString(lotDefinitionIdMaxValue)).append("\n");
    sb.append("    descriptionMinValue: ").append(toIndentedString(descriptionMinValue)).append("\n");
    sb.append("    descriptionMaxValue: ").append(toIndentedString(descriptionMaxValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

