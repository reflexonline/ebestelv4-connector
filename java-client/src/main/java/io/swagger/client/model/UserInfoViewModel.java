/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * UserInfoViewModel
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-06-15T06:47:22.067Z")
public class UserInfoViewModel {
  @SerializedName("email")
  private String email = null;

  @SerializedName("hasRegistered")
  private Boolean hasRegistered = null;

  @SerializedName("loginProvider")
  private String loginProvider = null;

  public UserInfoViewModel email(String email) {
    this.email = email;
    return this;
  }

   /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(value = "")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public UserInfoViewModel hasRegistered(Boolean hasRegistered) {
    this.hasRegistered = hasRegistered;
    return this;
  }

   /**
   * Get hasRegistered
   * @return hasRegistered
  **/
  @ApiModelProperty(value = "")
  public Boolean isHasRegistered() {
    return hasRegistered;
  }

  public void setHasRegistered(Boolean hasRegistered) {
    this.hasRegistered = hasRegistered;
  }

  public UserInfoViewModel loginProvider(String loginProvider) {
    this.loginProvider = loginProvider;
    return this;
  }

   /**
   * Get loginProvider
   * @return loginProvider
  **/
  @ApiModelProperty(value = "")
  public String getLoginProvider() {
    return loginProvider;
  }

  public void setLoginProvider(String loginProvider) {
    this.loginProvider = loginProvider;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserInfoViewModel userInfoViewModel = (UserInfoViewModel) o;
    return Objects.equals(this.email, userInfoViewModel.email) &&
        Objects.equals(this.hasRegistered, userInfoViewModel.hasRegistered) &&
        Objects.equals(this.loginProvider, userInfoViewModel.loginProvider);
  }

  @Override
  public int hashCode() {
    return Objects.hash(email, hasRegistered, loginProvider);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserInfoViewModel {\n");
    
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    hasRegistered: ").append(toIndentedString(hasRegistered)).append("\n");
    sb.append("    loginProvider: ").append(toIndentedString(loginProvider)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

