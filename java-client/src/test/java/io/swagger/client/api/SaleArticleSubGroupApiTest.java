/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.GetFilteredSaleArticleSubGroupQuery;
import io.swagger.client.model.SaleArticleSubGroupDto;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for SaleArticleSubGroupApi
 */
@Ignore
public class SaleArticleSubGroupApiTest {

    private final SaleArticleSubGroupApi api = new SaleArticleSubGroupApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void saleArticleSubGroupCreateSaleArticleSubGroupTest() throws ApiException {
        SaleArticleSubGroupDto dto = null;
        Object response = api.saleArticleSubGroupCreateSaleArticleSubGroup(dto);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void saleArticleSubGroupDeleteSaleArticleSubGroupTest() throws ApiException {
        Integer subgroupId = null;
        Object response = api.saleArticleSubGroupDeleteSaleArticleSubGroup(subgroupId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void saleArticleSubGroupExistsSaleArticleSubGroupTest() throws ApiException {
        Integer subgroupId = null;
        Object response = api.saleArticleSubGroupExistsSaleArticleSubGroup(subgroupId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void saleArticleSubGroupGetSaleArticleSubGroupTest() throws ApiException {
        Integer subgroupId = null;
        Object response = api.saleArticleSubGroupGetSaleArticleSubGroup(subgroupId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void saleArticleSubGroupGetSaleArticleSubGroupsTest() throws ApiException {
        GetFilteredSaleArticleSubGroupQuery query = null;
        Object response = api.saleArticleSubGroupGetSaleArticleSubGroups(query);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void saleArticleSubGroupUpdateSaleArticleSubGroupTest() throws ApiException {
        Integer subgroupId = null;
        SaleArticleSubGroupDto dto = null;
        Object response = api.saleArticleSubGroupUpdateSaleArticleSubGroup(subgroupId, dto);

        // TODO: test validations
    }
    
}
