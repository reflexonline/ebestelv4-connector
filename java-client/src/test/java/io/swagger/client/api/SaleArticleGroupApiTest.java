/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.GetFilteredSaleArticleGroupQuery;
import io.swagger.client.model.SaleArticleGroupDto;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for SaleArticleGroupApi
 */
@Ignore
public class SaleArticleGroupApiTest {

    private final SaleArticleGroupApi api = new SaleArticleGroupApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void saleArticleGroupCreateSaleArticleGroupTest() throws ApiException {
        SaleArticleGroupDto dto = null;
        Object response = api.saleArticleGroupCreateSaleArticleGroup(dto);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void saleArticleGroupDeleteSaleArticleGroupTest() throws ApiException {
        Integer number = null;
        Object response = api.saleArticleGroupDeleteSaleArticleGroup(number);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void saleArticleGroupExistsSaleArticleGroupTest() throws ApiException {
        Integer number = null;
        Object response = api.saleArticleGroupExistsSaleArticleGroup(number);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void saleArticleGroupGetSaleArticleGroupTest() throws ApiException {
        Integer number = null;
        Object response = api.saleArticleGroupGetSaleArticleGroup(number);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void saleArticleGroupGetSaleArticleGroupsTest() throws ApiException {
        GetFilteredSaleArticleGroupQuery query = null;
        Object response = api.saleArticleGroupGetSaleArticleGroups(query);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void saleArticleGroupUpdateSaleArticleGroupTest() throws ApiException {
        Integer number = null;
        SaleArticleGroupDto dto = null;
        Object response = api.saleArticleGroupUpdateSaleArticleGroup(number, dto);

        // TODO: test validations
    }
    
}
