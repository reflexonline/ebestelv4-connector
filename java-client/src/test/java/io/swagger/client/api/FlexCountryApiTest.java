/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.FlexCountryDto;
import io.swagger.client.model.GetFilteredFlexCountryQuery;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for FlexCountryApi
 */
@Ignore
public class FlexCountryApiTest {

    private final FlexCountryApi api = new FlexCountryApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void flexCountryCreateFlexCountryTest() throws ApiException {
        FlexCountryDto dto = null;
        Object response = api.flexCountryCreateFlexCountry(dto);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void flexCountryDeleteFlexCountryTest() throws ApiException {
        Integer countryId = null;
        Object response = api.flexCountryDeleteFlexCountry(countryId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void flexCountryExistsFlexCountryTest() throws ApiException {
        Integer countryId = null;
        Object response = api.flexCountryExistsFlexCountry(countryId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void flexCountryGetFlexCountriesTest() throws ApiException {
        GetFilteredFlexCountryQuery query = null;
        Object response = api.flexCountryGetFlexCountries(query);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void flexCountryGetFlexCountryTest() throws ApiException {
        Integer countryId = null;
        Object response = api.flexCountryGetFlexCountry(countryId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void flexCountryUpdateFlexCountryTest() throws ApiException {
        Integer countryId = null;
        FlexCountryDto dto = null;
        Object response = api.flexCountryUpdateFlexCountry(countryId, dto);

        // TODO: test validations
    }
    
}
