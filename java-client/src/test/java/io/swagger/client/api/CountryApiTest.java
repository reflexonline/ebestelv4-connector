/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.CountryDto;
import io.swagger.client.model.GetFilteredCountryQuery;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for CountryApi
 */
@Ignore
public class CountryApiTest {

    private final CountryApi api = new CountryApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void countryCreateCountryTest() throws ApiException {
        CountryDto dto = null;
        Object response = api.countryCreateCountry(dto);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void countryDeleteCountryTest() throws ApiException {
        String isoCode = null;
        Object response = api.countryDeleteCountry(isoCode);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void countryExistsCountryTest() throws ApiException {
        String isoCode = null;
        Object response = api.countryExistsCountry(isoCode);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void countryGetCountriesTest() throws ApiException {
        GetFilteredCountryQuery query = null;
        Object response = api.countryGetCountries(query);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void countryGetCountryTest() throws ApiException {
        String isoCode = null;
        Object response = api.countryGetCountry(isoCode);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void countryUpdateCountryTest() throws ApiException {
        String isoCode = null;
        CountryDto dto = null;
        Object response = api.countryUpdateCountry(isoCode, dto);

        // TODO: test validations
    }
    
}
