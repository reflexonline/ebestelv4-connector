/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.AddressDto;
import io.swagger.client.model.GetFilteredAddressQuery;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for AddressApi
 */
@Ignore
public class AddressApiTest {

    private final AddressApi api = new AddressApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void addressCreateAddressTest() throws ApiException {
        AddressDto dto = null;
        Object response = api.addressCreateAddress(dto);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void addressDeleteAddressTest() throws ApiException {
        Integer addressId = null;
        Object response = api.addressDeleteAddress(addressId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void addressExistsAddressTest() throws ApiException {
        Integer addressId = null;
        Object response = api.addressExistsAddress(addressId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void addressGetAddressTest() throws ApiException {
        Integer addressId = null;
        Object response = api.addressGetAddress(addressId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void addressGetAddressesTest() throws ApiException {
        GetFilteredAddressQuery query = null;
        Object response = api.addressGetAddresses(query);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void addressUpdateAddressTest() throws ApiException {
        Integer addressId = null;
        AddressDto dto = null;
        Object response = api.addressUpdateAddress(addressId, dto);

        // TODO: test validations
    }
    
}
