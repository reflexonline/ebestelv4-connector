/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.GetFilteredOrderCharacteristicQuery;
import io.swagger.client.model.OrderCharacteristicDto;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for OrderCharacteristicApi
 */
@Ignore
public class OrderCharacteristicApiTest {

    private final OrderCharacteristicApi api = new OrderCharacteristicApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void orderCharacteristicCreateOrderCharacteristicTest() throws ApiException {
        OrderCharacteristicDto dto = null;
        Object response = api.orderCharacteristicCreateOrderCharacteristic(dto);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void orderCharacteristicDeleteOrderCharacteristicTest() throws ApiException {
        Integer orderCharacteristicId = null;
        Object response = api.orderCharacteristicDeleteOrderCharacteristic(orderCharacteristicId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void orderCharacteristicExistsOrderCharacteristicTest() throws ApiException {
        Integer orderCharacteristicId = null;
        Object response = api.orderCharacteristicExistsOrderCharacteristic(orderCharacteristicId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void orderCharacteristicGetOrderCharacteristicTest() throws ApiException {
        Integer orderCharacteristicId = null;
        Object response = api.orderCharacteristicGetOrderCharacteristic(orderCharacteristicId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void orderCharacteristicGetOrderCharacteristicsTest() throws ApiException {
        GetFilteredOrderCharacteristicQuery query = null;
        Object response = api.orderCharacteristicGetOrderCharacteristics(query);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void orderCharacteristicUpdateOrderCharacteristicTest() throws ApiException {
        Integer orderCharacteristicId = null;
        OrderCharacteristicDto dto = null;
        Object response = api.orderCharacteristicUpdateOrderCharacteristic(orderCharacteristicId, dto);

        // TODO: test validations
    }
    
}
