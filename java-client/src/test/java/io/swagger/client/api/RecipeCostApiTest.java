/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.GetFilteredRecipeCostQuery;
import io.swagger.client.model.RecipeCostDto;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for RecipeCostApi
 */
@Ignore
public class RecipeCostApiTest {

    private final RecipeCostApi api = new RecipeCostApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void recipeCostCreateRecipeCostTest() throws ApiException {
        RecipeCostDto dto = null;
        Object response = api.recipeCostCreateRecipeCost(dto);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void recipeCostDeleteRecipeCostTest() throws ApiException {
        Integer recipeCostId = null;
        Object response = api.recipeCostDeleteRecipeCost(recipeCostId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void recipeCostExistsRecipeCostTest() throws ApiException {
        Integer recipeCostId = null;
        Object response = api.recipeCostExistsRecipeCost(recipeCostId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void recipeCostGetRecipeCostTest() throws ApiException {
        Integer recipeCostId = null;
        Object response = api.recipeCostGetRecipeCost(recipeCostId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void recipeCostGetRecipeCostsTest() throws ApiException {
        GetFilteredRecipeCostQuery query = null;
        Object response = api.recipeCostGetRecipeCosts(query);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void recipeCostUpdateRecipeCostTest() throws ApiException {
        Integer recipeCostId = null;
        RecipeCostDto dto = null;
        Object response = api.recipeCostUpdateRecipeCost(recipeCostId, dto);

        // TODO: test validations
    }
    
}
