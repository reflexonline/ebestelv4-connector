/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.GetFilteredProductionPlanningFilterQuery;
import io.swagger.client.model.ProductionPlanningFilterDto;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for ProductionPlanningFilterApi
 */
@Ignore
public class ProductionPlanningFilterApiTest {

    private final ProductionPlanningFilterApi api = new ProductionPlanningFilterApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void productionPlanningFilterCreateProductionPlanningFilterTest() throws ApiException {
        ProductionPlanningFilterDto dto = null;
        Object response = api.productionPlanningFilterCreateProductionPlanningFilter(dto);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void productionPlanningFilterDeleteProductionPlanningFilterTest() throws ApiException {
        Integer number = null;
        Object response = api.productionPlanningFilterDeleteProductionPlanningFilter(number);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void productionPlanningFilterExistsProductionPlanningFilterTest() throws ApiException {
        Integer number = null;
        Object response = api.productionPlanningFilterExistsProductionPlanningFilter(number);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void productionPlanningFilterGetProductionPlanningFilterTest() throws ApiException {
        Integer number = null;
        Object response = api.productionPlanningFilterGetProductionPlanningFilter(number);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void productionPlanningFilterGetProductionPlanningFiltersTest() throws ApiException {
        GetFilteredProductionPlanningFilterQuery query = null;
        Object response = api.productionPlanningFilterGetProductionPlanningFilters(query);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void productionPlanningFilterUpdateProductionPlanningFilterTest() throws ApiException {
        Integer number = null;
        ProductionPlanningFilterDto dto = null;
        Object response = api.productionPlanningFilterUpdateProductionPlanningFilter(number, dto);

        // TODO: test validations
    }
    
}
