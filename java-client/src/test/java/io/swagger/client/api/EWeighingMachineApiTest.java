/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.EWeighingMachineDto;
import io.swagger.client.model.GetFilteredEWeighingMachineQuery;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for EWeighingMachineApi
 */
@Ignore
public class EWeighingMachineApiTest {

    private final EWeighingMachineApi api = new EWeighingMachineApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void eWeighingMachineCreateEWeighingMachineTest() throws ApiException {
        EWeighingMachineDto dto = null;
        Object response = api.eWeighingMachineCreateEWeighingMachine(dto);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void eWeighingMachineDeleteEWeighingMachineTest() throws ApiException {
        Integer machineId = null;
        Object response = api.eWeighingMachineDeleteEWeighingMachine(machineId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void eWeighingMachineExistsEWeighingMachineTest() throws ApiException {
        Integer machineId = null;
        Object response = api.eWeighingMachineExistsEWeighingMachine(machineId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void eWeighingMachineGetEWeighingMachineTest() throws ApiException {
        Integer machineId = null;
        Object response = api.eWeighingMachineGetEWeighingMachine(machineId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void eWeighingMachineGetEWeighingMachinesTest() throws ApiException {
        GetFilteredEWeighingMachineQuery query = null;
        Object response = api.eWeighingMachineGetEWeighingMachines(query);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void eWeighingMachineUpdateEWeighingMachineTest() throws ApiException {
        Integer machineId = null;
        EWeighingMachineDto dto = null;
        Object response = api.eWeighingMachineUpdateEWeighingMachine(machineId, dto);

        // TODO: test validations
    }
    
}
