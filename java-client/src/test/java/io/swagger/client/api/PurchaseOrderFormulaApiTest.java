/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.GetFilteredPurchaseOrderFormulaQuery;
import io.swagger.client.model.PurchaseOrderFormulaDto;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for PurchaseOrderFormulaApi
 */
@Ignore
public class PurchaseOrderFormulaApiTest {

    private final PurchaseOrderFormulaApi api = new PurchaseOrderFormulaApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void purchaseOrderFormulaCreatePurchaseOrderFormulaTest() throws ApiException {
        PurchaseOrderFormulaDto dto = null;
        Object response = api.purchaseOrderFormulaCreatePurchaseOrderFormula(dto);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void purchaseOrderFormulaDeletePurchaseOrderFormulaTest() throws ApiException {
        Integer number = null;
        Object response = api.purchaseOrderFormulaDeletePurchaseOrderFormula(number);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void purchaseOrderFormulaExistsPurchaseOrderFormulaTest() throws ApiException {
        Integer number = null;
        Object response = api.purchaseOrderFormulaExistsPurchaseOrderFormula(number);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void purchaseOrderFormulaGetPurchaseOrderFormulaTest() throws ApiException {
        Integer number = null;
        Object response = api.purchaseOrderFormulaGetPurchaseOrderFormula(number);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void purchaseOrderFormulaGetPurchaseOrderFormulasTest() throws ApiException {
        GetFilteredPurchaseOrderFormulaQuery query = null;
        Object response = api.purchaseOrderFormulaGetPurchaseOrderFormulas(query);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void purchaseOrderFormulaUpdatePurchaseOrderFormulaTest() throws ApiException {
        Integer number = null;
        PurchaseOrderFormulaDto dto = null;
        Object response = api.purchaseOrderFormulaUpdatePurchaseOrderFormula(number, dto);

        // TODO: test validations
    }
    
}
