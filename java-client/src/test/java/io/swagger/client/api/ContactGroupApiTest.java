/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.ContactGroupDto;
import io.swagger.client.model.GetFilteredContactGroupQuery;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for ContactGroupApi
 */
@Ignore
public class ContactGroupApiTest {

    private final ContactGroupApi api = new ContactGroupApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void contactGroupCreateContactGroupTest() throws ApiException {
        ContactGroupDto dto = null;
        Object response = api.contactGroupCreateContactGroup(dto);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void contactGroupDeleteContactGroupTest() throws ApiException {
        Integer contactGroupId = null;
        Object response = api.contactGroupDeleteContactGroup(contactGroupId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void contactGroupExistsContactGroupTest() throws ApiException {
        Integer contactGroupId = null;
        Object response = api.contactGroupExistsContactGroup(contactGroupId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void contactGroupGetContactGroupTest() throws ApiException {
        Integer contactGroupId = null;
        Object response = api.contactGroupGetContactGroup(contactGroupId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void contactGroupGetContactGroupsTest() throws ApiException {
        GetFilteredContactGroupQuery query = null;
        Object response = api.contactGroupGetContactGroups(query);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void contactGroupUpdateContactGroupTest() throws ApiException {
        Integer contactGroupId = null;
        ContactGroupDto dto = null;
        Object response = api.contactGroupUpdateContactGroup(contactGroupId, dto);

        // TODO: test validations
    }
    
}
