/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.AccountingPeriodDto;
import io.swagger.client.model.GetFilteredAccountingPeriodQuery;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for AccountingPeriodApi
 */
@Ignore
public class AccountingPeriodApiTest {

    private final AccountingPeriodApi api = new AccountingPeriodApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void accountingPeriodCreateAccountingPeriodTest() throws ApiException {
        AccountingPeriodDto dto = null;
        Object response = api.accountingPeriodCreateAccountingPeriod(dto);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void accountingPeriodDeleteAccountingPeriodTest() throws ApiException {
        Integer year = null;
        Integer period = null;
        Object response = api.accountingPeriodDeleteAccountingPeriod(year, period);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void accountingPeriodExistsAccountingPeriodTest() throws ApiException {
        Integer year = null;
        Integer period = null;
        Object response = api.accountingPeriodExistsAccountingPeriod(year, period);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void accountingPeriodGetAccountingPeriodTest() throws ApiException {
        Integer year = null;
        Integer period = null;
        Object response = api.accountingPeriodGetAccountingPeriod(year, period);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void accountingPeriodGetAccountingPeriodsTest() throws ApiException {
        GetFilteredAccountingPeriodQuery query = null;
        Object response = api.accountingPeriodGetAccountingPeriods(query);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void accountingPeriodUpdateAccountingPeriodTest() throws ApiException {
        Integer year = null;
        Integer period = null;
        AccountingPeriodDto dto = null;
        Object response = api.accountingPeriodUpdateAccountingPeriod(year, period, dto);

        // TODO: test validations
    }
    
}
