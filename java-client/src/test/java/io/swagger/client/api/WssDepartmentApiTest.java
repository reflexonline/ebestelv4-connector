/*
 * Reflex 3000 API documentatie
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.GetFilteredWssDepartmentQuery;
import io.swagger.client.model.WssDepartmentDto;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for WssDepartmentApi
 */
@Ignore
public class WssDepartmentApiTest {

    private final WssDepartmentApi api = new WssDepartmentApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void wssDepartmentCreateWssDepartmentTest() throws ApiException {
        WssDepartmentDto dto = null;
        Object response = api.wssDepartmentCreateWssDepartment(dto);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void wssDepartmentDeleteWssDepartmentTest() throws ApiException {
        Integer number = null;
        Object response = api.wssDepartmentDeleteWssDepartment(number);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void wssDepartmentExistsWssDepartmentTest() throws ApiException {
        Integer number = null;
        Object response = api.wssDepartmentExistsWssDepartment(number);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void wssDepartmentGetWssDepartmentTest() throws ApiException {
        Integer number = null;
        Object response = api.wssDepartmentGetWssDepartment(number);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void wssDepartmentGetWssDepartmentsTest() throws ApiException {
        GetFilteredWssDepartmentQuery query = null;
        Object response = api.wssDepartmentGetWssDepartments(query);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void wssDepartmentUpdateWssDepartmentTest() throws ApiException {
        Integer number = null;
        WssDepartmentDto dto = null;
        Object response = api.wssDepartmentUpdateWssDepartment(number, dto);

        // TODO: test validations
    }
    
}
